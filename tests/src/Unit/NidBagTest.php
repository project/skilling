<?php
namespace Drupal\Tests\skilling\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\skilling\NidBag;

/**
 * Test the NidBag class.
 *
 * @group skilling
 */
class NidBagTest extends UnitTestCase {

  /**
   * Testing NidBag.
   */
  public function testNidBag1() {
    $bag = new NidBag();
    $bag->addToBag('pony', 'rainbow');
    $result = $bag->getNidsInPocket('pony');
    $this->assertArrayEquals(['rainbow'], $result);
  }

  /**
   * Testing NidBag.
   */
  public function testNidBag2() {
    $bag = new NidBag();
    $bag->addToBag('pony', 'rainbow');
    $bag->addToBag('pony', 'unicorn');
    $result = $bag->getNidsInPocket('pony');
    $this->assertArrayEquals(['rainbow', 'unicorn'], $result);
  }

  /**
   * Testing NidBag.
   */
  public function testNidBag3() {
    $bag = new NidBag();
    $result = $bag->getNidsInPocket('pony');
    $this->assertArrayEquals([], $result);
  }

  /**
   * Testing NidBag.
   */
  public function testNidBag4() {
    $bag = new NidBag();
    $bag->addToBag('pony', 'rainbow');
    $bag->addToBag('pony', 'unicorn');
    $bag->addToBag('sparkle', 'globe');
    $result = $bag->getAllNidsInBag();
    $this->assertArrayEquals(
      ['rainbow', 'unicorn', 'globe'],
      $result
    );
  }

  /**
   * Testing NidBag.
   */
  public function testNidBag5() {
    $bag = new NidBag();
    $bag->addToBag('pony', 'rainbow');
    $bag->addToBag('pony', 'unicorn');
    $bag->addToBag('sparkle', 'globe');
    $bag->clearAllPockets();
    $result = $bag->getAllNidsInBag();
    $this->assertArrayEquals([], $result);
  }

}
