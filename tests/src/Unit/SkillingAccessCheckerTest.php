<?php  /** @noinspection PhpUndefinedMethodInspection */

namespace Drupal\Tests\skilling\Unit;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\skilling\Access\SkillingAccessChecker;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities;
use Drupal\Tests\UnitTestCase;
use Drupal\user\Entity\User;
use Drupal\Core\Messenger\Messenger;
use Drupal\skilling\Access\FieldAccessSpecialCaseChecker;

/**
 * Tests access checks.
 *
 * @group skilling
 */
class SkillingAccessCheckerTest extends UnitTestCase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManagerMock;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserMock;

  /**
   * The current class service.
   *
   * If the current user is enrolled in more than one class, s/he chooses
   * one of them to be the context for the site.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClassMock;

  /**
   * Service to check relationships between users.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $checkUserRelationshipsServiceMock;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactoryMock;

  /**
   * Class to make Skilling user objects.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactoryMock;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilitiesMock;

  /**
   * The Skilling parser service.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parserMock;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messengerMock;

  /**
   * The field access special case checker service.
   *
   * @var \Drupal\skilling\Access\FieldAccessSpecialCaseChecker
   */
  protected $fieldAccessSpecialCaseCheckerMock;

  /**
   * Access checker with basic mocked services.
   *
   * @var \Drupal\skilling\Access\SkillingAccessChecker
   */
  protected $skillingAccessChecker;

  /**
   * Set up for testing.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function setup() {
    // Make some basic mock services that have no methods.
    // They'll be replaced with more capable mocked services
    // as needed.
    $this->entityTypeManagerMock =
      $this
        ->getMockBuilder(EntityTypeManagerInterface::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->currentUserMock =
      $this
        ->getMockBuilder(SkillingCurrentUser::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->currentClassMock =
      $this
        ->getMockBuilder('Drupal\skilling\SkillingClass\SkillingCurrentClass')
        ->disableOriginalConstructor()
        ->setMethods(['getCurrentEnrollments'])
        ->getMock();
    $this->currentClassMock->expects($this->any())->method('getCurrentEnrollments')
      ->will($this->returnValue(1));
    $this->checkUserRelationshipsServiceMock =
      $this
        ->getMockBuilder(SkillingCheckUserRelationships::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->configFactoryMock =
      $this
        ->getMockBuilder(ConfigFactory::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->skillingUserFactoryMock =
      $this
        ->getMockBuilder(SkillingUserFactory::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->skillingUtilitiesMock =
      $this
        ->getMockBuilder(Utilities::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->parserMock =
      $this
        ->getMockBuilder(SkillingParser::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->messengerMock =
      $this
        ->getMockBuilder(Messenger::class)
        ->disableOriginalConstructor()
        ->getMock();
    $this->messengerMock->expects($this->any())
      ->method('addWarning')
      ->will($this->returnCallback('returnFirstArg'));
    $stub = $this->fieldAccessSpecialCaseCheckerMock =
      $this
        ->getMockBuilder(FieldAccessSpecialCaseChecker::class)
        ->disableOriginalConstructor()
        ->getMock();
    $stub
      ->method('defineAllowedSpecialCases')
      ->will($this->returnValue(TRUE));
    $stub
      ->method('checkForSpecialCase')
      ->will($this->returnValue(NULL));

    // Make a default Skilling access checker.
    $this->skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $this->currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
  }

  /**
   * Test access for node that is not a Skilling content type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function testIsNodeRelevant() {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue('evil'));

    $this->assertTrue(
      $this->skillingAccessChecker->isNodeAccess($nodeMock, 'edit')
    );
  }

  /**
   * Make a node.
   *
   * @param string $contentType
   *   The content type.
   *
   * @return mixed
   *   The node.
   */
  protected function makeBasicNode($contentType) {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('node'));
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue($contentType));
    return $nodeMock;
  }

  /**
   * Is a lesson node relevant?
   */
  public function testIsLessonNodeRelevant() {
    $nodeMock = $this->makeBasicNode('lesson');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected lesson node to be relevant.'
    );
  }

  /**
   * Is a design page node relevant?
   */
  public function testIsDesignPageNodeRelevant() {
    $nodeMock = $this->makeBasicNode('design_page');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected design page node to be relevant.'
    );
  }

  /**
   * Is an exercise node relevant?
   */
  public function testIsExerciseNodeRelevant() {
    $nodeMock = $this->makeBasicNode('exercise');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected exercise node to be relevant.'
    );
  }

  /**
   * Is a submission node relevant?
   */
  public function testIsSubmissionNodeRelevant() {
    $nodeMock = $this->makeBasicNode('submission');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected submission node to be relevant.'
    );
  }

  /**
   * Is an enrollment node relevant?
   */
  public function testIsEnrollmentNodeRelevant() {
    $nodeMock = $this->makeBasicNode('enrollment');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected enrollment node to be relevant.'
    );
  }

  /**
   * Is a class node relevant?
   */
  public function testIsClassNodeRelevant() {
    $nodeMock = $this->makeBasicNode('class');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected class node to be relevant.'
    );
  }

  /**
   * Is a calendar node relevant?
   */
  public function testIsCalendarNodeRelevant() {
    $nodeMock = $this->makeBasicNode('calendar');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected calendar node to be relevant.'
    );
  }

  /**
   * Is an event node relevant?
   */
  public function testEventIsNodeRelevant() {
    $nodeMock = $this->makeBasicNode('event');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected event node to be relevant.'
    );
  }

  /**
   * Is a rubric item node relevant?
   */
  public function testRubricItemIsNodeRelevant() {
    $nodeMock = $this->makeBasicNode('rubric_item');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected rubric item node to be relevant.'
    );
  }

  /**
   * Is a character node relevant?
   */
  public function testIsCharacterNodeRelevant() {
    $nodeMock = $this->makeBasicNode('character');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected character node to be relevant.'
    );
  }

  /**
   * Is a pattern node relevant?
   */
  public function testIsPatternNodeRelevant() {
    $nodeMock = $this->makeBasicNode('pattern');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected pattern node to be relevant.'
    );
  }

  /**
   * Is a principle node relevant?
   */
  public function testIsPrincipleNodeRelevant() {
    $nodeMock = $this->makeBasicNode('principle');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected principle node to be relevant.'
    );
  }

  /**
   * Is a model node relevant?
   */
  public function testIsModelNodeRelevant() {
    $nodeMock = $this->makeBasicNode('model');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected model node to be relevant.'
    );
  }

  /**
   * Is a FiB node relevant?
   */
  public function testIsFibNodeRelevant() {
    $nodeMock = $this->makeBasicNode('fill_in_the_blank');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected FiB node to be relevant.'
    );
  }

  /**
   * Is a history node relevant?
   */
  public function testIsHistoryNodeRelevant() {
    $nodeMock = $this->makeBasicNode('history');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected history node to be relevant.'
    );
  }

  /**
   * Is a reflect note node relevant?
   */
  public function testIsReflectNoteNodeRelevant() {
    $nodeMock = $this->makeBasicNode('reflect_note');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected reflect note node to be relevant.'
    );
  }

  /**
   * Is an MCQ node relevant?
   */
  public function testIsMcqNodeRelevant() {
    $nodeMock = $this->makeBasicNode('multiple_choice_question');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected MCQ node to be relevant.'
    );
  }

  /**
   * Is a non-Skilling content type irrelevant?
   */
  public function testIsNodeIrrelevant() {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('node'));
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue('forum'));
    $this->assertFalse(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected node to be irrelevant.'
    );
  }

  /**
   * Is a user entity relevant?
   */
  public function testIsUserEntityRelevant() {
    $userMock =
      $this
        ->getMockBuilder(User::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId'])
        ->getMock();
    $userMock->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('user'));
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($userMock),
      'Expected user to be relevant.'
    );
  }

  /**
   * Make a paragraph.
   *
   * @param string $paragraphType
   *   Paragraph type to make.
   *
   * @return mixed
   *   The paragraph.
   */
  protected function makeBasicParagraph($paragraphType) {
    $mockParagraph =
      $this
        ->getMockBuilder(Paragraph::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $mockParagraph->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('paragraph'));
    $mockParagraph->expects($this->any())->method('bundle')
      ->will($this->returnValue($paragraphType));
    return $mockParagraph;
  }

  /**
   * Is a calendar event paragraph relevant?
   */
  public function testIsCalendarEventParagraphRelevant() {
    $mockParagraph = $this->makeBasicParagraph('calendar_event');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected calendar event paragraph to be relevant.'
    );
  }

  /**
   * Is an MCQ response paragraph relevant?
   */
  public function testIsMcqResponseParagraphRelevant() {
    $mockParagraph = $this->makeBasicParagraph('multiple_choice_question_respons');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected MCQ response paragraph to be relevant.'
    );
  }

  /**
   * Is a FiB response paragraph relevant?
   */
  public function testIsFibResponseParagraphRelevant() {
    $mockParagraph = $this->makeBasicParagraph('fill_in_the_blank_question_respo');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected FiB response paragraph to be relevant.'
    );
  }

  /**
   * Is a rubric item response paragraph relevant?
   */
  public function testIsRubricItemResponseParagraphRelevant() {
    $mockParagraph = $this->makeBasicParagraph('rubric_item_response');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected rubric item response paragraph to be relevant.'
    );
  }

  /**
   * Is a paragraph type irrelevant?
   */
  public function testIsParagraphIrrelevant() {
    $mockParagraph = $this->makeBasicParagraph('evil');
    $this->assertFalse(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected evil paragraph to be irrelevant.'
    );
  }

  /**
   * Mock a current user.
   *
   * @param array $methods
   *   Methods to add to the created object.
   *
   * @return mixed
   *   The user mock.
   */
  protected function makeCurrentUser(array $methods) {
    $mockCurrentUser =
      $this
        ->getMockBuilder(SkillingCurrentUser::class)
        ->disableOriginalConstructor()
        ->setMethods(array_keys($methods))
        ->getMock();
    foreach ($methods as $method => $value) {
      $mockCurrentUser->expects($this->any())->method($method)
        ->will($this->returnValue($value));
    }
    return $mockCurrentUser;
  }

  /**
   * Make an admin current user.
   */
  protected function makeAdmin() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => TRUE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make an author current user.
   */
  protected function makeAuthor() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => TRUE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a reviewer current user.
   */
  protected function makeReviewer() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => TRUE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Mock an instructor current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeInstructor($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => TRUE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a grader current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeGrader($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => TRUE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a student current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeStudent($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => TRUE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make an anonymous current user.
   */
  protected function makeAnonymous() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => TRUE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Set the setting for whether anons and students can see design pages.
   *
   * @param bool $value
   *   Setting of the flag.
   *
   * @return mixed
   *   The mock.
   */
  protected function makeAnonSeeDesignPageSetting($value) {
    $immutableConfig =
      $this
        ->getMockBuilder(ImmutableConfig::class)
        ->disableOriginalConstructor()
        ->setMethods(['get'])
        ->getMock();
    $immutableConfig->expects($this->any())->method('get')
      ->will($this->returnValue($value));
    $configFactoryMock =
      $this
        ->getMockBuilder(ConfigFactory::class)
        ->disableOriginalConstructor()
        ->setMethods(['get'])
        ->getMock();
    $configFactoryMock->expects($this->any())->method('get')
      ->will($this->returnValue($immutableConfig));
    return $configFactoryMock;
  }

  protected $joins;

  protected $conditions;

  /**
   * Make an AlterableInterface mock.
   *
   * @return mixed
   *   The mock.
   */
  protected function setupSearchAlterableInterface() {
    $searchAlterableInterface =
      $this
        ->getMockBuilder(AlterableInterface::class)
        ->disableOriginalConstructor()
        ->setMethods([
          'join', 'condition', 'addTag', 'hasTag', 'hasAllTags',
          'addMetaData', 'getMetaData', 'hasAnyTag',
        ])
        ->getMock();
    $searchAlterableInterface->expects($this->any())->method('join')
      ->will($this->returnCallback(
        function ($p1, $p2, $p3) {
          $this->joins[] = [$p1, $p2, $p3];
        }
      ));
    $searchAlterableInterface->expects($this->any())->method('condition')
      ->will($this->returnCallback(
        function ($p1, $p2, $p3) {
          $this->conditions[] = [$p1, $p2, $p3];
        }
      ));
    return $searchAlterableInterface;
  }

  /**
   * Test altering search query.
   *
   * This is messy. joins and conditions are the arrays affected by the
   * method being tested. They need to be cleared before each test.
   *
   * The alterable interface mock appends data it gets to these arrays.
   *
   * We need a current user with some roles.
   *
   * We need a config with a flag showing whether students/anons can see
   * design pages.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function testQueryAlter1() {
    // Set up the access checker.
    // Current user is an admin.
    $currentUserMock = $this->makeAdmin();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(TRUE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $this->joins = [];
    $this->conditions = [];
    $searchAlterableInterface = $this->setupSearchAlterableInterface();
    $skillingAccessChecker->alterSearchQuery($searchAlterableInterface);
    $this->assertTrue(
      count($this->joins) > 0,
      'Expected data in joins array.'
    );
    $this->assertTrue(
      count($this->conditions) > 0,
      'Expected data in conditions array.'
    );
  }

  /**
   * Mock a field definition object.
   *
   * @param string $fieldName
   *   Field name.
   * @param string $entityType
   *   Entity type.
   * @param string $bundle
   *   Bundle.
   *
   * @return mixed
   *   Mock object
   */
  protected function makeFieldDefinitionMock($fieldName, $entityType, $bundle) {
    $methods = get_class_methods(FieldDefinitionInterface::class);
    $methods[] = 'get';
    $fieldDefinitionMock =
      $this
        ->getMockBuilder(FieldDefinitionInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $fieldDefinitionMock->expects($this->any())->method('getName')
      ->will($this->returnValue($fieldName));
    $fieldDefinitionMock->expects($this->any())->method('get')
      ->will(
        $this->returnCallback(
          function ($propertyName) use ($entityType, $bundle) {
            if ($propertyName === 'entity_type') {
              return $entityType;
            };
            if ($propertyName === 'bundle') {
              return $bundle;
            };
          }
        )
      );
    return $fieldDefinitionMock;
  }

  /**
   * Make a field item list.
   *
   * @param string $entityTypeId
   *   Entity type.
   * @param string $bundle
   *   Bundle name.
   *
   * @return mixed
   *   Mock object.
   */
  protected function makeFieldItemListMock($entityTypeId, $bundle) {
    $entity = $this->makeEntityMock($entityTypeId, $bundle);
    $methods = get_class_methods(FieldItemListInterface::class);
    $fieldItemList =
      $this
        ->getMockBuilder(FieldItemListInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $fieldItemList->expects($this->any())->method('getEntity')
      ->will($this->returnValue($entity));
    return $fieldItemList;
  }

  /**
   * Make a mock entity.
   *
   * @param string $entityTypeId
   *   Type id, e.g., node.
   * @param string $bundle
   *   Bundle.
   *
   * @return mixed
   *   The entity.
   */
  protected function makeEntityMock($entityTypeId, $bundle) {
    $methods = get_class_methods(EntityInterface::class);
    $entity =
      $this
        ->getMockBuilder(EntityInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $entity->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue($entityTypeId));
    $entity->expects($this->any())->method('bundle')
      ->will($this->returnValue($bundle));
    return $entity;
  }

  /**
   * Anonymous user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousLesson() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson tags.'
    );
  }

  /**
   * Student user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentLesson() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to lesson tags.'
    );
  }

  /**
   * Grader user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderLesson() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to lesson tags.'
    );
  }

  /**
   * Instructor user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorLesson() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to lesson tags.'
    );
  }

  /**
   * Reviewer user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerLesson() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to lesson tags.'
    );
  }

  /**
   * Author user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorLesson() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to lesson tags.'
    );
  }

  /**
   * Anonymous user access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'calendar');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', 'calendar'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', 'calendar'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'calendar'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to calendar body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'calendar'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to calendar notes.'
    );
  }

  /**
   * Student user access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'calendar';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to student body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to calendar notes.'
    );

  }

  /**
   * Grader access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'calendar';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to student body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to calendar notes.'
    );

  }

  /**
   * Instructor access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'calendar';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to student body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor edit access to calendar notes.'
    );

  }

  /**
   * Reviewer access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'calendar';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to student body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to calendar notes.'
    );

  }

  /**
   * Author access to calendar fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorCalendar() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'calendar';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Calendar events field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar_events', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to calendar events list.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to calendar events list.'
    );

    // Days field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_days', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to calendar days.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to calendar days.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to student body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to calendar body.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to calendar notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to calendar notes.'
    );

  }

  /**
   * Anonymous user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousCharacter() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to character field_where_referenced.'
    );

  }

  /**
   * Student user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentCharacter() {
    // Current user is a student.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to character field_where_referenced.'
    );

  }

  /**
   * Grader user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderCharacter() {
    // Current user is a grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to character field_where_referenced.'
    );

  }

  /**
   * Instructor user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorCharacter() {
    // Current user is an instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to character field_where_referenced.'
    );

  }

  /**
   * Reviewer user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerCharacter() {
    // Current user is a reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to character field_where_referenced.'
    );

  }

  /**
   * Author user access to character fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorCharacter() {
    // Current user is an author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'character';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Caption field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_caption', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to character field_caption.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to character field_caption.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to character field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to character field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to character field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to character field_notes.'
    );

    // Photo field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_photo', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to character photo.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to character photo.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to character field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to character field_where_referenced.'
    );

  }

  /**
   * Anonymous user access to class fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousClass() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'class';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to class field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to class field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to class field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to class field_notes.'
    );

    // Calendar field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to class field_calendar.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to class field_calendar.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to class body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to class body.'
    );

    // When starts field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_starts', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to class field_when_starts.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to class field_when_starts.'
    );

  }

  /**
   * Student user access to class fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentClass() {
    // Current user is Student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'class';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to class field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to class field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to class field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to class field_notes.'
    );

    // Calendar field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to class field_calendar.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to class field_calendar.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to class body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to class body.'
    );

    // When starts field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_starts', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to class field_when_starts.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to class field_when_starts.'
    );

  }

  /**
   * Grader user access to class fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderClass() {
    // Current user is Student.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'class';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to class field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to class field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to class field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to class field_notes.'
    );

    // Calendar field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to class field_calendar.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to class field_calendar.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to class body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to class body.'
    );

    // When starts field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_starts', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to class field_when_starts.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to class field_when_starts.'
    );

  }

  /**
   * Instructor user access to class fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorClass() {
    // Current user is Student.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'class';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to class field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to class field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to class field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to class field_notes.'
    );

    // Calendar field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to class field_calendar.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to class field_calendar.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to class body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to class body.'
    );

    // When starts field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_starts', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to class field_when_starts.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to class field_when_starts.'
    );

  }

  /**
   * Author user access to class fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorClass() {
    // Current user is Author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'class';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to class field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to class field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to class field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to class field_notes.'
    );

    // Calendar field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_calendar', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to class field_calendar.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to class field_calendar.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to class body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to class body.'
    );

    // When starts field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_starts', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to class field_when_starts.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to class field_when_starts.'
    );

  }

  /**
   * Anonymous user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousDesignPage1() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(TRUE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page field_tags.'
    );

  }

  /**
   * Anonymous user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousDesignPage2() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(FALSE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to design page field_tags.'
    );

  }

  /**
   * Student user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentDesignPage1() {
    // Current user is Student.
    $currentUserMock = $this->makeStudent();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(TRUE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page field_tags.'
    );

  }

  /**
   * Student user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentDesignPage2() {
    // Current user is Student.
    $currentUserMock = $this->makeStudent();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(FALSE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to design page field_tags.'
    );

  }

  /**
   * Instructor user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorDesignPage() {
    // Current user is Instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to design page field_tags.'
    );

  }

  /**
   * Reviewer user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerDesignPage() {
    // Current user is Instructor.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to design page field_tags.'
    );

  }

  /**
   * Author user access to design page fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorDesignPage() {
    // Current user is Author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'design_page';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to design page body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to design page body.'
    );

    // Attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to design page Attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to design page Attachments.'
    );

    // field_order_in_book field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to design page field_order_in_book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to design page field_order_in_book.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to design page field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected no author edit access to design page field_tags.'
    );

  }

  /**
   * Anonymous user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousEvent() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to event field_notes.'
    );

  }

  /**
   * Student user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentEvent() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to event field_notes.'
    );

  }

  /**
   * Grader user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderEvent() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to event field_notes.'
    );

  }

  /**
   * Instructor user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorEvent() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to event field_notes.'
    );
  }

  /**
   * Reviewer user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerEvent() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to event field_notes.'
    );
  }

  /**
   * Author user access to event fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorEvent() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'event';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to event body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to event body.'
    );

    // Event type field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to event field_event_type.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to event field_event_type.'
    );

    // Exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to event field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to event field_exercise.'
    );

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to event field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to event field_internal_name.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to event field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to event field_notes.'
    );
  }

  /**
   * Anonymous user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousExercise() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Student user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentExercise() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Reviewer user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerExercise() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Grader user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderExercise() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Instructor user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorExercise() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Author user access to exercise fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorExercise() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'exercise';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $forbidden = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to exercise field_internal_name.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to exercise body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to exercise body.'
    );

    // Rubric items.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_rubric_items', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise rubric items.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to exercise rubric items.'
    );

    // Tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to exercise tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to exercise tags.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to exercise attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to exercise hidden attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to exercise field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to exercise field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $forbidden, $actual, 'Expected author edit access to exercise field_where_referenced.'
    );
  }

  /**
   * Anonymous user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousFiB() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Student user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentFiB() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Grader user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderFiB() {
    // Current user is student.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected grader view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Instructor user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorFiB() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no instructor edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Reviewer user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerFiB() {
    // Current user is reviewer.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no reviewer edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected reviewer view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Author user access to FiB fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorFiB() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'fill_in_the_blank';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Internal name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to FiB field_internal_name.'
    );

    // Question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to FiB question.'
    );

    // Responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_fib_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to FiB responses.'
    );

    // No match response field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_no_match_response', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB no match response.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author edit access to FiB no match response.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to FiB field_notes.'
    );

    // field_where_referenced.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected author view access to FiB field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to FiB field_where_referenced.'
    );
  }

  /**
   * Anonymous user access to history fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousHistory() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'history';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_when field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_when.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_when.'
    );

    // User field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_user.'
    );

    // field_event_type_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_event_type_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_event_type_name.'
    );

    // field_subject_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_subject_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_subject_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_subject_node.'
    );

    // field_details field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_details', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_details.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_details.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to history field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to history field_notes.'
    );
  }

  /**
   * Student user access to history fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentHistory() {
    // Current student is anonymous.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'history';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_when field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to history field_when.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_when.'
    );

    // User field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to history field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_user.'
    );

    // field_event_type_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to history field_event_type_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_event_type_name.'
    );

    // field_subject_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_subject_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to history field_subject_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_subject_node.'
    );

    // field_details field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_details', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected student view access to history field_details.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_details.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no student view access to history field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to history field_notes.'
    );
  }

  /**
   * Grader user access to history fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderHistory() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'history';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_when field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_when.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_when.'
    );

    // User field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_user.'
    );

    // field_event_type_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_event_type_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_event_type_name.'
    );

    // field_subject_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_subject_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_subject_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_subject_node.'
    );

    // field_details field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_details', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_details.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_details.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no grader view access to history field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to history field_notes.'
    );
  }

  /**
   * Instructor user access to history fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorHistory() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'history';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_when field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_when.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_when.'
    );

    // User field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_user.'
    );

    // field_event_type_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_event_type_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_event_type_name.'
    );

    // field_subject_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_subject_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_subject_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_subject_node.'
    );

    // field_details field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_details', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_details.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_details.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected instructor view access to history field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to history field_notes.'
    );
  }

  /**
   * Author user access to history fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorHistory() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'history';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_when field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_when.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_when.'
    );

    // User field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_user.'
    );

    // field_event_type_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_event_type_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_event_type_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_event_type_name.'
    );

    // field_subject_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_subject_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_subject_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_subject_node.'
    );

    // field_details field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_details', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_details.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_details.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no author view access to history field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to history field_notes.'
    );
  }

  /**
   * Anonymous user access to model fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousModel() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'model';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to model field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to model field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to model body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected  anonymous view access to model field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_attachments.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to model field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to model field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_notes.'
    );
  }

  /**
   * Student user access to model fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentModel() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'model';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to model field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to model field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to model field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to model field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to model body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to model body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to model field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to model field_attachments.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to model field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to model field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to model field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to model field_notes.'
    );
  }

  /**
   * Grader user access to model fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderModel() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'model';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to model field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to model field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to model body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to model field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model field_attachments.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to model field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to model field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to model field_notes.'
    );
  }

  /**
   * Instructor user access to model fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorModel() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'model';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model field_attachments.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to model field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to model field_notes.'
    );
  }

  /**
   * Author user access to model fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorModel() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'model';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to model field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to model field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to model body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to model field_attachments.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to model field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to model field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to model field_notes.'
    );
  }

  /**
   * Anonymous user access to MCQ fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousMcq() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'multiple_choice_question';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to MCQ field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to MCQ field_internal_name.'
    );

    // field_question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to MCQ field_question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to MCQ field_question.'
    );

    // field_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to MCQ field_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to MCQ field_responses.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to MCQ field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to MCQ field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to MCQ field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to MCQ field_notes.'
    );
  }

  /**
   * Student user access to MCQ fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentMcq() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'multiple_choice_question';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to MCQ field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to MCQ field_internal_name.'
    );

    // field_question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to MCQ field_question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to MCQ field_question.'
    );

    // field_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to MCQ field_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to MCQ field_responses.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to MCQ field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to MCQ field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to MCQ field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to MCQ field_notes.'
    );
  }

  /**
   * Grader user access to MCQ fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderMcq() {
    // Current user is grader.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'multiple_choice_question';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to MCQ field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to MCQ field_internal_name.'
    );

    // field_question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to MCQ field_question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to MCQ field_question.'
    );

    // field_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to MCQ field_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to MCQ field_responses.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to MCQ field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to MCQ field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to MCQ field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to MCQ field_notes.'
    );
  }

  /**
   * Instructor user access to MCQ fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorMcq() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'multiple_choice_question';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to MCQ field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to MCQ field_internal_name.'
    );

    // field_question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to MCQ field_question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to MCQ field_question.'
    );

    // field_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to MCQ field_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to MCQ field_responses.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to MCQ field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to MCQ field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to MCQ field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to MCQ field_notes.'
    );
  }

  /**
   * Author user access to MCQ fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorMcq() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'multiple_choice_question';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to MCQ field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to MCQ field_internal_name.'
    );

    // field_question field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_question', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to MCQ field_question.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to MCQ field_question.'
    );

    // field_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to MCQ field_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to MCQ field_responses.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to MCQ field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to MCQ field_where_referenced.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to MCQ field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to MCQ field_notes.'
    );
  }

  /**
   * Anonymous user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousPattern() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to pattern field_where_referenced.'
    );

  }

  /**
   * Student user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentPattern() {
    // Current user is student.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to pattern field_where_referenced.'
    );
  }

  /**
   * Grader user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderPattern() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to pattern field_where_referenced.'
    );
  }

  /**
   * Reviewer user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerPattern() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to pattern field_where_referenced.'
    );
  }

  /**
   * Instructor user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorPattern() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to pattern field_where_referenced.'
    );
  }

  /**
   * Author user access to pattern fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorPattern() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'pattern';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_tags.'
    );

    // field_summary field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_summary', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_summary.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_summary.'
    );

    // field_situation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_situation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_situation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_situation.'
    );

    // field_action field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_action', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_action.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_action.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to pattern field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to pattern field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to pattern field_where_referenced.'
    );
  }

  /**
   * Anonymous user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousPrinciple() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_where_referenced.'
    );
  }

  /**
   * Student user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentPrinciple() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected anonymous view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to principle field_where_referenced.'
    );
  }

  /**
   * Grader user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderPrinciple() {
    // Current user is grader.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected grader view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to principle field_where_referenced.'
    );
  }

  /**
   * Reviewer user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerPrinciple() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected reviewer view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to principle field_where_referenced.'
    );
  }

  /**
   * Instructor user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorPrinciple() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to principle field_where_referenced.'
    );
  }

  /**
   * Author user access to principle fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorPrinciple() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'principle';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to principle field_internal_name.'
    );

    // field_tags field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle field_tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to principle field_tags.'
    );

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to principle body.'
    );

    // field_attachments field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle field_attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to principle field_attachments.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to principle field_notes.'
    );

    // field_where_referenced field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_where_referenced', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to principle field_where_referenced.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to principle field_where_referenced.'
    );
  }

  /**
   * Anonymous user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousReflect() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to reflect field_notes.'
    );
  }

  /**
   * Student user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentReflect() {
    // Current user is student.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to reflect field_notes.'
    );
  }

  /**
   * Grader user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderReflect() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to reflect field_notes.'
    );
  }

  /**
   * Instructor user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorReflect() {
    // Current user is instructor.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to reflect field_notes.'
    );
  }

  /**
   * Reviewer user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerReflect() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to reflect field_notes.'
    );
  }

  /**
   * Author user access to reflection note fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorReflect() {
    // Current user is author.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'reflect_note';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_internal_name field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_internal_name', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to reflect field_internal_name.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to reflect field_internal_name.'
    );

    // field_node field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_node', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to reflect field_node.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to reflect field_node.'
    );

    // Note field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_note', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to reflect field_note.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to reflect field_note.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to reflect field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to reflect field_notes.'
    );
  }

  /**
   * Anonymous user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousRubricItem() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to rubric item field_notes.'
    );
  }

  /**
   * Student user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentRubricItem() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to rubric item field_notes.'
    );
  }

  /**
   * Grader user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderRubricItem() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to rubric item field_notes.'
    );
  }

  /**
   * Reviewer user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerRubricItem() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to rubric item field_notes.'
    );
  }

  /**
   * Instructor user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorRubricItem() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to rubric item field_notes.'
    );
  }

  /**
   * Author user access to rubric item fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorRubricItem() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'rubric_item';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);

    // field_categories field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to rubric item field_categories.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to rubric item field_categories.'
    );

    // field_rubric_item_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to rubric item field_rubric_item_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to rubric item field_rubric_item_responses.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to rubric item field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to rubric item field_notes.'
    );
  }

  /**
   * Anonymous user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousSubmission() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to submission field_notes.'
    );
  }

  /**
   * Student user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentSubmission() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected student view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to submission field_notes.'
    );
  }

  /**
   * Grader user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderSubmission() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to submission field_notes.'
    );
  }

  /**
   * Reviewer user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerSubmission() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to submission field_notes.'
    );
  }

  /**
   * Instructor user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorSubmission() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to submission field_notes.'
    );
  }

  /**
   * Author user access to submission fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorSubmission() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'submission';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_user field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_user', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_user.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_user.'
    );

    // field_exercise field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_exercise', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_exercise.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_exercise.'
    );

    // field_version field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_version', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_version.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_version.'
    );

    // field_solution field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_solution', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_solution.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_solution.'
    );

    // field_submitted_files field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_submitted_files', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_submitted_files.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_submitted_files.'
    );

    // field_difficulty field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_difficulty.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_difficulty.'
    );

    // field_difficulty_reasons field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_difficulty_reasons', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_difficulty_reasons.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_difficulty_reasons.'
    );

    // field_feedback_source field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_source', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_feedback_source.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_feedback_source.'
    );

    // field_when_feedback_given field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_when_feedback_given', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_when_feedback_given.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_when_feedback_given.'
    );

    // field_feedback_message field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_message', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_feedback_message.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_feedback_message.'
    );

    // field_feedback_responses field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_feedback_responses', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_feedback_responses.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_feedback_responses.'
    );

    // field_overall_evaluation field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_overall_evaluation', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_overall_evaluation.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_overall_evaluation.'
    );

    // field_complete field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_complete', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_complete.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_complete.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author view access to submission field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to submission field_notes.'
    );
  }

  /**
   * Anonymous user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousSuggestion() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to suggestion field_notes.'
    );
  }

  /**
   * Student user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessStudentSuggestion() {
    // Current user is student.
    $currentUserMock = $this->makeStudent();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no student edit access to suggestion field_notes.'
    );
  }

  /**
   * Grader user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessGraderSuggestion() {
    // Current user is grader.
    $currentUserMock = $this->makeGrader();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no grader edit access to suggestion field_notes.'
    );
  }

  /**
   * Reviewer user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessReviewerSuggestion() {
    // Current user is reviewer.
    $currentUserMock = $this->makeReviewer();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no reviewer edit access to suggestion field_notes.'
    );
  }

  /**
   * Instructor user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessInstructorSuggestion() {
    // Current user is instructor.
    $currentUserMock = $this->makeInstructor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no instructor edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected instructor edit access to suggestion field_notes.'
    );
  }

  /**
   * Author user access to suggestion fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAuthorSuggestion() {
    // Current user is author.
    $currentUserMock = $this->makeAuthor();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $contentTypeName = 'suggestion';
    $itemList = $this->makeFieldItemListMock('node', $contentTypeName);
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // field_suggestion field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to suggestion field_suggestion.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to suggestion field_suggestion.'
    );

    // field_suggestion_status field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_suggestion_status', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to suggestion field_suggestion_status.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no author edit access to suggestion field_suggestion_status.'
    );

    // Notes field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', $contentTypeName
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author view access to suggestion field_notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $neutral, $actual, 'Expected author edit access to suggestion field_notes.'
    );
  }

  /**
   * Test anonymous user's access to nodes of varioud types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function testGetNodeAccessAnon() {
    // Current user is anonymous.
    $anonUserMock = $this->makeAnonymous();
    $skillingAccessCheckerAnon = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $anonUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $lesson = $this->makeBasicNode(SkillingConstants::LESSON_CONTENT_TYPE);
    $this->assertTrue(
      $skillingAccessCheckerAnon->isNodeAccess($lesson, 'view'),
      'Anon user cannot access a lesson.'
    );
    $calendar = $this->makeBasicNode(SkillingConstants::CALENDAR_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($calendar, 'view'),
      'Anon user can access a calendar.'
    );
    $character = $this->makeBasicNode(SkillingConstants::CHARACTER_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($character, 'view'),
      'Anon user can access a character.'
    );
    $class = $this->makeBasicNode(SkillingConstants::CLASS_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($class, 'view'),
      'Anon user can access a class.'
    );
    $event = $this->makeBasicNode(SkillingConstants::EVENT_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($event, 'view'),
      'Anon user can access an event.'
    );
    $exercise = $this->makeBasicNode(SkillingConstants::EXERCISE_CONTENT_TYPE);
    $this->assertTrue(
      $skillingAccessCheckerAnon->isNodeAccess($exercise, 'view'),
      'Anon user cannot access an exercise.'
    );
    $fib = $this->makeBasicNode(SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($fib, 'view'),
      'Anon user can access a FIB.'
    );
    $history = $this->makeBasicNode(SkillingConstants::HISTORY_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($history, 'view'),
      'Anon user can access a history node.'
    );
    $model = $this->makeBasicNode(SkillingConstants::MODEL_CONTENT_TYPE);
    $this->assertTrue(
      $skillingAccessCheckerAnon->isNodeAccess($model, 'view'),
      'Anon user cannot access a model.'
    );
    $mcq = $this->makeBasicNode(SkillingConstants::MCQ_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($mcq, 'view'),
      'Anon user can access an MCQ.'
    );
    $pattern = $this->makeBasicNode(SkillingConstants::PATTERN_CONTENT_TYPE);
    $this->assertTrue(
      $skillingAccessCheckerAnon->isNodeAccess($pattern, 'view'),
      'Anon user cannot access a pattern.'
    );
    $principle = $this->makeBasicNode(SkillingConstants::PRINCIPLE_CONTENT_TYPE);
    $this->assertTrue(
      $skillingAccessCheckerAnon->isNodeAccess($principle, 'view'),
      'Anon user cannot access a principle.'
    );
    $reflection = $this->makeBasicNode(SkillingConstants::REFLECT_NOTE_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($reflection, 'view'),
      'Anon user can access a reflection.'
    );
    $rubricItem = $this->makeBasicNode(SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($rubricItem, 'view'),
      'Anon user can access a rubric item.'
    );
    $suggestion = $this->makeBasicNode(SkillingConstants::SUGGESTION_CONTENT_TYPE);
    $this->assertFalse(
      $skillingAccessCheckerAnon->isNodeAccess($suggestion, 'view'),
      'Anon user can access a suggestion.'
    );
  }

}
