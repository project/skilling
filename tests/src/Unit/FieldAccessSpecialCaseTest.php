<?php  /** @noinspection PhpUndefinedMethodInspection */

namespace Drupal\Tests\skilling\Unit;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidator;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\skilling\Access\SkillingAccessChecker;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\skilling\Access\SkillingFieldAccessSpecialCase;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities;
use Drupal\Tests\UnitTestCase;
use Drupal\user\Entity\User;
use Drupal\Core\Messenger\Messenger;
use Drupal\skilling\Access\FieldAccessSpecialCaseChecker;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Tests field access special cases.
 *
 * @group skilling
 */
class FieldAccessSpecialCaseTest extends UnitTestCase {

  /**
   * Path validation service.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidatorServiceMock;

  /**
   * Current path service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathServiceMock;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStackServiceMock;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserMock;

  /**
   * Class to make Skilling user objects.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactoryMock;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilitiesMock;

  /**
   * Set up for testing.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function setup() {
    // Make some basic mock services that have no methods.
    // They'll be replaced with more capable mocked services
    // as needed.
    $this->pathValidatorServiceMock =
      $this
        ->getMockBuilder(PathValidator::class)
        ->disableOriginalConstructor()
        ->setMethods(['isValid'])
        ->getMock();
    $this->pathValidatorServiceMock->expects($this->any())->method('isValid')
      ->will($this->returnValue(1));
    $this->currentPathServiceMock =
      $this
        ->getMockBuilder(CurrentPathStack::class)
        ->disableOriginalConstructor()
        ->setMethods(['getPath'])
        ->getMock();
    $this->requestStackServiceMock =
      $this
        ->getMockBuilder(RequestStack::class)
        ->disableOriginalConstructor()
        ->setMethods(['getCurrentRequest'])
        ->getMock();
    $this->currentUserMock =
      $this
        ->getMockBuilder(SkillingCurrentUser::class)
        ->disableOriginalConstructor()
        ->setMethods(['getRoles', 'isAnonymous'])
        ->getMock();
    //    $this->currentClassMock->expects($this->any())->method('getCurrentEnrollments')
    //      ->will($this->returnValue(1));
//    $this->skillingUserFactoryMock =
//      $this
//        ->getMockBuilder(SkillingUserFactory::class)
//        ->disableOriginalConstructor()
//        ->getMock();
    $this->skillingUtilitiesMock =
      $this
        ->getMockBuilder(Utilities::class)
        ->disableOriginalConstructor()
        ->getMock();
//    $this->messengerMock->expects($this->any())
//      ->method('addWarning')
//      ->will($this->returnCallback('returnFirstArg'));
    $stub = $this->fieldAccessSpecialCaseCheckerMock =
      $this
        ->getMockBuilder(FieldAccessSpecialCaseChecker::class)
        ->disableOriginalConstructor()
        ->getMock();
    $stub
      ->method('defineAllowedSpecialCases')
      ->will($this->returnValue(TRUE));
    $stub
      ->method('checkForSpecialCase')
      ->will($this->returnValue(NULL));

    // Make a default special case checker.


    // Make a default Skilling access checker.
//    $this->skillingAccessChecker = new SkillingAccessChecker(
//      $this->entityTypeManagerMock,
//      $this->currentUserMock,
//      $this->currentClassMock,
//      $this->checkUserRelationshipsServiceMock,
//      $this->configFactoryMock,
//      $this->skillingUserFactoryMock,
//      $this->skillingUtilitiesMock,
//      $this->parserMock,
//      $this->messengerMock,
//      $this->fieldAccessSpecialCaseCheckerMock
//    );
  }

  public function checkMakeSpecialCase() {
    $specialCase = new SkillingFieldAccessSpecialCase(
      $this->pathValidatorServiceMock,
      $this->skillingUtilitiesMock
    );
  }



  /**
   * Test access for node that is not a Skilling content type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function testIsNodeRelevant() {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue('evil'));

    $this->assertTrue(
      $this->skillingAccessChecker->isNodeAccess($nodeMock, 'edit'),
      'Expected node to be relevant.'
    );
  }

  /**
   * Make a node.
   *
   * @param string $contentType
   *   The content type.
   *
   * @return mixed
   *   The node.
   */
  protected function makeBasicNode($contentType) {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('node'));
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue($contentType));
    return $nodeMock;
  }


  /**
   * Is a non-Skilling content type irrelevant?
   */
  public function testIsNodeIrrelevant() {
    $nodeMock =
      $this
        ->getMockBuilder(Node::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $nodeMock->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('node'));
    $nodeMock->expects($this->any())->method('bundle')
      ->will($this->returnValue('forum'));
    $this->assertFalse(
      $this->skillingAccessChecker->isRelevantEntity($nodeMock),
      'Expected node to be irrelevant.'
    );
  }

  /**
   * Make a paragraph.
   *
   * @param string $paragraphType
   *   Paragraph type to make.
   *
   * @return mixed
   *   The paragraph.
   */
  protected function makeBasicParagraph($paragraphType) {
    $mockParagraph =
      $this
        ->getMockBuilder(Paragraph::class)
        ->disableOriginalConstructor()
        ->setMethods(['getEntityTypeId', 'bundle'])
        ->getMock();
    $mockParagraph->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue('paragraph'));
    $mockParagraph->expects($this->any())->method('bundle')
      ->will($this->returnValue($paragraphType));
    return $mockParagraph;
  }

  /**
   * Is a calendar event paragraph relevant?
   */
  public function testIsCalendarEventParagraphRelevant() {
    $mockParagraph = $this->makeBasicParagraph('calendar_event');
    $this->assertTrue(
      $this->skillingAccessChecker->isRelevantEntity($mockParagraph),
      'Expected calendar event paragraph to be relevant.'
    );
  }


  /**
   * Mock a current user.
   *
   * @param array $methods
   *   Methods to add to the created object.
   *
   * @return mixed
   *   The user mock.
   */
  protected function makeCurrentUser(array $methods) {
    $mockCurrentUser =
      $this
        ->getMockBuilder(SkillingCurrentUser::class)
        ->disableOriginalConstructor()
        ->setMethods(array_keys($methods))
        ->getMock();
    foreach ($methods as $method => $value) {
      $mockCurrentUser->expects($this->any())->method($method)
        ->will($this->returnValue($value));
    }
    return $mockCurrentUser;
  }

  /**
   * Make an admin current user.
   */
  protected function makeAdmin() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => TRUE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make an author current user.
   */
  protected function makeAuthor() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => TRUE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a reviewer current user.
   */
  protected function makeReviewer() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => TRUE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Mock an instructor current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeInstructor($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => TRUE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a grader current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeGrader($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => TRUE,
        'isStudent' => FALSE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make a student current user.
   *
   * @param bool $inClass
   *   What does isInClass return?
   *
   * @return mixed
   *   The mock user.
   */
  protected function makeStudent($inClass = FALSE) {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => TRUE,
        'isAnonymous' => FALSE,
        'isInClass' => $inClass,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Make an anonymous current user.
   */
  protected function makeAnonymous() {
    $mockCurrentUser = $this->makeCurrentUser(
      [
        'isAdministrator' => FALSE,
        'isAuthor' => FALSE,
        'isReviewer' => FALSE,
        'isInstructor' => FALSE,
        'isGrader' => FALSE,
        'isStudent' => FALSE,
        'isAnonymous' => TRUE,
        'isInClass' => FALSE,
      ]
    );
    return $mockCurrentUser;
  }

  /**
   * Set the setting for whether anons and students can see design pages.
   *
   * @param bool $value
   *   Setting of the flag.
   *
   * @return mixed
   *   The mock.
   */
  protected function makeAnonSeeDesignPageSetting($value) {
    $immutableConfig =
      $this
        ->getMockBuilder(ImmutableConfig::class)
        ->disableOriginalConstructor()
        ->setMethods(['get'])
        ->getMock();
    $immutableConfig->expects($this->any())->method('get')
      ->will($this->returnValue($value));
    $configFactoryMock =
      $this
        ->getMockBuilder(ConfigFactory::class)
        ->disableOriginalConstructor()
        ->setMethods(['get'])
        ->getMock();
    $configFactoryMock->expects($this->any())->method('get')
      ->will($this->returnValue($immutableConfig));
    return $configFactoryMock;
  }

  protected $joins;

  protected $conditions;

  /**
   * Make an AlterableInterface mock.
   *
   * @return mixed
   *   The mock.
   */
  protected function setupSearchAlterableInterface() {
    $searchAlterableInterface =
      $this
        ->getMockBuilder(AlterableInterface::class)
        ->disableOriginalConstructor()
        ->setMethods([
          'join', 'condition', 'addTag', 'hasTag', 'hasAllTags',
          'addMetaData', 'getMetaData', 'hasAnyTag',
        ])
        ->getMock();
    $searchAlterableInterface->expects($this->any())->method('join')
      ->will($this->returnCallback(
        function ($p1, $p2, $p3) {
          $this->joins[] = [$p1, $p2, $p3];
        }
      ));
    $searchAlterableInterface->expects($this->any())->method('condition')
      ->will($this->returnCallback(
        function ($p1, $p2, $p3) {
          $this->conditions[] = [$p1, $p2, $p3];
        }
      ));
    return $searchAlterableInterface;
  }

  /**
   * Test altering search query.
   *
   * This is messy. joins and conditions are the arrays affected by the
   * method being tested. They need to be cleared before each test.
   *
   * The alterable interface mock appends data it gets to these arrays.
   *
   * We need a current user with some roles.
   *
   * We need a config with a flag showing whether students/anons can see
   * design pages.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function testQueryAlter1() {
    // Set up the access checker.
    // Current user is an admin.
    $currentUserMock = $this->makeAdmin();
    // Students/anons can see design pages.
    $configFactoryMock = $this->makeAnonSeeDesignPageSetting(TRUE);
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $this->joins = [];
    $this->conditions = [];
    $searchAlterableInterface = $this->setupSearchAlterableInterface();
    $skillingAccessChecker->alterSearchQuery($searchAlterableInterface);
    $this->assertTrue(
      count($this->joins) > 0,
      'Expected data in joins array.'
    );
    $this->assertTrue(
      count($this->conditions) > 0,
      'Expected data in conditions array.'
    );
  }

  /**
   * Mock a field definition object.
   *
   * @param string $fieldName
   *   Field name.
   * @param string $entityType
   *   Entity type.
   * @param string $bundle
   *   Bundle.
   *
   * @return mixed
   *   Mock object
   */
  protected function makeFieldDefinitionMock($fieldName, $entityType, $bundle) {
    $methods = get_class_methods(FieldDefinitionInterface::class);
    $methods[] = 'get';
    $fieldDefinitionMock =
      $this
        ->getMockBuilder(FieldDefinitionInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $fieldDefinitionMock->expects($this->any())->method('getName')
      ->will($this->returnValue($fieldName));
    $fieldDefinitionMock->expects($this->any())->method('get')
      ->will(
        $this->returnCallback(
          function ($propertyName) use ($entityType, $bundle) {
            if ($propertyName === 'entity_type') {
              return $entityType;
            };
            if ($propertyName === 'bundle') {
              return $bundle;
            };
          }
        )
      );
    return $fieldDefinitionMock;
  }

  /**
   * Make a field item list.
   *
   * @param string $entityTypeId
   *   Entity type.
   * @param string $bundle
   *   Bundle name.
   *
   * @return mixed
   *   Mock object.
   */
  protected function makeFieldItemListMock($entityTypeId, $bundle) {
    $entity = $this->makeEntityMock($entityTypeId, $bundle);
    $methods = get_class_methods(FieldItemListInterface::class);
    $fieldItemList =
      $this
        ->getMockBuilder(FieldItemListInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $fieldItemList->expects($this->any())->method('getEntity')
      ->will($this->returnValue($entity));
    return $fieldItemList;
  }

  /**
   * Make a mock entity.
   *
   * @param string $entityTypeId
   *   Type id, e.g., node.
   * @param string $bundle
   *   Bundle.
   *
   * @return mixed
   *   The entity.
   */
  protected function makeEntityMock($entityTypeId, $bundle) {
    $methods = get_class_methods(EntityInterface::class);
    $entity =
      $this
        ->getMockBuilder(EntityInterface::class)
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    $entity->expects($this->any())->method('getEntityTypeId')
      ->will($this->returnValue($entityTypeId));
    $entity->expects($this->any())->method('bundle')
      ->will($this->returnValue($bundle));
    return $entity;
  }

  /**
   * Anonymous user access to lesson fields.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function testFieldAccessAnonymousLesson() {
    // Current user is anonymous.
    $currentUserMock = $this->makeAnonymous();
    $skillingAccessChecker = new SkillingAccessChecker(
      $this->entityTypeManagerMock,
      $currentUserMock,
      $this->currentClassMock,
      $this->checkUserRelationshipsServiceMock,
      $this->configFactoryMock,
      $this->skillingUserFactoryMock,
      $this->skillingUtilitiesMock,
      $this->parserMock,
      $this->messengerMock,
      $this->fieldAccessSpecialCaseCheckerMock
    );
    $itemList = $this->makeFieldItemListMock('node', 'lesson');
    $neutral = AccessResult::neutral()->setCacheMaxAge(0);
    $denied = AccessResult::forbidden()->setCacheMaxAge(0);

    // Body field.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'body', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson body.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual, 'Expected no anonymous edit access to lesson body.'
    );

    // Attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson attachments.'
    );

    // Hidden attachments.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_hidden_attachments', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson hidden attachments.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson hidden attachments.'
    );

    // Notes.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_notes', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson notes.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson notes.'
    );

    // Order in book.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_order_in_book', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson order in book.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson order in book.'
    );

    // Show ToC.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_show_toc', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous view access to lesson show ToC.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson show ToC.'
    );

    // Tags.
    $fieldDefinition = $this->makeFieldDefinitionMock(
      'field_tags', 'node', 'lesson'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'view', $itemList
    );
    $this->assertEquals(
      $neutral, $actual,
      'Expected anonymous view access to lesson tags.'
    );
    $actual = $skillingAccessChecker->getFieldAccess(
      $fieldDefinition, 'edit', $itemList
    );
    $this->assertEquals(
      $denied, $actual,
      'Expected no anonymous edit access to lesson tags.'
    );
  }

}
