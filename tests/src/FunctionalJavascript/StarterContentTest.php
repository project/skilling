<?php  /** @noinspection PhpUndefinedMethodInspection */

namespace Drupal\Tests\skilling\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Exception;

/**
 * Tests with starter content loaded.
 *
 * @group skilling
 */
class StarterContentTest extends WebDriverTestBase {
  /**
   * Use the standard installation profile.
   *
   * @var string
   */
  protected $profile = 'standard';

  protected $testOutputFilePath;

  const ADMIN_USER_NAME = 'bill';

  const PASSWORD = 'rosierosie';
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node', 'user', 'rest', 'search', 'system', 'toolbar', 'update', 'views',
    'book', 'ckeditor', 'comment', 'options', 'block', 'comment', 'config',
    'block_content', 'filter', 'dblog', 'media', 'menu_ui', 'path',
    'taxonomy', 'views_ui',
    'datetime', 'file', 'image', 'link', 'text',
    'ctools', 'token', 'pathauto',
    'inline_entity_form',
    'entity_reference_revisions', 'paragraphs',
    'checklistapi',
    'date_popup',
    'allowed_formats',
    'imce',
    'skilling_content_types', 'skilling_users', 'skilling',
  ];
//'token_custom',
  /**
   * Fixture user with administrative powers.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Fixture authenticated user with no permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $authUser;

  /** @var \Drupal\node\NodeInterface  */
  protected $node;

  protected $adminRoleId;

  /** @var \Drupal\node\NodeInterface */
  protected $exercise1;
  /** @var \Drupal\node\NodeInterface */
  protected $exercise2;
  /** @var \Drupal\node\NodeInterface */
  protected $submissionStudent1Exercise1;
  /** @var \Drupal\node\NodeInterface */
  protected $submissionStudent1Exercise2;
  /** @var \Drupal\node\NodeInterface */
  protected $submissionStudent2Exercise1;
  /** @var \Drupal\node\NodeInterface */
  protected $submissionStudent2Exercise2;

  protected $lesson2_1;
  protected $lesson2_2;

  protected $submissionStudent3Exercise1;
  protected $submissionStudent3Exercise2;
  protected $submissionStudent4Exercise1;
  protected $submissionStudent4Exercise2;


  protected $student1;
  protected $student2;
  protected $student3;
  protected $student4;

  protected $grader1;
  protected $grader2;

  protected $instructor1;
  protected $instructor2;

  protected $author;
  protected $reviewer;


  /** @var \Drupal\node\NodeInterface  */
  protected $mcq;

  protected $defaultTheme = 'skilling';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Make sure to complete the normal setup steps first.
    parent::setUp();
    $this->testOutputFilePath = \Drupal::root() . '/' . drupal_get_path('module', 'skilling')
      . '/tests/src/FunctionalJavascript/test-output/';
    // Create users.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'access content',
      'view the administration theme',
      'administer permissions',
      'administer nodes',
      'administer content types',
      'administer site configuration',
      'use text format skilling',
      'starter content',
    ], self::ADMIN_USER_NAME, TRUE);
    $this->authUser = $this->drupalCreateUser([
      // For node creation.
      'access content',
      'create page content',
      'edit own page content',
      'access administration pages',
      ]);
  }

  /**
   * Remember some of the created entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function rememberSomeEntities() {
    $screenCount = 0;
    $imageFileName = 'load';

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');
    $this->exercise1 = $this->loadNodeByTitle("Exercise 1");
//    fwrite(STDERR, "exer title: * {$this->exercise1->getTitle()} *");
    $this->mcq = $this->loadNodeByTitle('Logical operator eval order in If 1');
//    fwrite(STDERR, "mcq title: * {$this->mcq->getTitle()} *");

    $this->submissionStudent1Exercise1 = $this->loadNodeByTitle("student1 submitted solution for Exercise 1, attempt 1");
    $this->submissionStudent1Exercise2 = $this->loadNodeByTitle("student1 submitted solution for Exercise 2, attempt 1");
    $this->submissionStudent2Exercise1 = $this->loadNodeByTitle("student2 submitted solution for Exercise 1, attempt 1");
    $this->submissionStudent2Exercise2 = $this->loadNodeByTitle("student2 submitted solution for Exercise 2, attempt 1");
//    fwrite(STDERR, "sub: * {$this->submissionStudent1Exercise1->getTitle()} *");

    $this->submissionStudent3Exercise1
      = $this->loadNodeByTitle("student3 submitted solution for Exercise 1, attempt 1");
    $this->submissionStudent3Exercise2
      = $this->loadNodeByTitle("student3 submitted solution for Exercise 2, attempt 1");
    $this->submissionStudent4Exercise1
      = $this->loadNodeByTitle("student4 submitted solution for Exercise 1, attempt 1");
    $this->submissionStudent4Exercise2
      = $this->loadNodeByTitle("student4 submitted solution for Exercise 2, attempt 1");

    $this->lesson2_1 = $this->loadNodeByTitle("Lesson 2.1");
    $this->lesson2_2 = $this->loadNodeByTitle("Lesson 2.2");
  }


  public function rememberSomeUsers() {
    $this->student1 = $this->loadUserByName('student1');
    $this->student2 = $this->loadUserByName('student2');
    $this->student3 = $this->loadUserByName('student3');
    $this->student4 = $this->loadUserByName('student4');
    $this->instructor1 = $this->loadUserByName('instructor1');
    $this->instructor2 = $this->loadUserByName('instructor2');
    $this->grader1 = $this->loadUserByName('grader1');
    $this->grader2 = $this->loadUserByName('grader2');

    $this->author = $this->loadUserByName('author');
    $this->reviewer = $this->loadUserByName('reviewer');
  }

  /**
   * Load a node by title.
   *
   * @param string $title
   *   Title to find.
   *
   * @return mixed
   *   Node.
   */
  public function loadNodeByTitle($title) {
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');
    $ids = $nodeStorage->getQuery()
      ->condition('title', $title)
      ->execute();
    $id = reset($ids);
    return $nodeStorage->load($id);
  }

  /**
   * Load the user with the given name.
   *
   * @param string $name
   *   The name.
   *
   * @return object
   *   The user.
   */
  public function loadUserByName($name) {
    $user = user_load_by_name($name);
    $this->assertTrue($user, "Could not load user: $name");
    return $user;
  }

  /**
   * Save the HTML of the current page.
   *
   * @param string $filename
   *   Filename to use, e.g., gorn.html.
   */
  public function saveCurrentPageHtml($filename) {
    $html = $this->getSession()->getPage()->getHTML();
    $path = $this->testOutputFilePath . $filename;
    file_put_contents($path, $html);
  }

  /**
   * Save a screenshot of the current page.
   *
   * @param string $filename
   *   Filename to use, e.g., gorn.png.
   */
  public function saveCurrentPageScreenshot($filename) {
    $this->createScreenshot($this->testOutputFilePath . $filename);
  }

  /**
   * Then I fill in wysiwyg on field :locator with :value.
   *
   * From https://gist.github.com/johnennewdeeson/240e2b60c23ea3217887
   *
   * @throws \Exception
   */
  public function iFillInWysiwygOnFieldWith($locator, $value) {
    $el = $this->getSession()->getPage()->findField($locator);

    if (empty($el)) {
      throw new \Exception('Could not find WYSIWYG with locator: ' . $locator, $this->getSession());
    }

    $fieldId = $el->getAttribute('id');

    if (empty($fieldId)) {
      throw new Exception('Could not find an id for field with locator: ' . $locator);
    }

    $this->getSession()
      ->executeScript("CKEDITOR.instances[\"$fieldId\"].setData(\"$value\");");
  }

  /**
   * Run the test.
   *
   * This function calls other small bits.
   *
   * @throws \Exception
   */
  public function testCreateStarterContent() {
    $this->saveCurrentPageScreenshot('start.png');
    $this->drupalLogin($this->adminUser);
    $this->saveCurrentPageScreenshot('admin-logged-in.png');
    $this->makeHomePage();
    $this->loadHome();
    $this->makeAdminAdmin();
    $this->loadPermissionsPage();
    $this->searchForText();
    $this->checkContentTypes();
    $this->createStarterContent();

    $this->rememberSomeEntities();
    $this->rememberSomeUsers();

    $this->checkRoles();
    $this->positionLessonsBlock();
//    $this->checkAdminItemVisibility();
//    $this->checkGraderAdminItemVisibility();
//    $this->checkInstructorAdminItemVisibility();
//    $this->checkAuthorAdminItemVisibility();
//    $this->checkReviewerAdminItemVisibility();
//
//    $this->checkSubmissionAccess();
//    $this->checkUserPageAccess();
//    $this->checkEnrollmentAccess();
//    $this->checkHistoryRecordAccess();

    // Giving up on this one.
//    $this->checkReflectionNoteAccess();

    $this->checkFib();


  }

  /**
   * Make a home page.
   *
   * @throws \Exception
   */
  public function makeHomePage() {
    $this->saveCurrentPageScreenshot('start-make-home-page.png');
    $this->drupalGet('admin/content');
    $this->saveCurrentPageScreenshot('admin-content.png');
    $addContentLink = $this->getSession()->getPage()
      ->findLink('Add content');
    $this->assertNotEmpty($addContentLink, 'Could not find Add content link.');
    // Click the Add content link.
    $this->getSession()->getPage()->find('css', 'ul.action-links li a')->click();
    $this->saveCurrentPageScreenshot('add-content.png');
    $this->assertSession()->pageTextContains('Basic page');
    $this->assertSession()->linkExists('Basic page');
    $this->getSession()->getPage()->clickLink('Basic page');
    $this->saveCurrentPageScreenshot('add-basic-page.png');
    $this->assertSession()->pageTextContains('Create Basic page');
    // Add a page title.
    $field = $this->getSession()->getPage()->findField('title[0][value]');
    $this->assertNotEmpty($field);
    $this->getSession()->getPage()->fillField(
      'title[0][value]', 'Welcome');
    // Add some body text.
    $field = $this->getSession()->getPage()->findField('body[0][value]');
    $this->assertNotEmpty($field);
    $this->iFillInWysiwygOnFieldWith(
      'body[0][value]', 'Hello there!');
    // Save it. URL should be welcome.
    $this->getSession()->getPage()->hasButton('edit-submit');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->saveCurrentPageScreenshot('welcome-saved.png');
    // Set front page.
    $this->drupalGet('admin/config/system/site-information');
    $this->assertSession()->pageTextContains('Basic site settings');
    $this->getSession()->getPage()->hasField('site_frontpage');
    $this->getSession()->getPage()->fillField(
      'site_frontpage', '/welcome');
    $this->getSession()->getPage()->hasButton('edit-submit');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->saveCurrentPageScreenshot('front-page-set.png');
  }

  public function findHistoryRecordForUser($name) {
    $user = user_load_by_name($name);
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');
    $ids = $nodeStorage->getQuery()
      ->condition('type', 'history')
      ->condition('uid', $user->id())
      ->execute();
    $id = reset($ids);
    return $nodeStorage->load($id);

  }

  /**
   * Load the home page.
   */
  public function loadHome() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->saveCurrentPageScreenshot('home.png');
    $this->saveCurrentPageHtml('home.html');
    $this->assertSession()->pageTextContains('Hello there!');
  }

  /**
   * Make sure admin has admin role.
   */
  public function makeAdminAdmin() {
    $this->drupalGet('user');
    $this->saveCurrentPageScreenshot('user.png');
    $this->assertSession()->pageTextContains(self::ADMIN_USER_NAME);
    $this->assertSession()->linkExists('Edit');
    $this->getSession()->getPage()->clickLink('Edit');
    $adminRoleCheckbox = $this->getSession()->getPage()
      ->findById('edit-roles-administrator');
    $this->assertNotEmpty($adminRoleCheckbox);
    $this->saveCurrentPageScreenshot('user-edit1.png');
    $adminRoleCheckbox->check();
    $this->saveCurrentPageScreenshot('user-edit2.png');
    $submitButton = $this->getSession()->getPage()
      ->findById('edit-submit');
    $this->assertNotEmpty($submitButton);
    $submitButton->click();
    $this->saveCurrentPageScreenshot('user-edit3.png');
  }

  /**
   * Load the permissions page.
   */
  public function loadPermissionsPage() {
    $this->drupalGet('admin/people/permissions');
    $this->createScreenshot($this->testOutputFilePath . 'permissions.png');
    $this->assertSession()->pageTextContains('Permissions');
  }

  /**
   * Search page test.
   */
  public function searchForText() {
    $this->drupalGet('search/node');
    $this->assertSession()->pageTextContains('Search');
    $this->getSession()->getPage()->fillField('keys', 'Authentic');
    $this->saveCurrentPageScreenshot('search-start.png');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->saveCurrentPageScreenshot('search-started.png');
    $this->assertSession()->pageTextContains('Search for Authentic');
    $this->saveCurrentPageScreenshot('search-results.png');
  }

  /**
   * Check that content types exist.
   */
  public function checkContentTypes() {
    $this->drupalGet('admin/structure/types');
    $this->saveCurrentPageScreenshot('content-types.png');
    $this->assertSession()->pageTextContains('Content types');
    $this->assertSession()->pageTextContains('Basic page');
    $this->assertSession()->pageTextContains('Lesson');
    $this->assertSession()->pageTextContains('Exercise');
  }

  /**
   * Create the starter content.
   */
  public function createStarterContent() {
    $this->drupalGet('admin/skilling/config/starter-content');
    $this->saveCurrentPageScreenshot('create-starter1.png');
    $this->assertSession()->pageTextContains('Create starter content');
    $this->getSession()->getPage()->fillField('passwords', self::PASSWORD);
    $createLink = $this->getSession()->getPage()
      ->pressButton('Create');
    $this->saveCurrentPageScreenshot('create-starter2.png');
    $this->assertSession()->pageTextContains(
      'Created new user: student1'
    );
    $this->assertSession()->pageTextContains(
      'Created new user: student4'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Class 1'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Pattern 1'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Model 1'
    );
  }

  /**
   * Check that the expected roles exist in the right order.
   */
  public function checkRoles() {
    $this->drupalGet('admin/people/roles');
    $this->saveCurrentPageScreenshot('roles.png');
    $this->assertSession()->pageTextContains('Student');
    $this->assertSession()->pageTextContains('Grader');
    $this->assertSession()->pageTextContains('Instructor');
    $this->assertSession()->pageTextContains('Reviewer');
    $this->assertSession()->pageTextContains('Author');
    $this->assertSession()->pageTextContains('Administrator');
    // Check the order of the roles on the page.
    $expectedRoleNamesInOrder = [
      'Student', 'Grader', 'Instructor', 'Reviewer', 'Author', 'Administrator',
    ];
    $pageContent = $this->getTextContent();
    $position = 0;
    foreach ($expectedRoleNamesInOrder as $roleName) {
      // Look for the role.
      $rolePosition = strpos($pageContent, $roleName, $position);
      $this->assertNotFalse(
        $rolePosition,
        'Role ' . $roleName . ' not found.'
      );
      $this->assertTrue(
        $rolePosition > $position,
        'Role ' . $roleName . ' out of order.'
      );
      $position = $rolePosition;
    }
  }


  public function positionLessonsBlock() {
    // Find the bid of the Course book.
    /** @var \Drupal\book\BookManager $bookManager */
    $bookManager = \Drupal::service('book.manager');
    $allBooks = $bookManager->getAllBooks();
    $courseBid = FALSE;
    foreach ($allBooks as $book) {
      if ($book['title'] === 'Course') {
        $courseBid = $book['bid'];
      }
    }
    if (!$courseBid) {
      throw new \Exception('positionLessonsBlock: Course book not found.');
    }
    $this->drupalGet(
      'admin/structure/block/add/skilling_book_nav_block/skilling_bootstrap'
    );
    $this->saveCurrentPageScreenshot('block-layout1.png');
    $options = [
      'region' => 'sidebar_second',
      'settings[label]' => 'Lessons',
      'settings[book_to_show]' => $courseBid,
      'settings[label_display]' => TRUE,
    ];
    $this->drupalPostForm(NULL, $options, t('Save block'));
    $this->saveCurrentPageScreenshot('block-layout1.png');
    $this->assertSession()->pageTextContains('The block configuration has been saved.');
    $this->getSession()->getPage()->findButton('Save blocks');
    $this->saveCurrentPageScreenshot('block-layout2.png');
  }

  /**
   * Check admin admin item visibility.
   */
  public function checkAdminItemVisibility() {
    $this->assertSession()->pageTextContains('Skilling');
    $this->drupalGet('/admin/skilling');
    $this->saveCurrentPageScreenshot('admin-menu1.png');
    $this->assertSession()->pageTextContains('Content');
    $this->assertSession()->pageTextContains('List design pages');
    $this->assertSession()->pageTextContains('Add design page');
    $this->assertSession()->pageTextContains('List lessons');
    $this->assertSession()->pageTextContains('Add lesson');
    $this->assertSession()->pageTextContains('List patterns');
    $this->assertSession()->pageTextContains('Add pattern');
    $this->assertSession()->pageTextContains('List principles');
    $this->assertSession()->pageTextContains('Add principle');
    $this->assertSession()->pageTextContains('List model');
    $this->assertSession()->pageTextContains('Add model');
    $this->assertSession()->pageTextContains('List characters');
    $this->assertSession()->pageTextContains('Add character');
    $this->assertSession()->pageTextContains('Enrollments');
    $this->assertSession()->pageTextContains('List classes');
    $this->assertSession()->pageTextContains('Add enrollment');

    $this->assertSession()->pageTextContains('FiBs and MCQs');
    $this->assertSession()->pageTextContains('List FIBs');
    $this->assertSession()->pageTextContains('Add FIB');
    $this->assertSession()->pageTextContains('List MCQs');
    $this->assertSession()->pageTextContains('Add MCQ');

    $this->assertSession()->pageTextContains('History');
    $this->assertSession()->pageTextContains('List events');
    $this->assertSession()->pageTextContains('Delete history');

    $this->assertSession()->pageTextContains('Checklists');
    $this->assertSession()->pageTextContains('Post-installation checklist');
    $this->assertSession()->pageTextContains('New class checklist');

    $this->assertSession()->pageTextContains('Classes and events');
    $this->assertSession()->pageTextContains('List classes');
    $this->assertSession()->pageTextContains('Add class');
    $this->assertSession()->pageTextContains('List calendars');
    $this->assertSession()->pageTextContains('Add calendar');
    $this->assertSession()->pageTextContains('List events');
    $this->assertSession()->pageTextContains('Add event');

    $this->assertSession()->pageTextContains('Exercises and rubrics items');
    $this->assertSession()->pageTextContains('List exercises');
    $this->assertSession()->pageTextContains('Add exercise');
    $this->assertSession()->pageTextContains('List rubric items');
    $this->assertSession()->pageTextContains('Add rubric item');

    $this->assertSession()->pageTextContains('Submissions');
    $this->assertSession()->pageTextContains('List submissions');
    $this->assertSession()->pageTextContains('Start grading');

    $this->assertSession()->pageTextContains('Attachments');
    $this->assertSession()->pageTextContains('List attachments');
    $this->assertSession()->pageTextContains('List hidden attachments');

    $this->assertSession()->pageTextContains('Configuration');
    $this->assertSession()->pageTextContains('Student profile settings');
    $this->assertSession()->pageTextContains('Content management settings');
    $this->assertSession()->pageTextContains('Exercise submission settings');
    $this->assertSession()->pageTextContains('Starter content');
  }

  /**
   * Check grader admin item visibility.
   */
  public function checkGraderAdminItemVisibility() {
    $this->loginUserByName('grader1');
    $this->drupalGet('/admin/skilling');
    $this->saveCurrentPageScreenshot('grader-menu1.png');
    $this->assertSession()->pageTextContains('Start grading');
  }

  /**
   * Check instructor admin item visibility.
   */
  public function checkInstructorAdminItemVisibility() {
    $this->loginUserByName('instructor1');
    $this->drupalGet('/admin/skilling');
    $this->saveCurrentPageScreenshot('instructor-menu1.png');
    $this->assertSession()->pageTextContains('Content');
    $this->assertSession()->pageTextContains('List design pages');
    $this->assertSession()->pageTextContains('List lessons');

    $this->assertSession()->pageTextContains('Enrollments');
    $this->assertSession()->pageTextContains('List classes');
    $this->assertSession()->pageTextContains('Add enrollment');

    $this->assertSession()->pageTextContains('FiBs and MCQs');
    $this->assertSession()->pageTextContains('List FIBs');
    $this->assertSession()->pageTextContains('List MCQs');

    $this->assertSession()->pageTextContains('Classes and events');
    $this->assertSession()->pageTextContains('List classes');
    $this->assertSession()->pageTextContains('List calendars');
    $this->assertSession()->pageTextContains('Add calendar');
    $this->assertSession()->pageTextContains('List events');
    $this->assertSession()->pageTextContains('Add event');

    $this->assertSession()->pageTextContains('Exercises and rubrics items');
    $this->assertSession()->pageTextContains('List exercises');
    $this->assertSession()->pageTextContains('List rubric items');

    $this->assertSession()->pageTextContains('Submissions');
    $this->assertSession()->pageTextContains('List submissions');

  }

  /**
   * Check author admin item visibility.
   */
  public function checkAuthorAdminItemVisibility() {
    $this->loginUserByName('author');
    $this->drupalGet('/admin/skilling');
    $this->saveCurrentPageScreenshot('author-menu1.png');
    $this->assertSession()->pageTextContains('Content');
    $this->assertSession()->pageTextContains('List design pages');
    $this->assertSession()->pageTextContains('Add design page');
    $this->assertSession()->pageTextContains('List lessons');
    $this->assertSession()->pageTextContains('Add lesson');
    $this->assertSession()->pageTextContains('List patterns');
    $this->assertSession()->pageTextContains('Add pattern');
    $this->assertSession()->pageTextContains('List principles');
    $this->assertSession()->pageTextContains('Add principle');
    $this->assertSession()->pageTextContains('List model');
    $this->assertSession()->pageTextContains('Add model');
    $this->assertSession()->pageTextContains('List characters');
    $this->assertSession()->pageTextContains('Add character');
    $this->saveCurrentPageScreenshot('author-menu2.png');
    $this->getSession()->getPage()->findById('drupal-live-announce');
    $this->saveCurrentPageScreenshot('author-menu3.png');
    $this->assertSession()->pageTextContains('Exercises and rubrics items');
    $this->assertSession()->pageTextContains('List exercises');
    $this->assertSession()->pageTextContains('Add exercise');
    $this->assertSession()->pageTextContains('List rubric items');
    $this->assertSession()->pageTextContains('Add rubric item');
    $addContentLink = $this->getSession()->getPage()->findLink('Add rubric item');
    $this->saveCurrentPageScreenshot('author-menu4.png');

    $this->assertSession()->pageTextContains('Attachments');
    $this->assertSession()->pageTextContains('List attachments');
    $this->assertSession()->pageTextContains('List hidden attachments');

    $this->assertSession()->pageTextContains('Classes and events');
    $this->assertSession()->pageTextContains('List calendars');
    $this->assertSession()->pageTextContains('Add calendar');
    $this->assertSession()->pageTextContains('List events');
    $this->assertSession()->pageTextContains('Add event');

    $this->assertSession()->pageTextContains('FiBs and MCQs');
    $this->assertSession()->pageTextContains('List FIBs');
    $this->assertSession()->pageTextContains('Add FIB');
    $this->assertSession()->pageTextContains('List MCQs');
    $this->assertSession()->pageTextContains('Add MCQ');
  }

  /**
   * Check reviewer admin item visibility.
   */
  public function checkReviewerAdminItemVisibility() {
    $this->loginUserByName('reviewer');
    $this->drupalGet('/admin/skilling');
    $this->saveCurrentPageScreenshot('reviewer-menu1.png');
    $this->assertSession()->pageTextContains('Content');
    $this->assertSession()->pageTextContains('List design pages');
    $this->assertSession()->pageTextContains('List lessons');
    $this->assertSession()->pageTextContains('List patterns');
    $this->assertSession()->pageTextContains('List principles');
    $this->assertSession()->pageTextContains('List model');
    $this->assertSession()->pageTextContains('List characters');

    $this->assertSession()->pageTextContains('Exercises and rubrics items');
    $this->assertSession()->pageTextContains('List exercises');
    $this->assertSession()->pageTextContains('List rubric items');

    $this->assertSession()->pageTextContains('Attachments');
    $this->assertSession()->pageTextContains('List attachments');

    $this->assertSession()->pageTextContains('Classes and events');
    $this->assertSession()->pageTextContains('List calendars');
    $this->assertSession()->pageTextContains('List events');

    $this->assertSession()->pageTextContains('FiBs and MCQs');
    $this->assertSession()->pageTextContains('List FIBs');
    $this->assertSession()->pageTextContains('List MCQs');
  }

  /**
   * Check user's access to submissions.
   */
  public function checkSubmissionAccess() {
    $screenCount = 0;
//    $this->saveCurrentPageScreenshot('submissions0.png');
//    $this->drupalGet('user/logout');
//    $this->saveCurrentPageScreenshot('submissions0-2.png');
//    $this->drupalGet('user/login');
//    $this->saveCurrentPageScreenshot('submissions0-3.png');
//    $this->loginUserByName($this->adminUser->getAccountName(), $this->adminUser->passRaw);
//    $this->saveCurrentPageScreenshot('submissions0-4.png');
//    $this->drupalGet('/user');
//    $this->saveCurrentPageScreenshot('submissions0-6.png');
//
//    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
//    $this->saveCurrentPageScreenshot('submissions0-10.png');
    $this->loginUserByName('student1');
//    drupal_flush_all_caches();
//    $this->saveCurrentPageScreenshot('submissions0-15.png');
//    $this->drupalGet('/user');
//    $this->saveCurrentPageScreenshot('submissions1-5.png');
    $this->drupalGet('/skilling/submissions');
//    $this->saveCurrentPageScreenshot('submissions1.png');
    $this->assertSession()->pageTextContains('Exercise 1');
    // Access should be OK.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions2.png');
    $this->assertSession()->pageTextContains('student1');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions3.png');
    $this->assertSession()->pageTextContains('student1');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions4.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions5.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('student2');
    // Access should be OK.
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions6.png');
    $this->assertSession()->pageTextContains('student2');
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions7.png');
    $this->assertSession()->pageTextContains('student2');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions8.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions9.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('instructor1');
    // Access should be OK.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions10.png');
    $this->assertSession()->pageTextContains('student1');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions11.png');
    $this->assertSession()->pageTextContains('student1');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent3Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions12.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent3Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions13.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('instructor2');
    // Access should be OK.
    $this->drupalGet('/node/' . $this->submissionStudent3Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions14.png');
    $this->assertSession()->pageTextContains('student3');
    $this->drupalGet('/node/' . $this->submissionStudent3Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions15.png');
    $this->assertSession()->pageTextContains('student3');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions16.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions17.png');
    $this->assertSession()->pageTextContains('Access denied');

    $screenCount = 18;
    $this->loginUserByName('grader1');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('author');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('reviewer');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent1Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    // Access should be denied.
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise1->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/node/' . $this->submissionStudent2Exercise2->id());
    $this->saveCurrentPageScreenshot('submissions' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

  }

  public function checkUserPageAccess() {
    $screenCount = 0;
    $this->loginUserByName('student1');
    // Should be able to access own user page.
    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student1');
    // Should not be able to access other student's pages.
    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    // Other user pages.
    $this->drupalGet('/user/' . $this->grader1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->grader2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->instructor1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->instructor2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->author->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->reviewer->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check instructor access to students in/not in their classes.
    $this->loginUserByName('instructor1');
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');

    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student1');

    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student2');

    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('instructor2');
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');

    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student3');

    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student4');

    // Check grader access to students.
    $this->loginUserByName('grader1');
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');

    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check author access to students.
    $this->loginUserByName('author');
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');

    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check reviewer access to students.
    $this->loginUserByName('reviewer');
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');

    $this->drupalGet('/user/' . $this->student1->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student2->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student3->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->drupalGet('/user/' . $this->student4->id());
    $this->saveCurrentPageScreenshot('user-page' . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
  }

  /**
   * Log in the user with the given name.
   *
   * @param string $name
   *   User name.
   */
  public function loginUserByName($name, $password = '') {
    $this->saveCurrentPageScreenshot('logout1.png');
    $this->drupalGet('user/logout');
    $this->saveCurrentPageScreenshot('logout2.png');
    $this->drupalGet('user/login');
    $this->saveCurrentPageScreenshot('logout3.png');
    $this->getSession()->getPage()->fillField('name', $name);
    if ($password === '') {
      $password = self::PASSWORD;
    }
    $this->getSession()->getPage()->fillField('pass', $password);
    $this->saveCurrentPageScreenshot('logout4.png');
    $this->getSession()->getPage()->pressButton('Log in');
    $this->saveCurrentPageScreenshot('logout5.png');
  }


  public function checkEnrollmentAccess() {
    $screenCount = 0;
    $imageFileName = 'enroll';

    // Student 1's access to enrollment records.
    $this->loginUserByName('student1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    $enroll = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student1');

    $enroll = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student4 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Instructor 1's access to enrollment records.
    $this->loginUserByName('instructor1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    $enroll = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student1');

    $enroll = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('student2');

    $enroll = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student4 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Grader 1's access to enrollment records.
    $this->loginUserByName('grader1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    $enroll = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student4 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Author's access to enrollment records.
    $this->loginUserByName('author');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    $enroll = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student4 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Reviewer's access to enrollment records.
    $this->loginUserByName('reviewer');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    $enroll = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $enroll = $this->loadNodeByTitle('User student4 enrolled in Class 2');
    $this->drupalGet('node/' . $enroll->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount . '.png');
    $this->assertSession()->pageTextContains('Access denied');
  }

  public function checkHistoryRecordAccess() {
    $screenCount = 0;
    $imageFileName = 'history';

    // Make sure that there are history records for everyone.
    $this->loginUserByName('student1');
    $this->loginUserByName('student3');

    $historyRecordStudent1 = $this->findHistoryRecordForUser('student1');
    $historyRecordStudent3 = $this->findHistoryRecordForUser('student3');

    // Check student access to history records.
    $this->loginUserByName('student1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $historyRecordStudent1->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Event type name');
    $this->drupalGet('node/' . $historyRecordStudent3->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check instructor access to history records.
    $this->loginUserByName('instructor1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $historyRecordStudent1->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Event type name');
    $this->drupalGet('node/' . $historyRecordStudent3->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check grader access to history records.
    $this->loginUserByName('grader1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $historyRecordStudent1->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('node/' . $historyRecordStudent3->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check author access to history records.
    $this->loginUserByName('author');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $historyRecordStudent1->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('node/' . $historyRecordStudent3->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    // Check reviewer access to history records.
    $this->loginUserByName('reviewer');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $historyRecordStudent1->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');
    $this->drupalGet('node/' . $historyRecordStudent3->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

  }


  public function checkReflectionNoteAccess() {
    $screenCount = 0;
    $imageFileName = 'reflect';
    // Make a reflection.
    $this->loginUserByName('student4');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $this->lesson2_2->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $locator = '.skilling-reflect-container textarea';
    $el = $this->getSession()->getPage()->find('css', $locator);
    if (empty($el)) {
      throw new \Exception('Could not find element: ' . $locator, $this->getSession());
    }
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

$script = "
(function($) {
  $('.skilling-reflect-container textarea').val('peep');
  var e = $.Event('keyup');
  e.which = 65; // a
  $('.skilling-reflect-container textarea').trigger(e);
}(jQuery));
";

    $this->getSession()->executeScript($script);

//    $el->setValue('Deep thoughts');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
//    $btn = $this->getSession()->getPage()->findButton('Continue');
//    $this->assertNotEmpty($btn);
//    $btn->click();
//    // There is an exercise after the reflection, do wait until it is visible.
//    $this->assertSession()->waitForElementVisible('css', 'button.skilling-add-submission-link');
//    $this->saveCurrentPageScreenshot($imageFileName . $screenCunt++ . '.png');
    $milliseconds = round(microtime(true) * 1000);
    fwrite(STDERR, "before wait: " . $milliseconds . "\n");
    $this->assertSession()->assertWaitOnAjaxRequest(20000);
    $milliseconds = round(microtime(true) * 1000);
    fwrite(STDERR, "after wait: " . $milliseconds . "\n");
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $this->lesson2_2->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');

    // Find the reflection entity.
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');
    $ids = $nodeStorage->getQuery()
      ->condition('type', 'reflect_note')
//      ->condition('uid', $this->student4->id())
      ->execute();
    fwrite(STDERR, "reflect ids: * " . print_r($ids, TRUE) . " *\n");
    fwrite(STDERR, "count: " . count(ids) . "*\n");

//    xdebug_break();


    if (count($ids) === 0) {
      throw new \Exception('Could not find reflection');
    }
    $id = reset($ids);
    if (empty($id)) {
      throw new \Exception('Could not find reflection');
    }
    $note = $nodeStorage->load($id);
    if (empty($note)) {
      throw new \Exception('Could not find reflection');
    }

    // Access it.
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Reflect note');

    // Other users try to get it.
    $this->loginUserByName('student1');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('student3');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('instructor1');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('instructor2');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Reflect note');

    $this->loginUserByName('author');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('grader1');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('grader2');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('author');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

    $this->loginUserByName('reviewer');
    $this->drupalGet('node/' . $note->id());
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->assertSession()->pageTextContains('Access denied');

  }


  /**
   * Check FiB.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function checkFib() {
    // Set up screenshot file name components.
    $screenCount = 0;
    $imageFileName = 'fib';
    // Load a lesson with a FiB.
    $this->loginUserByName('student1');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    $this->drupalGet('node/' . $this->lesson2_1->id());
//    $this->assertSession()->waitForElementVisible('css', '.test-wait', 100000000000000000000000000);
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    /** @var \Behat\Mink\Session  $session */
    $session = $this->getSession();
    $page = $session->getPage();
    // Find the place to put the answer.
    $textbox = $page->find('css', 'div#fib_dogs input.skilling-insert-fib-response');
    $this->assertNotEmpty($textbox, 'FiB answer field not found');
    // Find the Check button.
    $checkButton = $page->findButton('Check');
    $this->assertNotEmpty($checkButton, 'FiB Check button not found');
    // Type in a correct answer.
    $textbox->setValue('Rosie');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    // Press the check button.
    $checkButton->click();
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    // Wait for a response.
    $this->assertSession()->waitForElementVisible('css', '.skilling-insert-fib .skilling-insert-fib-response-container .skilling-fib-response-message');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');
    // What is the response?
    $this->assertSession()->pageTextContains('cute');
    $this->saveCurrentPageScreenshot($imageFileName . $screenCount++ . '.png');


  }


}
