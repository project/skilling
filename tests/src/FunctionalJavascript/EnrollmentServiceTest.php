<?php  /** @noinspection PhpUndefinedMethodInspection */

namespace Drupal\Tests\skilling\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Exception;

/**
 * Tests with starter content loaded.
 *
 * @group skilling
 */
class EnrollmentServiceTest extends WebDriverTestBase {
  /**
   * Use the standard installation profile.
   *
   * @var string
   */
  protected $profile = 'standard';

  protected $testOutputFilePath;

  const ADMIN_USER_NAME = 'bill';

  const PASSWORD = 'rosierosie';
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node', 'user', 'rest', 'search', 'system', 'toolbar', 'update', 'views',
    'book', 'ckeditor', 'comment', 'options', 'block', 'comment', 'config',
    'block_content', 'filter', 'dblog', 'media', 'menu_ui', 'path',
    'taxonomy', 'views_ui',
    'datetime', 'file', 'image', 'link', 'text',
    'ctools', 'token', 'pathauto',
    'inline_entity_form',
    'entity_reference_revisions', 'paragraphs',
    'checklistapi',
    'date_popup',
    'allowed_formats',
    'entity_reference_integrity',
    'imce',
    'skilling_content_types', 'skilling_users', 'skilling',
  ];
//'token_custom',
  /**
   * Fixture user with administrative powers.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Fixture authenticated user with no permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $authUser;

  /** @var \Drupal\node\NodeInterface  */
  protected $node;

  protected $adminRoleId;

  protected $class1;
  protected $class2;

  protected $student1;
  protected $student2;
  protected $student3;
  protected $student4;

  protected $enrollS1C1;
  protected $enrollS2C1;
  protected $enrollS3C2;
  protected $enrollS4C2;

  /** @var \Drupal\node\NodeInterface  */
  protected $mcq;

  protected $defaultTheme = 'skilling';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Make sure to complete the normal setup steps first.
    parent::setUp();
    $this->testOutputFilePath = \Drupal::root() . '/' . drupal_get_path('module', 'skilling')
      . '/tests/src/FunctionalJavascript/test-output/';
    // Create users.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'access content',
      'view the administration theme',
      'administer permissions',
      'administer nodes',
      'administer content types',
      'administer site configuration',
      'use text format skilling',
      'starter content',
    ], self::ADMIN_USER_NAME, TRUE);
    $this->authUser = $this->drupalCreateUser([
      // For node creation.
      'access content',
      'create page content',
      'edit own page content',
      'access administration pages',
      ]);
  }

  /**
   * Remember some of the created entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function rememberSomeEntities() {
    $screenCount = 0;
    $imageFileName = 'load';

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');

    $this->class1 = $this->loadNodeByTitle("Class 1");
    $this->class2 = $this->loadNodeByTitle("Class 2");
    $this->enrollS1C1 = $this->loadNodeByTitle('User student1 enrolled in Class 1');
    $this->enrollS2C1 = $this->loadNodeByTitle('User student2 enrolled in Class 1');
    $this->enrollS3C2 = $this->loadNodeByTitle('User student3 enrolled in Class 2');
    $this->enrollS4C2 = $this->loadNodeByTitle('User student4 enrolled in Class 2');

  }


  public function rememberSomeUsers() {
    $this->student1 = $this->loadUserByName('student1');
    $this->student2 = $this->loadUserByName('student2');
    $this->student3 = $this->loadUserByName('student3');
    $this->student4 = $this->loadUserByName('student4');
  }

  /**
   * Load a node by title.
   *
   * @param string $title
   *   Title to find.
   *
   * @return mixed
   *   Node.
   */
  public function loadNodeByTitle($title) {
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $nodeStorage = $entityTypeManager->getStorage('node');
    $ids = $nodeStorage->getQuery()
      ->condition('title', $title)
      ->execute();
    $id = reset($ids);
    return $nodeStorage->load($id);
  }

  /**
   * Load the user with the given name.
   *
   * @param string $name
   *   The name.
   *
   * @return object
   *   The user.
   */
  public function loadUserByName($name) {
    $user = user_load_by_name($name);
    $this->assertTrue($user, "Could not load user: $name");
    return $user;
  }

  /**
   * Save the HTML of the current page.
   *
   * @param string $filename
   *   Filename to use, e.g., gorn.html.
   */
  public function saveCurrentPageHtml($filename) {
    $html = $this->getSession()->getPage()->getHTML();
    $path = $this->testOutputFilePath . $filename;
    file_put_contents($path, $html);
  }

  /**
   * Save a screenshot of the current page.
   *
   * @param string $filename
   *   Filename to use, e.g., gorn.png.
   */
  public function saveCurrentPageScreenshot($filename) {
    $this->createScreenshot($this->testOutputFilePath . $filename);
  }

  /**
   * Run the test.
   *
   * This function calls other small bits.
   *
   * @throws \Exception
   */
  public function testEnrollmentService() {
    $this->saveCurrentPageScreenshot('start.png');
    $this->drupalLogin($this->adminUser);
    $this->saveCurrentPageScreenshot('admin-logged-in.png');
    $this->createStarterContent();
    $this->rememberSomeEntities();
    $this->rememberSomeUsers();
    $this->checkGetEnrollmentIdsForClassId();
  }

  /**
   * Create the starter content.
   */
  public function createStarterContent() {
    $this->drupalGet('admin/skilling/config/starter-content');
    $this->saveCurrentPageScreenshot('create-starter1.png');
    $this->assertSession()->pageTextContains('Create starter content');
    $this->getSession()->getPage()->fillField('passwords', self::PASSWORD);
    $createLink = $this->getSession()->getPage()
      ->pressButton('Create');
    $this->saveCurrentPageScreenshot('create-starter2.png');
    $this->assertSession()->pageTextContains(
      'Created new user: student1'
    );
    $this->assertSession()->pageTextContains(
      'Created new user: student4'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Class 1'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Pattern 1'
    );
    $this->assertSession()->pageTextContains(
      'Created new node: Model 1'
    );
  }

  /**
   * Check enrollment service method getEnrollmentIdsForClassId.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function checkGetEnrollmentIdsForClassId() {

    /** @var \Drupal\skilling\Enrollment $enrollmentService */
    $enrollmentService = \Drupal::service('skilling.enrollment');

    // Class 1.
    $enrollmentIds = $enrollmentService->getStudentEnrollmentIdsForClassId($this->class1->id());
    $numEnrollments = count($enrollmentIds);
    $this->assertTrue($numEnrollments === 2, 'Found ' . $numEnrollments . ' enrollments');
    $this->assertTrue(
      in_array($this->enrollS1C1->id(), $enrollmentIds),
      'Not found enrollment for student 1 in class 1'
    );
    $this->assertTrue(
      in_array($this->enrollS2C1->id(), $enrollmentIds),
      'Not found enrollment for student 2 in class 1'
    );
    $this->assertFalse(
      in_array($this->enrollS3C2->id(), $enrollmentIds),
      'Found enrollment for student 3 in class 1'
    );
    // Class 2.
    $enrollmentIds = $enrollmentService->getStudentEnrollmentIdsForClassId($this->class2->id());
    $numEnrollments = count($enrollmentIds);
    $this->assertTrue($numEnrollments === 2, 'Found ' . $numEnrollments . ' enrollments');
    $this->assertTrue(
      in_array($this->enrollS3C2->id(), $enrollmentIds),
      'Not found enrollment for student 3 in class 2'
    );
    $this->assertTrue(
      in_array($this->enrollS4C2->id(), $enrollmentIds),
      'Not found enrollment for student 4 in class 2'
    );
    $this->assertFalse(
      in_array($this->enrollS1C1->id(), $enrollmentIds),
      'Found enrollment for student 1 in class 2'
    );
  }

}
