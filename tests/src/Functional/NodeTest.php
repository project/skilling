<?php

namespace Drupal\Tests\skilling\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality of My Module.
 *
 * @group skilling
 */
class NodeTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'user'];

  /**
   * Fixture user with administrative powers.
   *
   * @var \Drupal\user\Entity\UserInterface
   */
  protected $adminUser;

  /**
   * Fixture authenticated user with no permissions.
   *
   * @var \Drupal\user\Entity\UserInterface
   */
  protected $authUser;

  /** @var \Drupal\node\NodeInterface  */
  protected $node;

  protected $defaultTheme = 'skilling';
  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Create Basic page node type.
    if ($this->profile != 'standard') {
      $this
        ->drupalCreateContentType([
          'type' => 'page',
          'name' => 'Basic page',
        ]);
    }
    // Create users.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'access content',
      'view the administration theme',
      'administer permissions',
      'administer nodes',
      'administer content types',
    ]);
    $this->authUser = $this->drupalCreateUser([
        // For node creation.
        'access content',
        'create page content',
        'edit own page content',
        'access administration pages',
      ]);
    // Set the front page to "node".
//    \Drupal::configFactory()
//      ->getEditable('system.site')
//      ->set('page.front', '/node')
//      ->save(TRUE);
  }

  public function testSilly() {
    $this->assertEquals('dog', 'dog');
  }

  public function testCreatePage() {
    // Ensure we have a node page to access.
    $this->node = $this
      ->drupalCreateNode([
        'title' => 'Hello, world!',
        'uid' => $this->authUser->id(),
        'type' => 'page',
      ]);
    $nid = $this->node->id();
    /** @var \Drupal\node\NodeInterface  $node */
    $node = $this->container->get('entity_type.manager')->getStorage('node')
      ->load($nid);
    $this->assertEquals($node->getTitle(), 'Hello, world!', 'Hello world load fail');
  }

  public function testOpenEditForm() {
    $this->node = $this
      ->drupalCreateNode([
        'title' => 'Hello, world!',
        'uid' => $this->authUser->id(),
        'type' => 'page',
      ]);
    $nid = $this->node->id();
    $this->drupalLogin($this->authUser);
    $result = $this->drupalGet('node/' . $nid . '/edit');
    $session = $this->assertSession();
    $session->statusCodeEquals(200, 'Could not open edit form');

  }

  public function testFrontPage(){
//    $result = $this->drupalGet('node');
//    $session = $this->assertSession();
//    $session->statusCodeEquals(200, 'No node');
    $result = $this->drupalGet('<front>');
    $session = $this->assertSession();
    $session->statusCodeEquals(200, 'No user login');
  }

  public function testContentAdminPageAccess(){
    $this->drupalLogin($this->adminUser);
    $result = $this->drupalGet('admin/content');
    $session = $this->assertSession();
    $session->statusCodeEquals(200, 'No admin content');
  }

  public function testCreatedUser() {
    $this->drupalLogin($this->adminUser);
    $this->assertTrue($this->drupalUserIsLoggedIn($this->adminUser));
//    $this->assertSession()->statusCodeEquals(200);
  }

  public function xtestCreatePage(){
    // Create a page.
    // Create an article content type that we will use for testing.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'page',
        'name' => 'Page',
      ]);
    $type->save();
    $this->container->get('router.builder')->rebuild();
    $content = $this->getTextContent();
    $this->assertTrue(stripos($content, 'Page') !== FALSE);
  }

  public function xtestLoadPage() {
    $nids = $this->container->get('entity_type.manager')->getStorage('node')
      ->getQuery()
      ->condition('title', 'Page')
      ->execute();
    $nid = reset($nids);
    $node = $this->container->get('entity_type.manager')->getStorage('node')
      ->load($nid);
    $this->assertSession()->pageTextContains('Page');
  }

}
