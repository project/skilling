<?php

namespace Drupal\Tests\skilling\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group skilling
 */
class LoadTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'user', 'block', 'book', 'field',
    'field_ui', 'filter', 'menu_ui', 'path', 'system', 'search',
    //'link', 'paragraphs',
    //'views',
    ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  protected $defaultTheme = 'skilling';

  protected $profile = 'standard';


  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();


    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'access content',
      'view the administration theme',
      'administer permissions',
      'administer nodes',
      'administer content types',
      'administer site configuration',
    ], 'bill', TRUE);


    //    $messenger = \Drupal::service('messenger');
//    \Drupal::service('module_installer')->install(['user']);

    // Adjust order of fields on entity display.
    /* @var \Drupal\Core\Entity\Entity\EntityViewDisplay $entityViewDisplay */
//    $entityViewDisplay = \Drupal::entityTypeManager()
//      ->getStorage('entity_view_display')
//      ->load('user.user.default');
//
//    $this->user = $this->drupalCreateUser(['administer site configuration']);
//    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   *
   * @group skilling
   */
  public function testLoadHome() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/people/permissions');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Permissions');
    $this->drupalGet('search/node');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Search');
    $this->getSession()->getPage()->fillField('keys', 'Authentic');
    $this->getSession()->getPage()->pressButton('edit-submit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Search for Authentic');
    $this->drupalGet('admin/structure/types');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Content types');
    $this->assertSession()->pageTextContains('Basic page');


  }

}
