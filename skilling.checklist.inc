<?php

/**
 * @file
 * Define checklists that use the checklist API.
 */

use Drupal\Core\Url;
use Drupal\skilling\Help\HelpConstants;

/**
 * Implements hook_checklistapi_checklist_info().
 *
 * Defines a checklist based on
 * @link http://buytaert.net/drupal-learning-curve Dries Buytaert's Drupal learning curve @endlink
 * .
 */
function skilling_checklistapi_checklist_info() {
  $definitions = [];
  $definitions['post_install_checklist'] = [
    '#title' => t('Post-installation checklist'),
    '#path' => '/admin/skilling/checklist/post-installation',
    '#callback' => 'skilling_checklist_post_installation_items',
    '#description' => t('Things to do after installing Skilling.'),
    '#help' => t('<p>Tasks for an administrator to complete after installing Skilling. See the Skilling documentation for deets.</p>'),
  ];
  $definitions['new_class_checklist'] = [
    '#title' => t('New class checklist'),
    '#path' => '/admin/skilling/checklist/new-class',
    '#callback' => 'skilling_checklist_new_class_items',
    '#description' => t('Creating a new class.'),
    '#help' => t('<p>Tasks to complete when creating a new class. See the Skilling documentation for deets.</p>'),
  ];
  return $definitions;
}

/**
 * Define a post-installation checklist.
 *
 * @return array
 *   Checklist definition.
 */
function skilling_checklist_post_installation_items() {
  /** @var \Drupal\skilling\Help\Help $helpService */
  $helpService = \Drupal::service('skilling.help');
  $result = [
    'security' => [
      '#title' => t('Security'),
      '#description' => t('<p>Make your Skilling website more secure.</p>'),
      'private_file_system' => [
        '#title' => t('Setup a private file system'),
        '#description' => t('Skilling needs a private file system, for student submissions, and other things.'),
        'handbook_page' => [
          '#text' => t('Installation Guide'),
          '#url' => Url::fromUri('http://drupal.org/documentation/install'),
        ],
      ],
      'https' => [
        '#title' => t('HTTPS'),
        '#description' => t('Your site should use HTTPS.'),
        'handbook_page' => [
          '#text' => t('Manage nodes'),
          '#url' => Url::fromUri('http://drupal.org/node/306808'),
        ],
      ],
      'block_system' => [
        '#title' => t('Trusted host'),
        '#description' => t('Add a trusted host setting to your configuration.'),
        'handbook_page' => [
          '#text' => t('Working with blocks (content in regions)'),
          '#url' => Url::fromUri('http://drupal.org/documentation/modules/block'),
        ],
      ],
    ],
    'basic_pages' => [
      '#title' => t('Make basic pages'),
      '#description' => t('<p>Your website needs a home page, and some other static pages.</p>'),
      'home_page' => [
        '#title' => t('Make a home page'),
        '#description' => t('Make a new basic page, and set it as your home page.'),
        'handbook_page' => [
          '#text' => t('Creating a home page'),
          '#url' => $helpService->getDocUrl(HelpConstants::HELP_CREATE_HOME_PAGE),
        ],
      ],
      'help_page' => [
        '#title' => t('Make a help page'),
        '#description' => t('Make a custom help page for your site.'),
        'handbook_page' => [
          '#text' => t('Creating a help page'),
          '#url' => $helpService->getDocUrl(HelpConstants::HELP_CREATE_HELP_PAGE),
        ],
      ],

    ],
    'make_books' => [
      '#title' => t('Make book(s)'),
      '#description' => t('<p>Make book(s) authors put pages in.</p>'),
      'starter_content' => [
        '#title' => t('Make starter content (recommended)'),
        'make_starter_content' => [
          '#text' => t('This is the easiest way to make the books you need.'),
          '#url' => Url::fromUri('http://drupal.org/upgrade'),
        ],
      ],
      'show_blocks' => [
        '#title' => t('Show navigation blocks'),
        'show_book_blocks' => [
          '#text' => t('Show book blocks'),
          '#url' => Url::fromUri('http://drupal.org/documentation/modules/menu'),
        ],
        'show_class_block' => [
          '#text' => t('Show the class selector block.'),
          '#url' => Url::fromUri('http://drupal.org/documentation/modules/taxonomy'),
        ],
      ],
      'locale_i18n' => [
        '#title' => t('Locale and internationalization'),
        'handbook_page' => [
          '#text' => t('Multilingual Guide'),
          '#url' => Url::fromUri('http://drupal.org/documentation/multilingual'),
        ],
      ],
    ],
  ];
  return $result;
}

/**
 * Define a checklist for creating a new class.
 *
 * @return array
 *   Checklist definition.
 */
function skilling_checklist_new_class_items() {
  $result = [
    'make_instructor_account' => [
      '#title' => t('Make an instructor account'),
      '#description' => t('<p>Make a user account for the instructor.</p>'),
      'make_user' => [
        '#title' => t('Make a new account'),
        '#description' => t('Admin | something.'),
        'docs' => [
          '#text' => t('Make user account'),
          '#url' => Url::fromUri('http://drupal.org/documentation/install'),
        ],
      ],
      'instructor_role' => [
        '#title' => t('Instructor role'),
        '#description' => t('Give the new account the instructor role.'),
        'docs' => [
          '#text' => t('Give roles'),
          '#url' => Url::fromUri('http://drupal.org/node/306808'),
        ],
      ],
      'inform_instructor' => [
        '#title' => t('Inform instructor'),
        '#description' => t('Tell the instructor about the new account. Make sure s/he knows how to log in.'),
      ],
    ],
    'create_class_node' => [
      '#title' => t('Create class node'),
      '#description' => t('<p>Doing the things with the thing.</p>'),
      'clone_calendar' => [
        '#title' => t('Clone a calendar'),
        '#description' => t('Often each class has its own calendar. Usually you can clone an existing calendar.'),
        'docs' => [
          '#text' => t('Cloning a calendar'),
          '#url' => Url::fromUri('http://drupal.org/documentation/install'),
        ],
      ],
      'make_new_node' => [
        '#title' => t('Make a new node'),
        '#description' => t('Admin | Skilling something. Select the calendar. Enter a start date.'),
        'docs' => [
          '#text' => t('Creating a class'),
          '#url' => Url::fromUri('http://drupal.org/documentation/install'),
        ],
      ],
      'instructor_owner' => [
        '#title' => t('Make the instructor the node owner'),
        '#description' => t('On the class node edit page, under Authoring Information, make the instructor the author (owner) of the node.'),
        'handbook_page' => [
          '#text' => t('Creating a class'),
          '#url' => Url::fromUri('http://drupal.org/node/306808'),
        ],
      ],
      'inform_instructor' => [
        '#title' => t('Tell the instructor about the new class'),
        '#description' => t('Email, whatever.'),
      ],
    ],
    'add_students_to_class' => [
      '#title' => t('Add students to class'),
      '#description' => t('<p>Gain these skills to pass the <em><a href="http://headrush.typepad.com/creating_passionate_users/2005/10/getting_users_p.html">suck threshold</a></em> and start being creative with Drupal.</p>'),
      'create_student_accounts' => [
        '#title' => t('Create student accounts (optional)'),
        '#description' => t('Maybe you create them, maybe instructors create them, maybe students create their own.'),
        'handbook_page' => [
          '#text' => t('Student accounts'),
          '#url' => Url::fromUri('http://drupal.org/documentation/modules/block'),
        ],
      ],
      'enroll_students_in_class' => [
        '#title' => t('Enroll students in class'),
        '#description' => t('Maybe you do it, maybe instructors do it.'),
        'handbook_page' => [
          '#text' => t('Enrolling students'),
          '#url' => Url::fromUri('http://drupal.org/documentation/modules/block'),
        ],
      ],
      'inform_students' => [
        '#title' => t('Tell the students about their accounts'),
        '#description' => t('Usually instructors do this. Someone has to.'),
      ],
    ],
  ];
  return $result;
}
