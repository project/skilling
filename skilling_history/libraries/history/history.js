/**
 * @file
 * Record node view in history.
 *
 * Call a controller that will mark the nid defined
 * drupalSettings.skillingHistory.nodeToMarkAsViewed as viewed.
 * This code attached for students only in skilling.module.
 *
 * Do it this way, rather than through hook_node_view, because
 * the latter can't tell if a node fetch is real, or a prefetch
 * that might not be viewed at all.
 *
 * Kludgy. The history node in core uses the same approach.
 */

(function ($, Drupal, drupalSettings) {
  "use strict";
  Drupal.skillingSettings.recordedHistory = false;
  Drupal.behaviors.historyProcessing = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.recordedHistory) {
        return;
      }
      Drupal.skillingSettings.recordedHistory = true;
      if (drupalSettings.skillingHistory) {
        if (drupalSettings.skillingHistory.nodeToMarkAsViewed) {
          let nodeId = drupalSettings.skillingHistory.nodeToMarkAsViewed;
          $.ajax({
            url: Drupal.url("skilling-history/save-page-view"),
            type: 'GET',
            data: {
              "csrfToken": drupalSettings.csrfToken,
              "sessionId": drupalSettings.sessionId,
              "nodeId": nodeId
            },
            dataType: 'json',
            success: function (result) {
              // Should get OK back.
              if (result.status.toLowerCase() !== 'ok') {
                let filteredNid = Drupal.behaviors.skillingUtilities.stripTags(nodeId);
                let message = "Did not get OK from recording history node view for nid: " + nodeId;
                Drupal.behaviors.skillingUtilities.showFailReport(message);
              }
            },
            fail: (function (jqXHR, textStatus) {
              let message = Drupal.t(
                  "Skilling history fail @text. Please send a screen shot of your "
                  + "entire browser window, including URL, to someone.",
                  {'@text': textStatus}
              )
            })
          });
        }
      } // End skillingHistory is set.
    } // End attach.
  }
}(jQuery, Drupal, drupalSettings));
