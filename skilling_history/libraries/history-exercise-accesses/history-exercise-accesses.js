(function (Drupal, $) {
  "use strict";
  Drupal.skilling = Drupal.skilling || {};
  Drupal.skilling.setupExerciseAccessHistory = false;
  Drupal.behaviors.exerciseAccessHistory = {
    attach: function (context, settings) {
      if (Drupal.skilling.setupExerciseAccessHistory) {
        return;
      }
      Drupal.skilling.setupExerciseAccessHistory = true;
      $(document).ready(function(){
        $("button.skilling-view-submission").on("click", function () {
          Drupal.behaviors.exerciseAccessHistory.viewSubmissionButton(this);
        });
        $("button.skilling-view-feedback").on("click", function () {
          Drupal.behaviors.exerciseAccessHistory.viewSubmissionFeedbackButton(this);
        });
      });
    }, //End attach.
    /**
     * User clicked view submission button.
     *
     * @param button
     */
    viewSubmissionButton(button) {
      let $button = $(button);
      //Only record displays, not hides.
      if ($button.attr("aria-expanded") === "false") {
        let submissionNid = $button.data("submission-nid");
        //Send to server.
        Drupal.behaviors.exerciseAccessHistory.sendToServer(
            submissionNid,
            'view'
        );
      }
    },
    /**
     * User clicked view submission feedback button.
     *
     * @param button
     */
    viewSubmissionFeedbackButton(button) {
      let $button = $(button);
      //Only record displays, not hides.
      if ($button.attr("aria-expanded") === "false") {
        let submissionNid = $button.data("submission-nid");
        //Send to server.
        Drupal.behaviors.exerciseAccessHistory.sendToServer(
            submissionNid,
            'feedback'
        );
      }
    },
    /**
     * Send submission access notice to server.
     *
     * @param submissionNid
     * @param operation
     */
    sendToServer(submissionNid, operation) {
      $.ajax({
        url: Drupal.url("skilling-history/save-submission-access"),
        type: 'GET',
        data: {
          "csrfToken": drupalSettings.csrfToken,
          "sessionId": drupalSettings.sessionId,
          "submissionNid": submissionNid,
          "operation": operation
        },
        dataType: 'json',
        success: function (result) {
          // Should get OK back.
          if (result.status.toLowerCase() !== 'ok') {
            let filteredNid = Drupal.behaviors.skillingUtilities.stripTags(nodeId);
            let message = "Did not get OK from recording history submission access nid: " + submissionNid;
            Drupal.behaviors.skillingUtilities.showFailReport(message);
          }
        },
        fail: (function (jqXHR, textStatus) {
          let message = Drupal.t(
              "Skilling history fail @text. Please send a screen shot of your "
              + "entire browser window, including URL, to someone.",
              {'@text': textStatus}
          )
        })
      });
    }
  };
})(Drupal, jQuery);

