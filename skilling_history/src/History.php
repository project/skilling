<?php

namespace Drupal\skilling_history;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling_history\Entity\SkillingHistory;
use Drupal\user\Entity\User;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class History.
 */
class History {
  use StringTranslationTrait;

  /**
   * Has search already been recorded?
   *
   * @var bool
   */
  protected $searchRecorded = FALSE;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The path matcher service.
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new History object.
   *
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   The Skilling current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
      SkillingCurrentUser $skillingCurrentUser,
      EntityTypeManagerInterface $entityTypeManager,
      EntityFieldManagerInterface $entityFieldManager,
      PathMatcher $pathMatcher,
      MessengerInterface $messenger) {
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->pathMatcher = $pathMatcher;
    $this->messenger = $messenger;
  }

  /**
   * Record an MCQ answer.
   *
   * @param int $mcqNid
   *   Nid of the MCQ node.
   * @param string $internalName
   *   Internal name of the MCQ.
   * @param string $response
   *   The response the user chose.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordMcqAnswer($mcqNid, $internalName, $response) {
    $label = $this->t('MCQ: @user answered @response to MCQ @internalName (@nid)',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@response' => $response,
        '@internalName' => $internalName,
        '@nid' => $mcqNid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => 'mcq_history',
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_response', $response);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_mcq_item', [$mcqNid]);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record FiB answer.
   *
   * @param int $fibNid
   *   Nid of the MCQ node.
   * @param string $internalName
   *   Internal name of the MCQ.
   * @param string $response
   *   The response the user chose.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordFibAnswer($fibNid, $internalName, $response) {
    $label = $this->t('FiB: @user answered @internalName (@nid)',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@internalName' => $internalName,
        '@nid' => $fibNid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => 'fib_history',
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_response_text', $response);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_fib_item', [$fibNid]);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record storage of a reflection note.
   *
   * @param int $nid
   *   Nid of the containing node.
   * @param string $internalName
   *   Internal name.
   * @param string $note
   *   The note.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordStoreReflectNote($nid, $internalName, $note) {
    $label = $this->t('Reflection: @user answered @internalName in node @nid',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@internalName' => $internalName,
        '@nid' => $nid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_STORE_REFLECTION_NOTE,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_INTERNAL_NAME, $internalName);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_NOTE, $note);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_NODE, $nid);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record login.
   *
   * @param User $account
   *   Account that logged in.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordLogin($account) {
    $label = $this->t('Login: @user (@uid)',
      [
        '@user' => $account->getAccountName(),
        '@uid' => $account->id(),
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_NAME_LOGIN,
      'name' => $label,
      'uid' => $account->id(),
    ]);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record logout.
   *
   * @param User $account
   *   Account that logged out.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordLogout($account) {
    $label = $this->t('Logout: @user (@uid)',
      [
        '@user' => $account->getAccountName(),
        '@uid' => $account->id(),
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_NAME_LOGOUT,
      'name' => $label,
      'uid' => $account->id(),
    ]);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record a node view.
   *
   * @param $nid
   *   Node being viewed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordNodeView($nid) {
    $label = $this->t('Node view: @user views node @nid',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@nid' => $nid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_VIEW_PAGE,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    $nodeViewed = $this->entityTypeManager->getStorage('node')->load($nid);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_page', $nid);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set('field_page_type', $nodeViewed->bundle());
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record a badge award.
   *
   * @param $nid
   *   Badge awarded.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordBadgeAwarded($nid) {
    $label = $this->t('Badge awarded: @user gets award node @nid',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@nid' => $nid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_BADGE_AWARDED,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_BADGE, $nid);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record a view being viewed.
   *
   * @param $viewName
   *   The internal name of the view.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordViewView($viewName) {
    $label = $this->t('Views view: @user looks at view @view',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@view' => $viewName,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_VIEW_VIEW,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_VIEW_ID, $viewName);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record a search being run.
   *
   * @param $searchText
   *   Text searched for.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordSearch($searchText) {
    $searchText = trim($searchText);
    if (strlen($searchText) === 0) {
      return;
    }
    $label = $this->t('Search: @user searches for @searchText',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@searchText' => $searchText,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    //Trim search field to max length.
    $fieldDefs = $this->entityFieldManager->getFieldDefinitions(
      SkillingHistoryConstants::ENTITY_TYPE_NAME,
      SkillingHistoryConstants::BUNDLE_SEARCH
    );
    $searchTextFieldStorageDef
      = $fieldDefs[SkillingHistoryConstants::FIELD_SEARCH_TEXT]
      ->getFieldStorageDefinition();
    $maxLength = $searchTextFieldStorageDef->getSetting('max_length');
    if (strlen($searchText) > $maxLength) {
      $searchText = substr(0, $maxLength);
    }
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_SEARCH,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_SEARCH_TEXT, $searchText);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record a suggestion being made.
   *
   * @param $suggestion
   *   The suggestion.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordSuggestion($suggestion) {
    $suggestion = trim($suggestion);
    if (strlen($suggestion) === 0) {
      return;
    }
    $label = $this->t('Suggestion: from @user',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_SUGGESTION,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_SUGGESTION, $suggestion);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record Your class page being viewed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordYourClassView() {
    $label = $this->t('Your class: @user looks at your class',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_VIEW_YOUR_CLASS,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record access to a submission.
   *
   * @param $nid
   *   Submission node id.
   * @param $operation
   *   Operation.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function recordSubmissionAccess($nid, $operation) {
    // Check that the operation is allowed.
    $fieldDef = $this->entityFieldManager->getFieldDefinitions(
      SkillingHistoryConstants::ENTITY_TYPE_NAME,
      SkillingHistoryConstants::BUNDLE_SUBMISSION_ACCESS
    );
    $fieldStorageDef = $fieldDef['field_operation']->getFieldStorageDefinition();
    $allowedValues = $fieldStorageDef->getSetting('allowed_values');
    if (!in_array($operation, $allowedValues)) {
      throw new SkillingInvalidValueException(
        "Bad submission access operation: $operation"
      );
    }
    $label = $this->t('Submission access: @user @operation node @nid',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@operation' => $operation,
        '@nid' => $nid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_SUBMISSION_ACCESS,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_SUBMISSION, $nid);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_OPERATION, $operation);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record access to user account.
   *
   * @param $uid
   *   Account accessed.
   * @param $operation
   *   Operation performed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function recordAccountAccess($uid, $operation) {
    // Check that the operation is allowed.
    $fieldDefs = $this->entityFieldManager->getFieldDefinitions(
      SkillingHistoryConstants::ENTITY_TYPE_NAME,
      SkillingHistoryConstants::BUNDLE_ACCOUNT_ACCESS
    );
    $fieldStorageDef = $fieldDefs[SkillingHistoryConstants::FIELD_OPERATION]
      ->getFieldStorageDefinition();
    $allowedValues = $fieldStorageDef->getSetting('allowed_values');
    if (!in_array($operation, $allowedValues)) {
      throw new SkillingInvalidValueException(
        "Bad submission access operation: $operation"
      );
    }
    $label = $this->t('Account access: @user @operation on user @uid',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@operation' => $operation,
        '@uid' => $uid,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_ACCOUNT_ACCESS,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_USER, $uid);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_OPERATION, $operation);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Record that a message was sent to a user.
   *
   * @param $recipientId
   *   Recipient id.
   * @param $subject
   *   Subject.
   * @param $message
   *   Message.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function recordSentMessage($recipientId, $subject, $message) {
    $label = $this->t('Message sent: @user to @recipientId',
      [
        '@user' => $this->skillingCurrentUser->getAccountName(),
        '@recipientId' => $recipientId,
      ]
    );
    // Trim the label to max length allowed.
    $label = $this->trimName($label);
    // Create the new entity.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyRecord */
    $historyRecord = SkillingHistory::create([
      'bundle' => SkillingHistoryConstants::BUNDLE_SENT_MESSAGE,
      'name' => $label,
      'uid' => $this->skillingCurrentUser->id(),
    ]);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_RECIPIENT, $recipientId);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_SUBJECT, $subject);
    /** @noinspection PhpUndefinedMethodInspection */
    $historyRecord->set(SkillingHistoryConstants::FIELD_MESSAGE, $message);
    // Save it.
    $historyRecord->save();
  }

  /**
   * Trim the name of the new history entity to the max length allowed.
   *
   * @param $entityName
   *   Name.
   *
   * @return string
   *   Trimmed name.
   */
  protected function trimName($entityName) {
    // Get the max length of the name base field.
    $baseFields = $this->entityFieldManager->getBaseFieldDefinitions(
      'skilling_history'
    );
    /** @var \Drupal\Core\Field\BaseFieldDefinition $nameDef */
    $nameDef = $baseFields['name'];
    /** @var \Drupal\Core\Field\TypedData\FieldItemDataDefinition $nameItemDef */
    $nameItemDef = $nameDef->getItemDefinition();
    $nameMaxLength = $nameItemDef->getSetting('max_length');
    if (strlen($entityName) > $nameMaxLength) {
      $entityName = substr($entityName, 0, $nameMaxLength);
    }
    return $entityName;
  }

  /**
   * Find the last lesson viewed by the current user.
   *
   * @return int|null
   *   Lesson id or null if none.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLastLessonViewedByCurrentUser() {
    // Only for students.
    if (!$this->skillingCurrentUser->isStudent()) {
      return NULL;
    }
    $uid = $this->skillingCurrentUser->id();
    $historyRecordIds = $this->entityTypeManager
      ->getStorage(SkillingHistoryConstants::ENTITY_TYPE_NAME)
      ->getQuery()
      ->condition('bundle', SkillingHistoryConstants::BUNDLE_VIEW_PAGE)
      // For the right user.
      ->condition('uid', $uid)
      // Of view lesson.
      ->condition(
        SkillingHistoryConstants::FIELD_PAGE_TYPE,
        SkillingConstants::LESSON_CONTENT_TYPE
         )
      // Sort by date.
      ->sort('created', 'DESC')
      // Get the first record.
      ->range(0, 1)
      ->execute();
    // Found anything?
    if (count($historyRecordIds) === 0) {
      // Nothing.
      return NULL;
    }
    $historyRecordId = reset($historyRecordIds);
    // Load the node that was referred to.
    $historyRecord = $this->entityTypeManager
      ->getStorage(SkillingHistoryConstants::ENTITY_TYPE_NAME)
      ->load($historyRecordId);
    if (!$historyRecord) {
      return NULL;
    }
    $lessonNid = $historyRecord->get(SkillingHistoryConstants::FIELD_PAGE)->target_id;
    // Return the id of the lesson node.
    return $lessonNid;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function showLastLessonForCurrentStudent() {
    if (!$this->skillingCurrentUser->isStudent()) {
      return;
    }
    // Show last lesson viewed.
    $lastLessonId = $this->getLastLessonViewedByCurrentUser();
    if ($lastLessonId) {
      /** @var \Drupal\node\NodeInterface $lesson */
      $lesson = $this->entityTypeManager->getStorage('node')
        ->load($lastLessonId);
      if ($lesson) {
        $link = Link::createFromRoute(
          $lesson->getTitle(),
          'entity.node.canonical', ['node' => $lastLessonId]
        )->toString();
        $message = $this->t(
          'The last lesson you looked at was @link.',
          ['@link' => $link]
        );
        $this->messenger->addMessage($message);
      }
    }
  }



}
