<?php

namespace Drupal\skilling_history;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\SkillingConstants;

/**
 * Class SkillingHistoryAccessControlHandler
 *
 * This does entity-level access control only, it seems. Still need
 * to do field-level through module hooks. Not sure, though.
 *
 * @package Drupal\skilling_history
 */
class SkillingHistoryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws SkillingException
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // Possible results.
    $allowed = AccessResult::allowed()->setCacheMaxAge(0);
    $forbidden = AccessResult::forbidden()->setCacheMaxAge(0);
    // Copy entity to another var with type hint, for IDE.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyEntity */
    $historyEntity = $entity;
    // Get the current (logged in) user.
    /** @var \Drupal\skilling\SkillingCurrentUser $currentUser */
    $currentUser = Drupal::service(SkillingConstants::CURRENT_USER_SERVICE);
    // If config entity...
    if ($entity->getEntityTypeId() === SkillingHistoryConstants::ENTITY_BUNDLE_CONFIG_TYPE_NAME) {
      // View access OK.
      if ($operation === 'view') {
        return $allowed;
      }
      // Other - only admins.
      if ($currentUser->isAdministrator()) {
        return $allowed;
      }
      return $forbidden;
    }
    // Current user is owner of history entity?
    if ($currentUser->id() === $historyEntity->getOwnerId()) {
      // Admin can so anything - though storing history of admin is unlikely.
      if ($currentUser->isAdministrator()) {
        return $allowed;
      }
      // Views are allowed of own stuff.
      if ($operation === 'view') {
        return $allowed;
      }
      // No edit or delete.
      return $forbidden;
    }
    // Current user is admin?
    if ($currentUser->isAdministrator()) {
      return $allowed;
    }
    // Current user is anonymous user?
    if ($currentUser->id() === 0) {
      return $forbidden;
    }
    // Current user is instructor of owner of history entity?
    if ($currentUser->isInstructor()) {
      /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $checkUserRelationshipsService */
      $checkUserRelationshipsService = Drupal::service(SkillingConstants::USER_RELATIONSHIP_SERVICE);
      $isInstructor = $checkUserRelationshipsService->isUserUidInstructorOfUserUid(
        $currentUser->id(),
        $historyEntity->getOwnerId()
      );
      if ($isInstructor) {
        if ($operation === 'view') {
          return $allowed;
        }
        // No edit or delete.
        return $forbidden;
      }
    }
    return $forbidden;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, "create {$context['entity_type_id']} $entity_bundle");
  }
}
