<?php

namespace Drupal\skilling_history\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling_history\SkillingHistoryConstants;

class ConfirmDeleteAllForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'skilling_history_confirm_delete_all_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = \Drupal::entityTypeManager()->getStorage('skilling_history');
    $count = $storage->getAggregateQuery()->count()->execute();
    if ($count == 0) {
      $form['description'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No history records to delete.'),
      ];
    }
    else {
      $form['count'] = [
        '#type' => 'markup',
        '#markup' => $this->t(
          '<p>Number of records: @count</p>',
          ['@count' => $count]
        ),
      ];
      $form['description'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<p>Delete all history records?</p>'),
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Delete',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $storage = \Drupal::entityTypeManager()->getStorage('skilling_history');
    $count = $storage->getAggregateQuery()->count()->execute();
    $steps = ceil($count/SkillingHistoryConstants::DELETE_ALL_RECORDS_SUBSET_SIZE);
    // Set up the operations, erase groups of records.
    $operations = [];
    for ($step = 0; $step < $steps; $step ++) {
      $startRecord = $step * SkillingHistoryConstants::DELETE_ALL_RECORDS_SUBSET_SIZE + 1;
      $endRecord = $startRecord + SkillingHistoryConstants::DELETE_ALL_RECORDS_SUBSET_SIZE - 1;
      if ($endRecord > $count) {
        $endRecord = $count;
      }
      $operation = [
        'skilling_history_delete_record_subset',
        [
          $step + 1,
          $this->t(
            'Step @step: Erasing record @start to @end.',
            [
              '@step' => $step + 1,
              '@start' => $startRecord,
              '@end' => $endRecord,
            ]
          ),
        ],
      ];
      $operations[] = $operation;
    }
    $batch = [
      'operations' => $operations,
      'finished' => 'skilling_history_delete_record_subset_finished',
      'title' => $this->t('Delete history records'),
      'init_message' => $this->t('Deletion starting...'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Encountered an error when deleting history records.'),
    ];
    batch_set($batch);
  }


}
