<?php

namespace Drupal\skilling_history\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining Skilling History entity type entities.
 */
interface SkillingHistoryTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {}
