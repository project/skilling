<?php

namespace Drupal\skilling_history\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Skilling history Type. A configuration entity used to manage
 * bundles for the Skilling history entity.
 *
 * @ConfigEntityType(
 *   id = "skilling_history_type",
 *   label = @Translation("Skilling history type"),
 *   bundle_of = "skilling_history",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "skilling_history_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\skilling_history\SkillingHistoryTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\skilling_history\Form\SkillingHistoryTypeForm",
 *       "add" = "Drupal\skilling_history\Form\SkillingHistoryTypeForm",
 *       "edit" = "Drupal\skilling_history\Form\SkillingHistoryTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer skilling history types",
 *   links = {
 *     "canonical" = "/admin/structure/skilling_history_type/{skilling_history_type}",
 *     "add-form" = "/admin/structure/skilling_history_type/add",
 *     "edit-form" = "/admin/structure/skilling_history_type/{skilling_history_type}/edit",
 *     "delete-form" = "/admin/structure/skilling_history_type/{skilling_history_type}/delete",
 *     "collection" = "/admin/structure/skilling_history_type",
 *   }
 * )
 */
class SkillingHistoryType extends ConfigEntityBundleBase implements SkillingHistoryTypeInterface {

  /**
   * The machine name of the Skilling History type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Skilling History type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the Skilling History type.
   *
   * @var string
   */
  protected $description;
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
