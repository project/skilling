<?php

namespace Drupal\skilling_history\Entity;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Skilling history entity entities.
 *
 * @ingroup skilling_history
 */
interface SkillingHistoryInterface extends EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Skilling History entity name.
   *
   * @return string
   *   Name of the Skilling History entity.
   */
  public function getName();

  /**
   * Sets the Skilling History entity name.
   *
   * @param string $name
   *   The Skilling History entity name.
   *
   * @return \Drupal\skilling_history\Entity\SkillingHistoryInterface
   *   The called Skilling History entity.
   */
  public function setName($name);

  /**
   * Gets the Skilling History entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Skilling History entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Skilling History entity creation timestamp.
   *
   * @param int $timestamp
   *   The Skilling History entity creation timestamp.
   *
   * @return \Drupal\skilling_history\Entity\SkillingHistoryInterface
   *   The called Skilling History entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get notes about the entity.
   *
   * @return string
   *   Notes about the entity.
   */
  public function getNotes();

  /**
   * Set notes about the entity.
   *
   * @param $notesIn
   *   Notes.
   *
   * @return \Drupal\skilling_history\Entity\SkillingHistoryInterface
   *   The called Skilling History entity entity.
   */
  public function setNotes($notesIn);
}
