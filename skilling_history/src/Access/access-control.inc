<?php

/**
 * @file
 * Hooks for access control to blocks, fields, etc.
 *
 * Include this file in skilling.module.
 *
 * @see notes/security/hooks.txt for more.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
//use Drupal\node\NodeInterface;
//use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling_history\SkillingHistoryConstants;
use Drupal\skilling\SkillingConstants;

/**
 * Implements hook_entity_access().
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 * @param $operation
 * @param \Drupal\Core\Session\AccountInterface $account
 *
 * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\skilling\Exception\SkillingException
 */
function skilling_history_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  if (skilling_history_is_history_entity($entity)) {
    // Possible results.
    $allowed = AccessResult::allowed()->setCacheMaxAge(0);
    $forbidden = AccessResult::forbidden()->setCacheMaxAge(0);
    // Copy entity to another var with type hint, for IDE.
    /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyEntity */
    $historyEntity = $entity;
    // Get the current (logged in) user.
    /** @var Drupal\skilling\SkillingCurrentUser $currentUser */
    $currentUser = \Drupal::service(SkillingConstants::CURRENT_USER_SERVICE);
    // If config entity...
    if ($entity->getEntityTypeId() === SkillingHistoryConstants::ENTITY_BUNDLE_CONFIG_TYPE_NAME) {
      // View access OK.
      if ($operation === 'view') {
        return $allowed;
      }
      // Other - only admins.
      if ($currentUser->isAdministrator()) {
        return $allowed;
      }
      return $forbidden;
    }
    // Current user is owner of history entity?
    if ($currentUser->id() === $historyEntity->getOwnerId()) {
      // Admin can so anything - though storing history of admin is unlikely.
      if ($currentUser->isAdministrator()) {
        return $allowed;
      }
      // Views are allowed of own stuff.
      if ($operation === 'view') {
        return $allowed;
      }
      // No edit or delete.
      return $forbidden;
    }
    // Current user is admin?
    if ($currentUser->isAdministrator()) {
      return $allowed;
    }
    // Current user is anonymous user?
    if ($currentUser->id() === 0) {
      return $forbidden;
    }
    // Current user is instructor of owner of history entity?
    if ($currentUser->isInstructor()) {
      /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $checkUserRelationshipsService */
      $checkUserRelationshipsService = \Drupal::service(SkillingConstants::USER_RELATIONSHIP_SERVICE);
      $isInstructor = $checkUserRelationshipsService->isUserUidInstructorOfUserUid(
        $currentUser->id(),
        $historyEntity->getOwnerId()
      );
      if ($isInstructor) {
        if ($operation === 'view') {
          return $allowed;
        }
        // No edit or delete.
        return $forbidden;
      }
    }
    return $forbidden;
  }
  else {
    $result = AccessResult::neutral()->setCacheMaxAge(0);
  }
  return $result;
}

function skilling_history_is_history_entity(EntityInterface $entity) {
  $entityType = $entity->getEntityTypeId();
  $isHistoryEntity =
    $entityType === SkillingHistoryConstants::ENTITY_TYPE_NAME
    || $entityType === SkillingHistoryConstants::ENTITY_BUNDLE_CONFIG_TYPE_NAME;
  return $isHistoryEntity;
}

/**
 * Implements hook_entity_field_access().
 *
 * If can access entity (checked earlier), then can access any field.
 */
function skilling_history_entity_field_access($operation, FieldDefinitionInterface $fieldDefinition, \Drupal\Core\Session\AccountInterface $account, FieldItemListInterface $items = NULL) {
  if ($items) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $items->getEntity();
    if (skilling_history_is_history_entity($entity)) {
      $result = AccessResult::allowed()->setCacheMaxAge(0);
    }
    else {
      $result = AccessResult::neutral()->setCacheMaxAge(0);
    }
    return $result;
  }
  // No items.
  $result = AccessResult::neutral()->setCacheMaxAge(0);
  return $result;
}

