<?php


namespace Drupal\skilling_history;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class SkillingHistoryConstants {

  use StringTranslationTrait;

  const MODULE_NAME = 'skilling_history';
  const ENTITY_TYPE_NAME = 'skilling_history';
  const ENTITY_BUNDLE_CONFIG_TYPE_NAME = 'skilling_history_type';

  const DELETE_ALL_RECORDS_SUBSET_SIZE = 50;

  // Bundles of history entity.
  const BUNDLE_ACCOUNT_ACCESS = 'account_access';
  const BUNDLE_BADGE_AWARDED = 'badge_awarded';
  const BUNDLE_NAME_LOGIN = 'log_in';
  const BUNDLE_NAME_LOGOUT = 'log_out';
  const BUNDLE_SEARCH = 'search';
  const BUNDLE_SENT_MESSAGE = 'sent_message';
  const BUNDLE_STORE_REFLECTION_NOTE = 'store_reflection_note';
  const BUNDLE_SUGGESTION = 'suggestion';
  const BUNDLE_SUBMISSION_ACCESS = 'submission_access';
  const BUNDLE_VIEW_PAGE = 'view_page';
  const BUNDLE_VIEW_VIEW = 'view_view';
  const BUNDLE_VIEW_YOUR_CLASS = 'view_your_class';

  const OPERATION_CREATE = 'create';
  const OPERATION_DELETE = 'delete';
  const OPERATION_EDIT = 'edit';
  const OPERATION_FEEDBACK = 'feedback';
  const OPERATION_VIEW = 'view';

  //Field for which badge was awarded.
  const FIELD_BADGE = 'field_badge';

  // Entity operation.
  const FIELD_OPERATION = 'field_operation';

  // Field with ref to submission node.
  const FIELD_SUBMISSION = 'field_submission';

  // Fields for view view.
  const FIELD_VIEW_ID = 'field_view_id';

  // Field for search text.
  const FIELD_SEARCH_TEXT = 'field_search_text';

  // Field for suggestion.
  const FIELD_SUGGESTION = 'field_suggestion';

  // Field for user account access.
  const FIELD_USER = 'field_user';

  // Field for internal name of things (e.g., reflection note).
  const FIELD_INTERNAL_NAME = 'field_internal_name';

  // Field for a node reference.
  const FIELD_NODE = 'field_node';

  // Field for a reflection note.
  const FIELD_NOTE = 'field_note';

  // Field for a recipient user (of a message)
  const FIELD_RECIPIENT = 'field_recipient';

  // Field for the message subject.
  const FIELD_SUBJECT = 'field_subject';

  // Field for message.
  const FIELD_MESSAGE = 'field_message';

  // Field for type (bundle) of page viewed.
  const FIELD_PAGE_TYPE = 'field_page_type';

  // Field for page that was accessed.
  const FIELD_PAGE = 'field_page';
}
