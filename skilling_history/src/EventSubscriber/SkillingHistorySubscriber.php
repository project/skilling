<?php

namespace Drupal\skilling_history\EventSubscriber;

use Drupal;
use Drupal\Component\Utility\Html;
use Drupal\skilling\SkillingConstants;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Watch events.
 */
class SkillingHistorySubscriber implements EventSubscriberInterface {

  protected $viewPathsToRecord = [];

  public function __construct() {
    $this->viewPathsToRecord = [
      '/lessons',
      '/exercises',
      '/patterns',
      '/badges',
      '/principles',
      '/models',
      '/skilling/submissions',
      '/skilling/reflect-notes',
      '/skilling/notices',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['routingRouteFinished'];
    return $events;
  }

  /**
   * This method is called when the routing.route_finished event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   What happened.
   *
   */
  public function routingRouteFinished(GetResponseEvent $event) {
    /** @var \Drupal\skilling\SkillingCurrentUser $currentUser */
    $currentUser = Drupal::service('skilling.skilling_current_user');
    if ($currentUser->isStudent()) {
      $uri = Drupal::service('path.current')->getPath();
      // Store this fetch of a view?
      if (in_array($uri, $this->viewPathsToRecord)) {
        /** @var \Drupal\skilling_history\History $historyService */
        $historyService = Drupal::service(SkillingConstants::HISTORY_SERVICE_NAME);
        // Record the event in history.
        try {
          $viewId = substr($uri, 1);
          $historyService->recordViewView($viewId);
        }
        catch (Exception $e) {
          $message = 'Exception view view history call, uid: '
            . Html::escape($currentUser->id())
            . $e->getMessage();
          Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
        }

      }
    }
  }

}
