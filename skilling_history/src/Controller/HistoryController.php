<?php

namespace Drupal\skilling_history\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Utility\Token;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling_history\History;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class HistoryController.
 */
class HistoryController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $sessionService;

  /**
   * CSRF token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfTokenGeneratorService;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * Constructs a new HistoryController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling_history\History $historyService
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfTokenGeneratorService
   * @param \Drupal\Core\Utility\Token $token
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    History $historyService,
    SkillingCurrentUser $currentUser,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    Session $session,
    CsrfTokenGenerator $csrfTokenGeneratorService,
    Token $token
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->historyService = $historyService;
    $this->currentUser = $currentUser;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->sessionService = $session;
    $this->csrfTokenGeneratorService = $csrfTokenGeneratorService;
    $this->tokenService = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling_history.history'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.ajax_security'),
      $container->get('session'),
      $container->get('csrf_token'),
      $container->get('token')
    );
  }

  /**
   * Save a page view by a student.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function savePageView(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling-history/save-page-view']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isStudent()) {
      throw new AccessDeniedException('Access denied');
    }
    $nid = $request->query->get('nodeId');
    $this->historyService->recordNodeView($nid);
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

  /**
   * Save a submission access by a student.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function saveSubmissionAccess(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling-history/save-submission-access']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isStudent()) {
      throw new AccessDeniedException('Access denied');
    }
    $submissionNid = $request->query->get('submissionNid');
    $operation = $request->query->get('operation');
    $this->historyService->recordSubmissionAccess($submissionNid, $operation);
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }
}
