<?php

namespace Drupal\skilling_history\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\skilling_history\DeleteAllRecords;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Class StudentHistoryAdminController.
 */
class StudentHistoryAdminController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingSkillingCurrentUser;


  /**
   * Check user relationship service.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $checkUserRelationshipService;

  /**
   * Constructs a new StudentHistoryAdminController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_skilling_current_user
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $checkUserRelationshipService
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentUser $skilling_skilling_current_user,
    SkillingCheckUserRelationships $checkUserRelationshipService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingSkillingCurrentUser = $skilling_skilling_current_user;
    $this->checkUserRelationshipService = $checkUserRelationshipService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.check_user_relationships')
    );
  }

  /**
   * Show a student his/her own submissions.
   *
   * The code loads and shows a view, rather than let Views handle the
   * route and menu item itself. The view uses a contextual filter for
   * the user the submissions are from. The filter uses a default argument
   * of the current user's uid. Handling the route here prevents
   * users from appending a uid to the URL, and getting another user's
   * submission.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Renderable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function studentHistoryAdmin() {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('history_for_instructor');
    $result['view'] = $this->t('Attachment error.');
    if (is_object($view)) {
//      $args = [$this->currentUserService->id()];
      $args = [];
      $view->setDisplay('list');
//      $view->setArguments($args);
      $view->preExecute();
      $view->execute();
      // Filter records.
      // Todo: Could cache the decisions for each user in a local array.
      if (!$this->skillingSkillingCurrentUser->isAdministrator()) {
        foreach ($view->result as $index => $row) {
          /** @var \Drupal\skilling_history\Entity\SkillingHistoryInterface $historyEntity */
          $historyEntity = $row->_entity;
          $studentId = $historyEntity->getOwnerId();
          $currentUserIsInstructor
            = $this->checkUserRelationshipService->isUserUidInstructorOfUserUid(
            $this->skillingSkillingCurrentUser->id(),
            $studentId
          );
          if (!$currentUserIsInstructor) {
            unset($view->result[$index]);
          }
        }
      }
      $result['view'] = $view->buildRenderable('list');
    }
    return $result;
  }

}
