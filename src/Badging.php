<?php

namespace Drupal\skilling;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Class Badging.
 */
class Badging {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Submission service.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;

  /**
   * Constructs a new Badging object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentUser $skilling_skilling_current_user,
    Submissions $submissionsService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingCurrentUser = $skilling_skilling_current_user;
    $this->submissionsService = $submissionsService;
  }

  /**
   * Get the published badges.
   *
   * @return array|\Drupal\node\NodeInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPublishedBadges() {
    // Get the badges.
    $badgeNids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', SkillingConstants::BADGE_CONTENT_TYPE)
      ->condition('status', TRUE)
      ->execute();
    if (count($badgeNids) === 0) {
      return [];
    }
    /** @var \Drupal\node\NodeInterface[] $badges */
    $badges = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($badgeNids);
    return $badges;
  }

  public function updateUserBadges(SkillingUser $student) {
    if (!$student->isStudent()) {
      return;
    }
    // Get the total number of challenge completed exercises for the student.
    $completedCount = $this->submissionsService->getCountCompletedChallengeExercisesForStudent($student);
    // Get the badges that exist.
    $badges = $this->getPublishedBadges();
    $badgeNidsUserShouldHave = [];
    /** @var \Drupal\node\NodeInterface $badge */
    foreach ($badges as $badgeNid => $badge) {
      $exercisesNeeded = $badge->get(SkillingConstants::FIELD_NUMBER_CHALLENGES)
        ->first()->getValue()['value'];
      if ($completedCount >= $exercisesNeeded) {
        $badgeNidsUserShouldHave[] = $badgeNid;
      }
    }
    if (count($badgeNidsUserShouldHave) > 0) {
      $student->awardBadges($badgeNidsUserShouldHave);
    }
  }

}
