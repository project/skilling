<?php

//namespace src;
namespace Drupal\skilling;

use Drupal;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\skilling\Exception\SkillingValueMissingException;

class ExerciseDueRecord {

  /**
   * Exercise that is due.
   *
   * @var integer
   */
  protected $exerciseId;

  /**
   * Day the exercise is due, from the course start date.
   * First day of class is day 1, not day 0.
   *
   * @var integer
   */
  protected $day;

  /**
   * Is the exercise required?
   *
   * @var boolean
   */
  protected $required;

  /**
   * Maximum number of submissions allowed for the exercise.
   *
   * @var integer|null
   */
  protected $maximumSubmissions;

  /**
   * Exercise.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $exercise;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ExerciseDueRecord constructor.
   *
   * @param integer $exerciseIdIn
   *   Exercise id.
   * @param integer $dayIn
   *   Day exercise is due from start of a class.
   *   First day of class is day 1, not day 0.
   * @param boolean $requiredIn
   *   Is the exercise required?
   * @param integer $maxSubsIn
   *   Maximum submissions allowed.
   * @param \Drupal\Core\Entity\EntityTypeManager|null $entityTypeManagerIn
   *   Entity type manager service.
   *   Having it as an optional parameter makes unit testing easier.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function __construct(
    $exerciseIdIn,
    $dayIn,
    $requiredIn,
    $maxSubsIn,
    EntityTypeManager $entityTypeManagerIn = NULL
  ) {
    // Validate the exercise id.
    if (!$exerciseIdIn || !is_numeric($exerciseIdIn) || $exerciseIdIn < 1) {
      throw new SkillingValueMissingException(
        'Expected class id, got ' . $exerciseIdIn,
        __FILE__, __LINE__
      );
    }
    if (is_null($entityTypeManagerIn)) {
      $this->entityTypeManager = Drupal::entityTypeManager();
    } else {
      $this->entityTypeManager = $entityTypeManagerIn;
    }
    $exercise = $this->entityTypeManager->getStorage('node')
      ->load($exerciseIdIn);
    if (!$exercise) {
      throw new Drupal\skilling\Exception\SkillingNotFoundException(
        'Class with id not found. Id: ' . $exerciseIdIn,
        __FILE__, __LINE__
      );
    }
    $this->exerciseId = (integer)$exerciseIdIn;
    $this->exercise = $exercise;
    // Check day.
    if (!$dayIn || !is_numeric($dayIn) || $dayIn < 1) {
      throw new SkillingInvalidValueException(
        'Invalid day:' . $dayIn,
        __FILE__, __LINE__
      );
    }
    $this->day = (integer)$dayIn;
    // Required.
    // Convert truthy to strict boolean.
    $this->required = $requiredIn ? TRUE : FALSE;
    // Validate max subs.
    if ($maxSubsIn == 0) {
      $maxSubsIn = NULL;
    }
    if (!is_null($maxSubsIn)) {
      if (!$maxSubsIn || !is_numeric($maxSubsIn) || $maxSubsIn < 1) {
        throw new SkillingInvalidValueException(
          'Invalid max subs:' . $maxSubsIn,
          __FILE__, __LINE__
        );
      }
    }
    $this->maximumSubmissions = (integer)$maxSubsIn;
  }

  /**
   * Get the exercise id.
   *
   * @return int
   *   Exercise id
   */
  public function getExerciseId() {
    return $this->exerciseId;
  }

  /**
   * Get the exercise.
   *
   * @return \Drupal\node\NodeInterface
   *   Exercise
   */
  public function getExercise() {
    return $this->exercise;
  }

  /**
   * Get the day from the start of the class the exercise is due.
   * First day of class is day 1, not day 0.
   *
   * @return int
   *   Exercise id
   */
  public function getDay() {
    return $this->day;
  }

  /**
   * Is the exercise required?
   *
   * @return boolean
   *   True if required, else false.
   */
  public function getRequired() {
    return $this->required;
  }

  /**
   * Get the maximum number of submissions allowed for the exercise.
   *
   * @return int
   *   Maximum number of submissions
   */
  public function getMaximumSubmissions() {
    return $this->maximumSubmissions;
  }

}
