<?php

namespace Drupal\skilling\Help;

/**
 * Interface HelpInterface.
 */
interface HelpInterface {

  /**
   * Get a URL to a Skilling help document.
   *
   * @param string $docToLink
   *   Document relative URL. A constant from the help service.
   *
   * @return \Drupal\Core\Url
   *   The absolute URL to the document.
   */
  public function getDocUrl($docToLink);

  /**
   * Get a URL to a Skilling help document.
   *
   * @param string $docToLink
   *   Document relative URL. A constant from the help service.
   *
   * @return string
   *   The absolute URL to the document.
   */
  public function getDocUrlAsString($docToLink);

}
