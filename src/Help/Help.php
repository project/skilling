<?php

namespace Drupal\skilling\Help;

use Drupal\Core\Url;
use Drupal\skilling\Utilities;

/**
 * Class Help.
 */
class Help implements HelpInterface {

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Constructs a new Help object.
   *
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Utilities service.
   */
  public function __construct(Utilities $skillingUtilities) {
    $this->skillingUtilities = $skillingUtilities;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocUrl($docToLink) {
    $uri = $this->getDocUrlAsString($docToLink);
    $result = Url::fromUri($uri, ['attributes' => ['target' => '_blank']]);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocUrlAsString($docToLink) {
    $docToLink = $this->skillingUtilities->addSlashToFront($docToLink);
    $uri = HelpConstants::USER_DOC_BASE . $docToLink;
    return $uri;
  }

}
