<?php

namespace Drupal\skilling\Help;

/**
 * Help system constants.
 *
 * Moved from the Help class, to make the constants easy to access from
 * the submodules.
 *
 * @package Drupal\skilling\Help
 */
class HelpConstants {

  // Where user documentation is kept. No / at the end.
  const USER_DOC_BASE = 'https://www.drupal.org/docs/8/modules/skilling';
  // Where 'Start here' documentation lives.
  const USER_DOC_START = 'https://www.drupal.org/docs/8/modules/skilling';
  // Links to documentation on given topics.
  const USER_DOC_LINKS = [
    'command line access' => 'documentation/install/command-line-access',
    'setup https' => 'documentation/install/setup-https',
    'install with composer' => 'documentation/install/install-with-composer',
    'set up private file system' => 'documentation/install/setup-private-file-system',
    'set up trusted hosts' => 'documentation/install/setup-trusted-hosts',
    'allowed uploaded file attributes' => 'documentation/administer/allowed-uploaded-file-attributes',
    'alt text for photo' => 'documentation/all-users/alt-text-for-photo',
    'set up blocks for books' => 'documentation/install/set-up-blocks-for-books',
    'main menu for students' => 'documentation/students/main-menu',

  ];

  const HELP_CREATE_HELP_PAGE = 'administration/customizing-skilling/creating-a-help-page';
  const HELP_CREATE_HOME_PAGE = 'administration/customizing-skilling/creating-a-home-page';
  const HELP_TRUSTED_HOST = 'installation/trusted-hosts-setting';
  const HELP_USE_HTTPS = 'installation/use-https';
  const HELP_PRIVATE_FILE_SYSTEM = 'installation/set-up-a-private-file-path';

  // Title of the help page to show help if it exists.
  const HELP_PAGE_TITLE = 'Help';
  // Title of the page explaining the completion score.
  const EXPLAIN_COMPLETION_SCORE_PAGE_TITLE = 'Completion score';
  // Title of the help page to show submission policies if it exists.
//  const SUBMISSION_POLICIES_PAGE_TITLE = 'Submission policies';

  const DEFAULT_EXTERNAL_HELP_PAGE_URL = 'https://www.drupal.org/docs/8/modules/skilling';

}
