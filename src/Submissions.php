<?php

namespace Drupal\skilling;

use Drupal;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Exception\SkillingClassRoleException;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\node\Entity\Node;
use Drupal\Component\Utility\Html;
use Drupal\skilling\SkillingClass\SkillingClass;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Symfony\Component\HttpFoundation\ParameterBag;
use Drupal\Core\Config\ConfigFactory;
use Drupal\file\Entity\File;

/**
 * Service for managing submission data.
 */
class Submissions {

  use StringTranslationTrait;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Skilling current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * A service for tracking node ids.
   *
   * Helps track exercises inserted in lessons by the parser. Used to
   * update entity reference field on exercise nodes.
   *
   * @var \Drupal\skilling\NidBag
   */
  protected $nidBag;

  /**
   * The Skilling parser, for parsing student solution text.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   Skilling current user service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   Skilling utilities service.
   * @param SkillingCurrentClass $currentClass
   *   Skilling current class service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory service.
   * @param \Drupal\skilling\NidBag $nidBag
   *   A service for tracking node ids.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   *   The Skilling parser.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentUser $currentUser,
    SkillingUtilities $skilling_utilities,
    SkillingCurrentClass $currentClass,
    ConfigFactory $configFactory,
    NidBag $nidBag,
    SkillingParser $parser,
    FilterUserInputInterface $filterInputService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $currentUser;
    $this->skillingUtilities = $skilling_utilities;
    $this->currentClass = $currentClass;
    $this->configFactory = $configFactory;
    $this->nidBag = $nidBag;
    $this->parser = $parser;
    $this->filterInputService = $filterInputService;
  }

  /**
   * Change submission add/edit form for students.
   *
   * Make it suitable for floating in a separate window.
   *
   * @param array $form
   *   Form array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingClassRoleException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function changeFloatingSubmissionForm(array &$form) {
    // Is the form request from a floating submission form?
    $floater = FALSE;
    $request = Drupal::request();
    $query = $request->query;
    if ($query->has('floater')) {
      $floater = $query->getBoolean('floater');
    }
    if ($floater) {
      // Clear status messages from previous operations.
      $this->skillingUtilities->clearMessages();
      // The link to open the form was embedded in a lesson (or other entity).
      // This should only happen for students.
      if (!$this->currentUser->isStudent()) {
        throw new SkillingClassRoleException(
          'Not a student', __FILE__, __LINE__
        );
      }
      // Is it an add?
      $submissionOperation = NULL;
      $path = $request->getPathInfo();
      if ($path === '/node/add/submission') {
        $submissionOperation = 'add';
        // There is no submission id yet.
        $submissionNid = 0;
        // Validate data. If there are errors, an access denied exception
        // will be thrown.
        /** @var \Drupal\node\Entity\Node $exercise */
        list($exercise, $version, $submissionTitle) = $this->validateAddSubmissionLink($query);
        // Setup form fields.
        $form['title']['widget'][0]['value']['#default_value'] = $submissionTitle;
        $form['field_user']['widget'][0]['target_id']['#default_value'] = $this->currentUser->getDrupalUser();
        $form['field_version']['widget'][0]['value']['#default_value'] = $version;
        $form['field_exercise']['widget']['#default_value'] = [$exercise->id()];
        $form['field_complete']['widget']['#default_value'] = FALSE;
        // Add hidden field tracking whether user gave difficulty rating.
      }
      else {
        // Is it an edit?
        preg_match('/^\/node\/(\d+)\/edit/', $path, $match);
        if (isset($match[1]) && is_numeric($match[1])) {
          // Edit. Remember submission id.
          $submissionOperation = 'edit';
          $submissionNid = $match[1];
          /** @var \Drupal\node\Entity\Node $exercise */
          list($exercise, $version) = $this->validateEditSubmissionLink($submissionNid);
        }
        else {
          // Not valid URL.
          throw new SkillingException(
            'Bad edit URL', __FILE__, __LINE__
          );
        }
      }
      if ($exercise->field_challenge->value) {
        $moduleUrl = $this->skillingUtilities->getModuleUrlPath();
        $challengeImageUrl = $moduleUrl . 'images/challenge.png';
        // Separate out the Challenge label for translation.
        /** @noinspection HtmlUnknownTarget */
        $formTitle = $this->t(
          "Exercise: @e <img src='@challengeUrl' alt='@challengeLabel'> (attempt: @v)",
          [
            '@e' => $exercise->getTitle(),
            '@challengeUrl' => $challengeImageUrl,
            '@challengeLabel' => 'Challenge',
            '@v' => $version,
          ]
        );
      }
      else {
        $formTitle = $this->t(
          'Exercise: @e (attempt: @v)',
          [
            '@e' => $exercise->getTitle(),
            '@v' => $version,
          ]
        );
      }
      $form['instructions'] = [
        '#theme' => 'floating_submission_instructions',
        '#form_title' => $formTitle,
        '#version' => $version,
      ];
      // Turn off submission widgets according to config.
      // Check settings to see if allowed.
      $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
      $showTextField = $settings->get(SkillingConstants::SETTING_KEY_SUBMISSION_TEXT_WIDGET);
      $showUploadField = $settings->get(SkillingConstants::SETTING_KEY_SUBMISSION_UPLOAD_WIDGET);
      $requireInitials = $settings->get(SkillingConstants::SETTING_KEY_SUBMISSION_REQUIRE_INITIALS);
      // Need at least one.
      if (!$showTextField && !$showUploadField) {
        // Do nothing. Leave both fields enabled.
        // Configuration code should not allow this to happen.
      }
      else {
        // Hide fields as needed.
        if (!$showTextField) {
          $form['field_solution']['#access'] = FALSE;
        }
        if (!$showUploadField) {
          $form['field_submitted_files']['#access'] = FALSE;
        }
      }
      $form['title']['#access'] = FALSE;
      $form['field_user']['#access'] = FALSE;
      $form['field_initials']['#access'] = $requireInitials;
      $form['field_version']['#access'] = FALSE;
      $form['field_exercise']['#access'] = FALSE;
      $form['field_complete']['#access'] = FALSE;
      $form['field_feedback_message']['#access'] = FALSE;
      $form['field_feedback_responses']['#access'] = FALSE;
      $form['field_feedback_source']['#access'] = FALSE;
      $form['field_notes']['#access'] = FALSE;
      $form['field_overall_evaluation']['#access'] = FALSE;
      $form['field_when_feedback_given']['#access'] = FALSE;
      $form['revision_log']['#access'] = FALSE;
      $form['revision']['#access'] = FALSE;
      $form['revision_information']['#access'] = FALSE;
      $form['advanced']['#access'] = FALSE;
      skilling_add_floating_submission_js_settings(
        $form, $submissionOperation, $exercise->id(), $submissionNid);
    }
  }

  /**
   * Validate submission link params.
   *
   * Validate params sent from submission link for editing a submission
   * from a student.
   *
   * @param int $submissionId
   *   Nid of the submission.
   *
   * @return array
   *   Result of validation.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  protected function validateEditSubmissionLink($submissionId) {
    $submission = $this->checkSubmission($submissionId);
    // Get the submission's exercise nid.
    $exerciseId = $submission->get('field_exercise')->target_id;
    // Check that it's an exercise.
    $exercise = $this->skillingUtilities->getExerciseWithId($exerciseId);
    /* @noinspection PhpUndefinedFieldInspection */
    $version = $submission->field_version->value;
    if (!$version) {
      throw new SkillingValueMissingException(
        Html::escape('Submission: ' . $submissionId . ' Missing version'),
        __FILE__, __LINE__
      );
    }
    if (!is_numeric($version)) {
      throw new SkillingWrongTypeException(
        Html::escape('Submission: ' . $submissionId
          . ' Bad version: ' . $version),
        __FILE__, __LINE__
      );
    }
    return [$exercise, $version];
  }

  /**
   * Check that a node is a submission, published, owned by the current user.
   *
   * @param int $nid
   *   The nid of the submission to check.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function checkSubmission($nid) {
    if (!$nid || !is_numeric($nid)) {
      throw new SkillingInvalidValueException(
        'Bad nid: ' . $nid, __FILE__, __LINE__
      );
    }
    /* @var \Drupal\node\Entity\Node $node */
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!$node) {
      return NULL;
    }
    // Right bundle?
    if ($node->bundle() !== SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
      throw new SkillingException(
        'For node ' . $nid . ', expected submission, got '
        . $node->bundle(), __FILE__, __LINE__
      );
    }
    // Published?
    if (!$node->isPublished()) {
      throw new SkillingException(
        'Node not published: ' . $nid, __FILE__, __LINE__
      );
    }
    // Belongs to current user?
    if ($node->getOwnerId() !== $this->currentUser->id()) {
      throw new SkillingException(
        'Submission not owned by current user: ' . $nid, __FILE__, __LINE__
      );
    }
    return $node;
  }

  /**
   * Compute the title of a new submission.
   *
   * @param string $accountName
   *   User account name.
   * @param \Drupal\node\Entity\Node $exercise
   *   Exercise the submission is for.
   * @param int $version
   *   Submission version.
   *
   * @return string
   *   Submission title.
   */
  public function computeNewSubmissionTitle($accountName, Node $exercise, $version) {
    $title = t(
      'Submission. Student: @account Exercise: @exercise Version: @version',
      [
        '@account' => $accountName,
        '@exercise' => $exercise->getTitle(),
        '@version' => $version,
      ]
    );
    return $title;
  }

  /**
   * Validate params sent from link for adding a new submission from a student.
   *
   * @param \Symfony\Component\HttpFoundation\ParameterBag $queryParams
   *   The parameters to check.
   *
   * @return array
   *   Validation result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function validateAddSubmissionLink(ParameterBag $queryParams) {
    // Make an entity type manager.
    // Load current user service.
    /* @var \Drupal\skilling\Utilities $utilities */
    $utilities = Drupal::service('skilling.utilities');
    // Add must specify exercise.
    if (!$queryParams->has('exercisename')) {
      throw new SkillingValueMissingException(
        'Exercise not given', __FILE__, __LINE__
      );
    }
    $exerciseInternalName = trim($queryParams->get('exercisename'));
    // Load that exercise.
    /* @var \Drupal\node\Entity\Node $exercise */
    $exercise = $utilities->getPublishedExerciseWithInternalName($exerciseInternalName);
    $exerciseId = $exercise->id();
    // Must give version.
    if (!$queryParams->has('version')) {
      throw new SkillingValueMissingException(
        'No version', __FILE__, __LINE__
      );
    }
    if (!is_numeric($queryParams->get('version'))) {
      throw new SkillingWrongTypeException(
        'Version not numeric', __FILE__, __LINE__
      );
    }
    $version = $queryParams->getDigits('version');
    if ($version < 1) {
      throw new SkillingException(
        'Version less than 1: ' . $version, __FILE__, __LINE__
      );
    }
    // Is there already a submission with this version?
    $submissions = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'submission')
      // With the right exercise.
      ->condition('field_exercise.target_id', $exerciseId)
      // For current user.
      ->condition('uid', $this->currentUser->id())
      // With the given version.
      ->condition('field_version', $version)
      ->execute();
    if (count($submissions) > 0) {
      throw new SkillingException(
        'Sub with ver ' . $version . ' exists', __FILE__, __LINE__
      );
    }
    // What should the title of the submission be?
    $title = $this->computeNewSubmissionTitle(
      $this->currentUser->getAccountName(), $exercise, $version
    );
    $submissions = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('title', $title)
      ->execute();
    if (count($submissions) > 0) {
      throw new SkillingException(
        'Sub with computed title exists: ' . $title, __FILE__, __LINE__
      );
    }
    return [$exercise, $version, $title];
  }

  /**
   * Show submission status and links.
   *
   * Make a render array to show submission status, and submission links
   * for an exercise for students. The rendered data shows when
   * inserting an exercise into lessons, and directly on exercise pages.
   *
   * @param \Drupal\node\Entity\Node $exercise
   *   The exercise to make submission links for.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function createSubmissionLinks(Node $exercise) {
    if ($exercise->bundle() !== SkillingConstants::EXERCISE_CONTENT_TYPE) {
      return [];
    }
    // Drop the exercise's nid into a bag, in case other code wants to
    // know that the exercise was inserted here.
    $this->nidBag->addToBag(
      SkillingConstants::NID_POCKET_EXERCISES,
      $exercise->id()
    );
    // If exercise not published, return nothing.
    if (!$exercise->isPublished()) {
      return [];
    }
    // Compute data needed to render links for student to submit solution.
    $submissionLinks = $this->computeSubmissionLinkData($exercise);
    return $submissionLinks;
  }

  /**
   * Compute data needed to render links for student to submit solution.
   *
   * @param \Drupal\node\Entity\Node $exercise
   *   The exercise.
   *
   * @return array
   *   Link data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  protected function computeSubmissionLinkData(Node $exercise) {
    // Is a student?
    if (!$this->currentUser->isStudent()) {
      return [
        'result' => 'not_student',
      ];
    }
    // Get submissions for this student to this exercise.
    // This might include unpublished submissions.
    $submissionIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'submission')
      // For the current user.
      ->condition('field_user', $this->currentUser->id())
      // The exercise in question.
      ->condition('field_exercise.target_id', $exercise->id())
      ->sort('field_version')
      ->execute();
    // Are there any submissions?
    if (count($submissionIds) === 0) {
      // No submissions. Need a link to create a submission.
      /* @noinspection PhpUndefinedFieldInspection */
      return [
        'result' => 'first_submission_link',
        'exercise_name' => $exercise->field_internal_name->value,
        'exercise_nid' => $exercise->id(),
        'version' => 1,
      ];
    }
    // Load the submissions.
    $submissions = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($submissionIds);
    // Any unpublished?
    $unpublished = FALSE;
    /** @var \Drupal\node\Entity\Node $submission */
    foreach ($submissions as $submission) {
      if (!$submission->isPublished()) {
        $unpublished = TRUE;
        break;
      }
    }
    if ($unpublished) {
      // At least one submission is unpublished. Tell the user,
      // so s/he knows something is up.
      return [
        'result' => 'unpublished_submission',
      ];
    }
    // Has any of the submissions been marked complete?
    // If any one of them has, then the exercise has been completed.
    $complete = FALSE;
    foreach ($submissions as $submission) {
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_complete->value) {
        $complete = TRUE;
        break;
      }
    }
    // What is the highest version submitted?
    $highestVersion = 0;
    foreach ($submissions as $submission) {
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_version->value > $highestVersion) {
        /* @noinspection PhpUndefinedFieldInspection */
        $highestVersion = $submission->field_version->value;
      }
    }
    // Are any submissions waiting feedback?
    // If so, cannot submit a new solution.
    $waitingForFeedback = FALSE;
    foreach ($submissions as $submission) {
      /* @noinspection PhpUndefinedFieldInspection */
      if (!$submission->field_when_feedback_given->value) {
        $waitingForFeedback = TRUE;
        break;
      }
    }
    // Start building the result.
    $submissionSummaries = [];
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionSummaries['exercise_name'] = $exercise->field_internal_name->value;
    $submissionSummaries['exercise_nid'] = $exercise->id();
    $submissionSummaries['complete'] = $complete;
    $submissionSummaries['complete_badge_url'] = $this->skillingUtilities->getModuleUrlPath()
      . SkillingConstants::PATH_TO_COMPLETE_BADGE;
    $submissionSummaries['highest_version_submitted'] = $highestVersion;
    $submissionSummaries['waiting_for_feedback'] = $waitingForFeedback;
    // Get the student's status for the exercise.
    // Get exercise dues for the current class.
//    list($classExerciseDues, $classExerciseNids)
//      = $this->currentClass->getExerciseDueRecords();
    // Find the exercise due data for the exercise.
//    $exerciseDue = NULL;
//    /** @var \Drupal\skilling\ExerciseDueRecord $record */
//    foreach ($classExerciseDues as $record) {
//      if ($record->getExerciseId() == $exercise->id()) {
//        $exerciseDue = $record;
//        break;
//      }
//    }
    $submissionStatus = $this->getSubmissionStatusForStudentExercise(
      $this->currentUser,
      (int)$exercise->id(),
      $this->currentClass->getWrappedSkillingClass()
    );
    $submissionSummaries['submission_status'] = $submissionStatus;
    // Create submission data for each submission.
    $submissionSummaries['submissions'] = [];
    /* @var Node $submission */
    foreach ($submissions as $submission) {
      // Data for one submission.
      $submissionSummary = [];
      $submissionSummary['submission_id'] = $submission->id();
      /* @noinspection PhpUndefinedFieldInspection */
      $submissionSummary['version'] = $submission->field_version->value;
      $submissionSummary['complete'] = $submission->field_complete->value;
      $submissionSummary['waiting_for_feedback']
        = !$submission->field_when_feedback_given->value ? TRUE : FALSE;
      $submissionSummary['when_created'] = date('M j, Y, h:i a', $submission->getCreatedTime());
      /* @noinspection PhpUndefinedFieldInspection */
      $solution = $submission->field_solution->value;
      if ($solution) {
        $solution = $this->parser->parse($solution, $submission);
      }
      $submissionSummary['solution'] = $solution;
      // Make file links.
      $fileLinks = [];
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_submitted_files) {
        /* @noinspection PhpUndefinedFieldInspection */
        foreach ($submission->field_submitted_files->getValue() as $fileFieldValue) {
          $fileId = $fileFieldValue['target_id'];
          $file = File::load($fileId);
          $fileName = $file->getFilename();
          $fileInternalUri = $file->getFileUri();
          $fileUrl = file_create_url($fileInternalUri);
          $fileLinks[] = [
            'name' => $fileName,
            'url' => $fileUrl,
          ];
        }
      }
      $submissionSummary['files'] = $fileLinks;
      // Make grader attachment links.
      $graderAttachmentLinks = [];
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_grader_attachments) {
        /* @noinspection PhpUndefinedFieldInspection */
        foreach ($submission->field_grader_attachments->getValue() as $fileFieldValue) {
          $fileId = $fileFieldValue['target_id'];
          $file = File::load($fileId);
          $fileName = $file->getFilename();
          $fileInternalUri = $file->getFileUri();
          $fileUrl = file_create_url($fileInternalUri);
          $graderAttachmentLinks[] = [
            'name' => $fileName,
            'url' => $fileUrl,
          ];
        }
      }
      $submissionSummary['grader_attachments'] = $graderAttachmentLinks;
      $isFeedbackGiven = FALSE;
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_when_feedback_given->value) {
        $isFeedbackGiven = TRUE;
      }
      $submissionSummary['is_feedback_given'] = $isFeedbackGiven;
      if ($isFeedbackGiven) {
        $feedback = $submission->field_feedback_message->value;
        // Filter.
        $feedback = $this->filterInputService->filterUserContent($feedback);
        $submissionSummary['feedback'] = $feedback;
      }
      // Add summary to list.
      $submissionSummaries['submissions'][] = $submissionSummary;
    } // End for each submission.
    return $submissionSummaries;
  }

  /**
   * Get published submissions for published exercises for user with given uid.
   *
   * @param int $uid
   *   The user's id.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPublishedSubmissionsForUid($uid) {
    $submissionIds = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
      // Submission is published.
      ->condition('status', TRUE)
      // For the user.
      ->condition('field_user', $uid)
      // Exercise is published.
      ->condition('field_exercise.entity.status', 1)
      ->execute();
    $submissions = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($submissionIds);
    return $submissions;
  }

  /**
   * Extract the exercise nids from an array of submissions.
   *
   * @param array $submissions
   *   The submissions.
   *
   * @return array
   *   The exercise nids.
   *
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function extractExerciseNidsFromSubmissions(array $submissions) {
    $exerciseNids = [];
    /** @var \Drupal\node\Entity\Node $submission */
    foreach ($submissions as $submission) {
      // Check type.
      if ($submission->bundle() !== SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
        throw new SkillingWrongTypeException(
          'Expected submission, got ' . $submission->bundle(),
          __FILE__, __LINE__
        );
      }
      $exerciseNidSet = isset($submission->get('field_exercise')->target_id);
      if (!$exerciseNidSet) {
        throw new SkillingValueMissingException(
          'Missing exercise for submission ' . $submission->id(),
          __FILE__, __LINE__
        );
      }
      if (!$submission->get('field_exercise')->target_id) {
        throw new SkillingValueMissingException(
          'Missing exercise for submission' . $submission->id(),
          __FILE__, __LINE__
        );
      }
      $exerciseNids[] = $submission->get('field_exercise')->target_id;
    }
    $exerciseNids = array_unique($exerciseNids);
    return $exerciseNids;
  }

  /**
   * Get the status of all submissions from a student.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   *
   * @return array
   *   Submission statuses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getSubmissionStatusesForStudent(SkillingUser $student) {
    // Get published submissions for published exercises for the student.
    $submissions = $this->getPublishedSubmissionsForUid($student->id());
    $exerciseNids = $this->extractExerciseNidsFromSubmissions($submissions);
//    list($classExerciseDues, $classExerciseNids)
//      = $this->currentClass->getExerciseDueRecords();
    $statuses = $this->getSubmissionStatusForStudentExercisesSubmissions(
      $student,
      $exerciseNids,
//      $submissions,
      $this->currentClass->getWrappedSkillingClass()
    );
    return $statuses;
  }

  /**
   * Get status of submissions from a student for a list of exercises.
   *
   * The list may include exercises that have no submissions, and
   * there may be submissions for exercises that are not in the list.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   * @param array $exerciseNids
   *   The exercise.
   * @param \Drupal\skilling\SkillingClass\SkillingClass $class
   *
   * @return array
   *   Statuses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function getSubmissionStatusForStudentExercisesSubmissions(
      SkillingUser $student,
      array $exerciseNids,
//      array $submissions,
      SkillingClass $class) {
    $statuses = [];
//    list($exerciseDues, $ignoreMe) = $class->getExerciseDueRecords();
    foreach ($exerciseNids as $exerciseNid) {
//      // Find the exercise due data for the exercise.
//      $exerciseDue = NULL;
//      foreach ($exerciseDues as $record) {
//        if ($record['exerciseId'] == $exerciseNid) {
//          $exerciseDue = $record;
//          break;
//        }
//      }
//      // $exerciseDue could be null, if there is a submission for an exercise
//      // that is not on the timeline for a course.
      $statuses[$exerciseNid] = $this->getSubmissionStatusForStudentExercise(
        $student, $exerciseNid, $class
      );
    }
    return $statuses;
  }

  /**
   * Get the status of submissions from a student for an exercise.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   * @param int $exerciseNid
   *   The exercise nid.
   * @param $class
   *
   * @return string
   *   Status. Values are defined in SkillingConstants.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function getSubmissionStatusForStudentExercise(
      SkillingUser $student,
      $exerciseNid,
      SkillingClass $class
  ) {
    // Is the user a student?
    if (!$student->isStudent()) {
      throw new SkillingException(
        Html::escape('User is not a student.'), __FILE__, __LINE__
      );
    }
    // Is a class given?
    if (!$class) {
      throw new SkillingException(
        Html::escape('No class given.'), __FILE__, __LINE__
      );
    }
    $submissions = $this->getPublishedSubmissionsForUid($student->id());
    // Find the highest version submission for the exercise.
    $maxVersion = 0;
    // The submission with the highest version.
    $lastSubmission = NULL;
    // Was the exercise completed?
    $complete = FALSE;
    $submissionCountForExercise = 0;
    foreach ($submissions as $submission) {
      // Make sure the exercise reference is there.
      if (!isset($submission->field_exercise->target_id)
        || !$submission->field_exercise->target_id) {
        throw new SkillingValueMissingException(
          Html::escape('Exercise missing. Submission:' . $submission->id()),
          __FILE__, __LINE__
        );
      }
      // Is this submission for the target exercise?
      if ($submission->field_exercise->target_id == $exerciseNid) {
        // Is the submission for the right student?
        $submissionStudentUid = $submission->field_user->target_id;
        if ($submissionStudentUid == $student->id()) {
          $submissionCountForExercise ++;
          $version = $submission->field_version->value;
          // Is this version higher than the previous highest?
          if ($version > $maxVersion) {
            $maxVersion = $version;
            $lastSubmission = $submission;
          }
          // Was the exercise completed?
          if ($submission->field_complete->value) {
            $complete = TRUE;
          }
        }
      }
    }
    if (is_null($lastSubmission)) {
      // No submissions for the exercise.
      $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION;
    }
    else {
      // There's at least one submission for the exercise.
      // Is there feedback for the submission?
      $feedbackDate = $lastSubmission->field_when_feedback_given->value;
      if (!$feedbackDate) {
        // No feedback yet.
        $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK;
      }
      else {
        // There is feedback.
        // Was the exercise completed?
        if ($complete) {
          $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_COMPLETE;
        }
        else {
          // Get max submissions allowed for the exercise for the class.
          $maxAllowed = $class->getMaxSubmissionsAllowedForExercise($exerciseNid);
          if ($submissionCountForExercise >= $maxAllowed) {
            $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_MAX_SUBMISSIONS_REACHED;
          }
          else {
            $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_NOT_COMPLETE;
          }
        }
      } // End there is feedback.
    } // End there is a submission.
    return $exerciseSubmissionStatus;
  }


  /**
   * Get the details of all submissions from a student.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   *
   * @return array
   *   Submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getSubmissionDetailsForStudent(SkillingUser $student) {
    // Get published submissions for published exercises for the student.
    $submissions = $this->getPublishedSubmissionsForUid($student->id());
    $exerciseNids = $this->extractExerciseNidsFromSubmissions($submissions);
    $details = $this->getSubmissionDetailsForStudentExercisesSubmissions(
      $student, $exerciseNids, $submissions
    );
    return $details;
  }

  /**
   * Get the number of exercises a student has completed.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   *
   * @return int
   *   The count.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCountCompletedExercisesForStudent(SkillingUser $student) {
    // Get published submissions for published exercises for the student.
    /** @var \Drupal\node\NodeInterface[] $submissions */
    $submissions = $this->getPublishedSubmissionsForUid($student->id());
    $count = 0;
    foreach($submissions as $submission) {
      $completedFieldValue = $submission->get(SkillingConstants::FIELD_COMPLETE);
      $completed = $completedFieldValue->first()->getValue()['value'];
      if ($completed) {
        $count++;
      }
    }
    return $count;
  }

  /**
   * Get the number of challenge exercises a student has completed.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   *
   * @return int
   *   The count.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getCountCompletedChallengeExercisesForStudent(SkillingUser $student) {
    // Get published submissions for published exercises for the student.
    /** @var \Drupal\node\NodeInterface[] $submissions */
    $submissions = $this->getPublishedSubmissionsForUid($student->id());
    $exerciseNids = $this->extractExerciseNidsFromSubmissions($submissions);
    $exercises = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($exerciseNids);


    $count = 0;
    foreach($submissions as $submission) {
      $completedFieldValue = $submission->get(SkillingConstants::FIELD_COMPLETE);
      $completed = $completedFieldValue->first()->getValue()['value'];
      if ($completed) {
        // Is the associated exercise a challenge exercise?
        $exerciseNid = $submission->field_exercise->target_id;
        $challenge = $exercises[$exerciseNid]->field_challenge->value;
        if ($challenge) {
          $count++;
        }
      }
    }
    return $count;
  }



  /**
   * Get details of submissions from a student.
   *
   * Get details of submissions from a student for a list of exercises.
   * The list may include exercises that have no submissions, and
   * there may be submissions for exercises that are not in the list.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   * @param array $exerciseNids
   *   Exercise nids.
   * @param array $submissions
   *   Submissions.
   *
   * @return array
   *   Submission details.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getSubmissionDetailsForStudentExercisesSubmissions(
    SkillingUser $student,
    array $exerciseNids, array
    $submissions
  ) {
    $details = [];
    foreach ($exerciseNids as $exerciseNid) {
      $details[$exerciseNid] = $this->getSubmissionDetailsForStudentExercise(
        $student, $exerciseNid, $submissions
      );
    }
    return $details;
  }

  /**
   * Get the details of submissions from a student for an exercise.
   *
   * @param \Drupal\skilling\SkillingUser $student
   *   The student.
   * @param int $exerciseNid
   *   The exercise nid.
   * @param \Drupal\node\Entity\Node[] $submissions
   *   Submissions, could include submissions from other students and exercises.
   *
   * @return array
   *   Submissions. Format:
   *   [
   *     'complete' => bool, whether there is at least one complete,
   *     'submissions' => submission details
   *   ]
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getSubmissionDetailsForStudentExercise(
    SkillingUser $student,
    $exerciseNid,
    array $submissions
  ) {
    // Is the user a student?
    if (!$student->isStudent()) {
      throw new SkillingException(
        Html::escape('User is not a student.'), __FILE__, __LINE__
      );
    }
    // Deets of all submissions for the exercise.
    $allSubmissionDeets = [];
    $yes = t('Yes');
    $no = t('No');
    $dateTimeFormat = 'M j, g:i a';
    /* @var Node $submission */
    foreach ($submissions as $submission) {
      // Is this submission for the target exercise?
      /* @noinspection PhpUndefinedFieldInspection */
      if ($submission->field_exercise->target_id == $exerciseNid) {
        // Is the submission for the right student?
        /* @noinspection PhpUndefinedFieldInspection */
        $submissionStudentUid = $submission->field_user->target_id;
        if ($submissionStudentUid == $student->id()) {
          // Deets for one submission.
          $whenSubmitted = date($dateTimeFormat, $submission->getCreatedTime());
          /* @noinspection PhpUndefinedFieldInspection */
          $whenFeedbackValue = $submission->field_when_feedback_given->value;
          if (!$whenFeedbackValue) {
            $whenFeedbackDisplay = '';
          }
          else {
            $whenFeedbackDisplay = date(
              $dateTimeFormat,
              strtotime($whenFeedbackValue)
            );
          }
          /* @noinspection PhpUndefinedFieldInspection */
          $feedback = $submission->field_feedback_message->value;
          if ($feedback) {
            // Filter.
            $feedback = $this->filterInputService->filterUserContent($feedback);
          }
          /* @noinspection PhpUndefinedFieldInspection */
          $submissionDeets = [
            'studentUid' => $submissionStudentUid,
            'exerciseNid' => $exerciseNid,
            'whenSubmitted' => $whenSubmitted,
            'version' => $submission->field_version->value,
            'whenFeedbackGiven' => $whenFeedbackDisplay,
            'feedback' => $feedback,
            'complete' => $submission->field_complete->value ? $yes : $no,
          ];
          $allSubmissionDeets[] = $submissionDeets;
        } //End for right student.
      } //End for right exercise.
    } //End loop across submissions.
    if (count($allSubmissionDeets) > 1) {
      // Sort the submissions.
      usort($allSubmissionDeets, function ($a, $b) {
        if ($a['version'] == $b['version']) {
          return 0;
        }
        return $a['version'] < $b['version'] ? -1 : 1;
      });
    }
    return $allSubmissionDeets;
  }

}
