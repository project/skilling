<?php

namespace Drupal\skilling\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Skilling custom tag item annotation object.
 *
 * @see \Drupal\skilling\Plugin\SkillingCustomTagManager
 * @see plugin_api
 *
 * @Annotation
 */
class SkillingCustomTag extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The tag being interpreted.
   *
   * @var string
   */
  public $tag;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief, human readable, description of the tag type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Whether the tag has a close tag.
   *
   * @var boolean
   */
  public $has_close_tag;

}
