<?php

namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use DateInterval;
use DateTime;
use Drupal\node\NodeInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;

/**
 * Class Timeline.
 */
class Timeline {

  use StringTranslationTrait;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Submissions service.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;

  /**
   * Constructs a new Timeline object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param SkillingCurrentClass $skilling_current_class
   *   Current class service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\skilling\Submissions $submissionsService
   *   Submissions service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentClass $skilling_current_class,
    SkillingCurrentUser $skilling_skilling_current_user,
    Submissions $submissionsService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentClass = $skilling_current_class;
    $this->currentUser = $skilling_skilling_current_user;
    $this->submissionsService = $submissionsService;
  }

  /**
   * Make a timeline for the current user.
   *
   * Include all events from the
   * user's current class. For exercises, submissions for exercises that
   * are not on the calendar do not show.
   *
   * @return array
   *   Form: [problemMessage,eventsForTemplate]
   *   Ready to be wrapped and passed to the Twig template.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   */
  public function makeTimeline() {
    $problemMessage = '';
    $eventsForTemplate = [];
    if ($this->currentUser->isAnonymous()) {
      $problemMessage = $this->t('Timeline only available when logged in.');
      return [$problemMessage, $eventsForTemplate];
    }
    // Get the class.
//    /** @var \Drupal\node\Entity\Node $class */
//    $class = $this->currentClass->getWrappedDrupalClassNode();
//    if (!$class) {
//      $problemMessage = $this->t('No current class.');
//      return [$problemMessage, $eventsForTemplate];
//    }
    // Get the exercise due paragraph items for the class.
    $exerciseDueRecords = $this->currentClass->getExerciseDueRecords();
    $exerciseDuesNids = [];
    foreach ($exerciseDueRecords as $exerciseDueRecord) {
      $exerciseDuesNids[] = $exerciseDueRecord->getExerciseId();
    }
    $exerciseRecords = [];
    $exerciseNids = [];
    // Keep published exercises, adding data about the exercises.
    /** @var NodeInterface[] $allExercises */
    $allExercises = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($exerciseDuesNids);
    foreach ($exerciseDuesNids as $exerciseDuesNid) {
      /** @var NodeInterface $exercise */
      $exercise = $allExercises[$exerciseDuesNid];
      // Does it exist?
      if (!$exercise) {
        continue;
      }
      if ($exercise->isPublished()) {
        $exerciseNids[] = $exerciseDuesNid;
        // Keep exercise due for the exercise.
        /** @var ExerciseDueRecord $rawExerciseDue */
        foreach ($exerciseDueRecords as $rawExerciseDue) {
          if ($rawExerciseDue->getExerciseId() === $exerciseDuesNid) {
            // Compute max submissions for the exercise.
            $exerciseRecord = [];
            $exerciseRecord['exercise_nid'] = $exercise->id();
            $exerciseRecord['title'] = $exercise->getTitle();
            $exerciseRecord['day'] = $rawExerciseDue->getDay();
            $exerciseRecord['max_subs'] = $rawExerciseDue->getMaximumSubmissions();
            $exerciseRecord['exercise_internal_name'] = $exercise->field_internal_name->value;
            $exerciseRecord['exercise_required'] = $rawExerciseDue->getRequired();
            $exerciseRecord['exercise_challenge'] = $exercise->field_challenge->value;
            $exerciseRecord['exercise_submission_status'] = '';
            $exerciseRecords[] = $exerciseRecord;
          }
        }
      }
    }
    // Check submissions for students only.
    if (!$this->currentUser->isStudent()) {
      // For non-students, set special submission status.
      for ($index = 0; $index < count($exerciseRecords); $index++) {
        $exerciseRecord = $exerciseRecords[$index];
        // Does it exist?
        if (!$exerciseRecord) {
          continue;
        }
        $exerciseNid = $exerciseRecord['exercise_nid'];
        // Nid is 0 for non-exercise events.
        if ($exerciseNid !== 0) {
          $exerciseRecords[$index]['exercise_submission_status']
            = SkillingConstants::EXERCISE_SUBMISSION_NA;
        }
      } //End for each event.
    }
    else {
      // Find out the submission status of each exercise.
      // Returns array indexed by exercise nid.
      $exercisesSubmissionSummary = $this->submissionsService
        ->getSubmissionStatusForStudentExercisesSubmissions(
          $this->currentUser,
          $exerciseNids,
          $this->currentClass->getWrappedSkillingClass()
        );
      // Add submission status to event data.
      for ($index = 0; $index < count($exerciseRecords); $index++) {
        // Does it exist?
        if (!$exerciseRecords[$index]['exercise_nid']) {
          continue;
        }
        $exerciseNid = $exerciseRecords[$index]['exercise_nid'];
        if (isset($exercisesSubmissionSummary[$exerciseNid])) {
          $exerciseRecords[$index]['exercise_submission_status']
            = $exercisesSubmissionSummary[$exerciseNid];
        }
      } //End for each event.
    } //End user is a student.
    // Each event will have an HTML link. For exercises, the link
    // will be to the exercise.

    // **** KILL NEXT LOOP ****


    for ($index = 0; $index < count($exerciseRecords); $index++) {
      // Does it exist?
      if (!isset($exerciseRecords[$index])) {
        continue;
      }
      $exerciseRecord = $exerciseRecords[$index];
      // Does it exist?
      if (!$exerciseRecord) {
        continue;
      }
      $exerciseNid = $exerciseRecord['exercise_nid'];
      if (!$exerciseNid || !isset($exercises[$exerciseNid])) {
        // Should never happen.
        continue;
      }
      /* @var \Drupal\node\Entity\Node $exercise */
      /* @noinspection PhpUndefinedVariableInspection */
      $exercise = isset($exercises[$exerciseNid])
        ? $exercises[$exerciseNid] : NULL;
      if (!$exercise) {
        continue;
      }
      if (!$exercise->isPublished()) {
        // Already checked, but just in case.
        continue;
      }
      // Link to exercise, not lesson containing exercise.
      $nidForLink = $exerciseNid;
      $linkedObjectType = 'exercise';
      $exerciseRecords[$index]['linked_object_type'] = $linkedObjectType;
      $exerciseRecords[$index]['linked_nid'] = $nidForLink;
    } // End loop across event items.
    // Work out the max day.
    $lastDay = -666;
    foreach ($exerciseRecords as $exerciseRecord) {
      $eventDay = $exerciseRecord['day'];
      if ($eventDay > $lastDay) {
        $lastDay = $eventDay;
      }
    }
    if ($lastDay === -666) {
      // No exercise records? Set to value that following code won't freak about.
      $lastDay = 1;
    }
    // Sort by day.
    usort($exerciseRecords, function ($a, $b) {
      $aDay = $a['day'];
      $bDay = $b['day'];
      if ($aDay == $bDay) {
        return 0;
      }
      elseif ($aDay > $bDay) {
        return 1;
      }
      else {
        return -1;
      }
    });
    $now = new DateTime();
    // Date the class starts. May be in the middle of a week.
    $classStartDate = new DateTime($this->currentClass->getStartDate());
    // Day of the week the class starts, 0=Sunday.
    $classStartDateDayOfWeek = (int) $classStartDate->format('w');
    // Displayed calendar starts from the beginning of the week
    // that contains the class start date.
    $displayCalendarStartDate = clone $classStartDate;
    $displayCalendarStartDate->sub(
      new DateInterval('P' . $classStartDateDayOfWeek . 'D')
    );
    $displayCalendarEndDate = clone $classStartDate;
    $calendarLength = $lastDay;
    $daysInterval = new DateInterval("P" . $calendarLength . "D");
    $displayCalendarEndDate->add($daysInterval);
    $weeks = [];
    // Loop across events.
    foreach ($exerciseRecords as $exerciseRecord) {
      $eventDay = $exerciseRecord['day'];
      // Days from the beginning of week 1 that the event happens.
      $daysFromDisplayCalendarStart = ($eventDay - 1) + $classStartDateDayOfWeek;
      // Week the event is in, 1 based.
      $eventWeek = (int) ($daysFromDisplayCalendarStart / 7 + 1);
      if (!isset($weeks[$eventWeek])) {
        $weeks[$eventWeek] = [];
      }
      // The event's day of the week. 0 is Sunday.
      $eventDayOfWeek = $daysFromDisplayCalendarStart % 7;
      if (!isset($weeks[$eventWeek][$eventDayOfWeek])) {
        // Compute the components of the event's date for display.
        $interval = new DateInterval('P' . ($eventDay - 1) . 'D');
        $eventDate = clone $classStartDate;
        $eventDate->add($interval);
        $dateDisplayStuff = [
          'month' => $eventDate->format('M'),
          'dow' => $eventDate->format('D'),
          'day' => $eventDate->format('j'),
          'today' => 'no',
          'is_past' => $eventDate < $now,
        ];
        $weeks[$eventWeek][$eventDayOfWeek] = [
          'events' => [],
          'date_display' => $dateDisplayStuff,
        ];
      }
      $weeks[$eventWeek][$eventDayOfWeek]['events'][] = $exerciseRecord;
    }
    //Is today within the range of the calendar?
    if ($now >= $displayCalendarStartDate && $now <= $displayCalendarEndDate) {
      // Add today to the $weeks array.
      // Difference between now and display calendar's start date.
      $diff = $displayCalendarStartDate->diff($now, TRUE);
      // Difference expressed in days.
      $daysDiff = $diff->days;  //(int) $diff->format('%d');
      // The week now is in, 1 based.
      $nowWeek = (int) ($daysDiff / 7 + 1);
      // Now's day of the week, 0=Sunday.
      $nowDayOfWeek = $daysDiff % 7;
      // Is there another event today?
      if (!isset($weeks[$nowWeek])) {
        $weeks[$nowWeek] = [];
      }
      if (!isset($weeks[$nowWeek][$nowDayOfWeek])) {
        // No. Create a new entry in $weeks.
        $dateDisplayStuff = [
          'month' => $now->format('M'),
          'dow' => $now->format('D'),
          'day' => $now->format('j'),
          'today' => 'yes',
        ];
        $weeks[$nowWeek][$nowDayOfWeek] = [
          'events' => [],
          'date_display' => $dateDisplayStuff,
        ];
      }
      else {
        // Other events for today. Mark $weeks entry as for today.
        $weeks[$nowWeek][$nowDayOfWeek]['date_display']['today'] = 'yes';
      }
    }
    ksort($weeks);
    $sortedWeeks = [];
    foreach ($weeks as $key => $row) {
      ksort($row);
      $sortedWeeks[$key] = $row;
    }
    return [$problemMessage, $sortedWeeks];
  }

  /**
   * Format a date for the timeline. E.g.: W Jul 21.
   *
   * @param \DateTime $when
   *   Date to format.
   *
   * @return string
   *   Result.
   */
  protected function formatTimelineDate(DateTime $when) {
    $longDoW = $when->format('D');
    $doWLetter = substr($longDoW, 0, 1);
    $result = $doWLetter . ' ' . $when->format(' M j');
    return $result;
  }

}
