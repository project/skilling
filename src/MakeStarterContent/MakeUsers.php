<?php

namespace Drupal\skilling\MakeStarterContent;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\skilling\SkillingConstants;
use Drupal\user\Entity\User;

/**
 * Make some users for starter content.
 */
class MakeUsers {

  use MessengerTrait;

  const PASSWORD_LENGTH = 32;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Random data maker.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * Flag showing whether code is being run interactively.
   *
   * If it is, the code will generate status messages.
   *
   * @var bool
   */
  protected $interactive = TRUE;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->random = new Random();
  }

  /**
   * Set the interactive flag.
   *
   * @param bool $interactive
   *   Whether the code is being run interactively.
   */
  public function setInteractive($interactive) {
    $this->interactive = $interactive;
  }

  /**
   * Load user 1.
   *
   * @return \Drupal\user\Entity\User
   *   The user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   */
  public function loadAdminUser() {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->entityTypeManager
      ->getStorage('user')
      ->load(1);
    // Does the user exist?
    if (!$user) {
      throw new \Exception('User 1 not found.');
    }
    return $user;
  }

  /**
   * Find existing or make a new user.
   *
   * @param string $name
   *   User name.
   * @param string $password
   *   Password, MT for random.
   * @param string $email
   *   User email.
   * @param string $role
   *   User role.
   * @param string $firstName
   *   User's first name, e.g., Buffy.
   * @param string $lastName
   *   User's last name, e.g., Summers.
   *
   * @return \Drupal\user\Entity\User
   *   The user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeUser($name, $password, $email, $role, $firstName, $lastName) {
    /** @var \Drupal\user\Entity\User $user */
    $user = NULL;
    // Create a user, if one with the right name does not exist.
    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties([
        'name' => $name,
      ]);
    // Does the user exist?
    if (count($users) > 0) {
      // User exists.
      // Get first element of users[].
      $user = reset($users);
      // Make sure user has the given role.
      if ($role) {
        $user->addRole($role);
      }
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Used existing user: @n', ['@n' => $name])
        );
      }
    }
    else {
      // Create a user.
      // See https://docs.acquia.com/tutorials/fast-track-drupal-8-coding/create-users-programmatically
      $user = $this->entityTypeManager
        ->getStorage('user')
        ->create();
      // Mandatory settings.
      $user->setUsername($name);
      if ($password === '') {
        // Random password.
        $password = $this->makePassword();
      }
      $user->setPassword($password);
      $user->enforceIsNew();
      $user->setEmail($email);
      if ($role) {
        $user->addRole($role);
      }
      // Optional settings.
      $language = 'en';
      $user->set("init", 'email');
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      // Custom fields.
      $user->set('field_first_name', $firstName);
      $user->set('field_last_name', $lastName);
      $initials = strtoupper(substr($firstName, 0, 1));
      $initials .= strtoupper(substr($lastName, 0, 1));
      $user->set('field_initials', $initials);
      $user->activate();
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Created new user: @name', ['@name' => $name])
        );
      }
    }
    // Save user.
    $user->save();
    // Erase good/bad messages if not an instructor.
    if ($role !== SkillingConstants::SITE_ROLE_INSTRUCTOR) {
      $user->set('field_good_progress_messages', []);
      $user->set('field_poor_progress_messages', []);
      $user->save();
    }
    return $user;
  }

  /**
   * Make a random password.
   *
   * @return string
   *   Password.
   */
  protected function makePassword() {
    return $this->random->string(self::PASSWORD_LENGTH);
  }

  /**
   * Add grading persona values to a user.
   *
   * @param \Drupal\user\Entity\User $user
   *   User to add values to.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addGradingPersona(User $user) {
    $user->field_feedback_greetings[] = t('Howdy!');
    $user->field_feedback_greetings[] = t('Hi!');
    $user->field_feedback_signatures[] = t('Your pal, Grey');
    $user->field_feedback_signatures[] = t('Your helper, Grey');
    $user->field_feedback_summary_good[] = t('Great!');
    $user->field_feedback_summary_good[] = t('Good job!');
    $user->field_feedback_summary_needs_wor[] = t('Needs work.');
    $user->field_feedback_summary_needs_wor[] = t('Could be improved.');
    $user->field_feedback_summary_poor[] = t('You can do better.');
    $user->field_feedback_summary_poor[] = t('Needs a lot of work.');
    $user->save();
  }

}
