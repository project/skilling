<?php

namespace Drupal\skilling\MakeStarterContent;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\skilling\SkillingConstants;

/**
 * Class for holding and validating an FiB response.
 *
 * @package Drupal\skilling\MakeStarterContent
 */
class FibResponseContainer {
  use StringTranslationTrait;

  /**
   * Type of response, text or number.
   *
   * @var string|null
   */
  protected $responseType = NULL;

  /**
   * Lowest matching number.
   *
   * @var float|null
   */
  protected $lowest = NULL;

  /**
   * Highest matching number.
   *
   * @var float|null
   */
  protected $highest = NULL;

  /**
   * Strings that match this response.
   *
   * @var array|null
   */
  protected $matchingTextResponses = [];

  /**
   * Is the text comparison case-sensitive?
   *
   * @var bool|null
   */
  protected $caseSensitive = NULL;

  /**
   * Is the response correct?
   *
   * @var bool|null
   */
  protected $correct = NULL;

  /**
   * The explanation for this response.
   *
   * @var string|null
   */
  protected $explanation = NULL;

  /**
   * Author notes.
   *
   * @var string|null
   */
  protected $notes = NULL;

  /**
   * Result of the last run of isValid.
   *
   * @var bool|null
   */
  protected $lastCheckValid = NULL;

  /**
   * Error messages from the last run of isValid.
   *
   * @var array
   */
  protected $lastCheckErrorMessages = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new AdminController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Get the response type.
   *
   * @return string
   *   Response type.
   */
  public function getResponseType() {
    return $this->responseType;
  }

  /**
   * Set the response type, if valid.
   *
   * @param string $responseType
   *   Response type.
   */
  public function setResponseType($responseType) {
    $responseType = strtolower(trim($responseType));
    if (in_array($responseType, SkillingConstants::FIB_MATCH_TYPES)) {
      $this->responseType = $responseType;
    }
  }

  /**
   * Get the lowest number in the range for this response.
   *
   * @return float
   *   The lowest number.
   */
  public function getLowest() {
    return $this->lowest;
  }

  /**
   * Set the lowest number in the range for this response.
   *
   * Only sets if the value is numeric.
   *
   * @param float $lowest
   *   The lowest number.
   */
  public function setLowest($lowest) {
    if (is_numeric($lowest)) {
      $this->lowest = (float) $lowest;
    }
  }

  /**
   * Get the highest number in the range for this response.
   *
   * @return float
   *   The highest number.
   */
  public function getHighest() {
    return $this->highest;
  }

  /**
   * Set the highest number in the range for this response.
   *
   * Only sets if the value is numeric.
   *
   * @param float $highest
   *   The highest number.
   */
  public function setHighest($highest) {
    if (is_numeric($highest)) {
      $this->highest = (float) $highest;
    }
  }

  /**
   * Get the text responses that will match.
   *
   * @return array
   *   The responses.
   */
  public function getMatchingTextResponses() {
    return $this->matchingTextResponses;
  }

  /**
   * Set the text responses that will match.
   *
   * @param array $matchingTextResponses
   *   The responses.
   */
  public function setMatchingTextResponses(array $matchingTextResponses) {
    foreach ($matchingTextResponses as $matchingTextResponse) {
      $matchingTextResponse = trim($matchingTextResponse);
      $this->matchingTextResponses[] = $matchingTextResponse;
    }
  }

  /**
   * Are text comparisons case-sensitive?
   *
   * @return bool
   *   True if case-sensitive.
   */
  public function isCaseSensitive() {
    return $this->caseSensitive;
  }

  /**
   * Set whether text comparisons are case-sensitive.
   *
   * @param mixed $caseSensitive
   *   Truthy if case-sensitive.
   */
  public function setCaseSensitive($caseSensitive) {
    $this->caseSensitive = (bool) $caseSensitive;
  }

  /**
   * Is this response correct?
   *
   * @return bool
   *   True if correct.
   */
  public function isCorrect() {
    return $this->correct;
  }

  /**
   * Set whether this response is correct.
   *
   * @param mixed $correct
   *   Truthy if correct.
   */
  public function setCorrect($correct) {
    $this->correct = (bool) $correct;
  }

  /**
   * Get the explanation of the response.
   *
   * @return string
   *   The explanation.
   */
  public function getExplanation() {
    return $this->explanation;
  }

  /**
   * Set the explanation of the response.
   *
   * @param string $explanation
   *   The explanation.
   */
  public function setExplanation($explanation) {
    $this->explanation = $explanation;
  }

  /**
   * Get the author notes for this response.
   *
   * @return string
   *   The notes.
   */
  public function getNotes() {
    return $this->notes;
  }

  /**
   * Set the author notes for this response.
   *
   * @param string $notes
   *   The notes.
   */
  public function setNotes($notes) {
    $this->notes = $notes;
  }

  /**
   * Is this response valid?
   */
  public function isValid() {
    $errorMessages = [];
    if ($this->getResponseType() === SkillingConstants::FIB_MATCH_TYPE_NUMBER) {
      // A number response. Check required fields.
      $lowestOk = TRUE;
      if (is_null($this->getLowest()) || !is_numeric($this->getLowest())) {
        $lowestOk = FALSE;
        $errorMessages[] = $this->t('Lowest number invalid');
      }
      $highestOk = TRUE;
      if (is_null($this->getHighest()) || !is_numeric($this->getHighest())) {
        $highestOk = FALSE;
        $errorMessages[] = $this->t('Highest number invalid');
      }
      // Were lowest and highest both OK?
      if ($lowestOk && $highestOk) {
        // Check the relationship between lowest and highest.
        if ($this->getLowest() > $this->getHighest()) {
          $errorMessages[] = $this->t('Lowest greater than highest.');
        }
      }
    }
    elseif ($this->getResponseType() === 'text') {
      // Text response.
      // Are there any response options to match?
      if (is_null($this->matchingTextResponses) || count($this->matchingTextResponses) === 0) {
        $errorMessages[] = $this->t('No text responses to match.');
      }
      if (is_null($this->isCaseSensitive())) {
        $errorMessages[] = $this->t('Case-sensitive not set.');
      }
    }
    else {
      // Not text or number.
      $errorMessages[] = $this->t('Unknown response type');
    }
    // Check that correct is set.
    if (is_null($this->isCorrect())) {
      $errorMessages[] = $this->t('Correct not set.');
    }
    // What is the result of these checks?
    $this->lastCheckValid = (count($errorMessages) === 0);
    $this->lastCheckErrorMessages = $errorMessages;
    return $this->lastCheckValid;
  }

  /**
   * What did the last run of isValid say?
   *
   * @return null|bool
   *   True if last check said data is valid.
   */
  public function isLastCheckValid() {
    return $this->lastCheckValid;
  }

  /**
   * Get the error messages from the last isValid() call.
   *
   * @return array
   *   Error messages, empty if valid.
   */
  public function getLastCheckErrorMessages() {
    return $this->lastCheckErrorMessages;
  }

  /**
   * Create a paragraph.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $parent
   *   The parent entity.
   *
   * @return null|ParagraphInterface
   *   Paragraph, or null is isValid() has not been run.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createParagraph(ContentEntityInterface $parent) {
    if (is_null($this->isLastCheckValid()) || !$this->isLastCheckValid()) {
      return NULL;
    }
    // Create the entity.
    /** @var \Drupal\paragraphs\ParagraphInterface $responseParagraph */
    $responseParagraph = Paragraph::create(
      ['type' => SkillingConstants::FIB_RESPONSE_PARAGRAPH_TYPE]
    );
    // Set its fields.
    $responseParagraph->set(
      SkillingConstants::FIELD_RESPONSE_TYPE,
      $this->getResponseType()
    );
    if ($this->getResponseType() === SkillingConstants::FIB_MATCH_TYPE_NUMBER) {
      // A number-matching response.
      $responseParagraph->set(
        SkillingConstants::FIELD_LOWEST_NUMBER,
        $this->getLowest()
      );
      $responseParagraph->set(
        SkillingConstants::FIELD_HIGHEST_NUMBER,
        $this->getHighest()
      );
    }
    if ($this->getResponseType() === SkillingConstants::FIB_MATCH_TYPE_TEXT) {
      $matchingTexts = [];
      foreach ($this->getMatchingTextResponses() as $matchingTextResponse) {
        $matchingTexts[] =
          [
            'value' => $matchingTextResponse,
            'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          ];
      }
      $responseParagraph->set(
        SkillingConstants::FIELD_MATCHING_TEXT_RESPONSES,
        $matchingTexts
      );
      $responseParagraph->set(
        SkillingConstants::FIELD_CASE_SENSITIVE,
        $this->isCaseSensitive()
      );
    }
    $responseParagraph->set(
      SkillingConstants::FIELD_CORRECT,
      $this->isCorrect()
    );
    $responseParagraph->set(
      SkillingConstants::FIELD_EXPLANATION,
      [
        'value' => $this->getExplanation(),
        'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
      ]
    );
    $responseParagraph->setParentEntity(
      $parent,
      SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES
    );
    $responseParagraph->isNew();
    $responseParagraph->save();
    return $responseParagraph;
  }

}
