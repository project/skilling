<?php
/* @noinspection PhpUnusedLocalVariableInspection */
namespace Drupal\skilling\MakeStarterContent;

use DateInterval;
use DateTime;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\BookUpdateTracker;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Create starter content.
 */
class MakeStarterContent {
  use StringTranslationTrait;

  /**
   * Service to make users.
   *
   * @var \Drupal\skilling\MakeStarterContent\MakeUsers
   */
  protected $makeUsersService;

  /**
   * Service to make nodes.
   *
   * @var \Drupal\skilling\MakeStarterContent\MakeNodes
   */
  protected $makeNodesService;

  /**
   * Book page order updater service.
   *
   * @var \Drupal\skilling\BookUpdateTracker
   */
  protected $bookPageOrderUpdateService;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Objects created when making starter content.
   */
  protected $adminUser;

  protected $author;
  protected $reviewer;
  protected $student1;
  protected $student2;
  protected $student3;
  protected $student4;
  protected $instructor1;
  protected $instructor2;
  protected $grader1;
  protected $grader2;

  /** @var \Drupal\node\NodeInterface */
  protected $class1;
  /** @var \Drupal\node\NodeInterface */
  protected $class2;

  protected $rubricItemCategoryTermId1;
  protected $rubricItemCategoryTermId2;

  /** @var \Drupal\node\NodeInterface */
  protected $rubricItem1;
  /** @var \Drupal\node\NodeInterface */
  protected $rubricItem2;
  /** @var \Drupal\node\NodeInterface */
  protected $rubricItem3;
  /** @var \Drupal\node\NodeInterface */
  protected $rubricItem4;

  /** @var \Drupal\node\NodeInterface */
  protected $exercise1;
  /** @var \Drupal\node\NodeInterface */
  protected $exercise2;
  /** @var \Drupal\node\NodeInterface */
  protected $exercise3;
  /** @var \Drupal\node\NodeInterface */
  protected $exercise4;

  /** @var \Drupal\node\NodeInterface */
  protected $lesson1;
  /** @var \Drupal\node\NodeInterface */
  protected $lesson1Dot1;
  /** @var \Drupal\node\NodeInterface */
  protected $lesson1Dot2;
  /** @var \Drupal\node\NodeInterface */
  protected $lesson2;
  /** @var \Drupal\node\NodeInterface */
  protected $lesson2Dot1;

  const MAX_SUBS_PER_EXERCISE = 3;

  /**
   * Root of the design book.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $designBook;

  /**
   * Root of the course book.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $courseBook;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\skilling\MakeStarterContent\MakeNodes $makeNodes
   *   Node making service.
   * @param \Drupal\skilling\MakeStarterContent\MakeUsers $makeUsers
   *   User making service.
   * @param \Drupal\skilling\BookUpdateTracker $bookPageOrderUpdateService
   *   Book page order updater service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(
    MakeNodes $makeNodes,
    MakeUsers $makeUsers,
    BookUpdateTracker $bookPageOrderUpdateService,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager
) {
    $this->makeNodesService = $makeNodes;
    $this->makeUsersService = $makeUsers;
    $this->bookPageOrderUpdateService = $bookPageOrderUpdateService;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }


  /**
   * Trigger a batch job that recomputes all completion scores.
   *
   * Scores for all classes. Redirect to submissions admin menu.
   *
   * ARGH! This method cannot call another method to do its work.
   *
   * function calledByRoute() {
   *   $this->bee();
   * }
   * function bee() {
   *   make batch job
   *
   * This FAILS!
   *
   * Must be:
   *
   * function calledByRoute() {
   *   make batch job
   *
   * And don't forget:
   *
   * use DependencySerializationTrait;
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
//  public function createStarterContent($password = '') {
//    $returnUrl = Url::fromRoute('skilling.admin.config.starter_content');
//    // Make a BatchBuilder.
//    $batchBuilder = (new BatchBuilder())
//      ->setTitle($this->t('Make starter content'))
//      ->setErrorMessage($this->t('Problem making starter content.'))
//      ->setFinishCallback([$this, 'doneMakingStarterContent']);
//    // Add operations.
//    $this->adminUser = $this->makeUsersService->loadAdminUser();
//    $batchBuilder->addOperation([$this, 'makeUsers'], [$password]);
//    $batchBuilder->addOperation([$this, 'makeClasses']);
//    $batchBuilder->addOperation([$this, 'enrollUsersInClasses']);
//    // Start the batch job.
//    batch_set($batchBuilder->toArray());
//    return batch_process($returnUrl);
//  }


  /**
   * Create starter content.
   *
   * @param string $password
   *   Password to use for user accounts. If MT, make it random.
   * @param bool $interactive
   *   Whether this function is called interactively.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function createStarterContent($password = '', $interactive = TRUE) {
    // Get user 1.
    $this->adminUser = $this->makeUsersService->loadAdminUser();
    $this->makeUsers($password);
    $this->makeClasses();
    $this->enrollUsersInClasses();
    $this->makePatterns();
    $this->makePrinciples();
    $this->makeModels();
    $this->makeRubricItemCategory();
    $this->makeRubricItems();
    $this->makeExercises();
    $this->makeFibs();
    $this->makeMcqs();
//    $this->makeClassCalendars();
    $this->makeCharacters();
    // Make course book, and lessons.
    $this->courseBook = $this->makeCourse();
    $this->makeSubmissions();
    $this->designBook = $this->makeDesignBook();
    // Update page order in new books.
    $bookIds = [$this->courseBook->id(), $this->designBook->id()];
    $this->bookPageOrderUpdateService->updatePagesBookOrder($bookIds);
  }

  /**
   * Make some users.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeUsers($password) {
    // Make some users.
    $this->author = $this->makeUsersService->makeUser(
      'author',
      $password,
      'author@example.com',
      SkillingConstants::SITE_ROLE_AUTHOR,
      'Auth',
      'Or'
    );
    $this->reviewer = $this->makeUsersService->makeUser(
      'reviewer',
      $password,
      'reviewer@example.com',
      SkillingConstants::SITE_ROLE_REVIEWER,
      'Ree',
      'Viewer'
    );
    $this->student1 = $this->makeUsersService->makeUser(
      'student1',
      $password,
      'student1@example.com',
      SkillingConstants::SITE_ROLE_STUDENT,
      'Stu',
      'Dent1'
    );
    $this->student2 = $this->makeUsersService->makeUser(
      'student2',
      $password,
      'student2@example.com',
      SkillingConstants::SITE_ROLE_STUDENT,
      'Stu',
      'Dent2'
    );
    $this->student3 = $this->makeUsersService->makeUser(
      'student3',
      $password,
      'student3@example.com',
      SkillingConstants::SITE_ROLE_STUDENT,
      'Stu',
      'Dent3'
    );
    $this->student4 = $this->makeUsersService->makeUser(
      'student4',
      $password,
      'student4@example.com',
      SkillingConstants::SITE_ROLE_STUDENT,
      'Stu',
      'Dent4'
    );
    $this->instructor1 = $this->makeUsersService->makeUser(
      'instructor1',
      $password,
      'instructor1@example.com',
      SkillingConstants::SITE_ROLE_INSTRUCTOR,
      'Ins',
      'Tructor1'
    );
    $this->instructor2 = $this->makeUsersService->makeUser(
      'instructor2',
      $password,
      'instructor2@example.com',
      SkillingConstants::SITE_ROLE_INSTRUCTOR,
      'Ins',
      'Tructor2'
    );
    $this->grader1 = $this->makeUsersService->makeUser(
      'grader1',
      $password,
      'grader1@example.com',
      SkillingConstants::SITE_ROLE_GRADER,
      'Grey',
      'Der1'
    );
    $this->grader2 = $this->makeUsersService->makeUser(
      'grader2',
      $password,
      'grader2@example.com',
      SkillingConstants::SITE_ROLE_GRADER,
      'Grey',
      'Der2'
    );
    $this->makeUsersService->addGradingPersona($this->grader1);
    $this->makeUsersService->addGradingPersona($this->grader2);
  }

  /**
   * Make some classes for users to enroll in.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   */
  protected function makeClasses() {
    // Make classes.
    // Work out the start date for class 1.
    $startDate = new DateTime();
    $startDate->sub(new DateInterval('P' . 10 . 'D'));
    $dateFormat = 'D, M j';
    $startDateFormatted = $startDate->format($dateFormat);
    $this->class1 = $this->makeNodesService->makeNode(
      SkillingConstants::CLASS_CONTENT_TYPE,
      $this->t('Class 1'),
      $this->instructor1,
      [
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Class 1 notes'),
        ],
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Class 1 summary'),
          'value' => $this->t('Class 1 deets'),
        ],
        'field_internal_name' => $this->t('class_1'),
        'field_when_starts' => $startDateFormatted,
        'field_maximum_submissions_per_ex' => self::MAX_SUBS_PER_EXERCISE,
      ]
    );
    $startDate = new DateTime();
    $startDate->sub(new DateInterval('P' . 6 . 'D'));
    $startDateFormatted = $startDate->format($dateFormat);
    $this->class2 = $this->makeNodesService->makeNode(
      SkillingConstants::CLASS_CONTENT_TYPE,
      $this->t('Class 2'),
      $this->instructor2,
      [
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Class 2 notes'),
        ],
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Class 2 summary'),
          'value' => $this->t('Class 2 deets'),
        ],
        'field_internal_name' => $this->t('class_2'),
        'field_when_starts' => $startDateFormatted,
        'field_maximum_submissions_per_ex' => self::MAX_SUBS_PER_EXERCISE,
      ]
    );
  }

  /**
   * Enroll users in classes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function enrollUsersInClasses() {
    $student1EnrollClass1 = $this->makeNodesService->makeEnrollmentNode(
      $this->class1, $this->student1, ['student']
    );
    $student2EnrollClass1 = $this->makeNodesService->makeEnrollmentNode(
      $this->class1, $this->student2, ['student']
    );
    $student3EnrollClass2 = $this->makeNodesService->makeEnrollmentNode(
      $this->class2, $this->student3, ['student']
    );
    $student4EnrollClass2 = $this->makeNodesService->makeEnrollmentNode(
      $this->class2, $this->student4, ['student']
    );
    $grader1EnrollClass1 = $this->makeNodesService->makeEnrollmentNode(
      $this->class1, $this->grader1, ['grader']
    );
    $grader2EnrollClass2 = $this->makeNodesService->makeEnrollmentNode(
      $this->class2, $this->grader2, ['grader']
    );
    // Instructor is also the owner of the class node.
    $instructor1EnrollClass1 = $this->makeNodesService->makeEnrollmentNode(
      $this->class1, $this->instructor1, ['instructor']
    );
    $instructor2EnrollClass2 = $this->makeNodesService->makeEnrollmentNode(
      $this->class2, $this->instructor2, ['instructor']
    );
  }

  /**
   * Make some patterns.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makePatterns() {
    $pattern1 = $this->makeNodesService->makeNode(
      SkillingConstants::PATTERN_CONTENT_TYPE,
      $this->t('Pattern 1'),
      $this->author,
      [
        'field_internal_name' => $this->t('pattern_1'),
        'field_summary' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 1 summary.'),
        ],
        'field_situation' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 1 situation.'),
        ],
        'field_action' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 1 action.'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on pattern 1.'),
        ],
      ]
    );
    $pattern2 = $this->makeNodesService->makeNode(
      SkillingConstants::PATTERN_CONTENT_TYPE,
      $this->t('Pattern 2'),
      $this->author,
      [
        'field_internal_name' => $this->t('pattern_2'),
        'field_summary' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 2 summary.'),
        ],
        'field_situation' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 2 situation.'),
        ],
        'field_action' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Pattern 2 action.'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on pattern 2.'),
        ],
      ]
    );

  }

  /**
   * Make some principles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makePrinciples() {
    $principle1 = $this->makeNodesService->makeNode(
      SkillingConstants::PRINCIPLE_CONTENT_TYPE,
      $this->t('Principle 1'),
      $this->author,
      [
        'field_internal_name' => $this->t('principle_1'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Principle 1'),
          'value' => $this->t('Principle 1'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on principle 1.'),
        ],
      ]
    );
    $principle2 = $this->makeNodesService->makeNode(
      SkillingConstants::PRINCIPLE_CONTENT_TYPE,
      $this->t('Principle 2'),
      $this->author,
      [
        'field_internal_name' => $this->t('principle_2'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Principle 2'),
          'value' => $this->t('Principle 2'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on principle 2.'),
        ],
      ]
    );
  }

  /**
   * Make some models.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeModels() {
    $model1 = $this->makeNodesService->makeNode(
      SkillingConstants::MODEL_CONTENT_TYPE,
      $this->t('Model 1'),
      $this->author,
      [
        'field_internal_name' => $this->t('model_1'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Model 1'),
          'value' => $this->t('Model 1'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on model 1.'),
        ],
      ]
    );
    $model2 = $this->makeNodesService->makeNode(
      SkillingConstants::MODEL_CONTENT_TYPE,
      $this->t('Model 2'),
      $this->author,
      [
        'field_internal_name' => $this->t('model_2'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Model 2'),
          'value' => $this->t('Model 2'),
        ],
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $this->t('Some notes on model 2.'),
        ],
      ]
    );
  }

  /**
   * Make a test rubric item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeTestRI() {
    $owner = \Drupal::service('entity_type.manager')->getStorage('user')
      ->load(1);

    $this->rubricItem1 = $this->makeNodesService->makeRubricItemNode(
      $this->t('Test item 1'), $owner,
      [
        [
          'response' => $this->t('Test item 1 response 1'),
          'completes' => TRUE,
        ],
        [
          'response' => $this->t('Test item 1 response 2'),
          'completes' => FALSE,
        ],
      ]
    );
  }

  /**
   * Make rubric item vocab, and some terms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeRubricItemCategory() {
    $vid = 'rubric_item_categories';
    $vname = $this->t('');
    $vdescription = $this->t('');
    // Make the vocabulary.
    $vocabs = Vocabulary::loadMultiple();
    if (!key_exists($vid, $vocabs)) {
      // Create a vocabulary.
      $vocabulary = Vocabulary::create([
        'name' => $vname,
        'description' => $vdescription,
        'vid' => $vid,
        'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      ]);
      $vocabulary->save();
    }
    // Add the terms.
    $this->rubricItemCategoryTermId1 = $this->makeTaxTerm($vid, 'Term1');
    $this->rubricItemCategoryTermId2 = $this->makeTaxTerm($vid, 'Term2');
  }

  /**
   * Make a taxonomy term, or return its id if it exists.
   *
   * @param string $vid
   *   Vocab id
   * @param string $termName
   *   Term
   *
   * @return int
   *   Term id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeTaxTerm($vid, $termName) {
    $term = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $termName, 'vid' => $vid]);
    if (count($term) > 0) {
      $term = reset($term);
      $termId = $term->id();
    }
    else {
      // Create the term.
      $term = Term::create([
          'name' => $termName,
          'vid' => $vid,
          'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
        ]);
      $term->save();
      $termId = $term->id();
    }
    return (integer)$termId;
  }

  /**
   * Make rubric items and their responses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function makeRubricItems() {
    $this->rubricItem1 = $this->makeNodesService->makeRubricItemNode(
      $this->t('Rubric item 1'),
      $this->author,
      $this->rubricItemCategoryTermId1,
      [
        [
          'response' => $this->t('Rubric item 1 response 1'),
          'completes' => TRUE,
        ],
        [
          'response' => $this->t('Rubric item 1 response 2'),
          'completes' => FALSE,
        ],
      ]
    );
    $this->rubricItem2 = $this->makeNodesService->makeRubricItemNode(
      $this->t('Rubric item 2'),
      $this->author,
      $this->rubricItemCategoryTermId2,
      [
        [
          'response' => $this->t('Rubric item 2 response 1'),
          'completes' => TRUE,
        ],
        [
          'response' => $this->t('Rubric item 2 response 2'),
          'completes' => FALSE,
        ],
      ]
    );
    $this->rubricItem3 = $this->makeNodesService->makeRubricItemNode(
      $this->t('Rubric item 3'),
      $this->author,
      $this->rubricItemCategoryTermId1,
      [
        [
          'response' => $this->t('Rubric item 3 response 1'),
          'completes' => TRUE,
        ],
        [
          'response' => $this->t('Rubric item 3 response 2'),
          'completes' => FALSE,
        ],
      ]
    );
    $this->rubricItem4 = $this->makeNodesService->makeRubricItemNode(
      $this->t('Rubric item 4'),
      $this->author,
      $this->rubricItemCategoryTermId2,
      [
        [
          'response' => $this->t('Rubric item 4 response 1'),
          'completes' => TRUE,
        ],
        [
          'response' => $this->t('Rubric item 4 response 2'),
          'completes' => FALSE,
        ],
      ]
    );
  }

  /**
   * Make some exercises.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function makeExercises() {
    $this->exercise1 = $this->makeNodesService->makeExerciseNode(
      $this->t('Exercise 1'),
      $this->t('exercise_1'),
      $this->author,
      [$this->rubricItem1, $this->rubricItem2]
    );
    $this->exercise2 = $this->makeNodesService->makeExerciseNode(
      $this->t('Exercise 2'),
      $this->t('exercise_2'),
      $this->author,
      [$this->rubricItem1, $this->rubricItem3]
    );
    $this->exercise3 = $this->makeNodesService->makeExerciseNode(
      $this->t('Exercise 3'),
      $this->t('exercise_3'),
      $this->author,
      [$this->rubricItem2, $this->rubricItem3]
    );
    $this->exercise4 = $this->makeNodesService->makeExerciseNode(
      $this->t('Exercise 4'),
      $this->t('exercise_4'),
      $this->author,
      [$this->rubricItem3, $this->rubricItem4]
    );
  }

  /**
   * Make a course.
   *
   * @return \Drupal\node\Entity\Node
   *   Root node for course book.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeCourse() {
    $spaces = '&nbsp;&nbsp;';
    $book1 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Course'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on the course'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Course summary'),
          'value' => $this->t('Course deets'),
        ],
      ]
    );
    $this->makeNodesService->makeNewBook($book1);
    // Make some lessons, and add to book.
    $this->lesson1 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 1'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on lesson 1'),
        'body' => [
          'summary' => $this->t('Lesson 1 summary'),
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' =>
$this->t('This is lesson 1.') . "<br />
<br />
" . $this->t('Before you do an exercise, you should read this. The More... button makes you pause, instead of skipping straight to the exercise.') . "<br />
<br />
pause.<br />
<br />
" . $this->t('Here is an exercise.') . "<br />
<br />
exercise.<br />
&nbsp;&nbsp;internal_name=" . $this->t('exercise_1') . "<br />
<br />
pause.<br />
<br />
annotation.<br />
&nbsp;&nbsp;title=Annnnnotate good times, come on!<br />
<br />
&nbsp;&nbsp;This is an annotation, with a custom title.<br />
<br />
/annotation.<br />
<br />
pause.<br />
<br />
" . $this->t('Here is a pattern.') . "<br />
<br />
pattern.<br />
&nbsp;&nbsp;internal_name=" . $this->t('pattern_1') . "<br />
<br />" .
$this->t('Word to search for: exogenous.') . "<br />\n
<br />
",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($this->lesson1, $book1, $book1, 0);
    $this->lesson1Dot1 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 1.1'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on lesson 1.1'),
        'body' => [
          'summary' => $this->t('Lesson 1.1 summary'),
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' =>
$this->t('This is lesson 1.1.') . "<br />
<br />
character.<br />
&nbsp;&nbsp;internal_name=" . $this->t('character_jeremy_smile1') . "<br />
<br />
&nbsp;&nbsp;" . $this->t("Hey, dudes! It's me!") . "<br />
<br />
/character.<br />
<br />" .
$this->t('Here is an exercise.') . "<br />
<br />
exercise.<br />
&nbsp;&nbsp;internal_name=" . $this->t('exercise_2') . "<br />
<br />
Edit this lesson to see some token fu.<br />
<br />
container.<br />
&nbsp;&nbsp;condition=[current-user:is-anonymous]<br />
<br />
&nbsp;&nbsp;You are not logged in. If you were, you'd see your first name here.<br />
<br />
/container.<br />
<br />
container.<br />
&nbsp;&nbsp;condition= ! [current-user:is-anonymous]<br />
<br />
&nbsp;&nbsp;You are logged in. Your first name is [current-user:field_first_name].<br />
<br />
/container.<br />
<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($this->lesson1Dot1, $this->lesson1, $book1, 0);
    $this->lesson1Dot2 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 1.2'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on lesson 1.2'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Lesson 1.2 summary'),
          'value' =>
$this->t('This is lesson 1.2.') . "<br />
<br />" .
$this->t('Here is a principle.') . "<br />
<br />
principle.<br />
&nbsp;&nbsp;internal_name=" . $this->t('principle_1') . "<br />
<br />" .
$this->t('Here is a model.') . "<br />
<br />
model.<br />
&nbsp;&nbsp;internal_name=" . $this->t('model_1') . "<br />
<br />
<br />
Edit this lesson to see some more token fu.<br />
<br />
container.<br />
&nbsp;&nbsp;condition=[current-user:is-student]<br />
<br />
&nbsp;&nbsp;Completed exercises: [current-user:number-exercises-completed]<br />
<br />
{$spaces}annotation.<br />
{$spaces}{$spaces}style=warning<br />
{$spaces}{$spaces}condition=[current-user:number-exercises-completed] < 3<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;You haven't completed enough exercises.<br />
<br />
&nbsp;&nbsp;/annotation.<br />
<br />
/container.<br />
",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($this->lesson1Dot2, $this->lesson1, $book1, 10);
    $this->lesson2 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 2'),
      $this->author,
      [
        'field_notes' => $this->t('some notes'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Lesson summary'),
          'value' =>
$this->t('This is lesson 2.') . "<br />
<br />
container.<br />
&nbsp;&nbsp;condition = [current-user:is-student]<br />
<br />
&nbsp;&nbsp;annotation.<br />
&nbsp;&nbsp;&nbsp;&nbsp;style=warning
&nbsp;&nbsp;&nbsp;&nbsp;condition = ! [exercise:submitted:exercise_2]<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;You should submit exercise 2 before starting this lesson.<br />
<br />
&nbsp;&nbsp;/annotation.<br />
<br />
/container.<br />
<br />
There are a bunch of Skilling tags for making useful learning objects. 
Like diagnostic MCQs. Here's one now!<br />
<br />
mcq.<br />
&nbsp;&nbsp;internal_name=mcq_logical_op_eval_order_if1<br />
<br />
There are fill-in-the-blanks, too.<br />
<br />
fib.<br />
&nbsp;&nbsp;internal_name=" . $this->t('fib_fingers') . "<br />
<br />" .
$this->t('Here is a principle.') . "<br />
<br />
principle.<br />
&nbsp;&nbsp;internal_name=" . $this->t('principle_2') . "<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($this->lesson2, $book1, $book1, 10);
    $this->lesson2Dot1 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 2.1'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on lesson 2.1'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Lesson 2.1 summary'),
          'value' =>
$this->t('This is lesson 2.1.') . "<br />
<br />
annotation.<br />
&nbsp;&nbsp;style=warning<br />
&nbsp;&nbsp;title=Hi there!<br />
<br />
&nbsp;&nbsp;This is a warning, with a custom title.<br />
<br />
/annotation.<br />
<br />
fib.<br />
&nbsp;&nbsp;internal_name=" . $this->t('fib_dogs') . "<br />
<br />" .
$this->t('Here is an exercise.') . "<br />
<br />
exercise.<br />
&nbsp;&nbsp;internal_name=" . $this->t('exercise_3') . "<br />
<br />" .
$this->t('Here is a pattern.') . "<br />
<br />
pattern.<br />
&nbsp;&nbsp;internal_name=" . $this->t('pattern_2') . "<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($this->lesson2Dot1, $this->lesson2, $book1, 0);
    $lesson2Dot2 = $this->makeNodesService->makeNode(
      SkillingConstants::LESSON_CONTENT_TYPE,
      $this->t('Lesson 2.2'),
      $this->author,
      [
        'field_notes' => $this->t('Some notes on lesson 2.2'),
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Lesson 2.2 summary'),
          'value' =>
$this->t('This is lesson 2.2.') . "<br />
<br />
annotation.<br />
&nbsp;&nbsp;style=warning<br />
<br />
&nbsp;&nbsp;" . $this->t('Hep cats approaching!') . "<br />
<br />
/annotation.<br />
<br />
Next is a reflection field. Only students see it.<br />
<br />
reflect.<br />
&nbsp;&nbsp;internal_name=paws<br />
<br />
What do you think about hep cats?<br />
<br />
/reflect.<br />
<br />
" . $this->t('Here is an exercise.') . "<br />
<br />
exercise.<br />
&nbsp;&nbsp;internal_name=" . $this->t('exercise_4') . "<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($lesson2Dot2, $this->lesson2, $book1, 10);
    return $book1;
  }

  /**
   * Make some calendars, and add events.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function makeClassCalendars() {
    // $this->class1 and 2 have classes with start date set.
    // $this->exercise1 to 4 exist.
    $maxClassLengthDays = 50;
    // Make exercise due paragraphs for class 1
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer1Class1Due */
    $exer1Class1Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise1,
      rand(3, $maxClassLengthDays),
      0,
      rand() > 0.5
    );
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer2Class1Due */
    $exer2Class1Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise2,
      rand(3, $maxClassLengthDays),
      2,
      rand() > 0.5
    );
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer3Class1Due */
    $exer3Class1Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise3,
      rand(3, $maxClassLengthDays),
      0,
      rand() > 0.5
    );
    // Make exercise due paragraphs for class 2
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer1Class2Due */
    $exer1Class2Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise1,
      rand(3, $maxClassLengthDays),
      0,
      rand() > 0.5
    );
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer2Class2Due */
    $exer2Class2Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise2,
      rand(3, $maxClassLengthDays),
      2,
      rand() > 0.5
    );
    /** @var \Drupal\paragraphs\Entity\Paragraph $exer4Class2Due */
    $exer4Class2Due = $this->makeNodesService->makeExerciseDueParagraph(
      $this->exercise4,
      rand(3, $maxClassLengthDays),
      0,
      rand() > 0.5
    );
    $class1 = $this->entityTypeManager->getStorage('node')
      ->load($this->class1->id());
    $class1->field_exercises_due[] = [
            'target_id' => $exer1Class1Due->id(),
            'target_revision_id' => $exer1Class1Due->getRevisionId(),
          ];



    // See https://www.drupal.org/project/paragraphs/issues/2707017
//    $current = $class1
//      ->get(SkillingConstants::FIELD_EXERCISES_DUE)->getValue();
//    $current[] = [
//      'target_id' => $exer1Class1Due->id(),
//      'target_revision_id' => $exer1Class1Due->getRevisionId(),
//    ];
//    $current[] = [
//      'target_id' => $exer2Class1Due->id(),
//      'target_revision_id' => $exer2Class1Due->getRevisionId(),
//    ];
//    $current[] = [
//      'target_id' => $exer3Class1Due->id(),
//      'target_revision_id' => $exer3Class1Due->getRevisionId(),
//    ];
//    $class1->set(
//      SkillingConstants::FIELD_EXERCISES_DUE,
//      $current
//    );
    $class1->save();
    $this->class1 = $class1;

    $class2 = $this->entityTypeManager->getStorage('node')
      ->load($this->class2->id());
    $current = $class2
      ->get(SkillingConstants::FIELD_EXERCISES_DUE)->getValue();
    $current[] = [
      'target_id' => $exer1Class2Due->id(),
      'target_revision_id' => $exer1Class2Due->getRevisionId(),
    ];
    $current[] = [
      'target_id' => $exer2Class2Due->id(),
      'target_revision_id' => $exer2Class2Due->getRevisionId(),
    ];
    $current[] = [
      'target_id' => $exer4Class2Due->id(),
      'target_revision_id' => $exer4Class2Due->getRevisionId(),
    ];
    $class2->set(
      SkillingConstants::FIELD_EXERCISES_DUE,
      $current
    );
    $class2->save();
    $this->class2 = $class2;
  }

  /**
   * Make some exercise submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeSubmissions() {
    $submission1 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise1,
      $this->student1,
      $this->t("Student 1's solution to exercise 1"),
      1
    );
    $submission2 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise2,
      $this->student1,
      $this->t("Student 1's solution to exercise 2"),
      1
    );
    $submission3 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise1,
      $this->student2,
      $this->t("Student 2's solution to exercise 1"),
      1
    );
    $submission4 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise2,
      $this->student2,
      $this->t("Student 2's solution to exercise 2"),
      1
    );
    $submission5 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise1,
      $this->student3,
      $this->t("Student 3's solution to exercise 1"),
      1
    );
    $submission6 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise2,
      $this->student3,
      $this->t("Student 3's solution to exercise 2"),
      1
    );
    $submission7 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise1,
      $this->student4,
      $this->t("Student 4's solution to exercise 1"),
      1
    );
    $submission8 = $this->makeNodesService->makeSubmissionNode(
      $this->exercise2,
      $this->student4,
      $this->t("Student 4's solution to exercise 2"),
      1
    );
  }


  /**
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeTestFiB() {
    $owner = \Drupal::service('entity_type.manager')->getStorage('user')
      ->load(1);
    $fib1 = $this->makeNodesService->makeFiB(
      $this->t('Toes on foot'),
      $owner,
      $this->t('fib_toes'),
      $this->t('How many toes do you have on your left foot?'),
      $this->t("Hmmm... that doesn't look right."),
      $this->t('Dogs are cool!'),
      [
        [
          'response_type' => 'number',
          'lowest' => 1,
          'highest' => 4,
          'correct' => FALSE,
          'explanation' => $this->t('Evolution.'),
        ],
        [
          'response_type' => 'number',
          'lowest' => 5,
          'highest' => 5,
          'correct' => TRUE,
          'explanation' => $this->t('Good counting.'),
        ],
      ]
    );
    $fib2 = $this->makeNodesService->makeFiB(
      $this->t('Best dogs'),
      $owner,
      $this->t('fib_best_dogs'),
      $this->t("What is the name of one of the best dogs?"),
      $this->t("Hmmm... I don't know that one."),
      $this->t('Dogs are cool!'),
      [
        [
          'response_type' => 'text',
          'matching' => ['Renata'],
          'case_sensitive' => FALSE,
          'correct' => TRUE,
          'explanation' => $this->t("Aye! That's one."),
        ],
        [
          'response_type' => 'text',
          'matching' => ['Rosie'],
          'case_sensitive' => FALSE,
          'correct' => TRUE,
          'explanation' => $this->t("Rosie is sooo cute!"),
        ],
      ]
    );
    return [
      '#markup' => 'Something happened.',
    ];
  }

  /**
   * Make some fill-in-the-blank questions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  protected function makeFibs() {
    $fib1 = $this->makeNodesService->makeFiB(
      $this->t('Fingers on hand'),
      $this->author,
      $this->t('fib_fingers'),
      $this->t('How many fingers do you have on your left hand?'),
      $this->t("Hmmm... that doesn't look right."),
      $this->t('Dogs are cool!'),
      [
        [
          'response_type' => 'number',
          'lowest' => 1,
          'highest' => 4,
          'correct' => FALSE,
          'explanation' => $this->t('Evolution.'),
        ],
        [
          'response_type' => 'number',
          'lowest' => 5,
          'highest' => 5,
          'correct' => TRUE,
          'explanation' => $this->t('Good counting.'),
        ],
      ]
    );
    $fib2 = $this->makeNodesService->makeFiB(
      $this->t('Dogs'),
      $this->author,
      $this->t('fib_dogs'),
      $this->t("What is the name of one of Kieran's dogs?"),
      $this->t("Hmmm... I don't know that one."),
      $this->t('Dogs are cool!'),
      [
        [
          'response_type' => 'text',
          'matching' => ['Renata'],
          'case_sensitive' => FALSE,
          'correct' => TRUE,
          'explanation' => $this->t("Aye! That's one."),
        ],
        [
          'response_type' => 'text',
          'matching' => ['Rosie'],
          'case_sensitive' => FALSE,
          'correct' => TRUE,
          'explanation' => $this->t("Rosie is sooo cute!"),
        ],
      ]
    );
  }

  /**
   * Make character nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   */
  protected function makeCharacters() {
    $this->makeNodesService->makeCharacter(
      $this->t('Jeremy smile'),
      $this->author,
      $this->t('character_jeremy_smile1'),
      'jeremy-smile.png',
      $this->t('Jeremy'),
      $this->t('Jeremy smiling.')
    );
  }

  /**
   * Make a design book.
   *
   * @return \Drupal\node\Entity\Node
   *   Root node for design book.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeDesignBook() {
    $designBook = $this->makeNodesService->makeNode(
      SkillingConstants::DESIGN_PAGE_CONTENT_TYPE,
      $this->t('Design'),
      $this->author,
      [
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Design summary'),
          'value' => $this->t('Design deets'),
        ],
      ]
    );
    $this->makeNodesService->makeNewBook($designBook);
    // Make some lessons, and add to book.
    $designPage1 = $this->makeNodesService->makeNode(
      SkillingConstants::DESIGN_PAGE_CONTENT_TYPE,
      $this->t('Design page 1'),
      $this->author,
      [
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Design 1 summary'),
          'value' =>
$this->t('This is design page 1.') . "<br />\n" .
"<br />\n" .
$this->t('Word to search for: exogenous.') . "<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($designPage1, $designBook, $designBook, 5);
    $designPage2 = $this->makeNodesService->makeNode(
      SkillingConstants::DESIGN_PAGE_CONTENT_TYPE,
      $this->t('Design page 2'),
      $this->author,
      [
        'body' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'summary' => $this->t('Page summary'),
          'value' => $this->t('This is design page 2.') . "<br />\n",
        ],
      ]
    );
    $this->makeNodesService->addNodeToBook($designPage2, $designBook, $designBook, 10);
    return $designBook;
  }


  /**
   * Make MCQs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function makeMcqs() {
    $this->makeNodesService->makeMcq(
      $this->t('Logical operator eval order in If 1'),
      'mcq_logical_op_eval_order_if1',
      $this->t('What will this code output?') . "<br />
<br />
code.<br />
<br />
let a = 2;<br />
let b = 4;<br />
if (! a == 3 || b == 4) {<br />
&nbsp;&nbsp;console.log(\"Dog\");<br />
}<br />
else {<br />
&nbsp;&nbsp;console.log(\"Llama\");<br />
}<br />
<br />
/code.<br />
",
      [
        [
          'response' => 'Dog',
          'correct' => 1,
          'explanation' => $this->t("Yes, that's it!"),
        ],
        [
          'response' => 'Llama',
          'correct' => 0,
          'explanation' => $this->t("That's not it. Does the ! (not) get applied to a == 3, or to the ||? Review the \"order of logical operations\":/order-logical-ops."),
        ],
        [
          'response' => 'Dog Llama',
          'correct' => 0,
          'explanation' => $this->t("That's not it. Only one of the if statement's branches will run."),
        ],
        [
          'response' => 'Nothing',
          'correct' => 0,
          'explanation' => $this->t("That's not it. One of the if statement's branches will run, so there will be some output."),
        ],
      ],
      $this->author,
      'Here are some notes.'
    );
  }

}
