<?php
namespace Drupal\skilling\MakeStarterContent;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\book\BookManager;
use Drupal\node\NodeInterface;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Exception\SkillingNotFoundException;
use Drupal\skilling\SkillingConstants;
use Drupal\user\Entity\User;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Component\Utility\Html;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\UserInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Make nodes for starter content.
 */
class MakeNodes {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The book manager.
   *
   * @var \Drupal\book\BookManager
   */
  protected $bookManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystemService;

  /**
   * Field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Flag showing whether code is being run interactively.
   *
   * If it is, the code will generate status messages.
   *
   * @var bool
   */
  protected $interactive = TRUE;

  /**
   * Fields that use the Skilling format.
   *
   * If not in this list, long text fields should use the Stripped format.
   *
   * @var array
   */
  protected $skillingFormatFields = [];

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\book\BookManager $book_manager
   *   The book manager.
   * @param \Drupal\skilling\Utilities $utilities
   *   Utilities service.
   * @param \Drupal\Core\File\FileSystem $fileSystemService
   *   File system service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, BookManager $book_manager, SkillingUtilities $utilities, FileSystem $fileSystemService, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->bookManager = $book_manager;
    $this->skillingUtilities = $utilities;
    $this->fileSystemService = $fileSystemService;
    $this->entityFieldManager = $entityFieldManager;
    // Content type/fields that can have the Skilling format.
    $this->skillingFormatFields = [
      ['bundle' => 'page', 'field' => 'body'],
      ['bundle' => 'design_page', 'field' => 'body'],
      ['bundle' => 'exercise', 'field' => 'body'],
      ['bundle' => 'fill_in_the_blank', 'field' => 'field_question'],
      ['bundle' => 'lesson', 'field' => 'body'],
      ['bundle' => 'model', 'field' => 'body'],
      ['bundle' => 'multiple_choice_question', 'field' => 'field_question'],
      ['bundle' => 'pattern', 'field' => 'field_action'],
      ['bundle' => 'pattern', 'field' => 'field_summary'],
      ['bundle' => 'pattern', 'field' => 'field_situation'],
      ['bundle' => 'principle', 'field' => 'body'],
    ];
  }

  /**
   * Set the interactive flag.
   *
   * @param bool $interactive
   *   Whether the code is being run interactively.
   */
  public function setInteractive($interactive) {
    $this->interactive = $interactive;
  }

  /**
   * Make a node. Translate title and field values.
   *
   * Start by looking for a node with the same title of the same content type.
   * If it's found, return that. Otherwise, make a new node.
   *
   * @param string $bundle
   *   Content type.
   * @param string $title
   *   Node title.
   * @param \Drupal\user\Entity\User $user
   *   Node owner.
   * @param array $fieldData
   *   Field names and values.
   *
   * @return \Drupal\node\Entity\Node
   *   The node, either found or made.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeNode($bundle, $title, User $user, array $fieldData = []) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = NULL;
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'title' => (string) $title,
        'type' => $bundle,
      ]);
    // Does the node exist?
    if (count($nodes) > 0) {
      // Yes.
      $node = reset($nodes);
      if ($this->interactive) {
        $this->messenger()->addStatus(
          $this->t('Used existing node: @t', ['@t' => $title])
        );
      }
    }
    else {
      // Create a node.
      $node = $this->entityTypeManager
        ->getStorage('node')
        ->create(['type' => $bundle]);
      $node->set('title', $title);
      $node->set('uid', $user->get('uid')->value);
      foreach ($fieldData as $fieldName => $fieldValue) {
        $field = $node->get($fieldName);
        $fieldType = $field->getFieldDefinition()->getType();
        if ($fieldType === 'text_with_summary' || $fieldType === 'text_long') {
          // Set the format for long formatted text fields.
          $useSkillingFormat = FALSE;
          foreach ($this->skillingFormatFields as $fieldData) {
            $rightBundle = $fieldData['bundle'] === $bundle;
            $rightField = $fieldData['field'] === $fieldName;
            if ($rightBundle && $rightField) {
              $useSkillingFormat = TRUE;
              break;
            }
          }
          $field->format = $useSkillingFormat
            ? SkillingConstants::SKILLING_TEXT_FORMAT
            : SkillingConstants::STRIPPED_TEXT_FORMAT;
        }
        $node->set($fieldName, $fieldValue);
      }
      $node->save();
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Created new node: @t', ['@t' => $title])
        );
      }
    }
    return $node;
  }

  /**
   * Make a node into a book, unless it already exists.
   *
   * @param \Drupal\node\Entity\Node $rootNode
   *   The node.
   */
  public function makeNewBook(Node $rootNode) {
    $nid = $rootNode->id();
    $existingLink = $this->bookManager->loadBookLink($nid);
    if ($existingLink) {
      // Link already exists. Don't create a new one.
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Using existing book: @t',
            ['@t' => $rootNode->getTitle()]
          )
        );
        return;
      }
    }
    $link = [
      'nid' => $nid,
      'bid' => $nid,
      'pid' => 0,
      'weight' => 0,
    ];
    $this->bookManager->saveBookLink($link, TRUE);
    if ($this->interactive) {
      $this->messenger()->addStatus(
        t('Created new book: @t',
          ['@t' => $rootNode->getTitle()]
        )
      );
    }
  }

  /**
   * Add a node to a book.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node to add.
   * @param \Drupal\node\Entity\Node $parent
   *   Parent node in book.
   * @param \Drupal\node\Entity\Node $bookRoot
   *   Book root node.
   * @param int $weight
   *   Weight.
   */
  public function addNodeToBook(Node $node, Node $parent, Node $bookRoot, $weight) {
    $nid = $node->id();
    $existingLink = $this->bookManager->loadBookLink($nid);
    if ($existingLink) {
      // Link already exists. Don't create a new one.
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Using existing book link for @t',
            ['@t' => $node->getTitle()]
          )
        );
        return;
      }
    }
    // Create a new link.
    $pid = $parent->id();
    $bid = $bookRoot->id();
    $link = [
      'nid' => $nid,
      'bid' => $bid,
      'pid' => $pid,
      'weight' => $weight,
    ];
    $this->bookManager->saveBookLink($link, TRUE);
    if ($this->interactive) {
      $this->messenger()->addStatus(
        t('Created new book link for @t',
          ['@t' => $node->getTitle()]
        )
      );
    }
  }

  /**
   * Make a rubric item node, or return existing node with the given title.
   *
   * @param string $title
   *   Node title.
   * @param \Drupal\user\Entity\User $user
   *   Node owner.
   * @param array $rubricItemResponses
   *   Responses for this rubric item.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function makeRubricItemNode(
    $title,
    User $user,
    $termId,
    array $rubricItemResponses
  ) {
    $node = $this->makeNode(
      SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE,
      $title,
      $user,
      [
        'field_notes' => 'Dogs are good!',
        'field_rubric_item_responses' => [
          ['target_id' => $termId]
        ],
      ]
    );
    $responseParagraphs = [];
    foreach ($rubricItemResponses as $rubricItemResponse) {
      $responseText = $rubricItemResponse['response'];
      $completes = $rubricItemResponse['completes'];
      /** @var \Drupal\paragraphs\ParagraphInterface $responseParagraph */
      $responseParagraph = Paragraph::create(['type' => 'rubric_item_response']);
      $responseParagraph->set(
        SkillingConstants::FIELD_RESPONSE_TO_STUDENT,
        [
          'value' => $responseText,
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
        ]
      );
      $responseParagraph->set(
        SkillingConstants::FIELD_COMPLETES_RUBRIC_ITEM,
        $completes
      );
      $responseParagraph->setParentEntity(
        $node,
        SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES
      );
      $responseParagraph->isNew();
      $responseParagraph->save();
      $responseParagraphs[] = $responseParagraph;
    }
    // Add paragraphs to node.
    // Build up an array with reference data.
    $refs = [];
    foreach ($responseParagraphs as $responseParagraph) {
      $refs[] = [
        'target_id' => $responseParagraph->id(),
        'target_revision_id' => $responseParagraph->getRevisionId(),
      ];
    }
    // Add reference data to node.
    $node->set(SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES, $refs);
    $node->save();
    return $node;
  }

  /**
   * Make an exercise node, or return existing node with the given title.
   *
   * @param string $title
   *   Node title.
   * @param string $internalName
   *   Internal name of the exercise.
   * @param \Drupal\user\Entity\User $user
   *   Node owner.
   * @param array $rubricItems
   *   Rubric items for this exercise.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeExerciseNode($title, $internalName, User $user, array $rubricItems) {
    $exercise = $this->makeNode(
      SkillingConstants::EXERCISE_CONTENT_TYPE,
      $title,
      $user,
      [
        'field_internal_name' => $internalName,
        'field_notes' => t('Notes for @title', ['@title' => $title]),
        'body' => [
          'summary' => t('Summary of @title', ['@title' => $title]),
          'value' => t('Deets for @title', ['@title' => $title]),
        ],
        'field_rubric_items' => $rubricItems,
      ]
    );
    return $exercise;
  }

  /**
   * Make an enrollment node, or return existing node with the given title.
   *
   * @param \Drupal\node\Entity\Node $class
   *   The class.
   * @param \Drupal\user\Entity\User $user
   *   User enrolled.
   * @param array $classRoles
   *   Roles for the user in the class.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeEnrollmentNode(NodeInterface $class, User $user, array $classRoles) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = NULL;
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'enrollment',
        'field_class' => $class->id(),
        'field_user' => $user->id(),
      ]);
    // Does the node exist?
    if (count($nodes) > 0) {
      // Yes.
      $node = reset($nodes);
      // Add class roles.
      $node->field_class_roles = [];
      foreach ($classRoles as $classRole) {
        $node->field_class_roles[] = $classRole;
      }
      $node->save();
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Used existing enrollment: @t', ['@t' => $node->getTitle()])
        );
      }
    }
    else {
      // Create a node.
      $node = $this->entityTypeManager
        ->getStorage('node')
        ->create(['type' => SkillingConstants::ENROLLMENT_CONTENT_TYPE]);
      $title = t(
        'User @name enrolled in @class',
        [
          '@name' => $user->getAccountName(),
          '@class' => $class->getTitle(),
        ]
      );
      $node->set('title', $title);
      // Owner of enrollment node is owner of class.
      $node->set('uid', $class->getOwnerId());
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_class = $class;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_user = $user;
      // Clear the default.
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_class_roles = [];
      foreach ($classRoles as $classRole) {
        /* @noinspection PhpUndefinedFieldInspection */
        $node->field_class_roles[] = $classRole;
      }
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_base_progress_score_on_req = (rand() > 0.5);
      $node->save();
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Created new enrollment: @t', ['@t' => $title])
        );
      }
    }
    return $node;
  }

  /**
   * Make an enrollment node, or return existing node with the given title.
   *
   * @param \Drupal\node\Entity\Node $exercise
   *   The exercise the submission is for.
   * @param \Drupal\user\Entity\User $student
   *   The student making the submission.
   * @param string $solutionText
   *   The submission.
   * @param int $version
   *   Submission version.
   *
   * @return \Drupal\node\Entity\Node
   *   The node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeSubmissionNode(Node $exercise, User $student, $solutionText, $version) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = NULL;
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'type' => SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE,
        'field_exercise' => $exercise->id(),
        'field_user' => $student->id(),
        'field_version' => $version,
      ]);
    // Does the node exist?
    if (count($nodes) > 0) {
      // Yes.
      $node = reset($nodes);
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Used existing submission: @t', ['@t' => $node->getTitle()])
        );
      }
    }
    else {
      // Create a node.
      $node = $this->entityTypeManager
        ->getStorage('node')
        ->create(['type' => 'submission']);
      $title = t(
        '@name submitted solution for @exercise, attempt @version',
        [
          '@name' => $student->getAccountName(),
          '@exercise' => $exercise->getTitle(),
          '@version' => $version,
        ]
      );
      $node->set('title', $title);
      // Owner of node is student.
      $node->set('uid', $student->id());
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_exercise = $exercise;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_user = $student;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_complete = FALSE;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_solution->value = $solutionText;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_solution->format = SkillingConstants::STRIPPED_TEXT_FORMAT;
      /* @noinspection PhpUndefinedFieldInspection */
      $node->field_version = $version;
      $node->save();
      if ($this->interactive) {
        $this->messenger()->addStatus(
          t('Created new submission: @t', ['@t' => $title])
        );
      }
    }
    return $node;
  }


  public function makeExerciseDueParagraph(
    NodeInterface $exercise,
    $dayDue,
    $maxSubmissions,
    $required
  ) {
    $exerciseDueParagraph = Paragraph::create(['type' => 'exercise_due']);
    $exerciseDueParagraph->set(
      SkillingConstants::FIELD_EXERCISE,
      [
        'target_id' => $exercise->id(),
      ]
    );
    $exerciseDueParagraph->set(SkillingConstants::FIELD_DAY, $dayDue);
//    $exerciseDueParagraph->setParentEntity(
//      $parentClass,
//      SkillingConstants::FIELD_EXERCISES_DUE
//    );
    $exerciseDueParagraph->set(SkillingConstants::FIELD_MAX_SUBMISSIONS, $maxSubmissions);
    $exerciseDueParagraph->set(SkillingConstants::FIELD_REQUIRED, $required);
    $exerciseDueParagraph->isNew();
    $exerciseDueParagraph->save();
    return $exerciseDueParagraph;
  }
  /**
   * Make an event.
   *
   * @param string $title
   *   Event title.
   * @param string $type
   *   Event type.
   * @param string $summary
   *   Summary.
   * @param string $details
   *   Details.
   * @param null|Node $exercise
   *   Exercise due, if relevant.
   * @param string $notes
   *   Notes.
   * @param \Drupal\user\Entity\User $owner
   *   Node owner.
   *
   * @return \Drupal\node\Entity\Node
   *   Event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
//  public function makeEvent($title, $type, $summary, $details, $exercise, $notes, User $owner) {
//    /** @var \Drupal\node\Entity\Node $node */
//    $node = NULL;
//    $nodes = $this->entityTypeManager
//      ->getStorage('node')
//      ->loadByProperties([
//        'type' => SkillingConstants::EVENT_CONTENT_TYPE,
//        'title' => (string) $title,
//      ]);
//    // Does the node exist?
//    if (count($nodes) > 0) {
//      // Yes.
//      $node = reset($nodes);
//      if ($this->interactive) {
//        $status = t('Used existing @t',
//          ['@t' => SkillingConstants::EVENT_CONTENT_TYPE]);
//        $this->messenger()->addStatus($status . $node->getTitle());
//      }
//    }
//    else {
//      // Create a node.
//      $node = $this->entityTypeManager
//        ->getStorage('node')
//        ->create(['type' => SkillingConstants::EVENT_CONTENT_TYPE]);
//      $node->set('title', $title);
//      // Owner of node is student.
//      $node->set('uid', $owner->id());
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->field_event_type = $type;
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->body = $details;
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->body->summary = $summary;
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->body->format = SkillingConstants::STRIPPED_TEXT_FORMAT;
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->field_notes->value = $notes;
//      /* @noinspection PhpUndefinedFieldInspection */
//      $node->field_notes->format = SkillingConstants::STRIPPED_TEXT_FORMAT;
//      if ($type === SkillingConstants::EVENT_TYPE_EXERCISE_DUE) {
//        /* @noinspection PhpUndefinedFieldInspection */
//        $node->field_exercise->target_id = $exercise->id();
//      }
//      $node->save();
//      if ($this->interactive) {
//        $this->messenger()->addStatus(
//          t('Created new @type: @title',
//            [
//              '@type' => SkillingConstants::EVENT_CONTENT_TYPE,
//              '@title' => $title,
//            ]
//          )
//        );
//      }
//    }
//    return $node;
//  }

  /**
   * Add an event to a calendar.
   *
   * @param \Drupal\node\NodeInterface $calendar
   *   Calendar.
   * @param \Drupal\node\NodeInterface $event
   *   Event.
   * @param int $day
   *   Day.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
//  public function addEventToCalendar(NodeInterface $calendar, NodeInterface $event, $day) {
//    // Create an event paragraph.
//    $paragraph = Paragraph::create([
//      'type' => SkillingConstants::CALENDAR_EVENT_PARAGRAPH_TYPE,
//    ]);
//    $paragraph->set(SkillingConstants::FIELD_DAY, $day);
//    $paragraph->set(SkillingConstants::FIELD_EVENT, $event);
//    $paragraph->isNew();
//    $paragraph->save();
//    // Append the paragraph to the calendar.
//    $calendar->get(SkillingConstants::FIELD_CALENDAR_EVENTS)->appendItem($paragraph);
//    $calendar->save();
//  }

  /**
   * Assign a calendar to a class.
   *
   * @param \Drupal\node\Entity\Node $class
   *   The class.
   * @param \Drupal\node\Entity\Node $calendar
   *   The calendar.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
//  public function assignCalendarToClass(Node $class, Node $calendar) {
//    /* @noinspection PhpUndefinedFieldInspection */
//    $class->field_calendar->target_id = $calendar->id();
//    $class->save();
//  }

  /**
   * Make a fill-in-the-blank node.
   *
   * @param string $title
   *   Item title.
   * @param \Drupal\user\Entity\User $owner
   *   Node owner.
   * @param string $internalName
   *   Internal name.
   * @param string $question
   *   Question.
   * @param string $noMatchResponse
   *   Response when there is no a match.
   * @param string $notes
   *   Author notes.
   * @param array $responses
   *   Responses.
   *
   * @return \Drupal\node\Entity\Node
   *   FiB node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function makeFiB($title, User $owner, $internalName, $question, $noMatchResponse, $notes, $responses) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->makeNode(
      SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE,
      $title,
      $owner,
      [
        SkillingConstants::FIELD_INTERNAL_NAME => $internalName,
        SkillingConstants::FIELD_QUESTION => $question,
        SkillingConstants::FIELD_NO_MATCH_RESPONSE => $noMatchResponse,
        SkillingConstants::FIELD_NOTES => $notes,
      ]
    );
    // Make a paragraph for each response.
    $paragraphs = [];
    foreach ($responses as $response) {
      $paragraphContainer = new FibResponseContainer($this->entityTypeManager);
      $paragraphContainer->setResponseType($response['response_type']);
      if ($response['response_type'] === SkillingConstants::FIB_MATCH_TYPE_NUMBER) {
        $paragraphContainer->setLowest($response['lowest']);
        $paragraphContainer->setHighest($response['highest']);
      }
      if ($response['response_type'] === SkillingConstants::FIB_MATCH_TYPE_TEXT) {
        $paragraphContainer->setMatchingTextResponses($response['matching']);
        $paragraphContainer->setCaseSensitive($response['case_sensitive']);
      }
      $paragraphContainer->setCorrect($response['correct']);
      $paragraphContainer->setExplanation($response['explanation']);
      // Any problems?
      if (!$paragraphContainer->isValid()) {
        foreach ($paragraphContainer->getLastCheckErrorMessages() as $message) {
          $this->messenger->addError($message);
        }
        return $node;
      }
      $paragraphs[] = $paragraphContainer->createParagraph($node);
    }
    // Build up an array with reference data.
    $refs = [];
    foreach ($paragraphs as $paragraph) {
      $refs[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    // Add reference data to node.
    $node->set(SkillingConstants::FIELD_FIB_RESPONSES, $refs);
    $node->save();
    return $node;
  }

  /**
   * Make a character node.
   *
   * @param string $title
   *   Item title.
   * @param \Drupal\user\Entity\User $owner
   *   Node owner.
   * @param string $internalName
   *   Internal name.
   * @param string $imageFileName
   *   Name of the image in the module's images dir.
   * @param string $caption
   *   Character's caption.
   * @param string $notes
   *   Author notes.
   *
   * @return \Drupal\node\Entity\Node
   *   FiB node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function makeCharacter($title, User $owner, $internalName, $imageFileName, $caption, $notes) {
    // Check that the file exists in the module's image dir.
    $rootPath = $this->skillingUtilities->getSiteRootFilePath();
    $modulePathFromRoot = $this->skillingUtilities->getModuleRelativeUrlPath();
    $imageFilePath = $rootPath . $modulePathFromRoot
      . 'images/characters/' . $imageFileName;
    if (!file_exists($imageFilePath)) {
      $message = Html::escape('Character file not found: ' . $imageFileName);
      throw new SkillingNotFoundException($message, __FILE__, __LINE__);
    }
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->makeNode(
      SkillingConstants::CHARACTER_CONTENT_TYPE,
      $title,
      $owner,
      [
        'field_internal_name' => $internalName,
        'field_caption' => $caption,
        'field_notes' => [
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          'value' => $notes,
        ],
      ]
    );
    // Compute the path where character images are stored.
    // Get it from the field definition.
    /** @var \Drupal\field\Entity\FieldConfig $fieldConfig */
    $fieldConfig = $this->entityFieldManager
      ->getFieldDefinitions('node', SkillingConstants::CHARACTER_CONTENT_TYPE)['field_photo'];
    $characterImagePath = $fieldConfig->get('settings')['file_directory'];
    $characterImagePath = $this->skillingUtilities->addSlashToEnd($characterImagePath);
    // Load the image data from the file in the module's images dir.
    $data = file_get_contents($imageFilePath);
    // Make sure the destination dir is there.
    $directory = 'public://' . $characterImagePath;
    // Make a file entity, where character images are stored.
    $perms = FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY;
    $dirPrepResult = \Drupal::service('file_system')->prepareDirectory(
      $directory, $perms);
    $file = file_save_data(
      $data,
      $directory . $imageFileName,
      FileSystemInterface::EXISTS_REPLACE
    );
    if (!$file) {
      $message = Html::escape('Problem with character file: ' . $imageFileName);
      throw new SkillingException($message, __FILE__, __LINE__);
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $node->field_photo->target_id = $file->id();
    /* @noinspection PhpUndefinedFieldInspection */
    $node->field_photo->alt = $caption;
    $node->save();
    if ($this->interactive) {
      $this->messenger()->addStatus(
        t(
          'Used @file for character @t',
          ['@file' => $imageFileName, '@t' => $title]
        )
      );
    }
    return $node;
  }

  /**
   * Make an MCQ.
   *
   * @param string $title
   *   Node title.
   * @param string $internalName
   *   Internal name.
   * @param string $question
   *   Question.
   * @param array $responses
   *   Responses to the MCQ.
   * @param \Drupal\user\UserInterface $owner
   *   Node owner.
   * @param string $notes
   *   Notes.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\node\Entity\Node|mixed
   *   MCQ node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeMcq($title, $internalName, $question, array $responses, UserInterface $owner, $notes) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = NULL;
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'title' => (string) $title,
        'type' => SkillingConstants::MCQ_CONTENT_TYPE,
      ]);
    // Does the node exist?
    if (count($nodes) > 0) {
      // Yes.
      $node = reset($nodes);
      if ($this->interactive) {
        $this->messenger()->addStatus(
          $this->t('Used existing node: @t', ['@t' => $title])
        );
      }
    }
    else {
      // Create the MCQ node.
      /** @var \Drupal\node\Entity\Node $node */
      $node = $this->entityTypeManager
        ->getStorage('node')
        ->create(['type' => SkillingConstants::MCQ_CONTENT_TYPE]);
      $node->set('title', $title);
      $node->set('uid', $owner->get('uid')->value);
      $node->set(SkillingConstants::FIELD_INTERNAL_NAME, $internalName);
      $node->set(
        SkillingConstants::FIELD_QUESTION,
        [
          'value' => $question,
          'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
        ]
      );
      if ($notes) {
        $node->set(SkillingConstants::FIELD_NOTES, $notes);
      }
      // Make the response paragraph entities.
      $responseEntities = [];
      foreach ($responses as $response) {
        $paragraph = Paragraph::create([
          'type' => SkillingConstants::MCQ_RESPONSE_PARAGRAPH_TYPE,
          'field_response' => [
            'value' => $response['response'],
            'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          ],
          'field_correct' => $response['correct'],
          'field_explanation' => [
            'value' => $response['explanation'],
            'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          ],
        ]);
        $paragraph->setParentEntity($node, SkillingConstants::FIELD_RESPONSES);
        $paragraph->save();
        $responseEntities[] = [
          'target_id' => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ];
        $node->set('field_responses', $responseEntities);
        $node->save();
        if ($this->interactive) {
          $this->messenger()->addStatus(
            t('Created: @title', ['@title' => $title])
          );
        }
      }
      $node->save();
    }
    return $node;
  }

}
