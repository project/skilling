<?php

namespace Drupal\skilling;

use DateTime;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;

/**
 * Completion score service.
 *
 * Update completion score when:
 *
 * A whole new dawn. page-attach calls getCompletion since last update.
 *
 * Grader submits feedback.
 *  - hook_node_update
 *
 * Create new submission
 * - hook_node_insert
 *
 * Update submission - change completion value
 * -hook_node_update
 *
 * Delete submission that is marked complete.
 * -hook_node_delete
 *
 * Unpublish/republish submission node.
 * -hook_node_update
 *
 * Republish enrollment node
 * -runScheduledClassRoleUpdates
 *
 * Update enrollment, now has with student role.
 * -runScheduledClassRoleUpdates
 *
 * Unblock user, and user is student.
 * - hook_user_update
 */
class CompletionScore {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * User factory service.
   *
   * @var SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * The enrollment service.
   *
   * @var \Drupal\skilling\Enrollment
   */
  protected $enrollmentService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Utilities service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   User factory service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   The Skilling current user service.
   * @param SkillingCurrentClass $current_class
   *   The current class service.
   * @param \Drupal\skilling\Enrollment $enrollmentService
   *   The enrollment service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skillingUtilities,
    SkillingUserFactory $skillingUserFactory,
    SkillingCurrentUser $skilling_current_user,
    SkillingCurrentClass $current_class,
    Enrollment $enrollmentService,
    MessengerInterface $messenger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skillingUtilities;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->currentClass = $current_class;
    $this->enrollmentService = $enrollmentService;
    $this->messenger = $messenger;
  }

  /**
   * Update completion score for user with user id.
   *
   * @param int $uid
   *   User id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function updateCompletionScoresForUser($uid) {
    // Get enrollment records for the user.
    /** @var \Drupal\node\Entity\Node[] $enrollments */
    $enrollments = $this->skillingUtilities->getUserEnrollmentsPublished($uid);
    // Which enrollments does the user have a student role for?
    $studentEnrollments = [];
    foreach ($enrollments as $enrollment) {
      $isStudent = FALSE;
      $classRoles = $enrollment->get(SkillingConstants::FIELD_CLASS_ROLES)->getValue();
      foreach ($classRoles as $key => $value) {
        $classRole = $value['value'];
        if ($classRole === SkillingConstants::CLASS_ROLE_STUDENT) {
          $isStudent = TRUE;
          break;
        }
      }
      if ($isStudent) {
        $studentEnrollments[] = $enrollment;
      }
    }
    // Update completion score for enrollments.
    foreach ($studentEnrollments as $studentEnrollment) {
      $this->updateEnrollmentCompletionScore($studentEnrollment);
    }
  }

  /**
   * Update a student's completion scores for an enrollment.
   *
   * @param \Drupal\node\NodeInterface $enrollment
   *   Enrollment record.
   *
   * @return int|null
   *   Completion score, or NULL if it cannot be computed, e.g., student
   *   account is blocked.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Exception
   */
  public function updateEnrollmentCompletionScore(NodeInterface $enrollment) {
    // Exit if enrollment not published.
    if (!$enrollment->isPublished()) {
      return NULL;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $classNid = $enrollment->field_class->target_id;
    if (!$classNid) {
      throw new SkillingValueMissingException(
        Html::escape('Enrollment missing class'), __FILE__, __LINE__
      );
    }
    /** @var \Drupal\node\NodeInterface $class */
    $class = $this->skillingUtilities->getClassPublished($classNid);
    if (!$class) {
      throw new SkillingValueMissingException(
        Html::escape('Class missing:' . $classNid), __FILE__, __LINE__
      );
    }
    // Exit if class not published.
    if (!$class->isPublished()) {
      return NULL;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $studentUid = $enrollment->field_user->target_id;
    if (!$studentUid) {
      throw new SkillingValueMissingException(
        Html::escape(
          'Enrollment missing student. Enroll: ' . $enrollment->id()
        ),
        __FILE__, __LINE__
      );
    }
    /** @var SkillingUser $student */
    $student = $this->skillingUserFactory->makeSkillingUser($studentUid);
    if (!$student) {
      throw new SkillingValueMissingException(
        Html::escape('Student missing:' . $studentUid), __FILE__, __LINE__
      );
    }
    // Exit if student account blocked.
    if ($student->isBlocked()) {
      return NULL;
    }
    // Is the user a student in the class?
    $userIsStudent = $this->skillingUtilities->isEnrollmentHasClassRole(
      $enrollment, [SkillingConstants::CLASS_ROLE_STUDENT]);
    // Exit if the user is not a student.
    if (!$userIsStudent) {
      return NULL;
    }
    // Base score just on required exercises?
    $baseOnRequired = $enrollment
      ->get(SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY)
      ->getValue()[0]['value'];
    $baseOnRequired = $baseOnRequired === "1";
    // Remember the current score.
    $currentScore = (int) $enrollment->field_completion_score->value;
    // New score.
    $score = 0;
    // How many days from the start of the calendar is today?
    /* @noinspection PhpUndefinedFieldInspection */
    $startDate = $class->field_when_starts->value;
    $startDate = new DateTime($startDate);
    $now = new DateTime();
    // Check whether the class has started.
    if ($startDate <= $now) {
      $todayAsDayNumber = $now->diff($startDate)->days + 1;
      // Also array where key is exercise nid, value is day it is due.
      list($exercises, $daysExercisesDue)
        = $this->getExercisesForClass($class, $baseOnRequired);
      // Find submissions for the exercises, for the current user.
      $exerciseNids = array_keys($daysExercisesDue);
      if (count($exerciseNids) > 0) {
        $submissionNids = $this->entityTypeManager->getStorage('node')
          ->getQuery()
          ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
          // For the student.
          ->condition('field_user', $student->id())
          // Published submissions.
          ->condition('status', 1)
          // For one of the exercises.
          ->condition('field_exercise', $exerciseNids, 'IN')
          // Published exercises only.
          ->condition('field_exercise.entity.status', 1)
          ->execute();
        // Load submissions.
        $submissions = $this->entityTypeManager->getStorage('node')
          ->loadMultiple($submissionNids);
        // Returns array indexed by exercise nid.
        $exercisesSubmissionSummary = $this->prepareSubmissionSummary($exercises, $submissions);
        // Compute score.
        // Loop across the exercise data extracted from the calendar.
        foreach ($daysExercisesDue as $exerciseNid => $dayDue) {
          // Is exercise due before today?
          if ($dayDue < $todayAsDayNumber) {
            $score--;
          }
          // Is there a submission for the exercise that is complete
          // or awaiting feedback?
          if (isset($exercisesSubmissionSummary[$exerciseNid])) {
            $submissionStatus = $exercisesSubmissionSummary[$exerciseNid];
            $complete = $submissionStatus === SkillingConstants::EXERCISE_SUBMISSION_COMPLETE;
            $waiting = $submissionStatus === SkillingConstants::EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK;
            if ($complete || $waiting) {
              $score++;
            }
          }
        } // End for each.
      } // End there are exercises.
    }
    // Did the score change?
    if ($score !== $currentScore) {
      /* @noinspection PhpUndefinedFieldInspection */
      $enrollment->field_completion_score->value = $score;
      /* @noinspection PhpUndefinedFieldInspection */
      $enrollment->field_when_score_updated->value = $now->format('Y-m-d');
      global $globalEnrollmentBeingSaved;
      // Don't save if this code is triggred by other code saving
      // the enrollment record.
      if ($globalEnrollmentBeingSaved !== 'yup') {
        // Store it.
        $enrollment->save();
      }
    }
    return $score;
  }


  /**
   * @param NodeInterface $class
   *
   * @return array
   */
  protected function getExercisesForClass($class, $isRequiredOnly) {
    /** @var NodeInterface $exercises */
    $exercises = [];
    /** @var int[] $daysExercisesDue */
    $daysExercisesDue = [];
    $exerciseDueParaRefs = $class->get(SkillingConstants::FIELD_EXERCISES_DUE)->getValue();
    foreach ($exerciseDueParaRefs as $exerciseDueParaRef) {
      $exerciseDueParagraph = $this->entityTypeManager->getStorage('paragraph')
        ->load($exerciseDueParaRef['target_id']);
      // Check that it exists.
      if (!$exerciseDueParagraph) {
        continue;
      }
      $exerciseId = (int)$exerciseDueParagraph->get(SkillingConstants::FIELD_EXERCISE)
        ->getValue()[0]['target_id'];
      $day = (int)$exerciseDueParagraph->get(SkillingConstants::FIELD_DAY)
        ->getValue()[0]['value'];
      $required = $exerciseDueParagraph
        ->get(SkillingConstants::FIELD_REQUIRED)
        ->getValue()[0]['value'];
      $required = ($required === '1');
      if ($isRequiredOnly && !$required) {
        continue;
      }
      // Is the exercise published?
      /** @var NodeInterface $exercise */
      $exercise = $this->entityTypeManager->getStorage('node')
        ->load($exerciseId);
      // Check that it exists.
      if (!$exercise) {
        continue;
      }
      if ($exercise->isPublished()) {
        // Remember it.
        $exercises[] = $exercise;
        $daysExercisesDue[$exerciseId] = $day;
      }
    }
    return [$exercises, $daysExercisesDue];
  }

  /**
   * Get the day exercises are due.
   *
   * @param \Drupal\node\NodeInterface[] $calendarEvents
   *   Calendar events.
   * @param \Drupal\node\NodeInterface[] $events
   *   Events.
   *
   * @return array
   *   Key is exercise nid, value is day it is due
   */
//  protected function getExerciseDueDays(array $calendarEvents, array $events) {
//    $daysExercisesDue = [];
//    foreach ($calendarEvents as $calendarEvent) {
//      $eventDay = $calendarEvent->field_day->value;
//      $eventNid = $calendarEvent->field_event->target_id;
//      $event = $events[$eventNid];
//      $eventType = $event->field_event_type->value;
//      if ($eventType === SkillingConstants::EVENT_TYPE_EXERCISE_DUE) {
//        $exerciseNid = $event->field_exercise->target_id;
//        $daysExercisesDue[$exerciseNid] = $eventDay;
//      }
//    }
//    return $daysExercisesDue;
//  }

  /**
   * Prepare submission summary.
   *
   * Prepare an array indexed by exercise id that summarizes submission
   * status for that exercise, based on array of submission nodes.
   *
   * @param \Drupal\node\NodeInterface[] $exercises
   *   Exercises.
   * @param \Drupal\node\NodeInterface[] $submissions
   *   Submissions.
   *
   * @return array
   *   Summary.
   *
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  protected function prepareSubmissionSummary(array $exercises, array $submissions) {
    $exerciseSubmissionSummary = [];
    /** @var \Drupal\node\NodeInterface $exercise */
    foreach ($exercises as $exercise) {
      $exerciseNid = $exercise->id();
      // The submission status for the exercise.
      // no submission
      // waiting for feedback - submitted but no feedback
      // complete
      // not complete - grader says not complete.
      /** @var string $exerciseSubmissionStatus */
      /* @noinspection PhpUnusedLocalVariableInspection */
      $exerciseSubmissionStatus = '';
      // Find the submissions for the exercise.
      // Find the highest version submission for the exercise.
      $maxVersion = 0;
      $lastSubmission = NULL;
      foreach ($submissions as $submission) {
        $isNotExerciseSet = !isset($submission->field_exercise->target_id)
          || !$submission->field_exercise->target_id;
        if ($isNotExerciseSet) {
          throw new SkillingValueMissingException(
            Html::escape('Exercise missing. Submission:' . $submission->id()),
            __FILE__, __LINE__
          );
        }
        if ($submission->field_exercise->target_id == $exerciseNid) {
          $version = $submission->field_version->value;
          if ($version > $maxVersion) {
            $maxVersion = $version;
            $lastSubmission = $submission;
          }
        }
      }
      if (is_null($lastSubmission)) {
        // No submissions for the exercise.
        $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION;
      }
      else {
        // There's at least one submission for the exercise.
        // Is there feedback for the submission?
        $feedbackDate = $lastSubmission->field_when_feedback_given->value;
        if (!$feedbackDate) {
          // No feedback yet.
          $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK;
        }
        else {
          // There is feedback.
          // Did the submission complete the exercise?
          $complete = $lastSubmission->field_complete->value;
          if ($complete) {
            $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_COMPLETE;
          }
          else {
            $exerciseSubmissionStatus = SkillingConstants::EXERCISE_SUBMISSION_NOT_COMPLETE;
          }
        } // End there is feedback.
      } // End there is a submission.
      $exerciseSubmissionSummary[$exerciseNid] = $exerciseSubmissionStatus;
    } // End for each exercise.
    return $exerciseSubmissionSummary;
  }

  /**
   * Get completion scores for an enrollment.
   *
   * @param \Drupal\node\NodeInterface $enrollment
   *   The enrollment.
   *
   * @return int|null
   *   The score.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Exception
   */
  public function getCompletionScoreForEnrollment(NodeInterface $enrollment) {
    if (!$this->skillingCurrentUser->isStudent()) {
      throw new SkillingException(
        Html::escape('Not student'), __FILE__, __LINE__
      );
    }
    // Check whether need to recompute score, based on date.
    /* @noinspection PhpUndefinedFieldInspection */
    $updatedDate = $enrollment->field_when_score_updated->value;
    if (!$updatedDate) {
      throw new SkillingValueMissingException(
        Html::escape('Score updated date missing from enrollment ' . $enrollment->id()),
        __FILE__, __LINE__
      );
    }
    // Continue check.
    /* @noinspection PhpUndefinedFieldInspection */
    $lastComputationDate = new DateTime($updatedDate);
    $now = new DateTime();
    $daysDifference = $now->diff($lastComputationDate)->days;
    $recomputeNeeded = ($daysDifference !== 0);
    if ($recomputeNeeded) {
      $score = $this->updateEnrollmentCompletionScore($enrollment);
    }
    else {
      /* @noinspection PhpUndefinedFieldInspection */
      $score = $enrollment->field_completion_score->value;
    }
    return $score;
  }

  /**
   * Get the completion score for the current user, if a student.
   *
   * @return int|null
   *   The score.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getCompletionScoreForCurrentEnrollment() {
    if (!$this->skillingCurrentUser->isStudent()) {
      throw new SkillingException(
        Html::escape('Not student'), __FILE__, __LINE__
      );
    }
    $completionScore = NULL;
    $currentEnrollment = $this->currentClass->getCurrentEnrollment();
    if ($currentEnrollment) {
      $completionScore = $this->getCompletionScoreForEnrollment($currentEnrollment);
    }
    return $completionScore;
  }

  /**
   * Get emotional interpretation of score.
   *
   * @param int|null $completionScore
   *   The score.
   *
   * @return array
   *   Interpretation. Emoticon file name, and text.
   */
  public function getCompletionScoreInterpretation($completionScore) {
    if ($completionScore >= 4) {
      $fileName = 'very-happy.svg';
      $text = $this->t("Yay! You're ahead of the game!");
    }
    elseif ($completionScore >= 2) {
      $fileName = 'happy.svg';
      $text = $this->t("Good, you're a little ahead.");
    }
    elseif ($completionScore >= -1) {
      $fileName = 'neutral.svg';
      $text = $this->t("Nice, you're keeping up.");
    }
    elseif ($completionScore >= -3) {
      $fileName = 'sad.svg';
      $text = $this->t("You've fallen behind.");
    }
    else {
      $fileName = 'very-sad.svg';
      $text = $this->t("Argh! You're way behind.");
    }
    return [$fileName, $text];
  }

}
