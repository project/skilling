<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 1/24/19
 * Time: 11:56 AM
 */

namespace Drupal\skilling\Access;

use Drupal\Component\Utility\Html;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidator;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities;
use Symfony\Component\HttpFoundation\RequestStack;

class FieldAccessSpecialCaseChecker {

  /**
   * Path validation service.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidatorService;

  /**
   * Current path service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathService;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserService;

  /**
   * Special cases that are allowed.
   *
   * @var array
   *   Special cases that are allowed.
   */
  protected $specialCases;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStackService;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  // Keys required of each special case. Array means one of them is needed.
  const REQUIRED_KEYS = [
    ['role', 'roles'],
    'field_name',
    'operation',
    ['path', 'paths'],
    'http_method',
    'allowed',
  ];

  /**
   * SkillingFieldAccessSpecialCases constructor.
   *
   * @param \Drupal\Core\Path\PathValidator $pathValidatorService
   *   Path validator service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathService
   *   Current path service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUserService
   *   The current user service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStackService
   *   The request stack service.
   * @param \Drupal\skilling\Utilities $utilities
   *   The Skilling utilities service.
   */
  public function __construct(
    PathValidator $pathValidatorService,
    CurrentPathStack $currentPathService,
    SkillingCurrentUser $currentUserService,
    RequestStack $requestStackService,
    Utilities $utilities
  ) {
    $this->pathValidatorService = $pathValidatorService;
    $this->currentPathService = $currentPathService;
    $this->currentUserService = $currentUserService;
    $this->requestStackService = $requestStackService;
    $this->skillingUtilities = $utilities;
  }

  /**
   * Define allowed special cases.
   *
   * @param array $specialCaseDefinitions
   *   Definition of special cases.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function defineAllowedSpecialCases(array $specialCaseDefinitions) {
    foreach ($specialCaseDefinitions as $specialCaseDefinition) {
      foreach (self::REQUIRED_KEYS as $requiredKey) {
        $foundKey = FALSE;
        if (is_array($requiredKey)) {
          // Need one of them, e.g., role or roles.
          foreach ($requiredKey as $innerKey) {
            if (array_key_exists($innerKey, $specialCaseDefinition)) {
              $foundKey = TRUE;
              break;
            }
          }
        }
        else {
          // Required key is a single value.
          if (array_key_exists($requiredKey, $specialCaseDefinition)) {
            $foundKey = TRUE;
          }
        }
        if (!$foundKey) {
          throw new SkillingValueMissingException(
            Html::escape('Missing key: ' . $requiredKey),
            __FILE__, __LINE__
          );
        }
      }
      /** @var \Drupal\skilling\Access\SkillingFieldAccessSpecialCase $specialCase */
      $specialCase = new SkillingFieldAccessSpecialCase(
        $this->pathValidatorService, $this->skillingUtilities
      );
      if (isset($specialCaseDefinition['role'])) {
        $specialCase->setAccessingUserRole($specialCaseDefinition['role']);
      }
      else {
        $specialCase->setAccessingUserRoles($specialCaseDefinition['roles']);
      }
      $specialCase
        ->setFieldName($specialCaseDefinition['field_name'])
        ->setOperation($specialCaseDefinition['operation']);
      if (isset($specialCaseDefinition['path'])) {
        $specialCase->setPath($specialCaseDefinition['path']);
      }
      else {
        $specialCase->setPaths($specialCaseDefinition['paths']);
      }
      $specialCase
        ->setHttpMethod($specialCaseDefinition['http_method'])
        ->setAllowed($specialCaseDefinition['allowed']);
      $this->specialCases[] = $specialCase;
    }
  }

  /**
   * Check for a special case.
   *
   * Field name and operation are passed in. The rest of the context-defining
   * elements come from services.
   *
   * @param string $fieldNameToCheck
   *   Field name to check.
   * @param string $operationToCheck
   *   Operation to check.
   *
   * @return bool|null
   *   The allowed value defined in a special case that matches, or
   *   null if there is no match.
   */
  public function checkForSpecialCase($fieldNameToCheck, $operationToCheck) {
    /** @var \Symfony\Component\HttpFoundation\Request $currentRequest */
    $currentRequest = $this->requestStackService->getCurrentRequest();
    $currentMethod = strtolower($currentRequest->getMethod());
    $currentUserRoles = $this->currentUserService->getRoles();
    if (count($currentUserRoles) === 0) {
      if ($this->currentUserService->isAnonymous()) {
        $currentUserRoles = [SkillingConstants::SITE_ROLE_ANONYMOUS];
      }
    }
    $currentPath = strtolower($this->currentPathService->getPath());
    // Loop across defined special cases, looking for a match.
    /** @var \Drupal\skilling\Access\SkillingFieldAccessSpecialCase $specialCase */
    foreach ($this->specialCases as $specialCase) {
      if ($fieldNameToCheck !== $specialCase->getFieldName()) {
        continue;
      }
      if ($operationToCheck !== $specialCase->getOperation()) {
        continue;
      }
      // Check if one of the user's roles matches.
      $roleMatch = FALSE;
      foreach ($currentUserRoles as $role) {
        if ($specialCase->isKnownRole($role)) {
          $roleMatch = TRUE;
          break;
        }
      }
      if (!$roleMatch) {
        continue;
      }
      if (!$specialCase->isKnownPath($currentPath)) {
        continue;
      }
      if ($currentMethod !== $specialCase->getHttpMethod()) {
        continue;
      }
      // Everything matches.
      return $specialCase->isAllowed();
    }
    return NULL;
  }

}
