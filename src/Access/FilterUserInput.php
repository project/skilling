<?php

namespace Drupal\skilling\Access;

use Drupal\skilling\SkillingConstants;

/**
 * Class FilterUserInput.
 *
 * Filter input from students, graders, and instructors.
 */
class FilterUserInput implements FilterUserInputInterface {

  /**
   * {@inheritdoc}
   */
  public function filterUserContent($content) {
    $filtered = check_markup($content, SkillingConstants::STRIPPED_TEXT_FORMAT);
    $filtered = $this->addMissingProtocols($filtered);
    return (string) $filtered;
  }

  /**
   * {@inheritdoc}
   */
  public function addMissingProtocols($html) {
    $html = preg_replace_callback(
      '/href=[\'\"](.*?)[\'\"]/i',
      function ($matches) {
        $url = $matches[1];
        $fragments = parse_url($url);
        if ($fragments === FALSE) {
          // Mangled URL.
          $url = '';
        }
        else {
          if (!isset($fragments['scheme'])) {
            $url = 'http://' . $url;
          }
        }
        return "href='$url'";
      },
      $html
    );
    return $html;
  }

}
