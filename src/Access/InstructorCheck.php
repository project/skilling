<?php
namespace Drupal\skilling\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\Routing\Route;

/**
 * Check whether the current user is an instructor or administrator.
 *
 * @package Drupal\skilling\Access
 */
class InstructorCheck implements AccessInterface {

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * InstructorCheck constructor.
   *
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The Skilling current user service.
   */
  public function __construct(SkillingCurrentUser $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * Forbid access to user without instructor and/or administrator roles.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route being checked.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Result of check.
   */
  public function access(AccountInterface $account, Route $route) {
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    if ($admin || $instructor) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;
  }

}
