<?php

namespace Drupal\skilling\Access;

use Drupal\Core\Path\PathValidator;
use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\SkillingConstants;
use Drupal\Component\Utility\Html;
use Drupal\skilling\Utilities;

/**
 * Represents one special case for field access.
 *
 * @package Drupal\skilling\Access
 */
class SkillingFieldAccessSpecialCase {

  /**
   * Path validation service.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * SkillingFieldAccessSpecialCase constructor.
   *
   * @param \Drupal\Core\Path\PathValidator $pathValidator
   *   Path validator service.
   * @param \Drupal\skilling\Utilities $utilities
   *   The Skilling utilities service.
   */
  public function __construct(PathValidator $pathValidator, Utilities $utilities) {
    $this->pathValidator = $pathValidator;
    $this->skillingUtilities = $utilities;
  }

  /**
   * Field name for special case.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Role for special case.
   *
   * Only one role for the special case.
   *
   * This is a site-level role, that does not consider enrollment.
   *
   * @var string
   */
  protected $accessingUserRole = NULL;

  /**
   * Roles for special case.
   *
   * Special case has multiple roles.
   *
   * These are site-level roles, that do not consider enrollment.
   *
   * @var string[]
   */
  protected $accessingUserRoles = NULL;

  /**
   * Operation for the special case.
   *
   * @var string
   */
  protected $operation;

  /**
   * HTTP method for special case.
   *
   * @var string
   */
  protected $httpMethod;

  /**
   * URL path for special case.
   *
   * Only one path for the special case.
   *
   * @var string
   */
  protected $path = NULL;

  /**
   * URL paths for special case.
   *
   * More than one path in the special case.
   *
   * @var string[]
   */
  protected $paths = NULL;

  /**
   * Whether access is allowed for this special case.
   *
   * @var bool
   */
  protected $allowed;

  /**
   * Get the name of the field for the special case.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * Set the name of the field for the special case.
   *
   * @param string $fieldName
   *   The field name.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function setFieldName($fieldName) {
    if (!$fieldName) {
      throw new SkillingValueMissingException(
        Html::escape('Field name missing'),
        __FILE__, __LINE__
      );
    }
    $this->fieldName = $fieldName;
    return $this;
  }

  /**
   * Get the accessing user's role for the special case.
   *
   * @return string
   *   The role.
   */
  public function getAccessingUserRole() {
    return $this->accessingUserRole;
  }

  /**
   * Set the accessing user's role for the special case.
   *
   * Just one role for the special case.
   *
   * @param string $accessingUserRole
   *   The role.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setAccessingUserRole($accessingUserRole) {
    if (!in_array($accessingUserRole, SkillingConstants::SKILLING_ROLES)) {
      throw new SkillingInvalidValueException(
        Html::escape('Unknown role: ' . $accessingUserRole),
        __FILE__, __LINE__
      );
    }
    $this->accessingUserRole = $accessingUserRole;
    return $this;
  }

  /**
   * Get the accessing user's roles for the special case.
   *
   * @return string[]
   *   The roles.
   */
  public function getAccessingUserRoles() {
    return $this->accessingUserRoles;
  }

  /**
   * Set the accessing user's roles for the special case.
   *
   * More than one role matches the special case.
   *
   * @param string[] $accessingUserRoles
   *   The roles.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setAccessingUserRoles($accessingUserRoles) {
    foreach ($accessingUserRoles as $accessingUserRole) {
      if (!in_array($accessingUserRole, SkillingConstants::SKILLING_ROLES)) {
        throw new SkillingInvalidValueException(
          Html::escape('Unknown role: ' . $accessingUserRole),
          __FILE__, __LINE__
        );
      }
    }
    $this->accessingUserRoles = $accessingUserRoles;
    return $this;
  }

  /**
   * Are there multiple roles for the special case?
   *
   * @return bool
   *   True if there are multiple roles.
   */
  public function isMultipleRoles() {
    return !is_null($this->accessingUserRoles);
  }

  /**
   * Is a given role known?
   *
   * @param string $roleToCheck
   *   The role to check.
   *
   * @return bool
   *   True if role is known.
   */
  public function isKnownRole($roleToCheck) {
    $known = FALSE;
    if ($this->isMultipleRoles()) {
      // Multiple roles are known.
      foreach ($this->accessingUserRoles as $role) {
        if ($roleToCheck === $role) {
          $known = TRUE;
          break;
        }
      }
    }
    else {
      $known = ($roleToCheck === $this->accessingUserRole);
    }
    return $known;
  }

  /**
   * Get the operation.
   *
   * @return string
   *   The operation.
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Set the operation for the special case.
   *
   * @param string $operation
   *   Operation.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setOperation($operation) {
    $operation = strtolower(trim($operation));
    if ($operation !== SkillingConstants::VIEW_OPERATION) {
      throw new SkillingInvalidValueException(
        Html::escape('Only view is allowed.'),
        __FILE__, __LINE__
      );
    }
    $this->operation = $operation;
    return $this;
  }

  /**
   * Get the HTTP method for the special case.
   *
   * @return string
   *   The method.
   */
  public function getHttpMethod() {
    return $this->httpMethod;
  }

  /**
   * Set the HTTP method for the special case.
   *
   * Only GET is allowed.
   *
   * @param string $httpMethod
   *   The method.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setHttpMethod($httpMethod) {
    $httpMethod = strtolower(trim($httpMethod));
    if ($httpMethod !== 'get') {
      throw new SkillingInvalidValueException(
        Html::escape('Only GET is allowed.'),
        __FILE__, __LINE__
      );
    }
    $this->httpMethod = $httpMethod;
    return $this;
  }

  /**
   * Get the path for the special case.
   *
   * Only one path for the special case.
   *
   * @return string
   *   The path.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Set the path for the special case.
   *
   * Only one path for the special case.
   *
   * @param string $path
   *   The path.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setPath($path) {
    $this->path = $path;
    return $this;
  }

  /**
   * Get the paths for the special case.
   *
   * Multiple paths for the special case.
   *
   * @return string[]
   *   The paths.
   */
  public function getPaths() {
    return $this->paths;
  }

  /**
   * Set the paths for the special case.
   *
   * Multiple paths for the special case.
   *
   * @param string[] $paths
   *   The paths.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function setPaths(array $paths) {
    $this->paths = $paths;
    return $this;
  }

  /**
   * Are there multiple paths for the special case?
   *
   * @return bool
   *   True if there are multiple paths.
   */
  public function isMultiplePaths() {
    return !is_null($this->paths);
  }

  /**
   * Is a given path known?
   *
   * @param string $pathToCheck
   *   The path to check.
   *
   * @return bool
   *   True if path is known.
   */
  public function isKnownPath($pathToCheck) {
    $known = FALSE;
    if ($this->isMultiplePaths()) {
      // Multiple paths are known.
      foreach ($this->paths as $path) {
        if ($pathToCheck === $path) {
          $known = TRUE;
          break;
        }
      }
    }
    else {
      $known = ($pathToCheck === $this->path);
    }
    return $known;
  }

  /**
   * Is access allowed for the special case?
   *
   * @return bool
   *   True if access is to be allowed.
   */
  public function isAllowed() {
    return $this->allowed;
  }

  /**
   * Set whether access is allowed for the special case.
   *
   * @param bool $allowed
   *   True if access is to be allowed.
   *
   * @return SkillingFieldAccessSpecialCase
   *   This, for chaining.
   */
  public function setAllowed($allowed) {
    $this->allowed = $allowed;
    return $this;
  }

}
