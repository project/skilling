<?php

namespace Drupal\skilling\Access;

use Symfony\Component\HttpFoundation\Request;

/**
 * Security checks on AJAX calls.
 *
 * Call these methods from controllers that receive AJAX calls.
 */
interface SkillingAjaxSecurityInterface {

  /**
   * Check that the request passes basic security tests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Represents the HTTP request.
   * @param array $allowedMethods
   *   Methods that can be called.
   * @param array $allowedPaths
   *   Paths that are allowed.
   * @param bool $skipTokenTestForAnon
   *   If true, skip token check for anonymous user.
   *
   * @return bool
   *   True if request passes.
   */
  public function securityCheckAjaxRequest(Request $request, array $allowedMethods, array $allowedPaths, $skipTokenTestForAnon = FALSE);

  /**
   * Is a request from a trusted host?
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to check.
   *
   * @return bool
   *   True if request is from a trusted host.
   */
  public function isRequestFromTrustedHost(Request $request);

  /**
   * Get a CSRF token.
   *
   * @return string
   *   The token.
   */
  public function getCsrfToken();

  /**
   * Validate a CSRF token.
   *
   * @param string $token
   *   The token to validate.
   *
   * @return bool
   *   True if the token validates.
   */
  public function validateCsrfToken($token);


  }
