<?php
namespace Drupal\skilling\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Symfony\Component\Routing\Route;
use Drupal\skilling\Utilities as SkillingUtilities;

class StudentOfInstructorAccessCheck implements AccessInterface {

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The check user relationships service.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $userRelationshipsService;

  /**
   * SkillingAccessCheck constructor.
   *
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   The Skilling user factory service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The Skilling current user service.
   */
  public function __construct(SkillingUtilities $skilling_utilities, SkillingUserFactory $skillingUserFactory, SkillingCurrentUser $currentUser, SkillingCheckUserRelationships $userRelationshipsService) {
    $this->skillingUtilities = $skilling_utilities;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->currentUser = $currentUser;
    $this->userRelationshipsService = $userRelationshipsService;
  }

  /**
   * Allow access if user with uid in URL is an instructor of the current user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route being checked.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access check result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function access(AccountInterface $account, Route $route) {
    // Only for students.
    if (!$this->currentUser->isStudent()) {
      return AccessResult::forbidden();
    }
    // Get the instructor id from the URL.
    $instructorId = $this->skillingUtilities->getCurrentRouteMatch()
      ->getParameter('instructor_id');
    // Load the instructor.
    $instructor = $this->skillingUserFactory->makeSkillingUser($instructorId);
    $isInstructor = $this->userRelationshipsService->isUserInstructorOfUser(
      $instructor, $this->currentUser
    );
    $result = $isInstructor ? AccessResult::allowed() : AccessResult::forbidden();
    return $result;
  }

}
