<?php
namespace Drupal\skilling\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Symfony\Component\Routing\Route;
use Drupal\skilling\Utilities as SkillingUtilities;

/**
 * Checks if current user is an instructor of a user whose uid is in the URL.
 *
 * @package Drupal\skilling\Access
 */
class InstructorOfStudentAccessCheck implements AccessInterface {

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The check user relationships service.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $userRelationshipsService;

  /**
   * Constructor.
   *
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   The Skilling user factory service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The Skilling current user service.
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $userRelationshipsService
   *   The Skilling user relationships service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(SkillingUtilities $skilling_utilities, SkillingUserFactory $skillingUserFactory, SkillingCurrentUser $currentUser, SkillingCheckUserRelationships $userRelationshipsService) {
    $this->skillingUtilities = $skilling_utilities;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->currentUser = $currentUser;
    $this->userRelationshipsService = $userRelationshipsService;
  }

  /**
   * Allow access if user with uid in URL is a student of the current user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route being checked.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access check result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function access(AccountInterface $account, Route $route) {
    // Admin OK.
    if ($this->currentUser->isAdministrator()) {
      return AccessResult::allowed();
    }
    // Only for instructors.
    if (!$this->currentUser->isInstructor()) {
      return AccessResult::forbidden();
    }
    // Get the student id from the URL.
    $studentId = $this->skillingUtilities->getCurrentRouteMatch()
      ->getParameter('studentId');
    // Load the student.
    $student = $this->skillingUserFactory->makeSkillingUser($studentId);
    $isStudent = $this->userRelationshipsService->isUserInstructorOfUser(
      $this->currentUser, $student
    );
    $result = $isStudent ? AccessResult::allowed() : AccessResult::forbidden();
    return $result;
  }

}
