<?php

namespace Drupal\skilling\Access;

/**
 * Interface FilterUserInputInterface.
 */
interface FilterUserInputInterface {

  /**
   * Filter content from users, stripping all but a few tags.
   *
   * If missing, protocols are added to URLs in href attribute.
   * http is added.
   *
   * @param string $content
   *   The content to be filtered.
   *
   * @return string
   *   The result.
   */
  public function filterUserContent($content);

  /**
   * If hrefs in a string have URLs with a missing protocol, add it in.
   *
   * @param string $html
   *   HTML to check.
   *
   * @return string
   *   Result.
   */
  public function addMissingProtocols($html);

}
