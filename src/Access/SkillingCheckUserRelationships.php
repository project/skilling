<?php

namespace Drupal\skilling\Access;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingUser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\SkillingClass\SkillingClass;
use Drupal\skilling\SkillingClass\SkillingClassFactory;

/**
 * Check relationships between users.
 *
 * Relationships such as whether users
 * have an instructor-student relationship. That's used to control
 * access to submissions, and other stuff.
 *
 * @package Drupal\skilling\Access
 */
class SkillingCheckUserRelationships {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $userFactory;

  /** @var SkillingClassFactory */
  protected $classFactory;

  /**
   * SkillingCheckUserRelationships constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingUserFactory $userFactory
   *   The user factory service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingUtilities $skillingUtilities,
    SkillingUserFactory $userFactory,
    SkillingClassFactory $classFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->userFactory = $userFactory;
    $this->classFactory = $classFactory;
  }

  /**
   * Is one user an instructor or another?
   *
   * @param int $possibleInstructorUid
   *   One of the users.
   * @param int $possibleStudentUid
   *   The other user.
   *
   * @return bool
   *   Is the first user an instructor of the first?
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function isUserUidInstructorOfUserUid($possibleInstructorUid, $possibleStudentUid) {
    // Load user objects.
    $possibleInstructor = $this->userFactory->makeSkillingUser($possibleInstructorUid);
    if (!$possibleInstructor) {
      return FALSE;
    }
    if (!$possibleInstructor->isInstructor()) {
      return FALSE;
    }
    $possibleStudent = $this->userFactory->makeSkillingUser($possibleStudentUid);
    if (!$possibleStudent) {
      return FALSE;
    }
    $result = $this->isUserInstructorOfUser($possibleInstructor, $possibleStudent);
    return $result;
  }

  /**
   * Is one user an instructor for another?
   *
   * @param \Drupal\skilling\SkillingUser $possibleInstructor
   *   One of the users.
   * @param \Drupal\skilling\SkillingUser $possibleStudent
   *   The other user.
   *
   * @return bool
   *   Is the first user an instructor of the second?
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserInstructorOfUser(SkillingUser $possibleInstructor, SkillingUser $possibleStudent) {
    if ($possibleInstructor->isAnonymous() || $possibleStudent->isAnonymous()) {
      return FALSE;
    }
    if (!$possibleInstructor->isInstructor()) {
      return FALSE;
    }
    if (!$possibleStudent->isStudent()) {
      return FALSE;
    }
    // Get class nids the user teaches.
    $instructorClassNids = $this->getInstructorClassIds($possibleInstructor);
    //    // Get enrollments of possible instructor.
    //    $possibleInstructorEnrollments
    //      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleInstructor->id());
    //    // Find class nids for which the user is an instructor.
    //    $instructorClassNids = [];
    //    foreach ($possibleInstructorEnrollments as $possibleInstructorEnrollment) {
    //      $classSpecificRoles = $possibleInstructorEnrollment->field_class_roles->getValue();
    //      foreach ($classSpecificRoles as $key => $value) {
    //        $classRole = $value['value'];
    //        if ($classRole === SkillingConstants::CLASS_ROLE_INSTRUCTOR) {
    //          $instructorClassNids[] = $possibleInstructorEnrollment->field_class->target_id;
    //        }
    //      }
    //    }
    // Instructor of any classes?
    if (count($instructorClassNids) === 0) {
      return FALSE;
    }
    // Get the enrollments of the possible student.
    $studentClassNids = $this->getStudentClassIds($possibleStudent);
    //    $possibleStudentEnrollments
    //      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleStudent->id());
    //    // Find class nids for which the user is an instructor.
    //    $studentClassNids = [];
    //    foreach ($possibleStudentEnrollments as $possibleStudentEnrollment) {
    //      $classSpecificRoles = $possibleStudentEnrollment->field_class_roles->getValue();
    //      foreach ($classSpecificRoles as $key => $value) {
    //        $classRole = $value['value'];
    //        if ($classRole === SkillingConstants::CLASS_ROLE_STUDENT) {
    //          $studentClassNids[] = $possibleStudentEnrollment->field_class->target_id;
    //        }
    //      }
    //    }
    // Student in any classes?
    if (count($studentClassNids) === 0) {
      return FALSE;
    }
    // Are there any class nids in common?
    $commonClassNids = array_intersect($instructorClassNids, $studentClassNids);
    return count($commonClassNids) > 0;
  }


  /**
   * @param \Drupal\skilling\SkillingUser $user
   * @param SkillingClass $class
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isInstructorOfClass(SkillingUser $user, SkillingClass $class) {
    $userClasseIds = $this->getInstructorClassIds($user);
    $classId = $class->id();
    $result = in_array($classId, $userClasseIds);
    return $result;
  }

  /**
   * @param $userId
   * @param $classId
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function isInstructorIdOfClassId($userId, $classId) {
    $user = $this->userFactory->makeSkillingUser($userId);
    if (!$user) {
      return FALSE;
    }
    $class = $this->classFactory->makeSkillingClass($classId);
    if (!$class) {
      return FALSE;
    }
    $result = $this->isInstructorOfClass($user, $class);
    return $result;
  }


  /**
   * Get the ids of classes that a user is an instructor for.
   *
   * @param \Drupal\skilling\SkillingUser $possibleInstructor
   *   User who could be an instructor.
   *
   * @return array
   *   Nids of class nodes the user is instructor for
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstructorClassIds(SkillingUser $possibleInstructor) {
    $result = $this->getUserWithRoleClassIds(
      $possibleInstructor,
      SkillingConstants::CLASS_ROLE_INSTRUCTOR
    );
    return $result;
  }

  /**
   * Get the ids of classes that a user is a student in.
   *
   * @param \Drupal\skilling\SkillingUser $possibleStudent
   *   User who could be a student/
   *
   * @return array
   * Nids of class nodes the user is a student in.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStudentClassIds(SkillingUser $possibleStudent) {
    $result = $this->getUserWithRoleClassIds(
      $possibleStudent,
      SkillingConstants::CLASS_ROLE_STUDENT
    );
    return $result;
  }

  /**
   * Get the ids of classes that a user has a given role in.
   *
   * @param \Drupal\skilling\SkillingUser $user
   *   User who could have the role.
   *
   * @return array
   * Nids of class nodes the user is a student in.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserWithRoleClassIds(SkillingUser $user, $role) {
    $enrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($user->id());
    // Find class nids for which the user has the role.
    $classNids = [];
    foreach ($enrollments as $enrollment) {
      $classSpecificRoles = $enrollment->field_class_roles->getValue();
      foreach ($classSpecificRoles as $key => $value) {
        $classRole = $value['value'];
        if ($classRole === $role) {
          $classNids[] = $enrollment->field_class->target_id;
        }
      }
    }
    return $classNids;
  }






  /**
   * Is the first user a grader for the second user, an instructor?
   *
   * @param \Drupal\skilling\SkillingUser $possibleGrader
   *   One user.
   * @param \Drupal\skilling\SkillingUser $possibleInstructor
   *   The other user.
   *
   * @return bool
   *   True if the first user a grader for the second.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserGraderForInstructor(SkillingUser $possibleGrader, SkillingUser $possibleInstructor) {
    if ($possibleInstructor->isAnonymous() || $possibleGrader->isAnonymous()) {
      return FALSE;
    }
    if (!$possibleInstructor->isInstructor()) {
      return FALSE;
    }
    if (!$possibleGrader->isGrader()) {
      return FALSE;
    }
    // Get enrollments of possible instructor.
    /** @var \Drupal\node\Entity\Node[] $possibleInstructorEnrollments */
    $possibleInstructorEnrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleInstructor->id());
    // Find class nids for which the user is an instructor.
    $instructorClassNids = [];
    foreach ($possibleInstructorEnrollments as $possibleInstructorEnrollment) {
      $classSpecificRoles = $possibleInstructorEnrollment->get('field_class_roles')->getValue();
      foreach ($classSpecificRoles as $key => $value) {
        if ($value === SkillingConstants::CLASS_ROLE_INSTRUCTOR) {
          $instructorClassNids[] = $possibleInstructorEnrollment->get('field_class')->target_id;
        }
      }
    }
    // Instructor of any classes?
    if (count($instructorClassNids) === 0) {
      return FALSE;
    }
    // Get the enrollments of the possible grader.
    $possibleGraderEnrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleGrader->id());
    // Find class nids for which the user is a grader.
    $graderClassNids = [];
    foreach ($possibleGraderEnrollments as $possibleGraderEnrollment) {
      $classSpecificRoles = $possibleGraderEnrollment->field_class_roles->getValue();
      foreach ($classSpecificRoles as $key => $value) {
        if ($value === SkillingConstants::CLASS_ROLE_GRADER) {
          $graderClassNids[] = $possibleGraderEnrollment->field_class->target_id;
        }
      }
    }
    // Grader of any classes?
    if (count($graderClassNids) === 0) {
      return FALSE;
    }
    // Are there any class nids in common?
    $commonClassNids = array_intersect($instructorClassNids, $graderClassNids);
    return $commonClassNids > 0;
  }

  /**
   * Is first user a grader for a student, the second user?
   *
   * @param int $possibleGraderUid
   *   One of the users.
   * @param int $possibleStudentUid
   *   The other user.
   *
   * @return bool
   *   Is the first user a grader of the second?
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function isUserUidGraderOfUserUid($possibleGraderUid, $possibleStudentUid) {
    // Load user objects.
    $possibleGrader = $this->userFactory->makeSkillingUser($possibleGraderUid);
    if (!$possibleGrader) {
      return FALSE;
    }
    $possibleStudent = $this->userFactory->makeSkillingUser($possibleStudentUid);
    if (!$possibleStudent) {
      return FALSE;
    }
    $result = $this->isUserGraderOfUser($possibleGrader, $possibleStudent);
    return $result;
  }

  /**
   * Is one user a grader of another?
   *
   * @param \Drupal\skilling\SkillingUser $possibleGrader
   *   One of the users.
   * @param \Drupal\skilling\SkillingUser $possibleStudent
   *   The other user.
   *
   * @return bool
   *   Is the first user a grader of the second?
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserGraderOfUser(SkillingUser $possibleGrader, SkillingUser $possibleStudent) {
    if ($possibleGrader->isAnonymous() || $possibleStudent->isAnonymous()) {
      return FALSE;
    }
    if (!$possibleGrader->isGrader()) {
      return FALSE;
    }
    if (!$possibleStudent->isStudent()) {
      return FALSE;
    }
    // Get enrollments of possible grader.
    $possibleGraderEnrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleGrader->id());
    // Find class nids for which the user is a grader.
    $graderClassNids = [];
    foreach ($possibleGraderEnrollments as $possibleGraderEnrollment) {
      $classSpecificRoles = $possibleGraderEnrollment->field_class_roles->getValue();
      foreach ($classSpecificRoles as $key => $value) {
        $classRole = $value['value'];
        if ($classRole === SkillingConstants::CLASS_ROLE_GRADER) {
          $graderClassNids[] = $possibleGraderEnrollment->field_class->target_id;
        }
      }
    }
    // Grader of any classes?
    if (count($graderClassNids) === 0) {
      return FALSE;
    }
    // Get the enrollments of the possible student.
    $possibleStudentEnrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($possibleStudent->id());
    // Find class nids for which the user is an instructor.
    $studentClassNids = [];
    foreach ($possibleStudentEnrollments as $possibleStudentEnrollment) {
      $classSpecificRoles = $possibleStudentEnrollment->field_class_roles->getValue();
      foreach ($classSpecificRoles as $key => $value) {
        $classRole = $value['value'];
        if ($classRole === SkillingConstants::CLASS_ROLE_STUDENT) {
          $studentClassNids[] = $possibleStudentEnrollment->field_class->target_id;
        }
      }
    }
    // Student in any classes?
    if (count($studentClassNids) === 0) {
      return FALSE;
    }
    // Are there any class nids in common?
    $commonClassNids = array_intersect($graderClassNids, $studentClassNids);
    return count($commonClassNids) > 0;
  }

}
