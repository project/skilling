<?php

namespace Drupal\skilling\Access;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\SkillingUser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\taxonomy\Entity\Term;

/**
 * A service to check the current user's access to entities, blocks, and fields.
 *
 * Access checks are sent through this class, so it's important to get it right.
 *
 * @package Drupal\skilling
 */
class SkillingAccessChecker {
  use StringTranslationTrait;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The current class service.
   *
   * If the current user is enrolled in more than one class, s/he chooses
   * one of them to be the context for the site.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Service to check relationships between users.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $checkUserRelationshipsService;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Class to make Skilling user objects.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling parser service.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The field access special case checker service.
   *
   * @var \Drupal\skilling\Access\FieldAccessSpecialCaseChecker
   */
  protected $fieldAccessSpecialCaseChecker;

  /**
   * Make a SkillingAccessChecker object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeTanager
   *   The entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The current user service.
   * @param SkillingCurrentClass $currentClass
   *   The current class service.
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $checkUserRelationshipsService
   *   Service to check relationships between users.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration factory service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   Class to make Skilling user objects.
   * @param \Drupal\skilling\Utilities $utilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   *   The Skilling parser service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\skilling\Access\FieldAccessSpecialCaseChecker $fieldAccessSpecialCaseChecker
   *   The field access special case checker service.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeTanager,
    SkillingCurrentUser $currentUser,
    SkillingCurrentClass $currentClass,
    SkillingCheckUserRelationships $checkUserRelationshipsService,
    ConfigFactory $configFactory,
    SkillingUserFactory $skillingUserFactory,
    SkillingUtilities $utilities,
    SkillingParser $parser,
    MessengerInterface $messenger,
    FieldAccessSpecialCaseChecker $fieldAccessSpecialCaseChecker
  ) {
    $this->entityTypeManager = $entityTypeTanager;
    $this->currentUser = $currentUser;
    $this->currentClass = $currentClass;
    $this->checkUserRelationshipsService = $checkUserRelationshipsService;
    $this->configFactory = $configFactory;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->skillingUtilities = $utilities;
    $this->parser = $parser;
    $this->messenger = $messenger;
    $this->fieldAccessSpecialCaseChecker = $fieldAccessSpecialCaseChecker;
    $this->defineSpecialFieldAccess();
  }

  /**
   * Define special cases for field access.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  protected function defineSpecialFieldAccess() {
    $specialCases = [
      [
        'field_name' => SkillingConstants::FIELD_WHERE_REFERENCED,
        'operation' => SkillingConstants::VIEW_OPERATION,
        'path' => '/exercises',
        'role' => SkillingConstants::SITE_ROLE_STUDENT,
        'http_method' => 'get',
        'allowed' => TRUE,
      ],

      [
        'field_name' => SkillingConstants::FIELD_ORDER_IN_BOOK,
        'operation' => SkillingConstants::VIEW_OPERATION,
        'paths' => [
          '/exercises',
          '/lessons',
        ],
        'roles' => [
          SkillingConstants::SITE_ROLE_ANONYMOUS,
          SkillingConstants::SITE_ROLE_STUDENT,
          SkillingConstants::SITE_ROLE_AUTHOR,
          SkillingConstants::SITE_ROLE_INSTRUCTOR,
          SkillingConstants::SITE_ROLE_REVIEWER,
          SkillingConstants::SITE_ROLE_GRADER,
        ],
        'http_method' => 'get',
        'allowed' => TRUE,
      ],

      [
        'field_name' => SkillingConstants::FIELD_ORDER_IN_BOOK,
        'operation' => SkillingConstants::VIEW_OPERATION,
        'paths' => [
          '/admin/skilling/reflection-notes',
        ],
        'roles' => [
          SkillingConstants::SITE_ROLE_AUTHOR,
          SkillingConstants::SITE_ROLE_INSTRUCTOR,
          SkillingConstants::SITE_ROLE_REVIEWER,
        ],
        'http_method' => 'get',
        'allowed' => TRUE,
      ],

      [
        'field_name' => SkillingConstants::FIELD_ORDER_IN_BOOK,
        'operation' => SkillingConstants::VIEW_OPERATION,
        'path' => '/skilling/reflect-notes',
        'role' => SkillingConstants::SITE_ROLE_STUDENT,
        'http_method' => 'get',
        'allowed' => TRUE,
      ],
    ];
    $this->fieldAccessSpecialCaseChecker->defineAllowedSpecialCases($specialCases);
  }

  /**
   * Check user access to an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $operation
   *   Operation on the entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   Result of check
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function getEntityAccess(EntityInterface $entity, $operation) {
    $entityType = $entity->getEntityTypeId();
    if ($entityType === 'user') {
      // Make a Skilling User to pass to the access checking service.
      $userBeingAccessed = $this->skillingUserFactory->makeSkillingUser($entity->id());
      // Get the AccessResult for the user entity.
      $result = $this->getUserEntityAccess($userBeingAccessed, $operation);
      return $result;
    }
    elseif ($entityType === 'node') {
      // Make a new var of the right type.
      /** @var \Drupal\node\NodeInterface $node */
      $node = $entity;
      $result = $this->getNodeAccess($node, $operation);
      return $result;
    }
    elseif ($entityType === 'paragraph') {
      // Make a new var of the right type.
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      $paragraph = $entity;
      $result = $this->getParagraphAccess($paragraph, $operation);
      return $result;
    }
    elseif ($entityType == 'file') {
      /** @var File $file */
      $file = $entity;
      $result = $this->getFileAccess($file, $operation);
      return $result;
    }
    elseif ($entityType == 'taxonomy_term') {
      /** @var Term $term */
      $term = $entity;
      $result = $this->getTaxonomyTermAccess($term, $operation);
      return $result;
    }
    $result = AccessResult::neutral();
    $result->setCacheMaxAge(0);
    return $result;
  }


  public function getTaxonomyTermAccess(Term $term, $operation) {
    $allow = $this->isTaxonomyTermAccess($term, $operation);
    if ($allow) {
      $result = AccessResult::neutral();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;
  }

  public function isTaxonomyTermAccess(Term $term, $operation) {
    $taxonomyName = $term->bundle();
    if ($taxonomyName != SkillingConstants::TAXONOMY_RUBRIC_ITEM_CATEGORIES) {
      return TRUE;
    }
    $allow = FALSE;
    // Normalize operation name.
    $operation = $this->normalizeOperation($operation);
    // Some flag to make code more readable.
    $viewOperation = $operation === SkillingConstants::VIEW_OPERATION;
    $editOperation = $operation === SkillingConstants::EDIT_OPERATION;
    // Make role flags in local vars to make code easier to read.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    // Start checking access.
    if ($viewOperation) {
      $allow = $admin || $author || $instructor || $grader || $reviewer;
    }
    if ($editOperation) {
      $allow = $admin || $author;
    }
    return $allow;
  }

  /**
   * Is an entity one that Skilling's access check cares about?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   True if Skilling should check access, false if not.
   */
  public function isRelevantEntity(EntityInterface $entity) {
    $relevantEntity = FALSE;
    $entityType = $entity->getEntityTypeId();
    if ($entityType === 'user') {
      $relevantEntity = TRUE;
    }
    elseif ($entityType === 'node') {
      $contentType = $entity->bundle();
      $relevantEntity = in_array($contentType, SkillingConstants::SKILLING_CONTENT_TYPES);
    }
    elseif ($entityType === 'paragraph') {
      $paragraphType = $entity->bundle();
      $relevantEntity = in_array($paragraphType, SkillingConstants::SKILLING_PARAGRAPH_TYPES);
    }
    return $relevantEntity;
  }

  /**
   * Does the current user have access to a node?
   *
   * Returns an AccessResult, based on call to isNodeAccess().
   *
   * Field access is checked separately, in getFieldAccess()
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   * @param string $operation
   *   Operation to check - view, edit, update.
   *
   * @return \Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getNodeAccess(NodeInterface $node, $operation) {
    $allow = $this->isNodeAccess($node, $operation);
    if ($allow) {
      $result = AccessResult::neutral();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;
  }

  /**
   * Does the current user have access to a node?
   *
   * Field access is checked separately, in isFieldAccess()
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   * @param string $operation
   *   Operation to check - view, edit, update.
   *
   * @return bool
   *   True if has access, false if not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function isNodeAccess(NodeInterface $node, $operation) {
    // Only check content types that are controlled by Skilling.
    $contentType = $node->bundle();
    if (!in_array($contentType, SkillingConstants::SKILLING_CONTENT_TYPES)) {
      // Not a Skilling content type. Other code determines its fate.
      return TRUE;
    }
    // Default to not allowing access.
    $allow = FALSE;
    // Normalize operation name.
    $operation = $this->normalizeOperation($operation);
    // Make operation flags to make code easier to read.
    $viewOperation = $operation === SkillingConstants::VIEW_OPERATION;
    $editOperation = $operation === SkillingConstants::EDIT_OPERATION;
    // Error if unknown operation.
    if (!$viewOperation && !$editOperation) {
      throw new SkillingInvalidValueException(
        Html::escape('Operation: ' . $operation . ', expected view or edit.'),
        __FILE__, __LINE__
      );
    }
    // Make role flags in local vars to make code easier to read.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
    $anonymous = $this->currentUser->isAnonymous();
    $authenticated = $this->currentUser->isAuthenticated();
    // Start checking node access.
    switch ($contentType) {
      case SkillingConstants::LESSON_CONTENT_TYPE:
      case SkillingConstants::EXERCISE_CONTENT_TYPE:
      case SkillingConstants::PATTERN_CONTENT_TYPE:
      case SkillingConstants::PRINCIPLE_CONTENT_TYPE:
      case SkillingConstants::MODEL_CONTENT_TYPE:
      case SkillingConstants::BADGE_CONTENT_TYPE:
        if ($viewOperation) {
          // Everyone can see these nodes.
          $allow = TRUE;
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          $allow = $admin || $author;
        }
        else {
          // Already checked above. Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::DESIGN_PAGE_CONTENT_TYPE:
        // Design page.
        if ($viewOperation) {
          // Admins, authors, reviewers, instructors and graders have access.
          // Anons and students don't have access, unless site admin
          // has set a config setting.
          if ($admin || $author || $reviewer || $instructor || $grader) {
            $allow = TRUE;
            break;
          }
          elseif ($student || $anonymous) {
            // Check config setting to see if allowed.
            $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
            $allow = $settings->get(SkillingConstants::SETTING_KEY_STUDENTS_ANON_SEE_DESIGN_PAGES);
            break;
          }
          // If user does not have a known Skilling role, use
          // default access: false.
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          $allow = $admin || $author;
        }
        else {
          // Already checked above. Should never get there.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE:
        if ($viewOperation) {
          // Admins, authors, reviewers, instructors, and graders can see these
          // content types.
          if ($admin || $author || $reviewer || $instructor || $grader) {
            $allow = TRUE;
          }
          break;
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          $allow = $admin || $author;
        }
        else {
          // Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE:
        // Submission.
        // Only a few cases permitted:
        // - Admins see and edit all.
        // - Students see and edit their own.
        // - Instructors see and edit submissions from students in
        //   their classes.
        // Graders can't access.
        // Rules apply for view and edit operations.
        // Admins can view submissions.
        if ($admin) {
          $allow = TRUE;
          break;
        }
        // Students can see their own their submissions.
        $nodeOwnerUid = $node->getOwnerId();
        if ($nodeOwnerUid === $this->currentUser->id()) {
          $allow = TRUE;
          break;
        }
        // Instructors can access submissions from students who are in
        // their own classes.
        if ($this->isCurrentUserInstructorOfSubmissionStudent($node)) {
          $allow = TRUE;
          break;
        }
        break;

      case SkillingConstants::CLASS_CONTENT_TYPE:
        // Classes.
        if ($viewOperation) {
          // Instructors and admins can see classes.
          if ($admin || $instructor) {
            $allow = TRUE;
            break;
          }
          // Others only see classes they are in.
          if ($this->currentUser->isInClass($node)) {
            $allow = TRUE;
            break;
          }
          break;
        }
        elseif ($editOperation) {
          if ($admin) {
            // Admins can edit.
            $allow = TRUE;
            break;
          }
          if ($instructor) {
            // Instructors can only edit classes they own.
            if ($this->isCurrentUserOwnsNode($node)) {
              $allow = TRUE;
              break;
            }
          }
        }
        else {
          // Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::ENROLLMENT_CONTENT_TYPE:
        // Enrollment nodes.
        if ($viewOperation) {
          // Admins can see all enrollments.
          // Users can see their own enrollments.
          // Instructors can see enrollments of students who are
          // in their own classes.
          if ($admin) {
            $allow = TRUE;
            break;
          }
          // Instructors see enrollments in their classes.
          if ($instructor) {
            /* @noinspection PhpUndefinedFieldInspection */
            $classId = $node->get(SkillingConstants::FIELD_CLASS)->target_id;
            if ($this->currentUser->isInstructorOfClassNid($classId)) {
              $allow = TRUE;
              break;
            }
          }
          // Other users only see their own records. Enrolled users own their
          // own enrollment records.
          if ($this->isCurrentUserOwnsNode($node)) {
            $allow = TRUE;
            break;
          }
        }
        elseif ($editOperation) {
          // Admins can edit any enrollments.
          if ($admin) {
            $allow = TRUE;
            break;
          }
          // Instructors can edit enrollments in their classes.
          if ($instructor) {
            /* @noinspection PhpUndefinedFieldInspection */
            $classId = $node->get(SkillingConstants::FIELD_CLASS)->target_id;
            // No class id when this is a new record.
            if (is_null($classId) || $this->currentUser->isInstructorOfClassNid($classId)) {
              $allow = TRUE;
              break;
            }
          }
        }
        break;

      case SkillingConstants::NOTICE_CONTENT_TYPE:
        if ($viewOperation) {
          if ($authenticated) {
            if ($admin) {
              $allow = TRUE;
              break;
            }
            // People can see their own notices.
            $nodeOwnerUid = $node->getOwnerId();
            if ($nodeOwnerUid === $this->currentUser->id()) {
              $allow = TRUE;
              break;
            }
          }
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          $allow = $admin;
          break;
        }
        else {
          // Already checked above. Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::CHARACTER_CONTENT_TYPE:
        // Character content type.
        if ($viewOperation) {
          // Only authors, admins, and reviewers see these node types directly.
          if ($admin || $author || $reviewer) {
            $allow = TRUE;
            break;
          }
          break;
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          if ($admin || $author) {
            $allow = TRUE;
            break;
          }
        }
        else {
          // Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

//      case SkillingConstants::HISTORY_CONTENT_TYPE:
//        if ($viewOperation) {
//          // Admins can see.
//          if ($admin) {
//            $allow = TRUE;
//            break;
//          }
//          // Students can see their own. Students own history records
//          // about them.
//          if ($student) {
//            if ($this->isCurrentUserOwnsNode($node)) {
//              $allow = TRUE;
//              break;
//            }
//          }
//          // Instructors should have access to their own students.
//          if ($instructor) {
//            // Owner of the history node is the student whom the event is about.
//            $studentUid = $node->getOwnerId();
//            $possibleInstructorId = $this->currentUser->id();
//            $currentUserIsInstructorOfNodeOwner =
//              $this->checkUserRelationshipsService->isUserUidInstructorOfUserUid(
//                $possibleInstructorId, $studentUid
//            );
//            if ($currentUserIsInstructorOfNodeOwner) {
//              $allow = TRUE;
//              break;
//            }
//          }
//        }
//        elseif ($editOperation) {
//          // Only admins can edit history records.
//          $allow = $admin;
//        }
//        else {
//          // Should never get here.
//          throw new SkillingInvalidValueException(
//            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
//            __FILE__, __LINE__
//          );
//        }
//        break;

//      case SkillingConstants::CALENDAR_CONTENT_TYPE:
//        // Calendar nodes. Events are separate nodes.
//        if ($viewOperation) {
//          // Admins, authors, reviewers, and instructors can see
//          // all calendars.
//          if ($admin || $author || $reviewer || $instructor) {
//            $allow = TRUE;
//            break;
//          }
//        }
//        elseif ($editOperation) {
//          if ($admin || $author) {
//            $allow = TRUE;
//            break;
//          }
//          if ($instructor) {
//            // Instructors can only edit calendars they own.
//            if ($this->isCurrentUserOwnsNode($node)) {
//              $allow = TRUE;
//              break;
//            }
//          }
//        }
//        else {
//          // Should never get here.
//          throw new SkillingInvalidValueException(
//            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
//            __FILE__, __LINE__
//          );
//        }
//        break;

//      case SkillingConstants::EVENT_CONTENT_TYPE:
//        // Events on calendars.
//        if ($viewOperation) {
//          // Admins, authors, reviewers, and instructors can see
//          // all calendar events.
//          if ($admin || $author || $reviewer || $instructor) {
//            $allow = TRUE;
//            break;
//          }
//          // Students and graders can see events for calendars for classes
//          // they are in.
//          if ($student || $grader) {
//            if ($this->isCurrentUserInClassWithEvent($node)) {
//              $allow = TRUE;
//              break;
//            }
//          }
//        }
//        elseif ($editOperation) {
//          // Admins and authors can edit calendar records.
//          if ($admin || $author) {
//            $allow = TRUE;
//            break;
//          }
//        }
//        else {
//          // Should never get here.
//          throw new SkillingInvalidValueException(
//            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
//            __FILE__, __LINE__
//          );
//        }
//        break;

      case SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE:
      case SkillingConstants::MCQ_CONTENT_TYPE:
      case SkillingConstants::SUGGESTION_CONTENT_TYPE:
        // Only admins, authors, reviewers, and instructors can see FIB nodes.
        if ($admin || $author || $reviewer || $instructor) {
          $allow = TRUE;
        }
        break;

      case SkillingConstants::REFLECT_NOTE_CONTENT_TYPE:
        if ($viewOperation) {
          // Admins can see all reflect notes.
          // Authors can see them in a view, but restrictions are added on
          // individual nodes.
          if ($admin || $author) {
            $allow = TRUE;
            break;
          }
          // Instructors can see the reflect notes of their students.
          if ($instructor) {
            if ($this->isCurrentUserInstructorOfNodeOwner($node)) {
              $allow = TRUE;
              break;
            }
          }
          // Students can see their own reflect notes.
          if ($student) {
            if ($this->isCurrentUserOwnsNode($node)) {
              $allow = TRUE;
              break;
            }
          }
        }
        elseif ($editOperation) {
          // Admins can edit reflect notes.
          if ($admin) {
            $allow = TRUE;
            break;
          }
          // Students can edit their own reflect notes.
          if ($student) {
            if ($this->isCurrentUserOwnsNode($node)) {
              $allow = TRUE;
              break;
            }
          }
        }
        else {
          // Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      default:
        // Should never get here.
        throw new SkillingInvalidValueException(
          Html::escape('ARGH! Unknown content type: ' . $contentType),
          __FILE__, __LINE__
        );
    }
    return $allow;
  }

  /**
   * Check whether the current user has access to a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field to check.
   * @param string $operation
   *   Operation, view or edit.
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Items affected.
   *
   * @return \Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result of the access check.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getFieldAccess(FieldDefinitionInterface $fieldDefinition, $operation, FieldItemListInterface $items = NULL) {
    // If $items is set, we know what entity the user is trying to access.
    // See the field permissions example in examples module.
    if ($items) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $items->getEntity();
      // Is this entity one that Skilling should access check?
      $relevant = $this->isRelevantEntity($entity);
      if (!$relevant) {
        // No.
        $result = AccessResult::neutral()->setCacheMaxAge(0);
        return $result;
      }
    }
    // Normalize the operation.
    $operation = $this->normalizeOperation($operation);
    // Sanity check on operation.
    if ($operation !== SkillingConstants::VIEW_OPERATION && $operation !== SkillingConstants::EDIT_OPERATION) {
      throw new SkillingInvalidValueException(
        Html::escape('Operation: ' . $operation . ', expected view or edit.'),
        __FILE__, __LINE__
      );
    }
    // Get the field name.
    $fieldName = $fieldDefinition->getName();
    // Check whether this is a special case.
    $access = $this->fieldAccessSpecialCaseChecker->checkForSpecialCase($fieldName, $operation);
    if ($access !== NULL) {
      $result = $access ? AccessResult::allowed() : AccessResult::forbidden();
      $result->setCacheMaxAge(0);
      return $result;
    }
    // Access is not allowed by default.
    $allow = FALSE;
    // Promote is strange case. Base field, with get.
    $isFieldPromote = $fieldName === 'promote';
    $isGetExists = method_exists($fieldDefinition, 'get');
    if (!$isGetExists || $isFieldPromote) {
      // Handle entity base fields differently.
      $result = AccessResult::neutral()->setCacheMaxAge(0);
      return $result;
      // Check base fields.txt in old code for almost-there solution.
    }
    $entityType = $fieldDefinition->get('entity_type');
    $bundle = $fieldDefinition->get('bundle');
    // Differentiate between entity types: node or user.
    switch ($entityType) {
      case 'node':
        // What content type?
        switch ($bundle) {
          case SkillingConstants::LESSON_CONTENT_TYPE:
            // Fields for lessons.
            $allow = $this->isLessonFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::DESIGN_PAGE_CONTENT_TYPE:
            // Fields for design pages.
            $allow = $this->isDesignPageFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::EXERCISE_CONTENT_TYPE:
            // Fields for exercises.
            $allow = $this->isExerciseFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE:
            // Fields for exercise submissions.
            $allow = $this->isExerciseSubmissionFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::ENROLLMENT_CONTENT_TYPE:
            // Fields for enrollments.
            $allow = $this->isEnrollmentFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::CLASS_CONTENT_TYPE:
            // Fields for .
            $allow = $this->isClassFieldAccess($operation, $fieldName);
            break;

//          case SkillingConstants::CALENDAR_CONTENT_TYPE:
//            // Fields for .
//            $allow = $this->isCalendarFieldAccess($operation, $fieldName);
//            break;

//          case SkillingConstants::EVENT_CONTENT_TYPE:
//            // Fields for .
//            $allow = $this->isEventFieldAccess($operation, $fieldName);
//            break;
//
          case SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE:
            // Fields for .
            $allow = $this->isRubricItemFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::PATTERN_CONTENT_TYPE:
            // Fields for patterns.
            $allow = $this->isPatternFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::PRINCIPLE_CONTENT_TYPE:
            // Fields for principles.
            $allow = $this->isPrincipleFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::MODEL_CONTENT_TYPE:
            // Fields for models.
            $allow = $this->isModelFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::CHARACTER_CONTENT_TYPE:
            // Fields for characters.
            $allow = $this->isCharacterFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE:
            // Fields for design pages.
            $allow = $this->isFibFieldAccess($operation, $fieldName);
            break;

//          case SkillingConstants::HISTORY_CONTENT_TYPE:
//            // Fields for .
//            $allow = $this->isHistoryFieldAccess($operation, $fieldName);
//            break;

          case SkillingConstants::REFLECT_NOTE_CONTENT_TYPE:
            // Fields for .
            $allow = $this->isReflectNoteFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::MCQ_CONTENT_TYPE:
            // Fields for .
            $allow = $this->isMcqFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::SUGGESTION_CONTENT_TYPE:
            // Fields for suggestions.
            $allow = $this->isSuggestionFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::NOTICE_CONTENT_TYPE:
            // Fields for suggestions.
            $allow = $this->isNoticeFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::BADGE_CONTENT_TYPE:
            // Fields for badges.
            $allow = $this->isBadgeFieldAccess($operation, $fieldName);
            break;

        } //End nodes entity type.
        break;

      case 'paragraph':
        // What paragraph type?
        switch ($bundle) {
          case SkillingConstants::EXERCISE_DUE_PARAGRAPH_TYPE:
            // Fields for exercises due events.
            $allow = $this->isExerciseDueFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::MCQ_RESPONSE_PARAGRAPH_TYPE:
            // Fields for MCQ responses.
            $allow = $this->isMcqResponseFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::FIB_RESPONSE_PARAGRAPH_TYPE:
            // Fields for FiB responses.
            $allow = $this->isFibResponseFieldAccess($operation, $fieldName);
            break;

          case SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE:
            // Fields for .
            $allow = $this->isRubricItemResponseFieldAccess($operation, $fieldName);
            break;

        }
        break;

      case 'user':
        // Check visibility of fields for user entities.
        $allow = $this->isUserFieldAccess($operation, $fieldName);
        break;
    }
    $result = $allow ? AccessResult::neutral() : AccessResult::forbidden();
    $result->setCacheMaxAge(0);
    return $result;
  }

  /**
   * Check access to a field of the user entity for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of field.
   *
   * @return bool
   *   True if access is allowed.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function isUserFieldAccess($operation, $fieldName) {
    // Is this a user field that Skilling knows about?
    if (!in_array($fieldName, SkillingConstants::SKILLING_USER_FIELDS)) {
      return TRUE;
    }
    // Default to not allowing access.
    $allow = FALSE;
    // Flags to make code below easier to read.
    $viewOperation = $operation === SkillingConstants::VIEW_OPERATION;
    $editOperation = $operation === SkillingConstants::EDIT_OPERATION;
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
//    if ($viewOperation) {
//      if ($student) {
//        switch ($fieldName) {
//          case SkillingConstants::FIELD_FIRST_NAME:
//          case SkillingConstants::FIELD_LAST_NAME:
//          case SkillingConstants::FIELD_INITIALS:
//          case SkillingConstants::FIELD_ABOUT:
//          case SkillingConstants::FIELD_PICTURE:
//          case SkillingConstants::FIELD_BADGES_AWARDED:
//          case SkillingConstants::FIELD_SHOW_PORTFOLIO:
//          $allow = TRUE;
//          break;
//        }
//      }
//      elseif ($admin || $instructor) {
//        $allow = TRUE;
//      }
//
//    }
//    elseif ($editOperation) {
//      if ($admin || $instructor) {
//        $allow = TRUE;
//      }
//      if ($student) {
//        // Can't change badges awarded.
//        switch ($fieldName) {
//          case SkillingConstants::FIELD_FIRST_NAME:
//          case SkillingConstants::FIELD_LAST_NAME:
//          case SkillingConstants::FIELD_INITIALS:
//          case SkillingConstants::FIELD_ABOUT:
//          case SkillingConstants::FIELD_PICTURE:
//          case SkillingConstants::FIELD_SHOW_PORTFOLIO:
//            $allow = TRUE;
//            break;
//        }
//      }
//
//    }
//    else {
//      throw new SkillingInvalidValueException(
//        Html::escape('Operation: ' . $operation . ', expected view or edit.'),
//        __FILE__, __LINE__
//      );
//    }

    if ($viewOperation || $editOperation) {
      if ($admin ||  $instructor || $author || $reviewer) {
        // See all fields of accounts they have access to.
        // This may just be their own accounts.
        // Instructor, authors, and reviewers have access to all fields,
        // even though will never use them all. This will help instructors
        // and authors keep the entire feedback cycle in mind, and help
        // reviewers understand the system.
        if ($fieldName != SkillingConstants::FIELD_SHOW_PORTFOLIO) {
          $allow = TRUE;
        }
      }
      elseif ($grader) {
        switch ($fieldName) {
          case SkillingConstants::FIELD_FIRST_NAME:
          case SkillingConstants::FIELD_LAST_NAME:
          case SkillingConstants::FIELD_INITIALS:
          case SkillingConstants::FIELD_ABOUT:
          case SkillingConstants::FIELD_PICTURE:
          case SkillingConstants::FIELD_FEEDBACK_GREETINGS:
          case SkillingConstants::FIELD_FEEDBACK_SIGNATURES:
          case SkillingConstants::FIELD_FEEDBACK_SUMMARY_GOOD:
          case SkillingConstants::FIELD_FEEDBACK_SUMMARY_NEEDS_WORK:
          case SkillingConstants::FIELD_FEEDBACK_SUMMARY_POOR:
//          case SkillingConstants::FIELD_SHOW_PORTFOLIO:
            $allow = TRUE;
            break;
        }
      }
      elseif ($student) {
        if ($fieldName == SkillingConstants::FIELD_SHOW_PORTFOLIO) {
          $allow = FALSE;
        }
        elseif ($editOperation && $fieldName === SkillingConstants::FIELD_BADGES_AWARDED) {
          $allow = FALSE;
        }
        else {
          switch ($fieldName) {
            case SkillingConstants::FIELD_FIRST_NAME:
            case SkillingConstants::FIELD_LAST_NAME:
            case SkillingConstants::FIELD_INITIALS:
            case SkillingConstants::FIELD_ABOUT:
            case SkillingConstants::FIELD_PICTURE:
            case SkillingConstants::FIELD_SHOW_PORTFOLIO:
            case SkillingConstants::FIELD_BADGES_AWARDED:
              $allow = TRUE;
              break;
          }
        }
      }
    }
    else {
      // Already checked above. Should never get here.
      throw new SkillingInvalidValueException(
        Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the lesson content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isLessonFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a lesson.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_ATTACHMENTS:
          // Everyone can see body, tags, and attachments fields of
          // the lesson content type.
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_HIDDEN_ATTACHMENTS:
          // Admins, authors, reviewers, instructors, and graders
          // can see notes, and hidden attachments fields
          // of the lesson content type.
          $allow = $admin || $author || $reviewer || $instructor || $grader;
          break;

        case SkillingConstants::FIELD_SHOW_TOC:
          // Admins and authors can see the show-toc
          // field of the lesson content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_ORDER_IN_BOOK:
          // Only admins can see the order-in-book field of
          // the lesson content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a lesson.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_SHOW_TOC:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_HIDDEN_ATTACHMENTS:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the body, tags, show-toc, attachments,
          // hidden attachments, and notes fields of the lesson content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_ORDER_IN_BOOK:
          // Only admins can edit the order-in-book field of
          // the lesson content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the design page content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isDesignPageFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
    $anonymous = $this->currentUser->isAnonymous();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a design page.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_ATTACHMENTS:
          if ($admin || $author || $instructor || $grader || $reviewer) {
            $allow = TRUE;
            break;
          }
          elseif ($student || $anonymous) {
            // Get setting value for whether students and anons are allowed
            // to see design pages.
            $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
            $allowStudentsAnons = $settings->get(SkillingConstants::SETTING_KEY_STUDENTS_ANON_SEE_DESIGN_PAGES);
            $allow = $allowStudentsAnons;
            break;
          }
          else {
            throw new SkillingException(
              Html::escape('Role checking problem.'),
              __FILE__, __LINE__
            );
          }

        case SkillingConstants::FIELD_ORDER_IN_BOOK:
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a design page.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_ATTACHMENTS:
          // Admins and authors can edit the body, tags, and attachments
          // fields of the design page content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_ORDER_IN_BOOK:
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the pattern content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isPatternFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    $reviewer = $this->currentUser->isReviewer();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a pattern.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_SUMMARY:
        case SkillingConstants::FIELD_SITUATION:
        case SkillingConstants::FIELD_ACTION:
        case SkillingConstants::FIELD_EXPLANATION:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_NEEDS:
        case SkillingConstants::FIELD_PROVIDES:
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins, authors, instructors, graders, and reviewers can see
          // the internal name, and notes fields
          // of the pattern content type.
          $allow = $admin || $author || $instructor || $reviewer;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a pattern.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_SUMMARY:
        case SkillingConstants::FIELD_SITUATION:
        case SkillingConstants::FIELD_ACTION:
        case SkillingConstants::FIELD_NEEDS:
        case SkillingConstants::FIELD_PROVIDES:
        case SkillingConstants::FIELD_EXPLANATION:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Authors and admins can edit the tags, summary, situation,
          // action, attachments, notes, and internal name field of
          // the pattern content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Only admins can edit the where-referenced field of
          // the pattern content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the principle content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isPrincipleFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $reviewer = $this->currentUser->isReviewer();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a principle.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Everyone can see the tags, body, attachments, and
          // where-referenced fields of the principle content type.
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins, authors, instructors, graders, and reviewers can
          // see the internal name, and notes
          // fields of the principle content type.
          $allow = $admin || $author || $instructor || $grader || $reviewer;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a pPrinciple.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the tags, body, attachments,
          // internal name, and notes fields of the principle content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Only admins can edit the where-referenced field of
          // the principle content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the model content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isModelFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    $reviewer = $this->currentUser->isReviewer();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a model.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Everyone can see the tags, body, and attachments,
          // and where-referenced fields of the model content type.
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins, authors, instructors, graders, and reviewers can
          // see the internal name, notes fields of the pattern content type.
          $allow = $admin || $author || $instructor || $reviewer;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a model.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the tags, body, attachments,
          // internal name, and notes fields of the model content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Only admins can edit the where-referenced field of
          // the model content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the character content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isCharacterFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();

    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a character.
        case SkillingConstants::FIELD_PHOTO:
        case SkillingConstants::FIELD_CAPTION:
          // Everyone can see the photo and caption fields of
          // the character content type.
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Admins, authors, and reviewers can see the internal name, notes,
          // and where-referenced fields of the character content type.
          $allow = $admin || $author || $reviewer;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a character.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_PHOTO:
        case SkillingConstants::FIELD_CAPTION:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the character field of
          // the character content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Only admins can edit the where-referenced field of
          // the character content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the suggestion content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isSuggestionFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();

    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a suggestion.
        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_PAGE:
        case SkillingConstants::FIELD_SUGGESTION:
        case SkillingConstants::FIELD_SUGGESTION_STATUS:
        case SkillingConstants::FIELD_NOTES:
          // Admins, authors, and instructors can see
          // the suggestion, and notes fields of the suggestion content type.
          $allow = $admin || $author || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a suggestion.
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the suggestion, and notes fields of
          // the suggestion content type.
          $allow = $admin || $author || $instructor;
          break;

        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_PAGE:
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_SUGGESTION:
        case SkillingConstants::FIELD_SUGGESTION_STATUS:
          // Admins and authors can edit the suggestion, and notes fields of
          // the suggestion content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the MCQ content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isMcqFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing an MCQ.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_QUESTION:
        case SkillingConstants::FIELD_RESPONSES:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Admins, authors, reviewers, instructors, and graders can see
          // the notes, internal name, and where-referenced fields
          // of the MCQ content type.
          $allow = $admin || $author || $reviewer || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing an MCQ.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_QUESTION:
        case SkillingConstants::FIELD_RESPONSES:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_INTERNAL_NAME:
          // Admins and authors can edit the question, responses, notes,
          // and internal name field of the MCQ content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Admins can edit the where-referenced field
          // of the MCQ content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the FiB content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isFibFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a FiB.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_QUESTION:
        case SkillingConstants::FIELD_RESPONSE_TYPE:
        case SkillingConstants::FIELD_FIB_RESPONSES:
        case SkillingConstants::FIELD_NO_MATCH_RESPONSE:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
          $allow = $admin || $author || $reviewer || $instructor || $grader;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a FiB.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_QUESTION:
        case SkillingConstants::FIELD_RESPONSE_TYPE:
        case SkillingConstants::FIELD_FIB_RESPONSES:
        case SkillingConstants::FIELD_NO_MATCH_RESPONSE:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_INTERNAL_NAME:
          // Admins and authors, reviewers, instructors, and graders can edit
          // the question, responses, notes, and internal name fields
          // of the FiB content type.
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          // Admins can edit the where-referenced field
          // of the FiB content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the calendar content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
//  protected function isCalendarFieldAccess($operation, $fieldName) {
//    // Deny access by default.
//    $allow = FALSE;
//    $admin = $this->currentUser->isAdministrator();
//    $author = $this->currentUser->isAuthor();
//    $reviewer = $this->currentUser->isReviewer();
//    $instructor = $this->currentUser->isInstructor();
//    $grader = $this->currentUser->isGrader();
//    if ($operation === SkillingConstants::VIEW_OPERATION) {
//      switch ($fieldName) {
//        // Viewing a calendar.
//        case SkillingConstants::FIELD_TITLE:
//        case SkillingConstants::FIELD_DAYS:
//        case SkillingConstants::FIELD_BODY:
//        case SkillingConstants::FIELD_CALENDAR_EVENTS:
//          $allow = TRUE;
//          break;
//
//        case SkillingConstants::FIELD_NOTES:
//          // Admins, authors, reviewers, instructors, and graders can see
//          // the notes field of the calendar content type.
//          $allow = $admin || $author || $reviewer || $instructor || $grader;
//          break;
//
//        default:
//          $this->makeUnknownFieldErrorReportForUser($fieldName);
//      }
//    }
//    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
//      switch ($fieldName) {
//        // Editing a .
//        case SkillingConstants::FIELD_TITLE:
//        case SkillingConstants::FIELD_DAYS:
//        case SkillingConstants::FIELD_BODY:
//        case SkillingConstants::FIELD_NOTES:
//        case SkillingConstants::FIELD_CALENDAR_EVENTS:
//          // Admins and authors can edit the days, body (description),
//          // and notes fields of the calendar content type.
//          $allow = $admin || $author || $instructor;
//          break;
//
//        default:
//          $this->makeUnknownFieldErrorReportForUser($fieldName);
//      }
//    }
//    else {
//      throw new SkillingInvalidValueException(
//        Html::escape('Operation not supported: ' . $operation),
//        __FILE__, __LINE__
//      );
//    }
//    return $allow;
//  }

//  /**
//   * Check field access for the event content type for the current user.
//   *
//   * @param string $operation
//   *   Operation, view or edit.
//   * @param string $fieldName
//   *   Name of the field to check access to.
//   *
//   * @return bool
//   *   True means access is not blocked. False means it is blocked.
//   *
//   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
//   */
//  protected function isEventFieldAccess($operation, $fieldName) {
//    // Deny access by default.
//    $allow = FALSE;
//    $admin = $this->currentUser->isAdministrator();
//    $author = $this->currentUser->isAuthor();
//    $instructor = $this->currentUser->isInstructor();
//    if ($operation === SkillingConstants::VIEW_OPERATION) {
//      switch ($fieldName) {
//        // Viewing an event.
//        case SkillingConstants::FIELD_TITLE:
//        case SkillingConstants::FIELD_BODY:
//        case SkillingConstants::FIELD_TAGS:
//          // Anyone can see.
//          $allow = TRUE;
//          break;
//
//        case SkillingConstants::FIELD_EVENT_TYPE:
//        case SkillingConstants::FIELD_EXERCISE:
//        case SkillingConstants::FIELD_INTERNAL_NAME:
//        case SkillingConstants::FIELD_NOTES:
//          if ($admin || $author || $instructor) {
//            $allow = TRUE;
//            break;
//          }
//          break;
//
//        default:
//          $this->makeUnknownFieldErrorReportForUser($fieldName);
//      }
//    }
//    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
//      switch ($fieldName) {
//        // Editing an event .
//        case SkillingConstants::FIELD_TITLE:
//        case SkillingConstants::FIELD_BODY:
//        case SkillingConstants::FIELD_TAGS:
//        case SkillingConstants::FIELD_EVENT_TYPE:
//        case SkillingConstants::FIELD_EXERCISE:
//        case SkillingConstants::FIELD_INTERNAL_NAME:
//        case SkillingConstants::FIELD_NOTES:
//          if ($admin || $author || $instructor) {
//            $allow = TRUE;
//            break;
//          }
//          break;
//
//        default:
//          $this->makeUnknownFieldErrorReportForUser($fieldName);
//      }
//    }
//    else {
//      throw new SkillingInvalidValueException(
//        Html::escape('Operation not supported: ' . $operation),
//        __FILE__, __LINE__
//      );
//    }
//    return $allow;
//  }

  /**
   * Check field access for the exercise content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isExerciseFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing an exercise.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_WHERE_REFERENCED:
        case SkillingConstants::FIELD_CHALLENGE:
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_RUBRIC_ITEMS:
        case SkillingConstants::FIELD_HIDDEN_ATTACHMENTS:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author || $reviewer || $instructor || $grader;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      // Editing an exercise.
      switch ($fieldName) {
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_TAGS:
        case SkillingConstants::FIELD_CHALLENGE:
        case SkillingConstants::FIELD_ATTACHMENTS:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_RUBRIC_ITEMS:
        case SkillingConstants::FIELD_HIDDEN_ATTACHMENTS:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author;
          break;

        case SkillingConstants::FIELD_WHERE_REFERENCED:
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Report to the user that a field has been blocked.
   *
   * @param string $fieldMachineName
   *   The machine name of the field.
   */
  protected function makeUnknownFieldErrorReportForUser($fieldMachineName) {
    $message = $this->t(
      "Unknown field blocked: @field. Ask your admin to check Skilling's documentation.",
      ['@field' => $fieldMachineName]
    );
    $this->messenger->addWarning($message);
  }

  /**
   * Check field access for exercise submission content type for current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isExerciseSubmissionFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    $student = $this->currentUser->isStudent();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a submission.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_INITIALS:
        case SkillingConstants::FIELD_EXERCISE:
        case SkillingConstants::FIELD_VERSION:
        case SkillingConstants::FIELD_SOLUTION:
        case SkillingConstants::FIELD_SUBMITTED_FILES:
        case SkillingConstants::FIELD_DIFFICULTY:
        case SkillingConstants::FIELD_DIFFICULTY_REASONS:
        case SkillingConstants::FIELD_WHEN_FEEDBACK_GIVEN:
        case SkillingConstants::FIELD_FEEDBACK_MESSAGE:
        case SkillingConstants::FIELD_COMPLETE:
        case SkillingConstants::FIELD_OVERALL_EVALUATION:
        case SkillingConstants::FIELD_INCLUDE_IN_PORTFOLIO:
        case SkillingConstants::FIELD_GRADER_ATTACHMENTS:
          $allow = $admin || $instructor || $student;
          break;

        case SkillingConstants::FIELD_FEEDBACK_SOURCE:
        case SkillingConstants::FIELD_FEEDBACK_RESPONSES:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a submission.
        case SkillingConstants::FIELD_INITIALS:
        case SkillingConstants::FIELD_SOLUTION:
        case SkillingConstants::FIELD_SUBMITTED_FILES:
        case SkillingConstants::FIELD_DIFFICULTY:
        case SkillingConstants::FIELD_DIFFICULTY_REASONS:
          $allow = $admin || $instructor || $student;
          break;

        case SkillingConstants::FIELD_INCLUDE_IN_PORTFOLIO:
          $allow = $admin || $student;
          break;

        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_EXERCISE:
        case SkillingConstants::FIELD_VERSION:
        case SkillingConstants::FIELD_FEEDBACK_SOURCE:
        case SkillingConstants::FIELD_FEEDBACK_RESPONSES:
        case SkillingConstants::FIELD_WHEN_FEEDBACK_GIVEN:
        case SkillingConstants::FIELD_FEEDBACK_MESSAGE:
        case SkillingConstants::FIELD_COMPLETE:
        case SkillingConstants::FIELD_OVERALL_EVALUATION:
        case SkillingConstants::FIELD_GRADER_ATTACHMENTS:
        $allow = $admin || $instructor;
          break;

        case SkillingConstants::FIELD_NOTES:
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the enrollment content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isEnrollmentFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing an enrollment.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_CLASS:
        case SkillingConstants::FIELD_CLASS_ROLES:
        case SkillingConstants::FIELD_COMPLETION_SCORE:
        case SkillingConstants::FIELD_WHEN_SCORE_UPDATED:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY:
          $allow = $admin|| $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing an enrollment.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_CLASS:
        case SkillingConstants::FIELD_USER:
        case SkillingConstants::FIELD_CLASS_ROLES:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY:
          $allow = $admin || $instructor;
          break;

        case SkillingConstants::FIELD_COMPLETION_SCORE:
        case SkillingConstants::FIELD_WHEN_SCORE_UPDATED:
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the class content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isClassFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a class.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_WHEN_STARTS:
        case SkillingConstants::FIELD_EXERCISES_DUE:
        case SkillingConstants::FIELD_DEFAULT_MAX_SUBMISSIONS:
          $allow = TRUE;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
//        case SkillingConstants::FIELD_CALENDAR:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a class.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_BODY:
        case SkillingConstants::FIELD_WHEN_STARTS:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_EXERCISES_DUE:
        case SkillingConstants::FIELD_DEFAULT_MAX_SUBMISSIONS:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $instructor || $author;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the MCQ response paragraph for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isMcqResponseFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      // For all fields...
      $allow = $admin || $author || $reviewer || $instructor;
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      // For all fields...
      $allow = $admin || $author;
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the FiB response paragraph for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isFibResponseFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      // For all fields...
      $allow = $admin || $author || $reviewer || $instructor;
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      // For all fields...
      $allow = $admin || $author;
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the rubric item content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isRubricItemFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a rubric item.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_CATEGORIES:
        case SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a rubric item.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_CATEGORIES:
        case SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for RI response paragraph type for current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isRubricItemResponseFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a rubric item response.
        case SkillingConstants::FIELD_RESPONSE_TO_STUDENT:
        case SkillingConstants::FIELD_COMPLETES_RUBRIC_ITEM:
        case SkillingConstants::FIELD_EXERCISES:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author || $instructor;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a rubric item response.
        case SkillingConstants::FIELD_RESPONSE_TO_STUDENT:
        case SkillingConstants::FIELD_COMPLETES_RUBRIC_ITEM:
        case SkillingConstants::FIELD_EXERCISES:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $author;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the reflect note content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isReflectNoteFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    $author = $this->currentUser->isAuthor();
    $student = $this->currentUser->isStudent();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a reflect note.
        case SkillingConstants::FIELD_TITLE:
          $allow = $admin || $instructor;
          break;

        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
          $allow = $admin || $instructor || $author;
          break;

        case SkillingConstants::FIELD_NODE:
        case SkillingConstants::FIELD_NOTE:
          $allow = $admin || $instructor || $student || $author;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a reflect note.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_INTERNAL_NAME:
        case SkillingConstants::FIELD_NOTES:
        case SkillingConstants::FIELD_NODE:
          $allow = $admin || $instructor;
          break;

        case SkillingConstants::FIELD_NOTE:
          $allow = $admin || $instructor || $student;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the notice content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isNoticeFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $authenticated = $this->currentUser->isAuthenticated();

    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a notice.
        case SkillingConstants::FIELD_MESSAGE:
        case SkillingConstants::FIELD_WHEN_READ:
          $allow = $authenticated;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a notice.
        case SkillingConstants::FIELD_MESSAGE:
        case SkillingConstants::FIELD_WHEN_READ:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit the suggestion, and notes fields of
          // the suggestion content type.
          $allow = $admin;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check field access for the badge content type for the current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isBadgeFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      switch ($fieldName) {
        // Viewing a badge.
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_NUMBER_CHALLENGES:
        case SkillingConstants::FIELD_BADGE_IMAGE:
        case SkillingConstants::FIELD_NOTES:
          $allow = TRUE;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      switch ($fieldName) {
        // Editing a .
        case SkillingConstants::FIELD_TITLE:
        case SkillingConstants::FIELD_NUMBER_CHALLENGES:
        case SkillingConstants::FIELD_BADGE_IMAGE:
        case SkillingConstants::FIELD_NOTES:
          // Admins and authors can edit\.
          $allow = $admin || $author;
          break;

        default:
          $this->makeUnknownFieldErrorReportForUser($fieldName);
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }


  /**
   * What access does the current user have to another user's user page?
   *
   * @param \Drupal\skilling\SkillingUser $user
   *   The user whose page is being accessed.
   * @param string $operation
   *   Operation, e.g., view, edit.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function getUserEntityAccess(SkillingUser $user, $operation) {
    $allow = $this->isUserEntityAccess($user, $operation);
    if ($allow) {
      $result = AccessResult::neutral();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;
  }

  /**
   * Does the current user have access to the given user?
   *
   * @param \Drupal\skilling\SkillingUser $user
   *   The user being accessed.
   * @param string $operation
   *   View or edit.
   *
   * @return bool
   *   True if has access
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function isUserEntityAccess(SkillingUser $user, $operation) {
    // Default to not allowing access.
    $allow = FALSE;
    // Normalize operation name.
    $operation = $this->normalizeOperation($operation);
    // Some flag to make code more readable.
    $viewOperation = $operation === SkillingConstants::VIEW_OPERATION;
    $editOperation = $operation === SkillingConstants::EDIT_OPERATION;
    // Make role flags in local vars to make code easier to read.
    $admin = $this->currentUser->isAdministrator();
    $instructor = $this->currentUser->isInstructor();
    // Start checking access.
    if ($viewOperation) {
      if ($admin) {
        $allow = TRUE;
      }
      // People can see their own accounts.
      elseif ($this->currentUser->id() == $user->id()) {
        $allow = TRUE;
      }
      elseif ($instructor) {
        // See people in own classes.
        $isStudent = $this->checkUserRelationshipsService->isUserInstructorOfUser(
          $this->currentUser, $user
        );
        $isGrader = $this->checkUserRelationshipsService->isUserGraderForInstructor(
          $user, $this->currentUser
        );
        // Instructors can see each other.
        $isFellowInstructor = $user->isInstructor();
        $allow = $isStudent || $isGrader || $isFellowInstructor;
      } // End instructor.
    } // End view operation.
    elseif ($editOperation) {
      if ($admin) {
        $allow = TRUE;
      }
      // People can edit their own accounts.
      elseif ($this->currentUser->id() == $user->id()) {
        $allow = TRUE;
      }
      elseif ($instructor) {
        // See edit accounts for people in own classes.
        $isStudent = $this->checkUserRelationshipsService->isUserInstructorOfUser(
          $this->currentUser, $user
        );
        $isGrader = $this->checkUserRelationshipsService->isUserGraderForInstructor(
          $user, $this->currentUser
        );
        $allow = $isStudent || $isGrader;
      }
    } // End edit operation.
    else {
      // Should never get here.
      throw new SkillingInvalidValueException(
        Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Check whether the current user has access to a block.
   *
   * @param \Drupal\block\BlockInterface $block
   *   The block to check access to.
   *
   * @return \Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral|null
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function getBlockAccess(BlockInterface $block) {
    $allow = TRUE;
    // Convenience flags.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
    $anon = $this->currentUser->isAnonymous();
    $blockId = $block->getPluginId();
    switch ($blockId) {
      case SkillingConstants::TIMELINE_BLOCK_ID:
        // Timeline block.
        // Don't show timeline on submission node create/edit.
        if ($this->skillingUtilities->isSubmissionAddEditForm()) {
          $allow = FALSE;
        }
        else {
          // Instructors, graders, and students can see timeline.
          $allow = $instructor || $grader || $student;
        }
        break;

      case SkillingConstants::CLASS_SELECTOR_BLOCK_ID:
        // Class selector.
        // Don't show timeline on submission node create/edit.
        if ($this->skillingUtilities->isSubmissionAddEditForm()) {
          $allow = FALSE;
        }
        else {
          $allow = count($this->currentClass->getCurrentEnrollments()) > 1;
        }
        break;

      case SkillingConstants::BOOK_NAV_PLUGIN_ID:
        // Nav for lessons, or another book.
        // Don't show timeline on submission node create/edit.
        if ($this->skillingUtilities->isSubmissionAddEditForm()) {
          $allow = FALSE;
        }
        else {
          // Get the book this is for.
          $blockSettings = $block->get('settings');
          if (!isset($blockSettings[SkillingConstants::BOOK_NAV_BOOK_TO_SHOW])) {
            throw new SkillingValueMissingException(
              Html::escape('book_to_show missing'),
              __FILE__, __LINE__
            );
          }
          $bid = $blockSettings[SkillingConstants::BOOK_NAV_BOOK_TO_SHOW];
          // Load the root node.
          $rootNode = $this->entityTypeManager->getStorage('node')->load($bid);
          if (!$rootNode) {
            return AccessResult::forbidden();
          }
          // Is the block for a design page?
          if ($rootNode->bundle() === SkillingConstants::DESIGN_PAGE_CONTENT_TYPE) {
            // Aye, a design page.
            if ($admin || $author || $reviewer || $instructor || $grader) {
              $allow = TRUE;
            }
            elseif ($student || $anon) {
              // Access depends on Skilling config.
              $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
              $allow = $settings->get(SkillingConstants::SETTING_KEY_STUDENTS_ANON_SEE_DESIGN_PAGES);
            }
          } // End book root is design page.
        } // Don't show timeline on submission node create/edit.
        break;

      case SkillingConstants::BADGE_BLOCK_ID:
        // Badges.
        if ($student) {
          $allow = TRUE;
        }
        break;

    } // End switch.
    // Make access object, based on allow flag.
    $result = $allow ? AccessResult::neutral() : AccessResult::forbidden();
    $result->setCacheMaxAge(0);
    return $result;
  }

  /**
   * Add clauses to search query to exclude content types for some users.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The search query object.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function alterSearchQuery(AlterableInterface $query) {
    // List of content types to exclude.
    $excludedContentTypes = [];
    // Convenience flags.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
    $anon = $this->currentUser->isAnonymous();
    // Lessons - nothing to do, everyone can see them.
    // Design pages.
    if ($admin || $author || $reviewer || $instructor) {
      // Nothing to do.
    }
    elseif ($grader || $student || $anon) {
      // Depends on config.
      /** @var \Drupal\Core\Config\ImmutableConfig $settings */
      $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
      $allow = $settings->get(SkillingConstants::SETTING_KEY_STUDENTS_ANON_SEE_DESIGN_PAGES);
      if (!$allow) {
        $excludedContentTypes[] = SkillingConstants::DESIGN_PAGE_CONTENT_TYPE;
      }
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! Roleless.'), __FILE__, __LINE__);
    }
    // Exercises - nothing to do, everyone can see them.
    // Submissions not searchable by anyone.
    $excludedContentTypes[] = SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE;
    // Enrollments not searchable.
    $excludedContentTypes[] = SkillingConstants::ENROLLMENT_CONTENT_TYPE;
    // Classes - nothing to do, everyone can see them.
    // Calendar nodes searchable by some.
    if ($admin || $author || $reviewer || $instructor || $grader) {
      // Nothing to do.
    }
    elseif ($student || $anon) {
//      $excludedContentTypes[] = SkillingConstants::CALENDAR_CONTENT_TYPE;
//      $excludedContentTypes[] = SkillingConstants::EVENT_CONTENT_TYPE;
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! Rolelessness?'), __FILE__, __LINE__);
    }
    // Rubric items and responses.
    if ($admin || $author || $reviewer || $instructor) {
      // Nothing to do.
    }
    elseif ($grader || $student || $anon) {
      $excludedContentTypes[] = SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE;
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! Role make no sense.'), __FILE__, __LINE__);
    }
    // Characters.
    if ($admin || $author || $reviewer) {
      // Nothing to do.
    }
    elseif ($instructor || $grader || $student || $anon) {
      $excludedContentTypes[] = SkillingConstants::CHARACTER_CONTENT_TYPE;
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! Role! ARGH!'), __FILE__, __LINE__);
    }
    // Patterns - nothing to do, everyone can see them.
    // Principles - nothing to do, everyone can see them.
    // Models - nothing to do, everyone can see them.
    // FiBs.
    if ($admin || $author || $reviewer || $instructor) {
      // Nothing to do.
    }
    elseif ($grader || $student || $anon) {
      $excludedContentTypes[] = SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE;
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! FiB role!'), __FILE__, __LINE__);
    }
    // MCQs.
    if ($admin || $author || $reviewer || $instructor) {
      // Nothing to do.
    }
    elseif ($grader || $student || $anon) {
      $excludedContentTypes[] = SkillingConstants::MCQ_RESPONSE_PARAGRAPH_TYPE;
    }
    else {
      throw new SkillingException(
        Html::escape('ARGH! MCQ role mystery!'), __FILE__, __LINE__);
    }
//    // History not searchable by anyone.
//    $excludedContentTypes[] = SkillingConstants::HISTORY_CONTENT_TYPE;
    // Reflect notes not searchable by anyone.
    $excludedContentTypes[] = SkillingConstants::REFLECT_NOTE_CONTENT_TYPE;
    if (count($excludedContentTypes) > 0) {
      // Add extra restrictions.
      $aliasNumber = 0;
      foreach ($excludedContentTypes as $excludedContentType) {
        $aliasNumber++;
        $query->join('node', 'n' . $aliasNumber, 'n.nid = n' . $aliasNumber . '.nid');
        $query->condition('n' . $aliasNumber . '.type', $excludedContentType, '<>');
      }
    }
  }

  /**
   * Check for use of restricted Skilling tags that is not allowed.
   *
   * Assumes that the field being checked uses the Skilling
   * format for its content. This should be checked
   * by the caller.
   *
   * The access rules are in the module notes.
   *
   * @param string $bundle
   *   The bundle, e.g., lesson.
   * @param string $fieldName
   *   The name of the field.
   * @param string $value
   *   The value the user entered for the field.
   *
   * @return bool
   *   True if tag use is allowed, else false.
   */
  public function checkSkillingTagUse($bundle, $fieldName, $value) {
    /* @var bool $allowRestrictedTags Is use of restricted tags allowed? */
    $allowRestrictedTags = FALSE;
    if ($this->currentUser->isAuthor() || $this->currentUser->isAdministrator()) {
      // Authors and admins can do whatever they want.
      $allowRestrictedTags = TRUE;
    }
    if ($allowRestrictedTags) {
      return TRUE;
    }
    // Not allowed to use restricted tags.
    // Did the user enter any restricted tags?
    $restrictedTagsUsed = $this->parser->isCustomTagUsedInContent($value);
    return !$restrictedTagsUsed;
  }

  /**
   * Is current user instructor of node owner?
   *
   * Check whether the current user is an instructor of the owner of a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return bool
   *   True if current user is node owner's instructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isCurrentUserInstructorOfNodeOwner(NodeInterface $node) {
    $nodeOwnerUid = $node->getOwnerId();
    $owner = $this->skillingUserFactory->makeSkillingUser($nodeOwnerUid);
    $currentUserTeachesOwner = $this->checkUserRelationshipsService->isUserInstructorOfUser(
      $this->currentUser, $owner
    );
    return $currentUserTeachesOwner;
  }

  /**
   * Is current user instructor of submitter?
   *
   * Check whether the current user is an instructor of the student who
   * submitted an exercise submission.
   *
   * @param \Drupal\node\NodeInterface $submssion
   *   The submission.
   *
   * @return bool
   *   True if current user is submitting student's instructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isCurrentUserInstructorOfSubmissionStudent(NodeInterface $submssion) {
    $studentUid = $submssion->get(SkillingConstants::FIELD_USER)->target_id;
    $student = $this->skillingUserFactory->makeSkillingUser($studentUid);
    $currentTeachesStudent = $this->checkUserRelationshipsService->isUserInstructorOfUser(
      $this->currentUser, $student
    );
    return $currentTeachesStudent;
  }

  /**
   * Is current user grader of submitter?
   *
   * Check whether the current user is a grader of the student who
   * submitted an exercise submission.
   *
   * @param \Drupal\node\NodeInterface $submssion
   *   The submission.
   *
   * @return bool
   *   True if current user is submitting student's grader.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isCurrentUserGraderOfSubmissionStudent(NodeInterface $submssion) {
    $studentUid = $submssion->get(SkillingConstants::FIELD_USER)->target_id;
    $student = $this->skillingUserFactory->makeSkillingUser($studentUid);
    $currentTeachesStudent = $this->checkUserRelationshipsService->isUserGraderOfUser(
      $this->currentUser, $student
    );
    return $currentTeachesStudent;
  }

  /**
   * Does the current user own the given node?
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check.
   *
   * @return bool
   *   True if current user owns node.
   */
  protected function isCurrentUserOwnsNode(NodeInterface $node) {
    $nodeOwnerUid = $node->getOwnerId();
    $result = $nodeOwnerUid === $this->currentUser->id();
    return $result;
  }

  /**
   * Does the current user have access to a paragraph?
   *
   * Returns an AccessResult, based on call to isParagraphAccess().
   *
   * Field access is checked separately, in getFieldAccess()
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph.
   * @param string $operation
   *   Operation to check - view, edit, update.
   *
   * @return \Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getParagraphAccess(ParagraphInterface $paragraph, $operation) {
    $allow = $this->isParagraphAccess($paragraph, $operation);
    if ($allow) {
      $result = AccessResult::neutral();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;
  }

  /**
   * Does the current user have access to a given paragraph?
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph to check.
   * @param string $operation
   *   View or edit.
   *
   * @return bool
   *   Result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  public function isParagraphAccess(ParagraphInterface $paragraph, $operation) {
    // Only check paragraph types that are controlled by Skilling.
    $paragraphType = $paragraph->getType();
    if (!in_array($paragraphType, SkillingConstants::SKILLING_PARAGRAPH_TYPES)) {
      // Not a Skilling paragraph type. Other code determines its fate.
      return TRUE;
    }
    // Default to not allowing access.
    $allow = FALSE;
    // Normalize operation name.
    $operation = $this->normalizeOperation($operation);
    // Some flag to make code more readable.
    $viewOperation = $operation === SkillingConstants::VIEW_OPERATION;
    $editOperation = $operation === SkillingConstants::EDIT_OPERATION;
    // Make role flags in local vars to make code easier to read.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $reviewer = $this->currentUser->isReviewer();
    $instructor = $this->currentUser->isInstructor();
    $grader = $this->currentUser->isGrader();
    $student = $this->currentUser->isStudent();
    // Start checking access.
    switch ($paragraphType) {
      case SkillingConstants::EXERCISE_DUE_PARAGRAPH_TYPE:
        if ($viewOperation) {
          if ($admin || $author || $reviewer || $instructor) {
            $allow = TRUE;
            break;
          }
          if ($student || $grader) {
            // Only see events for classes they are enrolled in.
            /** @var \Drupal\node\NodeInterface $event */
              $allow = TRUE;
              break;
          }
        }
        elseif ($editOperation) {
          $allow = $admin || $author || $instructor;
        }
        else {
          // Already checked above. Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::MCQ_RESPONSE_PARAGRAPH_TYPE:
        if ($viewOperation) {
          if ($admin || $author || $reviewer || $instructor) {
            $allow = TRUE;
            break;
          }
        }
        elseif ($editOperation) {
          $allow = $admin || $author;
        }
        else {
          // Already checked above. Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::FIB_RESPONSE_PARAGRAPH_TYPE:
        if ($viewOperation) {
          if ($admin || $author || $reviewer || $instructor) {
            $allow = TRUE;
            break;
          }
        }
        elseif ($editOperation) {
          $allow = $admin || $author;
        }
        else {
          // Already checked above. Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

      case SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE:
        if ($viewOperation) {
          // Admins, authors, reviewers, instructors, and graders can see these
          // content types.
          if ($admin || $author || $reviewer || $instructor || $grader) {
            $allow = TRUE;
          }
          break;
        }
        elseif ($editOperation) {
          // Only admins and authors can edit.
          $allow = $admin || $author;
        }
        else {
          // Should never get here.
          throw new SkillingInvalidValueException(
            Html::escape('ARGH! Operation: ' . $operation . ', expected view or edit.'),
            __FILE__, __LINE__
          );
        }
        break;

    }
    return $allow;
  }

  /**
   * Check field access for the exercise due paragraph type for current user.
   *
   * @param string $operation
   *   Operation, view or edit.
   * @param string $fieldName
   *   Name of the field to check access to.
   *
   * @return bool
   *   True means access is not blocked. False means it is blocked.
   *
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   */
  protected function isExerciseDueFieldAccess($operation, $fieldName) {
    // Deny access by default.
    $allow = FALSE;
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    $reviewer = $this->currentUser->isReviewer();
    if ($operation === SkillingConstants::VIEW_OPERATION) {
      if ($admin || $author || $instructor || $reviewer) {
        $allow = TRUE;
      }
    }
    elseif ($operation === SkillingConstants::EDIT_OPERATION) {
      if ($admin || $author || $instructor) {
        $allow = TRUE;
      }
    }
    else {
      throw new SkillingInvalidValueException(
        Html::escape('Operation not supported: ' . $operation),
        __FILE__, __LINE__
      );
    }
    return $allow;
  }

  /**
   * Normalize operation name to edit or view.
   *
   * @param string $operationName
   *   Raw name.
   *
   * @return null|string
   *   Normalized name.
   */
  protected function normalizeOperation($operationName) {
    $result = NULL;
    switch ($operationName) {
      case 'edit':
      case 'update':
      case 'delete':
      case 'create':
        $result = SkillingConstants::EDIT_OPERATION;
        break;

      case 'view':
      case 'view label':
        $result = SkillingConstants::VIEW_OPERATION;
        break;
    }
    return $result;
  }

  /**
   * @param \Drupal\file\Entity\File $file
   * @param $operation
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function getFileAccess(File $file, $operation) {
    $allow = $this->isFileAccess($file, $operation);
    if ($allow) {
      $result = AccessResult::allowed();
//      $result = AccessResult::neutral();
    }
    else {
      $result = AccessResult::forbidden();
    }
    $result->setCacheMaxAge(0);
    return $result;

  }

  /**
   * @param \Drupal\file\Entity\File $file
   * @param $operation
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isFileAccess(File $file, $operation) {
    if ($this->currentUser->isAdministrator()) {
      return TRUE;
    }
    // Current user owns the file?
    $currentUserUid = $this->currentUser->id();
    $fileOwnerUid = $file->getOwnerId();
    if ($currentUserUid === $fileOwnerUid) {
      return TRUE;
    }
    // Is the current user an instructor of the file owner?
    if ($this->checkUserRelationshipsService->isUserUidInstructorOfUserUid($currentUserUid, $fileOwnerUid)) {
      // Access is granted.
      return TRUE;
    }
    // Is the current user a grader of the file owner?
    if ($this->checkUserRelationshipsService->isUserUidGraderOfUserUid($currentUserUid, $fileOwnerUid)) {
      // Access is granted.
      return TRUE;
    }
    //Is this file attached by grader to a submission by the current user?
    $fileReferences = file_get_file_references($file);
    foreach ($fileReferences as $fieldName => $entitiesTypesWithField) {
      if ($fieldName === SkillingConstants::FIELD_GRADER_ATTACHMENTS) {
        foreach ($entitiesTypesWithField as $entityTypeName => $entitiesWithField) {
          if ($entityTypeName === 'node') {
            /** @var \Drupal\node\NodeInterface $referencingNode */
            foreach ($entitiesWithField as $nid => $referencingNode) {
              if ($referencingNode->bundle() === SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
                //Is the submission owned by the current user?
                if ($referencingNode->getOwnerId() === $this->currentUser->id()) {
                  // Access is granted.
                  return TRUE;
                }
              }
            }
          }
        }
      }
    }
    // All access to attachements for right roles.
    $admin = $this->currentUser->isAdministrator();
    $author = $this->currentUser->isAuthor();
    $instructor = $this->currentUser->isInstructor();
    $reviewer = $this->currentUser->isReviewer();
    $grader = $this->currentUser->isGrader();
    if ($admin || $author || $instructor || $reviewer || $grader) {
      $references = file_get_file_references($file);
      $fieldNamesReferencing = array_keys($references);
      $isHiddenAttachments = FALSE;
      foreach ($fieldNamesReferencing as $fieldNameReferencing) {
        $isAttachment = $fieldNameReferencing === SkillingConstants::FIELD_HIDDEN_ATTACHMENTS
          || $fieldNameReferencing === SkillingConstants::FIELD_ATTACHMENTS;
        if ($isAttachment) {
          $isHiddenAttachments = TRUE;
          break;
        }
      }
      if ($isHiddenAttachments) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
