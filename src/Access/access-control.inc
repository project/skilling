<?php

/**
 * @file
 * Hooks for access control to blocks, fields, etc.
 *
 * Include this file in skilling.module.
 *
 * @see notes/security/hooks.txt for more.
 */

use Drupal\block\BlockInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileInterface;
use Drupal\skilling\SkillingConstants;

/**
 * Implements hook_entity_access().
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\skilling\Exception\SkillingException
 */
function skilling_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  // Limit user access to entities, specifically nodes and users.
  // Uses a Skilling service to do the actual work.
  /** @var \Drupal\skilling\Access\SkillingAccessChecker $accessChecker */
  $accessChecker = \Drupal::service('skilling.access_checker');
  $result = $accessChecker->getEntityAccess($entity, $operation);
  return $result;
}

/**
 * Implements hook_block_access().
 *
 * @throws \Drupal\skilling\Exception\SkillingValueMissingException
 * @throws \Drupal\skilling\Exception\SkillingNotFoundException
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function skilling_block_access(BlockInterface $block, $operation, AccountInterface $account) {
  /** @var \Drupal\skilling\Access\SkillingAccessChecker $accessChecker */
  $accessChecker = \Drupal::service('skilling.access_checker');
  $result = $accessChecker->getBlockAccess($block);
  return $result;
}

/**
 * Implements hook_entity_field_access().
 *
 * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
 * @throws \Drupal\skilling\Exception\SkillingException
 */
function skilling_entity_field_access($operation, FieldDefinitionInterface $fieldDefinition, \Drupal\Core\Session\AccountInterface $account, FieldItemListInterface $items = NULL) {
  /** @var \Drupal\skilling\Access\SkillingAccessChecker $accessChecker */
  $accessChecker = \Drupal::service('skilling.access_checker');
  $result = $accessChecker->getFieldAccess($fieldDefinition, $operation, $items);
  return $result;
}

/**
 * Implements hook_query_TAG_alter().
 *
 * @throws \Drupal\skilling\Exception\SkillingException
 */
function skilling_query_search_node_search_alter(AlterableInterface $query) {
  /** @var \Drupal\skilling\Access\SkillingAccessChecker $accessChecker */
  $accessChecker = \Drupal::service(SkillingConstants::ACCESS_CHECKER);
  $accessChecker->alterSearchQuery($query);
}

/**
 * Implements hook_file_download().
 *
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\skilling\Exception\SkillingException
 */
function skilling_file_download($uri) {
  // Return values.
  $block = -1;
  $notApplicable = NULL;
  // Is this a file that Skilling manages?
  $tempUri = strtolower(trim($uri));
  $leftPart = substr($tempUri, 0, strlen(SkillingConstants::MONITORED_FILE_URI_PATH));
  if ($leftPart !== SkillingConstants::MONITORED_FILE_URI_PATH) {
    // Not in Skilling's ken.
    return $notApplicable;
  }
  // This is a file that Skilling cares about.
  // Find the file entity.
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $files = $entityTypeManager->getStorage('file')
    ->loadByProperties(['uri' => $uri]);
  /** @var \Drupal\file\FileInterface $file */
  $file = NULL;
  if (count($files)) {
    /** @var \Drupal\file\FileInterface $item */
    foreach ($files as $item) {
      if ($item->getFileUri() === $uri) {
        $file = $item;
        break;
      }
    }
  }
  if (is_null($file)) {
    // Didn't find it. Something is wrong. Block.
    return $block;
  }
  // Get current user deets.
  /** @var Drupal\skilling\SkillingCurrentUser $currentUser */
  $currentUser = \Drupal::service(SkillingConstants::CURRENT_USER_SERVICE);
  if ($currentUser->isAdministrator()) {
    // Admins can see anything.
    return skilling_get_file_allowed_data($file);
  }
  $currentUserUid = (int) $currentUser->id();
  $fileOwnerUid = (int) $file->getOwnerId();
  // Current user owns the file?
  if ($fileOwnerUid === $currentUserUid) {
    // Access is granted.
    return skilling_get_file_allowed_data($file);
  }
  // Is the current user an instructor of the file owner?
  /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $relationshipsService */
  $relationshipsService = \Drupal::service(SkillingConstants::USER_RELATIONSHIP_SERVICE);
  if ($relationshipsService->isUserUidInstructorOfUserUid($currentUserUid, $fileOwnerUid)) {
    // Access is granted.
    return skilling_get_file_allowed_data($file);
  }
  // Is the current user a grader of the file owner?
  if ($relationshipsService->isUserUidGraderOfUserUid($currentUserUid, $fileOwnerUid)) {
    // Access is granted.
    $result = skilling_get_file_allowed_data($file);
    return $result;
  }
  //Is this file attached by grader to a submission by the current user?
  $fileReferences = file_get_file_references($file);
  foreach ($fileReferences as $fieldName => $entitiesTypesWithField) {
    if ($fieldName === SkillingConstants::FIELD_GRADER_ATTACHMENTS) {
      foreach ($entitiesTypesWithField as $entityTypeName => $entitiesWithField) {
        if ($entityTypeName === 'node') {
          /** @var \Drupal\node\NodeInterface $referencingNode */
          foreach ($entitiesWithField as $nid => $referencingNode) {
            if ($referencingNode->bundle() === SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
              //Is the submission owned by the current user?
              if ($referencingNode->getOwnerId() === $currentUser->id()) {
                // Access is granted.
                return skilling_get_file_allowed_data($file);
              }
            }
          }
        }
      }
    }
  }
  //If a hidden file for an exercise or lesson, admins, instructors, graders, authors,
  //and reviewers can see it.
  $isAuthor = $currentUser->isAuthor();
  $isInstructor = $currentUser->isInstructor();
  $isGrader = $currentUser->isGrader();
  $isReviewer = $currentUser->isReviewer();
  $isAdmin = $currentUser->isAdministrator();
  if ($isAuthor || $isInstructor || $isGrader || $isReviewer || $isAdmin) {
    $references = file_get_file_references($file);
    $fieldNamesReferencing = array_keys($references);
    $isHiddenAttachments = FALSE;
    foreach ($fieldNamesReferencing as $fieldNameReferencing) {
      if ($fieldNameReferencing === SkillingConstants::FIELD_HIDDEN_ATTACHMENTS) {
        $isHiddenAttachments = TRUE;
      }
    }
    if ($isHiddenAttachments) {
      // Access is granted.
      return skilling_get_file_allowed_data($file);
    }
  }
  // In other cases, block.
  return $block;
}

/**
 * Get data about a file when access is allowed.
 *
 * @param \Drupal\file\FileInterface $file
 *   The file.
 *
 * @return array
 *   Data when access is allowed.
 */
function skilling_get_file_allowed_data(FileInterface $file) {
  $headers = file_get_content_headers($file);
  return $headers;
}

