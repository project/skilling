<?php

namespace Drupal\skilling\Access;

use Drupal\Core\Access\CsrfTokenGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Security checks on AJAX calls.
 *
 * Call these methods from controllers that receive AJAX calls.
 */
class SkillingAjaxSecurity implements SkillingAjaxSecurityInterface {

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * Token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfTokenService;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Constructs a new object.
   *
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   Session service.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfTokenService
   *   CSRF token service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   Skilling current user service.
   */
  public function __construct(Session $session, CsrfTokenGenerator $csrfTokenService, SkillingCurrentUser $currentUser) {
    $this->session = $session;
    $this->csrfTokenService = $csrfTokenService;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function securityCheckAjaxRequest(Request $request, array $allowedMethods, array $allowedPaths, $skipTokenTestForAnon = FALSE) {
    // Request must be allowed method.
    if (!in_array($request->getMethod(), $allowedMethods)) {
      return FALSE;
    }
    // Request must be from trusted server.
    $fromTrustedHost = $this->isRequestFromTrustedHost($request);
    if (!$fromTrustedHost) {
      return FALSE;
    }
    $skipTokenTest = $this->currentUser->isAnonymous() && $skipTokenTestForAnon;
    if (!$skipTokenTest) {
      // Request must have a CSRF token.
      $csrfTokenInRequest = $request->get('csrfToken');
      if (!$csrfTokenInRequest) {
        return FALSE;
      }
      // Token must validate.
      if (!$this->validateCsrfToken($csrfTokenInRequest)) {
        return FALSE;
      }
      // Request must have a session id.
      $idInRequest = $request->get('sessionId');
      if (!$idInRequest) {
        return FALSE;
      }
      // Session id must match.
      $actualSessionId = $this->session->getId();
      if ($idInRequest !== $actualSessionId) {
        return FALSE;
      }
    }
    // Path must be allowed.
    $path = $request->getPathInfo();
    if (!in_array($path, $allowedPaths)) {
      return FALSE;
    }
    // Seems OK so far.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isRequestFromTrustedHost(Request $request) {
    // Request must be from trusted server.
    $host = $request->getHost();
    $trustedHosts = Request::getTrustedHosts();
    if (count($trustedHosts) === 0) {
      // No setting.
      return TRUE;
    }
    $fromTrustedHost = FALSE;
    foreach ($trustedHosts as $trustedHost) {
      if (preg_match($trustedHost, $host)) {
        $fromTrustedHost = TRUE;
        break;
      }
    }
    return $fromTrustedHost;
  }

  /**
   * {@inheritdoc}
   */
  public function getCsrfToken() {
    $token = $this->csrfTokenService->get();
    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function validateCsrfToken($token) {
    $result = $this->csrfTokenService->validate($token);
    return $result;
  }

}
