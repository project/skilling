<?php

namespace Drupal\skilling\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an Ajax command for rendering a book tree.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.renderBookTree()
 * defined in libraries/book-tree.js.
 */
class SkillingBookTreeCommand implements CommandInterface {

  /**
   * The book id. Also the nid of the book's root.
   *
   * @var int
   */
  protected $bid;

  /**
   * Title of the book's root node.
   *
   * @var string
   */
  protected $title;

  /**
   * Book tree.
   *
   * Each array element is:
   * key: nid
   * title: node title
   * children (optional): a book tree.
   *
   * @var array
   */
  protected $bookTree;

  /**
   * SkillingBookTreeCommand constructor.
   *
   * @param int $bid
   *   Book id.
   * @param string $title
   *   Title.
   * @param array $bookTree
   *   Book tree.
   */
  public function __construct($bid, $title, array $bookTree) {
    $this->bid = $bid;
    $this->title = $title;
    $this->bookTree = $bookTree;
  }

  /**
   * Render book tree command.
   *
   * @return array
   *   Command name and parameters.
   */
  public function render() {
    return [
      'command' => 'renderBookTree',
      'bid' => $this->bid,
      'title' => $this->title,
      'bookTree' => $this->bookTree,
    ];
  }

}
