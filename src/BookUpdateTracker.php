<?php

namespace Drupal\skilling;

use Drupal\book\BookManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\Utilities as SkillingUtilities;

/**
 * Tracks updates to books and their pages.
 *
 * E.g., adjusts lessons' order-in-book fields.
 *
 * @package Drupal\skilling
 */
class BookUpdateTracker {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling parser service.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $skillingParser;

  /**
   * Service from the book module.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected $bookManager;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Flag set when recomputing node order in book.
   *
   * To prevent Evil Loops.
   *
   * @var bool
   */
  public $computingNodeOrder = FALSE;

  /**
   * Constructor. Grabs service references.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $skillingParser
   *   The Skilling parser service.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   Service from the book module.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingUtilities $skillingUtilities,
    SkillingParser $skillingParser,
    BookManagerInterface $bookManager,
    ConfigFactoryInterface $configFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->skillingParser = $skillingParser;
    $this->bookManager = $bookManager;
    $this->configFactory = $configFactory;
  }

  /**
   * Schedule updates of book's page orders.
   *
   * @param array $bids
   *   Ids of books to update.
   */
  public function scheduleBookOrderUpdate(array $bids) {
    // Make sure all bids are integer.
    $newBids = [];
    foreach ($bids as $bid) {
      $newBids[] = (int) $bid;
    }
    $settings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $settings->set(SkillingConstants::SETTING_KEY_RECOMPUTE_BOOK_ORDER, $newBids);
    $settings->save();
  }

  /**
   * Run scheduled book page order updates.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function runScheduledBookOrderUpdates() {
    $moduleSettings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $bidsToRecompute = $moduleSettings->get(SkillingConstants::SETTING_KEY_RECOMPUTE_BOOK_ORDER);
    if (is_array($bidsToRecompute)) {
      if (count($bidsToRecompute) > 0) {
        $this->computingNodeOrder = TRUE;
        $this->updatePagesBookOrder($bidsToRecompute);
        $this->computingNodeOrder = FALSE;
        $moduleSettings->set(SkillingConstants::SETTING_KEY_RECOMPUTE_BOOK_ORDER, []);
        $moduleSettings->save();
      }
      // Erase the scheduled tasks.
      $moduleSettings->set(SkillingConstants::SETTING_KEY_RECOMPUTE_BOOK_ORDER, NULL);
      $moduleSettings->save();
    }
  }

  /**
   * Update book page order for the given books.
   *
   * @param array $bookIds
   *   Ids of the books to update.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updatePagesBookOrder(array $bookIds) {
    // Remove duplicate bids. Duplicates exist if, e.g., moving page in a book.
    $bookIds = array_unique($bookIds);
    foreach ($bookIds as $bookId) {
      // For truthy bids.
      if ($bookId) {
        $bookTree = $this->bookManager->bookTreeAllData($bookId);
        // Order the nids in an array.
        if ($bookTree && count($bookTree) > 0) {
          $nodeOrderList = [];
          $this->computeOrder(current($bookTree), $nodeOrderList);
          if (count($nodeOrderList) > 0) {
            // Load all of the nodes.
            $nodes = $this->entityTypeManager->getStorage('node')
              ->loadMultiple($nodeOrderList);
            // Update their order field.
            $this->computingNodeOrder = TRUE;
            foreach ($nodeOrderList as $order => $nid) {
              /** @var \Drupal\node\Entity\Node $node */
              $node = $nodes[$nid];
              /* @noinspection PhpUndefinedFieldInspection */
              $node->field_order_in_book->value = $order + 1;
              $node->save();
            }
            // Show other code all done.
            $this->computingNodeOrder = FALSE;
            // Delete cached tree for books.
            \Drupal::cache()->delete('book' . $bookId);
          } // End array of nids exists.
        }
      } // End truthy book id.
    } // End foreach book id.
  }

  /**
   * Compute order of nodes in tree.
   *
   * @param array $tree
   *   The tree.
   * @param array $nodeOrderList
   *   Node list order.
   */
  protected function computeOrder(array $tree, array &$nodeOrderList) {
    $nodeOrderList[] = $tree['link']['nid'];
    if (isset($tree['below'])) {
      foreach ($tree['below'] as $subTree) {
        $this->computeOrder($subTree, $nodeOrderList);
      }
    }
  }

}
