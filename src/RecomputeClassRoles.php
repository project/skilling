<?php

namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Recompute a user's class roles, from enrollment data.
 *
 * @package Drupal\skilling
 */
class RecomputeClassRoles {

  protected $recomputingClassRoles = FALSE;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Skilling user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * Recompute completion score service.
   *
   * @var \Drupal\skilling\CompletionScore
   */
  protected $recomputeCompletionScoreService;

  /**
   * RecomputeClassRoles constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   The Skilling user factory service.
   * @param \Drupal\skilling\CompletionScore $recomputeCompletionScoreService
   *   The recompute completion score service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactory $configFactory,
    SkillingUserFactory $skillingUserFactory,
    CompletionScore $recomputeCompletionScoreService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $configFactory;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->recomputeCompletionScoreService = $recomputeCompletionScoreService;
  }

  /**
   * Are class roles being recomputed?
   *
   * @return bool
   *   True if class roles are being recomputed.
   */
  public function isRecomputingClassRoles() {
    return $this->recomputingClassRoles;
  }

  /**
   * Schedule updates of a user's class role.
   *
   * @param int $uidToAdd
   *   Id of user to update.
   */
  public function scheduleClassRoleRecompute($uidToAdd) {
    // Add the new uid to the existing list, if it isn't there already.
    $settings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $uidsToRecompute = [];
    $uidsInKey = $settings->get(SkillingConstants::SETTING_KEY_RECOMPUTE_CLASS_ROLES);
    if (isset($uidsInKey) && is_array($uidsInKey)) {
      $uidsToRecompute = $uidsInKey;
    }
    if (!in_array($uidToAdd, $uidsToRecompute)) {
      $uidsToRecompute[] = (int) $uidToAdd;
      $settings->set(SkillingConstants::SETTING_KEY_RECOMPUTE_CLASS_ROLES, $uidsToRecompute);
      $settings->save();
    }
  }

  /**
   * Run scheduled class role updates.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function runScheduledClassRoleUpdates() {
    $this->recomputingClassRoles = TRUE;
    /** @var \Drupal\Core\Config\Config $moduleSettings */
    $moduleSettings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $uidsToRecompute = $moduleSettings->get(SkillingConstants::SETTING_KEY_RECOMPUTE_CLASS_ROLES);
    if (is_array($uidsToRecompute)) {
      foreach ($uidsToRecompute as $uid) {
        $this->recomputeClassRoles($uid);
      }
      // Erase the scheduled tasks.
      $moduleSettings->set(SkillingConstants::SETTING_KEY_RECOMPUTE_CLASS_ROLES, NULL);
      $moduleSettings->save();
    }
    $this->recomputingClassRoles = FALSE;
  }

  /**
   * Recompute class roles for a user, based on enrollment records.
   *
   * @param int $uid
   *   The user whose class roles are to be updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  protected function recomputeClassRoles($uid) {
    // Check that the user still exists. Might have just been deleted.
    $user = $this->entityTypeManager->getStorage('user')
      ->load($uid);
    if (!$user) {
      return;
    }
    $user = $this->skillingUserFactory->makeSkillingUser($uid);
    $enrollmentIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Published enrollments only.
      ->condition('status', TRUE)
      // For the user in question.
      ->condition('field_user.target_id', $uid)
      // Unblocked users.
      ->condition('field_user.entity.status', 1)
      ->execute();
    $enrollments = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($enrollmentIds);
    // Compute class roles user should have.
    // Clunky code, but easy to understand.
    $isStudent = FALSE;
    $isGrader = FALSE;
    $isInstructor = FALSE;
    foreach ($enrollments as $enrollment) {
      /* @noinspection PhpUndefinedFieldInspection */
      $classRolesForEnrollment = $enrollment->field_class_roles->getValue();
      foreach ($classRolesForEnrollment as $key => $role) {
        $roleName = $role['value'];
        if ($roleName === SkillingConstants::CLASS_ROLE_STUDENT) {
          $isStudent = TRUE;
        }
        elseif ($roleName === SkillingConstants::CLASS_ROLE_GRADER) {
          $isGrader = TRUE;
        }
        elseif ($roleName === SkillingConstants::CLASS_ROLE_INSTRUCTOR) {
          $isInstructor = TRUE;
        }
      } // End for each class role in enrollment.
    } // End for each enrollment.
    // Defer DB update until after all changes are made.
    // Add and remove roles for the user.
    $updateCompletionScores = FALSE;
    if ($isStudent) {
      $user->addStudentRole(FALSE);
      $updateCompletionScores = TRUE;
    }
    else {
      $user->removeStudentRole(FALSE);
    }
    if ($isGrader) {
      $user->addGraderRole(FALSE);
    }
    else {
      $user->removeGraderRole(FALSE);
    }
    if ($isInstructor) {
      $user->addInstructorRole(FALSE);
    }
    else {
      $user->removeInstructorRole(FALSE);
    }
    $user->save();
    if ($updateCompletionScores) {
      $this->recomputeCompletionScoreService->updateCompletionScoresForUser($uid);
    }
  }

}
