<?php

namespace Drupal\skilling\Migrate;

use Drupal\skilling\Exception\SkillingException;

/**
 * Something failed during migration.
 */
class SkillingMigrationException extends SkillingException {

  /**
   * Constructs a SkillingMigrationException.
   *
   * @param string $message
   *   Message reporting error.
   * @param string $file
   *   (optional) File name where the error happened.
   * @param int $line
   *   (optional) Line number where the error happened.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf("Migration problem. \nMessage: %s", $message);
    parent::__construct($message, $file, $line);
  }

}
