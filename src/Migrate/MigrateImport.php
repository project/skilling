<?php

namespace Drupal\skilling\Migrate;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\Utilities;

/**
 * Class MigrateExport.
 */
class MigrateImport {

  use MigrateTrait;
  use StringTranslationTrait;

  protected $entityTypeBundleInfoService;

  /**
   * Constructs a new MigrateExport object.
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    EntityTypeBundleInfo $entityTypeBundleInfoService,
    Utilities $utilities
  ) {
    // Call trait method to remember the services.
    $this->storeServices($messenger, $entityTypeManager, $fileSystem, $utilities);
    $this->createPath($this->migratePath);
    $this->createPath($this->migratePath . 'files');
    $this->entityTypeBundleInfoService = $entityTypeBundleInfoService;
  }

  /**
   * Import stuff.
   *
   * @param string $path
   *   Path to input from.
   * @param bool $justTesting
   *   If true, check the data without importing it.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Migrate\SkillingMigrationException
   */
  public function import(string $path, bool $justTesting) {
    // Make sure the path ends in a /.
    $path = $this->utilities->addSlashToEnd($path);
    $this->migratePath = $path;
    $this->importLessons($justTesting);
  }

  /**
   * @return int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Migrate\SkillingMigrationException
   */
  protected function importLessons(bool $justTesting) {
    // Import lesson JSON.
    $jsonPath = $this->migratePath . 'lessons.json';
    $jsonString = file_get_contents($jsonPath);
    $jsonLessons = json_decode($jsonString);
    // Run through the lessons.
    foreach ($jsonLessons as $jsonLesson) {
      // Flag to show whether there is an import error for this lesson.
      $importError = FALSE;
      $title = $jsonLesson->title;
      if (!$title) {
        $this->messenger->addWarning(
          $this->t('Lesson with no title.')
        );
        $importError = TRUE;
        $title = $this->t('(Unknown)');
//        continue;
      }
      $bodyFull = $jsonLesson->bodyFull;
      $bodySummary = $jsonLesson->bodySummary;
      $sourceOwnerName = $jsonLesson->sourceOwnerName;
      $destinationOwnerId = $this->getUserIdWithAccountName($sourceOwnerName);
      if (!$destinationOwnerId) {
        $warning = $this->t(
          "Lesson with title: '@t'.<br><br>Owner user '@u' not found. Assigning to user 1.",
          ['@t' => $title, '@u' => $sourceOwnerName]
        );
        $this->messenger->addWarning($warning);
        $destinationOwnerId = 1;
      }
      $published = $jsonLesson->published;
      $fieldShowToc = $jsonLesson->fieldShowToc;
      // Field order in book probably does not apply.
//      $fieldOrderInBook = $jsonLesson->fieldOrderInBook;
      $fieldNotes = $jsonLesson->fieldNotes;
      // Try importing the taxonomy terms.
      // Return is array with error flag, and array of term ids.
      list($taxonomyImportError, $termIds)
        = $this->updateTaxomonies($jsonLesson->fieldTags, $justTesting);
      if ($taxonomyImportError) {
        $importError = TRUE;
      }
      // Process attachments.
      $attachments = $jsonLesson->attachments;
      // Copy files from source to destination.
      // Return is array with error flag, and array of file entity ids
      // to add to the field.
      list($attachmentImportError, $attachmentFilesProperties)
        = $this->processFileField($attachments, $justTesting);
      if ($attachmentImportError) {
        $importError = TRUE;
      }
      // Process hidden attachments.
      $hiddenAttachments = $jsonLesson->hiddenAttachments;
      // Copy files from source to destination.
      // Return is array with error flag, and array of file entity ids
      // to add to the field.
      list($hiddenAttachmentImportError, $hiddenAttachmentFilesProperties)
        = $this->processFileField($hiddenAttachments, $justTesting);
      if ($hiddenAttachmentImportError) {
        $importError = TRUE;
      }
      if (!$importError && !$justTesting) {
        // Create a lesson node.
        /** @var \Drupal\node\NodeInterface $lesson */
        $lesson = $this->entityTypeManager
          ->getStorage('node')
          ->create(['type' => SkillingConstants::LESSON_CONTENT_TYPE]);
        // Set base properties.
        $lesson->setTitle($title);
        $lesson->setOwnerId($destinationOwnerId);
        $lesson->setPublished($published);
        // Set fields.
        $lesson->set(
          SkillingConstants::FIELD_BODY,
          [
            'value' => $bodyFull,
            'summary' => $bodySummary,
            'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          ]
        );
        $lesson->set(SkillingConstants::FIELD_SHOW_TOC, $fieldShowToc);
//        $lesson->set(SkillingConstants::FIELD_ORDER_IN_BOOK, $fieldOrderInBook);
        if ($fieldNotes) {
          $lesson->set(
            SkillingConstants::FIELD_NOTES,
            [
              'value' => $fieldNotes,
              'format' => SkillingConstants::STRIPPED_TEXT_FORMAT,
            ]
          );
        }
        // Add term ids to the tags field, if there are any.
        if ($termIds) {
          $tagsField = $lesson->get('field_tags');
          foreach ($termIds as $termId) {
            $tagsField->appendItem(['target_id' => $termId]);
          }
        }
        // Add file ids to the attachments field, if there are any.
        if ($attachmentFilesProperties) {
          $attachmentsField = $lesson->get('field_attachments');
          foreach ($attachmentFilesProperties as $fileProperties) {
            $attachmentsField->appendItem($fileProperties);
          }
        }
        // Add file ids to the hidden attachments field, if there are any.
        if ($hiddenAttachmentFilesProperties) {
          $hiddenAttachmentsField = $lesson->get('field_hidden_attachments');
          foreach ($hiddenAttachmentFilesProperties as $fileProperties) {
            $hiddenAttachmentsField->appendItem($fileProperties);
          }
        }
        $lesson->save();
      }
    }
    return count($jsonLessons);
  }


  /**
   * Update the taxonomies for an entity.
   *
   * Takes an array of items, each with a term, and a taxomomy id (its
   * machine name). Makes the terms it does not find. Returns the
   * term ids of the corresponding terms.
   *
   * @param array $importedTerms
   *   The terms.
   * @param bool $justTesting
   *   If true, no DB updates are made.
   *
   * @return array
   *   Array of term ids. MT if just testing.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updateTaxomonies(array $importedTerms, bool $justTesting) {
    $destinationSiteTermIds = [];
    $importError = FALSE;
    foreach ($importedTerms as $importedTerm) {
      $termText = $importedTerm->term;
      $taxonomy = $importedTerm->taxonomy;
      // Is that term in that vocab?
      $term = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadByProperties([
          'name' => $termText,
          'vid' => $taxonomy,
        ]);
      if ($term) {
        // There is a matching term on this (the destination) site.
        $destinationSiteTermIds[] = reset($term)->id();
      }
      else {
        // Create the term.
        // Does the taxonomy exist?
        $taxonomyBundles = $this->entityTypeBundleInfoService->getBundleInfo('taxonomy_term');
        if (!in_array($taxonomy, array_keys($taxonomyBundles))) {
          // Taxonomy does not exist.
          $message = $this->t(
            "Taxonomy with machine name @n not found.",
            ['@n' => $taxonomy]
          );
          $this->messenger->addError($message);
          $importError = TRUE;
        }
        else {
          // Taxonomy does exist. Add the term to it.
          if ($justTesting) {
            $message = $this->t(
              'Migration would have created the term "@term" in the taxonomy "@tax", if this was for real.',
              ['@term' => $termText, '@tax' => $taxonomy]
            );
          }
          else {
            /** @var \Drupal\taxonomy\Entity\Term $term */
            $term = $this->entityTypeManager->getStorage('taxonomy_term')
              ->create([
                'name' => $termText,
                'vid' => $taxonomy,
              ]);
            $term->save();
            $message = $this->t(
              'Created term "@term" in the taxonomy "@tax".',
              ['@term' => $termText, '@tax' => $taxonomy]
            );
            // Add new term id to list of terms to use.
            $destinationSiteTermIds[] = $term->id();
          } // Not just testing.
          $this->messenger->addStatus($message);
        } // End the taxonomy exists.
      } // End no matching term.
    } // End foreach imported term.
    return [$importError, $destinationSiteTermIds];
  }

  protected function processFileField(array $importedFileData, bool $justTesting) {
    $importError = FALSE;
    $importedFileProperties = [];
    foreach ($importedFileData as $importedFileDatum) {
      // Get the URI for the file as it was on the source site.
      $sourceUri = $importedFileDatum->sourceUri;
      if (!$sourceUri) {
        // No source given - panic!
        $this->messenger->addError(
          $this->t('Source URI missing for file to import.')
        );
        $importError = TRUE;
        continue;
      }
      // Check if a file at that URI already exists as a file entity.
      /* @var \Drupal\file\FileInterface[] $files */
      $files = $this->entityTypeManager->getStorage('file')
        ->getQuery()
        ->condition('uri', $sourceUri)
        ->execute();
      if (count($files) > 0) {
        // Yes, there is a file with that URI.
        // Remember its file entity id.
        $fileId = reset($files);
      }
      else {
        // No, did not find a file entity with the URI given.
        if (!$importError && !$justTesting) {
          // Create it.
          // Build the path to the file to copy.
          $path = $this->migratePath . 'files/'
            . $this->migrateFilePrefix . $importedFileDatum->sourceId;
          // Copy the file into place.
          \Drupal::service('file_system')->copy($path, $sourceUri, FileSystemInterface::EXISTS_REPLACE);
          // Create the file entity.
          $file = File::Create(['uri' => $sourceUri]);
          // Find the owner of the file.
          $fileOwnerName = $importedFileDatum->ownerName;
          $ownerId = $this->getUserIdWithAccountName($fileOwnerName);
          if (!$ownerId) {
            $warning = $this->t(
              "File with URI: '@uri'.<br><br>Owning user '@u' not found. Assigning to user 1.",
              ['@uri' => $sourceUri, '@u' => $fileOwnerName]
            );
            $this->messenger->addWarning($warning);
            $ownerId = 1;
          }
          $file->setOwnerId($ownerId);
          $file->save();
          // Remember the id of the new entity.
          $fileId = $file->id();
        } // End do the import.
      } // End existing file not found.
      // Remember the file properties that need to be stored in the
      // Drupal field.
      $importedFileProperties[] = [
        'target_id' => $fileId,
        'display' => $importedFileDatum->display,
        'description' => $importedFileDatum->description,
      ];
    } // End for each file data record.
    return [$importError, $importedFileProperties];
  } // End processFileField.


  protected function getUserIdWithAccountName(string $accountName) {
    $uids = $this->entityTypeManager->getStorage('user')
      ->getQuery()
      ->condition('name', $accountName)
      ->execute();
    if (count($uids) === 0) {
      return FALSE;
    }
    $uid = reset($uids);
    return $uid;
  }

  protected function isUserIdExists(int $id) {
    if (!is_numeric($id)) {
      return FALSE;
    }
    if ($id < 1) {
      return FALSE;
    }
    $count = $this->entityTypeManager->getStorage('user')
      ->getQuery()
      ->condition('uid', $id)
      ->count()
      ->execute();
    return $count > 0;
  }






  protected function stringToFile(string $string, string $filePath) {
    $result = file_put_contents($filePath, $string);
    if ($result) {
      $this->fileSystem->chmod($filePath);
    }
    return $result;
  }

  protected function extractFileFieldItemList(FileFieldItemList $fileFieldItemList) {
    $fileItemDeets = [];
    foreach ($fileFieldItemList as $fileFieldItem) {
      // Get the deets of the file field item.
      $fileDeets = $this->extractFileEntityReferenceItemDeets($fileFieldItem);
      // Copy the file itself.
      $migratedPath = $this->copyFileForMigration($fileDeets['sourceUri']);
      // Remember where it was copied to.
      $fileDeets['migratedPath'] = $migratedPath;
      $fileItemDeets[] = $fileDeets;
    }
    return $fileItemDeets;
  }

  protected function extractFileEntityReferenceItemDeets(FileItem $fileItem) {
    $fileId = $fileItem->getValue()['target_id'];
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityTypeManager->getStorage('file')
      ->load($fileId);
    $fileDeets = [
      'description' => $fileItem->getValue()['description'],
      'sourceId' => $fileId,
      'sourceUri' => $file->getFileUri(),
      'mimeType' => $file->getMimeType(),
      'ownerId' => $file->getOwnerId(),
    ];
    return $fileDeets;
  }

  protected function copyFileForMigration(string $sourceUri) {
    $source = $this->fileSystem->realpath($sourceUri) ?: $sourceUri;
    $destination = $this->migratePath . 'files/' . $this->migrateFilePrefix . $this->fileCounter;
    $this->fileCounter++;
    // Perform the copy operation.
    if (!@copy($source, $destination)) {
      $this->messenger->addError('Could not copy ' . $sourceUri);
      return FALSE;
    }
    // Set the permissions on the new file.
    $this->fileSystem->chmod($destination);
    return $destination;
  }

}
