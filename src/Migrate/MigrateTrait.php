<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 5/14/19
 * Time: 10:10 AM
 */

namespace Drupal\skilling\Migrate;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\skilling\Utilities;

trait MigrateTrait {
  protected $migratePath = '/home/kieran/subdomains/u1/migrate/';

  protected $fileCounter = 0;

  protected $migrateFilePrefix = 'file';

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $utilities;


  protected function storeServices(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    Utilities $utilities
  ) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->utilities = $utilities;
  }

  /**
   * @param $path
   *
   * @return bool
   *
   * @see https://stackoverflow.com/questions/2303372/create-a-folder-if-it-doesnt-already-exist
   */
  protected function createPath($path) {
    if (is_dir($path)) return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = $this->createPath($prev_path);
    $canMakeDir = $return && is_writable($prev_path);
    if ($canMakeDir) {
      $mkdirResult = mkdir($path);
      if ($mkdirResult) {
        $this->fileSystem->chmod($path);
      }
      return $mkdirResult;
    }
    return false;
  }




}
