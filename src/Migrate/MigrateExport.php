<?php

namespace Drupal\skilling\Migrate;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\skilling\Utilities;

/**
 * Class MigrateExport.
 */
class MigrateExport {

  use MigrateTrait;
  use StringTranslationTrait;

  /**
   * List of files that have been exported.
   *
   * Used to reduce duplicates.
   *
   * @var \Drupal\skilling\Migrate\FileDescription[]
   */
  protected $exportedFiles = [];

  /**
   * Constructs a new MigrateExport object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   * @param \Drupal\skilling\Utilities $utilities
   *   Utilities.
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    Utilities $utilities
  ) {
    // Call trait method to remember the services.
    $this->storeServices($messenger, $entityTypeManager, $fileSystem, $utilities);
    $this->createPath($this->migratePath);
    $this->createPath($this->migratePath . 'files');
  }

  /**
   * Export stuff.
   *
   * @param string $path
   *   Directory to export to.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function export(string $path) {
    // Make sure the path ends in a /.
    $path = $this->utilities->addSlashToEnd($path);
    $this->migratePath = $path;
    $this->exportLessons();
  }

  /**
   * Export lessons.
   *
   * @return int
   *   Number of lessons exported.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function exportLessons() {
    $lessonNids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'lesson')
      ->execute();
    $numberLessonsFound = count($lessonNids);
    if ($numberLessonsFound > 0) {
      $lessons = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($lessonNids);
      $allLessonDeets = [];
      /** @var \Drupal\node\NodeInterface $lesson */
      foreach ($lessons as $lesson) {
        $lessonDeets = [
          'sourceId' => $lesson->id(),
          'title' => $lesson->getTitle(),
          'sourceOwnerId' => $lesson->getOwnerId(),
          'published' => $lesson->isPublished(),
          'bodySummary' => $lesson->body->summary,
          'bodyFull' => $lesson->body->value,
          'fieldShowToc' => $lesson->field_show_toc->value,
          'fieldOrderInBook' => $lesson->field_order_in_book->value,
          'fieldNotes' => $lesson->field_notes->value,
        ];
        // Get images referenced in the body.
        $bodyImages = $this->exportImagesFromContent($lesson->body->value);
        if ($bodyImages) {
          $lessonDeets['bodyImages'] = $bodyImages;
        }
        // Get the account name of the owner.
        /** @var \Drupal\user\UserInterface $user */
        $user = $this->entityTypeManager->getStorage('user')
          ->load($lesson->getOwnerId());
        if ($user) {
          $userName = $user->getAccountName();
        }
        else {
          $userName = $this->t('(Unknown)');
        }
        $lessonDeets['sourceOwnerName'] = $userName;
        $attachments = $lesson->field_attachments;
        $attachedFileDeets = $this->extractFileFieldItemList($attachments);
        $hiddenAttachments = $lesson->field_hidden_attachments;
        $hiddenAttachedFileDeets = $this->extractFileFieldItemList($hiddenAttachments);
        $lessonDeets['attachments'] = $attachedFileDeets;
        $lessonDeets['hiddenAttachments'] = $hiddenAttachedFileDeets;
        // Export terms.
        $lessonDeets['fieldTags'] = $this->exportTags($lesson->field_tags);
        $allLessonDeets[] = $lessonDeets;
      }
      $json = json_encode($allLessonDeets);
      $jsonPath = $this->migratePath . 'lessons.json';
      $stringToFileResult = $this->stringToFile($json, $jsonPath);
      if (!$stringToFileResult) {
        $this->messenger->addError('Could not output ' . $jsonPath);
      }
    }
    return $numberLessonsFound;
  }


  protected function exportImagesFromContent($content) {
    $result = [];
    // Get images referenced in the content.
    preg_match_all(
      '/!(.*)\<img.*src=(?:\'|\")(.*)(?:\'|\").*.*\/\>(.*)!/',
      $content,
      $matches
    );
    if (isset($matches[2])) {
      $imagePaths = $matches[2];
      if (count($imagePaths) > 0) {
        // For this site, compute the base URL to an image file in public://.
        // E.g., /sites/default/files/.
        $urlBasePublic = $this->utilities->addSlashToEnd(
          base_path()
            . \Drupal::service('stream_wrapper_manager')
              ->getViaUri("public://")->basePath()
        );
        // How many character that is.
        $urlBasePublicLen = strlen($urlBasePublic);
        foreach ($imagePaths as $imagePath) {
          $copyOk = TRUE;
          $sourceUri = '';
          $destination = '';
          $fileName = '';
          // Convert relative path into Drupal URI.
          $imageUrlBase = substr($imagePath, 0, $urlBasePublicLen);
          if ($imageUrlBase === $urlBasePublic) {
            // Compute the URI of the image in public://.
            $sourceUri = 'public://' . substr($imagePath, $urlBasePublicLen);
            if (isset($this->exportedFiles[$sourceUri])) {
              // File already copied. Nothing to do.
            }
            else {
              // Make a file description.
              $fileDescription = new FileDescription();
              $fileDescription->setSourceDrupalUri($sourceUri);
              // Copy the file.
              list($fileName, $destination) = $this->copyFileForMigration($sourceUri);
              // Did it work?
              if ($destination) {
                // Yes.
                $fileDescription->setExportedFileName($fileName);
                // Remember that the file was copied.
                $this->exportedFiles[$sourceUri] = $fileDescription;


//                UP TO HERE.



              }
              else {
                // Something broke.
                $copyOk = FALSE;
              }
            } // End not file already copied.
          } // End file path matches public:// root.
          else {
            $copyOk = FALSE;
          }
          if ($copyOk) {
            // Remember data needed to restore the file on import.
            $result[] = [
              // Image's original URL from the content.
              // E.g., "/sites/default/files/pictures/2019-04/pup6.jpg".
              'originalUrl' => $imagePath,
              // Drupal URI of the image on the source site.
              // E.g., "public://pictures/2019-04/pup6.jpg".
              'originalDrupalUri' => $sourceUri,
              // Name of the exported file, e.g., "file17".
              'exportedFileName' => $fileName,
              // File path of the exported file.
              // E.g., "/home/joline/something/migrate/files/file17".
              'exportedPath' => $destination,
            ];
          }
        }
      }
    }
    return $result;
  }


  protected function exportTags(EntityReferenceFieldItemListInterface $tags) {
    $result = [];
    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $tag */
    foreach ($tags as $tag) {
      $termId = $tag->get('target_id')->getValue();
      /** @var \Drupal\taxonomy\Entity\Term $term */
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($termId);
      $termName = $term->getName();
      $vocabId = $term->bundle();
      $result[] = [
        'term' => $termName,
        'taxonomy' => $vocabId,
      ];
    }
    return $result;
  }

  protected function stringToFile(string $string, string $filePath) {
    $result = file_put_contents($filePath, $string);
    if ($result) {
      $this->fileSystem->chmod($filePath);
    }
    return $result;
  }

  protected function extractFileFieldItemList(FileFieldItemList $fileFieldItemList) {
    $fileItemDeets = [];
    foreach ($fileFieldItemList as $fileFieldItem) {
      // Get the deets of the file field item.
      $fileDeets = $this->extractFileEntityReferenceItemDeets($fileFieldItem);
      $sourceUri = $fileDeets['sourceUri'];
      // Already copied?
      if (isset($this->exportedFiles[$sourceUri])) {
        // File already copied. Nothing to do.
      }
      else {
        // Copy the file itself.
        $migratedPath = $this->copyFileForMigration($sourceUri);
        // Remember that the file was copied.
        $this->exportedFiles[$sourceUri] = $fileDeets['sourceId'];
      }
      $fileItemDeets[] = $fileDeets;
    }
    return $fileItemDeets;
  }

  protected function extractFileEntityReferenceItemDeets(FileItem $fileItem) {
    $fileId = $fileItem->getValue()['target_id'];
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityTypeManager->getStorage('file')
      ->load($fileId);
    // Get the account name of the owner.
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')
      ->load($file->getOwnerId());
    if ($user) {
      $userName = $user->getAccountName();
    }
    else {
      $userName = $this->t('(Unknown)');
    }
    $fileDeets = [
      'description' => $fileItem->getValue()['description'],
      'display' => $fileItem->getValue()['display'],
      'sourceId' => $fileId,
      'sourceUri' => $file->getFileUri(),
      'sourceFileName' => $file->getFilename(),
      'mimeType' => $file->getMimeType(),
      'ownerName' => $userName,
    ];
    return $fileDeets;
  }

  protected function copyFileForMigration(string $sourceUri) {
    $source = $this->fileSystem->realpath($sourceUri) ?: $sourceUri;
    $fileName = $this->migrateFilePrefix . $this->fileCounter;
    $destination = $this->migratePath . 'files/' . $fileName;
    $this->fileCounter ++;
    // Perform the copy operation.
    if (!@copy($source, $destination)) {
      $this->messenger->addError('Could not copy ' . $sourceUri);
      return FALSE;
    }
    // Set the permissions on the new file.
    $this->fileSystem->chmod($destination);
    return [$fileName, $destination];
  }

}
