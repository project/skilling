<?php

namespace Drupal\skilling\Migrate;

/**
 * Support class for describing files that are exported.
 *
 * This class is used for:
 *
 * <ul>
 *   <li>
 *     Images that are referred to in content fields
 *     (mostly the body field). These files do not have file entities.
 *   </li>
 *   <li>
 *     Files that are referenced by file fields, e.g.,
 *     field_attachments.
 *   </li>
 * </ul>
 *
 * @package Drupal\skilling\Migrate
 */
class FileDescription {

  /**
   * The Drupal URI of the file.
   *
   * E.g., public://here/there/dog.png.
   *
   * @var string
   */
  protected $sourceDrupalUri;

  /**
   * The name of the exported file, e.g., file12.
   *
   * @var string
   */
  protected $exportedFileName;

  /**
   * Optional file field description.
   *
   * @var string
   */
  protected $description;

  /**
   * Optional file field display flag.
   *
   * @var bool
   */
  protected $display;

  /**
   * Name of the owner of a file, part of a file field.
   *
   * @var string
   */
  protected $ownerName;

  /**
   * MIME type, file field only.
   *
   * @var string
   */
  protected $mimeType;

  /**
   * Get the source Drupal URI.
   *
   * This is the full path to the file on the source website, expressed
   * as a Drupal URI. E.g., public://this/that/the-file.png.
   *
   * @return string
   *   The URI.
   */
  public function getSourceDrupalUri(): string {
    return $this->sourceDrupalUri;
  }

  /**
   * Set the source Drupal URI.
   *
   * This is the full path to the file on the source website, expressed
   * as a Drupal URI. E.g., public://this/that/the-file.png.
   *
   * @param string $sourceDrupalUri
   *   The URI.
   *
   * @return FileDescription
   *   $this, for fluent setter.
   */
  public function setSourceDrupalUri(string $sourceDrupalUri): FileDescription {
    $this->sourceDrupalUri = $sourceDrupalUri;
    return $this;
  }

  /**
   * Get the file name in the export data set, e.g., file12.
   *
   * @return string
   *   The file name.
   */
  public function getExportedFileName(): string {
    return $this->exportedFileName;
  }

  /**
   * Set the file name in the export data set, e.g., file12.
   *
   * @param string $exportedFileName
   *   The file name.
   *
   * @return FileDescription
   *   $this, for fluent setter.
   */
  public function setExportedFileName(string $exportedFileName): FileDescription {
    $this->exportedFileName = $exportedFileName;
    return $this;
  }

  /**
   * Get the description of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Get the description of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @param string $description
   *   The description.
   *
   * @return FileDescription
   *   $this, for fluent setter.
   */
  public function setDescription(string $description): FileDescription {
    $this->description = $description;
    return $this;
  }

  /**
   * Is the display flag for the file set?
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @return bool
   *   True if the flag is set.
   */
  public function isDisplay(): bool {
    return $this->display;
  }

  /**
   * Set whether the display flag for the file is set.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @param bool $display
   *   True if the flag is set.
   *
   * @return FileDescription
   *   True if the flag is set.
   */
  public function setDisplay(bool $display): FileDescription {
    $this->display = $display;
    return $this;
  }

  /**
   * Get the name of the owner of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @return string
   *   The owner name.
   */
  public function getOwnerName(): string {
    return $this->ownerName;
  }

  /**
   * Set the name of the owner of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @param string $ownerName
   *   The owner name.
   *
   * @return FileDescription
   *   $this, for fluent setter.
   */
  public function setOwnerName(string $ownerName): FileDescription {
    $this->ownerName = $ownerName;
    return $this;
  }

  /**
   * Get the MIME type of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @return string
   *   The MIME type.
   */
  public function getMimeType(): string {
    return $this->mimeType;
  }

  /**
   * Set the MIME type of the file.
   *
   * This only applies to file field files, not the images from
   * content fields, like body.
   *
   * @param string $mimeType
   *   The MIME type.
   *
   * @return FileDescription
   *   $this, for fluent setter.
   */
  public function setMimeType(string $mimeType): FileDescription {
    $this->mimeType = $mimeType;
    return $this;
  }

}
