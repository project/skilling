<?php

namespace Drupal\skilling;

use DateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Access\SkillingAccessChecker;

/**
 * Class Notice.
 */
class Notice {

  use StringTranslationTrait;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;
  /**
   * Drupal\skilling\Utilities definition.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;
  /**
   * Drupal\skilling\Access\SkillingAccessChecker definition.
   *
   * @var \Drupal\skilling\Access\SkillingAccessChecker
   */
  protected $skillingAccessChecker;

  /**
   * Constructs a new Notice object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   The current user service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Utilities service.
   * @param \Drupal\skilling\Access\SkillingAccessChecker $skillingAccessChecker
   *   Access checking service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skillingCurrentUser,
    Utilities $skillingUtilities,
    SkillingAccessChecker $skillingAccessChecker
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->skillingUtilities = $skillingUtilities;
    $this->skillingAccessChecker = $skillingAccessChecker;
  }

  /**
   * Notify a user of something.
   *
   * @param \Drupal\skilling\SkillingUser $user
   *   User to be notified.
   * @param string $title
   *   Notice title.
   * @param string $message
   *   Notice message.
   * @param array $termNames
   *   Taxonomy term names.
   *
   * @return \Drupal\node\Entity\Node
   *   New notice node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function notifyUser(SkillingUser $user, string $title, string $message) {
    // Validate arguments.
    if (!$user) {
      throw new SkillingException('User required.', __FILE__, __LINE__);
    }
    if ($user->isAnonymous()) {
      throw new SkillingException('Cannot notify the anonymous user.', __FILE__, __LINE__);
    }
    $title = trim($title);
    if (!$title) {
      $title = $this->t('(No message title)');
    }
    $message = trim($message);
    if (!$message) {
      $message = $this->t('(No message body)');
    }
    // Add the notification.
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->entityTypeManager
      ->getStorage('node')
      ->create(['type' => SkillingConstants::NOTICE_CONTENT_TYPE]);
    $node->set('title', $title);
    // Set owner.
    $node->set('uid', $user->id());
    // Add the message.
    $node->field_message->value = $message;
    /* @noinspection PhpUndefinedFieldInspection */
    $node->field_message->format = SkillingConstants::STRIPPED_TEXT_FORMAT;
    $node->save();
    return $node;
  }

  /**
   * Record when a notice was read.
   *
   * @param \Drupal\Core\Entity\EntityInterface $notice
   *   The notice.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function setNoticeRead(EntityInterface $notice) {
    // Only record if there is no date already.
    if (!$notice->field_when_read->value) {
      $now = new DateTime();
      $formattedNow = $now->format('Y-m-d\TH:i:s');
      $notice->field_when_read->value = $formattedNow;
      $notice->save();
    }
  }

  /**
   * Record when a notice was read.
   *
   * @param int $noticeId
   *   The notice's id.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setNoticeIdRead(int $noticeId) {
    $notice = $this->entityTypeManager->getStorage('node')
      ->load($noticeId);
    if ($notice) {
      $this->setNoticeRead($notice);
    }
  }

  /**
   * Get the number of new notices for the current user.
   *
   * @return int
   *   Count.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUnreadNoticeCountCurrentUser() {
    $count = 0;
    if ($this->skillingCurrentUser->isAuthenticated()) {
      if (!$this->skillingCurrentUser->isBlocked()) {
        $count = $this->entityTypeManager->getStorage('node')
          ->getQuery()
          ->condition('type', SkillingConstants::NOTICE_CONTENT_TYPE)
          // For the current user.
          ->condition('uid', $this->skillingCurrentUser->id())
          // Published.
          ->condition('status', 1)
          // No read date.
          ->notExists('field_when_read')
          ->count()
          ->execute();
      }
    }
    return $count;
  }

}
