<?php

namespace Drupal\skilling;

use \Drupal\Core\Render\BubbleableMetadata;

/**
 * Interface SkillingTokensInterface.
 */
interface SkillingTokensInterface {

  /**
   * Define Skilling tokens.
   *
   * @return array
   *   Token definitions.
   */
  public function tokenInfo();

  /**
   * Alter existing token definitions.
   *
   * @param array $data
   *   Existing token data.
   */
  public function tokenInfoAlter(array &$data);

  /**
   * Supply replacement data for Skilling tokens.
   *
   * @param string $type
   *   Token type.
   * @param array $tokens
   *   Array of tokens to be replaced.
   * @param array $data
   *   Associative array of data objects to be used when
   *   generating replacement values.
   * @param array $options
   *   Associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   The bubbleable metadata.
   *
   * @return array
   *   Associative array of replacement values.
   */
  public function tokens($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata);

}
