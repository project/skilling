<?php

namespace Drupal\skilling;

//use Drupal\Core\Language\LanguageDefault;
use Drupal;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Html;
use Exception;

/**
 * Represents one user. Wraps a Drupal User entity.
 *
 * SkillingUser is NOT a service. Code can create many
 * SkillingUsers. Create SkillingUsers with the SkillingUserFactory service.
 */
class SkillingUser {

  use StringTranslationTrait;

  /**
   * Returned when a field is empty.
   */
  const UNKNOWN_FIELD_VALUE = '';
//  const UNKNOWN_FIELD_VALUE = '(Unknown)';

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
//  protected $moduleHandler;

  /**
   * The history service.
   *
   * @var \Drupal\skilling_history\History
   */
//  protected $historyService;

  /**
   * Default language service.
   *
   * @var \Drupal\Core\Language\LanguageDefault
   */
//  protected $languageDefault;

  /**
   * @var \Drupal\user\UserInterface
   *   Wrapped Drupal User entity.
   */
  protected $drupalUser = NULL;

  protected $uid = 0;
  protected $classRoles = [];
  protected $classRolesLoaded = FALSE;

  /**
   * SkillingUser constructor. Don't call directly. Use SkillingStudentFactory.
   *
   * @param integer $uid User uid. 0 for anon.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct($uid,
      EntityTypeManagerInterface $entity_type_manager,
      SkillingUtilities $skillingUtilities//,
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skillingUtilities;
    $this->uid = $uid;
    if ( ! $this->uid ) {
      return;
    }
    //Not anon user.
    //Load the Drupal user.
    $this->drupalUser = $this->entityTypeManager
      ->getStorage('user')->load($uid);
  }

  public function isAnonymous() {
    return $this->uid === 0;
  }

  /**
   * Is the current user authenticated?
   */
  public function isAuthenticated() {
    return !$this->isAnonymous();
  }

  public function isBlocked() {
    $result = TRUE;
    if ($this->drupalUser) {
      $result = $this->drupalUser->isBlocked();
    }
    return $result;
  }

  /**
   * Get the wrapped Drupal user object.
   *
   * @return \Drupal\user\UserInterface
   *   The wrapped user object.
   */
  public function getDrupalUser() {
    return $this->drupalUser;
  }

  /**
   * Get the user's uid.
   *
   * @return int
   */
  public function id() {
    return $this->uid;
  }

  /**
   * Is the user an admin (site role)?
   * @return bool True if the user is an admin.
   */
  public function isAdministrator() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::SITE_ROLE_ADMIN);
  }

  /**
   * Is the user an author (site role)?
   * @return bool True if the user is an author.
   */
  public function isAuthor() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::SITE_ROLE_AUTHOR);
  }

  /**
   * Is the user a reviewer (site role)?
   * @return bool True if the user is a reviewer.
   */
  public function isReviewer() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::SITE_ROLE_REVIEWER);
  }

  /**
   * Is the user an instructor in at least one class (class role)?
   * @return bool True if the user is an instructor in a class.
   */
  public function isInstructor() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::CLASS_ROLE_INSTRUCTOR);
  }

  /**
   * Is the user a grader in at least one class (class role)?
   * @return bool True if the user is a grader in a class.
   */
  public function isGrader() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::CLASS_ROLE_GRADER);
  }

  /**
   * Is the user a student in at least one class (class role)?
   * @return bool True if the user is a student in a class.
   */
  public function isStudent() {
    if ( $this->isAnonymous() ) {
      return FALSE;
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->hasRole(SkillingConstants::SITE_ROLE_STUDENT);
  }


  public function getAccountName() {
    if ( $this->isAnonymous() ) {
      return 'anonymous';
    }
    if (!$this->drupalUser) {
      return FALSE;
    }
    return $this->drupalUser->getAccountName();
  }

  /**
   * Get the user's first name.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   First name.
   */
  public function getFirstName() {
    if (!$this->drupalUser) {
      return $this->t('');
    }
    $fn = $this->drupalUser->field_first_name->value;
    if (!$fn) {
      $fn = $this->t(self::UNKNOWN_FIELD_VALUE);
    }
    return $fn;
  }

  /**
   * Get the user's last name.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Last name.
   */
  public function getLastName() {
    if (!$this->drupalUser) {
      return $this->t('');
    }
    $ln = $this->drupalUser->field_last_name->value;
    if (!$ln) {
      $ln = $this->t(self::UNKNOWN_FIELD_VALUE);
    }
    return $ln;
  }

  /**
   * Get the user's initials.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Initials.
   */
  public function getInitials() {
    if (!$this->drupalUser) {
      return $this->t('');
    }
    $initials = $this->drupalUser->field_initials->value;
    if (!$initials) {
      $initials = $this->t(self::UNKNOWN_FIELD_VALUE);
    }
    return $initials;
  }

  /**
   * Get the user's full name.
   *
   * @return string
   *   Name
   */
  public function getFullName() {
    return $this->getFirstName() . ' ' . $this->getLastName();
  }

  /**
   * Get the about field's value.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   About.
   */
  public function getAbout() {
    if (!$this->drupalUser) {
      return $this->t('');
    }
    $about = $this->drupalUser->get(SkillingConstants::FIELD_ABOUT)->value;
    if (!$about) {
      $about = $this->t(self::UNKNOWN_FIELD_VALUE);
    }
    return $about;
  }

  /**
   * Get the badges field's value.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Badges.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBadges() {
    $badgesRefs = $this->drupalUser->get(SkillingConstants::FIELD_BADGES_AWARDED)->getValue();
    $badgeNids = [];
    foreach ($badgesRefs as $badgesRef) {
      $nid = $badgesRef['target_id'];
      $badgeNids[] = $nid;
    }
    if (count($badgeNids) === 0 ) {
      return [];
    }
    $badges = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($badgeNids);
    return $badges;
  }

  /**
   * Get the URL of the user's picture.
   *
   * @param string $imageStyle
   *   Image style, default to thumbnail.
   *
   * @return string
   *   URL, or '' if not present.
   */
  public function getPictureUrl($imageStyle = 'thumbnail') {
    if (!$this->drupalUser) {
      return null;
    }
    $pictureField = $this->drupalUser->get(SkillingConstants::FIELD_PICTURE);
    if ($pictureField) {
      $pictureView = $pictureField->view($imageStyle);
      $pictureView['#label_display'] = 'hidden';
    }
    else {
      $pictureView = '';
    }
    return $pictureView;
  }

  /**
   * Get a link to the user's account page.
   *
   * Text will be the account name.
   *
   * @return mixed
   *   Link to user's account page.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getLinkToUserPage() {
    if (!$this->drupalUser) {
      return null;
    }
    return $this->drupalUser->toLink()->toString();
  }

  public function getEmail() {
    if ($this->isAnonymous()) {
      return NULL;
    }
    if (!$this->drupalUser) {
      return null;
    }
    return $this->getDrupalUser()->getEmail();
  }

  /**
   * Get the user's preferred language code.
   *
   * For anonymous, return the site default language.
   * If the user has not set a default language,
   * returns the site default language.
   *
   * @return string
   *   Language code.
   */
  public function getPreferredLangcode() {
    if (!$this->drupalUser) {
      return null;
    }
    return $this->getDrupalUser()->getPreferredLangcode();
  }

  /**
   * Save the user data.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save() {
    if ($this->drupalUser) {
      $this->drupalUser->save();
    }
  }

  /**
   * Add the student role to the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addStudentRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->addRole(SkillingConstants::CLASS_ROLE_STUDENT);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Remove the student role from the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeStudentRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->removeRole(SkillingConstants::CLASS_ROLE_STUDENT);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Add the grader role to the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addGraderRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->addRole(SkillingConstants::CLASS_ROLE_GRADER);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Remove the grader role from the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeGraderRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->removeRole(SkillingConstants::CLASS_ROLE_GRADER);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Add the instructor role to the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addInstructorRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->addRole(SkillingConstants::CLASS_ROLE_INSTRUCTOR);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Remove the instructor role from the user.
   *
   * @param bool $save
   *   If true, save the change immediately. Set to false when several
   *   changes are needed. Call the save() method when all changes
   *   have been made.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeInstructorRole($save = TRUE) {
    if ($this->drupalUser) {
      $this->drupalUser->removeRole(SkillingConstants::CLASS_ROLE_INSTRUCTOR);
      if ($save) {
        $this->drupalUser->save();
      }
    }
  }

  /**
   * Return all of the roles the user has in a flat array.
   *
   * @return string[] Roles.
   */
  public function getRoles() {
    if (!$this->drupalUser || $this->isAnonymous()) {
      $roles = [];
    }
    else {
      $roles = $this->drupalUser->getRoles();
    }
    return $roles;
  }

  /**
   * Get the user's roles in a class.
   * If class is falsey, return MT array.
   *
   * @param Node $class The class.
   *
   * @return array Roles in the class.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getClassRolesForClass(Node $class) {
    if (!$class) {
      return [];
    }
    //Is it a class?
    if ($class->bundle() !== SkillingConstants::CLASS_CONTENT_TYPE) {
      throw new SkillingWrongTypeException(
        Html::escape('Node ' . $class->id() . ' not a class.'),
        __FILE__, __LINE__
      );
    }
    $classRoles = $this->getClassRoles();
    if (!isset($classRoles[$class->id()])) {
      return [];
    }
    $roles = $this->classRoles[$class->id()];
    return $roles;
  }

  /**
   * Does the user have a role?
   *
   * @param array $roles Roles to check.
   *
   * @return bool True if the user has a role in the array.
   */
  public function hasRole(array $roles) {
    $commonRoles = array_intersect($roles, $this->getRoles());
    return count($commonRoles) > 0;
  }

  /**
   * Does the user have a given role in a given class?
   *
   * @param string $role
   *   Role to check for.
   * @param \Drupal\node\Entity\Node $class
   *   The class.
   *
   * @return bool
   *   True if the user has the given role in the given class.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function hasRoleForClass($role, Node $class) {
    $classRoles = $this->getClassRolesForClass($class);
    return in_array($role, $classRoles);
  }

  /**
   * Get class roles for this user. Only for published enrollments, and unblocked users.
   *
   * @return array Array of roles the user has in active classes.
   *    Keyed by class nid. E.g,:
   *    [ 3211 => ['grader'], 2193 => ['instructor'] ]
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassRoles() {
    //Exit if anon or blocked.
    if ( $this->isAnonymous() || $this->isBlocked() ) {
      return [];
    }
    //Already loaded them?
    if ( $this->classRolesLoaded ) {
      return $this->classRoles;
    }
    // Load the published enrollments for the user.
    $enrollments
      = $this->skillingUtilities->getUserEnrollmentsPublished($this->id());
    //Extract role data.
    $roles = [];
    /**
     * @var \Drupal\node\Entity\Node $enrollment
     */
    foreach( $enrollments as $enrollment ) {
      //Only published enrollments.
      //This is a duplicate check, but what the heck.
      if ( ! $enrollment->isPublished() ) {
        continue;
      }
      /** @var Node $class */
      /** @noinspection PhpUndefinedFieldInspection */
      $class = $enrollment->field_class->entity;
      //Check (again) that the class is published.
      /** @noinspection PhpUndefinedFieldInspection */
      if ( $class->status->value != 1 ) {
        continue;
      }
      $classId = $class->id();
      $rolesForClass = [];
      /** @noinspection PhpUndefinedFieldInspection */
      foreach ($enrollment->field_class_roles as $classRole) {
        $rolesForClass[] = $classRole->value;
      }
      $roles[$classId] = $rolesForClass;
    }
    //Cache the roles in this class.
    $this->classRoles = $roles;
    $this->classRolesLoaded = TRUE;
    return $roles;
  }

  /**
   * Get nids of students with published enrollments that have this user
   * with the special role given, usually instructor.
   *
   * @return int[]
   *   Student nids.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStudentsNidsOfSpecialClassRole($classRole) {
    $enrollmentIds = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      //Enrollment is published
      ->condition('status', TRUE)
      //For this user.
      ->condition('field_user', $this->id())
      ->condition('field_class_roles', [$classRole], 'IN')
      ->execute();
    if (count($enrollmentIds) === 0) {
      //No instructor enrollments.
      return [];
    }
    //Load the enrollments.
    $enrollments = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($enrollmentIds);
    //Get their class nids.
    $classNids = [];
    /** @var Node $enrollment */
    foreach ($enrollments as $enrollment) {
      /** @noinspection PhpUndefinedFieldInspection */
      $classNids[] = $enrollment->field_class->target_id;
    }
    //Load nids of students in those classes.
    $enrollmentIds = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      //Enrollment is published
      ->condition('status', TRUE)
      //For one of the classes.
      ->condition('field_class', $classNids, 'IN')
      ->condition('field_class_roles', [SkillingConstants::CLASS_ROLE_STUDENT], 'IN')
      ->execute();
    //Load the enrollment nodes.
    $enrollments = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($enrollmentIds);
    //Extract the student uids.
    $studentUids = [];
    /** @var \Drupal\node\Entity\Node $enrollment */
    foreach ($enrollments as $enrollment) {
      /** @noinspection PhpUndefinedFieldInspection */
      $studentUids[] = $enrollment->field_user->target_id;
    }
    return $studentUids;
  }

  /**
   * Get the nids of classes for which the current user is a grader.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassNidsForGrader() {
    $classNids = [];
    $classRoles = $this->getClassRoles();
    foreach ($classRoles as $classNid => $classRoleList) {
      if ( in_array(SkillingConstants::CLASS_ROLE_GRADER, $classRoleList) ) {
        $classNids[] = $classNid;
      }
    }
    return $classNids;
  }

  /**
   * @param array $classNids
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassSpecificRolesForClassNids(array $classNids) {
    //Load the enrollment for the user and class.
    $enrollmentNids = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      //Enrollment is published
      ->condition('status', TRUE)
      //For the user.
      ->condition('field_user', $this->id())
      //User is not blocked.
      ->condition('field_user.entity.status', 1)
      //Class is published.
      //      ->condition('field_class.entity.status', 1)
      //Class is the right one.
      ->condition('field_class', $classNids, 'IN')
      ->execute();
    $enrollments = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($enrollmentNids);
    $classSpecificRoles = [];
    /** @var Node $enrollment */
    foreach ($enrollments as $enrollment) {
      $rolesForClass = [];
      foreach ($enrollment->field_class_roles->getValue() as $key=>$item) {
        $rolesForClass[] = $item['value'];
      }
      $classSpecificRoles = array_merge($classSpecificRoles, $rolesForClass);
    }
    return $classSpecificRoles;
  }

  /**
   * Is the user an instructor of the given class?
   *
   * @param \Drupal\node\NodeInterface $class
   *   The class.
   *
   * @return bool
   *   True if the user is an instructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isInstructorOfClass($class) {
    $classNid = $class->id();
    $result = $this->isInstructorOfClassNid($classNid);
    return $result;
  }

  /**
   * Is the user an instructor of the class with the given nid?
   *
   * @param int $classNid The class's node id.
   *
   * @return bool True if the user is an instructor.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isInstructorOfClassNid($classNid) {
    $classRoles = $this->getClassSpecificRolesForClassNids([$classNid]);
    $isInstructor = in_array(
      SkillingConstants::CLASS_ROLE_INSTRUCTOR, $classRoles
    );
    return $isInstructor;
  }

  /**
   * Is the user a grader of the given class?
   *
   * @param \Drupal\node\NodeInterface $class
   *   The class.
   *
   * @return bool
   *   True if the user is a grader.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isGraderOfClass($class) {
    $classNid = $class->id();
    $result = $this->isInstructorOfClassNid($classNid);
    return $result;
  }

  /**
   * Is the user a grader of the class with the given nid?
   *
   * @param int $classNid The class's node id.
   *
   * @return bool True if the user is a grader.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isGraderOfClassNid($classNid) {
    $classRoles = $this->getClassSpecificRolesForClassNids([$classNid]);
    $isGrader = in_array(
      SkillingConstants::CLASS_ROLE_GRADER, $classRoles
    );
    return $isGrader;
  }

  /**
   * Is the user in the given class?
   *
   * @param \Drupal\node\NodeInterface $class
   *  The class.
   *
   * @return bool
   *   True if the user is in the class.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isInClass($class) {
    $classNid = $class->id();
    return $this->isInClassNid($classNid);
  }

  /**
   * Is the user in the class with the given nid?
   *
   * @param int $classNid
   *   The class's node id.
   *
   * @return bool
   *   True if the user is in the class.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isInClassNid($classNid) {
    $classRoles = $this->getClassSpecificRolesForClassNids([$classNid]);
    return sizeof($classRoles > 0);
  }

  /**
   * Get the nids of badges a student has been awarded.
   *
   * @return array
   *   Nids of badge nodes.
   */
  public function getBadgesAwardedIds() {
    if (!$this->isStudent()) {
      return [];
    }
    $user = $this->getDrupalUser();
    if (!$user) {
      return [];
    }
    $fieldValues = $user->get(SkillingConstants::FIELD_BADGES_AWARDED);
    $badgeNids = [];
    foreach ($fieldValues as $fieldValue) {
      $targetId = $fieldValue->getValue()['target_id'];
      $badgeNids[] = $targetId;
    }
    return $badgeNids;
  }

  /**
   * @param $badgeNidsToAward
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function awardBadges($badgeNidsToAward) {
    // Get the badges the user has.
    $badgesStudentHas = $this->getBadgesAwardedIds();
    foreach ($badgeNidsToAward as $badgeNid) {
      if (!in_array($badgeNid, $badgesStudentHas)) {
        // Award it.
        $user = $this->getDrupalUser();
        if (!$user) {
          continue;
        }
        $user->field_badges_awarded[] = [
          'target_id' => $badgeNid,
        ];
        $this->save();
        // Store event in history?
        // Check whether the history module is active.
        $moduleHandler = Drupal::moduleHandler();
        $historyService = Drupal::service('skilling_history.history');
        $isHistoryModuleActive =
          $moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
        if ($isHistoryModuleActive) {
          // Store the event.
          try {
            $historyService->recordBadgeAwarded($badgeNid);
          }
          catch (Exception $e) {
            $message = 'Exception view your class: '
              . $e->getMessage();
            Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
          }
        }
      }
    }
  }

}
