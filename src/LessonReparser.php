<?php

namespace Drupal\skilling;

use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingParser\SkillingParser;

/**
 * This service is called when a lesson is created, updated, or deleted.
 * It reparses the lesson body. This causes the parser to update references
 * from exercises, patterns, and principles to lessons.
 *
 * This service's methods are called from hooks in the .module file.
 *
 * @package Drupal\skilling
 */
class LessonReparser {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var \Drupal\skilling\NidBag $nidBag */
  protected $nidBag;

  /**
   * The Skilling parser service.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Make a LessonReparser.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\NidBag $nidBag
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    NidBag $nidBag,
    SkillingParser $parser
  ) {
    //Remember services.
    $this->entityTypeManager = $entityTypeManager;
    $this->nidBag = $nidBag;
    $this->parser = $parser;
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function lessonInserted(\Drupal\node\Entity\Node $node) {
    //Use the nid bag with pockets to track exercises, patterns, and principles
    //inserted into content by authors.
    //Empty the bag.
    $this->nidBag->clearAllPockets();
    //Parse the body. Parser will put nids of nodes inserted into lessons
    //into the bag.
    $this->parseLessonBody($node);
    //Get the exercise nids in the bag.
    /** @var integer[] $exerciseNids */
    $nids = $this->nidBag->getAllNidsInBag();
    //Add refs from exercises, patterns, etc., to the node being created.
    $this->addLessonRefsToNodes($node->id(), $nids);
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function lessonDeleted(Node $node) {
    //Empty the bag.
    $this->nidBag->clearAllPockets();
    //Parse the body. Parser will put nids of embedded lessons into bag.
    $this->parseLessonBody($node);
    //Get the exercise nids in the bag.
    /** @var integer[] $nids */
    $nids = $this->nidBag->getAllNidsInBag();
    //Remove refs from nodes to the node (lesson) being deleted.
    $this->removeLessonRefsToNodes($node->id(), $nids);
  }

  /**
   * @param \Drupal\node\Entity\Node $node
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function lessonUpdated(\Drupal\node\Entity\Node $node) {
    // Empty the bag.
    $this->nidBag->clearAllPockets();
    // Parse the body of the lesson before the edits.
    // Parser will put nids of embedded nodes into bag.
    /** @noinspection PhpUndefinedFieldInspection */
    $this->parseLessonBody($node->original);
    // Get the nids in the bag.
    /** @var integer[] $originalNids */
    $originalNids = $this->nidBag->getAllNidsInBag();
    // Empty the bag again.
    $this->nidBag->clearAllPockets();
    // Parse the body of the node after the edits.
    // Parser will put nids of embedded nodes into bag.
    $this->parseLessonBody($node);
    // Get the nids in the bag.
    /** @var integer[] $newNids */
    $newNids = $this->nidBag->getAllNidsInBag();
    $this->removeLessonRefsToNodes($node->id(), $originalNids);
    $this->addLessonRefsToNodes($node->id(), $newNids);
//    // Find the nids that have been removed in the edit.
//    $removedNids = array_diff($originalNids, $newNids);
//    // Remove refs to the lesson in those nodes.
//    if (count($removedNids) > 0) {
//      $this->removeLessonRefsToNodes($node->id(), $removedNids);
//    }
//    // Find the nids that have been added in the edit.
//    $addedNids = array_diff($newNids, $originalNids);
//    // Add refs to the lesson in those nodes.
//    if ( count($addedNids) > 0 ) {
//      $this->addLessonRefsToNodes($node->id(), $addedNids);
//    } //End exercise refs added.
  }

  /**
   * Parse the body content of a lesson. This will trigger
   * the exercise plugin to update exercise-to-lesson refs.
   *
   * @param \Drupal\node\Entity\Node $node
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function parseLessonBody(Node $node) {
    if ( $node->bundle() === SkillingConstants::LESSON_CONTENT_TYPE ) {
      /** @noinspection PhpUndefinedFieldInspection */
      $bodyContent = $node->body->value;
      if ( $bodyContent ) {
        // Reset custom tags to MT, so preparse will trigger.
        $this->parser->customTagsReset();
        $this->parser->parse($bodyContent, $node);
      }
    }
  }

  /**
   * Store reference to a lesson nid in nodes with nids in array.
   *
   * @param integer $lessonNid The nid of the lesson.
   * @param integer[] $nids The nids of nodes that should reference the lesson.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function addLessonRefsToNodes($lessonNid, array $nids) {
    /** @var Node[] $nodes */
    $nodes = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($nids);
    /** @var Node $node */
    foreach($nodes as $node) {
      //Does the inserted node have a where referenced field?
      if ($node->field_where_referenced) {
        //Is the lesson already referenced?
        $alreadyReferenced = FALSE;
          /** @noinspection PhpUndefinedFieldInspection */
          $currentReferences = $node->field_where_referenced->referencedEntities();
          /** @var Node $currentReference */
          foreach ($currentReferences as $currentReference) {
            if ($currentReference->id() == $lessonNid) {
              $alreadyReferenced = TRUE;
              break;
            }
          }
        if ( ! $alreadyReferenced ) {
          /** @noinspection PhpUndefinedFieldInspection */
          $node->field_where_referenced[] = ['target_id' => $lessonNid];
          $node->save();
        }
      }
    }
  }

  /**
   * Remove references from nodes to a lesson.
   *
   * @param integer $lessonNid Lesson nid.
   * @param integer[] $nids Nids that should not ref the lesson.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function removeLessonRefsToNodes($lessonNid, array $nids) {
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    /** @var Node $node */
    foreach($nodes as $node) {
      //Does the inserted node have a where-referenced field?
      if ($node->field_where_referenced) {
        //Is the lesson referenced?
        $foundReference = FALSE;
        $referenceIndex = 0;
        /** @noinspection PhpUndefinedFieldInspection */
        $currentReferences = $node->field_where_referenced->referencedEntities();
        /**
         * @var integer $key
         * @var Node $currentReference
         */
        foreach ($currentReferences as $key => $currentReference) {
          if ($currentReference->id() == $lessonNid) {
            $foundReference = TRUE;
            $referenceIndex = $key;
            break;
          }
        }
        if ($foundReference) {
          //Using get() rather than magic method.
          $node->get('field_where_referenced')->removeItem($referenceIndex);
          $node->save();
        }
      }
    }
  }

}
