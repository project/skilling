<?php

namespace Drupal\skilling;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\Core\Render\Markup;

class PostRenderCallbackThing implements TrustedCallbackInterface
{
  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['postRender'];
  }

  /**
   * Adjust heading tags in HTML for final output to the user.
   *
   * @param \Drupal\Core\Render\Markup $markup
   *   Markup to adjust.
   * @param array $renderArray
   *   Render array.
   *
   * @return string
   *   Result. Headings adjusted.
   */
  public static function postRender(Markup $markup, array $renderArray) {
    $html = (string) $markup;
    /** @var SkillingParser $parser */
    $parser = \Drupal::service('skilling.skillingparser');
    $result = $parser->adjustHeadingLevels($html);
    return $result;
  }
}
