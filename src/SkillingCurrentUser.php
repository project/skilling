<?php

namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Session\AccountProxy;
use Drupal\skilling\Utilities as SkillingUtilities;

/**
 * Represents the currently logged in user.
 */
class SkillingCurrentUser extends SkillingUser {

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUserAccountProxy;

  /**
   * SkillingCurrentUser constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\Utilities $skillingUtilities
   * @param \Drupal\Core\Session\AccountProxy $currentUserAccountProxy
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingUtilities $skillingUtilities,
    AccountProxy $currentUserAccountProxy//,
    //, LanguageDefault $languageDefault
  ) {
    if ($currentUserAccountProxy->isAnonymous()) {
      $uid = 0;
    }
    else {
      $uid = $currentUserAccountProxy->id();
    }
    parent::__construct($uid, $entityTypeManager, $skillingUtilities//,
    //  , $languageDefault
    );
  }
}
