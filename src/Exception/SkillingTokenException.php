<?php

namespace Drupal\skilling\Exception;

class SkillingTokenException extends SkillingException {

  /**
   * Constructs a SkillingTokenException.
   *
   * @param string $message
   *   Explanation.
   * @param string $file
   *   (optional) File error was in.
   * @param int $line.
   *   (optional) Line error was in.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf('Skilling token exception: %s', $message);
    parent::__construct($message, $file, $line);
  }

}
