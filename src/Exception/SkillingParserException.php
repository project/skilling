<?php

namespace Drupal\skilling\Exception;

/**
 * Something didn't parse.
 *
 * @package Drupal\skilling\Exception
 */
class SkillingParserException extends SkillingException {

  /**
   * Constructs a SkillingParserException.
   *
   * @param string $message
   *   Explanation.
   * @param string $file
   *   (optional) File error was in.
   * @param int $line
   *   (optional) Line error was in.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf('Skilling parser exception: %s', $message);
    parent::__construct($message, $file, $line);
  }

}
