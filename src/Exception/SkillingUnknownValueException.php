<?php

namespace Drupal\skilling\Exception;

/**
 * Something had an unknown value.
 */
class SkillingUnknownValueException extends SkillingException {

  /**
   * Constructs a SkillingUnknownValueException.
   *
   * @param string $message
   *   Message reporting error.
   * @param mixed $value
   *   The unknown value.
   * @param string $file
   *   (optional) File name where the error happened.
   * @param int $line
   *   (optional) Line number where the error happened.
   */
  public function __construct($message, $value = 'NO IDEA', $file = NULL, $line = NULL) {
    $message = sprintf("Unknown value. \nMessage: %s\nValue: %s",
      $message, $value);
    parent::__construct($message, $file, $line);
  }

}
