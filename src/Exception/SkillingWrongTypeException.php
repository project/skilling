<?php

namespace Drupal\skilling\Exception;

/**
 * Something was not the expected type.
 */
class SkillingWrongTypeException extends SkillingException {

  /**
   * Constructs a SkillingWrongTypeException.
   *
   * @param string $message
   *   Message reporting error.
   * @param string $file
   *   (optional) File name where the error happened.
   * @param int $line
   *   (optional) Line number where the error happened.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf("Wrong type. \nMessage: %s", $message);
    parent::__construct($message, $file, $line);
  }

}
