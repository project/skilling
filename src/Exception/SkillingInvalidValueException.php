<?php

namespace Drupal\skilling\Exception;

/**
 * Something was missing.
 */
class SkillingInvalidValueException extends SkillingException {

  /**
   * Constructs a SkillingInvalidValueException.
   *
   * @param string $message
   *   Message reporting error.
   * @param string $file
   *   (optional) File name where the error happened.
   * @param int $line
   *   (optional) Line number where the error happened.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf("Invalid value. \nMessage: %s", $message);
    parent::__construct($message, $file, $line);
  }

}
