<?php

namespace Drupal\skilling\Exception;

/**
 * User does not have a required class role.
 */
class SkillingClassRoleException extends SkillingException {

  /**
   * Constructs a SkillingNotFoundException.
   *
   * @param string $message
   *   Message reporting error.
   * @param string $file
   *   (optional) File name where the error happened.
   * @param int $line
   *   (optional) Line number where the error happened.
   */
  public function __construct($message, $file = NULL, $line = NULL) {
    $message = sprintf("Class role needed. \nMessage: %s", $message);
    parent::__construct($message, $file, $line);
  }

}
