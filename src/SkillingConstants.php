<?php

namespace Drupal\skilling;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Constants used across the module.
 */
class SkillingConstants {

  use StringTranslationTrait;

  const MODULE_NAME = 'skilling';

  const HISTORY_MODULE_NAME = 'skilling_history';
  const HISTORY_SERVICE_NAME = 'skilling_history.history';

  // Names of services defined by Skilling.
  const USER_RELATIONSHIP_SERVICE = 'skilling.check_user_relationships';
  const CURRENT_USER_SERVICE = 'skilling.skilling_current_user';
  const CURRENT_CLASS_SERVICE = 'skilling.current_class';
  const ACCESS_CHECKER = 'skilling.access_checker';
  const SKILLING_UTILITIES_SERVICE = 'skilling.utilities';

  const FLOATER_QUERY_STRING = 'floater=true';

  const EDIT_OPERATION = 'edit';
  const VIEW_OPERATION = 'view';
  const VIEW_LABEL_OPERATION = 'view label';

  // Skilling checks entity and field access for these entity types.
  const ENTITY_TYPES_ACCESS_CHECKED = ['node', 'paragraph', 'user'];

  // Skilling's content types.
  const LESSON_CONTENT_TYPE = 'lesson';
  const DESIGN_PAGE_CONTENT_TYPE = 'design_page';
  const EXERCISE_CONTENT_TYPE = 'exercise';
  const EXERCISE_SUBMISSION_CONTENT_TYPE = 'submission';
  const ENROLLMENT_CONTENT_TYPE = 'enrollment';
  const CLASS_CONTENT_TYPE = 'class';
  const RUBRIC_ITEM_CONTENT_TYPE = 'rubric_item';
  const CHARACTER_CONTENT_TYPE = 'character';
  const PATTERN_CONTENT_TYPE = 'pattern';
  const PRINCIPLE_CONTENT_TYPE = 'principle';
  const MODEL_CONTENT_TYPE = 'model';
  const FILL_IN_THE_BLANK_CONTENT_TYPE = 'fill_in_the_blank';
  const REFLECT_NOTE_CONTENT_TYPE = 'reflect_note';
  const MCQ_CONTENT_TYPE = 'multiple_choice_question';
  const SUGGESTION_CONTENT_TYPE = 'suggestion';
  const NOTICE_CONTENT_TYPE = 'notification';
  const BADGE_CONTENT_TYPE = 'badge';

  // Array of Skilling's content types.
  // SkillingAccessChecker does not check other content types.
  const SKILLING_CONTENT_TYPES = [
    self::LESSON_CONTENT_TYPE,
    self::DESIGN_PAGE_CONTENT_TYPE,
    self::EXERCISE_CONTENT_TYPE,
    self::EXERCISE_SUBMISSION_CONTENT_TYPE,
    self::ENROLLMENT_CONTENT_TYPE,
    self::CLASS_CONTENT_TYPE,
    self::RUBRIC_ITEM_CONTENT_TYPE,
    self::CHARACTER_CONTENT_TYPE,
    self::PATTERN_CONTENT_TYPE,
    self::PRINCIPLE_CONTENT_TYPE,
    self::MODEL_CONTENT_TYPE,
    self::FILL_IN_THE_BLANK_CONTENT_TYPE,
    self::REFLECT_NOTE_CONTENT_TYPE,
    self::MCQ_CONTENT_TYPE,
    self::SUGGESTION_CONTENT_TYPE,
    self::NOTICE_CONTENT_TYPE,
  ];

  // Skilling's paragraph types.
  const EXERCISE_DUE_PARAGRAPH_TYPE = 'exercise_due';
  const MCQ_RESPONSE_PARAGRAPH_TYPE = 'multiple_choice_question_respons';
  const FIB_RESPONSE_PARAGRAPH_TYPE = 'fill_in_the_blank_question_respo';
  const RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE = 'rubric_item_response';

  // Array of Skilling's paragraph types.
  // SkillingAccessChecker does not check other paragraph types.
  const SKILLING_PARAGRAPH_TYPES = [
    self::EXERCISE_DUE_PARAGRAPH_TYPE,
    self::MCQ_RESPONSE_PARAGRAPH_TYPE,
    self::FIB_RESPONSE_PARAGRAPH_TYPE,
    self::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE,
  ];

  // Taxonomies
  const TAXONOMY_RUBRIC_ITEM_CATEGORIES = 'rubric_item_categories';

  // Class roles.
  const CLASS_ROLE_STUDENT = 'student';
  const CLASS_ROLE_INSTRUCTOR = 'instructor';
  const CLASS_ROLE_GRADER = 'grader';

  // Site roles.
  const SITE_ROLE_STUDENT = 'student';
  const SITE_ROLE_INSTRUCTOR = 'instructor';
  const SITE_ROLE_GRADER = 'grader';
  const SITE_ROLE_AUTHOR = 'author';
  const SITE_ROLE_REVIEWER = 'reviewer';
  const SITE_ROLE_ADMIN = 'administrator';
  const SITE_ROLE_ANONYMOUS = 'anonymous';

  const SKILLING_ROLES = [
    self::SITE_ROLE_ADMIN,
    self::SITE_ROLE_AUTHOR,
    self::SITE_ROLE_REVIEWER,
    self::CLASS_ROLE_INSTRUCTOR,
    self::CLASS_ROLE_GRADER,
    self::CLASS_ROLE_STUDENT,
    self::SITE_ROLE_ANONYMOUS,
  ];

  // Text format.
  const SKILLING_TEXT_FORMAT = 'skilling';
  const STRIPPED_TEXT_FORMAT = 'stripped';

  // Field types that use a format. In core Drupal, anyway.
//  const FIELD_TYPES_USING_FORMAT = ['text', 'text_with_summary', 'text_long'];

  // Machine name of the Skilling theme.
  const SKILLING_THEME_NAME = 'skilling_theme';

  // Event types.
//  const EVENT_TYPE_EXERCISE_DUE = "exercise due";
//  const EVENT_TYPE_SHOW_TEXT = "show text";

  const PATH_TO_COMPLETE_BADGE = '/images/completion-badge.png';

  // Block cache tag.
  const BLOCK_CURRENT_CLASS_CACHE_TAG = 'skilling_current_class';

  // Exercise submission status.
  const EXERCISE_SUBMISSION_NO_SUBMISSION = 'no submission';
  const EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK = 'waiting for feedback';
  const EXERCISE_SUBMISSION_COMPLETE = 'complete';
  const EXERCISE_SUBMISSION_NOT_COMPLETE = 'not complete';
  const EXERCISE_SUBMISSION_MAX_SUBMISSIONS_REACHED = 'max reached';
  // For instructors and others.
  const EXERCISE_SUBMISSION_NA = 'na';

  // Alternate names for internal_name option in custom tags.
  const INTERNAL_NAME_ALTS = [
    'internal_name', 'internal-name', 'internalname',
    'machine_name', 'machine-name', 'machinename',
  ];

  /**
   * Alternative names for the title option.
   *
   * @var string[]
   */
  const TITLE_ALTS = [
    'title', 'heading', 'header',
  ];
  // Floatable option name.
  const FLOATABLE = 'floatable';
  /**
   * Alternative names for the style option.
   *
   * @var string[]
   */
  const STYLE_ALTS = [
    'style', 'look', 'type', 'styling',
  ];

  /**
   * Alternative names for the show links option.
   *
   * @var string[]
   */
  const SHOW_LINKS_ALTS = [
    'links', 'showlinks', 'show-links', 'show_links',
  ];

  /**
   * Different option values for true.
   *
   * @var string[]
   */
  const TRUE_ALTS = [
    'yes', 'true', 'aye', 'on', 'positive', '1'
  ];

  /**
   * Alternative names for the line numbers option.
   *
   * @var string[]
   */
  const LINE_NUMBERS_ALTS = [
    'line-numbers', 'line_numbers', 'linenumbers',
  ];

  /**
   * Alternative names for the start option.
   *
   * @var string[]
   */
  const START_ALTS = [
    'start', 'start-at', 'startat', 'begin', 'begin-at', 'beginat',
  ];

  // Names for pockets in nid bag.
  const NID_POCKET_EXERCISES = 'exercises';
  const NID_POCKET_PATTERNS = 'patterns';
  const NID_POCKET_PRINCIPLES = 'principles';
  const NID_POCKET_MODELS = 'models';
  const NID_POCKET_CHARACTERS = 'characters';
  const NID_POCKET_FILL_IN_THE_BLANKS = 'fill_in_the_blanks';
  const NID_POCKET_MCQS = 'mcqs';

  // Max length of an internal name.
  const MAX_LENGTH_INTERNAL_NAME = 100;

  // Field names.
  const FIELD_TITLE = 'title';
  const FIELD_BODY = 'body';
  const FIELD_TAGS = 'field_tags';
  const FIELD_ATTACHMENTS = 'field_attachments';
  const FIELD_SHOW_TOC = 'field_show_toc';
  const FIELD_HIDDEN_ATTACHMENTS = 'field_hidden_attachments';
  const FIELD_NOTES = 'field_notes';
  const FIELD_ORDER_IN_BOOK = 'field_order_in_book';
  const FIELD_INTERNAL_NAME = 'field_internal_name';
  const FIELD_WHERE_REFERENCED = 'field_where_referenced';
  const FIELD_EXERCISE_CLASS = 'field_exercise_class';
  const FIELD_RUBRIC_ITEMS = 'field_rubric_items';
  const FIELD_SUMMARY = 'field_summary';
  const FIELD_SITUATION = 'field_situation';
  const FIELD_ACTION = 'field_action';
  const FIELD_DESCRIPTION = 'field_description';
  const FIELD_PHOTO = 'field_photo';
  const FIELD_CAPTION = 'field_caption';
  const FIELD_QUESTION = 'field_question';
  const FIELD_RESPONSES = 'field_responses';
  const FIELD_DAYS = 'field_days';
  const FIELD_USER = 'field_user';
  const FIELD_INITIALS = 'field_initials';
  const FIELD_CLASS = 'field_class';
  const FIELD_STUDENT = 'field_student';
  const FIELD_EXERCISE = 'field_exercise';
  const FIELD_VERSION = 'field_version';
  const FIELD_SOLUTION = 'field_solution';
  const FIELD_SUBMITTED_FILES = 'field_submitted_files';
  const FIELD_DIFFICULTY = 'field_difficulty';
  const FIELD_DIFFICULTY_REASONS = 'field_difficulty_reasons';
  const FIELD_FEEDBACK_SOURCE = 'field_feedback_source';
  const FIELD_WHEN_FEEDBACK_GIVEN = 'field_when_feedback_given';
  const FIELD_FEEDBACK_MESSAGE = 'field_feedback_message';
  const FIELD_FEEDBACK_RESPONSES = 'field_feedback_responses';
  const FIELD_COMPLETE = 'field_complete';
  const FIELD_OVERALL_EVALUATION = 'field_overall_evaluation';
  const FIELD_CLASS_ROLES = 'field_class_roles';
  const FIELD_COMPLETION_SCORE = 'field_completion_score';
  const FIELD_WHEN_SCORE_UPDATED = 'field_when_score_updated';
  const FIELD_WHEN_STARTS = 'field_when_starts';
  const FIELD_CATEGORIES = 'field_categories';
  const FIELD_RUBRIC_ITEM_RESPONSES = 'field_rubric_item_responses';
  const FIELD_COMPLETES_RUBRIC_ITEM = 'field_completes_rubric_item';
  const FIELD_EXERCISES = 'field_exercises';
  const FIELD_NODE = 'field_node';
  const FIELD_NOTE = 'field_note';
  const FIELD_RESPONSE_TO_STUDENT = 'field_response_to_student';
  const FIELD_INCLUDE_IN_PORTFOLIO = 'field_include_in_portfolio';
  const FIELD_GRADER_ATTACHMENTS = 'field_grader_attachments';
  const FIELD_CHALLENGE = 'field_challenge';
  const FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY = "field_base_progress_score_on_req";
  const FIELD_DEFAULT_MAX_SUBMISSIONS = 'field_maximum_submissions_per_ex';
  const FIELD_NEEDS = 'field_needs';
  const FIELD_PROVIDES = 'field_provides';

  // FiB fields.
  const FIELD_RESPONSE_TYPE = 'field_response_type';
  const FIELD_FIB_RESPONSES = 'field_fib_responses';
  const FIELD_NO_MATCH_RESPONSE = 'field_no_match_response';
  const FIELD_CASE_SENSITIVE = 'field_case_sensitive';
  const FIELD_CORRECT = 'field_correct';
  const FIELD_HIGHEST_NUMBER = 'field_highest_number';
  const FIELD_LOWEST_NUMBER = 'field_lowest_number';
  const FIELD_MATCHING_TEXT_RESPONSES = 'field_matching_text_responses';
  const FIELD_EXPLANATION = 'field_explanation';

  // Exercise due para.
  const FIELD_EXERCISES_DUE = 'field_exercises_due';
//  const FIELD_EXERCISE = 'field_exercise';
  const FIELD_DAY = 'field_day';
  const FIELD_REQUIRED = 'field_required';
  const FIELD_MAX_SUBMISSIONS = 'field_maximum_submissions';

  // Suggestion.
  const FIELD_SUGGESTION = 'field_suggestion';
  const FIELD_DETAILS = 'field_details';
  const FIELD_SUGGESTION_STATUS = 'field_suggestion_status';
  const FIELD_PAGE = 'field_page';

  // Badges.
  const FIELD_NUMBER_CHALLENGES = 'field_number_challenges';
  const FIELD_BADGE_IMAGE = 'field_badge_image';

  // Notice.
  const FIELD_WHEN_READ = 'field_when_read';
  const FIELD_MESSAGE = 'field_message';

  // User entity fields.
  const FIELD_FIRST_NAME = 'field_first_name';
  const FIELD_LAST_NAME = 'field_last_name';
  const FIELD_ABOUT = 'field_about';
  const FIELD_PICTURE = 'user_picture';
  const FIELD_FEEDBACK_GREETINGS = 'field_feedback_greetings';
  const FIELD_FEEDBACK_SIGNATURES = 'field_feedback_signatures';
  const FIELD_FEEDBACK_SUMMARY_GOOD = 'field_feedback_summary_good';
  const FIELD_FEEDBACK_SUMMARY_NEEDS_WORK = 'field_feedback_summary_needs_wor';
  const FIELD_FEEDBACK_SUMMARY_POOR = 'field_feedback_summary_poor';
  const FIELD_GOOD_PROGRESS_MESSAGES = 'field_good_progress_messages';
  const FIELD_POOR_PROGRESS_MESSAGES = 'field_poor_progress_messages';
  const FIELD_SHOW_PORTFOLIO = 'field_show_portfolio';
  const FIELD_BADGES_AWARDED = 'field_badges_awarded';

  // User fields that Skilling checks.
  const SKILLING_USER_FIELDS = [
    self::FIELD_FIRST_NAME,
    self::FIELD_LAST_NAME,
    self::FIELD_ABOUT,
    self::FIELD_PICTURE,
    self::FIELD_SHOW_PORTFOLIO,
    self::FIELD_BADGES_AWARDED,
    self::FIELD_FEEDBACK_GREETINGS,
    self::FIELD_FEEDBACK_SIGNATURES,
    self::FIELD_FEEDBACK_SUMMARY_GOOD,
    self::FIELD_FEEDBACK_SUMMARY_NEEDS_WORK,
    self::FIELD_FEEDBACK_SUMMARY_POOR,
    self::FIELD_GOOD_PROGRESS_MESSAGES,
    self::FIELD_POOR_PROGRESS_MESSAGES,
  ];

  const COMPLETION_SCORE_SMILIES_PATH = 'images/smilies/';

  // Id of book nav plugin.
  const BOOK_NAV_PLUGIN_ID = 'skilling_book_nav_block';
  // Name of setting for book to show.
  const BOOK_NAV_BOOK_TO_SHOW = 'book_to_show';
  // Id of the timeline block, in plugin annotation.
  const TIMELINE_BLOCK_ID = 'timeline_block';
  // Id of the class selector block, in plugin annotation.
  const CLASS_SELECTOR_BLOCK_ID = 'class_selector';
  // Id of the badge block, in plugin annotation.
  const BADGE_BLOCK_ID = 'badges';


  const SETTINGS_MAIN_KEY = 'skilling.settings';
  const SETTING_KEY_RECOMPUTE_BOOK_ORDER = 'recompute_on_page_load.recompute_book_order';
  const SETTING_KEY_RECOMPUTE_CLASS_ROLES = 'recompute_on_page_load.recompute_class_roles';
  const SETTING_KEY_SUBMISSION_TEXT_WIDGET = 'submissions.show_submission_text_widget';
  const SETTING_KEY_SUBMISSION_UPLOAD_WIDGET = 'submissions.show_submission_upload_widget';
  const SETTING_KEY_SUBMISSION_REQUIRE_INITIALS = 'submissions.require_initials';
  const SETTING_KEY_SUBMISSIONS_POLICIES = 'submissions.submissions_policies_message';
  const SETTING_KEY_CHARACTER_IMAGE_STYLE = 'character_image_style';
  const SETTING_KEY_STUDENTS_ANON_SEE_DESIGN_PAGES = 'design_pages.students_anons_view';
  const SETTING_KEY_COMPLETION_SCORE_EXPLANATION_URL = 'completion_explanation_path';
  const SETTING_KEY_CONTENT_EXPORT_PATH = 'content_export_path';
  const SETTING_KEY_CONTENT_IMPORT_PATH = 'content_import_path';
  const SETTING_KEY_SCHEDULE_CLEAR_CACHE = 'schedule_clear_cache';
  const SETTING_KEY_GRADERS_SEE_STUDENT_NAMES = 'graders_see_student_names';

  const DEFAULT_COMPLETION_SCORE_EXPLANATION_URL = '/skilling/completion-score-explanation';

  // Step size for the difficulty range widget on the submissions form.
  const DIFFICULTY_RANGE_WIDGET_STEP_SIZE = 0.1;

  // Email keys for hook_mail().
  const EMAIL_KEY_STUDENT_TO_INSTRUCTOR = 'student_to_instructor';
  const EMAIL_KEY_INSTRUCTOR_TO_STUDENT = 'instructor_to_student';

  // Route name of controller that shows a "message sent" confirmation.
  const ROUTE_MESSAGE_SENT_CONFIRMATION = 'skilling.random_stuff_controller_message_sent';

  // UserData key to track timeline visibility.
  const KEY_TIMELINE_SHOWING = 'skilling_timeline_showing';
  // UserData key to track book tree visibility.
  const KEY_BOOK_TREE_SHOWING = 'skilling_book_tree_showing';

  // Types of matches for FiB responses.
  const FIB_MATCH_TYPE_TEXT = 'text';
  const FIB_MATCH_TYPE_NUMBER = 'number';
  // Complete list of them.
  const FIB_MATCH_TYPES = [
    self::FIB_MATCH_TYPE_TEXT,
    self::FIB_MATCH_TYPE_NUMBER,
  ];

  // Skilling applies file download restrictions to files whose URIs start
  // with this. See skilling_file_download().
  const MONITORED_FILE_URI_PATH = 'private://skilling/';

  /**
   * Markers used to adjust heading levels in author content.
   */
  const PARSE_MARKER_INCREASE_HEADING_LEVEL = '<div class="skilling-parse-unit-increase-heading-offset"></div>';
  const PARSE_MARKER_DECREASE_HEADING_LEVEL = '<div class="skilling-parse-unit-decrease-heading-offset"></div>';
  const PARSE_MARKER_INCREASE_HEADING_LEVEL_REGEX = '<div class="skilling-parse-unit-increase-heading-offset"><\/div>';
  const PARSE_MARKER_DECREASE_HEADING_LEVEL_REGEX = '<div class="skilling-parse-unit-decrease-heading-offset"><\/div>';

  /**
   * Return a suggestion for the user on sending someone an error report.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Translatable suggestion.
   */
  public static function getErrorSuggestion() {
    return t('Please send a screen shot of your entire browser window, including the URL, to someone.');
  }

  /**
   * Get translatable display value for submission status.
   *
   * @param string $submissionStatus
   *   Status.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Display value.
   */
  public static function getSubmissionStatusDisplayValue($submissionStatus) {
    switch ($submissionStatus) {
      case self::EXERCISE_SUBMISSION_NO_SUBMISSION:
        return t('No submission yet');

      case self::EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK:
        return t('Submitted, waiting for feedback');

      case self::EXERCISE_SUBMISSION_COMPLETE:
        return t('Complete');

      case self::EXERCISE_SUBMISSION_NOT_COMPLETE:
        return t('Submitted, not complete');

      case self::EXERCISE_SUBMISSION_NA:
        return '';

      default:
        // Should never happen.
        return t('Submission status unknown');
    }
  }

}
