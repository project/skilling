<?php

namespace Drupal\skilling;

use DateInterval;
use DateTime;
use Drupal;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Exception\SkillingUnknownValueException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserDataInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\skilling\Exception\SkillingNotFoundException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Skilling utility code.
 *
 * @package Drupal\skilling
 */
class Utilities {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * User data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Represents the current path for the current request.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathService;

  /**
   * Has information about the current route.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Local cache of enrollments.
   *
   * @var array
   */
//  protected $userEnrollmentsCache = [];

  /**
   * Shows Drupal messages.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructs a new Utilities object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   Module handler service.
   * @param \Drupal\user\UserDataInterface $userData
   *   User data service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathService
   *   Represents the current path for the current request.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   Has information about the current route.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Shows Drupal messages.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatter $date_formatter, ModuleHandler $module_handler, UserDataInterface $userData, CurrentPathStack $currentPathService, CurrentRouteMatch $currentRouteMatch, Messenger $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
    $this->userData = $userData;
    $this->currentPathService = $currentPathService;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->messenger = $messenger;
  }

  /**
   * Make sure the first character of a string is a /.
   *
   * @param string $string
   *   String to process.
   *
   * @return string
   *   Result string.
   */
  public function addSlashToFront($string) {
    $first_char = $string[0];
    if ($first_char !== '/') {
      $string = '/' . $string;
    }
    return $string;
  }

  public function getSiteBaseUrl() {
    // Hmmmm...
//    global $base_url;
    $result = $this->addSlashToEnd(base_path());
    return $result;
  }

  public function addSlashToEnd($string) {
    $lastChar = $string[-1];
    if ($lastChar !== '/') {
      $string .= '/';
    }
    return $string;
  }

  public function removeSlashFromEnd($string) {
    $lastChar = $string[-1];
    if ($lastChar === '/') {
      $string = substr($string, 0, strlen($string) - 1);
    }
    return $string;
  }

  /**
   * Remove a leading / from a string.
   *
   * @param string $string
   *   The string.
   *
   * @return string
   *   String without a leading /.
   */
  public function removeSlashFromStart($string) {
    $firstChar = $string[0];
    if ($firstChar === '/') {
      $string = substr($string, 1, strlen($string) - 1);
    }
    return $string;
  }

  /**
   * Get the URL path to the skilling module. End with a /.
   *
   * @return string
   *   The path.
   */
  public function getModuleUrlPath() {
    $base_url = $this->getSiteBaseUrl();
    $path = $base_url . $this->moduleHandler->getModule(SkillingConstants::MODULE_NAME)->getPath();
    // Make sure it ends with a /.
    $path = $this->addSlashToEnd($path);
    return $path;
  }

  /**
   * Get the relative URL path to the skilling module. End with a /.
   *
   * @return string
   *   The path.
   */
  public function getModuleRelativeUrlPath() {
    $path = $this->moduleHandler->getModule(SkillingConstants::MODULE_NAME)->getPath();
    // Make sure it ends with a /.
    $path = $this->addSlashToEnd($path);
    return $path;
  }

  /**
   * Get the file path to the root of the site.
   *
   * @return string
   *   The path.
   */
  public function getSiteRootFilePath() {
    $path = Drupal::root();
    $path = $this->addSlashToEnd($path);
    return $path;
  }


  /**
   * Has a user completed the survey with the given internal name?
   *
   * @param int $uid
   *   User uid.
   * @param string $surveyInternalName
   *   Internal name of the survey.
   *
   * @return bool
   *   True if the user has completed the survey.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserCompletedSurvey($uid, $surveyInternalName) {
    $storage = Drupal::entityTypeManager()
      ->getStorage('webform_submission');
    $submissions = $storage->loadByProperties([
      'webform_id' => $surveyInternalName,
      'uid' => $uid,
    ]);
    $result = (count($submissions) > 0);
    return $result;
  }

  /**
   * Return a published exercise with a given internal name.
   *
   * @param string $internalName
   *   Internal name of the exercise.
   *
   * @return \Drupal\node\Entity\Node
   *   The exercise node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getPublishedExerciseWithInternalName($internalName) {
    if (!$internalName) {
      throw new SkillingValueMissingException(
        Html::escape('Exercise name not set'),
        __FILE__, __LINE__
      );
    }
    // Does the exercise exist?
    $exercises = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_CONTENT_TYPE)
      // Is published?
      ->condition('status', TRUE)
      // With the right name?
      ->condition(SkillingConstants::FIELD_INTERNAL_NAME, $internalName)
      ->execute();
    if (!$exercises) {
      throw new SkillingNotFoundException(
        'Exercise not found: ' . $internalName,
        __FILE__, __LINE__
      );
    }
    if (!is_array($exercises)) {
      throw new SkillingNotFoundException(
        'Exercise not found: ' . $internalName,
        __FILE__, __LINE__
      );
    }
    if (count($exercises) !== 1) {
      throw new SkillingException(
        'Wrong number of exercises found: ' . $internalName,
        __FILE__, __LINE__
      );
    }
    $exerciseId = reset($exercises);
    $exercise = $this->getExerciseWithId($exerciseId);
    return $exercise;
  }

  /**
   * Get a published exercise with a given id.
   *
   * @param int $nid
   *   Node id.
   *
   * @return \Drupal\node\Entity\Node
   *   The node object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getExerciseWithId($nid) {
    if (!$nid) {
      throw new SkillingValueMissingException(
        Html::escape('Exercise nid missing'), __FILE__, __LINE__
      );
    }
    if (!is_numeric($nid)) {
      throw new SkillingUnknownValueException(
        Html::escape('Bad exercise nid: ' . $nid),
        __FILE__, __LINE__
      );
    }
    // Load the node.
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!$node) {
      return NULL;
    }
    // Right bundle?
    if ($node->bundle() !== SkillingConstants::EXERCISE_CONTENT_TYPE) {
      throw new SkillingWrongTypeException(
        Html::escape(
          'For node ' . $nid . ', expected '
          . SkillingConstants::EXERCISE_CONTENT_TYPE
          . ', got ' . $node->bundle()
        ),
        __FILE__, __LINE__
      );
    }
    return $node;
  }

  /**
   * Get the URL path of the current page.
   *
   * @return string
   *   The URL path, e.g., /user/17
   */
  public function getCurrentUrlPath() {
    $path = $this->currentPathService->getPath();
    return $path;
  }

  /**
   * Get the route match object for the current page.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface
   *   Route match object.
   */
  public function getCurrentRouteMatch() {
    $routeMatch = $this->currentRouteMatch->getCurrentRouteMatch();
    return $routeMatch;
  }


  public function getCurrentNode() {
    $node = Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      return $node;
    }
    return NULL;
  }

  /**
   * Find the canonical form of a name.
   *
   * $alternatives should be like this:
   * 'first_name' => ['firstname', 'first-name', 'first name',]
   *
   * Including the canonical name in the list of alternate names is optional.
   *
   * @param string $originalName
   *   Original name.
   * @param string[] $alternatives
   *   Alternative names.
   *
   * @return string
   *   Canonical form.
   */
  protected function findCanonicalName($originalName, array $alternatives) {
    $result = $originalName;
    // Run through all the name substitutions available.
    foreach ($alternatives as $canonicalName => $allNames) {
      // Is the original name the same as the canonical?
      if ($originalName === $canonicalName) {
        $result = $canonicalName;
        break;
      }
      // See if the original name is a known alternative.
      if (in_array($originalName, $allNames)) {
        $result = $canonicalName;
        break;
      }
    }
    return $result;
  }

  /**
   * Check whether a string starts with another string.
   *
   * See
   * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php.
   *
   * @param string $haystack
   *   Larger string.
   * @param string $needle
   *   String to look for.
   *
   * @return bool
   *   True if haystack starts with needle.
   */
  public function isStartsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }

  /**
   * Check whether a string ends with another string.
   *
   * See
   * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php.
   *
   * @param string $haystack
   *   Larger string.
   * @param string $needle
   *   String to look for.
   *
   * @return bool
   *   True if haystack ends with needle.
   */
  public function isEndsWith($haystack, $needle) {
    $length = strlen($needle);
    return $length === 0 || (substr($haystack, -$length) === $needle);
  }

  /**
   * Check whether first character of string is whitespace.
   *
   * See https://www.php.net/manual/en/function.ctype-space.php
   *
   * @param string $string String to check.
   *
   * @return bool True if whitespace.
   */
  public function isWhitespace($string){
    //    Return FALSE if passed an empty string.
    if($string == "") return FALSE;

    $char    =    ord($string);

    //    Control Characters
    if($char < 33)                        return TRUE;
    if($char > 8191 && $char < 8208)    return TRUE;
    if($char > 8231 && $char < 8240)    return TRUE;

    //    Additional Characters
    switch($char){
      case 160:    // Non-Breaking Space
      case 8287:    // Medium Mathematical Space
      case 0xC2:
      case 0xA0:
        return TRUE;
        break;
    }
    return FALSE;
  }

  /**
   * Make whitespace chars that start lines into &nbsp;.
   *
   * Make other whitespace chars into spaces.
   *
   * @param string $content The content to process.
   *
   * @return string
   */
  public function makeLeadingSpacesNonbreaking($content) {
    $result = '';
    $lines = explode("\n", $content);
    foreach ($lines as $line) {
      $foundNonwhitespaceChar = FALSE;
      $line = str_replace('&nbsp;', ' ', $line);
      for ($charPos = 0; $charPos < strlen($line); $charPos++) {
        $charToCheck = $line[$charPos];
        // Ignore extra char in Windows line ending.
        if ($charToCheck === "\r") {
          continue;
        }
        // Pass non-whitespace char through, remember it was found.
        $isWhiteSpace = $this->isWhitespace($charToCheck);
        // Method checks for strange unicode chars as well.
        if (!$isWhiteSpace) {
          $result .= $charToCheck;
          $foundNonwhitespaceChar = TRUE;
          continue;
        }
        // Char is whitespace.
        // If still on the left-hand (indenting) spaces, emit a nonbreaking space.
        $result .= $foundNonwhitespaceChar ? ' ' : '&nbsp;';
      }
      // Add back in the line ending character, removed by explode().
      $result .= "\n";
    }
    return $result;
  }

  /**
   * Remove empty lines from the start of an array of strings.
   *
   * Remove empty lines, until reach first
   * non-empty line. Leave other empty lines intact.
   *
   * @param array $contentLines Lines to process.
   *
   * @return array Result.
   */
  public function removeLeadingEmptyLines(array $contentLines) {
    $result = [];
    $indexBeingProcessed = 0;
    $foundNonEmtpyLine = FALSE;
    while ($indexBeingProcessed < count($contentLines)) {
      $lineToCheck = $contentLines[$indexBeingProcessed];
      if (strlen($lineToCheck) > 0) {
        $foundNonEmtpyLine = TRUE;
      }
      if ($foundNonEmtpyLine) {
        $result[] = $lineToCheck;
      }
      $indexBeingProcessed ++;
    }
    return $result;
  }

  /**
   * Remove empty lines from the end of an array of strings.
   *
   * Remove empty lines, until reach first
   * non-empty line. Leave other empty lines intact.
   *
   * @param array $contentLines Lines to process.
   *
   * @return array Result.
   */
  public function removeTrailingEmptyLines(array $contentLines) {
    $result = [];
    $indexBeingProcessed = count($contentLines) - 1;
    $foundNonEmtpyLine = FALSE;
    while ($indexBeingProcessed >= 0) {
      $lineToCheck = $contentLines[$indexBeingProcessed];
      if (strlen($lineToCheck) > 0) {
        $foundNonEmtpyLine = TRUE;
      }
      if ($foundNonEmtpyLine) {
        array_unshift($result, $lineToCheck);
      }
      $indexBeingProcessed --;
    }
    return $result;
  }

  /**
   * Make sure empty lines have a nonbreaking space.
   *
   * @param array $lines Lines to check.
   *
   * @return array Result.
   */
  public function addNbspToEmptyLines(array $lines) {
    $result = [];
    foreach ($lines as $line) {
      if (strlen($line) === 0) {
        $line = "&nbsp;";
      }
      $result[] = $line;
    }
    return $result;
  }

  /**
   * Get a published class with a nid.
   *
   * @param int $nid
   *   The node id.
   *
   * @return null|Node
   *   Class, NULL if not found. Could be unpublished.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassPublished($nid) {
    $class = $this->getNodePublished($nid, SkillingConstants::CLASS_CONTENT_TYPE);
    return $class;
  }

  /**
   * Get published node with a nid of a given type.
   *
   * @param int $nid
   *   Node id.
   * @param string $type
   *   Content type.
   *
   * @return null|Node
   *   Node, NULL if not found, or wrong type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNodePublished($nid, $type) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!$node || !$node->isPublished() || $node->bundle() !== $type) {
      return NULL;
    }
    return $node;
  }

  /**
   * Get the published enrollments for a user.
   *
   * Since this could be called often during access checking,
   * cache the results.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   The enrollments.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserEnrollmentsPublished($uid) {
    $enrollmentIds = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Enrollment is published.
      ->condition('status', TRUE)
      // For the user.
      ->condition('field_user', $uid)
      // User is not blocked.
      ->condition('field_user.entity.status', 1)
      // Class is published.
      // ->condition('field_class.entity.status', 1)
      ->execute();
    $enrollments = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($enrollmentIds);
    return $enrollments;
  }


  /**
   * Does an enrollment have one or more given class roles?
   *
   * @param \Drupal\node\NodeInterface $enrollment
   *   Enrollment to check.
   * @param array $targetRoles
   *   Roles to look for.
   *
   * @return bool
   *   True if the enrollment contains one of more of the roles.
   *
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function isEnrollmentHasClassRole(NodeInterface $enrollment, array $targetRoles) {
    if ($enrollment->bundle() !== SkillingConstants::ENROLLMENT_CONTENT_TYPE) {
      throw new SkillingWrongTypeException(
        'Bad content type: ' . $enrollment->id(),
        __FILE__, __LINE__
      );
    }
    $enrolmentRoles = [];
    foreach ($enrollment->get('field_class_roles') as $classRole) {
      $enrolmentRoles[] = $classRole->value;
    }
    $commonRoles = array_intersect($enrolmentRoles, $targetRoles);
    $hasRole = count($commonRoles) > 0;
    return $hasRole;
  }

  /**
   * Get the published submission with the given id.
   *
   * @param int $id
   *   The submission id.
   *
   * @return \Drupal\node\Entity\Node|null
   *   The submission, NULL if not published.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPublishedSubmissionWithNid($id) {
    /** @var \Drupal\node\Entity\Node $submission */
    $submission = $this->entityTypeManager->getStorage('node')
      ->load($id);
    if (!$submission || !$submission->isPublished()) {
      $submission = NULL;
    }
    return $submission;
  }

  /**
   * Is the current page a submission add or edit form?
   */
  public function isSubmissionAddEditForm() {
    $routeMatcher = $this->getCurrentRouteMatch();
    $routeName = $routeMatcher->getRouteName();
    $isSubmissionEditForm = FALSE;
    if ($routeName === 'entity.node.edit_form') {
      // Node edit form. Is it for submissions?
      $bundle = $routeMatcher->getParameter('node')->bundle();
      $isSubmissionEditForm = ($bundle === SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE);
    }
    $isSubmissionAddForm = FALSE;
    if ($routeName === 'node.add') {
      // Node add form. Is it for submissions?
      $bundle = $routeMatcher->getParameter('node_type')->get('type');
      $isSubmissionAddForm = ($bundle === SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE);
    }
    return $isSubmissionEditForm || $isSubmissionAddForm;
  }

  /**
   * Flatten a multidimensional array.
   *
   * @param array $array
   *   Array to flatten.
   *
   * @return array
   *   Flat array.
   *
   * @see https://stackoverflow.com/questions/1319903/how-to-flatten-a-multidimensional-array
   */
  public function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function ($a) use (&$return) {
      $return[] = $a;
    });
    return $return;
  }

  /**
   * Remove status messages.
   */
  public function clearMessages() {
    $this->messenger->deleteAll();
  }

  /**
   * Make a link to Skilling documentation.
   *
   * @param string $docLinkId
   *   Id of the link to make, from SkillingConstants.
   * @param string|null $text
   *   Text of the link to make. Has a sensible default.
   *
   * @return string
   *   Link to documentation page.
   */
  public function getDocLink($docLinkId, $text=NULL) {
    // Compute documentation site.
    $docSite = SkillingConstants::USER_DOC_SITE;
    $docSite = $this->addSlashToEnd($docSite);
    // Compute destination page.
    // Default to documentation base in case link id is not found.
    $page = SkillingConstants::USER_DOC_BASE;
    $docLinkId = strtolower(trim($docLinkId));
    if (in_array($docLinkId, SkillingConstants::USER_DOC_LINKS)) {
      $page = SkillingConstants::USER_DOC_LINKS[$docLinkId];
    }
    $destination = $docSite . $page;
    // Compute text of link.
    if (is_null($text)) {
      $text = $this->t('Documentation');
    }
    $html = "<a href='" . $destination . "' target='_blank'>" . $text . "</a>";
    return $html;
  }

  /**
   * Is the private file system set up?
   *
   * @return bool
   */
  public function isPrivateFileSystemSetup() {
    $privateFileSystemOk = FALSE;
    if (Settings::get('file_private_path')) {
      if (Drupal::hasService('stream_wrapper.private')) {
        $fileSystem = Drupal::service('file_system');
        // Check if the private file stream wrapper is ready to use.
        if ($fileSystem->validScheme('private')) {
          $privateFileSystemOk = TRUE;
        }
      }
    }
    return $privateFileSystemOk;
  }

  /**
   * Is a date valid?
   *
   * @param string $toCheck Date
   *   Date in form YYYY-MM-DD.
   *
   * @return bool True if date is valid.
   */
  public function isValidIsoDate($toCheck) {
    $test = explode('-', $toCheck);
    if (count($test) !== 3) {
      return FALSE;
    }
    $year = $test[0];
    $month = $test[1];
    $day = $test[2];
    $result = checkdate($month, $day, $year);
    return $result;
  }

  /**
   * HTML encode the items in an array.
   * @param array $contentLines
   *   Content to encode.
   *
   * @return array Encoded array.
   */
  public function htmlEncodeArray(array $contentLines) {
    $result = [];
    foreach ($contentLines as $contentLine) {
      $newLine = htmlentities($contentLine);
      $result[] = $newLine;
    }
    return $result;
  }

  /**
   * Format an error message for sending back to fib.js.
   *
   * @param string $message
   *   The message.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON result.
   */
  public function formatJsonErrorReport($message) {
    $message = trim(Html::escape($message));
    $result = new JsonResponse(
      ['status' => 'error', 'errorMessage' => $message]
    );
    return $result;
  }

  /**
   * Count the number of published nodes that exist in a specific bundle
   * @param $bundle
   *
   * @return array|int
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function countNodesOfBundle($bundle) {
    // Check number of nodes in the bundle.
    $count = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', $bundle)
//      ->condition('status', TRUE)
      ->count()
      ->execute();
    return $count;
  }

  public function daysDiffNowString(DateTime $targetDate) {
    $result = '';
    $todayStartTime = new DateTime((new DateTime())->format('Y-m-d'));
    $daysDiff = $targetDate->diff($todayStartTime)->days;
    if ($daysDiff === 0) {
      $result = $this->t('today');
    }
    else if ($daysDiff === 1) {
      if ($todayStartTime > $targetDate) {
        $result = $this->t('yesterday');
      }
      else {
        $result = $this->t('tomorrow');
      }
    }
    else if ($daysDiff < 7) {
      $result = $daysDiff . $this->t(' days');
    }
    else {
      $weeks = (integer) ($daysDiff / 7);
      $daysLeftOver = $daysDiff - $weeks * 7;
      $weeksLabel = ($weeks === 1)
        ? $this->t(' week')
        : $this->t(' weeks');
      $daysLabel = ($daysLeftOver === 1)
        ? $this->t(' day')
        : $this->t(' days');
      if ($daysLeftOver === 0) {
        $result = $weeks . $weeksLabel;
      }
      else {
        $result = $weeks . $weeksLabel . $this->t(' and ')
          . $daysLeftOver . $daysLabel;
      }
    }
    if ($daysDiff > 1) {
      if ($todayStartTime > $targetDate) {
        $result .= $this->t(' ago');
      }
    }
    return $result;
  }

  /**
   * Compute date to display, given date and days offset.
   *
   * @param \DateTime $startDate
   *   Start date
   * @param $offset
   *   Offset in days
   *
   * @return DateTime
   *   Date.
   *
   * @throws \Exception
   */
  public function getCalendarDateFromDayOffset(
      DateTime $startDate,
      $offset) {
    $eventDate = clone $startDate;
    $eventDate->add(new DateInterval('P' . $offset . 'D'));
    return $eventDate;
  }

  /**
   * Compute date to display, given date and days offset.
   *
   * @param \DateTime $startDate
   *   Start date
   * @param $offset
   *   Offset in days
   * @param string $dateFormat
   *   How to format date, defaults to 'D, M j, Y'.
   *
   * @return string
   *   Formatted date.
   *
   * @throws \Exception
   */
  public function getCalendarDateFromDayOffsetFormatted(
      DateTime $startDate,
      $offset,
      $dateFormat = 'D, M j, Y') {
    $eventDate = $this->getCalendarDateFromDayOffset($startDate, $offset);
    // Format the date
    $eventDateFormatted = $eventDate->format($dateFormat);
    return $eventDateFormatted;

  }

}
