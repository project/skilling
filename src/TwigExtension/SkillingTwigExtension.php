<?php

namespace Drupal\skilling\TwigExtension;

use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Class SkillingTwigExtension.
 */
class SkillingTwigExtension extends \Twig_Extension {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /** @var SkillingCurrentUser */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
//      new \Twig_SimpleFunction(
//        'submission_status',
//        [$this, 'submission_status'],
//        ['is_safe' => ['html']]
//      ),
      new \Twig_SimpleFunction(
        'user_name',
        [$this, 'user_name'],
        ['is_safe' => ['html']]
      ),
      new \Twig_SimpleFunction(
        'popover',
        [$this, 'popover'],
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'skilling.twig.extension';
  }

  /**
   * Get data to show the status of a submission.
   *
   * @param string $statusType
   *   Status.
   * @param bool $isPast
   *   Is the event in the past?
   *
   * @return array
   *   Result.
   */
//  public function submission_status($statusType, $isPast) {
//    $title = SkillingConstants::getSubmissionStatusDisplayValue($statusType);
//    if ($statusType === SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION) {
//      $class = 'skilling-exercise-no-submission';
//      $marker = $isPast ? '!' : '?';
//    }
//    elseif ($statusType === SkillingConstants::EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK) {
//      $class = 'skilling-exercise-waiting-feedback';
//      $marker = '⌛';
//    }
//    elseif ($statusType === SkillingConstants::EXERCISE_SUBMISSION_COMPLETE) {
//      $class = 'skilling-exercise-complete';
//      $marker = '✔';
//    }
//    elseif ($statusType === SkillingConstants::EXERCISE_SUBMISSION_NOT_COMPLETE) {
//      $class = 'skilling-exercise-not-complete';
//      $marker = '❌';
//    }
//    elseif ($statusType === SkillingConstants::EXERCISE_SUBMISSION_NA) {
//      $class = 'skilling-exercise-na';
//      $marker = '';
//    }
//    else {
//      $class = '';
//      $title = '';
//      $marker = '(Unknown status)';
//    }
//    $result = [
//      'css_class' => $class,
//      'title_attr' => $title,
//      'marker' => $marker,
//    ];
//    return $result;
//  }

  /**
   * Show the name of the logged in user.
   *
   * @return string
   *   Username.
   */
  public function user_name() {
    $userName = '';
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
    if (!$this->currentUser->isAnonymous()) {
      $userName = $this->currentUser->getAccountName();
    }
    return $userName;
  }

  /**
   * Make a popover box.
   *
   * @param string $text
   *   Clickable text to show on the screen.
   * @param string $title
   *   Title, shown on hover, and as box title.
   * @param string $message
   *   Message to show in the box.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   HTML.
   */
  public function popover($text, $title, $message) {
    $result = $this->t(
      "<a href='#/' tabindex='0' role='button' data-trigger='focus'
             data-toggle='popover' title='@title'
             data-content='@message'
          >@text</a>",
      ['@title' => $title, '@text' => $text, '@message' => $message]
    );
    return $result;
  }

}
