<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\skilling\SkillingConstants;

/**
 * Configure the design page settings.
 */
class CharactersConfigurationForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      SkillingConstants::SETTINGS_MAIN_KEY,
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skilling_character_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(SkillingConstants::SETTINGS_MAIN_KEY);
    $form['instructions'] = [
      '#markup' => $this->t(
        "These settings affect how characters are used."),
    ];
    // Image style to use when showing characters.
    // Load the available styles.
    $styles = ImageStyle::loadMultiple();
    $options = [];
    $options['original'] = $this->t('Original');
    /** @var \Drupal\image\Entity\ImageStyle $style */
    foreach ($styles as $styleName => $style) {
      $options[$styleName] = $style->get('label');
    }
    // What is the current style?
    $currentStyle = $config->get(SkillingConstants::SETTING_KEY_CHARACTER_IMAGE_STYLE);
    if (!$currentStyle) {
      $currentStyle = 'thumbnail';
    }
    $imageStylesManagementLink = Url::fromRoute('entity.image_style.collection');
    /* @noinspection HtmlUnknownTarget */
    $form['character_image_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Image style'),
      '#default_value' => $currentStyle,
      '#options' => $options,
      '#description' => $this->t(
        "Image style to use when showing character photos. Choose
        Original to show the photo as uploaded. You can <a href='@link'>change</a> the 
        available image styles", ['@link' => $imageStylesManagementLink->toString()]
      ),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settingsGroup = $this->config(SkillingConstants::SETTINGS_MAIN_KEY);
    $settingsGroup->set(SkillingConstants::SETTING_KEY_CHARACTER_IMAGE_STYLE,
        $form_state->getValue('character_image_style'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
