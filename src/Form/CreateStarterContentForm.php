<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\MakeStarterContent\MakeStarterContent;

/**
 * Class CreateStarterContentForm.
 */
class CreateStarterContentForm extends FormBase {

  const MIN_PASSWORD_LENGTH = 8;

  /**
   * Service that makes starter content.
   *
   * @var \Drupal\skilling\MakeStarterContent\MakeStarterContent
   */
  protected $makeStarterContent;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new CreateStarterContentForm object.
   *
   * @param \Drupal\skilling\MakeStarterContent\MakeStarterContent $makeStarterContent
   *   Service that makes starter content.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    MakeStarterContent $makeStarterContent,
    MessengerInterface $messenger
  ) {
    $this->makeStarterContent = $makeStarterContent;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.starter.make_starter_content'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_starter_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t("
        You can create starter content for Skilling.
        If items with the right names (user names, titles) already exist,
        they will be used. If not, new ones will be created."
      ) . '</p>',
    ];
    $form['passwords'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Passwords'),
      '#description' => $this->t('Password to use for all user accounts. Leave blank for random passwords.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $password = trim($form_state->getValue('passwords'));
    $length = strlen($password);
    if ($length > 0 && $length < self::MIN_PASSWORD_LENGTH) {
      $form_state->setErrorByName(
        'passwords',
        $this->t(
          'Sorry, passwords must be at least @l characters.',
          ['@l' => self::MIN_PASSWORD_LENGTH]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $password = trim($form_state->getValue('passwords'));
    $this->makeStarterContent->createStarterContent($password);
    // Flush Drupal caches.
    drupal_flush_all_caches();
    $this->messenger->addStatus($this->t('Caches cleared.'));
    // If this method was called through an interactive process,
    // redirect back to an admin page.
    return $this->redirect('skilling.admin.config.starter_content');
  }

}
