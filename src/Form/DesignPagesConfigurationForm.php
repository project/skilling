<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the design page settings.
 */
class DesignPagesConfigurationForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      'skilling.settings',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skilling_design_page_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skilling.settings');
    $form['instructions'] = [
      '#markup' => $this->t(
        "These settings affect how design pages are used."),
    ];
    $form['students_anons_view'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Students and anonymous users can view design pages'),
      '#description' => $this->t(
        "Can students and anonymous users see design pages?
        This applies to seeing page content, design pages listed 
        in search, and book navigation blocks."
      ),
      '#default_value' => $config->get('design_pages.students_anons_view'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settingsGroup = $this->config('skilling.settings');
    $settingsGroup->set('design_pages.students_anons_view',
        $form_state->getValue('students_anons_view'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
