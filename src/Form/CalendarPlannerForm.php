<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CalendarPlannerForm.
 */
class CalendarPlannerForm extends FormBase {

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $utilities;

  /**
   * Constructs a new CalendarPlannerForm object.
   *
   * @param \Drupal\skilling\Utilities $utilities
   *   Utilities service.
   */
  public function __construct(Utilities $utilities) {
    $this->utilities = $utilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.utilities')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calendar_planner_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attributes'] = ['target' => '_blank'];
    $defaultDate = date('Y-m-d');
    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t("
        Enter a start date, and click the button, for a calender planner."
      ) . '</p>',
    ];
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('Date for the first day of the planning calendar.'),
      '#weight' => 0,
      '#required' => TRUE,
      '#size' => 15,
      '#default_value' => $defaultDate,
    ];
    $form['calendar_nid'] = [
      '#type' => 'hidden',
//      '#value' => 222,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Show planner'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $startDate = $form_state->getValue('start_date');
    if (! $this->utilities->isValidIsoDate($startDate)) {
      $form_state->setErrorByName(
        'start_date',
        $this->t('Sorry, that is not a valid date.')
      );
    }
  }

  /**
   * {@inheritdoc}
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $startDate = $form_state->getValue('start_date');
    $calendarNid = $form_state->getValue('calendar_nid');
    $form_state->setRedirect(
      'skilling.calendar_planner',
      ['node' => $calendarNid, 'startDate' =>  $startDate]
    );
  }

}
