<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the student profile settings.
 */
class StudentProfileConfigurationForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      'skilling.settings',
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skilling_student_profile_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skilling.settings');
    $form['instructions'] = [
      '#markup' => $this->t(
        "These settings affect student profiles."),
    ];
    $form['allow_contact_form'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Students can have personal contact forms.'),
      '#description' => $this->t(
        "Let students decide whether they have personal contact forms."
      ),
      '#default_value' => $config->get('student_profile.allow_contact_form'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settingsGroup = $this->config('skilling.settings');
    $settingsGroup->set('student_profile.allow_contact_form',
        $form_state->getValue('allow_contact_form'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
