<?php

namespace Drupal\skilling\Form;

use Drupal\book\BookManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ClearCachesForm.
 */
class ClearCachesForm extends FormBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManager
   */
  protected $bookManager;

  /**
   * Constructs a new ClearCachesForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\book\BookManager $bookManager
   *   The book manager service.
   */
  public function __construct(
    MessengerInterface $messenger,
    BookManager $bookManager
  ) {
    $this->messenger = $messenger;
    $this->bookManager = $bookManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('messenger'),
      $container->get('book.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clear_caches_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t("
        You can clear various caches, if they're fouled up. Choose the 
        caches you want to clear."
      ) . '</p>',
    ];
    $form['drupal_caches'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Drupal caches'),
      '#description' => $this->t(
        "Drupal's caches, not maintained by Skilling."
      ),
    );
    $form['book_tree_caches'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Book tree caches'),
      '#description' => $this->t(
        "Caches for book trees, like the lessons tree."
      ),
    );
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $numCachesCleared = 0;
    if ($formState->getValue("book_tree_caches")) {
      // For all of the books.
      $books = $this->bookManager->getAllBooks();
      foreach ($books as $book) {
        $bookId = $book['bid'];
        $title = $book['title'];
        // Clear the cache of the book tree.
        \Drupal::cache()->delete('book' . $bookId);
        // Remember the act.
        $this->messenger->addStatus($this->t(
          'Cleared book tree cache for book: @t',
          ['@t' => $title]
        ));
        $numCachesCleared++;
      }
    }
    if ($formState->getValue("drupal_caches")) {
      // Flush Drupal caches.
      drupal_flush_all_caches();
      $this->messenger->addStatus($this->t('Drupal'));
      $numCachesCleared++;
    }
    // Create the output message.
    if ($numCachesCleared === 0) {
      $this->messenger->addStatus($this->t('No caches cleared.'));
    }
    // Back to this form.
    return $this->redirect('skilling.admin.config.clear_caches');
  }

}
