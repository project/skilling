<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\skilling\Migrate\MigrateExport;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class ExportContentForm.
 */
class ExportContentForm extends FormBase {

  /**
   * Service that exports content.
   *
   * @var \Drupal\skilling\Migrate\MigrateExport
   */
  protected $exportContentService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new ExportContentForm object.
   *
   * @param \Drupal\skilling\Migrate\MigrateExport $exportContentService
   *   Service that exports content.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   */
  public function __construct(
    MigrateExport $exportContentService,
    MessengerInterface $messenger,
    ConfigFactory $configFactory
  ) {
    $this->exportContentService = $exportContentService;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.migrate_export'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'skilling_export_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get stored path, if there is one.
    $settings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $path = $settings->get(SkillingConstants::SETTING_KEY_CONTENT_EXPORT_PATH);
    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t("
        You can export lessons, patterns, exercises, and other content 
        from one Skilling site, for importing into another."
      ) . '</p><p>' . $this->t('THIS IS EXPERIMENTAL. FOR TESTERS ONLY.') . '</p>',
    ];
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export file path'),
      '#description' => $this->t(
        "Directory (folder) where the exported files will be stored 
        on your server. Use an absolute path. The directory must exist,
        and be writable by your web server.<br>
        Linux server example:<br>
        <code>/home/yourname/yoursite/export/</code><br>
        Windows server example:<br>
        <code>C:\\foldername\\export\\</code>"
      ),
      '#size' => 64,
      '#weight' => 0,
      '#default_value' => $path,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Does the path exist?
    $path = trim($form_state->getValue('path'));
    if (!file_exists($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t('Sorry, the server cannot find that path.')
      );
      return;
    }
    if (!is_dir($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t('Sorry, the path exists, but it is not a directory.')
      );
      return;
    }
    // Can the server write to it?
    if (!is_writable($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t(
          "Sorry, the server cannot write to that path. It is
          usually a permissions issue."
        )
      );
      return;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = trim($form_state->getValue('path'));
    // Store path in config.
    $settings = $this->configFactory->getEditable(
      SkillingConstants::SETTINGS_MAIN_KEY
    );
    $settings->set(SkillingConstants::SETTING_KEY_CONTENT_EXPORT_PATH, $path);
    $settings->save();
    $this->exportContentService->export($path);
    $this->messenger->addStatus($this->t('Export complete.'));
    return $this->redirect('skilling.admin.migrate.export_content');
  }

}
