<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\SkillingConstants;

/**
 * Configure the submissions settings.
 */
class SubmissionsConfigurationForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [SkillingConstants::SETTINGS_MAIN_KEY];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skilling_submissions_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skilling.settings');
    $form['instructions'] = [
      '#markup' => t('These settings affect how students submit 
        exercise solutions. '),
    ];
    $form['show_submission_text_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Students can submit text'),
      '#description' => $this->t(
        "There will be a text widget on exercise submission forms,
        where students can type their solutions. This option and/or the
        upload option must be selected."
      ),
      '#default_value' => $config->get(SkillingConstants::SETTING_KEY_SUBMISSION_TEXT_WIDGET),
    ];
    $form['show_submission_upload_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Students can upload files'),
      '#description' => $this->t(
        "There will be a file upload widget on exercise submission forms,
        where students can upload their solutions. This option and/or the
        text option must be selected."
      ),
      '#default_value' => $config->get(SkillingConstants::SETTING_KEY_SUBMISSION_UPLOAD_WIDGET),
    ];
    $form['require_initials'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Student initials required for submission'),
      '#description' => $this->t(
        "Students are required to enter their initials, before they
        can submit a solution."
      ),
      '#default_value' => $config->get(SkillingConstants::SETTING_KEY_SUBMISSION_REQUIRE_INITIALS),
    ];
    $form['graders_see_student_names'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Graders see student names'),
      '#description' => $this->t(
        "Graders can see the names of students who submit solutions."
      ),
      '#default_value' => $config->get(SkillingConstants::SETTING_KEY_GRADERS_SEE_STUDENT_NAMES),
    ];
    $form['submissions_policies'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Submission policies message'),
      '#description' => $this->t(
        'This will appear on the "Your submissions" page for students. HTML.'
      ),
      '#default_value' => $config->get(SkillingConstants::SETTING_KEY_SUBMISSIONS_POLICIES),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $showTextWidget = $form_state->getValue('show_submission_text_widget');
    $showUploadWidget = $form_state->getValue('show_submission_upload_widget');
    if (!$showTextWidget && !$showUploadWidget) {
      $form_state->setErrorByName(
        'show_submission_text_widget',
        $this->t('Please select the text and/or the upload submission widget.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config(SkillingConstants::SETTINGS_MAIN_KEY);
    $showTextWidget = $form_state->getValue('show_submission_text_widget');
    $showUploadWidget = $form_state->getValue('show_submission_upload_widget');
    $requireIntials = $form_state->getValue('require_initials');
    $submissionsPolicies = $form_state->getValue('submissions_policies');
    $gradersSeeStudentNames = $form_state->getValue('graders_see_student_names');
    $config->set(SkillingConstants::SETTING_KEY_SUBMISSION_TEXT_WIDGET, $showTextWidget);
    $config->set(SkillingConstants::SETTING_KEY_SUBMISSION_UPLOAD_WIDGET, $showUploadWidget);
    $config->set(SkillingConstants::SETTING_KEY_SUBMISSION_REQUIRE_INITIALS, $requireIntials);
    $config->set(SkillingConstants::SETTING_KEY_SUBMISSIONS_POLICIES, $submissionsPolicies);
    $config->set(SkillingConstants::SETTING_KEY_GRADERS_SEE_STUDENT_NAMES, $gradersSeeStudentNames);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
