<?php

namespace Drupal\skilling\Form;

use DateTime;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingUserFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\skilling_history\History;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\skilling\Notice;

/**
 * Form for instructor sending a message to a student.
 */
class StudentContactForm extends FormBase {

  /**
   * Regexs to match when replacing stuff in progress messages.
   */
  const REGEX_STUDENT_FIRST_NAME = '/\[student\-first\-name\]/i';
  const REGEX_INSTRUCTOR_FIRST_NAME = '/\[instructor\-first\-name\]/i';

  /**
   * Tiles for predefined message groups.
   */
  const GROUP_TITLE_POOR = 'Poor progress messages';
  const GROUP_TITLE_GOOD = 'Good progress messages';

  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $pluginManagerMail;

  /**
   * Drupal\skilling_history\History definition.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Drupal\skilling\Access\SkillingCheckUserRelationships definition.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $checkUserRelationships;

  /**
   * Skilling user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The notice service.
   *
   * @var \Drupal\skilling\Notice
   */
  protected $noticeService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new InstructorContactForm object.
   *
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Mail\MailManagerInterface $plugin_manager_mail
   *   Mail manager service.
   * @param \Drupal\skilling_history\History $historyService
   *   Skilling history service.
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $skilling_check_user_relationships
   *   User relationship checking service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   Skilling user factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   * @param \Drupal\skilling\Notice $noticeService
   *   The notice service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(
    SkillingCurrentUser $skilling_current_user,
    EntityTypeManagerInterface $entity_type_manager,
    MailManagerInterface $plugin_manager_mail,
    History $historyService,
    SkillingCheckUserRelationships $skilling_check_user_relationships,
    SkillingUserFactory $skillingUserFactory,
    MessengerInterface $messenger,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler,
    Notice $noticeService,
    Renderer $renderer
  ) {
    $this->skillingCurrentUser = $skilling_current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManagerMail = $plugin_manager_mail;
    $this->historyService = $historyService;
    $this->checkUserRelationships = $skilling_check_user_relationships;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->messenger = $messenger;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
    $this->noticeService = $noticeService;
    $this->renderer = $renderer;
  }

  /**
   * Create object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return \Drupal\Core\Form\FormBase|\Drupal\skilling\Form\StudentContactForm
   *   Created object.
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.skilling_current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('skilling_history.history'),
      $container->get('skilling.check_user_relationships'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('messenger'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler'),
      $container->get('skilling.notice'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'student_contact_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param null $studentId
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $studentId = NULL) {
    // Check current user is allowed to send a message. Check is not
    // strictly needed, but... reasons.
    if (!$this->isAllowed($studentId)) {
      $form['problem'] = [
        '#markup' => $this->t('Sorry, this is for instructors and administrators only.'),
      ];
      return $form;
    }
    $student = $this->skillingUserFactory->makeSkillingUser($studentId);
    // Add content identifying the student.
    $studentName = $student->getFullName();
    $studentLinkToUserPage = $student->getLinkToUserPage();
    // Make a new thing that can run through t().
    $insertable = new FormattableMarkup($studentLinkToUserPage, []);
    $form['student'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Student: @fn (@link)',
        ['@fn' => $studentName, '@link' => $insertable]),
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('The topic of this message.'),
      '#default_value' => $this->t('Your progress'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The message to send to the student.'),
      '#weight' => '20',
      '#required' => TRUE,
    ];
    $form['student_id'] = [
      '#type' => 'hidden',
      '#value' => $studentId,
    ];
    $form['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#weight' => '40',
    ];
    // Add prior messages to student.
//    $messagesSentIds = $this->entityTypeManager->getStorage('node')
//      ->getQuery()
//      ->condition('type', SkillingConstants::HISTORY_CONTENT_TYPE)
//      // Is published?
//      ->condition('status', TRUE)
//      // Of view lesson.
//      ->condition(
//        'field_event_type_name',
//        SkillingConstants::HISTORY_EVENT_INSTRUCTOR_SENT_MESSAGE_TO_STUDENT
//      )
//      // Sort by date.
//      ->sort('created', 'DESC')
//      ->execute();
//    if (count($messagesSentIds) === 0) {
//      // No prior messages have been sent.
//      $form['message_history'] = [
//        '#type' => 'markup',
//        '#markup' => t('No message sent to this student using this form.'),
//      ];
//    }
//    else {
//      // Messages have been sent.
//      // Load them.
//      $messagesSent = $this->entityTypeManager->getStorage('node')
//        ->loadMultiple($messagesSentIds);
//      $form['message_history'] = [
//        '#type' => 'details',
//        '#title' => t('Message history'),
//        '#description' => t(
//          'Here are messages already sent to the student using this form. Most recent messages are first.'),
//        '#weight' => 80,
//      ];
//      /** @var \Drupal\node\NodeInterface $messageSent */
//      foreach ($messagesSent as $messageSent) {
//        /** @var \Drupal\skilling\Plugin\HistoryItemInterface $historyItem */
//        $historyItem = $this->historyItemFactory->makeHistoryEventObject($messageSent);
//        /* @noinspection PhpUndefinedFieldInspection */
//        $deets = $historyItem->getHistoryEventNode()->field_details->value;
//        $deets = unserialize($deets);
//        $subject = $deets['subject'];
//        $message = $deets['message'];
//        /* @noinspection PhpUndefinedFieldInspection */
//        $notes = $historyItem->getHistoryEventNode()->field_notes->value;
//        /* @noinspection PhpUndefinedFieldInspection */
//        $messageHistoryThemeable = [
//          '#theme' => 'history_message_to_student',
//          '#when' => $historyItem->getHistoryEventNode()->field_when->value,
//          '#subject' => $subject,
//          '#message' => $message,
//          '#notes' => $notes,
//        ];
//        $form['message_history'][] = $messageHistoryThemeable;
//      }
//    }
    // Add progress message templates.
    /** @var \Drupal\Core\Field\FieldItemListInterface $poorProgressMessages */
    $poorProgressMessages = $this->skillingCurrentUser->getDrupalUser()->get('field_poor_progress_messages');
    /** @var \Drupal\Core\Field\FieldItemListInterface $goodProgressMessages */
    $goodProgressMessages = $this->skillingCurrentUser->getDrupalUser()->get('field_good_progress_messages');
    $totalMessages = $poorProgressMessages->count() + $goodProgressMessages->count();
    if ($totalMessages > 0) {
      $studentFirstName = $student->getFirstName();
      $instructorFirstName = $this->skillingCurrentUser->getFirstName();
      $form['templates'] = [
        '#type' => 'details',
        '#title' => t('Message templates'),
        '#description' => t(
          'Click a message template to copy it into the Message field. You can change the templates on your account edit page.'),
        '#weight' => 100,
      ];
      if ($poorProgressMessages->count() > 0) {
        $form['templates']['poor'] = $this->addProgressMessagesToForm(
          $form, $poorProgressMessages, $studentFirstName, $instructorFirstName, self::GROUP_TITLE_POOR
        );
      }
      if ($goodProgressMessages->count() > 0) {
        $form['templates']['good'] = $this->addProgressMessagesToForm(
          $form, $goodProgressMessages, $studentFirstName, $instructorFirstName, self::GROUP_TITLE_GOOD
        );
      }
    }
    return $form;
  }

  /**
   * Add form fields for progress messages.
   *
   * @param array $form
   *   Form to add to.
   * @param \Drupal\Core\Field\FieldItemListInterface $messagesField
   *   Node field with messages to add.
   * @param string $studentFirstName
   *   Student first name.
   * @param string $instructorFirstName
   *   Instructor first name.
   *   Instructor first name.
   * @param string $groupTitle
   *   Title of this message group.
   *
   * @return array
   *   Renderable array.
   */
  protected function addProgressMessagesToForm(array &$form, FieldItemListInterface $messagesField, $studentFirstName, $instructorFirstName, $groupTitle) {
    // Make a simple array out of the multivalued text field.
    $progressMessages = [];
    $fieldValues = $messagesField->getValue();
    foreach ($fieldValues as $value) {
      $message = $value['value'];
      // Replace tokenish placeholders.
      $message = preg_replace(
        self::REGEX_STUDENT_FIRST_NAME,
        $studentFirstName,
        $message
      );
      $message = preg_replace(
        self::REGEX_INSTRUCTOR_FIRST_NAME,
        $instructorFirstName,
        $message
      );
      $progressMessages[] = $message;
    }
    $result = [
      '#theme' => 'progress_messages',
      '#heading' => $this->t($groupTitle),
      '#messages' => $progressMessages,
      '#weight' => '200',
      '#attached' => [
        'library' => 'skilling/progress-messages',
      ],
    ];
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Check current user is allowed to send a message. Check is not
    // strictly needed, but... reasons.
    $studentId = $form_state->getValue('student_id');
    if (!$this->isAllowed($studentId)) {
      $this->messenger->addError($this->t('Sorry, this is for instructors only.'));
      return;
    }
    $student = $this->skillingUserFactory->makeSkillingUser($studentId);
    // Get data from the form fields.
    $from = $this->skillingCurrentUser->getEmail();
    $to = $student->getEmail();
    $subject = $form_state->getValue('subject');
    // Apply stripped filter.
    $subject = $this->filterInputService->filterUserContent($subject);
    $message = $form_state->getValue('message');
    $message = $this->filterInputService->filterUserContent($message);
    // Make a params array to use with the hook_mail template.
    $params = [];
    $params['from'] = $from;
    $params['to'] = $to;
    $params['subject'] = $subject;
    $params['message'] = $message;
    // Send.
    $result = $this->pluginManagerMail->mail(
      SkillingConstants::MODULE_NAME,
      SkillingConstants::EMAIL_KEY_INSTRUCTOR_TO_STUDENT,
      $to,
      $student->getPreferredLangcode(),
      $params,
      $from
    );
    if ($result['result'] === FALSE) {
      $message = t('There was a problem sending your message.');
      $this->messenger->addError($message);
      \Drupal::logger('mail-log')->error($message);
      return;
    }
    // Log in history.
    // Check whether the history module is active.
    $isHistoryModuleActive =
      $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
    if ($isHistoryModuleActive) {
      // Store the event.
      try {
        $this->historyService->recordSentMessage($studentId, $subject, $message);
      }
      catch (\Exception $e) {
        $message = 'Exception sending message: '
          . $e->getMessage();
        \Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
      }
    }
    // Add notification to receiver.
    $title = $this->t('Message from @n',
      [
        '@n' => $this->skillingCurrentUser->getFullName(),
      ]);
    $now = new DateTime();
    $nowFormatted = $now->format('M j, Y g:i:s a');
    $messageRenderable = [
      '#theme' => 'notice_message',
      '#from' => $from,
      '#subject' => $subject,
      '#when_sent' => $nowFormatted,
      '#message' => $message,
    ];
    $rendered = $this->renderer->render($messageRenderable);
    $this->noticeService->notifyUser($student, $title, $rendered);
    // Go to confirmation page.
    $form_state->setRedirect(SkillingConstants::ROUTE_MESSAGE_SENT_CONFIRMATION);
  }

  /**
   * Check whether current user is allowed to send message to student with id.
   *
   * @param int $studentId
   *   Id of student message will be sent to.
   *
   * @return bool
   *   Whether allowed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function isAllowed($studentId) {
    // Admins are allowed.
    if ($this->skillingCurrentUser->isAdministrator()) {
      return TRUE;
    }
    // Have to be instructor.
    if (!$this->skillingCurrentUser->isInstructor()) {
      return FALSE;
    }
    // Current user must be an instructor of user with given $studentId.
    $student = $this->skillingUserFactory->makeSkillingUser($studentId);
    $isStudentOfInstructor = $this->checkUserRelationships->isUserInstructorOfUser(
      $this->skillingCurrentUser, $student
    );
    return $isStudentOfInstructor;
  }

}
