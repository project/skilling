<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\skilling\Migrate\MigrateImport;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class ImportContentForm.
 */
class ImportContentForm extends FormBase {

  /**
   * Service that imports content.
   *
   * @var \Drupal\skilling\Migrate\MigrateImport
   */
  protected $importContentService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new ImportContentForm object.
   *
   * @param \Drupal\skilling\Migrate\MigrateImport $importContentService
   *   Service that imports content.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   */
  public function __construct(
    MigrateImport $importContentService,
    MessengerInterface $messenger,
    ConfigFactory $configFactory
  ) {
    $this->importContentService = $importContentService;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.migrate_import'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get stored path, if there is one.
    $settings = $this->configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $path = $settings->get(SkillingConstants::SETTING_KEY_CONTENT_IMPORT_PATH);
    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t("
        You can import lessons, patterns, exercises, and other content 
        exported from another Skilling site."
      ) . '</p><p>' . $this->t('THIS IS EXPERIMENTAL. FOR TESTERS ONLY.') . '</p>',
    ];
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Import file path'),
      '#description' => $this->t(
        "Directory (folder) on your server where the imported files 
        will be read from. Use an absolute path. The directory must exist,
        and be readable by your web server.<br>
        Linux server example:<br>
        <code>/home/yourname/yoursite/import/</code><br>
        Windows server example:<br>
        <code>C:\\foldername\\import\\</code>"
      ),
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $path,
    ];
    $form['test'] = [
      '#type' => 'checkbox',
      '#title' => 'Just testing',
      '#description' => "When checked, Skilling checks the data for import,
        but doesn't actually import anything. Good for checking the integrity
        of your data before importing, especially if you have made manual changes
        to the exported data files.<br>
        <br>
        We recommend running a test at least once before importing, to check
        for errors.",
      '#default_value' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Does the path exist?
    $path = trim($form_state->getValue('path'));
    if (!file_exists($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t('Sorry, the server cannot find that path.')
      );
      return;
    }
    if (!is_dir($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t('Sorry, the path exists, but it is not a directory.')
      );
      return;
    }
    // Can the server read from it?
    if (!is_readable($path)) {
      $form_state->setErrorByName(
        'path',
        $this->t(
          "Sorry, the server cannot read from that path. It is
          usually a permissions issue."
        )
      );
      return;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Migrate\SkillingMigrationException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = trim($form_state->getValue('path'));
    // Store path in config.
    $settings = $this->configFactory->getEditable(
      SkillingConstants::SETTINGS_MAIN_KEY
    );
    $settings->set(SkillingConstants::SETTING_KEY_CONTENT_IMPORT_PATH, $path);
    $settings->save();
    // Get Just testing value.
    $justTesting = $form_state->getValue('test');
    // Run the import.
    $this->importContentService->import($path, $justTesting);
    // Reload this form.
    $message = $justTesting
      ? $this->t('Import testing complete. If there were no errors, you are probably good to go.')
      : $this->t('Import complete.');
    $this->messenger->addStatus($message);
    return $this->redirect('skilling.admin.migrate.import_content');
  }

}
