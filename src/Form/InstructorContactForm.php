<?php

namespace Drupal\skilling\Form;

use DateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Notice;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingUserFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\skilling_history\History;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * For students sending a message to an instructor.
 */
class InstructorContactForm extends FormBase {

  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $pluginManagerMail;

  /**
   * Drupal\skilling\History definition.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Drupal\skilling\Access\SkillingCheckUserRelationships definition.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $checkUserRelationships;

  /**
   * Skilling user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * The notice service.
   *
   * @var \Drupal\skilling\Notice
   */
  protected $noticeService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new InstructorContactForm object.
   *
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Mail\MailManagerInterface $plugin_manager_mail
   *   Mail manager service.
   * @param \Drupal\skilling_history\History $skillingHistory
   *   Skilling history service.
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $skilling_check_user_relationships
   *   User relationship checking service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   Skilling user factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Drupal\skilling\Notice $noticeService
   *   The notice service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   */
  public function __construct(
    SkillingCurrentUser $skilling_current_user,
    EntityTypeManagerInterface $entity_type_manager,
    MailManagerInterface $plugin_manager_mail,
    History $skillingHistory,
    SkillingCheckUserRelationships $skilling_check_user_relationships,
    SkillingUserFactory $skillingUserFactory,
    MessengerInterface $messenger,
    FilterUserInputInterface $filterInputService,
    Notice $noticeService,
    Renderer $renderer,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->skillingCurrentUser = $skilling_current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManagerMail = $plugin_manager_mail;
    $this->historyService = $skillingHistory;
    $this->checkUserRelationships = $skilling_check_user_relationships;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->messenger = $messenger;
    $this->filterInputService = $filterInputService;
    $this->noticeService = $noticeService;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.skilling_current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('skilling_history.history'),
      $container->get('skilling.check_user_relationships'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('messenger'),
      $container->get('skilling.filter_user_input'),
      $container->get('skilling.notice'),
      $container->get('renderer'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instructor_contact_form';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $instructor_id = NULL) {
    // Current user must be a student. This check is not strictly needed,
    // but... because.
    if (!$this->skillingCurrentUser->isStudent()) {
      $form['problem'] = [
        '#markup' => $this->t('Sorry, this is for students only.'),
      ];
      return $form;
    }
    // Current user must be a student of user with given $instructor_id.
    // This check is not strictly needed, but... because.
    $instructor = $this->skillingUserFactory->makeSkillingUser($instructor_id);
    $isInstructor = $this->checkUserRelationships->isUserInstructorOfUser(
      $instructor, $this->skillingCurrentUser
    );
    if (!$isInstructor) {
      $form['problem'] = [
        '#markup' => $this->t('Sorry, this is for students sending messages to instructors only.'),
      ];
      return $form;
    }
    $name = $instructor->getFullName();
    $about = $instructor->getAbout();
    $pictureUrl = $instructor->getPictureUrl();
    $form['instructions'] = [
      '#theme' => 'student_message_to_instructor_instructions',
      '#instructor_name' => $name,
      '#instructor_about' => $about,
      '#instructor_picture_url' => $pictureUrl,
      '#weight' => '0',
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('The topic of this message.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '20',
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The message to send to your instructor.'),
      '#weight' => '30',
      '#required' => TRUE,
    ];
    $form['instructor_id'] = [
      '#type' => 'hidden',
      '#value' => $instructor_id,
    ];
    $form['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#weight' => '40',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Current user must be a student. This check is not strictly needed,
    // but... because.
    if (!$this->skillingCurrentUser->isStudent()) {
      $this->messenger->addError('Sorry, this is for students only.');
      return;
    }
    // Current user must be a student of user with given $instructor_id.
    // This check is not strictly needed, but... because.
    $instructorId = $form_state->getValue('instructor_id');
    $instructor = $this->skillingUserFactory->makeSkillingUser($instructorId);
    $isInstructor = $this->checkUserRelationships->isUserInstructorOfUser(
      $instructor, $this->skillingCurrentUser
    );
    if (!$isInstructor) {
      $this->messenger->addError('Sorry, this is for students sending messages to instructors only.');
      return;
    }
    // Get data from the form fields.
    $from = $this->skillingCurrentUser->getEmail();
    $to = $instructor->getEmail();
    $subject = $form_state->getValue('subject');
    // Apply stripped filter.
    $subject = $this->filterInputService->filterUserContent($subject);
    $message = $form_state->getValue('message');
    $message = $this->filterInputService->filterUserContent($message);
    // Make a params array to use with the hook_mail template.
    $params = [];
    $params['from'] = $from;
    $params['to'] = $to;
    $params['subject'] = $subject;
    $params['message'] = $message;
    // Send.
    $result = $this->pluginManagerMail->mail(
      SkillingConstants::MODULE_NAME,
      SkillingConstants::EMAIL_KEY_STUDENT_TO_INSTRUCTOR,
      $to,
      $instructor->getPreferredLangcode(),
      $params,
      $from,
      TRUE
    );
    if ($result['result'] === FALSE) {
      $message = t('There was a problem sending your message.');
      $this->messenger->addError($message);
      \Drupal::logger('mail-log')->error($message);
      return;
    }
    // Add notification to receiver.
    $title = $this->t('Message from @n (@acc)',
      [
        '@n' => $this->skillingCurrentUser->getFullName(),
        '@acc' => $this->skillingCurrentUser->getAccountName(),
      ]);
    $now = new DateTime();
    $nowFormatted = $now->format('M j, Y g:i:s a');
    $messageRenderable = [
      '#theme' => 'notice_message',
      '#from' => $from,
      '#subject' => $subject,
      '#when_sent' => $nowFormatted,
      '#message' => $message,
    ];
    $rendered = $this->renderer->render($messageRenderable);
    $this->noticeService->notifyUser($instructor, $title, $rendered);
    // Log in history.
    // Check whether the history module is active.
    $isHistoryModuleActive =
      $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
    if ($isHistoryModuleActive) {
      // Store the event.
      try {
        $this->historyService->recordSentMessage($instructorId, $subject, $message);
      }
      catch (\Exception $e) {
        $message = 'Exception sending message: '
          . $e->getMessage();
        \Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
      }
    }
    // Go to confirmation page.
    $form_state->setRedirect(SkillingConstants::ROUTE_MESSAGE_SENT_CONFIRMATION);
  }

}
