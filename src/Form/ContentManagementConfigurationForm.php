<?php

namespace Drupal\skilling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\SkillingConstants;

/**
 * Content management configuration.
 */
class ContentManagementConfigurationForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [SkillingConstants::SETTINGS_MAIN_KEY];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'skilling_content_management_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skilling.settings');
    $form['instructions'] = [
      '#markup' => $this->t(
        "These settings affect how authorized users manage content."),
    ];
    $form['show_book_in_lessons_list'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('On the lessons list, show the books lessons are in'),
      '#description' => $this->t(
        "This helps when there is more than one lesson book, or
        when finding lessons that are not part of a book.
        Otherwise, the book column takes screen space for no reason."
      ),
      '#default_value' => $config->get('content_management.show_book_in_lessons_list'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settingsGroup = $this->config('skilling.settings');
    $settingsGroup->set('content_management.show_book_in_lessons_list',
        $form_state->getValue('show_book_in_lessons_list'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
