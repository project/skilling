<?php

namespace Drupal\skilling\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_skilling",
 *   title = @Translation("Skilling Filter"),
 *   description = @Translation("Translate Skilling markup to HTML."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class SkillingFilter extends FilterBase {

  /**
   * Performs the filter processing.
   *
   * @param string $text
   *   The text string to be filtered.
   * @param string $langcode
   *   The language code of the text to be filtered.
   *
   * @return \Drupal\filter\FilterProcessResult
   *   The filtered text, wrapped in a FilterProcessResult object, and possibly
   *   with associated assets, cacheability metadata and placeholders.
   *
   * @see \Drupal\filter\FilterProcessResult
   */
  public function process($text, $langcode) {
    // Get the node being viewed, if there is one.
    /** @var \Drupal\skilling\Utilities $utilities */
    $utilities = \Drupal::service('skilling.utilities');
    $currentNode = $utilities->getCurrentNode();
    $parser = \Drupal::service('skilling.skillingparser');
    $result = $parser->parse($text, $currentNode);
    $markup = new FilterProcessResult($result);
    $markup->setAttachments(array(
      'library' => array('skilling/display'),
    ));
    // Content is not cachable. All of the conditions and things.
    $markup->setCacheMaxAge(0);
    return $markup;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p><a href="https://textile-lang.com/" target="_blank">Textile reference</a></p>
        ');
    }
    else {
      return $this->t('<a href="https://textile-lang.com/" target="_blank">Textile reference</a>');
    }
  }

}
