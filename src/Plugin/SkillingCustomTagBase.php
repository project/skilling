<?php

namespace Drupal\skilling\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\skilling\SkillingParser\SkillingParser;

/**
 * Base class for Skilling custom tag plugins.
 */
abstract class SkillingCustomTagBase extends PluginBase implements SkillingCustomTagInterface {

  const PARSING_ERROR_CLASS = 'skilling-parsing-error';
  const OPTION_ERROR_CLASS = 'skilling-option-error';

  const INTERNAL_NAME_ALTS = ['internal_name', 'internal-name', 'internalname'];

  /**
   * The parser this class is being used by.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $utilitiesService;


  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->ajaxSecurityService = \Drupal::service('skilling.ajax_security');
    $this->utilitiesService = \Drupal::service('skilling.utilities');
  }

  /**
   * Get the utilities service.
   *
   * @return \Drupal\skilling\Utilities
   */
  public function getUtilities() {
    return $this->utilitiesService;
  }


  /**
   * Remember the parser this class is being used by.
   *
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   *   The parser.
   */
  public function setParser(SkillingParser $parser) {
    $this->parser = $parser;
  }

  /**
   * Retrieve the @tag property from the annotation and return it.
   *
   * @return string
   *   Tag property value.
   */
  public function tag() {
    return $this->pluginDefinition['tag'];
  }

  /**
   * Retrieve the @description property from the annotation and return it.
   *
   * @return string
   *   Description property value.
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Retrieve the @hasCloseTag property from the annotation and return it.
   *
   * @return bool
   *   True if the tag type uses a close tag.
   */
  public function hasCloseTag() {
    return $this->pluginDefinition['has_close_tag'];
  }

  /**
   * Find the first option with a name in the given list.
   *
   * Used to give custom tags leeway in the spelling of
   * option names, e.g., internal_name, internal-name, internalname.
   *
   * @param array $options
   *   Options given by user.
   * @param array $optionNames
   *   Option name variants.
   *
   * @return string
   *   Option value, MT if not found.
   */
  public function findOption(array $options, array $optionNames = NULL) {
    $result = '';
    if (!is_null($optionNames)) {
      foreach ($optionNames as $optionName) {
        if (isset($options[$optionName])) {
          $result = $options[$optionName];
          break;
        }
      }
    }
    return trim($result);
  }

  /**
   * Find the internal_name option.
   *
   * @param array $options
   *   The tag's options, from the user.
   *
   * @return string
   *   The name, MT if option not found.
   */
  public function findInternalNameOption(array $options) {
    $internalName = $this->findOption(
      $options,
      self::INTERNAL_NAME_ALTS
    );
    return $internalName;
  }

  /**
   * Format an error message.
   *
   * @param string $message
   *   The message to show.
   *
   * @return string
   *   Formatted message.
   */
  public function formatCustomTagError($message) {
    $result = "\np(" . self::PARSING_ERROR_CLASS . "). $message\n\n";
    // Tell the parser about it.
    $this->parser->customTagErrorReported($result);
    return $result;
  }

  /**
   * Called before parser starts.
   */
  public function preparse() {
  }

}
