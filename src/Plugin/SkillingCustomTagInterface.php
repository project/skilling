<?php

namespace Drupal\skilling\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\node\Entity\Node;

/**
 * Defines an interface for Skilling custom tag plugins.
 */
interface SkillingCustomTagInterface extends PluginInspectionInterface {

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $node
   *   Node the tag is on.
   *
   * @return mixed
   *   Rendered result.
   */
  public function processTag($content = '', array $options = NULL, Node $node = NULL);

  /**
   * Find the first option with a name in the given list.
   * Used to give custom tags leeway in the spelling of
   * option names, e.g., internal_name, internal-name, internalname.
   *
   * @param array $options Options for a tag instance.
   * @param array $optionNames Option name variants.
   *
   * @return string
   *   Option value, MT string if not found.
   */
  public function findOption(array $options, array $optionNames=NULL);

  public function formatCustomTagError($message);

  public function findInternalNameOption(array $options);

  /**
   * Called before parser starts.
   */
  public function preparse();

  }
