<?php

namespace Drupal\skilling\Plugin\CKEditorPlugin;

use Drupal\ckeditor\Annotation\CKEditorPlugin;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Annotation\Translation;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "codeTag" plugin.
 *
 * @CKEditorPlugin(
 *   id = "codeTag",
 *   label = @Translation("CodeTag"),
 * )
 */
class CodeTag extends CKEditorPluginBase {
  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $result =  drupal_get_path('module', 'skilling') . '/libraries/codeTag/plugin.js';
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }



  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $result = array(
      'Code' => array(
        'label' => $this->t('CodeTag'),
        'image' => drupal_get_path('module', 'skilling') . '/libraries/codeTag/icons/code.png',
      ),
    );
    return $result;
  }

}
