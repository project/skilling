<?php

namespace Drupal\skilling\Plugin\CKEditorPlugin;

use Drupal\ckeditor\Annotation\CKEditorPlugin;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Annotation\Translation;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Templates" plugin, for CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "templates",
 *   label = @Translation("Templates Plugin")
 * )
 */
class TemplatesPlugin extends PluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
//    return ['skilling/templates'];
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $result =  drupal_get_path('module', 'skilling')
      . '/libraries/templates/plugin.js';
    return $result;
  }

  /**
   * @return array
   */
  public function getButtons() {
    $iconImage = drupal_get_path('module', 'skilling') .
      '/libraries/templates/icons/templates.png';

    return [
      'Templates' => [
        'label' => t('Add Skilling templates'),
        'image' => $iconImage,
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
//  public function getConfig(Editor $editor) {
//    return [];
//  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];
    $settings = $editor->getSettings();
    // Set replace content default value if set.
    if (isset($settings['plugins']['templates']['replace_content'])) {
      $config['templates_replaceContent'] = $settings['plugins']['templates']['replace_content'];
    }
    // Set template files default value if set.
    if (isset($settings['plugins']['templates']['template_path']) && !empty($settings['plugins']['templates']['template_path'])) {
      $config['templates_files'] = [$settings['plugins']['templates']['template_path']];
    }
    else {
      // Use templates plugin default file.
      $config['templates_files'] = $this->getTemplatesDefaultPath();
    }
    return $config;
  }

  /**
   * Generate the path to the template file.
   *
   * @return array
   *   List of path to the template file
   */
  private function getTemplatesDefaultPath() {
    $defaultPath = base_path() . drupal_get_path('module', 'skilling') .
      '/libraries/templates/templates/default.js';
    return [$defaultPath];
  }

}
