<?php
namespace Drupal\skilling\Plugin\views\access;

use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An access plugin for views. It inspects a context filter argument,
 * interpreting it as a class nid. If the current user is an instructor
 * or grader of the class, allows access.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "ViewsInstructorGraderOfStudentAccess",
 *     title = @Translation("Instructor or grader of student"),
 *     help = @Translation("Access based on whether the current user is an instructor or grader of the user with uid in a contextual filter parameter."),
 * )
 */
class ViewsInstructorGraderOfStudentAccess extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /** @var \Drupal\skilling\SkillingCurrentUser */
  protected $skillingCurrentUser;

  /**
   * Constructs an InstructorAccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.skilling_current_user')
    );
  }

  /**
   * Determine if the current user is an instructor of the
   * class identified in a contextual filter argument.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user who wants to access this view.
   *
   * @return bool
   *   Returns whether the user has access to the view.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(AccountInterface $account) {
    //This works, but is positional. Is there a better way?
    if (! isset($this->view->element['#arguments'][0])) {
      return FALSE;
    }
    $possibleStudentId = $this->view->element['#arguments'][0];
    if (!$possibleStudentId || !is_numeric($possibleStudentId)) {
      return FALSE;
    }
    $currentUserAsInstructorStudents
      = $this->skillingCurrentUser->getStudentsNidsOfSpecialClassRole(
      SkillingConstants::CLASS_ROLE_INSTRUCTOR
    );
    $currentUserAsGraderStudents
      = $this->skillingCurrentUser->getStudentsNidsOfSpecialClassRole(
      SkillingConstants::CLASS_ROLE_GRADER
    );
    $isStudentsInstructor = in_array($possibleStudentId, $currentUserAsInstructorStudents);
    $isStudentsGrader = in_array($possibleStudentId, $currentUserAsGraderStudents);
    $isAdmin = $this->skillingCurrentUser->isAdministrator();
    return $isStudentsInstructor || $isStudentsGrader || $isAdmin;
  }

  /**
   * Allows access plugins to alter the route definition of a view.
   *
   * Some requirement has to be added here. Don't know why. Check .services.yml
   * for a tag.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to change.
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_access', 'TRUE');
//    $route->setRequirement('_instructor_check', 'TRUE');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
