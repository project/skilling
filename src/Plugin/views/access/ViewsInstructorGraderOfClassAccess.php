<?php

namespace Drupal\skilling\Plugin\views\access;

use Drupal\skilling\SkillingCurrentUser;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An access plugin for views.
 *
 * It inspects a context filter argument,
 * interpreting it as a class nid. If the current user is an instructor
 * or grader of the class, allows access.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "InstructorGraderAccess",
 *     title = @Translation("Instructor or grader of class"),
 *     help = @Translation("Access based on whether the current user is an instructor or grader of a class with node id in a contextual filter parameter."),
 * )
 */
class ViewsInstructorGraderOfClassAccess extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Constructs an InstructorAccess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   Skilling current user service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SkillingCurrentUser $skillingCurrentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.skilling_current_user')
    );
  }

  /**
   * Is current user instructor of class with nid in contextual filter argument?
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user who wants to access this view.
   *
   * @return bool
   *   Returns whether the user has access to the view.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(AccountInterface $account) {
    // This works, but is positional. Is there a better way?
    if (!isset($this->view->element['#arguments'][0])) {
      return FALSE;
    }
    $classId = $this->view->element['#arguments'][0];
    if (!$classId || !is_numeric($classId)) {
      return FALSE;
    }
    $isClassInstructor = $this->skillingCurrentUser->isInstructorOfClassNid($classId);
    $isClassGrader = $this->skillingCurrentUser->isGraderOfClassNid($classId);
    $isAdmin = $this->skillingCurrentUser->isAdministrator();
    return $isClassInstructor || $isClassGrader || $isAdmin;
  }

  /**
   * Allows access plugins to alter the route definition of a view.
   *
   * Some requirement has to be added here. Check .services.yml
   * for a tag.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to change.
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_access', 'TRUE');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Determine whether configuration is valid.
   */
  protected function isValidConfig() {
    return TRUE;
  }

}
