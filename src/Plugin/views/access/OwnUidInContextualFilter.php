<?php
namespace Drupal\skilling\Plugin\views\access;

use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An access plugin for views. It inspects a context filter argument.
 * If it exists, the code interprets that as a UID. If the current
 * user does not have that UID, access is denied.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "OwnUidInUrl",
 *     title = @Translation("Own UID in contextual filter"),
 *     help = @Translation("Deny access if the contextual filter argument is not the UID of the current user."),
 * )
 */
class OwnUidInContextualFilter extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /** @var \Drupal\skilling\SkillingCurrentUser */
  protected $skillingCurrentUser;

  /**
   * Constructs an OwnUidInContextualFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.skilling_current_user')
    );
  }

  /**
   * Determine if the current user is an instructor of the
   * class identified in a contextual filter argument.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user who wants to access this view.
   *
   * @return bool
   *   Returns whether the user has access to the view.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function access(AccountInterface $account) {
    // Not for anonymous user.
    if ($this->skillingCurrentUser->isAnonymous()) {
      return FALSE;
    }
    // This works, but is positional. Is there a better way?
    if (!isset($this->view->element['#arguments'][0])) {
      // There is no value. Contextual filter will use default. In the view,
      // set that to the UID of the current user.
      return TRUE;
    }
    $uid = $this->view->element['#arguments'][0];
    $currentUserUid = $this->skillingCurrentUser->id();
    return $uid == $currentUserUid;
  }

  /**
   * Allows access plugins to alter the route definition of a view.
   *
   * Some requirement has to be added here. KRM doesn't know why.
   * Check .services.yml
   * for a tag, if used.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to change.
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_access', 'TRUE');
    //    $route->setRequirement('_own_id_in_filter_check', 'TRUE');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
