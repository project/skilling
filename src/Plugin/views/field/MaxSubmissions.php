<?php

namespace Drupal\skilling\Plugin\views\field;

use Drupal;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing the max submissions allowed for an exercise.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("max_submissions")
 */
class MaxSubmissions extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\views\ResultRow $row
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function render(ResultRow $row) {
    // Default if exercise not in class, or no current class.
    $result = $this->t('N/A');
    $exercise = $row->_entity;
    // Is it an exercise?
    if ($exercise->bundle() === SkillingConstants::EXERCISE_CONTENT_TYPE) {
      /** @var SkillingCurrentClass $currentClass */
      $currentClass = Drupal::service('skilling.current_class');
      if ($currentClass->isCurrentClass()) {
        /** @var \Drupal\skilling\ExerciseDueRecord|null $exerciseDueData */
        $exerciseDueData
          = $currentClass->getExerciseDueRecordForExercise($exercise->id());
        if (!is_null($exerciseDueData)) {
          $maxSubs = $exerciseDueData->getMaximumSubmissions();
          if ($maxSubs && is_numeric($maxSubs)) {
            $result = $maxSubs;
          }
        }
      }
    }
    return $result;
  }

}
