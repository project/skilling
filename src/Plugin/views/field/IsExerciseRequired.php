<?php

namespace Drupal\skilling\Plugin\views\field;

use Drupal;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing whether an exercise is required.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("is_exercise_required")
 */
class IsExerciseRequired extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
//  protected function defineOptions() {
//    $options = parent::defineOptions();
//
//    $options['hide_alter_empty'] = ['default' => FALSE];
//    return $options;
//  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function render(ResultRow $row) {
    // Default if exercise not in class, or no current class.
    $result = $this->t('N/A');
    $exercise = $row->_entity;
    // Is it an exercise?
    if ($exercise->bundle() === SkillingConstants::EXERCISE_CONTENT_TYPE) {
      /** @var \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClass */
      $currentClass = Drupal::service('skilling.current_class');
      if ($currentClass->isCurrentClass()) {
        $exerciseDueData
          = $currentClass->getExerciseDueRecordForExercise($exercise->id());
        if (!is_null($exerciseDueData)) {
          $isRequired = $exerciseDueData->getRequired();
          $result = $isRequired
            ? (string) $this->t('Yes')
            : (string) $this->t('No');
        }
      }
    }
    return $result;
  }

}
