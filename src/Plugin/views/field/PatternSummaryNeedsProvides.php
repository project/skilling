<?php

namespace Drupal\skilling\Plugin\views\field;

use DateInterval;
use DateTime;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing the summary, needs and provides for a pattern.
 *
 * Used in view showing patterns.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("pattern_summary_needs_provides")
 */
class PatternSummaryNeedsProvides extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
//  protected function defineOptions() {
//    $options = parent::defineOptions();
//
//    $options['hide_alter_empty'] = ['default' => FALSE];
//    return $options;
//  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function render(ResultRow $values) {
    // Get the class start date.
    /** @var \Drupal\node\Entity\Node $pattern */
    $pattern = $values->_entity;
    // Grab the summary, and parse it.
    $summary = $pattern->field_summary->value;
    /** @var \Drupal\skilling\SkillingParser\SkillingParser $parser */
    $parser = \Drupal::service('skilling.skillingparser');
    $summaryParsed = $parser->parse($summary);
    $needs = $pattern->field_needs->value;
    $provides = $pattern->field_provides->value;
    $renderable = [
      '#theme' => 'pattern_summary_needs_provides',
      '#summary' => $summaryParsed,
      '#needs' => $needs,
      '#provides' => $provides,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
