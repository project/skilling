<?php

namespace Drupal\skilling\Plugin\views\field;

use DateInterval;
use DateTime;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing whether a notice has been read.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("is_notice_unread")
 */
class IsNoticeUnread extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
//  protected function defineOptions() {
//    $options = parent::defineOptions();
//
//    $options['hide_alter_empty'] = ['default' => FALSE];
//    return $options;
//  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function render(ResultRow $values) {
    $result = '';
    /** @var \Drupal\node\Entity\Node $notice */
    $notice = $values->_entity;
    $whenRead = $notice->get('field_when_read')->value;
    if (is_null($whenRead)) {
      $result = $this->t('Unread');
    }
    return $result;
  }

}
