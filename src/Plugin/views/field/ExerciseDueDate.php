<?php

namespace Drupal\skilling\Plugin\views\field;

use DateInterval;
use DateTime;
use Drupal;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing an exercise's due date.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("exercise_due_date")
 */
class ExerciseDueDate extends  Drupal\views\Plugin\views\field\Date { //FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\views\ResultRow $row
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   * @throws \Exception
   */
  public function render(ResultRow $row) {
    // Default if exercise not in class, or no current class.
    $result = $this->t('N/A');
    $exercise = $row->_entity;
    // Is it an exercise?
    if ($exercise->bundle() === SkillingConstants::EXERCISE_CONTENT_TYPE) {
      /** @var SkillingCurrentClass $currentClass */
      $currentClass = Drupal::service('skilling.current_class');
      if ($currentClass->isCurrentClass()) {
        $exerciseDueData
          = $currentClass->getExerciseDueRecordForExercise($exercise->id());
        if (!is_null($exerciseDueData)) {
          /** @var Drupal\skilling\Utilities $utilities */
          $utilities = Drupal::service('skilling.utilities');
          $classStartDate = $currentClass->getStartDate();
          $classStartDate = new DateTime($classStartDate);
          $dayExerDue = $exerciseDueData->getDay() - 1;
          $result = $utilities->getCalendarDateFromDayOffsetFormatted(
            $classStartDate,
            $dayExerDue
          );
        }
      }
    }
    return $result;
  }

}
