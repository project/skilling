<?php

namespace Drupal\skilling\Plugin\views\field;

use DateInterval;
use DateTime;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Show whether owner of the current node (intended as submission)
 * is in a class where the current user is an instructor.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("instructor_of_node_owner")
 */
class InstructorOfNodeOwner extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $values->_entity;
    /** @var \Drupal\user\UserInterface $nodeOwner */
    $nodeOwner = $node->getOwner();
    /** @var \Drupal\skilling\SkillingUserFactory $skillingUserFactory */
    $skillingUserFactory = \Drupal::service('skilling.skilling_user_factory');
    $skillingOwner = $skillingUserFactory->makeSkillingUser($nodeOwner->id());
    /** @var \Drupal\skilling\SkillingCurrentUser $currentUserService */
    $currentUserService = \Drupal::service('skilling.skilling_current_user');

    /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $relationshipService */
    $relationshipService = \Drupal::service('skilling.check_user_relationships');
    $isInstructor = $relationshipService->isUserInstructorOfUser(
      $currentUserService, $skillingOwner
    );
    $result = ($isInstructor || $currentUserService->isAdministrator())
      ?  'yes' : 'no';
    return $result;
  }

}
