<?php

namespace Drupal\skilling\Plugin\views\field;

use DateInterval;
use DateTime;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A field showing the date you get by adding a number of days to a date.
 *
 * Used in view showing events in a course.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("date_plus_days_field")
 */
class DateEventHappens extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function render(ResultRow $values) {
    // Get the class start date.
    /** @var \Drupal\node\Entity\Node $class */
    $class = $values->_entity;
    $classStartDate = $class->get(SkillingConstants::FIELD_WHEN_STARTS)
      ->getValue()[0]['value'];
//    $classStartDate = $class->field_when_starts->value;
    $classStartDate = new DateTime($classStartDate);
    // Get the day offset the event happens.
    /** @var \Drupal\paragraphs\ParagraphInterface $calendarEvent */
    $calendarEvent = $values->_relationship_entities['field_calendar_events'];
    $eventDayOffset = $calendarEvent->field_day->value;
    // Compute the event date.
    $eventDate = clone $classStartDate;
    $eventDate->add(new DateInterval('P' . $eventDayOffset . 'D'));
    // Format the event date.
    $dateFormat = 'D, M j, Y';
    $eventDateFormatted = $eventDate->format($dateFormat);
    return $eventDateFormatted;
  }

}
