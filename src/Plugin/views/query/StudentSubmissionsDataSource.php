<?php /** @noinspection PhpUnusedParameterInspection */

namespace Drupal\skilling\Plugin\views\query;

use DateInterval;
use DateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Submissions;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;

/**
 * Student's submissions views query plugin.
 *
 * Exposes data one student's exercise submissions.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "student_submissions",
 *   title = @Translation("Student's submissions"),
 *   help = @Translation("One student's exercise submissions.")
 * )
 */
class StudentSubmissionsDataSource extends QueryPluginBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Submissions service.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Skilling current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Name of the field to sort by.
   *
   * @var string
   */
  protected $sortField = 'when_first_submission';

  /**
   * Sort direction, asc or desc.
   *
   * @var string
   */
  protected $sortDirection = 'desc';

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $pluginId
   *   Plugin id.
   * @param mixed $pluginDefinition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   Skilling current user service.
   * @param \Drupal\skilling\Submissions $submissionsService
   *   Submissions service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param SkillingCurrentClass $currentClass
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skillingCurrentUser,
    Submissions $submissionsService,
    Renderer $renderer,
    SkillingCurrentClass $currentClass
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->submissionsService = $submissionsService;
    $this->renderer = $renderer;
    $this->currentClass = $currentClass;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.submissions'),
      $container->get('renderer'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * Workaround to ignore joins that views will be looking for.
   *
   * @param mixed $table
   *   Table.
   * @param null|mixed $relationship
   *   Relationship.
   *
   * @return string
   *   An MT string.
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * Workaround for method Views uses to limit fields in a query.
   *
   * @param mixed $table
   *   Table.
   * @param mixed $field
   *   Field.
   * @param string $alias
   *   Alias.
   * @param array $params
   *   Params.
   *
   * @return mixed
   *   Something.
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }


  /**
   * Called by Views to tell this plugin how to sort the rows.
   *
   * @param string $table
   *   Ignored.
   * @param string $field
   *   Ignored.
   * @param string $order
   *   Sorting orer: asc or desc.
   * @param string $alias
   *   Name of the field to sort by.
   * @param array $options
   *   Ignored.
   */
  public function addOrderBy($table, $field, $order, $alias, $options=[]) {
    $this->sortField = $alias;
    $this->sortDirection = $order;
  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }
    /** @noinspection PhpUndefinedFieldInspection */
    $this->where[$group]['conditions'][] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   * @throws \Exception
   */
  public function execute(ViewExecutable $view) {
    $studentId = $this->skillingCurrentUser->id();
    $submissions = $this->submissionsService->getPublishedSubmissionsForUid($studentId);
    $exerciseNids = $this->submissionsService->extractExerciseNidsFromSubmissions($submissions);
    $exercises = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($exerciseNids);
    // Get exercise due records for the current class.
    $classExerciseDues = $this->currentClass->getExerciseDueRecords();
    if (isset($this->where)) {
      foreach ($this->where as $where_group => $where) {
        foreach ($where['conditions'] as $condition) {
          // Remove dot from beginning of the string.
          $field_name = ltrim($condition['field'], '.');
          $filters[$field_name] = $condition['value'];
        }
      }
    }

    $rows = [];
    /* @var \Drupal\node\Entity\Node $exercise */
    foreach ($exercises as $exercise) {
      // Apply the completed filter.
      $submissionsDeets = $this->submissionsService->createSubmissionLinks($exercise);
      if (isset($filters['completed'])) {
        if ($filters['completed'][0] === 'completed') {
          if (!$submissionsDeets['complete']) {
            continue;
          }
        }
        if ($filters['completed'][0] === 'not_completed') {
          if ($submissionsDeets['complete']) {
            continue;
          }
        }
      }
      // Find the exercise due data for the exercise.
      $exerciseDue = NULL;
      foreach ($classExerciseDues as $record) {
        if ($record->getExerciseId() == $exercise->id()) {
          $exerciseDue = $record;
          break;
        }
      }
      $submissionStatus = $this->submissionsService
        ->getSubmissionStatusForStudentExercise(
          $this->skillingCurrentUser,
          $exercise->id(),
          $this->currentClass->getWrappedSkillingClass()
        );
      if (isset($filters['status'])) {
        $valueToFilterFor = $filters['status'][0];
        if ($valueToFilterFor !== $submissionStatus) {
          continue;
        }
      }
      // Compute max submissions allowed.
      $exerciseMaxSubs = $exerciseDue->getMaximumSubmissions();
      if (is_null($exerciseMaxSubs)) {
        $exerciseMaxSubs = $this->currentClass->getDefaultMaxSubmissions();
      }
      $exerciseMaxSubsDisplay =
        is_null($exerciseMaxSubs)
        ? $this->t('No limit')
        : $exerciseMaxSubs;
      $row = [];
      $row['exercise_nid'] = $exercise->id();
      $row['exercise_title'] = $exercise->getTitle();
      $row['exercise_internal_name'] = $exercise->field_internal_name->value;
      $row['exercise_max_subs'] = $exerciseMaxSubsDisplay;
      // Compute when due date.
      if (is_null($exerciseDue)) {
        $row['exercise_due_date'] = null;
      }
      else {
        $classStartDate = new DateTime($this->currentClass->getStartDate());
        $dueDaysInterval = new DateInterval(
          'P' . ($exerciseDue->getDay() - 1) . 'D'
        );
        $exerciseDueDate = clone $classStartDate;
        $exerciseDueDate->add($dueDaysInterval);
        $row['exercise_due_date'] = $exerciseDueDate->getTimestamp();
      }
      $row['where_referenced'] = [];
      // Get array of lesson ids that reference the exercise.
      $whereReferencedValues = $exercise->field_where_referenced->getValue();
      // Are there any?
      if (count($whereReferencedValues) > 0) {
        // Extract the lesson nids.
        $lessonNids = [];
        foreach ($whereReferencedValues as $whereReferencedValue) {
          $lessonNids[] = $whereReferencedValue['target_id'];
        }
        // Load the lessons that refer to the exercise.
        $lessonsWithExerRef = $this->entityTypeManager->getStorage('node')
          ->loadMultiple($lessonNids);
        if (count($lessonsWithExerRef) === 0) {
          // Should not happen.
          throw new SkillingException(
            'No exercises loaded', __FILE__, __LINE__);
        }
        // Make links for each exercise.
        $exerciseLinks = [];
        /** @var \Drupal\node\Entity\Node $lesson */
        foreach ($lessonsWithExerRef as $lesson) {
          $nid = $lesson->id();
          // Make URL to the position of the exercise inside the lesson.
          $url = Url::fromRoute(
            'entity.node.canonical',
            ['node' => $nid],
            ['fragment' => $row['exercise_internal_name']]
          );
          $link = [
            '#type' => 'link',
            '#url' => $url,
            '#title' => $lesson->getTitle(),
          ];
          $rendered = $this->renderer->render($link);
          $exerciseLinks[] = $rendered;
        }
        $renderable = [
          '#theme' => 'lesson_links',
          '#links' => $exerciseLinks,
        ];
        $rendered = $this->renderer->render($renderable);
        $row['where_referenced'] = $rendered;
      }
      $row['submissions'] = $this->renderExerciseSubmissions($submissionsDeets);
      $row['completed'] = $submissionsDeets['complete'];
      $row['status'] = SkillingConstants::getSubmissionStatusDisplayValue($submissionStatus);
      $row['when_first_submission'] = $this->getWhenFirstSubmission($submissionsDeets);
      $rows[] = $row;
    }
    // Sort by whatever field is named in $this->sortField.
    if ($this->sortDirection === 'asc') {
      usort($rows, function ($a, $b) {
        if ($a[$this->sortField] < $b[$this->sortField]) {
          return -1;
        }
        if ($a[$this->sortField] > $b[$this->sortField]) {
          return 1;
        }
        return 0;
      });
    }
    else {
      // Descending.
      usort($rows, function ($a, $b) {
        if ($a[$this->sortField] < $b[$this->sortField]) {
          return 1;
        }
        if ($a[$this->sortField] > $b[$this->sortField]) {
          return -1;
        }
        return 0;
      });
    }
    $index = 0;
    foreach ($rows as $row) {
      $row['index'] = $index;
      $view->result[] = new ResultRow($row);
      $index++;
    }
  }

  /**
   * Render exercise submissions.
   *
   * Uses the same template used when inserting an exercise into a lesson.
   *
   * @param array $exerciseSubmissions
   *   Submission data.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   Rendered submissions.
   *
   * @throws \Exception
   */
  protected function renderExerciseSubmissions(array $exerciseSubmissions) {
    $renderable = [];
    $renderable['#theme'] = 'student_submissions_for_exercise';
    $renderable['#exercise_nid'] = $exerciseSubmissions['exercise_nid'];
    $renderable['#internal_name'] = $exerciseSubmissions['exercise_name'];
    $renderable['#submission_links'] = $exerciseSubmissions;
    $renderable['#attached'] = [
      'library' => 'skilling/exercise-links',
      'drupalSettings' => [
        'skillingFloatQueryParam' => SkillingConstants::FLOATER_QUERY_STRING,
      ],
    ];
    $renderedSubmissionDeets = $this->renderer->render($renderable);
    return $renderedSubmissionDeets;
  }

  /**
   * Get the earliest date among some submissions.
   *
   * The param array is formatted for rendering.
   *
   * @param array $exerciseSubmissions
   *   Submission data.
   *
   * @return int
   *   Timestamp of the earliest submission.
   */
  protected function getWhenFirstSubmission(array $exerciseSubmissions) {
    $submissions = $exerciseSubmissions['submissions'];
    // Timestamp a little in the future, as start value.
    $whenFirstSubmission = time() + 3600;
    foreach ($submissions as $submission) {
      // Get timestamp of submission date/time.
      $when = strtotime($submission['when_created']);
      if ($when < $whenFirstSubmission) {
        $whenFirstSubmission = $when;
      }
    }
    return $whenFirstSubmission;
  }

}
