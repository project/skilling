<?php
namespace Drupal\skilling\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\filter\Standard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show unread notices only?
 * @ViewsFilter("unread_filter")
 */
class UnreadNotices extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
//    $form['value'] = [
//      '#type' => 'checkbox',
//      '#title' => $this->t('Unread only?'),
//      '#default_value' => $this->value,
//    ];
    $form['value'] = array(
      '#type' => 'radios',
      '#title' => t('Notices to show'),
      '#options' => ['all' => 'Show all notices', 'unread' => 'Only show unread notices'],
      '#description' => t('Which notices would you like to see?'),
      '#default_value' => 'unread',
    );
  }

  /**
   * Override query so no filtering if the user doesn't select any options.
   */
  // Couldn't get this to work.
  public function query() {
//    parent::query();
//    if (!empty($this->value)) {
//      $this->query->addWhere(
//        0, //Default group
//        'node__field_when_read.field_when_read_value',
//        'NULL',
//        'IS'
//      );
//    }
  }
//(node__field_when_read.field_when_read_value IS NULL)

  public function postExecute(&$values) {
    if ($this->value[0] === 'unread') {
      foreach ($values as $key => $value) {
        $node = $value->_entity;
        $whenRead = $node->get('field_when_read')->value;
        if (!is_null($whenRead)) {
          unset($values[$key]);
          continue;
        }

      }

      $values = array_values($values);
    }

    parent::postExecute($values);
  }

}
