<?php
namespace Drupal\skilling\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
/**
 * Has the user completed the exercise?
 * @ViewsFilter("completed_exercise")
 */
class FilterCompleted extends FilterPluginBase {
  public $no_operator = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'select',
      '#options' => [
        'completed' => $this->t('Only show completed exercises'),
        'not_completed' => $this->t('Only show not completed exercises'),
      ],
      '#title' => $this->t('Filter by completion'),
      '#default_value' => $this->value,
    ];
  }

}
