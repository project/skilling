<?php
namespace Drupal\skilling\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\filter\Standard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Is the current user an instructor of the node owner for the class given in the URL?
 * @ViewsFilter("instructor_of_node_owner_in_class_filter")
 */
class FilterInstructorOfNodeOwnerInClass extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check for instructor of node owner in class (or admin)?'),
      '#default_value' => $this->value,
    ];
  }

  /**
   * Override query so no filtering if the user doesn't select any options.
   */
  // Couldn't get this to work.
  public function query() {
//    parent::query();
//    if (!empty($this->value)) {
//      $this->query->addWhere(
//        0, //Default group
//        'node__field_when_read.field_when_read_value',
//        'NULL',
//        'IS'
//      );
//    }
  }
//(node__field_when_read.field_when_read_value IS NULL)

  /**
   * @param $values
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function postExecute(&$values) {
    if ($this->value) {
      $classId = NULL;
      $filteredRecords = [];
      $isProcessingCanProceed = FALSE;
      /** @var \Symfony\Component\HttpFoundation\Request $request */
      $request = $this->view->getRequest();
      $url = $request->getRequestUri();
      if (strpos($url, '/preview') !== FALSE) {
        if (isset($this->view->args[0])) {
          $param = $this->view->args[0];
          if (is_numeric($param)) {
            $classId = $param;
            $isProcessingCanProceed = TRUE;
          }
        }
      }
      else {
        $urlParts = explode('/', $url);
        if (count($urlParts) > 2) {
          $lastPiece = $urlParts[count($urlParts) - 1];
          if (is_numeric($lastPiece)) {
            $classId = $lastPiece;
            $isProcessingCanProceed = TRUE;
          }
        }
      }
      $entityTypeManager = \Drupal::entityTypeManager();
      if ($isProcessingCanProceed) {
        // Got a class id.
        // Is it for a class?
        /** @var \Drupal\node\NodeInterface $classNode */
        $classNode = $entityTypeManager->getStorage('node')
          ->load($classId);
        if ($classNode->getType() !== SkillingConstants::CLASS_CONTENT_TYPE) {
          $isProcessingCanProceed = FALSE;
        }
      }
      /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $userRelationshipsService */
      $userRelationshipService = \Drupal::service('skilling.check_user_relationships');
      /** @var \Drupal\skilling\SkillingCurrentUser $currentUserService */
      $currentUserService = \Drupal::service('skilling.skilling_current_user');
      if ($isProcessingCanProceed) {
        // Is the current user an instructor of the class?
        if (!$userRelationshipService->isInstructorIdOfClassId(
          $currentUserService->id(), $classId
        )) {
          $isProcessingCanProceed = FALSE;
        }
      }
      if ($isProcessingCanProceed) {
        /** @var \Drupal\skilling\SkillingUserFactory $skillingUserFactory */
        $skillingUserFactory = \Drupal::service('skilling.skilling_user_factory');
        $filteredRecords = [];
        foreach ($values as $key => $value) {
          /** @var \Drupal\node\NodeInterface $node */
          $node = $value->_entity;
          $nodeOwner = $node->getOwner();
          $skillingOwner = $skillingUserFactory->makeSkillingUser($nodeOwner->id());
          $isInstructor = $userRelationshipService->isUserInstructorOfUser(
            $currentUserService, $skillingOwner
          );
          $keepRow = ($isInstructor || $currentUserService->isAdministrator());
          if ($keepRow) {
            $filteredRecords[] = $value;
          }
        }
      }
      $values = $filteredRecords;
    }
    parent::postExecute($values);
  }

}
