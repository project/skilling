<?php
namespace Drupal\skilling\Plugin\views\filter;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\filter\Standard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Is the current user an instructor of the node owner?
 * @ViewsFilter("instructor_of_node_owner_filter")
 */
class FilterInstructorOfNodeOwner extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }
  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check for instructor of node owner (or admin)?'),
      '#default_value' => $this->value,
    ];
//    $form['value'] = array(
//      '#type' => 'radios',
//      '#title' => t('Notices to show'),
//      '#options' => ['all' => 'Show all notices', 'unread' => 'Only show unread notices'],
//      '#description' => t('Which notices would you like to see?'),
//      '#default_value' => 'unread',
//    );
  }

  /**
   * Override query so no filtering if the user doesn't select any options.
   */
  // Couldn't get this to work.
  public function query() {
//    parent::query();
//    if (!empty($this->value)) {
//      $this->query->addWhere(
//        0, //Default group
//        'node__field_when_read.field_when_read_value',
//        'NULL',
//        'IS'
//      );
//    }
  }
//(node__field_when_read.field_when_read_value IS NULL)

  /**
   * @param $values
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function postExecute(&$values) {
    if ($this->value) {
      /** @var \Drupal\skilling\SkillingCurrentUser $currentUserService */
      $currentUserService = Drupal::service('skilling.skilling_current_user');
      /** @var \Drupal\skilling\Access\SkillingCheckUserRelationships $relationshipService */
      $relationshipService = Drupal::service('skilling.check_user_relationships');
      $filteredRecords = [];
      foreach ($values as $key => $value) {
        /** @var \Drupal\node\NodeInterface $node */
        $node = $value->_entity;
        $nodeOwner = $node->getOwner();
        /** @var \Drupal\skilling\SkillingUserFactory $skillingUserFactory */
        $skillingUserFactory = Drupal::service('skilling.skilling_user_factory');
        $skillingOwner = $skillingUserFactory->makeSkillingUser($nodeOwner->id());
        $isInstructor = $relationshipService->isUserInstructorOfUser(
          $currentUserService, $skillingOwner
        );
        $keepRow = ($isInstructor || $currentUserService->isAdministrator());
        if ($keepRow) {
          $filteredRecords[] = $value;
        }
      }
      $values = $filteredRecords;
    }
    parent::postExecute($values);
  }

}
