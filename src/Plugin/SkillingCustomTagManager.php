<?php

namespace Drupal\skilling\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Skilling custom tag plugin manager.
 */
class SkillingCustomTagManager extends DefaultPluginManager {

  /**
   * Constructs a new SkillingCustomTagManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SkillingCustomTag', $namespaces, $module_handler, 'Drupal\skilling\Plugin\SkillingCustomTagInterface', 'Drupal\skilling\Annotation\SkillingCustomTag');

    $this->alterInfo('skilling_skilling.custom_tag_info');
    $this->setCacheBackend($cache_backend, 'skilling_skilling.custom_tag_plugins');
  }

}
