<?php

namespace Drupal\skilling\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that a user doesn't have a bad overlap in class roles, like
 * grader and student.
 *
 * @Constraint(
 *   id = "class_roles_overlap",
 *   label = @Translation("Class roles for an enrollment don't overlap inappropriately", context = "Validation"),
 *   type = "string"
 * )
 */
class ClassRolesOverlap extends Constraint {

  public $studentOverlapErrorMessage
      = 'Sorry, someone who is a student in a class cannot be an instructor '
        . 'or grader for the same class.';

}
