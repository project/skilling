<?php

namespace Drupal\skilling\Plugin\Validation\Constraint;

use Drupal\skilling\SkillingConstants;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\skilling\Utilities as SkillingUtilities;

class ClassRolesOverlapValidator extends ConstraintValidator {

  /**
   * Checks if the passed value is valid.
   *
   * @param mixed $value The value that should be validated
   * @param Constraint $constraint The constraint for the validation
   */
  public function validate($value, Constraint $constraint) {
    /** @var SkillingUtilities $utilities */
    $utilities = \Drupal::service('skilling.utilities');
    //Get the list of values, and flatten it.
    $flat = $utilities->flatten($value->getValue());
    $isStudent = in_array(SkillingConstants::CLASS_ROLE_STUDENT, $flat);
    $isGrader = in_array(SkillingConstants::CLASS_ROLE_GRADER, $flat);
    $isInstructor = in_array(SkillingConstants::CLASS_ROLE_INSTRUCTOR, $flat);
    $badRoleOverlap = $isStudent && $isGrader || $isStudent && $isInstructor;
    if ( $badRoleOverlap ) {
      $this->context->addViolation($constraint->studentOverlapErrorMessage);
    }
  }
}
