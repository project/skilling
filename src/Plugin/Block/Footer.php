<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Skilling footer block.
 *
 * @Block(
 *  id = "skilling_footer",
 *  admin_label = @Translation("Skilling footer"),
 * )
 */
class Footer extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['class_selector'] = [
      '#theme' => 'skilling_footer',
    ];
    return $build;
  }

}
