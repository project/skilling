<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\Utilities;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'ClassSelector' block.
 *
 * @Block(
 *  id = "class_selector",
 *  admin_label = @Translation("Class selector"),
 * )
 */
class ClassSelector extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current class service.
   *
   * @var \Drupal\skilling\SkillingClass\SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new ClassSelector object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\Utilities $skillingUtilitiesIn
   *   The Skilling utilities service
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManagerIn
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClassIn
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              Utilities $skillingUtilitiesIn,
                              EntityTypeManagerInterface $entityTypeManagerIn,
                              SkillingCurrentClass $currentClassIn) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingUtilities = $skillingUtilitiesIn;
    $this->entityTypeManager = $entityTypeManagerIn;
    $this->currentClass = $currentClassIn;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.utilities'),
      $container->get('entity_type.manager'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Make content to let user switch current class.
    $build = [];
    $classes = $this->currentClass->getCurrentClasses();
    // Only makes sense when there is more than one class.
    if (count($classes) >= 2) {
      // Extract data Twig template needs.
      $renderData = [];
      foreach ($classes as $class) {
        $renderData[] = [
          'title' => $class->getTitle(),
          'nid' => $class->id(),
        ];
      }
      $returnPath = $this->skillingUtilities->getCurrentUrlPath();
      // Render array has a cache tag.
      $build['class_selector'] = [
        '#theme' => 'class_selector',
        '#class_list' => $renderData,
        '#current_class_nid' => $this->currentClass->getId(),
        '#return_url' => $returnPath,
        '#cache' => [
          'tags' => [SkillingConstants::BLOCK_CURRENT_CLASS_CACHE_TAG],
          'contexts' => ['user'],
        ],
      ];
    }
    return $build;
  }

}
