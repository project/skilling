<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Timeline;
use Drupal\skilling\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a timeline block.
 *
 * @Block(
 *  id = "timeline_block",
 *  admin_label = @Translation("Timeline block"),
 * )
 */
class TimelineBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Timeline service.
   *
   * @var \Drupal\skilling\Timeline
   */
  protected $timelineService;

  /** @var \Drupal\skilling\Utilities */
  protected $utilities;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $userFactory;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new CalendarBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\skilling\Timeline $timelineService
   *   Timeline service.
   * @param \Drupal\skilling\Utilities $utilities
   * @param SkillingCurrentClass $current_class
   * @param \Drupal\skilling\SkillingUserFactory $userFactory
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              Renderer $renderer,
                              Timeline $timelineService,
                              Utilities $utilities,
                              SkillingCurrentClass $current_class,
                              SkillingUserFactory $userFactory,
                              SkillingCurrentUser $skillingCurrentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
    $this->timelineService = $timelineService;
    $this->utilities = $utilities;
    $this->currentClass = $current_class;
    $this->userFactory = $userFactory;
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('skilling.timeline'),
      $container->get('skilling.utilities'),
      $container->get('skilling.current_class'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('skilling.skilling_current_user')
    );
  }

  /**
   * Only show for students.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param bool $return_as_object
   *
   * @return bool|\Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultInterface
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    if ($account->isAnonymous()) {
      return AccessResult::forbidden();
    }
    // Hide block if not a current class.
    if ($this->currentClass->isCurrentClass()) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function build() {
    $moduleUrl = $this->utilities->getModuleUrlPath();
    $challengeImageUrl = $moduleUrl . 'images/challenge.png';
    list($problemMessage, $weeksForTemplate) = $this->timelineService->makeTimeline();
    $renderable = [
      '#markup' => '<div id="skilling-timeline-weeks-container"></div>',
      '#attached' => [
        'library' => ['skilling/timeline'],
        'drupalSettings' => [
          'weeks' => json_encode($weeksForTemplate),
          'challengeImageUrl' => $challengeImageUrl,
          'problemMessage' => $problemMessage,
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    return $renderable;
  }

}
