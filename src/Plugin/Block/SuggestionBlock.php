<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Provides a 'SuggestionBlock' block.
 *
 * @Block(
 *  id = "suggestion_block",
 *  admin_label = @Translation("Suggestion"),
 * )
 */
class SuggestionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Constructs a new SuggestionBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   The current user service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SkillingCurrentUser $skilling_current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingCurrentUser = $skilling_current_user;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.skilling_current_user')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['suggestion_block_trigger'] =
      [
        '#theme' => 'suggestion',
      ];
    return $build;
  }

}
