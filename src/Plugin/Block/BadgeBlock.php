<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\skilling\Badging;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a timeline block.
 *
 * @Block(
 *  id = "badges",
 *  admin_label = @Translation("Badges"),
 * )
 */
class BadgeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $userFactory;

  /** The badging service.
   *
   * @var \Drupal\skilling\Badging
   */
  protected $badgingService;

  /**
   * @var \Drupal\skilling\SkillingUser
   */
//  protected $user = FALSE;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Constructs a new BadgeBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\skilling\SkillingUserFactory $userFactory
   * @param \Drupal\skilling\Badging $badgingService
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              Renderer $renderer,
                              SkillingUserFactory $userFactory,
                              Badging $badgingService,
                              SkillingCurrentUser $skillingCurrentUser
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
    $this->userFactory = $userFactory;
    $this->badgingService = $badgingService;
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('skilling.badging'),
      $container->get('skilling.skilling_current_user')
    );
  }


  public function access(AccountInterface $account, $return_as_object = FALSE) {
    if ($account->isAnonymous()) {
      return AccessResult::forbidden();
    }
    // Hide block if there are no badges.
    $publishedBadges = $this->badgingService->getPublishedBadges();
    if (count($publishedBadges) === 0) {
      return AccessResult::forbidden();
    }
    $user = $this->userFactory->makeSkillingUser($account->id());
    if (!$return_as_object) {
      return $user->isStudent();
    }
    if ($user->isStudent()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function build() {
    $badges = $this->skillingCurrentUser->getBadges();
    $badgeData = [];
    foreach ($badges as $badge) {
      $fid = $badge->field_badge_image->target_id;
      $file = File::load($fid);
      $imageUrl = $file->url();
      $url = \Drupal\Core\Url::fromRoute('entity.node.canonical',
        ['node' => $badge->id()]);
      $badgeUrl = $url->toString();
      $badgeData[] = [
        'title' => $badge->getTitle(),
        'imageUrl' => $imageUrl,
        'badgeUrl' => $badgeUrl,
      ];
    }

    $renderable = [
      '#theme' => 'badges',
      '#badges' => $badgeData,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    return $renderable;
  }

}
