<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\book\BookManager;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\block\Entity\Block;

/**
 * Provides a 'SkillingBookNavBlock' block.
 *
 * @Block(
 *  id = "skilling_book_nav_block",
 *  admin_label = @Translation("Skilling book navigation"),
 * )
 */
class SkillingBookNavBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManager
   */
  protected $bookManager;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var ConfigFactory $configFactory */
  protected $configFactory;

  /**
   * @var SkillingUtilities
   */
  protected $skillingUtilities;

  /**
   * The alias manager service.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $aliasManager;



  /** @var [] Titles of defined books, keyed by bid. */
  protected $definedBooks;

  /**
   * Bids of books already shown by Skilling book nav blocks.
   *
   * @var array
   */
  protected $booksAlreadyShown = [];

  /**
   * Whether to cause config save to fail.
   *
   * @var bool
   */
  protected $disallowSave = FALSE;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    BookManager $book_manager,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactory $configFactory,
    SkillingUtilities $skillingUtilities,
    AliasManager $aliasManager
) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->bookManager = $book_manager;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->skillingUtilities = $skillingUtilities;
    $this->aliasManager = $aliasManager;
    $books = $this->bookManager->getAllBooks();
    foreach ($books as $book) {
      $this->definedBooks[$book['bid']] = $book['title'];
    }
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('book.manager'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('skilling.utilities'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $current = isset($this->configuration['book_to_show'])
        ? $this->configuration['book_to_show']
        : NULL;
    return [
            'book_to_show' => $current,
          ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $optionsToShow = $this->definedBooks;
    if (count($optionsToShow) === 0) {
      $form['book_to_show'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<strong>No books available.</strong>'),
        '#weight' => '10',
      ];
      $this->disallowSave = TRUE;
    }
    else {
      $form['book_to_show'] = [
        '#type' => 'radios',
        '#title' => $this->t('Book to show'),
        '#description' => $this->t('Which book will be shown in this block?'),
        '#options' => $optionsToShow,
        '#required' => TRUE,
        '#default_value' => $this->configuration['book_to_show'],
        '#weight' => '10',
      ];
    }
    return $form;
  }


  public function blockValidate($form, FormStateInterface $form_state) {
    if ($this->disallowSave) {
      $form_state
        ->setErrorByName(
          'book_to_show',
          $this->t('Cannot save. All books already have navigation blocks.')
        );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['book_to_show'] = $form_state->getValue('book_to_show');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    // What book to show?
    $bid = $this->configuration['book_to_show'];
    // Is it real?
    if (!$bid || !array_key_exists($bid, $this->definedBooks)) {
      $build['book_not_found'] = [
        '#theme' => 'error_report',
        '#error_report' => $this->t(
          'Book nav: bad bid: @bid',
          ['@bid' => $bid]
        ),
      ];
      return $build;
    }
    // Get cache tags for the config settings.
    $settings = $this->configFactory->get('skilling.settings');
//    $cacheTags = $settings->getCacheTags();
    // Make an HTML container that includes the $bid in its HTML id.
    $prefix = new FormattableMarkup(
      '<div id="skilling-book-nav-:bid" data-skilling-bid=":bid">',
      [':bid' => $bid]
    );
    $bookTree = $this->getBookTree($bid);
    $bookNode = $this->entityTypeManager->getStorage('node')
      ->load($bid);
    if (!$bookNode) {
      return $build;
    }
    $build['book_nav_block_' . $bid] = [
      '#theme' => 'skilling_book_tree',
      '#key' => $bookTree['key'],
      '#alias' => $bookTree['alias'],
      '#title' => $bookTree['title'],
      '#children' => $bookTree['children'],
//      '#theme' => 'throbber',
//      // The id skilling-book-nav-X is used in render-book-tree-ajax-command.js.
      '#prefix' => (string) $prefix,
      '#suffix' => '</div>',
      '#attached' => [
        'library' => ['skilling/book-tree'],
      ],
      '#cache' => [
        'tags' => $bookNode->getEntityType()->getListCacheTags(),
//        'tags' => $cacheTags,
      ],
    ];
    return $build;
  }

//'tags' => $node->getEntityType()->getListCacheTags()

  public function getBookTree($bid) {
    $bookTree = $this->bookManager->bookTreeAllData($bid);
    // Make a nested array.
    $bookTreeExtract = $this->makeBookTreeExtract(current($bookTree));
    return $bookTreeExtract;
  }

  protected function makeBookTreeExtract(array $subtreeRoot) {
    $nid = $subtreeRoot['link']['nid'];
    $alias = $this->aliasManager->getAliasByPath('/node/' . $nid);
    $alias = $this->skillingUtilities->removeSlashFromStart($alias);
    $alias = $this->skillingUtilities->getSiteBaseUrl() . $alias;
    $extract = [
      'key' => $subtreeRoot['link']['nid'],
      'alias' => $alias,
      'title' => $subtreeRoot['link']['title'],
      'children' => [],
    ];
    if (isset($subtreeRoot['below'])) {
      foreach ($subtreeRoot['below'] as $child) {
        $extract['children'][] = $this->makeBookTreeExtract($child);
      }
    }
    return $extract;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Make cacheable in https://www.drupal.org/node/2483181
   */
//  public function getCacheMaxAge() {
//    return 0;
//  }

}
