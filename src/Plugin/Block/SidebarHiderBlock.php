<?php

namespace Drupal\skilling\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;

/**
 * Provides a 'SidebarHiderBlock' block.
 *
 * @Block(
 *  id = "sidebar_hider_block",
 *  admin_label = @Translation("Sidebar hider block"),
 * )
 */
class SidebarHiderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new SidebarHiderBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   The current user service.
   * @param SkillingCurrentClass $currentClass
   *   The current class service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SkillingCurrentUser $skillingCurrentUser,
    SkillingCurrentClass $currentClass
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->currentClass = $currentClass;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'left_sidebar_css_id' => 'left-sidebar',
      'right_sidebar_css_id' => 'right-sidebar',
      'hiding_class' => 'd-none',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['left_sidebar_css_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Left sidebar CSS id'),
      '#description' => $this->t('CSS id of the left sidebar.'),
      '#default_value' => $this->configuration['left_sidebar_css_id'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '40',
    ];
    $form['right_sidebar_css_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Right sidebar CSS id'),
      '#description' => $this->t('CSS id of the right sidebar.'),
      '#default_value' => $this->configuration['right_sidebar_css_id'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '80',
    ];
    $form['hiding_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hiding class'),
      '#description' => $this->t('CSS class that hides a sidebar.'),
      '#default_value' => $this->configuration['hiding_class'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '100',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['left_sidebar_css_id'] = $form_state->getValue('left_sidebar_css_id');
    $this->configuration['right_sidebar_css_id'] = $form_state->getValue('right_sidebar_css_id');
    $this->configuration['hiding_class'] = $form_state->getValue('hiding_class');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $leftSidebarId = $this->configuration['left_sidebar_css_id'];
    $rightSidebarId = $this->configuration['right_sidebar_css_id'];
    $hidingClass = $this->configuration['hiding_class'];
    $allowedTimeline = $this->currentClass->isCurrentClass();
    $build['sidebar_hider'] = [
      '#theme' => 'sidebar_hider',
      '#left_sidebar_id' => $leftSidebarId,
      '#right_sidebar_id' => $rightSidebarId,
      // User is allowed to see a calendar only if there's a current class.
      '#allowed_timeline' => $allowedTimeline,
      '#attached' => [
        'library' => 'skilling/sidebar-hider',
        'drupalSettings' => [
          'leftSideBarId' => $leftSidebarId,
          'rightSideBarId' => $rightSidebarId,
          'hidingClass' => $hidingClass,
          'allowedTimeline' => $allowedTimeline,
        ],
      ],
    ];
    return $build;
  }

}
