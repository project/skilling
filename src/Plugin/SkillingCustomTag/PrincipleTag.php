<?php
/**
 * Principle custom tag plugin.
 *
 * User: mathieso
 * Date: 6/26/18
 * Time: 12:38 PM
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a principle custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_principle_custom_tag",
 *  tag = "principle",
 *  admin_label = @Translation("Principle"),
 *  description = @Translation("Inserts a principle into a lesson."),
 *  has_close_tag = false
 * )
 */
class PrincipleTag extends SkillingCustomTagBase {

  /** @var EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var SkillingUtilities  */
  protected $skillingUtilities;

  /** @var SkillingCurrentUser */
  protected $currentUser;

  /** @var NidBag */
  protected $nidBag;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
  }

  /**
   * Process content, with options.
   *
   * @param string $content Content to process.
   * @param array $options Options.
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    //Nothing for blocked user.
    if ( ! $this->currentUser->isAnonymous() ) {
      if ( $this->currentUser->isBlocked() ) {
        return '';
      }
    }
    //Find the name of the principle.
    //Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ( $internalName === '' ) {
      return $this->formatCustomTagError(
        t('Principle tag needs internal_name option.')
      );
    }
    //Find the principle id.
    $principleIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      //Is a principle.
      ->condition('type', 'principle')
      //Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if ( ! $principleIds ) {
      return $this->formatCustomTagError(
        t('Principle with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if ( sizeof($principleIds) !== 1 ) {
      return $this->formatCustomTagError(
        t(
          "Unexpected number of principles with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    // Load the principle.
    $principleId = current($principleIds);
    /** @var Node $principle */
    $principle = $this->entityTypeManager->getStorage('node')->load($principleId);
    // Should not happen.
    if (!$principle) {
      return $this->formatCustomTagError(
        t("Principle with id @nid not found.", ['@nid' => $principleId])
      );
    }
    // If not published, return nothing.
    if (!$principle->isPublished()) {
      return '';
    }
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_PRINCIPLES, $principleId);
    // Get body as shown in teaser.
    $summary = $principle->body->view('teaser')[0]['#text'];
    $summary = $this->parser->parse($summary);
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      '#theme' => 'insert_principle',
      '#title' => $principle->getTitle(),
      '#summary' => $summary,
      '#principle_nid' => $principleId,
      '#internal_name' => $principle->field_internal_name->value,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
