<?php
/**
 * Comment custom tag plugin.
 *
 * Add comments or ignored content.
 *
 * User: mathieso
 * Date: 3/16/20
 * Time: 2:24 PM
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides a comment tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_comment_custom_tag",
 *  tag = "comment",
 *  admin_label = @Translation("Comment"),
 *  description = @Translation("Comment that is not rendered."),
 *  has_close_tag = true
 * )
 */
class CommentTag extends SkillingCustomTagBase {

  use StringTranslationTrait;


  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node containing this annotation.
   *
   * @return string
   *   Rendered content.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Does nothing.
    return '';
  }

}
