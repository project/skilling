<?php
/**
 * Model custom tag plugin.
 *
 * User: mathieso
 * Date: 6/26/18
 * Time: 12:38 PM
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a model custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_model_custom_tag",
 *  tag = "model",
 *  admin_label = @Translation("Model"),
 *  description = @Translation("Inserts a model into a page."),
 *  has_close_tag = false
 * )
 */
class ModelTag extends SkillingCustomTagBase {

  /** @var EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var SkillingUtilities  */
  protected $skillingUtilities;

  /** @var SkillingCurrentUser */
  protected $currentUser;

  /** @var NidBag */
  protected $nidBag;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
  }

  /**
   * Process content, with options.
   *
   * @param string $content Content to process.
   * @param array $options Options.
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    //Nothing for blocked user.
    if ( ! $this->currentUser->isAnonymous() ) {
      if ( $this->currentUser->isBlocked() ) {
        return '';
      }
    }
    //Find the name of the model.
    //Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ( $internalName === '' ) {
      return $this->formatCustomTagError(
        t('Model tag needs internal_name option.')
      );
    }
    //Find the model id.
    $modelIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      //Is a model.
      ->condition('type', 'model')
      //Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if ( ! $modelIds ) {
      return $this->formatCustomTagError(
        t('Model with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if ( sizeof($modelIds) !== 1 ) {
      return $this->formatCustomTagError(
        t(
          "Unexpected number of models with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    //Load the model.
    $modelId = current($modelIds);
    /** @var Node $model */
    $model = $this->entityTypeManager->getStorage('node')->load($modelId);
    //Should not happen.
    if ( ! $model ) {
      return $this->formatCustomTagError(
        t("Model with id @nid not found.", ['@nid' => $modelId])
      );
    }
    //If not published, return nothing.
    if ( ! $model->isPublished() ) {
      return '';
    }
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_MODELS, $modelId);
    // Get body as shown in teaser.
    $summary = $model->body->view('teaser')[0]['#text'];
    $summary = $this->parser->parse($summary);
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      '#theme' => 'insert_model',
      '#title' => $model->getTitle(),
      '#summary' => $summary,
      '#model_nid' => $modelId,
      '#internal_name' => $model->field_internal_name->value,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
