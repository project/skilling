<?php
namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a fill-in-the-blank custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_fib_custom_tag",
 *  tag = "fib",
 *  admin_label = @Translation("Fill-in-the-blank"),
 *  description = @Translation("Inserts a fill-in-the-blank question into a lesson."),
 *  has_close_tag = false
 * )
 */
class FibTag extends SkillingCustomTagBase {

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * A service for tracking node ids.
   *
   * Helps track nodes inserted in lessons by the parser. Used to
   * update entity reference field on inserted nodes.
   *
   * @var \Drupal\skilling\NidBag
   */
  protected $nidBag;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
    $this->session = \Drupal::service('session');
  }

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node with the tag.
   *
   * @return string
   *   Rendered result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Find the internal name of the question.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        t('Fill-in-the-blank tag needs internal_name option.')
      );
    }
    // Find the fib question id.
    $fillInTheBlankIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is a FIB.
      ->condition('type', SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$fillInTheBlankIds) {
      return $this->formatCustomTagError(
        t('Fill-in-the-blank with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if (count($fillInTheBlankIds) !== 1) {
      return $this->formatCustomTagError(
        t(
          "Unexpected number of fill-in-the-blanks with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    // Load the question.
    $fillInTheBlankId = current($fillInTheBlankIds);
    /* @var Node $fillInTheBlank */
    $fillInTheBlank = $this->entityTypeManager->getStorage('node')->load($fillInTheBlankId);
    // Should not happen.
    if (!$fillInTheBlank) {
      return $this->formatCustomTagError(
        t("Fill-in-the-blank with id @nid not found.", ['@nid' => $fillInTheBlankId])
      );
    }
    // If not published, return nothing.
    if (!$fillInTheBlank->isPublished()) {
      return '';
    }
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_FILL_IN_THE_BLANKS, $fillInTheBlankId);
    // Extract fields.
    $title = $fillInTheBlank->getTitle();
    $question = $this->parser->parse($fillInTheBlank->get('field_question')->value);
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      // Template to use.
      '#theme' => 'insert_fib',
      '#title' => $title,
      '#fib_nid' => $fillInTheBlankId,
      '#internal_name' => $internalName,
      '#question' => $question,
      '#attached' => [
        'library' => 'skilling/fib',
        'drupalSettings' => [
          'csrfToken' => $this->ajaxSecurityService->getCsrfToken(),
          'sessionId' => $this->session->getId(),
        ],
      ],
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
