<?php

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a character custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_character_custom_tag",
 *  tag = "character",
 *  admin_label = @Translation("Character"),
 *  description = @Translation("Inserts a character into a lesson."),
 *  has_close_tag = true
 * )
 */
class CharacterTag extends SkillingCustomTagBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var SkillingUtilities  */
  protected $skillingUtilities;

  /** @var ConfigFactory $configFactory */
  protected $configFactory;

  /**
   * A service for tracking node ids.
   *
   * Helps track nodes inserted in lessons by the parser. Used to
   * update entity reference field on inserted nodes.
   *
   * @var \Drupal\skilling\NidBag
   */
  protected $nidBag;

  /**
   * CharacterTag constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
    $this->configFactory = \Drupal::service('config.factory');
  }

  /**
   * Process the tag.
   *
   * @param string $content
   *   Tag content.
   * @param array $options
   *   Tag options.
   *
   * @return string
   *   Rendered result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Find the name of the character.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        $this->t('Character tag needs internal_name option.')
      );
    }
    // Find the character id from the internal name.
    $characterIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is a character.
      ->condition('type', SkillingConstants::CHARACTER_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$characterIds) {
      return $this->formatCustomTagError(
        $this->t('Character with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if (count($characterIds) !== 1) {
      return $this->formatCustomTagError(
        $this->t(
          "Unexpected number of characters with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    // Load the character.
    $characterId = reset($characterIds);
    /** @var \Drupal\node\Entity\Node $character */
    $character = $this->entityTypeManager
      ->getStorage('node')
      ->load($characterId);
    // Should not happen.
    if (!$character) {
      return $this->formatCustomTagError(
        $this->t("Character with id @nid not found.", ['@nid' => $characterId])
      );
    }
    // If not published, return nothing.
    if (!$character->isPublished()) {
      return '';
    }
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_CHARACTERS, $characterId);
    // Compute the photo's URL, depending on configured image style.
    $photoUrl = '';
    $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
    $imageStyle = $settings->get(SkillingConstants::SETTING_KEY_CHARACTER_IMAGE_STYLE);
    $imageField = $character->get('field_photo')->getValue();
    if (!empty($imageField[0]['target_id'])) {
      $file = File::load($imageField[0]['target_id']);
      if (!$imageStyle || $imageStyle === 'original') {
        // Use the file as stored.
        $photoUrl = file_create_url($file->getFileUri());
      }
      else {
        // Use the image with an image style applied.
        $photoUrl = ImageStyle::load($imageStyle)->buildUrl($file->getFileUri());
      }
    }
    $altText = $imageField[0]['alt'];
    $renderable = [
      // Template to use.
      '#theme' => 'insert_character',
      '#caption' => $character->get('field_caption')->value,
      '#image_path' => $photoUrl,
      '#alt' => $altText,
      '#content' => $content,
      '#internal_name' => $character->get(SkillingConstants::FIELD_INTERNAL_NAME)->value,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
