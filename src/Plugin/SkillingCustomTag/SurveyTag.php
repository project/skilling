<?php
namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\webform\Entity\Webform;
//use Drupal\webform\WebformRequest;
use Drupal\skilling\Utilities as SkillingUtilities;

/**
 * Provides a survey custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_survey_custom_tag",
 *  tag = "survey",
 *  admin_label = @Translation("Survey"),
 *  description = @Translation("Shows a link that opens a survey."),
 *  has_close_tag = true
 * )
 */
class SurveyTag extends SkillingCustomTagBase {

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   Node containing the tag.
   *
   * @return string
   *   Rendered survey.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    /** @var \Drupal\skilling\Utilities $skillingUtilities */
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
    /** @var \Drupal\Core\Render\Renderer $renderer */
    $this->renderer = \Drupal::service('renderer');
    $internalName = $this->findInternalNameOption($options);
    // Internal name option is required.
    if ($internalName === '') {
      return $this->formatCustomTagError(
        t('Survey tag needs internal_name option.')
      );
    }
    $webform = Webform::load($internalName);
    if (!$webform) {
      return $this->formatCustomTagError(
        t(
          'Survey tag internal_name not found: @n',
          ['@n' => $internalName]
        )
      );
    }
    // Get the title, if there is one.
    $title = $this->findOption($options, ['title']);
    // Description.
    $description = $content;
    // Only show survey tags for students.
    if (!$this->currentUser->isStudent()) {
      $renderable = [
        '#theme' => 'non_student_survey_message',
        '#internal_name' => $internalName,
        '#title' => $title,
        '#description' => $description,
      ];
      $rendered = $this->renderer->render($renderable);
      return $rendered;
    }
    // Is the survey available?
    if (!$webform->status()) {
      return '';
    }
    // URL of the webform.
    $link = $webform->url();
    // Todo: what if Drupal is installed in a subdirectory?
    $currentPath = \Drupal::service('path.current')->getPath();
    // Add a page refresh to the Webform URL.
    $link .= '?destination=' . $currentPath;
    // Has the user completed the survey?
    $completed = $this->skillingUtilities->isUserCompletedSurvey(
      $this->currentUser->id(), $internalName
    );
    // Is the survey required?
    $required = FALSE;
    $requiredOption = $this->findOption(
      $options,
      ['required', 'require']
    );
    if ($requiredOption) {
      $required = TRUE;
    }
    // Is the survey repeatable?
    $repeatable = FALSE;
    $repeatableOption = $this->findOption(
      $options,
      ['repeatable', 'repeat', 'repeats']
    );
    if ($repeatableOption) {
      $repeatable = TRUE;
    }
    // Compute instructions, and whether to show the link.
    $showLink = TRUE;
    if ($completed) {
      if ($repeatable) {
        // Completed, and repeatable.
        $instructions = t(
          'Thanks for completing the survey.
           You can do it again, if you want.'
        );
      }
      else {
        // Completed, not repeatable.
        $showLink = FALSE;
        $instructions = t('Thanks for completing the survey.');
      }
    }
    else {
      // Not completed.
      if ( $required ) {
        // Not completed, required.
        $instructions = t('Please complete this required survey.');
      }
      else {
        // Not completed, not required.
        $instructions = t('Please complete this survey.');
      }
    }
    $renderable = [
      '#theme' => 'survey_link',
      '#internal_name' => $internalName,
      '#title' => $title,
      '#description' => $description,
      '#instructions' => $instructions,
      '#show_link' => $showLink,
      '#link' => $link,
      '#completed' => $completed,
      '#required' => $required,
      '#repeatable' => $repeatable,
//      '#attached' => [
//        'library' => 'skilling/exercise-links',
//        'drupalSettings' => [
//          'skillingFloatQueryParam' => SkillingConstants::FLOATER_QUERY_STRING,
//        ],
//      ],
    ];
    $rendered = $this->renderer->render($renderable);
    return $rendered;
  }
}
