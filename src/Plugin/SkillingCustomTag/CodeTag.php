<?php

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\settings_tray_test\Plugin\Block\SettingsTrayFormAnnotationIsFalseBlock;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\Utilities;

/**
 * Provides a code tag.
 *
 * NB! The code tag has special treatment in the parser.
 * Check trimWhitespaceAndComments().
 *
 * @SkillingCustomTag(
 *  id = "skilling_code_custom_tag",
 *  tag = "code",
 *  admin_label = @Translation("Code"),
 *  description = @Translation("Shows source code to the reader."),
 *  has_close_tag = true
 * )
 */
class CodeTag extends SkillingCustomTagBase {

  use StringTranslationTrait;

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\NodeInterface $nodeWithTag
   *   The node with the tag.
   *
   * @return string
   *   Rendered content.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    $showLineNumbers = FALSE;
    $startNumber = 1;
    // Show line numbers?
    $lineNumbersOption = $this->findOption(
      $options, SkillingConstants::LINE_NUMBERS_ALTS
    );
    if (in_array(strtolower($lineNumbersOption), SkillingConstants::TRUE_ALTS)) {
      $showLineNumbers = TRUE;
    }
    // Start number set?
    $startNumberOption = $this->findOption(
      $options, SkillingConstants::START_ALTS
    );
    if ($startNumberOption && is_numeric($startNumberOption)) {
      // If there is a start number, that implies that line numbers are to be shown.
      $showLineNumbers = TRUE;
      $startNumber = (int) $startNumberOption;
    }
    /** @var \Drupal\skilling\Utilities $utilities */
    $utilities = $this->getUtilities();
    $content = $this->htmlEncode($content);
    $content = $utilities->makeLeadingSpacesNonbreaking($content);
    //Replace highlight markers with highlighting spans.
    $content = str_replace('>>>>', '<span class="skilling-code-emph3">', $content);
    $content = str_replace('<<<<', '</span>', $content);
    $content = str_replace('>>>', '<span class="skilling-code-emph2">', $content);
    $content = str_replace('<<<', '</span>', $content);
    $content = str_replace('>>', '<span class="skilling-code-emph1">', $content);
    $content = str_replace('<<', '</span>', $content);
    // Break content to show into individual lines
    $contentLines = explode("\n", $content);
    // Remove MT elements at front and end of array.
    $contentLines = $utilities->removeLeadingEmptyLines($contentLines);
    $contentLines = $utilities->removeTrailingEmptyLines($contentLines);
    $contentLines = $utilities->addNbspToEmptyLines($contentLines);
    $renderable = [
      '#theme' => 'code',
//      '#content' => $content,
      '#content_lines' => $contentLines,
      '#line_numbers' => $showLineNumbers,
      '#start_number' => $startNumber,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }


  protected function htmlEncode($content) {
    // Replace highlight markers with content that won't be broken by entity encoding.
    $content = str_replace('>>>>', '88dog88open', $content);
    $content = str_replace('<<<<', '88dog88close', $content);
    $content = str_replace('>>>', '77llama77open', $content);
    $content = str_replace('<<<', '77llama77close', $content);
    $content = str_replace('>>', '66goat66open', $content);
    $content = str_replace('<<', '66goat66close', $content);
    // Encode.
    $content = htmlentities($content);
    // Restore the highlight markers.
    $content = str_replace('66goat66open', '>>', $content);
    $content = str_replace('66goat66close', '<<', $content);
    $content = str_replace('77llama77open', '>>>', $content);
    $content = str_replace('77llama77close', '<<<', $content);
    $content = str_replace('88dog88open', '>>>>', $content);
    $content = str_replace('88dog88close', '<<<<', $content);
    return $content;
  }

}
