<?php
namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a fill-in-the-blank custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_mcq_custom_tag",
 *  tag = "mcq",
 *  admin_label = @Translation("Multiple-choice"),
 *  description = @Translation("Inserts a multiple-choice question into a lesson."),
 *  has_close_tag = false
 * )
 */
class McqTag extends SkillingCustomTagBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling current user service.
   *
   * Provides data about the current user, e.g., roles.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * A service for tracking node ids.
   *
   * Helps track nodes inserted in lessons by the parser. Used to
   * update entity reference field on inserted nodes.
   *
   * @var \Drupal\skilling\NidBag
   */
  protected $nidBag;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
    $this->session = \Drupal::service('session');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
  }

  /**
   * Process content, with options.
   *
   * @param string $content Content to process.
   * @param array $options Options.
   *
   * @return mixed
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Find the internal name of the question.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        t('MCQ tag needs internal_name option.')
      );
    }
    // Find the MCQ question id.
    $mcqIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is an MCQ.
      ->condition('type', SkillingConstants::MCQ_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$mcqIds) {
      return $this->formatCustomTagError(
        t('MCQ with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if (count($mcqIds) !== 1) {
      return $this->formatCustomTagError(
        t(
          "Unexpected number of MCQs with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    // Load the question.
    $mcqId = reset($mcqIds);
    /** @var Node $mcq */
    $mcq = $this->entityTypeManager->getStorage('node')->load($mcqId);
    // Should not happen.
    if (!$mcq) {
      return $this->formatCustomTagError(
        t("MCQ with id @nid not found.", ['@nid' => $mcqId])
      );
    }
    // If not published, return nothing.
    if (!$mcq->isPublished()) {
      return '';
    }
    //Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_MCQS, $mcqId);
    $responses = [];
    // Responses are A, B, C, etc.
    $charCodeNextItem = 'A';
    $responseIndex = 0;
    foreach ($mcq->field_responses as $responseRef) {
      /** @var \Drupal\paragraphs\Entity\Paragraph $responsePara */
      $responsePara = $responseRef->entity;
//      $correct = $responsePara->field_correct->value;
      $responseText = $this->parser->parse($responsePara->field_response->value);
//      $explanation = $this->parser->parse($responsePara->field_explanation->value);
      $responses[] = [
        'label' => $charCodeNextItem,
        'index' => $responseIndex,
        'response' => $responseText,
//        'explanation' => $explanation,
      ];
      $charCodeNextItem++;
      $responseIndex++;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      '#theme' => 'insert_mcq',
      '#title' => $mcq->getTitle(),
      '#mcq_nid' => $mcqId,
      '#internal_name' => $internalName,
      '#question' => $this->parser->parse($mcq->get('field_question')->value),
      '#responses' => $responses,
      '#attached' => [
        'library' => 'skilling/mcq',
        'drupalSettings' => [
          'csrfToken' => $this->ajaxSecurityService->getCsrfToken(),
          'sessionId' => $this->session->getId(),
        ],
      ],
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
