<?php

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides a container custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_container_custom_tag",
 *  tag = "container",
 *  admin_label = @Translation("Container"),
 *  description = @Translation("Container for other content."),
 *  has_close_tag = true
 * )
 */
class ContainerTag extends SkillingCustomTagBase {

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   Array with the tag.
   *
   * @return string
   *   HTML.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
//    // Is it floatable?
//    $floatable = $this->findOption(
//      $options, [SkillingConstants::FLOATABLE]
//    );

    // Extra spaces before HTML tags are needed so that Textile does not wrap
    // them in p tags.
    $result =
      "\n\n <div class='skilling-container'>\n 
        $content\n
      </div>\n";
    return $result;
  }

}
