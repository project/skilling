<?php

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides a reflect custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_reflect_custom_tag",
 *  tag = "reflect",
 *  admin_label = @Translation("Reflect"),
 *  description = @Translation("A reflect to reflect."),
 *  has_close_tag = true
 * )
 */
class ReflectTag extends SkillingCustomTagBase {

  use StringTranslationTrait;

  /**
   * Tags defined, to check for duplicates.
   *
   * @var array
   */
  private static $reflectTagNames = [];

  /**
   * Default title.
   *
   * @var string
   */
  const DEFAULT_TITLE = 'Reflect';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * Provides data about the current user, e.g., roles.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * The CSRF token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfTokenGeneratorService;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $pluginId
   *   Plugin id.
   * @param mixed $pluginDefinition
   *   Plugin definition.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
    $this->session = \Drupal::service('session');
    $this->csrfTokenGeneratorService = \Drupal::service('csrf_token');
  }

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node containing the tag. Could be inserted into another node.
   *
   * @return string
   *   Rendered content.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Is there a node this tag is in?
    if (!$nodeWithTag || !$nodeWithTag->id()) {
      return '';
      // Using the error tag shows errors in taxonomy (and search?) context,
      // when there is is no lesson summary.
//      return $this->formatCustomTagError(
//        $this->t('Cannot render reflect tag outside node context.')
//      );
    }
    $title = $this->findOption(
      $options, SkillingConstants::TITLE_ALTS
    );
    if (!$title) {
      $title = $this->t(self::DEFAULT_TITLE);
    }
    // Find the internal name of the reflect tag.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        $this->t('Reflect tag needs internal_name option.')
      );
    }
    // Is there already a tag with this internal name?
    $alreadyExists = in_array($internalName, self::$reflectTagNames);
    if ($alreadyExists) {
      return $this->formatCustomTagError(
        $this->t(
          'Duplicate reflect tag with internal_name @name.',
          ['@name' => $internalName]
        )
      );
    }
    // Remember that this internal name is in use.
    self::$reflectTagNames[] = $internalName;
    // Load the note, if there is one.
    $note = '';
    $noteIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is a reflect note.
      ->condition('type', SkillingConstants::REFLECT_NOTE_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      // For the current user.
      ->condition('uid', $this->currentUser->id())
      // For the current node.
      ->condition('field_node.target_id', $nodeWithTag->id())
      ->execute();
    if (count($noteIds) > 0) {
      // Load the exercise.
      $noteId = reset($noteIds);
      /** @var \Drupal\node\Entity\Node $noteNode */
      $noteNode = $this->entityTypeManager->getStorage('node')
        ->load($noteId);
      // Should not happen.
      if (!$noteNode) {
        return $this->formatCustomTagError(
          t("Note with id @nid not found.", ['@nid' => $noteId])
        );
      }
      /* @noinspection PhpUndefinedFieldInspection */
      $note = trim($noteNode->field_note->value);
    }
    $renderable = [
      '#theme' => 'insert-reflect',
      '#title' => $title,
      '#nid' => $nodeWithTag->id(),
      '#internal_name' => $internalName,
      '#content' => $content,
      '#note' => $note,
      '#user_is_student' => $this->currentUser->isStudent(),
      '#attached' => [
        'library' => 'skilling/reflect-note',
        'drupalSettings' => [
          'csrfToken' => $this->ajaxSecurityService->getCsrfToken(),
          'sessionId' => $this->session->getId(),
          'userIsStudent' => $this->currentUser->isStudent(),
        ],
      ],
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
