<?php
/**
 * Pattern custom tag plugin.
 *
 * User: mathieso
 * Date: 6/2/18
 * Time: 2:42 PM
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\skilling\NidBag;

/**
 * Provides a pattern custom tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_pattern_custom_tag",
 *  tag = "pattern",
 *  admin_label = @Translation("Pattern"),
 *  description = @Translation("Inserts a pattern into a lesson."),
 *  has_close_tag = false
 * )
 */
class PatternTag extends SkillingCustomTagBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var SkillingUtilities  */
  protected $skillingUtilities;

  /** @var SkillingCurrentUser */
  protected $currentUser;

  /** @var NidBag */
  protected $nidBag;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->skillingUtilities = \Drupal::service('skilling.utilities');
    $this->nidBag = \Drupal::service('skilling.nid_bag');
    $this->currentUser = \Drupal::service('skilling.skilling_current_user');
  }

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node with the tag.
   *
   * @return mixed
   *   Content
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Nothing for blocked user.
    if (!$this->currentUser->isAnonymous()) {
      if ($this->currentUser->isBlocked()) {
        return '';
      }
    }
    // Find the name of the pattern.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        t('Pattern tag needs internal_name option.')
      );
    }
    // Find the pattern id.
    $patternIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is a pattern.
      ->condition('type', 'pattern')
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$patternIds) {
      return $this->formatCustomTagError(
        t('Pattern with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if (count($patternIds) !== 1) {
      return $this->formatCustomTagError(
        t(
          "Unexpected number of patterns with internal_name @n found.",
          ['@n' => $internalName]
        )
      );
    }
    // Load the pattern.
    $patternId = current($patternIds);
    /** @var Node $pattern */
    $pattern = $this->entityTypeManager->getStorage('node')->load($patternId);
    // Should not happen.
    if (!$pattern) {
      return $this->formatCustomTagError(
        t("Pattern with id @nid not found.", ['@nid' => $patternId])
      );
    }
    // If not published, return nothing.
    if (!$pattern->isPublished()) {
      return '';
    }
    $summary = $pattern->field_summary->value;
    $summary = $this->parser->parse($summary);
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_PATTERNS, $patternId);
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      '#theme' => 'insert_pattern',
      '#title' => $pattern->getTitle(),
      '#summary' => $summary,
      '#pattern_nid' => $patternId,
      '#internal_name' => $pattern->field_internal_name->value,
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
