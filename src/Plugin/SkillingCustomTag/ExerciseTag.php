<?php
namespace Drupal\skilling\Plugin\SkillingCustomTag;

use DateTime;
use Drupal;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingConstants;
use DateInterval;

/**
 * Provides an exercise tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_exercise_custom_tag",
 *  tag = "exercise",
 *  admin_label = @Translation("Exercise"),
 *  description = @Translation("Inserts an exercise into a lesson."),
 *  has_close_tag = false
 * )
 */
class ExerciseTag extends SkillingCustomTagBase {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling current user service.
   *
   * Provides data about the current user, e.g., roles.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * A service for tracking node ids.
   *
   * Helps track nodes inserted in lessons by the parser. Used to
   * update entity reference field on inserted nodes.
   *
   * @var \Drupal\skilling\NidBag
   */
  protected $nidBag;

  /**
   * Submissions service.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;

  /**
   * @var array Track exercise ids that have been used. Can't insert two
   * instances of the same exercise in the same lesson.
   */
  protected static $usedExerciseIds = [];

  /**
   * @var array Internal names of exercises that were to be inserted,
   * but were not found.
   */
  public static $exerciseInternalNamesNotFound = [];

  /**
   * Current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Whether the markup is rendered out of the context of the normal
   * plugin. The code is used by skilling.module as well.
   *
   * @var bool
   */
  protected $isOutOfContext = FALSE;

  /**
   * ExerciseTag constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $pluginId
   *   Plugin id.
   * @param $pluginDefinition
   *   Plugin definition.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    // Class can be used by skilling.module, not in plugin context.
    if ($pluginId === 'out-of-plugin-context') {
      $this->isOutOfContext = TRUE;
    }
    else {
      parent::__construct($configuration, $pluginId, $pluginDefinition);
    }
    $this->entityTypeManager = Drupal::service('entity_type.manager');
    $this->skillingUtilities = Drupal::service('skilling.utilities');
    $this->nidBag = Drupal::service('skilling.nid_bag');
    $this->currentUser = Drupal::service('skilling.skilling_current_user');
    $this->submissionsService = Drupal::service('skilling.submissions');
    $this->currentClass = Drupal::service('skilling.current_class');
    $this->dateFormatter = Drupal::service('date.formatter');
  }


  public function preparse() {
    // Forget exercises inserted do far.
    self::$usedExerciseIds = [];
  }

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   *
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *
   * @return string
   *   Rendered result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Exception
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Find the name of the exercise.
    // Allow for some mistyping of option name by authors.
    $internalName = $this->findOption(
      $options, SkillingConstants::INTERNAL_NAME_ALTS
    );
    if ($internalName === '') {
      return $this->formatCustomTagError(
        $this->t('Exercise tag needs internal_name option.')
      );
    }
    // Find the exercise.
    // This code is slightly different from
    // Utilities::getPublishedExerciseWithInternalName. It does not
    // throw an exception when an exercise is unpublished.
    $exerciseIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is an exercise.
      ->condition('type', SkillingConstants::EXERCISE_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$exerciseIds) {
      self::$exerciseInternalNamesNotFound[] = $internalName;
      return $this->formatCustomTagError(
        $this->t('Exercise with internal_name @n not found.', ['@n' => $internalName])
      );
    }
    if (count($exerciseIds) !== 1) {
      return $this->formatCustomTagError(
        $this->t(
          'Unexpected number of exercises with internal_name @n found.',
          ['@n' => $internalName])
      );
    }
    $exerciseId = reset($exerciseIds);
    // Load the exercise.
    /** @var \Drupal\node\Entity\Node $exercise */
    $exercise = $this->entityTypeManager->getStorage('node')
      ->load($exerciseId);
    // Should not happen.
    if (!$exercise) {
      return $this->formatCustomTagError(
        $this->t('Exercise with id @id not found.', ['@id' => $exerciseId])
      );
    }
    // Check to see that it hasn't been referenced already.
    if (isset(self::$usedExerciseIds[$exerciseId])) {
      return $this->formatCustomTagError(
        $this->t(
          'Sorry, cannot insert the same exercise more than once in a lesson. Internal name: @n.',
          ['@n' => $internalName])
      );
    }
    self::$usedExerciseIds[$exerciseId] = 'used';
    // If not published, return nothing.
    if (!$exercise->isPublished()) {
      return '';
    }
    // Drop the nid into a bag, in case others want it.
    $this->nidBag->addToBag(SkillingConstants::NID_POCKET_EXERCISES, $exerciseId);
    // Get body as shown in exercise's teaser.
    $summary = $exercise->body->summary;
    // Get the full body.
    $task = $exercise->body->value;
    // Check that the parser exists.
    if (!$this->parser) {
      $this->parser = Drupal::service('skilling.skillingparser');
    }
    $task = $this->parser->parse($task);
    $renderableSubmissionLinks = $this->submissionsService->createSubmissionLinks($exercise);
    $showExerciseLink = $this->currentUser->isAdministrator()
      || $this->currentUser->isAuthor()
      || $this->currentUser->isReviewer()
      || $this->currentUser->isInstructor()
      || $this->currentUser->isGrader();
    $moduleUrl = $this->skillingUtilities->getModuleUrlPath();
    $challengeImageUrl = $moduleUrl . 'images/challenge.png';
    // Get data from the class about due date, required, etc.
    // Only makes sense if there is a current class, and the user is a student.
    // Leave vars as null to make template skip showing them at all.
    $isRequiredDisplay = null;
    $exerciseMaxSubsDisplay = null;
    $whenDueDisplay = null;
    $numberOfSubmissions = null;
    $exerciseSubmissionStatus = null;
    if ($this->currentClass->isCurrentClass()) {
      if ($this->currentUser->isStudent()) {
        // Submissions from the user.
        $exerciseSubmissionStatus = $this->submissionsService->getSubmissionStatusForStudentExercise(
          $this->currentUser, $exerciseId, $this->currentClass->getWrappedSkillingClass()
        );
        $numberOfSubmissions = 0;
        if (isset($renderableSubmissionLinks['submissions'])) {
          $numberOfSubmissions = count($renderableSubmissionLinks['submissions']);
        }
      }
      /** @var \Drupal\skilling\ExerciseDueRecord|null $exerciseDue */
      $exerciseDue = $this->currentClass->getExerciseDueRecordForExercise($exerciseId);
      if (is_null($exerciseDue)) {
        // Exercise not in class timeline.
        $whenDueDisplay = -1;
        $isRequiredDisplay = NULL;
        $exerciseMaxSubsDisplay = $this->currentClass->getDefaultMaxSubmissions();
      }
      else {
        // Compute max submissions allowed.
        $exerciseMaxSubs = $exerciseDue->getMaximumSubmissions();
        if (is_null($exerciseMaxSubs)) {
          $exerciseMaxSubs = $this->currentClass->getDefaultMaxSubmissions();
        }
        $exerciseMaxSubsDisplay =
          is_null($exerciseMaxSubs)
            ? (string) $this->t('No limit')
            : $exerciseMaxSubs;
        // Compute is required display.
        $isRequired = $exerciseDue->getRequired();
        $isRequiredDisplay = $isRequired
          ? (string) $this->t('Yes')
          : (string) $this->t('No');
        // Compute when due.
        $classStartDate = new DateTime($this->currentClass->getStartDate());
        $dueDaysInterval = new DateInterval('P' . ($exerciseDue->getDay() - 1) . 'D');
        $exerciseDueDate = clone $classStartDate;
        $exerciseDueDate->add($dueDaysInterval);
        $whenDueDisplay = $this->skillingUtilities->daysDiffNowString($exerciseDueDate);
      }
    } // End there is a current class.
    // Get challenge, might be missing?
    $challenge = FALSE;
    if (isset($exercise->get(SkillingConstants::FIELD_CHALLENGE)->value)) {
      $challenge = $exercise->get(SkillingConstants::FIELD_CHALLENGE)->value;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $renderable = [
      '#theme' => 'insert_exercise',
      '#out_of_plugin_context' => $this->isOutOfContext,
      '#title' => $exercise->getTitle(),
      '#summary' => $summary,
      '#task' => $task,
      // Exercise nid is passed to submission popups, that pass it to
      // code in exercise-links JS library that closes popups,
      // and adjusts submission links on the current page.
      '#exercise_nid' => $exercise->id(),
      '#internal_name' => $exercise->field_internal_name->value,
      '#due_date' => $whenDueDisplay,
      '#required' => $isRequiredDisplay,
      '#max_subs' => $exerciseMaxSubsDisplay,
      '#number_subs' => $numberOfSubmissions,
      '#challenge' => $challenge,
      '#challenge_image_url' => $challengeImageUrl,
      '#submission_links' => $renderableSubmissionLinks,
      '#show_exercise_link' => $showExerciseLink ? 'yes' : 'no',
      '#submission_status' => $exerciseSubmissionStatus,
      '#is_student' => $this->currentUser->isStudent(),
      '#cache' => [
        'max-age' => 0,
      ],
      '#attached' => [
        'library' => 'skilling/exercise-links',
        'drupalSettings' => [
          'skillingFloatQueryParam' => SkillingConstants::FLOATER_QUERY_STRING,
        ],
      ],
    ];
    $rendered = Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
