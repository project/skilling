<?php
/**
 * Annotation custom tag plugin.
 *
 * User: mathieso
 * Date: 6/2/18
 * Time: 2:42 PM
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides an annotation tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_annotation_custom_tag",
 *  tag = "annotation",
 *  admin_label = @Translation("Annotation"),
 *  description = @Translation("Shows an annotation to the reader."),
 *  has_close_tag = true
 * )
 */
class AnnotationTag extends SkillingCustomTagBase {

  use StringTranslationTrait;

  /**
   * Default title.
   *
   * @var string
   */
  const DEFAULT_TITLE = 'Note';

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node containing this annotation.
   *
   * @return string
   *   Rendered content.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // What should the style of the annotation be?
    $style = $this->findOption(
      $options, SkillingConstants::STYLE_ALTS
    );
    // Set class based on style.
    if (!$style) {
      $containerClass = 'skilling-annotation-note';
      $defaultTitle = $this->t('Note');
    }
    else {
      switch ($style) {
        case 'hint':
          $containerClass = 'skilling-annotation-hint';
          $defaultTitle = $this->t('Hint');
          break;

        case 'note':
          $containerClass = 'skilling-annotation-note';
          $defaultTitle = $this->t('Note');
          break;

        case 'warning':
          $containerClass = 'skilling-annotation-warning';
          $defaultTitle = $this->t('Warning');
          break;

        case 'check':
        case 'checkitout':
        case 'check_it_out':
        case 'check-it-out':
          $containerClass = 'skilling-annotation-check';
          $defaultTitle = $this->t('Check it out');
          break;

        case 'cool':
        case 'coolthing':
        case 'cool-thing':
        case 'cool_thing':
          $containerClass = 'skilling-annotation-cool';
          $defaultTitle = $this->t('Cool thing');
          break;
      }
    }
    // What should the title of the annotation be?
    $title = $this->findOption(
      $options, SkillingConstants::TITLE_ALTS
    );
    if (!$title) {
      $title = $defaultTitle;
    }
    $content = $this->parser->parse($content);
    $renderable = [
      '#theme' => 'annotation',
      '#container_class' => $containerClass,
      '#title' => $title,
      '#content' => $content,
      '#attached' => [
        'library' => 'skilling/annotation',
      ],
    ];

    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
