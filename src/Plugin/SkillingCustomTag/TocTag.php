<?php
/**
 * ToC custom tag plugin.
 *
 * Let's authors add a table of contents for a lesson anywhere in that lesson.
 */

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Migrate\SkillingMigrationException;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides an annotation tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_toc_custom_tag",
 *  tag = "toc",
 *  admin_label = @Translation("Table of contents"),
 *  description = @Translation("Inserts a table of content for a lesson."),
 *  has_close_tag = false
 * )
 */
class TocTag extends SkillingCustomTagBase {

  const DEFAULT_TITLE = 'Lesson contents';

  use StringTranslationTrait;

  /**
   * Add ToC tag, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   The node containing this annotation.
   *
   * @return string
   *   Rendered content.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    // Include links, or not?
    $includeLinks = FALSE;
    $includeLinksString = $this->findOption($options, SkillingConstants::SHOW_LINKS_ALTS);
    if ($includeLinksString) {
      $includeLinks = in_array(strtolower($includeLinksString), SkillingConstants::TRUE_ALTS);
    }
    // What should the title of the ToC be?
    $title = $this->findOption(
      $options, SkillingConstants::TITLE_ALTS
    );
    if (!$title) {
      $title = $this->t(TocTag::DEFAULT_TITLE);
    }

    $renderable = [
      '#theme' => 'toc',
      '#links' => $includeLinks,
      '#title' => $title,
      '#attached' => [
        'library' => 'skilling/toc',
      ],
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
