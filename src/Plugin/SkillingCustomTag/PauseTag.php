<?php

namespace Drupal\skilling\Plugin\SkillingCustomTag;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\skilling\Plugin\SkillingCustomTagBase;
use Drupal\skilling\SkillingConstants;

/**
 * Provides a pause tag.
 *
 * @SkillingCustomTag(
 *  id = "skilling_pause_custom_tag",
 *  tag = "pause",
 *  admin_label = @Translation("Pause"),
 *  description = @Translation("Stop reading for a moment."),
 *  has_close_tag = false
 * )
 */
class PauseTag extends SkillingCustomTagBase {

  /**
   * Counter to uniquely identify every pause button.
   *
   * Used when restoring state on browser back.
   *
   * @var int
   */
  static private $pauseCounter = 0;

  use StringTranslationTrait;

  /**
   * Process content, with options.
   *
   * @param string $content
   *   Content to process.
   * @param array $options
   *   Options.
   * @param \Drupal\node\Entity\Node|null $nodeWithTag
   *   Node woth the tag.
   *
   * @return string
   *   Rendered content.
   */
  public function processTag($content = '', array $options = NULL, Node $nodeWithTag = NULL) {
    self::$pauseCounter++;
    $renderable = [
      '#theme' => 'pause',
      '#counter' => self::$pauseCounter,
      '#attached' => [
        'library' => 'skilling/pause',
      ],
    ];
    $rendered = \Drupal::service('renderer')->render($renderable);
    return $rendered;
  }

}
