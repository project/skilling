<?php /** @noinspection PhpUnusedParameterInspection */

namespace Drupal\skilling\EventSubscriber;

use Drupal;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Watch events.
 */
class SkillingSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['routingRouteFinished'];
    return $events;
  }

  /**
   * This method is called when the routing.route_finished event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   What happened.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function routingRouteFinished(GetResponseEvent $event) {
    /** @var \Drupal\skilling\BookUpdateTracker $bookUpdateTracker */
    $bookUpdateTracker = Drupal::service('skilling.book_update_tracker');
    $bookUpdateTracker->runScheduledBookOrderUpdates();
    /** @var \Drupal\skilling\RecomputeClassRoles $recomputeClassRoles */
    $recomputeClassRoles = Drupal::service('skilling.recompute_class_roles');
    $recomputeClassRoles->runScheduledClassRoleUpdates();
    // Recompute completion score if needed.
    /** @var \Drupal\skilling\SkillingCurrentUser $currentUser */
    $currentUser = Drupal::service('skilling.skilling_current_user');
    if ($currentUser->isStudent()) {
      /** @var \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClass */
      $currentClass = Drupal::service('skilling.current_class');
      if ($currentClass) {
        /** @var \Drupal\skilling\CompletionScore $completionScoreService */
        $completionScoreService = Drupal::service('skilling.completion_score');
        $completionScoreService->getCompletionScoreForCurrentEnrollment();
      }
    }
    $this->runScheduledCacheClear();
  }

  /**
   * Run a clear cache if a setting says to.
   */
  protected function runScheduledCacheClear() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = Drupal::service('config.factory');
    $settings = $configFactory->getEditable(SkillingConstants::SETTINGS_MAIN_KEY);
    $cacheClearScheduled = $settings->get(SkillingConstants::SETTING_KEY_SCHEDULE_CLEAR_CACHE);
    if ($cacheClearScheduled) {
      // Reset the flag.
      $settings->set(
        SkillingConstants::SETTING_KEY_SCHEDULE_CLEAR_CACHE, FALSE);
      $settings->save();
      // Clear the cache.
      drupal_flush_all_caches();
    }
  }}
