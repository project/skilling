<?php

namespace Drupal\skilling\SkillingClass;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\skilling\Exception\SkillingNotFoundException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingUserFactory;

/**
 * Make a SkillingClass object.
 */
class SkillingClassFactory {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManagerIn,
    SkillingUserFactory $skillingUserFactoryIn
  ) {
    $this->entityTypeManager = $entityTypeManagerIn;
    $this->skillingUserFactory = $skillingUserFactoryIn;
  }


  /**
   * @param $thingToWrap
   *
   * @return \Drupal\skilling\SkillingClass\SkillingClass
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function makeSkillingClass($thingToWrap) {
    /** @var \Drupal\Core\Entity\EntityInterface $class */
    $class = NULL;
    if (is_numeric($thingToWrap)) {
      // Passed a nid.
      $classId = $thingToWrap;
      // Validate the exercise id.
      if ($classId < 1) {
        throw new SkillingValueMissingException(
          'Expected class id, got ' . $classId,
          __FILE__, __LINE__
        );
      }
      $class = $this->entityTypeManager->getStorage('node')
        ->load($classId);
      if (!$class) {
        throw new SkillingNotFoundException(
          'Class with id not found. Id: ' . $classId,
          __FILE__, __LINE__
        );
      }
    } // end is numeric
    // If node, should have bundle() method.
    if (method_exists($thingToWrap, 'bundle')) {
      $class = $thingToWrap;
      if ($class->bundle() !== SkillingConstants::CLASS_CONTENT_TYPE) {
        throw new SkillingWrongTypeException(
          'Wrong bundle: ' . $class->bundle(),
          __FILE__, __LINE__
        );
      }
    }
    // Still found nothing?
    if (is_null($class)) {
      throw new SkillingNotFoundException(
        'Class making failed. Thing: ' . $thingToWrap,
        __FILE__, __LINE__
      );

    }
    $skillingClass = new SkillingClass(
      $this->entityTypeManager,
      $this->skillingUserFactory
    );
    /** @var NodeInterface $node */
    $node = $class;
    $skillingClass->setWrappedClassNode($node);
    return $skillingClass;
  }
}
