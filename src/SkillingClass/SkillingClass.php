<?php

namespace Drupal\skilling\SkillingClass;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\ExerciseDueRecord;

/**
 * Class SkillingClass
 *
 * @package Drupal\skilling\SkillingClass
 *
 * Represents a class that a student can take.
 *
 * Wraps a class content type node.
 *
 * Make instances with the SkillingClassFactory.
 */
class SkillingClass {


  //*** FIELDS ***

  /**
   * The wrapped class content type node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $wrappedDrupalClassNode = NULL;


  //*** SERVICES ***

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  protected $skillingUserFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManagerIn
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactoryIn
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManagerIn,
    SkillingUserFactory $skillingUserFactoryIn
  ) {
    // Remember services.
    $this->entityTypeManager = $entityTypeManagerIn;
    $this->skillingUserFactory = $skillingUserFactoryIn;
  }


  /**
   * @param \Drupal\node\NodeInterface $classNode
   *
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function setWrappedClassNode(NodeInterface $classNode) {
    if ($classNode->bundle() !== SkillingConstants::CLASS_CONTENT_TYPE) {
      throw new SkillingWrongTypeException(
        'Expected class node, got ' . $classNode->bundle(),
        __FILE__, __LINE__
      );
    }
    $this->wrappedDrupalClassNode = $classNode;
  }

  /**
   * Get the wrapped class node.
   *
   * @return NodeInterface
   *   The class.
   */
  public function getWrappedDrupalClassNode() {
    return $this->wrappedDrupalClassNode;
  }

  /**
   * @return int
   */
  public function id() {
    return $this->wrappedDrupalClassNode->id();
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->wrappedDrupalClassNode->getTitle();
  }

  /**
   * Get the current class' cache tags.
   *
   * @return string[]
   *   The tags.
   */
  public function getCacheTags() {
    return $this->wrappedDrupalClassNode->getCacheTags();
  }

  /**
   * Get the instructors for the class.
   *
   * @return \Drupal\skilling\SkillingUser[]
   *   User objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getInstructors() {
    // Find enrollments for the class, with instructor role.
    $enrollmentIds = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Enrollment is published.
      ->condition('status', TRUE)
      // For the class.
      ->condition('field_class', $this->wrappedDrupalClassNode->id())
      // Class is published.
      ->condition('field_class.entity.status', 1)
      // Is an instructor.
      ->condition('field_class_roles', [SkillingConstants::CLASS_ROLE_INSTRUCTOR], 'IN')
      ->execute();
    $enrollments = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($enrollmentIds);
    $instructorIds = [];
    /** @var \Drupal\node\Entity\Node $enrollment */
    foreach ($enrollments as $enrollment) {
      $instructorIds[] = $enrollment->get('field_user')->target_id;
    }
    // Load instructor's user objects.
    /** @var \Drupal\user\Entity\User[] $instructors */
    $instructors = $this->entityTypeManager
      ->getStorage('user')
      ->loadMultiple($instructorIds);
    $result = [];
    foreach ($instructors as $instructor) {
      $result[] = $this->skillingUserFactory->makeSkillingUser($instructor);
    }
    return $result;
  }

  /**
   * Get an instructor for the class.
   *
   * @return \Drupal\skilling\SkillingUser
   *   User object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getInstructor() {
    $instructors = $this->getInstructors();
    // If there is more than one, just get the first one.
    $instructor = reset($instructors);
    return $instructor;
  }


    /**
   * Get the exercise due paragraph items for the class.
   * Published exercises only.
   *
   * @return ExerciseDueRecord[]
   *   The exercise dues paragraph items, and exercise nids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function getExerciseDueRecords() {
    /** @var ExerciseDueRecord[] $exerciseDues */
    $exerciseDues = [];
    if (is_null($this->wrappedDrupalClassNode)) {
      throw new SkillingValueMissingException(
        'No class node.',
        __FILE__, __LINE__
      );
    }
    $exerciseDueParaRefs = $this->wrappedDrupalClassNode->get(SkillingConstants::FIELD_EXERCISES_DUE)->getValue();
    foreach ($exerciseDueParaRefs as $exerciseDueParaRef) {
      /** @var \Drupal\paragraphs\ParagraphInterface $exerciseDueParagraph */
      $exerciseDueParagraph = $this->entityTypeManager->getStorage('paragraph')
        ->load($exerciseDueParaRef['target_id']);
      if (!$exerciseDueParagraph) {
        // In case the para does not exist.
        continue;
      }
      $exerciseId = (int)$exerciseDueParagraph->get(SkillingConstants::FIELD_EXERCISE)
        ->getValue()[0]['target_id'];
      /** @var NodeInterface $exercise */
      $exercise = $this->entityTypeManager->getStorage('node')
        ->load($exerciseId);
      if (!$exercise) {
        // In case the exercise does not exist.
        $exerciseDueParagraph->delete();
        continue;
      }
      if ($exercise->isPublished()) {
        // Check that something exists.
        $dayDataThing = $exerciseDueParagraph->get(SkillingConstants::FIELD_DAY)
          ->getValue();
        if (isset($dayDataThing[0])) {
          $day = (int) $exerciseDueParagraph->get(SkillingConstants::FIELD_DAY)
            ->getValue()[0]['value'];
          $required = FALSE;
          if (isset($exerciseDueParagraph->get(SkillingConstants::FIELD_REQUIRED)
              ->getValue()[0]['value'])) {
            $required = (boolean) $exerciseDueParagraph->get(SkillingConstants::FIELD_REQUIRED)
              ->getValue()[0]['value'];
            // Convert from truthy/falsey to real bool.
            $required = !(!$required);
          }
          // Compute max subs allowed for exercise.
          // Start with value from class.
          $exerciseMaxSubs = (integer)$this->wrappedDrupalClassNode->get(SkillingConstants::FIELD_DEFAULT_MAX_SUBMISSIONS)
            ->getValue()[0]['value'];
          if (isset($exerciseDueParagraph->get(SkillingConstants::FIELD_MAX_SUBMISSIONS)
              ->getValue()[0]['value'])) {
            $exerciseMaxSubs = $exerciseDueParagraph->get(SkillingConstants::FIELD_MAX_SUBMISSIONS)
              ->getValue()[0]['value'];
            if (!$exerciseMaxSubs) {
              $exerciseMaxSubs = NULL;
              // Source of bug?
//              $exerciseMaxSubs = 0;
            }
            else {
              $exerciseMaxSubs = (integer) $exerciseMaxSubs;
            }
          }
          $exerciseDueRecord = new ExerciseDueRecord($exerciseId, $day, $required, $exerciseMaxSubs);
//            [
//            'exerciseId' => $exerciseId,
//            'day' => $day,
//            'required' => $required,
//            'maxSubs' => $exerciseMaxSubs,
//          ];
          $exerciseDues[] = $exerciseDueRecord;
//          $exerciseNids[] = $exerciseId;
        }
      }
    }
    return $exerciseDues;
//    return [$exerciseDues, $exerciseNids];
  }


  /**
   * Get exercise due record for an exercise for the class.
   * Return null if there is no record for the exercise.
   *
   * @param int $exerciseId
   *  Exercise id.
   *
   * @return ExerciseDueRecord|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getExerciseDueRecordForExercise($exerciseId) {
    $classExerciseDues = $this->getExerciseDueRecords();
    // Find the exercise due data for the exercise.
    $exerciseDue = NULL;
    /** @var ExerciseDueRecord $record */
    foreach ($classExerciseDues as $record) {
      if ($record->getExerciseId() == $exerciseId) {
        $exerciseDue = $record;
        break;
      }
    }
    return $exerciseDue;
  }

  /**
   * Get the default max submissions for a class.
   *
   * @return int|NULL
   */
  public function getDefaultMaxSubmissions() {
    $exerciseMaxSubs = $this->getWrappedDrupalClassNode()
      ->get(SkillingConstants::FIELD_DEFAULT_MAX_SUBMISSIONS)
      ->getValue()[0]['value'];
    // Could be null or MT string.
    if (is_numeric($exerciseMaxSubs)) {
      $exerciseMaxSubs = (int)$exerciseMaxSubs;
    }
    return $exerciseMaxSubs;
  }

  /**
   * @param $exerciseId
   *
   * @return int|NULL
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getMaxSubmissionsAllowedForExercise($exerciseId) {
    $exerciseDueRecord = $this->getExerciseDueRecordForExercise($exerciseId);
    if ($exerciseDueRecord) {
      $maxSubs = $exerciseDueRecord->getMaximumSubmissions();
    }
    else {
      $maxSubs = $this->getDefaultMaxSubmissions();
    }
    return $maxSubs;
  }

  /**
   * Get the start date.
   * @return mixed
   */
  public function getStartDate() {
    $whenStarts = $this->wrappedDrupalClassNode
      ->get(SkillingConstants::FIELD_WHEN_STARTS)
      ->getValue()[0]['value'];
    return $whenStarts;
  }

  public function getDescriptionFull() {
    $result = $this->getWrappedDrupalClassNode()->get('body')->getValue()[0]['value'];
    return $result;
  }

  public function getDescriptionSummary() {
    $result = $this->getWrappedDrupalClassNode()->get('body')->getValue()[0]['summary'];
    return $result;
  }

  public function getDescriptionFilter() {
    $result = $this->getWrappedDrupalClassNode()->get('body')->getValue()[0]['format'];
    return $result;
  }

}
