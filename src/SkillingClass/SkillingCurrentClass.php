<?php

namespace Drupal\skilling\SkillingClass;

//namespace src\SkillingClass;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\user\UserDataInterface;

/**
 * Knows what the user's current class is. Wraps a SkillingClass
 *
 * A user can be enrolled in more
 * than one class. A user can have different roles in different classes.
 *
 * If the user is enrolled in more than one class, s/he
 * has to choose one to use for things like choosing which
 * calendar to show.
 *
 * The id of the chosen class is stored in a UserData setting.
 * However, that isn't assumed to be correct, since enrollments
 * can change at any time. The constructor checks the
 * current setting value, and resets it if needed.
 *
 * If there is no current class (e.g., anon, no enrollment),
 * the UserData setting is 0.
 *
 * Besides the UserData setting, the constructor sets up
 * several properties that other code can query.
 *
 */
class SkillingCurrentClass {

  const CURRENT_CLASS_KEY = 'current_class_id';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User data interface.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The current class.
   *
   * @var SkillingClass
   */
  protected $wrappedSkillingClassNode = NULL;

  /**
   * The current user's current enrollment.
   *
   * Used to set the current class.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $currentEnrollment = NULL;

  /**
   * Current enrollments.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $currentEnrollments = [];

  /**
   * Current classes.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $currentClasses = [];

  /**
   * @var \Drupal\skilling\SkillingClass\SkillingClassFactory
   */
  protected $skillingClassFactory;

  /** @var SkillingUserFactory */
  protected $skillingUserFactory;

  /**
   * CurrentClass constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManagerIn
   *   The entity type manager service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactoryIn
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   The Skilling current user service.
   *
   * @param \Drupal\skilling\SkillingClass\SkillingClassFactory $classFactoryIn
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManagerIn,
    SkillingUserFactory $skillingUserFactoryIn,
    UserDataInterface $userData,
    SkillingUtilities $skillingUtilities,
    SkillingCurrentUser $skillingCurrentUser,
    SkillingClassFactory $classFactoryIn
  ) {
    // Remember services.
    $this->entityTypeManager = $entityTypeManagerIn;
    $this->skillingUserFactory = $skillingUserFactoryIn;
    $this->userData = $userData;
    $this->skillingUtilities = $skillingUtilities;
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->skillingClassFactory = $classFactoryIn;
    // Set the current class.
    if (!$this->skillingCurrentUser->isAnonymous()) {
      // User is logged in.
      // Get enrollments.
      $this->currentEnrollments = $this->getCurrentUserEnrollments();
      // Are there any?
      if (count($this->currentEnrollments) === 0) {
        // No enrollments for the user.
        $this->wrappedSkillingClassNode = NULL;
        $this->currentClasses = [];
        $this->currentEnrollments = [];
        $this->currentEnrollment = NULL;
        $this->userData->set(
          SkillingConstants::MODULE_NAME,
          $this->skillingCurrentUser->id(),
          self::CURRENT_CLASS_KEY,
          0
        );
      }
      else {
        // There are enrollments.
        if (count($this->currentEnrollments) === 1) {
          // Just one. Use the class.
          /** @var \Drupal\node\Entity\Node $enrollment */
          $this->currentEnrollment = current($this->currentEnrollments);
          /* @noinspection PhpUndefinedFieldInspection */
          $this->wrappedSkillingClassNode = $this->skillingClassFactory->makeSkillingClass(
            $this->currentEnrollment->field_class->entity
          );
          // TODO - change ^.
          $this->currentClasses = [$this->wrappedSkillingClassNode];
          $this->userData->set(
            SkillingConstants::MODULE_NAME,
            $this->skillingCurrentUser->id(),
            self::CURRENT_CLASS_KEY,
            $this->wrappedSkillingClassNode->id()
          );
        }
        else {
          // There is more than one enrollment.
          // Get the current id setting.
          $currentClassIdSetting = $this->userData->get(
            SkillingConstants::MODULE_NAME,
            $this->skillingCurrentUser->id(),
            self::CURRENT_CLASS_KEY
          );
          // Is there one?
          if (!$currentClassIdSetting) {
            // There are multiple enrollments, but no current class
            // set. Use the class for the first enrollment.
            /** @var \Drupal\node\Entity\Node $currentEnrollment */
            $this->currentEnrollment = current($this->currentEnrollments);
            /* @noinspection PhpUndefinedFieldInspection */
            $this->wrappedSkillingClassNode = $this->skillingClassFactory->makeSkillingClass(
              $this->currentEnrollment->field_class->entity
            );
//            $this->wrappedDrupalClassNode = $this->currentEnrollment->field_class->entity;
            $this->currentClasses = [$this->wrappedSkillingClassNode];
          }
          else {
            // There are multiple enrollments, and there is a class
            // setting.
            // Is the setting valid?
            $settingIsValid = FALSE;
            foreach ($this->currentEnrollments as $currentEnrollment) {
              /** @var \Drupal\node\Entity\Node $class */
              $class = $currentEnrollment->field_class->entity;
              // Add to the current class list while we are here.
              $skillingClass = $this->skillingClassFactory->makeSkillingClass($class);
              $this->currentClasses[$skillingClass->id()] = $skillingClass;
              if ($skillingClass->id() === $currentClassIdSetting) {
                // Found the current class setting in the
                // enrolled classes. The setting is good.
                $this->wrappedSkillingClassNode = $skillingClass; // $class;
                $this->currentEnrollment = $currentEnrollment;
                $settingIsValid = TRUE;
              }
            }
            if (!$settingIsValid) {
              // Did not find the current setting in the current
              // class list. Use the first class instead.
              reset($this->currentEnrollments);
              $this->currentEnrollment = current($this->currentEnrollments);
              $classNode = $this->currentEnrollment->field_class->entity;
              $skillingClass = $this->skillingClassFactory->makeSkillingClass($class);
              $this->wrappedSkillingClassNode = $skillingClass;
            }
          }
          // Current class is set. Remember its id.
          $this->userData->set(
            SkillingConstants::MODULE_NAME,
            $this->skillingCurrentUser->id(),
            self::CURRENT_CLASS_KEY,
            $this->wrappedSkillingClassNode->id()
          );
        } //End there is more than one enrollment.
      } //End there are enrollments.
    } //End user is logged in.
  }

  /**
   * Load the current user's enrollments.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getCurrentUserEnrollments() {
    $enrollments = [];
    // Is anon?
    if (!$this->skillingCurrentUser->isAnonymous()) {
      // Is user blocked?
      if (!$this->skillingCurrentUser->isBlocked()) {
        // get the published enrollments.
        $enrollments = $this->skillingUtilities->getUserEnrollmentsPublished(
          $this->skillingCurrentUser->id()
        );
      }
    }
    return $enrollments;
  }

  /**
   * Is there a current class?
   *
   * @return bool
   *   True if there is a current class.
   */
  public function isCurrentClass() {
    return !is_null($this->wrappedSkillingClassNode);
  }

  /**
   * Get the current class.
   *
   * If more than one enrollment,
   * the user has to choose one to be current.
   *
   * @return \Drupal\skilling\SkillingClass\SkillingClass
   *   The class.
   */
  public function getWrappedSkillingClass() {
    return $this->wrappedSkillingClassNode;
  }

  /**
   * Get the current classes.
   *
   * @return \Drupal\skilling\SkillingClass\SkillingClass[]
   *   The classes.
   */
  public function getCurrentClasses() {
    return $this->currentClasses;
  }

  /**
   * Get the current user's enrollment in the current class.
   *
   * @return \Drupal\node\NodeInterface
   *   Enrollment node.
   */
  public function getCurrentEnrollment() {
    return $this->currentEnrollment;
  }

  /**
   * Get whether progress score for current enrollment is based on
   * required exercises only.
   *
   * @return bool
   *   True if progress score for current enrollment is based on
   *   required exercises only.
   */
  public function getIsProgressScoreBasedOnRequiredOnlyForCurrentEnrollment() {
    $baseOnRequired = false;
    if (isset($this->currentEnrollment
        ->get(SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY)
        ->getValue()[0]['value'])) {
      $baseOnRequired = $this->currentEnrollment
        ->get(SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY)
        ->getValue()[0]['value'];
      $baseOnRequired = $baseOnRequired === "1";
    }
    return $baseOnRequired;
  }

  /**
   * Get the current user's enrollments.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Enrollment nodes.
   */
  public function getCurrentEnrollments() {
    return $this->currentEnrollments;
  }

  /**
   * Set the current class nid in the user data key storage.
   *
   * @param int $nid
   *   The nid.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function setUserDataCurrentClassNid($nid) {
    // Check that the current user is in the class.
    if (!array_key_exists($nid, $this->currentClasses)) {
      throw new SkillingException(
        Html::escape(
          'Current class switch: User '
          . $this->skillingCurrentUser->id()
          . ' not in class ' . $nid
        ), __FILE__, __LINE__
      );
    }
    $this->userData->set(
      SkillingConstants::MODULE_NAME,
      $this->skillingCurrentUser->id(),
      self::CURRENT_CLASS_KEY,
      $nid
    );
    // Invalidate class selector block cache.
    Cache::invalidateTags([SkillingConstants::BLOCK_CURRENT_CLASS_CACHE_TAG]);
  }

  /**
   * @return int
   */
  public function getId() {
    $result = $this->wrappedSkillingClassNode->id();
    return $result;
  }

  /**
   * @return string
   */
  public function getTitle() {
    $result = $this->wrappedSkillingClassNode->getTitle();
    return $result;
  }

  /**
   * @return string[]
   */
  public function getCacheTags() {
    $result = $this->wrappedSkillingClassNode->getCacheTags();
    return $result;
  }

  /**
   * @return \Drupal\skilling\SkillingUser[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getInstructors() {
    $result = $this->wrappedSkillingClassNode->getInstructors();
    return $result;
  }

  /**
   * @return \Drupal\skilling\SkillingUser
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function getInstructor() {
    $result = $this->wrappedSkillingClassNode->getInstructor();
    return $result;
  }

  /**
   * @return \Drupal\skilling\ExerciseDueRecord[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function getExerciseDueRecords() {
    $result = $this->wrappedSkillingClassNode->getExerciseDueRecords();
    return $result;
  }

  /**
   * @param $exerciseId
   *
   * @return \Drupal\skilling\ExerciseDueRecord|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getExerciseDueRecordForExercise($exerciseId) {
    $result = $this->wrappedSkillingClassNode
      ->getExerciseDueRecordForExercise($exerciseId);
    return $result;
  }

  public function getDefaultMaxSubmissions() {
    $result = $this->wrappedSkillingClassNode->getDefaultMaxSubmissions();
    return $result;
  }

  /**
   * @param $exerciseId
   *
   * @return int|NULL
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getMaxSubmissionsAllowedForExercise($exerciseId) {
    $result = $this->wrappedSkillingClassNode
      ->getMaxSubmissionsAllowedForExercise($exerciseId);
    return $result;
  }

  /**
   * @return mixed
   */
  public function getStartDate() {
    $result = $this->wrappedSkillingClassNode->getStartDate();
    return $result;
  }


}
