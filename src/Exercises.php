<?php

namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;

/**
 * Class Exercises.
 */
class Exercises {

  use StringTranslationTrait;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Submissions service.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;

  /**
   * @var \Drupal\skilling\Timeline
   */
  protected $timelineService;

  /**
   * Constructs a new Exercise object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManagerIn
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $skillingCurrentClassIn
   *   Current class service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUserIn
   *   Skilling current user service.
   * @param \Drupal\skilling\Submissions $submissionsServiceIn
   *   Submissions service.
   * @param \Drupal\skilling\Timeline $timelineServiceIn
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManagerIn,
    SkillingCurrentClass $skillingCurrentClassIn,
    SkillingCurrentUser $skillingCurrentUserIn,
    Submissions $submissionsServiceIn,
    Timeline $timelineServiceIn
) {
    $this->entityTypeManager = $entityTypeManagerIn;
    $this->currentClass = $skillingCurrentClassIn;
    $this->currentUser = $skillingCurrentUserIn;
    $this->submissionsService = $submissionsServiceIn;
    $this->timelineService = $timelineServiceIn;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getCourseExerciseSummary() {
    $result = [];
    if ($this->currentUser->isAnonymous()) {
      $result['problem_message'] = $this->t('Calendar only available when logged in.');
      return $result;
    }
    // Get the exercise due paragraph items for the class.
    $exerciseDuesForClass = $this->currentClass->getExerciseDueRecords();
    // Load all published exercises.
    $allPublishedExerciseNids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_CONTENT_TYPE)
      ->condition('status', 1)
      ->execute();
    $allPublishedExercises = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($allPublishedExerciseNids);
    // Find nids of exercises that are complete.
    // Load complete submissions from the current user.
    $submissionIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'submission')
      // For the current user.
      ->condition('field_user', $this->currentUser->id())
      // Published submissions.
      ->condition('status', 1)
      // Complete.
      ->condition('field_complete', 1)
      // Exercise is published. Extra check.
      ->condition('field_exercise.entity.status', 1)
      ->execute();
    // Load the submissions.
    $submissions = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($submissionIds);
    // Find the exercise nids for completed submissions.
    $exerciseNidsComplete = [];
    foreach ($submissions as $submission) {
      // TODO: replace
      $complete = $submission->field_complete->value;
      if ($complete) {
        $exerciseNidsComplete[] = $submission->field_exercise->target_id;
      }
    }
    // Count all the things.
    // Initialize counts.
    // Optional - not in calendar, not challenge.
    $countOptional = 0;
    $countOptionalCompleted = 0;
    // Required - in calendar, not challenge.
    $countRequired = 0;
    $countRequiredCompleted = 0;
    $countChallenge = 0;
    $countChallengeCompleted = 0;
    foreach ($allPublishedExercises as $exercise) {
      $exerciseNid = $exercise->id();
      // Is challenge?
      $challenge = $exercise->field_challenge->value;
      if ($challenge) {
        $countChallenge++;
        // Complete?
        if (in_array($exerciseNid, $exerciseNidsComplete)) {
          $countChallengeCompleted++;
        }
      }
      // Find due data for the exercise
      $dueDataForExercise = NULL;
      foreach ($exerciseDuesForClass as $exerciseDue) {
        if ($exerciseDue->getExerciseId() == $exerciseNid) {
          $dueDataForExercise = $exerciseDue;
          break;
        }
      }
      if (is_null($dueDataForExercise)) {
        // No due data for it, assume optional.
        $countOptional++;
        // Complete?
        if (in_array($exerciseNid, $exerciseNidsComplete)) {
          $countOptionalCompleted++;
        }
      }
      else if ($dueDataForExercise->getRequired()) {
        // Required.
        $countRequired++;
        // Complete?
        if (in_array($exerciseNid, $exerciseNidsComplete)) {
          $countRequiredCompleted++;
        }
      }
      else {
        $countOptional++;
        // Complete?
        if (in_array($exerciseNid, $exerciseNidsComplete)) {
          $countOptionalCompleted++;
        }
      }

    }
    $result['required_exercises'] = $countRequired;
    $result['required_exercises_completed'] = $countRequiredCompleted;
    $result['optional_exercises'] = $countOptional;
    $result['optional_exercises_completed'] = $countOptionalCompleted;
    $result['challenge_exercises'] = $countChallenge;
    $result['challenge_exercises_completed'] = $countChallengeCompleted;
    return $result;
  }

}
