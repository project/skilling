<?php

/**
 * @file
 * A bag with pockets to track different groups of nids.
 * Each node type being tracked has a pocket.
 * When the skilling parser finds an object like a pattern
 * inserted by an author, it drops a nid into a pocket.
 * Hook node logic looks in the bag to find the nids,
 * and updates fields in nodes that indicate which
 * lessons the nodes are referenced in.
 */

namespace Drupal\skilling;

class NidBag {

  /**
   * The bag of nids.
   *
   * @var array
   */
  protected $bag = [];

  /**
   * Add a nid to the bag.
   *
   * @param string $pocketName Pocket name.
   * @param integer $nid The nid.
   */
  public function addToBag($pocketName, $nid) {
    if (! isset($this->bag[$pocketName])) {
      $this->bag[$pocketName] = [];
    }
    if (!in_array($nid, $this->bag[$pocketName])) {
      $this->bag[$pocketName][] = $nid;
    }
  }

  /**
   * Get all of the nids in a pocket.
   *
   * @param string $pocketName
   *   Pocket name.
   *
   * @return integer[]
   *   The nids.
   */
  public function getNidsInPocket($pocketName) {
    $result = isset($this->bag[$pocketName]) ? $this->bag[$pocketName] : [];
    return $result;
  }

  /**
   * Get all of the nids in all pockets.
   *
   * @return integer[] The nids.
   */
  public function getAllNidsInBag() {
    $nids = [];
    foreach ($this->bag as $pocketName=>$pocketNids) {
      $nids = array_merge($nids, $pocketNids);
    }
    return $nids;
  }

  /**
   * Empty a pocket.
   *
   * @param string $pocketName Name of the pocket.
   *
   */
  public function clearPocket($pocketName) {
    $this->bag[$pocketName] = [];
  }

  /**
   * Empty all of the pockets.
   */
  public function clearAllPockets() {
    $this->bag = [];
  }

}
