<?php

namespace Drupal\skilling;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\Exception\SkillingException;
use Drupal\Component\Utility\Html;

/**
 * Class Enrollment.
 */
class Enrollment implements EnrollmentInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Constructs a new Enrollment object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   The Skilling current user service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Utilities $skillingUtilities,
    SkillingCurrentUser $skillingCurrentUser
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->skillingCurrentUser = $skillingCurrentUser;
  }

  /**
   * Change the enrollment form.
   *
   * @param array $form
   *   The form renderable.
   */
  public function alterEnrollmentForm(array &$form, $classPrepopulate = NULL) {
    if (!is_null($classPrepopulate)) {
      // Class came in through the URL.
      // Is it a class?
      /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
      $entityTypeManager = \Drupal::service('entity_type.manager');
      /** @var \Drupal\node\Entity\Node $classNode */
      $classNode = $entityTypeManager->getStorage('node')
        ->load($classPrepopulate);
      if ($classNode) {
        if ($classNode->bundle() === SkillingConstants::CLASS_CONTENT_TYPE) {
          // It's a class.
          // Set the default value of the class field.
          $form[SkillingConstants::FIELD_CLASS]['widget']['#default_value'][0]
            = $classPrepopulate;
        }
      }
    }
    // Hide the title. Compute it from other elements.
    $form['title']['#type'] = 'hidden';
    // Need to give the field a default value, for Drupal's validation code
    // to get to the point where a value for the field can be computed.
    $form['title']['widget'][0]['value']['#default_value'] = 'Dogs are the best!';
    // Hide completion score fields from non-admins.
    /** @var \Drupal\skilling\SkillingCurrentUser $currentUser */
    $currentUser = \Drupal::service('skilling.skilling_current_user');
    if (!$currentUser->isAdministrator()) {
      $form['field_completion_score']['#access'] = FALSE;
      $form['field_when_score_updated']['#access'] = FALSE;
    }
    $form['#validate'][] = '_skilling_enrollment_form_validate';
  }

  /**
   * Compute a title for an enrollment.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function computeEnrollmentTitleFieldValue(FormStateInterface $formState) {
    // Get the class title.
    $gotClass = FALSE;
    // Avoid inspection error.
    $classTitle = 'Some class';
    $classField = $formState->getValue('field_class');
    if ($classField) {
      $classNid = $classField[0]['target_id'];
      /** @var \Drupal\node\Entity\Node $class */
      $class = $this->entityTypeManager->getStorage('node')->load($classNid);
      if ($class) {
        $classTitle = $class->getTitle();
        $gotClass = TRUE;
      }
    }
    // Get the user name.
    $gotUser = FALSE;
    // Avoid inspection error.
    $userName = 'Someone';
    $userField = $formState->getValue('field_user');
    if ($userField) {
      $userUid = $userField[0]['target_id'];
      /** @var \Drupal\user\Entity\User $user */
      $user = $this->entityTypeManager->getStorage('user')->load($userUid);
      if ($user) {
        $userName = $user->getAccountName();
        $gotUser = TRUE;
      }
    }
    if ($gotClass && $gotUser) {
      // Got both the user and the class. Make the enrollment node's title.
      $title = $this->t(
        '@user enrolled in @class',
        [
          '@user' => $userName,
          // So don't escape class name, which might have 's in it.
          '@class' => new FormattableMarkup($classTitle, [])
        ]

      );
      $formState->setValue('title', [['value' => (string) $title]]);
    }
  }

  /**
   * Part of enrollment node form validation.
   *
   * Check whether an enrollment already exists for a new enrollment.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   State of the submitted form.
   *
   * @return bool
   *   True if no problem, false if there is a problem.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function checkDuplicateEnrollment(FormStateInterface $formState) {
    // Get the class that the user specified.
    $gotClass = FALSE;
    // Avoid inspection errors.
    $classNid = 666;
    $userUid = 666;
    $classField = $formState->getValue('field_class');
    if ($classField) {
      $classNid = $classField[0]['target_id'];
      $gotClass = TRUE;
    }
    // Get the user that the user specified.
    $gotUser = FALSE;
    $userField = $formState->getValue('field_user');
    if ($userField) {
      $userUid = $userField[0]['target_id'];
      $gotUser = TRUE;
    }
    if (!$gotClass || !$gotUser) {
      return FALSE;
    }
    // Load enrollment with those attributes.
    $enrollmentNids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // With the right class.
      ->condition('field_class.target_id', $classNid)
      // With the right user.
      ->condition('field_user.target_id', $userUid)
      ->execute();
    if (count($enrollmentNids) === 0) {
      // No existing enrollment records found. All OK.
      return TRUE;
    }
    if (count($enrollmentNids) > 1) {
      // This should not happen.
      $deets = Html::escape('Duplicate enrollment: User ' . $userUid . ', class ' . $classNid);
      throw new SkillingException($deets, __FILE__, __LINE__);
    }
    // There is one record.
    $enrollmentNid = reset($enrollmentNids);
    // Is this an add or an edit?
    $operation = 'add';
    if (strpos($formState->getValue('form_id'), 'edit') !== FALSE) {
      $operation = 'edit';
    }
    if ($operation === 'add') {
      // The new record is a duplicate.
      $deets = $this->getEnrollmentsExistsErrorMessage($enrollmentNid);
      $formState->setErrorByName('field_class', $deets);
      return FALSE;
    }
    elseif ($operation === 'edit') {
      // If the edit is for the enrollment that was found, no problem.
      /** @var \Drupal\node\NodeForm $nodeForm */
      $nodeForm = $formState->getFormObject();
      $idFromForm = $nodeForm->getEntity()->id();
      if ($idFromForm == $enrollmentNid) {
        return TRUE;
      }
      // There's already a record for the enrollment the user wants to record.
      $deets = $this->getEnrollmentsExistsErrorMessage($enrollmentNid);
      $formState->setErrorByName('field_class', $deets);
      return FALSE;
    }
    else {
      // This should not happen.
      $deets = Html::escape('Bad operation: ' . $operation);
      throw new SkillingException($deets, __FILE__, __LINE__);
    }
  }

  /**
   * Create an error message for when an attempt to make a duplicate enrollment.
   *
   * @param int $enrollmentNid
   *   Nid of an enrollment record.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Error message.
   */
  protected function getEnrollmentsExistsErrorMessage($enrollmentNid) {
    $enrollmentUrl = $this->skillingUtilities->getSiteBaseUrl()
      . 'node/' . $enrollmentNid;
    $deets = $this->t(
      'That person is already enrolled in that class. <a href=":url" target="_blank">View the record</a>',
      [':url' => $enrollmentUrl]
    );
    return $deets;
  }

  /**
   * Get enrollments for students.
   *
   * Only include published enrollments, for unblocked users, in
   * published classes.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   *   The enrollments.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStudentEnrollments() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $enrollmentIds = $nodeStorage->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Published.
      ->condition('status', 1)
      // Have student role.
      ->condition('field_class_roles', ['student'], 'IN')
      // User is not blocked.
      ->condition('field_user.entity.status', 1)
      // Class is published.
      ->condition('field_class.entity.status', 1)
      ->execute();
    $result = [];
    if (count($enrollmentIds) > 0) {
      $result = $nodeStorage->loadMultiple($enrollmentIds);
    }
    return $result;
  }

  /**
   * Get ids of enrolled students.
   *
   * Only include published enrollments, for unblocked users, in
   * published classes.
   *
   * @return int[]
   *   Ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEnrolledStudentIds() {
    $result = [];
    $enrollments = $this->getStudentEnrollments();
    if (count($enrollments) > 0) {
      foreach ($enrollments as $enrollment) {
        $result[] = (int) $enrollment->get(SkillingConstants::FIELD_USER)->target_id;
      }
    }
    return $result;
  }

  /**
   * Get the ids of student enrollments for a class.
   *
   * @param int $classId
   *   Class id.
   *
   * @return array|int[]
   *   Enrollment ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getStudentEnrollmentIdsForClassId($classId) {
    $enrollmentIds = [];
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    // Does the node exist?
    /** @var \Drupal\node\Entity\Node $class */
    $class = $nodeStorage->load($classId);
    if ($class) {
      if ($class->bundle() === SkillingConstants::CLASS_CONTENT_TYPE) {
        if ($class->isPublished()) {
          // The class is published.
          $enrollmentIds = $nodeStorage->getQuery()
            ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
            // Published.
            ->condition('status', 1)
            // Have student role.
            ->condition('field_class_roles', ['student'], 'IN')
            // User is not blocked.
            ->condition('field_user.entity.status', 1)
            // Right class.
            ->condition('field_class', $classId)
            // Class is published.
            ->condition('field_class.entity.status', 1)
            ->execute();
        }
      }
    }
    return $enrollmentIds;
  }

  /**
   * Get the ids of student enrollments for all classes.
   *
   * @return array|int[]
   *   Enrollment ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStudentEnrollmentIdsForAllClasses() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    // The class is published.
    $enrollmentIds = $nodeStorage->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Published.
      ->condition('status', 1)
      // Have student role.
      ->condition('field_class_roles', ['student'], 'IN')
      // User is not blocked.
      ->condition('field_user.entity.status', 1)
      // Class is published.
      ->condition('field_class.entity.status', 1)
      ->execute();
    return $enrollmentIds;
  }

  /**
   * @param $userId
   * @param $classId
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isStudentIdInClassId($userId, $classId) {
    $enrollmentIds = $this->entityTypeManager
      ->getStorage('node')->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      // Published.
      ->condition('status', 1)
      // With the right class.
      ->condition('field_class.target_id', $classId)
      // With the right user.
      ->condition('field_user.target_id', $userId)
      // Have student role.
      ->condition('field_class_roles', ['student'], 'IN')
      // User is not blocked.
      ->condition('field_user.entity.status', 1)
      // Class is published.
      ->condition('field_class.entity.status', 1)
      ->execute();
    $result = (count($enrollmentIds) > 0);
    return $result;
  }

}
