<?php
namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Core\Render\Renderer;

/**
 * Assessment service. Used by the grading UI.
 *
 * @package Drupal\skilling
 */
class Assessment {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Assessment constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeTanager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The current user service.
   * @param \Drupal\skilling\Utilities $utilities
   *   The Skilling utilities service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeTanager,
    SkillingCurrentUser $currentUser,
    SkillingUtilities $utilities,
    \Drupal\Core\Datetime\DateFormatter $dateFormatter,
    Renderer $renderer
  ) {
    $this->entityTypeManager = $entityTypeTanager;
    $this->currentUser = $currentUser;
    $this->skillingUtilities = $utilities;
    $this->dateFormatter = $dateFormatter;
    $this->renderer = $renderer;
  }

  /**
   * Compute number of submissions to grade.
   *
   * @return int
   *   Number of submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function computeNumSubmissionsWaitingAssessment($includeUnpublishedExercises = TRUE) {
    // Exit if not grader.
    if (!$this->currentUser->isGrader()) {
      return NULL;
    }
    // Exit if blocked.
    if ($this->currentUser->isBlocked()) {
      return NULL;
    }
    // User has to be a grader in a published class.
    $classNids = $this->currentUser->getClassNidsForGrader();
    /** @var integer $totalSubmissionsToGrade Total number of ungraded
     * submissions in class for which the current user is a grader.
     */
    $totalSubmissionsToGrade = 0;
    foreach ($classNids as $classNid) {
      /** @var \Drupal\node\Entity\Node $class */
      $class = $this->entityTypeManager
        ->getStorage('node')->load($classNid);
      // Check that it exists.
      if (!$class) {
        continue;
      }
      $classUngradedSubmissions
        = $this->countUngradedSubmissionsForClass($class, $includeUnpublishedExercises);
      $totalSubmissionsToGrade += $classUngradedSubmissions;
    }
    return $totalSubmissionsToGrade;
  }

  /**
   * Count ungraded submissions in a class.
   *
   * Never count unpublished classes.
   * Never count submissions from
   * blocked users. Never count unpublished submissions.
   * Never count unpublished enrollments.
   * Never count unpublished exercises.
   *
   * @param \Drupal\node\Entity\Node $class
   *   Class to count submissions for.
   *
   * @return int
   *   Number of submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function countUngradedSubmissionsForClass(Node $class, $includeUnpublishedExercises = TRUE) {
    // Another redundant check. Paranoia much?
    if (!$class->isPublished()) {
      return 0;
    }
    // Load the published enrollments for the class, where users
    // are not blocked, and have the student role.
    $enrollmentQuery = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      ->condition('status', TRUE)
      ->condition('field_class', $class->id())
      ->condition('field_class_roles', ['student'], 'IN')
      ->condition('field_user.entity.status', 1);
    $enrollmentIds = $enrollmentQuery->execute();
    if (count($enrollmentIds) === 0) {
      return 0;
    }
    // Load the enrollments, to get the user ids from them.
    $enrollments = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($enrollmentIds);
    // Grab the user ids.
    $studentIds = [];
    /** @var \Drupal\node\Entity\Node $enrollment */
    foreach ($enrollments as $enrollment) {
      /* @noinspection PhpUndefinedFieldInspection */
      $studentIds[] = $enrollment->field_user->target_id;
    }
    // Query to grab ids of published submissions from those students
    // that have not been graded.
    $query = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
      // Published submissions.
      ->condition('status', 1)
      ->condition('field_user', $studentIds, 'IN')
      // No feedback given.
      ->notExists('field_when_feedback_given');
    if (!$includeUnpublishedExercises) {
      // Published exercises only.
      $query->condition('field_exercise.entity.status', 1);
    }
    $submissionCount = $query
      ->count()
      ->execute();
    return $submissionCount;
  }

  /**
   * Get ungraded submissions for a set of classes.
   *
   * Never count submissions from
   * blocked users. Never count unpublished submissions.
   * Never count unpublished enrollments.
   * Never count unpublished exercises.
   *
   * @param array $classIds
   *   Nids of classes to check.
   *
   * @param bool $includeUnpublishedExercises
   *   Whether to include unpublished exercises.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUngradedSubmissionsForClasses(
    array $classIds,
    $includeUnpublishedExercises = TRUE
  ) {
    // Load classes that are published.
    $classes = [];
    foreach ($classIds as $classId) {
      /** @var \Drupal\node\Entity\Node $class */
      $class = $this->entityTypeManager
        ->getStorage('node')
        ->load($classId);
      // Class exists?
      if (!$class) {
        continue;
      }
      // Only published classes.
      if (!$class->isPublished()) {
        continue;
      }
      $classes[$classId] = $class;
    }
    // Leave if there are no classes left.
    if (count($classes) === 0) {
      return [];
    }
    // Load the published enrollments for the classes, where users
    // are not blocked, and have the student role.
    $filteredClassIds = array_keys($classes);
    $enrollmentQuery = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::ENROLLMENT_CONTENT_TYPE)
      ->condition('status', TRUE)
      ->condition('field_class', $filteredClassIds, 'IN')
      ->condition('field_class_roles', ['student'], 'IN')
      ->condition('field_user.entity.status', 1);
    $enrollmentIds = $enrollmentQuery->execute();
    if (count($enrollmentIds) === 0) {
      return [];
    }
    // Load the enrollments, to get the user ids from them.
    $enrollments = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($enrollmentIds);
    // Grab the user ids.
    $studentIds = [];
    /** @var \Drupal\node\Entity\Node $enrollment */
    foreach ($enrollments as $enrollment) {
      /* @noinspection PhpUndefinedFieldInspection */
      $studentIds[] = $enrollment->field_user->target_id;
    }
    // Leave if there are none.
    if (count($studentIds) === 0) {
      return [];
    }
    // Query to grab ids of published submissions from those students
    // that have not been graded.
    $query = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
      // Published submissions.
      ->condition('status', 1)
      ->condition('field_user', $studentIds, 'IN')
      // No feedback given.
      ->notExists('field_when_feedback_given');
    if (!$includeUnpublishedExercises) {
      // Published exercises only.
      $query->condition('field_exercise.entity.status', 1);
    }
    $submissionIds = $query->execute();
    // Leave if there are none.
    if (count($submissionIds) === 0) {
      return [];
    }
    // Load the submissions from the ids.
    /* @noinspection PhpUndefinedFieldInspection */
    $submissions = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($submissionIds);
    // Leave if there are none.
    if (count($submissions) === 0) {
      return [];
    }
    // Get previous submissions.
    /** @var \Drupal\node\Entity\Node $submission */
    foreach ($submissions as $submission) {
      $version = $submission->field_version->value;
      if ($version !== 1) {
        // There were earlier submissions for this one.
        // Load them.
        $exerciseId = $submission->field_exercise->target_id;
        $studentId = $submission->field_user->target_id;
        $priorSubmissionIds = $this->entityTypeManager->getStorage('node')
          ->getQuery()
          ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
          // Published submissions.
          ->condition('status', 1)
          // The right student.
          ->condition('field_user', $studentId)
          // Published exercises only.
          ->condition('field_exercise.entity.status', 1)
          // The right exercise.
          ->condition('field_exercise', $exerciseId)
          // Feedback given.
          ->exists('field_when_feedback_given')
          // Sort by version, most recent first.
          ->sort('field_version', 'DESC')
          ->execute();
        // Found any?
        if (count($priorSubmissionIds) > 0) {
          // Got some. Load them.
          $priorSubmissions = $this->entityTypeManager->getStorage('node')
            ->loadMultiple($priorSubmissionIds);
          // Loop across them, updating suubmission field_solution.
          /** @var \Drupal\node\Entity\Node $priorSubmission */
          foreach ($priorSubmissions as $priorSubmission) {
            $priorSubmissionVersion = $priorSubmission->field_version->value;
            $feedback = $priorSubmission->field_feedback_message->value;
            $renderable = [
              '#theme' => 'prior_submission',
              '#version' => $priorSubmissionVersion,
              '#feedback' => $feedback,
            ];
            $description = $this->renderer->render($renderable);
            // Add to the submission.
            $submission->field_solution->value .= $description;
          } // End for each prior submission.
        } // End there were prior submissions.
      } // There was a submission that isn't version 1.
    } // For each submission.
    $result = [];
    // Format the data in arrays.
    /** @var \Drupal\node\Entity\Node $submission */
    foreach ($submissions as $submission) {
      $timeAgo = $this->dateFormatter->formatTimeDiffSince(
        $submission->getCreatedTime()
      );
      // Make URLs to files.
      $submittedFiles = [];
      $fileData = $submission->field_submitted_files->getValue();
      foreach ($fileData as $fileDatum) {
        $fileTargetId = $fileDatum['target_id'];
        /** @var \Drupal\file\Entity\File $file */
        $file = File::load($fileTargetId);
        $fileUrl = $file->createFileUrl();
        $fileOwnerId = $file->getOwnerId();
        $fileDeets['url'] = $fileUrl;
        $fileDeets['owner'] = $fileOwnerId;
        $submittedFiles[] = $fileDeets;
      }
      // Make URLs to files attached by grader.
      $graderFiles = [];
      $fileData = $submission->field_grader_attachments->getValue();
      foreach ($fileData as $fileDatum) {
        $fileTargetId = $fileDatum['target_id'];
        /** @var \Drupal\file\Entity\File $file */
        $file = File::load($fileTargetId);
        $fileUrl = $file->createFileUrl();
        $fileOwnerId = $file->getOwnerId();
        $fileDeets['fid'] = $fileTargetId;
        $fileDeets['ownerUid'] = $fileOwnerId;
        $fileDeets['url'] = $fileUrl;
        $fileDeets['owner'] = $fileOwnerId;
        $graderFiles[] = $fileDeets;
      }
      /* @noinspection PhpUndefinedFieldInspection */
      $submissionElement = [
        'submissionId' => $submission->id(),
        'studentId' => $submission->field_user->target_id,
        'exerciseId' => $submission->field_exercise->target_id,
        'version' => $submission->field_version->value,
        'when' => $timeAgo,
        'submittedContent' => $submission->field_solution->value,
        'submittedFileUrls' => $submittedFiles,
        'graderFiles' => $graderFiles,
        'exerciseCompleted' => FALSE,
        'rubricItemChoices' => [],
        'overallEvaluation' => '',
        'feedback' => '',
        'feedbackDate' => '',
      ];
      $result[$submission->id()] = $submissionElement;
    }
    return $result;
  }

  /**
   * Get classes for a grader.
   *
   * @param array $classIds
   *   Ids of classes data is needed for.
   *
   * @return array
   *   The classes for the grader.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassesForGrader(array $classIds) {
    // Exit if none.
    if ( count($classIds) === 0) {
      return [];
    }
    // Load the classes.
    $classes = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($classIds);
    // Extract needed data.
    $result = [];
    /** @var \Drupal\node\Entity\Node $class */
    foreach ($classes as $class) {
      /* @noinspection PhpUndefinedFieldInspection */
      $result[$class->id()] = [
        'classId' => $class->id(),
        'name' => $class->title->value,
      ];
    }
    return $result;
  }

}
