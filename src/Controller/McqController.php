<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;

/**
 * Class McqController.
 */
class McqController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling parser, for parsing student solution text.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * The module handler service, for testing whether Skilling history is enabled.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   *   Skilling parser.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skilling_current_user,
    SkillingUtilities $skillingUtilities,
    SkillingParser $parser,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->skillingUtilities = $skillingUtilities;
    $this->parser = $parser;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.utilities'),
      $container->get('skilling.skillingparser'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler')
    );
  }

  /**
   * Check student response.
   *
   * Expect in request:
   *   internalName: internal name of the item.
   *   response: what the user chose. 0, 1, etc. Index into responses array.
   *
   * Return JSON object:
   *  status: "OK" or "error"
   *  errorMessage: message to show if there was a problem.
   *  correct: Was the response correct?
   *  responseMessage: Message to show about the response.
   *
   * Security note: get two values from client: internal name, and response.
   * Both subject to Stripping.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function checkResponse(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/check-mcq-response'],
      TRUE
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in MCQ::checkResponse');
    }
    // Get the internal name of the MCQ.
    $internalName = $request->get('internalName');
    // Filter.
    $internalName = $this->filterInputService->filterUserContent($internalName);
    // Check that MCQ with that name exists.
    $mcqIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is an exercise.
      ->condition('type', SkillingConstants::MCQ_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$mcqIds) {
      $message = 'Bad internal name: ' . Html::escape($internalName);
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Have the id of the MCQ.
    $mcqId = reset($mcqIds);
    // Get the response the student chose.
    $userResponse = $request->get('response');
    // Clean.
    $userResponse = $this->filterInputService->filterUserContent($userResponse);
    // Load the MCQ.
    /** @var \Drupal\node\NodeInterface $mcq */
    $mcq = $this->entityTypeManager->getStorage('node')->load($mcqId);
    // Get the user who owns the data.
    if (!$mcq) {
      $message = 'Bad node in MCQ::checkResponse';
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Find the response option chosen by the user.
    $responses = $mcq->field_responses;
    $maxResponse = $responses->count();
    if ($userResponse > $maxResponse) {
      $message = 'Bad response in MCQ::checkResponse';
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // -1 since response in 1-based.
    $chosenResponse = $mcq->field_responses[$userResponse - 1]->entity;
    // Covert to boolean.
    $correct = $chosenResponse->field_correct->value ? TRUE : FALSE;
    // Don't do Stripping on the explanation. It might have extra stuff
    // authors want.
    $explanation = $this->parser->parse($chosenResponse->field_explanation->value);
    if ($this->skillingCurrentUser->isStudent()) {
      // Check whether the history module is active.
      $isHistoryModuleActive =
        $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
      if ($isHistoryModuleActive) {
        /** @var \Drupal\skilling_history\History $historyService */
        $historyService = \Drupal::service(SkillingConstants::HISTORY_SERVICE_NAME);
        // Record the response in history.
        try {
          $historyService->recordMcqAnswer($mcqId, $internalName, $userResponse);
        }
        catch (\Exception $e) {
          $message = 'Exception MCQ history call, MCQ nid: ' . Html::escape($mcqId)
            . $e->getMessage();
          return $this->skillingUtilities->formatJsonErrorReport($message);
        }
      }
    }
    // Send result of response check to user.
    $result = [
      'status' => 'OK',
      'correct' => $correct,
      'responseMessage' => $explanation,
    ];
    return new JsonResponse($result);
  }

  /**
   * Format an error message for sending back to mcq.js.
   *
   * @param string $message
   *   The message.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON result.
   */
//  protected function formatJsonErrorReport($message) {
//    $result = new JsonResponse(
//      ['status' => 'error', 'errorMessage' => $message]
//    );
//    return $result;
//  }

}
