<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Views;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling_history\History;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class ReflectNoteController.
 */
class ReflectNoteController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling history event recorder.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new ReflectNoteController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\skilling_history\History $historyService
   *   Skilling history service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skilling_current_user,
    History $historyService,
    SkillingUtilities $skillingUtilities,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->historyService = $historyService;
    $this->skillingUtilities = $skillingUtilities;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling_history.history'),
      $container->get('skilling.utilities'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler')
    );
  }

  /**
   * Show reflect notes view for student.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   Renderable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function showReflectNotes() {
    if (!$this->skillingCurrentUser->isStudent()) {
      return [
        '#markup' => $this->t('Sorry, this view is for students only.'),
      ];
    }
    $args = [$this->skillingCurrentUser->id()];
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('reflect_notes_for_students');
    $result = [
      '#markup' => 'Reflect note attachment error.',
    ];
    if (is_object($view)) {
      $view->setArguments($args);
      $view->setDisplay('reflect_notes');
      $view->preExecute();
      $view->execute();
      $result = $view->buildRenderable('reflect_notes', $args);
      // Record the event.
//      $this->historyService->createHistoryNode(
//        t('View reflect notes'), SkillingConstants::HISTORY_EVENT_VIEW_REFLECT_NOTES,
//        $this->skillingCurrentUser->id(), [], ''
//      );
    }
    return $result;
  }

  /**
   * Save note.
   *
   * Security note: get identifier and note from client. Strip them.
   * Also, treat anything unexpected (e.g., node not found) as
   * a security exception.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveNote(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/save-reflect-note']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in saveNote');
    }
    // Get the user who owns the data.
    if ($this->skillingCurrentUser->isAnonymous()) {
      throw new AccessDeniedException('Access denied, anon user in saveNote');
    }
    $reflectNoteNodeOwnerId = $this->skillingCurrentUser->id();
    // Extract the nid of the node with the reflect tag, and the tag ident.
    // They were concatenated to create a unique identifier in DOM land.
    $nidAndIdentifier = $request->get('identifier');
    // Just in case.
    $nidAndIdentifier = $this->filterInputService->filterUserContent($nidAndIdentifier);
    $dashPos = strpos($nidAndIdentifier, '-');
    if ($dashPos === FALSE) {
      throw new AccessDeniedException('Access denied, dash pos in saveNote');
    }
    // Get the nid of the node with the reflect tag in it.
    $nidNodeWithReflectTag = substr($nidAndIdentifier, 0, $dashPos);
    $identifier = substr($nidAndIdentifier, $dashPos + 1);
    // Check that the node exists.
    $nodeWithReflectTag = $this->entityTypeManager->getStorage('node')
      ->load($nidNodeWithReflectTag);
    if (!$nodeWithReflectTag) {
      throw new AccessDeniedException('Access denied, nid not found in saveNote');
    }
    // What the user typed.
    $note = $request->get('note');
    // Filter it.
    $note = $this->filterInputService->filterUserContent($note);
    $note = trim($note);
    // Load reflect note node, if exists.
    $reflectNoteNids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::REFLECT_NOTE_CONTENT_TYPE)
      ->condition('uid', $reflectNoteNodeOwnerId)
      ->condition('field_node.target_id', $nidNodeWithReflectTag)
      ->condition('field_internal_name', $identifier)
      ->execute();
    /** @var \Drupal\node\NodeInterface $reflectNoteNode */
    $reflectNoteNode = NULL;
    if ($reflectNoteNids) {
      // Found existing reflect note node.
      $reflectNoteNid = reset($reflectNoteNids);
      $reflectNoteNode = $this->entityTypeManager->getStorage('node')
        ->load($reflectNoteNid);
      if (!$reflectNoteNid) {
        throw new AccessDeniedException('Access denied, node missing in saveNote');
      }
      // Delete if the note is MT.
      if (strlen($note) === 0) {
        $reflectNoteNode->delete();
      }
    }
    else {
      // Did not find existing reflect note node.
      if (strlen($note) !== 0) {
        $reflectNoteNode = $this->entityTypeManager
          ->getStorage('node')
          ->create(['type' => SkillingConstants::REFLECT_NOTE_CONTENT_TYPE]);
        $title = $this->t(
          'Reflect note from user @uid node @nid ident @ident',
          [
            '@uid' => $reflectNoteNodeOwnerId,
            '@nid' => $nidNodeWithReflectTag,
            '@ident' => $identifier,
          ]
        );
        $reflectNoteNode->setTitle($title);
        $reflectNoteNode->setOwnerId($reflectNoteNodeOwnerId);
        /* @noinspection PhpUndefinedFieldInspection */
        $reflectNoteNode->field_node->target_id = $nidNodeWithReflectTag;
        /* @noinspection PhpUndefinedFieldInspection */
        $reflectNoteNode->field_internal_name->value = $identifier;
      }
    } // End node not found.
    if (strlen($note) !== 0) {
      // Reflect note node has been loaded or created.
      // Set the note text.
      /* @noinspection PhpUndefinedFieldInspection */
      $reflectNoteNode->field_note->value = $note;
      $reflectNoteNode->save();
    }
    // Record the event.
    $deets = [
      'nid-with-reflect-tag' => $nidNodeWithReflectTag,
      'identifier' => $identifier,
      'note' => $note,
    ];
    // Store event in history?
    // Check whether the history module is active.
    $isHistoryModuleActive =
      $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
    if ($isHistoryModuleActive) {
      // Store the event.
      try {
        $this->historyService->recordStoreReflectNote(
          $nidNodeWithReflectTag, $identifier, $note
        );
      }
      catch (\Exception $e) {
        $message = 'Exception view your class: '
          . $e->getMessage();
        \Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
      }
    }
    // Return JSON response.
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

}
