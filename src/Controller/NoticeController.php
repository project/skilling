<?php

namespace Drupal\skilling\Controller;

use DateTime;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\skilling\CompletionScore;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\Help\HelpConstants;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Render\Renderer;
use Drupal\skilling\History;

/**
 * Show notices.
 */
class NoticeController extends ControllerBase {

  const EXERCISE_SUBMISSION_LINK_TEXT = 'Submit solution';
  const NOT_STUDENT_MESSAGE = 'If you were a student... config.';

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserService;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var SkillingUtilities
   */
  protected $skillingUtilities;

  /**
   * @var ConfigFactory
   */
  protected $configFactory;

  /**
   * @var Renderer
   */
  protected $renderer;

  /**
   * The Skilling history event recorder.
   *
   * @var \Drupal\skilling\History
   */
//  protected $historyService;

  /**
   * The completion score service.
   *
   * @var \Drupal\skilling\CompletionScore
   */
  protected $completionScoreService;

  /**
   * Constructs a new SubmissionController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\Utilities $skillingUtilities
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   * @param \Drupal\Core\Render\Renderer $renderer
   * @param \Drupal\skilling\SkillingCurrentUser $currentUserService
   *   The current user service.
   * @param \Drupal\skilling\History $historyService
   * @param \Drupal\skilling\CompletionScore $completionScoreService
   *   The completion score service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingUtilities $skillingUtilities,
    ConfigFactory $configFactory,
    Renderer $renderer,
    SkillingCurrentUser $currentUserService,
//    History $historyService,
    CompletionScore $completionScoreService
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->configFactory = $configFactory;
    $this->renderer = $renderer;
    $this->currentUserService = $currentUserService;
//    $this->historyService = $historyService;
    $this->completionScoreService = $completionScoreService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('skilling.skilling_current_user'),
//      $container->get('skilling.history'),
      $container->get('skilling.completion_score')
    );
  }

  /**
   * Show a student his/her own submissions.
   *
   * The code loads and shows a view, rather than let Views handle the
   * route and menu item itself. The view uses a contextual filter for
   * the user the submissions are from. The filter uses a default argument
   * of the current user's uid. Handling the route here prevents
   * users from appending a uid to the URL, and getting another user's
   * submission.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Renderable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function showNotices() {
    if (!$this->currentUserService->isAuthenticated()) {
      return [
        '#markup' => $this->t('Sorry, this is for logged in users only.'),
      ];
    }
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('notices');
    $result['view'] = $this->t('Notices attachment error.');
    if (is_object($view)) {
      $args = [$this->currentUserService->id()];
      $view->setDisplay('notices_embed');
      $view->setArguments($args);
      $view->preExecute();
      $view->execute();
      $result['view'] = $view->buildRenderable('notices_embed');
      // Record the event.
//      $this->historyService->createHistoryNode(
//        t('View notices list'), SkillingConstants::HISTORY_EVENT_VIEW_NOTICES_LIST,
//        $this->currentUserService->id(), [], ''
//      );
    }
    return $result;
  }

  /**
   * Mark all notices as read for the current user.
   *
   * @return string[]
   *   Message about number of notices changed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function markAllNoticesAsRead() {
    $count = 0;
    if ($this->currentUserService->isAuthenticated()) {
      if (!$this->currentUserService->isBlocked()) {
        $nids = $this->entityTypeManager->getStorage('node')
          ->getQuery()
          ->condition('type', SkillingConstants::NOTICE_CONTENT_TYPE)
          // For the current user.
          ->condition('uid', $this->currentUserService->id())
          // Published.
          ->condition('status', 1)
          // No read date.
          ->notExists('field_when_read')
          ->execute();
        $count = count($nids);
        if ($count > 0) {
          $notices = $this->entityTypeManager->getStorage('node')
            ->loadMultiple($nids);
          $now = new DateTime();
          $formattedNow = $now->format('Y-m-d\TH:i:s');
          foreach ($notices as $notice) {
            $notice->field_when_read->value = $formattedNow;
            $notice->save();
          }
        }
      }
    }
    if ($count === 0) {
      $message = $this->t('No unread messages marked.');
    }
    elseif ($count === 1) {
      $message = $this->t('Marked one message as read.');
    }
    else {
      $message = $this->t(
        'Marked @c messages as read.',
        ['@c' => $count]);
    }
    $result = [
      '#markup' => $message,
    ];
    return $result;
  }

}
