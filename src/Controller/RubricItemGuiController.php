<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Drupal\skilling\SkillingConstants;

/**
 * Class RubricItemGuiController.
 */
class RubricItemGuiController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserService;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * CSRF token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfTokenGeneratorService;

  /**
   * Constructs a new RubricItemGuiController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\SkillingCurrentUser $currentUserService
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfTokenGeneratorService
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $currentUserService,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    CsrfTokenGenerator $csrfTokenGeneratorService
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUserService = $currentUserService;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->csrfTokenGeneratorService = $csrfTokenGeneratorService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.ajax_security'),
      $container->get('csrf_token')
    );
  }

  /**
   * Load initial data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return string
   *   Return Hello string.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadInitialData(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/rubric-items/load']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    $isAllowed = $this->currentUserService->isAuthor()
      || $this->currentUserService->isAdministrator();
    if (!$isAllowed) {
      throw new AccessDeniedException('Access denied');
    }
    $result = [];
    // What is the exercise?
    $exerciseId = $request->query->get('exerciseId');
    if (!is_numeric($exerciseId) ) {
      throw new AccessDeniedException('Access denied');
    }
//    $exerciseIdOrAdd = (int)$exerciseIdOrAdd;
//    $result['exerciseIdOrAdd'] = $exerciseIdOrAdd;
    if ($exerciseId != 0) {
      // Load it.
      /** @var \Drupal\node\NodeInterface $exercise */
      $exercise = $this->entityTypeManager->getStorage('node')
        ->load($exerciseId);
      if (!$exercise) {
        throw new AccessDeniedException('Access denied');
      }
      // Is the node an exercise?
      if ($exercise->bundle() !== SkillingConstants::EXERCISE_CONTENT_TYPE) {
        throw new AccessDeniedException('Access denied');
      }
    }
    //
    // Get all the exercise names.
    $exercisesJson = [];
    $allExerciseIds = $this->entityTypeManager->getStorage('node')
        ->getQuery()
        ->condition('type', SkillingConstants::EXERCISE_CONTENT_TYPE)
        ->execute();
    $allExercises = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($allExerciseIds);
    /** @var \Drupal\node\NodeInterface $oneExercise */
    foreach ($allExercises as $oneExercise) {
      if (!$oneExercise) {
        // In case does not exist.
        continue;
      }
      $exerciseJson = [
        'exerciseId' => (int)$oneExercise->id(),
        'name' => $oneExercise->getTitle(),
      ];
      $exerciseRIIdsJson = [];
      // Extract the RI nids from the reference field.
      $exerciseRIIds = [];
      $exerciseRIIdRecords = $oneExercise->get(SkillingConstants::FIELD_RUBRIC_ITEMS)->getValue();
      foreach($exerciseRIIdRecords as $exerciseRIIdRecord) {
        $exerciseRIIds[] = $exerciseRIIdRecord['target_id'];
      }
      foreach ($exerciseRIIds as $exerciseRIId) {
        // Is rubric item real?
        $rubricItem = $this->entityTypeManager->getStorage('node')
          ->load($exerciseRIId);
        if (!$rubricItem) {
          continue;
        }
        $exerciseRIIdsJson[] = (int)$exerciseRIId;
      }
      $exerciseJson['rubricItemIds'] = $exerciseRIIdsJson;
      $exercisesJson[] = $exerciseJson;
    }
    $result['exercises'] = $exercisesJson;
    // Get the taxonomy terms.
    $taxTermsJson = [];
    /** @var \Drupal\taxonomy\TermStorage $termStorage */
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    /** @var \Drupal\taxonomy\TermInterface $terms */
    $terms = $termStorage->loadTree(SkillingConstants::TAXONOMY_RUBRIC_ITEM_CATEGORIES);
    foreach ($terms as $term) {
      $termObj = $termStorage->load($term->tid);
      // Is it real?
      if (!$termObj) {
        continue;
      }
      $taxTerm = [
        'categoryId' => (int)$termObj->tid->value,
        'name' => $termObj->name->value,
      ];
      $taxTermsJson[] = $taxTerm;
    }
    $result['rubricItemCategories'] = $taxTermsJson;
    // Get the rubric items.
    // Load them all, since they could be added.
    $allRubricItemIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE)
//      ->sort('title')
      ->execute();
    $allRubricItems = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($allRubricItemIds);
    // Remember response options across all items.
    $allResponseOptionsIds = [];
    $result['rubricItems'] = [];
    /** @var \Drupal\node\NodeInterface $rubricItem */
    foreach ($allRubricItems as $rubricItem) {
      // Does it exist?
      if (!$rubricItem) {
        continue;
      }
      // Store the simple fields in a JSON array.
      $rubricItemJson = [
        'rubricItemId' => (int)$rubricItem->id(),
        'name' => $rubricItem->getTitle(),
        'notes' => $rubricItem->get(SkillingConstants::FIELD_NOTES)->value,
      ];
      // Make JSON for the rubric item category ids.
      $categories = $rubricItem->get(SkillingConstants::FIELD_CATEGORIES)->getValue();
      $categoriesJson = [];
      foreach ($categories as $category) {
        $catId = (int)$category['target_id'];
        $categoriesJson[] = $catId;
      }
      $rubricItemJson['categories'] = $categoriesJson;
      // Make JSON for the response options ids.
      $responseOptions = $rubricItem->get(SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES)->getValue();
      $responseOptionsJson = [];
      foreach ($responseOptions as $responseOption) {
        $respOpId = (int)$responseOption['target_id'];
        // Does it exist?
        if (!$respOpId) {
          continue;
        }
        $responseOption = $this->entityTypeManager->getStorage('paragraph')
          ->load($respOpId);
        if (!$responseOption) {
          continue;
        }
        $responseOptionsJson[] = $respOpId;
        // Store in RO array for all rubric items.
        if (!in_array($respOpId, $allResponseOptionsIds)) {
          $allResponseOptionsIds[] = $respOpId;
        }
      }
      $rubricItemJson['responseOptionIds'] = $responseOptionsJson;
      // Add this item to the JSON array.
      $result['rubricItems'][] = $rubricItemJson;
    } // End for each rubric item.
    // Get response option data.
    $responseOptionsJson = [];
    foreach ($allResponseOptionsIds as $responseOptionId) {
      /** @var \Drupal\paragraphs\Entity\Paragraph $responseOption */
      $responseOption = $this->entityTypeManager->getStorage('paragraph')
        ->load($responseOptionId);
      // Is it real?
      if (!$responseOption) {
        continue;
      }
      if ($responseOption->bundle() != SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE) {
        continue;
      }
      $responseOptionJson = [
        'responseOptionId' => (int)$responseOptionId,
        'name' => $responseOption->get(SkillingConstants::FIELD_RESPONSE_TO_STUDENT)->value,
        'notes' => $responseOption->get(SkillingConstants::FIELD_NOTES)->value,
        'completes' =>
          $responseOption->get(SkillingConstants::FIELD_COMPLETES_RUBRIC_ITEM)->value == 1,
      ];
      // Get response option exercises applies to.
      $appliesToExercises = $responseOption
        ->get(SkillingConstants::FIELD_EXERCISES)->getValue();
      $appliesToExerciseJson = [];
      foreach($appliesToExercises as $appliesToExercise) {
        $targetId = $appliesToExercise['target_id'];
        // Does it exist?
        $exercise = $this->entityTypeManager->getStorage('node')
          ->load($exerciseId);
        if (!$exercise) {
          continue;
        }
        $appliesToExerciseJson[] = $targetId;
      }
      $responseOptionJson['appliesToExerciseIds'] = $appliesToExerciseJson;
      $responseOptionsJson[] = $responseOptionJson;
    }
    $result['responseOptions'] = $responseOptionsJson;
    return new JsonResponse($result);
  }

}
