<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;

/**
 * Class ListsController.
 */
class ListsController extends ControllerBase {

  /**
   * Drupal\Core\Menu\MenuLinkTreeInterface definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new ListsController object.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTreeIn
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClassIn
   */
  public function __construct(
    MenuLinkTreeInterface $menuLinkTreeIn,
    SkillingCurrentClass $currentClassIn
  ) {
    $this->menuLinkTree = $menuLinkTreeIn;
    $this->currentClass = $currentClassIn;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('menu.link_tree'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * Show lists.
   *
   * @see https://drupal.stackexchange.com/questions/224593/how-to-display-a-block-with-menu-child-items-programmatically
   *
   * @return array
   *   Render array.
   */
  public function showLists() {
    $introRenderable = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Here are lists of interesting things.'),
    ];
    $output = $this->makeListFromSubmenu('main', 'Lists', $introRenderable);
    return $output;
  }


  /**
   * Create exercise list. Render an embedded display of a view. Which one
   * depends on whether there is a current class.
   *
   * @return array
   */
  public function showExercisesList() {
    $result = [];
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('exercises');
    $result['view'] = $this->t('Exercises view embed error.');
    if (is_object($view)) {
      $display = $this->currentClass->isCurrentClass()
        ? 'with_current_class'
        : 'no_current_class';  // User has no current class.
      $view->setDisplay($display);
      $view->preExecute();
      $view->execute();
      $result['view'] = $view->buildRenderable($display);
    }
    return $result;
  }

  /**
   * Show lists.
   *
   * @see https://drupal.stackexchange.com/questions/224593/how-to-display-a-block-with-menu-child-items-programmatically
   *
   * @return array
   *   Render array.
   */
  public function showYourStuff() {
    $introRenderable = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Here is your stuff.'),
    ];
    $output = $this->makeListFromSubmenu('main', 'Your stuff', $introRenderable);
    return $output;
  }

  public function makeListFromSubmenu($menuName, $submenuTitle, $introRenderable) {
    $output = [];
    $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menuName);
    $parameters->setMinDepth(1);
    $parameters->onlyEnabledLinks();
    $tree = $this->menuLinkTree->load($menuName, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $this->menuLinkTree->transform($tree, $manipulators);
    $list = [];
    // Find the submenu item in the array.
    $listsElement = NULL;
    $listsElementTitleTranslation = $this->t($submenuTitle);
    foreach ($tree as $key=>$item) {
      if (stripos($key, 'inaccessible') === False) {
        $title = $item->link->getTitle();
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        if ($this->t($title) == $listsElementTitleTranslation) {
          $listsElement = $item;
          break;
        }
      }
    }
    if (!is_null($listsElement)) {
      // Found the Lists element.
      foreach ($listsElement->subtree as $item) {
        $title = $item->link->getTitle();
        $url = $item->link->getUrlObject();
        $list[] = Link::fromTextAndUrl($title, $url);
      }
      $output['introduction'] = $introRenderable;
      $output['sections'] = [
        '#theme' => 'item_list',
        '#items' => $list,
      ];
    }
    return $output;
  }

}
