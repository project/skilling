<?php


namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;


class CharacterController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skilling_utilities,
    SkillingCurrentUser $currentUser
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skilling_utilities;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('skilling.skilling_current_user')
    );
  }

//  public function chooseGridCharacter() {
//    // Get character nodes.
//    $storageHandler = $this->entityTypeManager->getStorage('node');
//    $nids = $storageHandler->getQuery()
//      ->condition('type', SkillingConstants::CHARACTER_CONTENT_TYPE)
//      ->execute();
//    $count = count($nids);
//    $nodes = $storageHandler->loadMultiple($nids);
//    // Find the unique captions.
//    $captions = [];
//    foreach ($nodes as $node) {
//      $caption = trim($node->field_caption->value);
//      if (!in_array($caption, $captions)) {
//        $captions[] = $caption;
//      }
//    }
//    // Make array of links for rendering.
//    $characterLinks = [];
//    foreach ($captions as $caption) {
//      $url = Url::fromRoute(
//        'view.characters_grid.characters_grid',
//        ['arg_0' => $caption]
//      )->toString();
//      $characterLinks[] = [
//        'url' => $url,
//        'caption' => $caption
//      ];
//    }
//    $output['characters_links'] = [
//      '#theme' => 'characters_grid',
//      '#characters_links' => $characterLinks,
//    ];
//    return $output;
//  }

}
