<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Renderer;
use Drupal\skilling\Timeline;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CalendarController.
 */
class CalendarController extends ControllerBase {

  /**
   * Renderer service.
   *
   * @var \Drupal\core\Render\Renderer
   */
  protected $renderer;

  /**
   * Timeline service.
   *
   * @var \Drupal\skilling\Timeline
   */
  protected $timelineService;

  /**
   * Constructs a new CalendarController object.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\skilling\Timeline $timelineService
   *   Timeline service.
   */
  public function __construct(Renderer $renderer, Timeline $timelineService) {
    $this->renderer = $renderer;
    $this->timelineService = $timelineService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('renderer'),
      $container->get('skilling.timeline')
    );
  }

  /**
   * Show calendar.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   */
  public function showCalendar() {
    list($problemMessage, $weeksForTemplate) = $this->timelineService->makeTimeline();
    $renderable = [
      '#theme' => 'timeline',
      '#problem_message' => $problemMessage,
      '#weeks' => $weeksForTemplate,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    return $renderable;
  }

}
