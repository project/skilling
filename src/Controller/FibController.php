<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;

/**
 * Class FibController.
 */
class FibController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The Skilling parser, for parsing student solution text.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $parser;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * The module handler service, for testing whether Skilling history is enabled.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $parser
   *   Skilling authoring language parser.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   Ajax security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skilling_current_user,
    SkillingUtilities $skillingUtilities,
    SkillingParser $parser,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->skillingUtilities = $skillingUtilities;
    $this->parser = $parser;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.utilities'),
      $container->get('skilling.skillingparser'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler')
    );
  }

  /**
   * Check student response.
   *
   * Expect in request:
   *   internalName: internal name of the item.
   *   response: What the user typed.
   *
   * Return JSON object:
   *  status: "OK" or "error"
   *  errorMessage: message to show if there was a problem.
   *  match: Was there a match?
   *  responseMessage: Message to show about the response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  public function checkResponse(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/check-fib-response'],
      TRUE
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in FiB::checkResponse');
    }
    // Get the internal name of the MCQ.
    $internalName = $request->get('internalName');
    // Filter with Stripping.
    $internalName = $this->filterInputService->filterUserContent($internalName);
    // Check that FiB with that name exists.
    $fibIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is FiB.
      ->condition('type', SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE)
      // Has the right internal name.
      ->condition(SkillingConstants::FIELD_INTERNAL_NAME, $internalName)
      ->execute();
    if (!$fibIds) {
      $message = 'Bad internal name: ' . Html::escape($internalName);
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Have the id of the FiB.
    $fibId = reset($fibIds);
    // Get the response the student typed.
    $userResponse = $request->get('answer');
    // Filter.
    $userResponse = $this->filterInputService->filterUserContent($userResponse);
    // Load the FiB.
    /** @var \Drupal\node\NodeInterface $fib */
    $fib = $this->entityTypeManager->getStorage('node')->load($fibId);
    // Get the user who owns the data.
    if (!$fib) {
      $message = 'Bad node in FiB::checkResponse';
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Check the user's response.
    /* @noinspection PhpUndefinedFieldInspection */
    $responses = $fib->field_fib_responses;
    /** @var bool $matched Whether the user's text was matched. NOT whether it is right. */
    $matched = FALSE;
    /** @var bool $correct Whether the user's text is right. */
    $correct = FALSE;
    $responseMessage = '';
    foreach ($responses as $response) {
      if ($matched) {
        break;
      }
      $responseParagraph = $response->entity;
      $responseType = $responseParagraph->field_response_type->value;
      if ($responseType === 'number') {
        if (is_numeric($userResponse)) {
          $userNumberResponse = (float) $userResponse;
          $minMatchResponse = $responseParagraph->field_lowest_number->value;
          // Was a min specified?
          if (!$minMatchResponse === '0' && !$minMatchResponse) {
            $message = 'Bad min in FiB::checkResponse, fib id:' . Html::escape($fib->id());
            return $this->skillingUtilities->formatJsonErrorReport($message);
          }
          $minMatchResponse = (float) $minMatchResponse;
          $maxMatchResponse = $responseParagraph->field_highest_number->value;
          if (!$maxMatchResponse === '0' && !$maxMatchResponse) {
            $message = 'Bad max in FiB::checkResponse, fib id:' . Html::escape($fib->id());
            return $this->skillingUtilities->formatJsonErrorReport($message);
          }
          $maxMatchResponse = (float) $maxMatchResponse;
          if ($userNumberResponse >= $minMatchResponse && $userNumberResponse <= $maxMatchResponse) {
            $matched = TRUE;
            $correct = $responseParagraph->field_correct->value ? TRUE : FALSE;
            $responseMessage = $this->parser->parse($responseParagraph->field_explanation->value);
            break;
          }
        } // End userResponse is_numeric().
      } // End responseType is number.
      elseif ($responseType === 'text') {
        $matchTextItems = $responseParagraph->field_matching_text_responses;
        // Case sensitive applies for each response individually, not for
        // entire question.
        $caseSensitive = $responseParagraph->field_case_sensitive->value;
        $userResponseToCheck = $userResponse;
        if (!$caseSensitive) {
          $userResponseToCheck = strtolower($userResponseToCheck);
        }
        foreach ($matchTextItems as $matchTextItem) {
          $matchText = trim($matchTextItem->value);
          if (!$caseSensitive) {
            $matchText = strtolower($matchText);
          }
          if ($userResponseToCheck == $matchText) {
            $matched = TRUE;
            $correct = $responseParagraph->field_correct->value ? TRUE : FALSE;
            $responseMessage = $this->parser->parse($responseParagraph->field_explanation->value);
            break;
          }
        }
      }
      else {
        $message = 'Bad type in FiB::checkResponse: ' . Html::escape($responseType);
        return $this->skillingUtilities->formatJsonErrorReport($message);
      }
    } // End for each response.
    if (!$matched) {
      $correct = FALSE;
      // Send no-match response.
      /* @noinspection PhpUndefinedFieldInspection */
      $responseMessage = $this->parser->parse($fib->field_no_match_response->value);
    }
    if ($this->skillingCurrentUser->isStudent()) {
      // Check whether the history module is active.
      $isHistoryModuleActive =
        $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
      if ($isHistoryModuleActive) {
        /** @var \Drupal\skilling_history\History $historyService */
        $historyService = \Drupal::service(SkillingConstants::HISTORY_SERVICE_NAME);
        // Record the response in history.
        try {
          $historyService->recordFibAnswer($fibId, $internalName, $userResponse);
        }
        catch (\Exception $e) {
          $message = 'Exception FIB history call, FIB nid: ' . Html::escape($fibId)
            . $e->getMessage();
          return $this->skillingUtilities->formatJsonErrorReport($message);
        }
      }
    }
    // Send result of response check to user.
    // Response is escaped, maybe should not be, since it comes from authors.
    $result = [
      'status' => 'OK',
      'match' => $correct,
      'responseMessage' => $responseMessage,
    ];
    return new JsonResponse($result);
  }

  /**
   * Check student response.
   *
   * Expect in request:
   *   internalName: internal name of the item.
   *   response: What the user typed.
   *
   * Return JSON object:
   *  status: "OK" or "error"
   *  errorMessage: message to show if there was a problem.
   *  match: Was there a match?
   *  responseMessage: Message to show about the response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function giveUp(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/fib-give-up'],
      TRUE
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in FiB::giveUp');
    }
    // Get the internal name of the MCQ.
    $internalName = $request->get('internalName');
    // Filter with Stripping.
    $internalName = $this->filterInputService->filterUserContent($internalName);
    // Check that FiB with that name exists.
    $fibIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is FiB.
      ->condition('type', SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE)
      // Has the right internal name.
      ->condition(SkillingConstants::FIELD_INTERNAL_NAME, $internalName)
      ->execute();
    if (!$fibIds) {
      $message = 'Bad internal name: ' . Html::escape($internalName);
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Have the id of the FiB.
    $fibId = reset($fibIds);
    // Load the FiB.
    /** @var \Drupal\node\NodeInterface $fib */
    $fib = $this->entityTypeManager->getStorage('node')->load($fibId);
    // Get the user who owns the data.
    if (!$fib) {
      $message = 'Bad node in FiB::giveUp';
      return $this->skillingUtilities->formatJsonErrorReport($message);
    }
    // Check the user's response.
    /* @noinspection PhpUndefinedFieldInspection */
    $responses = $fib->field_fib_responses;
    /** @var bool $matched Whether found a correct response. */
    $matched = FALSE;
    $answer = $this->t('Could not find the correct answer.');
    foreach ($responses as $response) {
      if ($matched) {
        break;
      }
      $responseParagraph = $response->entity;
      $responseType = $responseParagraph->field_response_type->value;
      // Skip incorrect responses.
      $correct = $responseParagraph->field_correct->value ? TRUE : FALSE;
      if (!$correct) {
        continue;
      }
      if ($responseType === 'number') {
        $minMatchResponse = $responseParagraph->field_lowest_number->value;
        // Was a min specified?
        if (!$minMatchResponse === '0' && !$minMatchResponse) {
          $message = 'Bad min in FiB::checkResponse, fib id:' . Html::escape($fib->id());
          return $this->skillingUtilities->formatJsonErrorReport($message);
        }
        $maxMatchResponse = $responseParagraph->field_highest_number->value;
        if (!$maxMatchResponse === '0' && !$maxMatchResponse) {
          $message = 'Bad max in FiB::checkResponse, fib id:' . Html::escape($fib->id());
          return $this->skillingUtilities->formatJsonErrorReport($message);
        }
        $answer = $this->t(
          'Between @min and @max would be correct',
          [
            '@min' => $minMatchResponse,
            '@max' => $maxMatchResponse,
          ]
        );
        // Flag to show answer found.
        $matched = TRUE;
      } // End responseType is number.
      elseif ($responseType === 'text') {
        /** @var \Drupal\Core\Field\FieldItemList $matchTextItems */
        $matchTextItems = $responseParagraph->field_matching_text_responses;
        if ($matchTextItems->count() > 0) {
          $answer = $matchTextItems->get(0)->value;
          // Flag to show answer found.
          $matched = TRUE;
        }
      }
      else {
        $message = 'Bad type in FiB::checkResponse: ' . Html::escape($responseType);
        return $this->skillingUtilities->formatJsonErrorReport($message);
      }
    } // End for each response.
    if ($this->skillingCurrentUser->isStudent()) {
      // Check whether the history module is active.
      $isHistoryModuleActive =
        $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
      if ($isHistoryModuleActive) {
        /** @var \Drupal\skilling_history\History $historyService */
        $historyService = \Drupal::service(SkillingConstants::HISTORY_SERVICE_NAME);
        // Record the response in history.
        try {
          $historyService->recordFibAnswer($fibId, $internalName, $this->t('Gave up'));
        }
        catch (\Exception $e) {
          $message = 'Exception FIB history call, FIB nid: ' . Html::escape($fibId)
            . $e->getMessage();
          return $this->skillingUtilities->formatJsonErrorReport($message);
        }
      }
    }
    // Send result of response check to user.
    // Response is escaped, maybe should not be, since it comes from authors.
    $result = [
      'status' => 'OK',
      'answer' => $answer,
    ];
    return new JsonResponse($result);
  }

}
