<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Views;

/**
 * Class RubricItemCategoriesController.
 */
class RubricItemCategoriesController extends ControllerBase {

  /**
   * Showrubricitemcategories.
   *
   * @return string
   *   Return Hello string.
   */
  public function showRubricItemCategories() {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('rubric_item_categories');
    $result['rubric_item_categories_view'] = $this->t('Rubric item categories embedding error.');
    if (is_object($view)) {
      $view->setDisplay('rubric_item_categories_report_embed');
      $view->preExecute();
      $view->execute();
      $result['rubric_item_categories_view'] = $view->buildRenderable('rubric_item_categories_report_embed');
    }
    $view = Views::getView('rubric_items_with_no_categories');
    $result['rubric_items_with_no_categories'] = $this->t('Rubric items with no categories embedding error.');
    if (is_object($view)) {
      $view->setDisplay('rubric_items_with_no_categories_report_embed');
      $view->preExecute();
      $view->execute();
      $result['rubric_items_with_no_categories'] = $view->buildRenderable('rubric_items_with_no_categories_report_embed');
    }
    return $result;
  }

}
