<?php

namespace Drupal\skilling\Controller;

use DateInterval;
use DateTime;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\Utilities;

/**
 * Class DaysTestController.
 */
class DaysTestController extends ControllerBase {

  /**
   * Drupal\skilling\Utilities definition.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Constructs a new DaysTestController object.
   */
  public function __construct(Utilities $skilling_utilities) {
    $this->skillingUtilities = $skilling_utilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('skilling.utilities')
    );
  }

  /**
   * Dude.
   *
   * @return string
   *   Return Hello string.
   */
  public function dude() {
    $days = [30, 29, 28, 22, 21, 20, 16, 15, 14, 13, 8, 7, 6, 2, 1,
      0, -1, -2, -6, -7, -8, -13, -14, -15, -16];
//    $days = [1, 0, -1];
//    $days = [-2];
    $now = new DateTime();
    $result = '';
    foreach ($days as $day) {
      $dueDaysInterval = new DateInterval('P' . abs($day) . 'D');
      $exerciseDueDate = clone $now;
      if ($day >= 0) {
        $exerciseDueDate->add($dueDaysInterval);
      }
      else {
        $exerciseDueDate->sub($dueDaysInterval);
      }
      // Trim to midnight.
      $exerciseDueDate = new DateTime($exerciseDueDate->format('Y-m-d'));

      $whenDueDisplay = $this->skillingUtilities->daysDiffNowString($exerciseDueDate);
      $result .= "Day $day is $whenDueDisplay<br>";
    }
    return [
      '#type' => 'markup',
      '#markup' => $result,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
