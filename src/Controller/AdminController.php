<?php

namespace Drupal\skilling\Controller;

/* @noinspection PhpUnusedLocalVariableInspection */
/* @noinspection PhpInconsistentReturnPointsInspection */

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\skilling\CompletionScore;
use Drupal\skilling\Enrollment;
use Drupal\skilling\MakeStarterContent\MakeNodes;
use Drupal\skilling\MakeStarterContent\MakeStarterContent;
use Drupal\skilling\MakeStarterContent\MakeUsers;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * Controller for skilling administration pages.
 */
class AdminController extends ControllerBase {

  // This must be here for a batch job to work.
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $sessionService;

  /**
   * Service that makes starter content.
   *
   * @var \Drupal\skilling\MakeStarterContent\MakeStarterContent
   */
  protected $makeStarterContentService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The completion score service.
   *
   * @var \Drupal\skilling\CompletionScore
   */
  protected $completionScoreService;

  /**
   * The enrollment service.
   *
   * @var \Drupal\skilling\Enrollment
   */
  protected $enrollmentService;

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Constructs a new AdminController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\MakeStarterContent\MakeNodes $makeNodes
   *   Service to make nodes.
   * @param \Drupal\skilling\MakeStarterContent\MakeUsers $makeUsers
   *   Service to make users.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   Symfony session service.
   * @param \Drupal\skilling\MakeStarterContent\MakeStarterContent $makeStarterContentService
   *   Service that makes starter content.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\skilling\CompletionScore $completionScoreService
   *   The completion score service.
   * @param \Drupal\skilling\Enrollment $enrollmentService
   *   The enrollment service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The current user service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skilling_utilities,
    MakeNodes $makeNodes,
    MakeUsers $makeUsers,
    Session $session,
    MakeStarterContent $makeStarterContentService,
    MessengerInterface $messenger,
    CompletionScore $completionScoreService,
    Enrollment $enrollmentService,
    SkillingCurrentUser $currentUser
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skilling_utilities;
    $this->makeNodesService = $makeNodes;
    $this->makeUsersService = $makeUsers;
    $this->sessionService = $session;
    $this->makeStarterContentService = $makeStarterContentService;
    $this->messenger = $messenger;
    $this->completionScoreService = $completionScoreService;
    $this->enrollmentService = $enrollmentService;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('skilling.starter.make_nodes'),
      $container->get('skilling.starter.make_users'),
      $container->get('session'),
      $container->get('skilling.starter.make_starter_content'),
      $container->get('messenger'),
      $container->get('skilling.completion_score'),
      $container->get('skilling.enrollment'),
      $container->get('skilling.skilling_current_user')
    );
  }

  /**
   * Launch delete history.
   *
   * @return array
   *   Return Hello string.
   */
  public function launchDeleteHistory() {
    $build['explanation'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'You can delete all of the student history records. This action cannot be undone.'
      ),
    ];
    return $build;
  }

  /**
   * Delete history.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Where to go.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function deleteHistory() {
    $storageHandler = $this->entityTypeManager->getStorage('node');
    $nids = $storageHandler->getQuery()
      ->condition('type', SkillingConstants::HISTORY_CONTENT_TYPE)
      ->execute();
    $count = count($nids);
    $nodes = $storageHandler->loadMultiple($nids);
    $storageHandler->delete($nodes);
    $this->messenger->addStatus(
      $this->t('History deleted: @n events.', ['@n' => $count])
    );
    return $this->redirect('skilling.admin.config.delete_history');
  }

  /**
   * Trigger a batch job that recomputes all completion scores.
   *
   * Scores for all classes. Redirect to submissions admin menu.
   *
   * ARGH! This method cannot call another method to do its work.
   *
   * function calledByRoute() {
   *   $this->bee();
   * }
   * function bee() {
   *   make batch job
   *
   * This FAILS!
   *
   * Must be:
   *
   * function calledByRoute() {
   *   make batch job
   *
   * And don't forget:
   *
   * use DependencySerializationTrait;
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function recomputeCompletionScoresAllClasses() {
    $returnUrl = Url::fromRoute('skilling.admin.submissions');
    // Find student enrollments.
    $enrollmentIds = $this->enrollmentService->getStudentEnrollmentIdsForAllClasses();
    if (count($enrollmentIds) === 0) {
      $this->messenger->addStatus($this->t('No scores to update.'));
      return new RedirectResponse(Url::fromRoute('skilling.admin.submissions'));
    }
    // Take the site offline.
    // Couldn't bring the site back in batch finish method.
    // \Drupal::state()->set('system.maintenance_mode', TRUE);
    // Make a BatchBuilder.
    $batchBuilder = (new BatchBuilder())
      ->setTitle($this->t('Updating completion scores'))
      ->setErrorMessage($this->t('Problem updating completion scores.'))
      ->setFinishCallback([$this, 'doneUpdatingCompletionScores']);
    // Add an operation for each student enrollment.
    foreach ($enrollmentIds as $enrollmentId) {
      $batchBuilder->addOperation([$this, 'updateCompletionScoreBatchStep'], [$enrollmentId]);
    }
    // Start the batch job.
    batch_set($batchBuilder->toArray());
    return batch_process($returnUrl);
  }

  /**
   * Trigger a batch job that recomputes completion scores for a class.
   *
   * Redirect to submissions admin menu.
   *
   * ARGH! This method cannot call another method to do its work.
   *
   * function calledByRoute() {
   *   $this->>bee();
   * }
   * function bee() {
   *   make batch job
   *
   * This FAILS!
   *
   * Must be:
   *
   * function calledByRoute() {
   *   make batch job
   *
   * And don't forget:
   *
   * use DependencySerializationTrait;
   *
   * @param int $classId
   *   The class id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Not sure what this is.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function recomputeCompletionScoresOneClass(int $classId) {
    // Does the current user have instructor access to the class?
    if (!$this->currentUser->isInstructorOfClassNid($classId)) {
      throw new AccessDeniedException('Access denied');
    }
    $returnUrl = Url::fromRoute('view.class_progress.progress', ['arg_0' => $classId]);
    // Find student enrollments.
    $enrollmentIds = $this->enrollmentService->getStudentEnrollmentIdsForClassId($classId);
    if (count($enrollmentIds) === 0) {
      $this->messenger->addStatus($this->t('No scores to update.'));
      return new RedirectResponse($returnUrl);
    }
    // Make a BatchBuilder.
    $batchBuilder = (new BatchBuilder())
      ->setTitle($this->t('Updating completion scores'))
      ->setErrorMessage($this->t('Problem updating completion scores.'))
      ->setFinishCallback([$this, 'doneUpdatingCompletionScores']);
    // Add an operation for each student enrollment.
    foreach ($enrollmentIds as $enrollmentId) {
      $batchBuilder->addOperation([$this, 'updateCompletionScoreBatchStep'], [$enrollmentId]);
    }
    // Start the batch job.
    batch_set($batchBuilder->toArray());
    return batch_process($returnUrl);
  }

  /**
   * Run a batch step, updating a completion score for one student.
   *
   * @param int $enrollmentId
   *   Student id.
   * @param array $context
   *   Batch context.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function updateCompletionScoreBatchStep($enrollmentId, array &$context) {
    /** @var \Drupal\node\NodeInterface $enrollment */
    $enrollment = $this->entityTypeManager->getStorage('node')
      ->load($enrollmentId);
    if (!$enrollment) {
      $this->messenger->addWarning('Check the message log for a Strange Thing.');
      $message = $this->t(
        'Strange Thing: updateCompletionScoreBatchStep: expected enrollment @id not found.',
        ['@id' => $enrollmentId]
      );
      $context['message'] = $message;
      \Drupal::logger(SkillingConstants::MODULE_NAME)->notice($message);
      return;
    }
    $context['message'] = $this->t(
      'Updating for enrollment @title.',
      ['@id' => $enrollment->getTitle()]
    );
    $this->completionScoreService->updateEnrollmentCompletionScore($enrollment);
  }

  /**
   * Run when done updating completion scores.
   */
  public function doneUpdatingCompletionScores() {
    // Bring the site online.
    // This didn't work.
    // \Drupal::state()->set('system.maintenance_mode', FALSE);
    // Show message.
    $this->messenger->addStatus($this->t('Updated student completion scores.'));
  }

}
