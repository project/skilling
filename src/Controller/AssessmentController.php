<?php

namespace Drupal\skilling\Controller;

use DateTime;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Utility\Token;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\Access\SkillingCheckUserRelationships;
use Drupal\skilling\Assessment;
use Drupal\skilling\Badging;
use Drupal\skilling\Notice;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingParser\SkillingParser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling_history\History;
use Drupal\skilling_history\SkillingHistoryConstants;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Config\ConfigFactory;


/**
 * Ajax interaction with grading interface.
 */
class AssessmentController extends ControllerBase {

  const PATH_TO_ASSESSMENT_INTERFACE = 'libraries/feedback/feedback.html';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * Service with various helpers for assessment.
   *
   * @var \Drupal\skilling\Assessment
   */
  protected $assessmentService;

  /**
   * The history service.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * User relationship service.
   *
   * @var \Drupal\skilling\Access\SkillingCheckUserRelationships
   */
  protected $userRelationshipService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $sessionService;

  /**
   * CSRF token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfTokenGeneratorService;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * The parser service.
   *
   * @var \Drupal\skilling\SkillingParser\SkillingParser
   */
  protected $skillingParser;

  /** The badging service.
   *
   * @var \Drupal\skilling\Badging
   */
  protected $badgingService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The notice service.
   *
   * @var \Drupal\skilling\Notice
   */
  protected $noticeService;

  /**
   * @var Renderer
   */
  protected $renderer;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Overall evaluations graders can give.
   *
   * @var array
   */
  const VALID_EVALUATIONS = ['good', 'needs work', 'poor'];

  /**
   * Constructs a new AssessmentController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   The Skilling utilities service.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   Date formatter service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   Current user service.
   * @param \Drupal\skilling\Assessment $assessment
   *   Service with various helpers for assessment.
   * @param \Drupal\skilling_history\History $historyService
   *   The history service.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\Access\SkillingCheckUserRelationships $userRelationshipService
   *   User relationship service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   Session service.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfTokenGeneratorService
   *   CSRF token generator service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   The user factory service.
   * @param \Drupal\skilling\SkillingParser\SkillingParser $skillingParser
   * @param \Drupal\skilling\Badging $badgingService
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   * @param \Drupal\skilling\Notice $noticeService
   * @param \Drupal\Core\Render\Renderer $renderer
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skilling_utilities,
    DateFormatter $date_formatter,
    SkillingCurrentUser $currentUser,
    Assessment $assessment,
    History $historyService,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    SkillingCheckUserRelationships $userRelationshipService,
    FilterUserInputInterface $filterInputService,
    Session $session,
    CsrfTokenGenerator $csrfTokenGeneratorService,
    Token $token,
    SkillingUserFactory $skillingUserFactory,
    SkillingParser $skillingParser,
    Badging $badgingService,
    ModuleHandlerInterface $moduleHandler,
    Notice $noticeService,
    Renderer $renderer,
    ConfigFactory $configFactory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skilling_utilities;
    $this->currentUser = $currentUser;
    $this->assessmentService = $assessment;
    $this->dateFormatter = $date_formatter;
    $this->historyService = $historyService;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->userRelationshipService = $userRelationshipService;
    $this->filterInputService = $filterInputService;
    $this->sessionService = $session;
    $this->csrfTokenGeneratorService = $csrfTokenGeneratorService;
    $this->tokenService = $token;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->skillingParser = $skillingParser;
    $this->badgingService = $badgingService;
    $this->moduleHandler = $moduleHandler;
    $this->noticeService = $noticeService;
    $this->renderer = $renderer;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('date.formatter'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.assessment'),
      $container->get('skilling_history.history'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.check_user_relationships'),
      $container->get('skilling.filter_user_input'),
      $container->get('session'),
      $container->get('csrf_token'),
      $container->get('token'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('skilling.skillingparser'),
      $container->get('skilling.badging'),
      $container->get('module_handler'),
      $container->get('skilling.notice'),
      $container->get('renderer'),
      $container->get('config.factory')
    );
  }

  /**
   * Make a link to start the grading interface.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function startGradingInterface() {
    $build = [];
    $build['start_grading_link'] = $this->computeAssessmentLink();
    return $build;
  }

  /**
   * Return render array element for grading link.
   *
   * MT array if there is nothing to show.
   *
   * @return array
   *   Render array element for grading link.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function computeAssessmentLink() {
    // Assume nothing to grade.
    $build = [];
    if (!$this->currentUser->isGrader()) {
      $build['problem'] = [
        '#markup' => $this->t('Sorry, only available to graders.'),
      ];
      return $build;
    }
    $waitingSubmissionCount = $this->assessmentService->computeNumSubmissionsWaitingAssessment();
    // Problem?
    if (is_null($waitingSubmissionCount)) {
      $build['problem'] = [
        '#markup' => $this->t('Sorry, could not count number of waiting submissions.'),
      ];
      $build['suggestion'] = [
        '#markup' => SkillingConstants::getErrorSuggestion(),
      ];
      return $build;
    }
    // Anything waiting?
    if ($waitingSubmissionCount === 0) {
      // No.
      $build = [
        '#markup' => $this->t('You have nothing to grade.'),
        '#cache' => ['max-age' => 0],
      ];
    }
    else {
      // User has things to grade.
      $assessmentInterfaceUrl
        = $this->skillingUtilities->getModuleUrlPath() . self::PATH_TO_ASSESSMENT_INTERFACE;
      $linkText = ($waitingSubmissionCount === 1)
        ? $this->t('one submission')
        : $this->t('@s submissions', ['@s' => $waitingSubmissionCount]);
      $sessionId = $this->sessionService->getId();
      $csrfToken = $this->csrfTokenGeneratorService->get();
      $build = [
        '#type' => 'container',
        '#attributes' => ['id' => 'start-assessing-wrapper'],
        // Not cachable.
        '#cache' => [
          'max-age' => 0,
        ],
        '#attached' => [
          'library' => 'skilling/start-grading',
          'drupalSettings' => [
            'sessionId' => $sessionId,
            'assessmentInterfaceUrl' => $assessmentInterfaceUrl,
            'csrfToken' => $csrfToken,
          ],
        ],
        'start_grading_link' => [
          '#markup' => $this->t(
            'You have <a href="#" id="start-grading">@lt</a> to grade.',
            ['@lt' => $linkText]
          ),
        ],
      ];
    }
    return $build;
  }

  /**
   * Load and return grader persona data.
   *
   * Security note: No client input apart from the GET. Server data sent
   * to client originates from graders (or defaults from installation).
   * Data entered through Drupal form system, using Stripped.
   *
   * Stripped is used again in this method, before sending data to the client.
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response with persona data.
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   */
  public function getGraderPersona(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-grader-persona']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $result = [
      'greetings' => [],
      'signatures' => [],
      'summaryGood' => [],
      'summaryNeedsWork' => [],
      'summaryPoor' => [],
    ];
    $drupalUser = $this->currentUser->getDrupalUser();
    /* @noinspection PhpUndefinedFieldInspection */
    $greetings = $drupalUser->field_feedback_greetings->getValue();
    foreach ($greetings as $greeting) {
      $text = $this->filterInputService->filterUserContent($greeting['value']);
      $text = $this->tokenService->replace($text);
      $result['greetings'][] = $text;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $signatures = $drupalUser->field_feedback_signatures->getValue();
    foreach ($signatures as $signature) {
      $text = $this->filterInputService->filterUserContent($signature['value']);
      $text = $this->tokenService->replace($text);
      $result['signatures'][] = $text;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $summariesGood = $drupalUser->field_feedback_summary_good->getValue();
    foreach ($summariesGood as $summaryGood) {
      $text = $this->filterInputService->filterUserContent($summaryGood['value']);
      $text = $this->tokenService->replace($text);
      $result['summaryGood'][] = $text;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $summariesNeedsWork = $drupalUser->field_feedback_summary_needs_wor->getValue();
    foreach ($summariesNeedsWork as $summaryNeedsWork) {
      $text = $this->filterInputService->filterUserContent($summaryNeedsWork['value']);
      $text = $this->tokenService->replace($text);
      $result['summaryNeedsWork'][] = $text;
    }
    /* @noinspection PhpUndefinedFieldInspection */
    $summariesPoor = $drupalUser->field_feedback_summary_poor->getValue();
    foreach ($summariesPoor as $summaryPoor) {
      $text = $this->filterInputService->filterUserContent($summaryPoor['value']);
      $text = $this->tokenService->replace($text);
      $result['summaryPoor'][] = $text;
    }
    return new JsonResponse($result);
  }

  /**
   * Get submissions for grader.
   *
   * Security note: No client input apart from the GET. Server data sent
   * to client computed in getUngradedSubmissionsForClasses().
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Submissions to grade.
   *
   * @see getUngradedSubmissionsForClasses
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSubmissionListForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-submissions-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $graderClassNids = $this->currentUser->getClassNidsForGrader();
    $result = $this->assessmentService->getUngradedSubmissionsForClasses(
      $graderClassNids
    );
    return new JsonResponse($result);
  }

  /**
   * Get class list for grader.
   *
   * Security note: No client input apart from the GET. Server data sent
   * to client computed in getClassesForGrader().
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Classes.
   *
   * @see getClassesForGrader
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassListForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-class-list-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $graderClassNids = $this->currentUser->getClassNidsForGrader();
    $result = $this->assessmentService->getClassesForGrader($graderClassNids);
    return new JsonResponse($result);
  }

  /**
   * Get student list for grader.
   *
   * Security notes:
   *
   * This method get a list of student ids from the client.
   *
   * - Students ids are scanned to make sure they are all numeric.
   * - Each student id is checked to make sure the current user is a
   *   grader of that student.
   *
   * The method returns data about students. Names are returned only if the
   * current user has the instructor role, and is an instructor of that student.
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Student data.
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function getStudentListForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-student-list-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $studentIds = $request->query->get('studentIds');
    // Should be array.
    if (!is_array($studentIds)) {
      throw new AccessDeniedException('Access denied');
    }
    // Every element should be numeric.
    $allNumeric = TRUE;
    foreach ($studentIds as $studentId) {
      if (!is_numeric($studentId)) {
        $allNumeric = FALSE;
        break;
      }
    }
    if (!$allNumeric) {
      throw new AccessDeniedException('Access denied');
    }
    // Make sure current user is grader of students with given ids.
    $isGraderOfStudent = TRUE;
    foreach ($studentIds as $studentId) {
      $grader = $this->userRelationshipService->isUserUidGraderOfUserUid(
        $this->currentUser->id(),
        $studentId
      );
      if (!$grader) {
        $isGraderOfStudent = FALSE;
        break;
      }
    }
    if (!$isGraderOfStudent) {
      throw new AccessDeniedException('Access denied');
    }
    $result = [];
    if (count($studentIds) > 0) {
      $students = $this->entityTypeManager->getStorage('user')
        ->loadMultiple($studentIds);
      // Load the published enrollments for the students.
      $enrollmentQuery = $this->entityTypeManager
        ->getStorage('node')
        ->getQuery()
        ->condition('type', 'enrollment')
        // Enrollment is published.
        ->condition('status', TRUE)
        // For the users.
        ->condition('field_user', $studentIds, 'IN')
        // Have student role.
        ->condition('field_class_roles', ['student'], 'IN')
        // User is not blocked.
        ->condition('field_user.entity.status', 1)
        // Class is published.
        ->condition('field_class.entity.status', 1);
      $enrollmentIds = $enrollmentQuery->execute();
      $enrollments = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($enrollmentIds);
      // Make array with student id as key, value is array of
      // classes the student is enrolled in.
      $enrolledClasses = [];
      /** @var \Drupal\node\Entity\Node $enrollment */
      foreach ($enrollments as $enrollment) {
        /* @noinspection PhpUndefinedFieldInspection */
        $studentId = $enrollment->field_user->target_id;
        /* @noinspection PhpUndefinedFieldInspection */
        $classId = $enrollment->field_class->target_id;
        if (!isset($enrolledClasses[$studentId])) {
          $enrolledClasses[$studentId] = [];
        }
        // Does the grader have access to the class?
        $isGraderForClass = $this->currentUser->isGraderOfClassNid($classId);
        if ($isGraderForClass) {
          $enrolledClasses[$studentId][] = $classId;
        }
      }
      // Prep data for sending to client.
      /** @var \Drupal\user\Entity\User $student */
      foreach ($students as $student) {
        // Skip students there is no enrollment record for.
        // Should not happen because of logic above.
        if (isset($enrolledClasses[$student->id()])) {
          $isCanSeeNames = $this->getIsCanSeeNames($student);
          if ($isCanSeeNames) {
            // Filter content, just in case.
            /* @noinspection PhpUndefinedFieldInspection */
            $firstName = $this->filterInputService->filterUserContent($student->field_first_name->value);
            /* @noinspection PhpUndefinedFieldInspection */
            $lastName = $this->filterInputService->filterUserContent($student->field_last_name->value);
          }
          else {
            $firstName = $this->t('(Name withheld)');
            $lastName = $this->t('(Name withheld)');
          }
          $result[$student->id()] = [
            'studentId' => $student->id(),
            'firstName' => $firstName,
            'lastName' => $lastName,
            'classesStudentIsIn' => $enrolledClasses[$student->id()],
            'isCanSeeNames' => $isCanSeeNames,
          ];
        }
      }
    }
    return new JsonResponse($result);
  }

  /**
   * Can the current user see the names of a student?
   *
   * @param \Drupal\user\Entity\User $student
   *   The student.
   *
   * @return bool
   *   True if allowed, else false.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  protected function getIsCanSeeNames(User $student) {
    // Only graders who are also instructors can see student names.
    $instructorOfStudent = $this->userRelationshipService->isUserUidInstructorOfUserUid(
      $this->currentUser->id(), $student->id()
    );
    $graderOfStudent = $this->userRelationshipService->isUserUidGraderOfUserUid(
      $this->currentUser->id(), $student->id()
    );
    // Can graders see names?
    $settings = $this->configFactory->get(SkillingConstants::SETTINGS_MAIN_KEY);
    $gradersSeeStudentNames = $settings->get(SkillingConstants::SETTING_KEY_GRADERS_SEE_STUDENT_NAMES);
    // Flag.
    $isCanSeeNames = FALSE;
    if ($instructorOfStudent) {
      $isCanSeeNames = TRUE;
    }
    if ($graderOfStudent && $gradersSeeStudentNames) {
      $isCanSeeNames = TRUE;
    }
    return $isCanSeeNames;
  }

  /**
   * Get grader data for grader interface.
   *
   * Security notes. No input from client, except for GET.
   *
   * Output to client includes data from graders. Filter it.
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Grader data.
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   */
  public function getGraderForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-grader-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $drupalUser = $this->currentUser->getDrupalUser();
    /* @noinspection PhpUndefinedFieldInspection */
    $result = [
      'graderId' => $drupalUser->id(),
      'firstName' => $this->filterInputService->filterUserContent($drupalUser->field_first_name->value),
      'lastName' => $this->filterInputService->filterUserContent($drupalUser->field_last_name->value),
    ];
    return new JsonResponse($result);
  }

  /**
   * Get exercises for grader.
   *
   * Security notes. The method gets ids from the client.
   * Make sure they are numeric, and all are ids for exercises.
   * Also check that the number of exercises retrieved from Drupal
   * is the same as the number of ids passed.
   *
   * All data returned from this method to the client comes from
   * authors. Still, in the context of the grading client, extra
   * tags added into exercise fields are unlikely to be functionally
   * relevant. Therefore, content is filtered using Stripped.
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Exercise data.
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getExercisesForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-exercises-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $exerciseIds = $request->query->get('exerciseIds');
    if (!$exerciseIds) {
      throw new AccessDeniedException('Access denied');
    }
    // Every element should be numeric.
    $allNumeric = TRUE;
    foreach ($exerciseIds as $exerciseId) {
      if (!is_numeric($exerciseId)) {
        $allNumeric = FALSE;
        break;
      }
    }
    if (!$allNumeric) {
      throw new AccessDeniedException('Access denied');
    }
    $result = [];
    if (count($exerciseIds) > 0) {
      /** @var \Drupal\skilling\Exercises $exercises */
      $exercises = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($exerciseIds);
      // Check that we got the expected number of exercises from the DB.
      if (count($exercises) !== count($exerciseIds)) {
        throw new AccessDeniedException('Access denied');
      }
      // Make sure that every entity is an exercise.
      $allExercises = TRUE;
      /** @var \Drupal\Node\NodeInterface $exercise */
      foreach ($exercises as $exercise) {
        if ($exercise->bundle() !== SkillingConstants::EXERCISE_CONTENT_TYPE) {
          $allExercises = FALSE;
        }
      }
      if (!$allExercises) {
        throw new AccessDeniedException('Access denied');
      }
      /** @var \Drupal\node\Entity\Node $exercise */
      foreach ($exercises as $exercise) {
        $rubricItemIds = [];
        /* @noinspection PhpUndefinedFieldInspection */
        foreach ($exercise->field_rubric_items as $rubricItem) {
          $rubricItemIds[] = $rubricItem->target_id;
        }
        $bodyToShow = $exercise->body->value;
        $titleToShow = $this->filterInputService->filterUserContent($exercise->title->value);
        if (!$exercise->isPublished()) {
          $titleToShow .= (string)($this->t(" (U)"));
        }
        /* @noinspection PhpUndefinedFieldInspection */
        $result[$exercise->id()] = [
          'exerciseId' => $exercise->id(),
          'published' => $exercise->isPublished(),
          'title' => $titleToShow,
          'description' => $this->filterInputService->filterUserContent($bodyToShow),
//          'description' => $this->filterInputService->filterUserContent($exercise->body->value),
          'notes' => $this->filterInputService->filterUserContent($exercise->field_notes->value),
          'rubricItemIds' => $rubricItemIds,
        ];
      }
    }
    return new JsonResponse($result);
  }

  /**
   * Get rubric items for grader.
   *
   * Security notes. This method gets an array of ids from the client.
   * Check that all are numeric, and correspond to rubric item ids. Also
   * check that the number of items retrieved from Drupal matches the number
   * that is expected.
   *
   * All of the content the method sends to the client originates from authors.
   * Apply Stripped to it anyway, because... reasons.
   *
   * This is not a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Rubric item data.
   *
   * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
   * for HTTP methods for state changing operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRubricItemsForGrader(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['GET'],
      ['/skilling/get-rubric-items-for-grader']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    // Get rubric item ids from request.
    $rubricItemIds = $request->query->get('rubricItemIds');
    if (!$rubricItemIds) {
      throw new AccessDeniedException('Access denied');
    }
    // Every element should be numeric.
    $allNumeric = TRUE;
    foreach ($rubricItemIds as $rubricItemId) {
      if (!is_numeric($rubricItemId)) {
        $allNumeric = FALSE;
        break;
      }
    }
    if (!$allNumeric) {
      throw new AccessDeniedException('Access denied');
    }
    $resultRIs = [];
    $resultRIOptions = [];
    if (count($rubricItemIds) > 0) {
      // Load rubric items from ids.
      $rubricItems = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($rubricItemIds);
      // We should have a known number of items.
      if (count($rubricItems) !== count($rubricItems)) {
        throw new AccessDeniedException('Access denied');
      }
      // Make sure that every entity is a rubric item.
      $allRubricItems = TRUE;
      /** @var \Drupal\Node\NodeInterface $rubricItem */
      foreach ($rubricItems as $rubricItem) {
        if ($rubricItem->bundle() !== SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE) {
          $allRubricItems = FALSE;
        }
      }
      if (!$allRubricItems) {
        throw new AccessDeniedException('Access denied');
      }
      /** @var \Drupal\node\Entity\Node $rubricItem */
      foreach ($rubricItems as $rubricItem) {
        // Load rubric item responses for the rubric item.
        $rubricItemResponseIds = [];
        /* @noinspection PhpUndefinedFieldInspection */
        $responseParagraphList = $rubricItem->field_rubric_item_responses->getValue();
        foreach ($responseParagraphList as $responseRef) {
          $rubricItemResponseIds[] = $responseRef['target_id'];
        }
        // Load the responses.
        $rubricItemResponses = $this->entityTypeManager
          ->getStorage('paragraph')
          ->loadMultiple($rubricItemResponseIds);
        // Make a rep of the rubric item for transfer.
        /* @noinspection PhpUndefinedFieldInspection */
        $ri = [
          'rubricItemId' => $rubricItem->id(),
          'title' => $this->filterInputService->filterUserContent($rubricItem->getTitle()),
          'notes' => $this->filterInputService->filterUserContent($rubricItem->field_notes->value),
          'responseOptionIds' => $rubricItemResponseIds,
        ];
        // Add RI to results.
        $resultRIs[$rubricItem->id()] = $ri;
        // Make a rep of the rubric item response option for transfer.
        /** @var \Drupal\paragraphs\ParagraphInterface $rubricItemResponse */
        foreach ($rubricItemResponses as $rubricItemResponse) {
          // Compute array of exercise ids the option applies to.
          // If MT, applies to all exercises that have the RI that has the
          // rubric item response option.
          $exerciseAppliesToIds = [];
          /* @noinspection PhpUndefinedFieldInspection */
          foreach ($rubricItemResponse->field_exercises as $appliesTo) {
            $exerciseAppliesToIds[] = $appliesTo->target_id;
          }
          /* @noinspection PhpUndefinedFieldInspection */
          $rubricItemResponseOption = [
            'responseOptionId' => $rubricItemResponse->id(),
            'response' => $this->filterInputService->filterUserContent($rubricItemResponse->field_response_to_student->value),
            'notes' => $this->filterInputService->filterUserContent($rubricItemResponse->field_notes->value),
            'completes' => $rubricItemResponse->field_completes_rubric_item->value,
            'appliesToExerciseIds' => $exerciseAppliesToIds,
          ];
          // Add rubric item response option to results.
          $resultRIOptions[$rubricItemResponse->id()] = $rubricItemResponseOption;
        } //End for each rubric item response.
      } //End for each rubric item.
    }
    $result = new JsonResponse([
      'rubricItems' => $resultRIs,
      'responseOptions' => $resultRIOptions,
    ]);
    return $result;
  }

  /**
   * Save new response options.
   *
   * Receives array with each entry like this:
   *  rubricItemId: 90 //RI this response option is for.
   *  newOptionTitle: 'Boo' //Text of option.
   *  indexInRINewOptions: 0 //Order of new option in client's list of
   *    new options for this RI. Send it back to client.
   *  keep: true //Whether to keep this response as a permanent
   *    addition to the RI, or just use it this once.
   *
   * This is a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return array like this:
   *    rubricItemId: 90 //From input
   *    indexInRINewOptions: 0 //From input
   *    optionNid: node id for this response option in the DB. Client will use
   *      this as the new index of the option in its arrays.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function saveNewResponseOptions(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/save-new-response-options']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    $result = [];
    $newResponseOptions = $request->get('newResponseOptions');
    foreach ($newResponseOptions as $newResponseOption) {
      $rubricItemId = $newResponseOption['rubricItemId'];
      // Should be a number.
      if (!is_numeric($rubricItemId)) {
        throw new AccessDeniedException('Access denied');
      }
      // Load the rubric item. Deny access if there is a problem.
      /** @var \Drupal\node\Entity\Node $rubricItem */
      $rubricItem = $this->entityTypeManager
        ->getStorage('node')
        ->load($rubricItemId);
      if (!$rubricItem) {
        throw new AccessDeniedException('Access denied');
      }
      // Check that the node is a rubric item.
      if ($rubricItem->bundle() !== SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE) {
        throw new AccessDeniedException('Access denied');
      }
      // Filter content from grader. You never know.
      $newOptionTitle = $this->filterInputService->filterUserContent($newResponseOption['newOptionTitle']);
      $indexInRINewOptions = $newResponseOption['indexInRINewOptions'];
      $keep = $newResponseOption['keep'];
      if ($keep) {
        // Make a new paragraph for the new response option.
        /** @var \Drupal\paragraphs\ParagraphInterface $responseOption */
        $responseOption = $this->entityTypeManager
          ->getStorage('paragraph')
          ->create(['type' => SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE]);
        $responseOption->setParentEntity(
          $rubricItem,
          SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES
        );
        $responseOption->set(
          SkillingConstants::FIELD_RESPONSE_TO_STUDENT,
          [
            'value' => $newOptionTitle,
            'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
          ]
        );
        // $newOptionTitle has already been filtered.
        try {
          $responseOption->save();
          $rubricItem->field_rubric_item_responses[] = [
            'target_id' => $responseOption->id(),
            'target_revision_id' => $responseOption->getRevisionId(),
          ];
          $rubricItem->save();
          $result[] = [
            'rubricItemId' => $rubricItemId,
            'indexInRINewOptions' => $indexInRINewOptions,
            'optionNid' => $responseOption->id(),
          ];
        }
        catch (EntityStorageException $e) {
          throw new AccessDeniedException('Access denied');
        }
      }
    } //End for each new response option.
    return new JsonResponse($result);
  }

  /**
   * Save feedback.
   *
   * Security check. Everything is checked. Go through the code.
   *
   * This is a state-changing operation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Result of save.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Exception
   */
  public function saveFeedback(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/save-feedback']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    /** @var array $feedbackData */
    $feedbackData = $request->get('feedbackData');
    // Should be an array.
    if (!is_array($feedbackData)) {
      throw new AccessDeniedException('Access denied');
    }
    // Get and check submission id.
    $submissionId = $feedbackData['submissionId'];
    if (!$submissionId || !is_numeric($submissionId)) {
      throw new AccessDeniedException('Access denied');
    }
    $submissionId = (integer) $submissionId;
    // Make sure an node with this id exists.
    /** @var \Drupal\node\Entity\Node $submissionNode */
    try {
      $submissionNode = $this->entityTypeManager
        ->getStorage('node')
        ->load($submissionId);
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if didn't find it.
    if (!$submissionNode) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if it wasn't a submission.
    if ($submissionNode->bundle() !== SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
      throw new AccessDeniedException('Access denied');
    }
    // Get and check whether exercise was completed.
    $exerciseCompleted = $this->filterInputService->filterUserContent(
      $feedbackData['exerciseCompleted']
    );
    if ($exerciseCompleted !== 'true' && $exerciseCompleted !== 'false') {
      throw new AccessDeniedException('Access denied');
    }
    $exerciseCompleted = ($feedbackData['exerciseCompleted'] === 'true');
    // Get and check the overall evaluation.
    $overallEvaluation = $this->filterInputService->filterUserContent(
      $feedbackData['overallEvaluation']
    );
    $overallEvaluation = strtolower($overallEvaluation);
    // Is it a known value?
    if (!in_array($overallEvaluation, self::VALID_EVALUATIONS)) {
      throw new AccessDeniedException('Access denied');
    }
    // Get and filter feedback. Use Stripped.
    $feedback = $this->filterInputService->filterUserContent($feedbackData['feedback']);
    if (!$feedback) {
      throw new AccessDeniedException('Access denied');
    }
    // Replace tokens in the feedback message.
    $studentUid = $submissionNode->get('field_user')->target_id;
    $student = $this->skillingUserFactory->makeSkillingUser($studentUid);
    $feedback = $this->tokenService->replace($feedback, ['user' => $student->getDrupalUser()]);
    // Validate all of the rubric item data sent.
    foreach ($feedbackData['rubricItemChoices'] as $rubricItemId => $rubricItemResponses) {
      if (!$rubricItemId || !is_numeric($rubricItemId)) {
        throw new AccessDeniedException('Access denied');
      }
      $rubricItemId = (integer) $rubricItemId;
      // Make sure that the RI exists.
      /** @var \Drupal\node\Entity\Node $rubricItemNode */
      try {
        $rubricItemNode = $this->entityTypeManager
          ->getStorage('node')
          ->load($rubricItemId);
      }
      catch (InvalidPluginDefinitionException $e) {
        throw new AccessDeniedException('Access denied');
      }
      if (!$rubricItemNode) {
        throw new AccessDeniedException('Access denied');
      }
      // Exit if it wasn't a rubric item.
      if ($rubricItemNode->getType() !== SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE) {
        throw new AccessDeniedException('Access denied');
      }
      if (!is_array($rubricItemResponses)) {
        throw new AccessDeniedException('Access denied');
      }
      foreach ($rubricItemResponses as $responseOptionId) {
        if (!$responseOptionId || !is_numeric($responseOptionId)) {
          throw new AccessDeniedException('Access denied');
        }
        // Check that the response option exists.
        /** @var \Drupal\paragraphs\ParagraphInterface $responseOptionParagraph */
        try {
          $responseOptionParagraph = $this->entityTypeManager
            ->getStorage('paragraph')
            ->load($responseOptionId);
        }
        catch (InvalidPluginDefinitionException $e) {
          throw new AccessDeniedException('Access denied');
        }
        if (!$responseOptionParagraph) {
          throw new AccessDeniedException('Access denied');
        }
        // Exit if it wasn't a response option.
        if ($responseOptionParagraph->getType() !== SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE) {
          throw new AccessDeniedException('Access denied');
        }
      } //End for each response option for a rubric item.
    }
    // What the grader chose on each rubric item.
    $rubricItemChoices = json_encode($feedbackData['rubricItemChoices']);
    // Filter.
    $feedback = $this->filterInputService->filterUserContent($feedback);
    // Update the submission.
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_complete->value = $exerciseCompleted;
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_overall_evaluation->value = $overallEvaluation;
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_feedback_message->value = $feedback;
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_feedback_message->format = SkillingConstants::STRIPPED_TEXT_FORMAT;
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_feedback_source->target_id = $this->currentUser->id();
    /* @noinspection PhpUndefinedFieldInspection */
    $submissionNode->field_when_feedback_given->value
      = $this->dateFormatter->format(time(), 'custom', 'Y-m-d\TH:i:s', 'UTC');
    $submissionNode->field_feedback_responses->value = $rubricItemChoices;
    $submissionNode->save();
    // Add notice.
    /** @var \Drupal\node\NodeInterface $exercise */
    $exercise = $this->entityTypeManager->getStorage('node')
      ->load($submissionNode->field_exercise->target_id);
    // Check if exists.
    if (!$exercise) {
      $result = [
        'status' => 'ARGH!',
      ];
      return new JsonResponse($result);
    }
    $title = $this->t(
      "Feedback for '@exercise'",
      ['@exercise' => $exercise->getTitle()]
    );
    $completeFormatted = $exerciseCompleted ? $this->t('Yes') : $this->t('No');
    $messageRenderable = [
      '#theme' => 'notice_feedback',
      '#overall' => ucfirst($overallEvaluation),
      '#complete' => $completeFormatted,
      '#feedback' => $feedback,
    ];
    $rendered = $this->renderer->render($messageRenderable);
    $this->noticeService->notifyUser($student, $title, $rendered);
    // Store event in history?
    // Check whether the history module is active.
    $isHistoryModuleActive =
      $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
    if ($isHistoryModuleActive) {
      // Store the event.
      try {
        $this->historyService->recordSubmissionAccess(
          $submissionId, SkillingHistoryConstants::OPERATION_FEEDBACK
        );
      }
      catch (\Exception $e) {
        $message = 'Exception submission access history call: '
          . $e->getMessage();
        \Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
      }
    }

    /** @var \Drupal\skilling\CompletionScore $completionScoreService */
    $completionScoreService = \Drupal::service('skilling.completion_score');
    $completionScoreService->updateCompletionScoresForUser($studentUid);

    // Check for new badge.
    if ($exerciseCompleted) {
      $this->badgingService->updateUserBadges($student);
    }

    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

  /**
   * Ajax call to delete file uploaded by a grader for a submission.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function deleteGraderFile(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/delete-grader-file']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    // Get and check submission id.
    $submissionId = $request->get('submissionId');
    if (!$submissionId || !is_numeric($submissionId)) {
      throw new AccessDeniedException('Access denied');
    }
    $submissionId = (integer) $submissionId;
    // Make sure an node with this id exists.
    /** @var \Drupal\node\Entity\Node $submissionNode */
    try {
      $submissionNode = $this->entityTypeManager
        ->getStorage('node')
        ->load($submissionId);
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if didn't find it.
    if (!$submissionNode) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if it wasn't a submission.
    if ($submissionNode->bundle() !== SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
      throw new AccessDeniedException('Access denied');
    }
    //Get the file id.
    $fileToDeleteId = $request->get('fileId');
    if (!$fileToDeleteId || !is_numeric($fileToDeleteId) || $fileToDeleteId < 1) {
      throw new AccessDeniedException('Access denied');
    }
    //Get the value of grader files in the submission.
    $graderFilesFieldInSubmission = $submissionNode->get(SkillingConstants::FIELD_GRADER_ATTACHMENTS);
    $graderFileItemList = $graderFilesFieldInSubmission->getValue();
    //Is the file attached to the field?
    $fileAttachedToField = FALSE;
    $fileIndex = 0;
    foreach ($graderFileItemList as $index => $fileItem) {
      $fileId = $fileItem['target_id'];
      if ($fileId === $fileToDeleteId) {
        $fileAttachedToField = TRUE;
        $fileIndex = $index;
        break;
      }
    }
    if (!$fileAttachedToField) {
      throw new AccessDeniedException('Access denied');
    }
    //Check that the file is owned by a grader/instructor of the student
    //who owns the submission.
    $studentId = $submissionNode->getOwnerId();
    /** @var \Drupal\file\Entity\File $fileToDelete */
    $fileToDelete = $this->entityTypeManager->getStorage('file')
      ->load($fileId);
    if (!$fileToDelete) {
      throw new AccessDeniedException('Access denied');
    }
    $fileOwnerId = $fileToDelete->getOwnerId();
    $isInstructor = $this->userRelationshipService->isUserUidInstructorOfUserUid(
      $fileOwnerId, $studentId
    );
    $isGrader = $this->userRelationshipService->isUserUidGraderOfUserUid(
      $fileOwnerId, $studentId
    );
    if (!$isInstructor && !$isGrader) {
      throw new AccessDeniedException('Access denied');
    }
    //Delete the file.
    $fileToDelete->delete();
    //Update submission node.
    unset($graderFileItemList[$fileIndex]);
    $submissionNode->set(
      SkillingConstants::FIELD_GRADER_ATTACHMENTS,
      $graderFileItemList
    );
    $submissionNode->save();
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

  /**
   * Ajax call to upload a grader file for a submission.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Result of call.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function uploadGraderFile(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/upload-grader-file']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied');
    }
    // Is the current user a grader?
    if (!$this->currentUser->isGrader()) {
      throw new AccessDeniedException('Access denied');
    }
    // Get and check submission id.
    $submissionId = $request->get('submissionId');
    if (!$submissionId || !is_numeric($submissionId)) {
      throw new AccessDeniedException('Access denied');
    }
    $submissionId = (integer) $submissionId;
    // Make sure an node with this id exists.
    /** @var \Drupal\node\Entity\Node $submissionNode */
    try {
      $submissionNode = $this->entityTypeManager
        ->getStorage('node')
        ->load($submissionId);
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if didn't find it.
    if (!$submissionNode) {
      throw new AccessDeniedException('Access denied');
    }
    // Exit if it wasn't a submission.
    if ($submissionNode->bundle() !== SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE) {
      throw new AccessDeniedException('Access denied');
    }
    //Check that current user a grader/instructor of the student
    //who owns the submission.
    $studentId = $submissionNode->getOwnerId();
    $currentUserId = $this->currentUser->id();
    $isInstructor = $this->userRelationshipService->isUserUidInstructorOfUserUid(
      $currentUserId, $studentId
    );
    $isGrader = $this->userRelationshipService->isUserUidGraderOfUserUid(
      $currentUserId, $studentId
    );
    if (!$isInstructor && !$isGrader) {
      throw new AccessDeniedException('Access denied');
    }
    //Get the uploaded file.
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
    $uploadedFile = $request->files->get('file');
    if (!$uploadedFile->isValid()) {
      return $this->makeUploadErrorResponse('File not uploaded properly. Try again, maybe?');
    }
    //Get uploaded file metadata.
    $uploadedFileName = $uploadedFile->getClientOriginalName();
    $uploadedFileExtension = $uploadedFile->getClientOriginalExtension();
    $uploadedFileSize = $uploadedFile->getClientSize();
    $uploadedFilePath = $uploadedFile->getPathname();
    //Get node field metadata.
    $nodeFieldMetadata = $this->getFileFieldMetaData(
      SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE,
      SkillingConstants::FIELD_GRADER_ATTACHMENTS
    );
    if (!$nodeFieldMetadata) {
      throw new AccessDeniedException('Access denied');
    }
    //Check uploaded file's extension.
    $allowedExtensions = explode(' ', $nodeFieldMetadata['extensions']);
    if (!in_array($uploadedFileExtension, $allowedExtensions)) {
      return $this->makeUploadErrorResponse('Extension not allowed.');
    }
    //Check the file size.
    $maxSizeAllowed = Bytes::toInt($nodeFieldMetadata['max file size']);
    if ($uploadedFileSize > $maxSizeAllowed) {
      return $this->makeUploadErrorResponse('File too large.');
    }
    //Check cardinality.
    /** @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $fieldValueInNode */
    $fieldValueInNode = $submissionNode->get(
      SkillingConstants::FIELD_GRADER_ATTACHMENTS
    );
    $fieldAttachedFileItemList = $fieldValueInNode->getValue();
    $nodeFileFieldNumAttachments = count($fieldAttachedFileItemList);
    $allowedCardinality = $nodeFieldMetadata['cardinality'];
    if ($allowedCardinality !== -1) {
      //Not unlimited.
      if ($nodeFileFieldNumAttachments >= $allowedCardinality) {
        return $this->makeUploadErrorResponse('Maximum number of files reached.');
      }
    }
    //OK. Attach the file.
    //Prepare the directory.
    $directory = 'private://' . $nodeFieldMetadata['directory'];
    $result = \Drupal::service('file_system')->prepareDirectory(
      $directory, FileSystemInterface::CREATE_DIRECTORY
    );
    if (!$result) {
      throw new AccessDeniedException('Access denied');
    }
    //Read the file's contents.
    $fileData = file_get_contents($uploadedFilePath);
    //Save in right dir, creating a file entity instance.
    $savedFile = file_save_data($fileData,
      $directory . '/' . $uploadedFileName,
      FileSystemInterface::EXISTS_RENAME
    );
    if (!$savedFile) {
      throw new AccessDeniedException('Access denied');
    }
    //Attach to the node.
    $submissionNode->field_grader_attachments[] = [
      'target_id' => $savedFile->id(),
    ];
    $submissionNode->save();
    $result = [
      'status' => 'OK',
      'fid' => $savedFile->id(),
      'owner' => $this->currentUser->id(),
      'ownerUid' => $this->currentUser->id(),
      'url' => $savedFile->createFileUrl(),
    ];
    return new JsonResponse($result);
  }

  /**
   * Make a response for file upload attempt with an error message.
   *
   * @param string $message The error message.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse Response to return to client.
   */
  protected function makeUploadErrorResponse($message) {
    $result = [
      'status' => 'FAIL',
      'message' => $message,
    ];
    return new JsonResponse($result);
  }


  /**
   * Get metadata for a file field.
   *
   * @param string $contentType Content type with the field.
   * @param string $fieldName Name of the field.
   *
   * @return array|bool Metadata, or false if a problem.
   */
  public function getFileFieldMetaData($contentType, $fieldName) {
    if ($contentType === '' || $fieldName === '') {
      return FALSE;
    }
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('node', $contentType);
    if (!$fields || count($fields) === 0) {
      return FALSE;
    }
    //Get file field definition.
    if (!isset($fields[$fieldName])) {
      return FALSE;
    }
    /** @var \Drupal\field\Entity\FieldConfig $fieldDef */
    $fieldDef = $fields[$fieldName];
    //Get settings, doesn't include cardinality.
    $directory = $fieldDef->getSetting('file_directory');
    //Resolve tokens.
    /** @var \Drupal\Core\Utility\Token $tokenService */
    $tokenService = \Drupal::service('token');
    $directory = $tokenService->replace($directory);
    $fileExtensions = $fieldDef->getSetting('file_extensions');
    $maxFileSize = $fieldDef->getSetting('max_filesize');
    //Get cardinality.
    /** @var \Drupal\field\Entity\FieldStorageConfig $fieldStorageDef */
    $fieldStorageDef = $fieldDef->getFieldStorageDefinition();
    $cardinality = $fieldStorageDef->getCardinality();
    //Return results.
    $result = [
      'content type' => $contentType,
      'field' => $fieldName,
      'directory' => $directory,
      'extensions' => $fileExtensions,
      'max file size' => $maxFileSize,
      'cardinality' => $cardinality,
    ];
    return $result;
  }

}
