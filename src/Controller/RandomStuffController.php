<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * A place for random pages that don't belong anywhere else.
 *
 * It's like the Island of Random Toy Parts.
 */
class RandomStuffController extends ControllerBase {

  /**
   * Confirm that a message was sent.
   *
   * @return string[]
   *   Renderable.
   */
  public function messageSentConfirmation() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Your message has been sent.'),
    ];
  }

}
