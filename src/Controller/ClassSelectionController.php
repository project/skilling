<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class ClassSelectionController.
 */
class ClassSelectionController extends ControllerBase {

  /**
   * The current class.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ClassSelectionController object.
   *
   * @param SkillingCurrentClass $currentClass
   *   The current class service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(SkillingCurrentClass $currentClass, MessengerInterface $messenger) {
    $this->currentClass = $currentClass;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('skilling.current_class'),
      $container->get('messenger')
    );
  }

  /**
   * Set the current class.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Where to go after changing the current class.
   *
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function setClass(Request $request) {
    $classToMakeCurrent = $request->query->get('nid');
    $this->currentClass->setUserDataCurrentClassNid($classToMakeCurrent);
    $this->messenger->addStatus($this->t('Current class changed'));
    $returnPath = $request->query->get('destination');
    return new RedirectResponse($returnPath);
  }

}
