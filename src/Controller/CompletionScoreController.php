<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Url;
use Drupal\skilling\Help\HelpConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CompletionScoreController.
 */
class CompletionScoreController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var \Drupal\Core\Config\ConfigFactoryInterface */
  protected $configFactory;

  /**
   * The alias manager service.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $aliasManager;

  /**
   * Controller constructor.
   *
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $config_factory, AliasManager $aliasManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
    $this->aliasManager = $aliasManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * Explain completion score.
   *
   * @return array|RedirectResponse
   *   Render array with default link, or redirect object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function explainCompletionScore() {
    // Check for a published page with the right title.
    $helpNodeNid = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('title', $this->t(HelpConstants::EXPLAIN_COMPLETION_SCORE_PAGE_TITLE))
      ->condition('status', 1)
      ->execute();
    // Does a custom page exist?
    if (!$helpNodeNid) {
      // No. Render a link to external documentation.
      $url = Url::fromUri(HelpConstants::DEFAULT_EXTERNAL_HELP_PAGE_URL);
      $link = Link::fromTextAndUrl($this->t('Documentation'), $url);
      $renderable = $link->toRenderable();
      return $renderable;
    }
    // Redirect to the custom help page.
    $helpNodeNid = reset($helpNodeNid);
    $redirect = $this->redirect(
      'entity.node.canonical',
      ['node' => $helpNodeNid]
    );
    return $redirect;
  }

}
