<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling_history\History;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProgressBasedOnRequiredController.
 */
class ProgressBasedOnRequiredController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling history event recorder.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * The current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new ReflectNoteController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   Skilling current user service.
   * @param \Drupal\skilling_history\History $historyService
   *   Skilling history service.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClass
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skilling_current_user,
    History $historyService,
    SkillingUtilities $skillingUtilities,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    SkillingCurrentClass $currentClass
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->historyService = $historyService;
    $this->skillingUtilities = $skillingUtilities;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->currentClass = $currentClass;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling_history.history'),
      $container->get('skilling.utilities'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * Save note.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveBasedOnRequired(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/save-progress-base']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in saveBasedOnRequired');
    }
    if (!$this->skillingCurrentUser->isStudent()) {
      throw new AccessDeniedException('Access denied, not student in saveBasedOnRequired');
    }
    $required = $request->get('required');
    if ($required !== "true" && $required !== "false") {
      throw new AccessDeniedException('Access denied, bad required in saveBasedOnRequired: ' . $required);
    }
    // Convert to boolean.
    $required = $required === 'true';
    $enrollmentNid = $this->currentClass->getCurrentEnrollment()->id();
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entityTypeManager->getStorage('node')
      ->load($enrollmentNid);
    if (!$node) {
      $result = [
        'status' => 'Error',
      ];
      return new JsonResponse($result);
    }
    $node->set(SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY, $required);
    // Hacky: stop other code trying to do its own save, triggered by this save.
    global $globalEnrollmentBeingSaved;
    $globalEnrollmentBeingSaved = 'yup';
    $node->save();
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

}
