<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\skilling\Help\HelpConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class HelpController.
 */
class HelpController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Show help.
   *
   * Looks for a page with the translated title "Help" (or whatever is in the
   * constants file). If found, redirect there. If not, render a default link.
   *
   * @return array|RedirectResponse
   *   Render array with default link, or redirect object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function showHelp() {
    // Check for a published page with the title "Help".
    $helpNodeNid = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('title', $this->t(HelpConstants::HELP_PAGE_TITLE))
      ->condition('status', 1)
      ->execute();
    // Does a custom help page exist?
    if (!$helpNodeNid) {
      // No. Render a link to external documentation.
      $url = Url::fromUri(HelpConstants::DEFAULT_EXTERNAL_HELP_PAGE_URL);
      $link = Link::fromTextAndUrl($this->t('Documentation'), $url);
      $renderable = $link->toRenderable();
      return $renderable;
    }
    // Redirect to the custom help page.
    $helpNodeNid = reset($helpNodeNid);
    $redirect = $this->redirect(
      'entity.node.canonical',
      ['node' => $helpNodeNid]
    );
    return $redirect;
  }

}
