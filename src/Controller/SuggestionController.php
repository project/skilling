<?php

namespace Drupal\skilling\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling_history\History;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class SuggestionController.
 */
class SuggestionController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The history service.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new controller object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   *   Skilling current user service.
   * @param \Drupal\skilling_history\History $historyService
   *   Skilling history service.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   Ajax security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skillingCurrentUser,
    History $historyService,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->historyService = $historyService;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling_history.history'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler')
    );
  }

  /**
   * Save suggestion.
   *
   * Security note: get identifier and suggestion from client. Strip them.
   * Also, treat anything unexpected (e.g., node not found) as
   * a security exception.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response to the client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function saveSuggestion(Request $request) {
    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/save-suggestion']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in saveSuggestion');
    }
    if ($this->skillingCurrentUser->isAnonymous()) {
      throw new AccessDeniedException('Access denied, anon user in saveSuggestion');
    }
    // Get the user who owns the data.
    $suggestionNodeOwnerId = $this->skillingCurrentUser->id();
    // What the user typed.
    $suggestion = $request->get('suggestion');
    // Filter it.
    $suggestion = $this->filterInputService->filterUserContent($suggestion);
    // What page the user was on.
    $page = $request->get('page');
    // Check it. If not, skip all processing.
    if (UrlHelper::isValid($page)) {
      /** @var \Drupal\node\NodeInterface $suggestionNode */
      $suggestionNode = $this->entityTypeManager
        ->getStorage('node')
        ->create(['type' => SkillingConstants::SUGGESTION_CONTENT_TYPE]);
      $title = $this->t(
        'Suggestion from user @uid',
        [
          '@uid' => $this->skillingCurrentUser->getAccountName(),
        ]
      );
      $suggestionNode->setTitle($title);
      $suggestionNode->setOwnerId($suggestionNodeOwnerId);
      // Set the suggestion text.
      $suggestionNode->get(SkillingConstants::FIELD_SUGGESTION)->value = $suggestion;
      // Set the page.
      //    $suggestionNode->get(SkillingConstants::FIELD_PAGE)->value = $page;
      $suggestionNode->set(SkillingConstants::FIELD_PAGE, ['uri' => $page]);
      $suggestionNode->save();
      // Store event in history?
      // Check whether the history module is active.
      $isHistoryModuleActive =
        $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
      if ($isHistoryModuleActive) {
        // Store the event.
        try {
          $this->historyService->recordSuggestion($suggestion);
        }
        catch (\Exception $e) {
          $message = 'Exception suggest: '
            . $e->getMessage();
          \Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
        }
      }
    }
    // Return JSON response.
    $result = [
      'status' => 'OK',
    ];
    return new JsonResponse($result);
  }

}
