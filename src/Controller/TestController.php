<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\skilling\Badging;
use Drupal\skilling\CompletionScore;
use Drupal\skilling\Dog;
use Drupal\skilling\MakeStarterContent\MakeNodes;
use Drupal\skilling\MakeStarterContent\MakeStarterContent;
use Drupal\skilling\MakeStarterContent\MakeUsers;
use Drupal\skilling\Migrate\MigrateExport;
use Drupal\skilling\Migrate\MigrateImport;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUser;
use Drupal\skilling\Timeline;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\skilling\Notice;
use Drupal\skilling\SkillingUserFactory;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Class TestController.
 */
class TestController extends ControllerBase {

  use DependencySerializationTrait;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  protected $dog;

  /**
   * Service that makes starter content.
   *
   * @var \Drupal\skilling\MakeStarterContent\MakeStarterContent
   */
  protected $makeStarterContentService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $sessionService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The completion score service.
   *
   * @var \Drupal\skilling\CompletionScore
   */
  protected $completionScoreService;


  protected $noticeService;

  protected $skillingUserFactory;

  protected $fileSystemService;


  protected $migrateExportService;
  protected $migrateImportService;

  /** The badging service.
   *
   * @var \Drupal\skilling\Badging
   */
  protected $badgingService;


  /**
   * @var \Drupal\skilling\Timeline
   */
  protected $timelineService;


  /**
   * Constructs a new AdminController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   Skilling utilities service.
   * @param \Drupal\skilling\MakeStarterContent\MakeNodes $makeNodes
   *   Service to make nodes.
   * @param \Drupal\skilling\MakeStarterContent\MakeUsers $makeUsers
   *   Service to make users.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   Symfony session service.
   * @param \Drupal\skilling\MakeStarterContent\MakeStarterContent $makeStarterContentService
   *   Service that makes starter content.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\skilling\CompletionScore $completionScoreService
   *   The completion score service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skilling_utilities,
    MakeNodes $makeNodes,
    MakeUsers $makeUsers,
    Session $session,
    MakeStarterContent $makeStarterContentService,
    MessengerInterface $messenger,
    CompletionScore $completionScoreService,
    Notice $noticeService,
    SkillingUserFactory $skillingUserFactory,
    FileSystem $fileSystemService,
    MigrateExport $migrateExportService,
    MigrateImport $migrateImportService,
    Badging $badgingService,
    Timeline $timelineService
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skilling_utilities;
    $this->makeNodesService = $makeNodes;
    $this->makeUsersService = $makeUsers;
    $this->sessionService = $session;
    $this->makeStarterContentService = $makeStarterContentService;
    $this->messenger = $messenger;
    $this->completionScoreService = $completionScoreService;
    $this->noticeService = $noticeService;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->fileSystemService = $fileSystemService;
    $this->migrateExportService = $migrateExportService;
    $this->migrateImportService = $migrateImportService;
    $this->badgingService = $badgingService;
    $this->timelineService = $timelineService;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('skilling.starter.make_nodes'),
      $container->get('skilling.starter.make_users'),
      $container->get('session'),
      $container->get('skilling.starter.make_starter_content'),
      $container->get('messenger'),
      $container->get('skilling.completion_score'),
      $container->get('skilling.notice'),
      $container->get('skilling.skilling_user_factory'),
      $container->get('file_system'),
      $container->get('skilling.migrate_export'),
      $container->get('skilling.migrate_import'),
      $container->get('skilling.badging'),
      $container->get('skilling.timeline')

    );
  }



  //  public function __construct(SkillingCurrentUser $skilling_current_user, SkillingUtilities $utilities, MakeStarterContent $makeStarterContentService) {
//    $this->skillingCurrentUser = $skilling_current_user;
//    $this->skillingUtilities = $utilities;
//    $this->makeStarterContentService = $makeStarterContentService;
//  }

  /**
   * {@inheritdoc}
   */
//  public static function create(ContainerInterface $container) {
//    /* @noinspection PhpParamsInspection */
//    return new static(
//      $container->get('skilling.skilling_current_user'),
//      $container->get('skilling.utilities'),
//      $container->get('skilling.starter.make_starter_content')
//    );
//  }


  public function testRIResponseCreate() {
    $newOptionTitle = 'Duck';
    $rubricItemId = '115';
    // Load the rubric item. Deny access if there is a problem.
    /** @var \Drupal\node\Entity\Node $rubricItem */
    $rubricItem = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->load($rubricItemId);
    if (!$rubricItem) {
      throw new AccessDeniedException('Access denied');
    }
    // Check that the node is a rubric item.
    if ($rubricItem->bundle() !== SkillingConstants::RUBRIC_ITEM_CONTENT_TYPE) {
      throw new AccessDeniedException('Access denied');
    }
    // Filter content from grader. You never know.
    //    $newOptionTitle = $this->filterInputService->filterUserContent($newResponseOption['newOptionTitle']);
    $indexInRINewOptions = 0; //$newResponseOption['indexInRINewOptions'];
    //    $keep = $newResponseOption['keep'];
    //    if ($keep) {
    // Make a new paragraph for the new response option.
    /** @var \Drupal\paragraphs\ParagraphInterface $responseParagraph */
    $responseOption = Paragraph::create(['type' => 'rubric_item_response']);
    /** @var \Drupal\paragraphs\ParagraphInterface $responseOption */
//    $responseOption = \Drupal::entityTypeManager()
//      ->getStorage('paragraph')
//      ->create(['type' => SkillingConstants::RUBRIC_ITEM_RESPONSE_PARAGRAPH_TYPE]);
    $responseOption->set(
      SkillingConstants::FIELD_RESPONSE_TO_STUDENT,
      [
        'value' => $newOptionTitle,
        'format' => SkillingConstants::SKILLING_TEXT_FORMAT,
      ]
    );
    $responseOption->set(
      SkillingConstants::FIELD_COMPLETES_RUBRIC_ITEM,
      FALSE
    );
    $responseOption->setParentEntity(
      $rubricItem,
      SkillingConstants::FIELD_RUBRIC_ITEM_RESPONSES
    );
    $responseOption->isNew();
    $responseOption->save();
    $rubricItem->field_rubric_item_responses[] = [
      'target_id' => $responseOption->id(),
      'target_revision_id' => $responseOption->getRevisionId(),
    ];
    // $newOptionTitle has already been filtered.
    try {
      $rubricItem->save();
      $result[] = [
        'rubricItemId' => $rubricItemId,
        'indexInRINewOptions' => $indexInRINewOptions,
        'optionNid' => $responseOption->id(),
      ];
    } catch (EntityStorageException $e) {
      throw new AccessDeniedException('Access denied');
    }
    //    }
    return [
      '#markup' => 'Did something',
    ];
  } //End for each new response option.


  public function testCreateRI() {
    $this->makeStarterContentService->makeTestRI();
    return [
      '#markup' => 'Did something',
    ];
  }

  public function testCreateFib() {
    $this->makeStarterContentService->makeTestFiB();
    return [
      '#markup' => 'Did something',
    ];
  }

  //  public function testEvents() {
  //    $this->makeStarterContentService->testCalendarsAndEvents();
  //    return [
  //      '#markup' => 'Did something with events.',
  //    ];
  //  }


  public function toggleDog() {
    if (!$this->skillingCurrentUser->isAnonymous()) {
      /** @var UserDataInterface $userData */
      $userData = \Drupal::service('user.data');
      $showDog = $userData->get(
        SkillingConstants::MODULE_NAME,
        $this->skillingCurrentUser->id(),
        'show_dog'
      );
      $showDog = !$showDog;
      $showDog = $userData->set(
        SkillingConstants::MODULE_NAME,
        $this->skillingCurrentUser->id(),
        'show_dog',
        $showDog
      );
      $route = $this->skillingUtilities->getCurrentRouteMatch();
      $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
      $response = new RedirectResponse($previousUrl);
      $response->setMaxAge(0);
      return $response;
    }
  }



  public function dog() {
    //    $menu_link_service = \Drupal::getContainer()->get('plugin.manager.menu.link');
    //    $route_params = array('node' => $entity->id());
    //    $menu_links = $menu_link_service->loadLinksByRoute('entity.node.canonical', $route_params, $menu_name);
    //    if (!empty($menu_links)) {
    //      $root_menu_item = reset($menu_links);


    $entityViewDisplay = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('user.user.default');
    $r = 5;


    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: dog'),
    ];
  }


  public function llama() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: llama'),
    ];
  }

  public function monkey() {
    \Drupal::service('messenger')->addMessage(t('Monkey page'));
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: monkey'),
    ];
  }

  public function storeFile() {
    $scheme = \Drupal::request()->getScheme();
    if ($scheme !== 'https') {
      \Drupal::service('messenger')->addMessage(t('Use https'));
    }


    //  use \Drupal\file\Entity\File;
    $ok = FALSE;
    if (Settings::get('file_private_path')) {
      if (\Drupal::hasService('stream_wrapper.private')) {
        $fileSystem = \Drupal::service('file_system');
        // Check if the private file stream wrapper is ready to use.
        if ($fileSystem->validScheme('private')) {
          $ok = TRUE;
        }
      }
    }
    return [
      '#type' => 'markup',
      '#markup' => 'Done. ' . ($ok ? 'Ready' : 'Not ready'),
    ];


    //Save Image in local from remote data.
    $rootPath = $this->skillingUtilities->getSiteRootFilePath();
    $modulePathFromRoot = $this->skillingUtilities->getModuleRelativeUrlPath();
    $imageFilePath = $rootPath . $modulePathFromRoot
      . 'images/characters/jeremy-smile.png';
    $fs = \Drupal::service('file_system');
    $isDir = is_dir("public://characters");
    if (!$isDir) {
      $result = $fs->mkdir("public://characters", NULL, TRUE);
    }
    $data = file_get_contents($imageFilePath);
    $file = file_save_data($data, "public://characters/jeremy-smile.png", FILE_EXISTS_REPLACE);

    // Create node object and save it.
    $node = Node::create([
      'type' => 'character',
      'title' => 'Sample Node',
      'field_internal_name' => 'thing',
      'field_photo' => [
        'target_id' => $file->id(),
        'alt' => 'Jeremy smile',
        'title' => 'Sample File',
      ],
    ]);
    $node->save();
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Done. <a href="/node/' . $node->id() . '">go</a>'),
    ];
  }

  //  public function thong() {
  //    $thong = new Thong();
  //    return [
  //      '#type' => 'markup',
  //      '#markup' => 'Done. *' . $thong->getName() . '*',
  //    ];
  //  }

  public function ack() {
    $entityViewDisplay = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('user.user.default');
    return [
      '#markup' => '<pre>' . print_r($entityViewDisplay, TRUE) . '</pre>',
    ];
  }

  public function doBatch1() {
    $r=5;
    $lessonIds =
//      [1,2,3,4];
      \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->condition('type', 'lesson')
      ->execute();
    $batchBuilder = (new BatchBuilder())
      ->setTitle($this->t('Batchy thing'))
      ->setFinishCallback([$this, 'doneBatchyThing']);
//      ->setErrorMessage(t('Batch has encountered an error'))
//      ->setInitMessage(t('The initialization message (optional)'));
    foreach ($lessonIds as $lessonId) {
      $batchBuilder->addOperation([$this, 'showLesson'], [$lessonId]);
      //      $batchBuilder->addOperation([$this, 'showLesson'], [$lessonId, count($lessonIds)]);
    }
    batch_set($batchBuilder->toArray());
    return batch_process('user');
  }

  public function showLesson($lessonId, &$context) {
    //  public function showLesson($lessonId, $lessonCount, &$context) {
    $r=6;
//    $sandbox = &$context['sandbox'];
//    if (!$sandbox) {
//      $sandbox['progress'] = 0;
//    }
//    $sandbox['progress']++;

//    $context['message'] = 'This ' . $lessonId;


    $lesson = \Drupal::entityTypeManager()->getStorage('node')->load($lessonId);
    if ($lesson) {
      $title = $lesson->getTitle();
      $context['message'] = $this->t('Found "@lesson"', ['@lesson' => $title]);
    }

  }

  public function doneBatchyThing($success, $results, $operations) {
    \Drupal::service('messenger')->addMessage(t('doneBatchyThing'));
  }

  public function testNotice1() {
    $title = "Thruth";
    $message = 'This is a notification.';
    $tags = ['Feedback', 'Message from instructor'];
    $name = 'student1';
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $name]);
    if (is_array($user)) {
      $user = reset($user);
    }
    $skillingUser = $this->skillingUserFactory->makeSkillingUser($user->id());
    $notice = $this->noticeService->notifyUser($skillingUser, $title, $message, $tags);
    return [
      '#markup' => '<p>Link: <a href="/node/' . $notice->id() . '">node/' . $notice->id() . '</a></p>',
    ];
  }

  public function testExport() {
    $numberLessonsExported = $this->migrateExportService->export();
    $result = 'Exported ' . $numberLessonsExported . ' lessons';
    return [
      '#markup' => $result,
    ];

  }

  public function testImport() {
    $numberLessonsImported = $this->migrateImportService->import(FALSE);
    $result = 'Imported ' . $numberLessonsImported . ' lessons';
    return [
      '#markup' => $result,
    ];

  }

  public function testBadging() {
    $u = $this->skillingUserFactory->makeSkillingUser(4);
    $this->badgingService->updateUserBadges($u);
    return [
      '#markup' => 'Poot',
    ];

  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function classExercisesText() {
    $this->timelineService->makeTimeline();
    return [
      '#markup' => 'Rosie',
    ];
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function tax() {
    $exerciseIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'exercise')
      ->execute();
    $exerciseId = reset($exerciseIds);
    $exercise = $this->entityTypeManager->getStorage('node')
      ->load($exerciseId);

    $exerciseDueParagraph = Paragraph::create(['type' => 'exercise_due']);
    $exerciseDueParagraph->field_exercise->target_id = $exerciseId;
    $exerciseDueParagraph->set(SkillingConstants::FIELD_DAY, 3);
    $exerciseDueParagraph->set(
      SkillingConstants::FIELD_REQUIRED, FALSE);
    $exerciseDueParagraph->save();

    $class = Node::create([
      'type' => 'class',
      'title' => 'Goons',
      'field_internal_name' => 'goose',
      'field_when_starts' => '2025-12-31',
      'field_exercises_due' => [
        [
          'target_id' => $exerciseDueParagraph->id(),
          'target_revision_id' => $exerciseDueParagraph->getRevisionId(),
        ]
      ]
    ]);
    $class->save();
    return [
      '#markup' => 'Rosie',
    ];
    $class->field_exercises_due =
      [
        [
        'target_id' => $exerciseDueParagraph->id(),
        'target_revision_id' => $exerciseDueParagraph->getRevisionId(),
        ]
    ];
    $class->save();
    return [
      '#markup' => 'Rosie',
    ];




    $class1->field_exercises_due[] = [
      'target_id' => $exer1Class1Due->id(),
      'target_revision_id' => $exer1Class1Due->getRevisionId(),
    ];


    $maxClassLengthDays = 50;
    $r = rand(3, $maxClassLengthDays);
    $terms1 = \Drupal::entityManager()->getStorage('taxonomy_term')
      ->loadTree("myname");
    $terms2 = \Drupal::entityManager()->getStorage('taxonomy_term')
      ->loadTree("tags");
    $r=6;
    $vid = 'tags';
    $termName = 'Dogs';
    $term = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $termName, 'vid' => $vid]);
    $termName = 'Dogsasdfasd';
    $term = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $termName, 'vid' => $vid]);

    $vid = 'tags';
    $termName = 'ROSIE';
    $termId = $this->makeTaxTerm($vid, $termName);



    $r = Vocabulary::loadMultiple();


    $r=6;


//    $new_term = Term::create([
//      'name' => 'Rosie',
//      'vid' => 'bestdogs',
//      'parent' => [],
//    ]);
//
//    // Save the taxonomy term.
//    $new_term->save();
//
//    // Return the taxonomy term id.
//    $r =  $new_term->id();

    return [
      '#markup' => 'Rosie',
    ];
  }

  protected function makeTaxTerm($vid, $termName) {
    $term = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $termName, 'vid' => $vid]);
    if (count($term) > 0) {
      $term = reset($term);
      $termId = $term->id();
    }
    else {
      // Create the term.
      $term = Term::create([
        'name' => $termName,
        'vid' => $vid,
        'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      ]);
      $term->save();
      $termId = $term->id();
    }
    return $termId;
  }

}
