<?php

namespace Drupal\skilling\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling_history\History;
use Drupal\skilling\SkillingClass\SkillingClassFactory;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\Utilities;

/**
 * Shows "Your class" page.
 */
class YourClassController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $skillingCurrentClass;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The history service.
   *
   * @var \Drupal\skilling_history\History
   */
  protected $historyService;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\skilling\SkillingClass\SkillingClassFactory
   */
  protected $skillingClassFactory;

  /**
   * Constructs a new MyClassController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param SkillingCurrentClass $skilling_current_class
   *   The Skilling current class service.
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   *   The Skilling current user service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   The Skilling utilities service.
   * @param \Drupal\skilling_history\History $historyService
   *   The Skilling history event recorder.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   * @param \Drupal\skilling\SkillingClass\SkillingClassFactory $skillingClassFactory
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentClass $skilling_current_class,
    SkillingCurrentUser $skilling_current_user,
    Utilities $skilling_utilities,
    History $historyService,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService,
    ModuleHandlerInterface $moduleHandler,
    SkillingClassFactory $skillingClassFactory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingCurrentClass = $skilling_current_class;
    $this->skillingCurrentUser = $skilling_current_user;
    $this->skillingUtilities = $skilling_utilities;
    $this->historyService = $historyService;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
    $this->moduleHandler = $moduleHandler;
    $this->skillingClassFactory = $skillingClassFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.current_class'),
      $container->get('skilling.skilling_current_user'),
      $container->get('skilling.utilities'),
      $container->get('skilling_history.history'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input'),
      $container->get('module_handler'),
      $container->get('skilling.class_factory')
    );
  }

  /**
   * Generate the "Your class" page.
   *
   * Security note: filter data originating from instructor. Has already
   * been done by Drupal, but... just because.
   *
   * @return string[]
   *   Renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function yourClass() {
    if ($this->skillingCurrentUser->isAnonymous()) {
      return [
        '#markup' => $this->t('Please log in.'),
      ];
    }
    // Get classes.
    $classes = $this->skillingCurrentClass->getCurrentClasses();
    if (count($classes) === 0) {
      return [
        '#markup' => $this->t('You are not enrolled in any classes.'),
      ];
    }
    $classesRenderable = [];
    /** @var \Drupal\Node\NodeInterface $class */
    foreach ($classes as $class) {
      $classTitle = $class->getTitle();
      $classDescription = $class->getDescriptionFull();
      $classRenderable = [
        'title' => $this->filterInputService->filterUserContent($classTitle),
        'description' => $this->filterInputService->filterUserContent($classDescription),
      ];
      $classesRenderable[$class->id()] = $classRenderable;
    }
    // Find instructors for classes.
    $instructors = [];
    /** @var \Drupal\skilling\SkillingClass\SkillingClass $class */
    foreach ($classes as $class) {
      $instructorsForClass = $class->getInstructors();
      foreach ($instructorsForClass as $instructor) {
        if (!isset($instructors[$instructor->id()])) {
          $instructors[$instructor->id()] = $instructor;
        }
      }
    }
    // Make renderables for each instructor.
    $instructorsRenderable = [];
    foreach ($instructors as $instructor) {
      if ($instructor->isBlocked()) {
        continue;
      }
      $picture = '';
      // Is there a picture?
      if ($instructor->getPictureUrl()) {
        // Yes. Show it at the original upload size.
        $picture = $instructor->getPictureUrl();
      }
      $instructorRenderable = [
        'first_name' => $this->filterInputService->filterUserContent(
          $instructor->getFirstName()
        ),
        'last_name' => $this->filterInputService->filterUserContent(
          $instructor->getLastName()
        ),
        'about' => $this->filterInputService->filterUserContent(
          $instructor->getAbout()
        ),
        'picture' => $picture,
        'uid' => $instructor->id(),
      ];
      $instructorsRenderable[$instructor->id()] = $instructorRenderable;
    }
    // Store event in history?
    // Check whether the history module is active.
    $isHistoryModuleActive =
      $this->moduleHandler->moduleExists(SkillingConstants::HISTORY_MODULE_NAME);
    if ($isHistoryModuleActive) {
      try {
        // Show last lesson looked at.
        $this->historyService->showLastLessonForCurrentStudent();
        // Store the event.
        $this->historyService->recordYourClassView();
      }
      catch (Exception $e) {
        $message = 'Exception view your class: '
          . $e->getMessage();
        Drupal::logger(SkillingConstants::HISTORY_MODULE_NAME)->error($message);
      }
    }
    return [
      '#theme' => 'your_class',
      '#classes' => $classesRenderable,
      '#instructors' => $instructorsRenderable,
    ];
  }

}
