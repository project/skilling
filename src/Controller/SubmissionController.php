<?php

namespace Drupal\skilling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\skilling\CompletionScore;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;
use Drupal\skilling\Exercises;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Utilities as SkillingUtilities;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Render\Renderer;

/**
 * Class SubmissionController.
 */
class SubmissionController extends ControllerBase {

  const EXERCISE_SUBMISSION_LINK_TEXT = 'Submit solution';
  const NOT_STUDENT_MESSAGE = 'If you were a student... config.';

  /**
   * The current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUserService;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var SkillingUtilities
   */
  protected $skillingUtilities;

  /**
   * @var ConfigFactory
   */
  protected $configFactory;

  /**
   * @var Renderer
   */
  protected $renderer;

  /**
   * The Skilling history event recorder.
   *
   * @var \Drupal\skilling_history\History
   */
//  protected $historyService;

  /**
   * The completion score service.
   *
   * @var \Drupal\skilling\CompletionScore
   */
  protected $completionScoreService;

  /**
   * The exercises service.
   *
   * @var \Drupal\skilling\Exercises
   */
  protected $exercisesService;

  /**
   * The current class service.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;

  /**
   * Constructs a new SubmissionController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\Utilities $skillingUtilities
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   * @param \Drupal\Core\Render\Renderer $renderer
   * @param \Drupal\skilling\SkillingCurrentUser $currentUserService
   *   The current user service.
   * @param \Drupal\skilling\CompletionScore $completionScoreService
   *   The completion score service.
   * @param \Drupal\skilling\Exercises $exercisesService
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClass
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingUtilities $skillingUtilities,
    ConfigFactory $configFactory,
    Renderer $renderer,
    SkillingCurrentUser $currentUserService,
//    History $historyService,
    CompletionScore $completionScoreService,
    Exercises $exercisesService,
    SkillingCurrentClass $currentClass
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->configFactory = $configFactory;
    $this->renderer = $renderer;
    $this->currentUserService = $currentUserService;
//    $this->historyService = $historyService;
    $this->completionScoreService = $completionScoreService;
    $this->exercisesService = $exercisesService;
    $this->currentClass = $currentClass;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.utilities'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('skilling.skilling_current_user'),
//      $container->get('skilling.history'),
      $container->get('skilling.completion_score'),
      $container->get('skilling.exercises'),
      $container->get('skilling.current_class')
    );
  }

  /**
   * Close popup submission window. This is the destination
   * of popup (floating) submissions windows.
   *
   * @param $operation
   * @param $exercise_nid
   * @param $submission_nid
   *
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Exception
   */
  public function closeSubmissionWindow($operation, $exercise_nid, $submission_nid) {
    $renderable = [
      '#theme' => 'update_opener_and_close', //Template to use.
      '#operation' => $operation,
      '#exercise_nid' => $exercise_nid,
      '#submission_nid' => $submission_nid,
    ];
    $rendered = $this->renderer->render($renderable);
    return new Response($rendered);
  }

  /**
   * Make a message  that a submission link is out of date.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function getSubmissionChangedMessage() {
    $renderable = [
      '#theme' => 'submission_link_changed',
    ];
    $rendered = $this->renderer->render($renderable);
    $result = [
      'message' => $rendered,
    ];
    return new JsonResponse($result);
  }

  public function showCompletionScore() {
    return [
      '#markup' => $this->t('showCompletionScore'),
    ];
  }

  /**
   * Show a student his/her own submissions.
   *
   * The code loads and shows a view, rather than let Views handle the
   * route and menu item itself. The view uses a contextual filter for
   * the user the submissions are from. The filter uses a default argument
   * of the current user's uid. Handling the route here prevents
   * users from appending a uid to the URL, and getting another user's
   * submission.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Renderable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function showSubmissionsForStudent() {
    if (!$this->currentUserService->isStudent()) {
      return [
        '#markup' => $this->t('Sorry, this is for students only.'),
      ];
    }
    $settings = $this->configFactory->get('skilling.settings');
    $result = [];
    // Show submission policies.
    $message = $settings->get(SkillingConstants::SETTING_KEY_SUBMISSIONS_POLICIES);
    $result['policies'] = [
      '#markup' => $message
    ];
    // Interpret the completion score.
    $explanation = '';
    $completionScore = $this->completionScoreService->getCompletionScoreForCurrentEnrollment();
    if (!is_null($completionScore)) {
      // getCompletionScoreInterpretation returns an array.
      /** @noinspection PhpUnusedLocalVariableInspection */
      list($fileName, $explanation) = $this->completionScoreService->getCompletionScoreInterpretation($completionScore);
    }
    // Work out content that tells the user how to trigger a recompute of their
    // completion score.
    $recomputeUrl = Url::fromRoute('skilling.recompute_completion_score')->toString();
    /** @noinspection HtmlUnknownTarget */
    $recompute = $this->t(
      'You can <a href="@url">recompute</a> your progress score.',
      ['@url' => $recomputeUrl]
    );
    $isRequiredOnly = $this->currentClass->getIsProgressScoreBasedOnRequiredOnlyForCurrentEnrollment();
    $result['progress'] = [
      '#theme' => 'progress_on_submissions_report',
      '#explanation' => $explanation,
      '#recompute' => $recompute,
      '#based_on_required_only' => $isRequiredOnly,
      '#attached' => [
        'library' => [
          'skilling/progress-score-base-change',
        ]
      ],
    ];
    // Get exercise summary counts.
    $counts = $this->exercisesService->getCourseExerciseSummary();
    $result['counts'] = [
      '#theme' => 'exercise_counts',
      '#counts' => $counts,
    ];

    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('submissions');
    $result['view'] = $this->t('Submissions attachment error.');
    if (is_object($view)) {
      $view->setDisplay('student_submissions_report_embed');
      $view->preExecute();
      $view->execute();
      $result['view'] = $view->buildRenderable('student_submissions_report_embed');
      // Record the event.
//      $this->historyService->createHistoryNode(
//        t('View submissions list'), SkillingConstants::HISTORY_EVENT_VIEW_SUBMISSIONS_LIST,
//        $this->currentUserService->id(), [], ''
//      );
    }
    return $result;
  }

  /**
   * Show submission policies.
   *
   * Looks for a page with the translated title "Submission policies"
   * (or whatever is in the
   * constants file). If found, redirect there. If not, render a default link.
   *
   * @return array|RedirectResponse
   *   Render array with default link, or redirect object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
//  public function showSubmissionPolicies() {
//    // Check for a published page with the right title.
//    $policiesNodeNid = $this->entityTypeManager->getStorage('node')->getQuery()
//      ->condition('title', $this->t(HelpConstants::SUBMISSION_POLICIES_PAGE_TITLE))
//      ->condition('status', 1)
//      ->execute();
//    // Does the page exist?
//    if (!$policiesNodeNid) {
//      // No. Render a link to external documentation.
//      $url = Url::fromUri(HelpConstants::DEFAULT_EXTERNAL_HELP_PAGE_URL);
//      $link = Link::fromTextAndUrl($this->t('Documentation'), $url);
//      $renderable = $link->toRenderable();
//      return $renderable;
//    }
//    // Redirect to the custom page.
//    $policiesNodeNid = reset($policiesNodeNid);
//    $redirect = $this->redirect(
//      'entity.node.canonical',
//      ['node' => $policiesNodeNid]
//    );
//    return $redirect;
//  }

  /**
   * Recompute the current user's completion score.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Show submission report.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function updateCurrentUserCompletionScore() {
    if ($this->currentUserService->isStudent()) {
      // Recompute the completion score.
      $userId = $this->currentUserService->id();
      $this->completionScoreService->updateCompletionScoresForUser($userId);
    }
    // Show the submission report.
    return $this->redirect('skilling.show_student_submissions');
  }

}
