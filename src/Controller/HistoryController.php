<?php

namespace Drupal\skilling\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\skilling\Access\FilterUserInputInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\skilling\Exception\SkillingUnknownValueException;
use Drupal\skilling\History;
use Drupal\skilling\SkillingConstants;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\skilling\SkillingCurrentUser;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class HistoryController.
 */
class HistoryController extends ControllerBase {

  /**
   * Drupal\webprofiler\Entity\EntityManagerWrapper definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * The Skilling history event recorder.
   *
   * @var \Drupal\skilling\History
   */
//  protected $historyService;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\Skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  /**
   * Service to filter user input.
   *
   * @var \Drupal\skilling\Access\FilterUserInputInterface
   */
  protected $filterInputService;

  /**
   * Constructs a new HistoryController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\skilling\SkillingCurrentUser $skilling_current_user
   * @param \Drupal\skilling\History $historyService
   * @param \Drupal\skilling\Utilities $skillingUtilities
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   * @param \Drupal\skilling\Access\FilterUserInputInterface $filterInputService
   *   Input filter service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SkillingCurrentUser $skilling_current_user,
//    History $historyService,
    SkillingUtilities $skillingUtilities,
    SkillingAjaxSecurityInterface $ajaxSecurityService,
    FilterUserInputInterface $filterInputService
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingCurrentUser = $skilling_current_user;
//    $this->historyService = $historyService;
    $this->skillingUtilities = $skillingUtilities;
    $this->ajaxSecurityService = $ajaxSecurityService;
    $this->filterInputService = $filterInputService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('skilling.skilling_current_user'),
//      $container->get('skilling.history'),
      $container->get('skilling.utilities'),
      $container->get('skilling.ajax_security'),
      $container->get('skilling.filter_user_input')
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   */
  public function recordNodeView(Request $request) {

    return new JsonResponse('OK');

    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/history/node-view']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in recordNodeView');
    }
    $nid = $request->get('nodeId');
    // Filter with Stripping.
    $nid = $this->filterInputService->filterUserContent($nid);
    if (!is_numeric($nid) || $nid < 1) {
      throw new AccessDeniedException('Access denied, nid fail in recordNodeView');
    }
    $this->historyService->recordNidViewEvent($nid);
    return new JsonResponse('OK');
  }

  /**
   * Record an answer to a FiB.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   */
  public function recordFibAnswer(Request $request) {

    return new JsonResponse('OK');

    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request,
      ['POST'],
      ['/skilling/history/fib-answer']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in recordFibAnswer');
    }
    // Only for students.
    if (!$this->skillingCurrentUser->isStudent()) {
      return new JsonResponse('OK');
    }
    $fibNid = $request->get('fibNid');
    if (!$fibNid || !is_numeric($fibNid)) {
      $message = 'Bad fib nid:' . Html::escape($fibNid);
      return new JsonResponse($message);
    }
    // Check that it's for a FiB node.
    /** @var \Drupal\Node\NodeInterface $fib */
    $fib = $this->entityTypeManager->getStorage('node')->load($fibNid);
    if (!$fib) {
      $message = 'Unknown fib nid:' . Html::escape($fibNid);
      return new JsonResponse($message);
    }
    if ($fib->bundle() !== SkillingConstants::FILL_IN_THE_BLANK_CONTENT_TYPE) {
      $message = 'Not a fib nid:' . Html::escape($fibNid);
      return new JsonResponse($message);
    }
    // Get the user's answer.
    $answer = $request->get('answer');
    // Filter it.
    $answer = $this->filterInputService->filterUserContent($answer);
    if (!$answer) {
      $message = 'Missing FIB answer, FIB nid: ' . Html::escape($fibNid);
      return new JsonResponse($message);
    }
    $correct = $request->get('correct');
    if ($correct === '') {
      $message = 'Missing FIB correct, FIB nid: ' . Html::escape($fibNid);
      return new JsonResponse($message);
    }
    try {
      $internalName = $fib->get(SkillingConstants::FIELD_INTERNAL_NAME)->value;
      $this->historyService->recordFibAnswer($fibNid, $internalName, $answer);
    }
    catch (InvalidPluginDefinitionException $e) {
      $message = 'InvalidPluginDefinitionException, FIB nid: ' . Html::escape($fibNid)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    catch (EntityStorageException $e) {
      $message = 'EntityStorageException, FIB nid: ' . Html::escape($fibNid)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    catch (SkillingUnknownValueException $e) {
      $message = 'SkillingUnknownValueException, FIB nid: ' . Html::escape($fibNid)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    return new JsonResponse('OK');
  }

  /**
   * Record an answer to an MCQ.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response, ok or error.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function recordMcqAnswer(Request $request) {

    return new JsonResponse('OK');

    $passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
      $request, ['POST'], ['/skilling/save-mcq-response']
    );
    if (!$passedBasicSecurity) {
      throw new AccessDeniedException('Access denied, sec fail in recordMcqAnswer');
    }
    // Only for students.
    if (!$this->skillingCurrentUser->isStudent()) {
      return new JsonResponse('OK');
    }
    // Get the internal name of the MCQ.
    $internalName = $request->get('internalName');
    // Check that MCQ with that name exists.
    $mcqIds = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      // Is an exercise.
      ->condition('type', SkillingConstants::MCQ_CONTENT_TYPE)
      // Has the right internal name.
      ->condition('field_internal_name', $internalName)
      ->execute();
    if (!$mcqIds) {
      $message = 'Bad internal name: ' . Html::escape($internalName);
      return new JsonResponse($message);
    }
    // Have the id of the MCQ.
    $mcqId = reset($mcqIds);
    // Get the response the student chose.
    $response = $request->get('response');
    // Clean.
    $response = $this->filterInputService->filterUserContent($response);
    try {
      $this->historyService->recordMcqAnswer($mcqId, $internalName, $response);
    }
    catch (InvalidPluginDefinitionException $e) {
      $message = 'InvalidPluginDefinitionException, MCQ nid: ' . Html::escape($mcqId)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    catch (EntityStorageException $e) {
      $message = 'EntityStorageException, MCQ nid: ' . Html::escape($mcqId)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    catch (SkillingUnknownValueException $e) {
      $message = 'SkillingUnknownValueException, MCQ nid: ' . Html::escape($mcqId)
        . $e->getMessage();
      return new JsonResponse($message);
    }
    return new JsonResponse('OK');
  }

  /**
   * Show a student his/her own history.
   *
   * The code loads and shows a view, rather than let Views handle the
   * route and menu item itself. The view uses a contextual filter for
   * the user the history events are about. The filter uses a default argument
   * of the current user's uid. Handling the route here prevents
   * users from appending a uid to the URL, and getting another user's
   * history.
   *
   * @return array|\Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   Renderable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   */
  public function showStudentHistoryForStudent() {

    return new JsonResponse('OK');

    if (!$this->skillingCurrentUser->isStudent()) {
      return [
        '#markup' => $this->t('Sorry, this view is for students only.'),
      ];
    }
    $args = [$this->skillingCurrentUser->id()];
    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView('history_for_students');
    $result = $this->t('History attachment error.');
    if (is_object($view)) {
      $view->setArguments($args);
      $view->setDisplay('embed');
      $view->preExecute();
      $view->execute();
      $result = $view->buildRenderable('embed', $args);
      // Record the event.
      $this->historyService->createHistoryNode(
        t('View history'), SkillingConstants::HISTORY_EVENT_VIEW_HISTORY,
        $this->skillingCurrentUser->id(), [], ''
      );
    }
    return $result;
  }

}
