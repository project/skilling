<?php

namespace Drupal\skilling;

use DateTime;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Token;
use \Drupal\Core\Render\BubbleableMetadata;
use Drupal\skilling\Exception\SkillingException;
use Drupal\skilling\SkillingClass\SkillingCurrentClass;

/**
 * Class SkillingTokens.
 */
class SkillingTokens implements SkillingTokensInterface {

  const TRUE_VALUE = 'TRUE';
  const FALSE_VALUE = 'FALSE';
  const NOT_AVAILABLE_VALUE = 'FALSE';

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Drupal\skilling\SkillingCurrentUser definition.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $skillingCurrentUser;

  /**
   * Current class.
   *
   * @var SkillingCurrentClass
   */
  protected $currentClass;
  /**
   * Drupal\skilling\Submissions definition.
   *
   * @var \Drupal\skilling\Submissions
   */
  protected $submissionsService;
  /**
   * Drupal\skilling\Utilities definition.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * Constructs a new SkillingTokens object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\skilling\SkillingCurrentUser $skillingCurrentUser
   * @param \Drupal\skilling\SkillingClass\SkillingCurrentClass $currentClass
   * @param \Drupal\skilling\Submissions $skilling_submissions
   * @param \Drupal\skilling\Utilities $skilling_utilities
   * @param \Drupal\Core\Utility\Token $token
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingCurrentUser $skillingCurrentUser,
    SkillingCurrentClass $currentClass,
    Submissions $skilling_submissions,
    Utilities $skilling_utilities,
    Token $token
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingCurrentUser = $skillingCurrentUser;
    $this->currentClass = $currentClass;
    $this->submissionsService = $skilling_submissions;
    $this->skillingUtilities = $skilling_utilities;
    $this->tokenService = $token;
  }

  /**
   * Define Skilling tokens.
   *
   * @return array
   *   Token definitions.
   */
  public function tokenInfo() {
    $info = [];
    // General Skilling token category.
    // Not specific to exercises, or submissions.
//    $info['types']['skilling'] = ['name' => t('Skilling'), 'description' => t('Skilling tokens of doom')];
//    // To help authors test tokens.
//    $info['tokens']['skilling']['doom-song'] =
//      [
//        'name' => t('Doom song'),
//        'description' => t('A token to sing the Doom Song. So authors can try out the token system.'),
//      ];
    // Tokens about exercises.
    $info['types']['exercise'] = ['name' => t('Exercise'), 'description' => t('Exercise tokens.')];
    $info['tokens']['exercise']['submitted'] =
      [
        'name' => t('Submitted'),
        'description' => t('Has the current user submitted a given exercise?'),
        'dynamic' => TRUE,
      ];
    $info['tokens']['exercise']['completed'] =
      [
        'name' => t('Completed'),
        'description' => t('Has the current user completed a given exercise?'),
        'dynamic' => TRUE,
      ];
    // Tokens about classes and calendar events.
//    $info['types']['class'] = ['name' => t('Class'), 'description' => t('Class tokens.')];
//    $info['tokens']['class']['event-passed'] =
//      [
//        'name' => t('Event passed'),
//        'description' => t('The date for an event is in the past.'),
//        'dynamic' => TRUE,
//      ];
    return $info;
  }

  /**
   * Alter existing token definitions.
   *
   * @param array $data
   *   Existing token data.
   */
  public function tokenInfoAlter(array &$data) {
    // Add a token to current-user for days since last login.
    $data['tokens']['current-user']['days-since-login'] = [
      'name' => t("Days since current user's last login."),
      'description' => t("Number of days since current user's last login. -1 for never logged in before."),
    ];
    // Add tokens to test for roles.
    $data['tokens']['current-user']['is-author'] = [
      'name' => t('Author role'),
      'description' => t('Does the current user have the author role?'),
    ];
    $data['tokens']['current-user']['is-reviewer'] = [
      'name' => t('Reviewer role'),
      'description' => t('Does the current user have the reviewer role?'),
    ];
    $data['tokens']['current-user']['is-instructor'] = [
      'name' => t('Instructor role'),
      'description' => t('Does the current user have the instructor role?'),
    ];
    $data['tokens']['current-user']['is-grader'] = [
      'name' => t('Grader role'),
      'description' => t('Does the current user have the grader role?'),
    ];
    $data['tokens']['current-user']['is-student'] = [
      'name' => t('Student role'),
      'description' => t('Does the current user have the student role?'),
    ];
    $data['tokens']['current-user']['is-anonymous'] = [
      'name' => t('Anonymous'),
      'description' => t('Is the current user not logged in?'),
    ];
    $data['tokens']['current-user']['number-exercises-with-submissions'] = [
      'name' => t('Number of exercises with submissions'),
      'description' => t('Number of exercises the current user has submitted solutions for. Does not include resubmissions.'),
    ];
    $data['tokens']['current-user']['number-exercises-completed'] = [
      'name' => t('Number of exercises completed'),
      'description' => t('Number of exercises the current user has completed.'),
    ];
    $data['tokens']['current-user']['instructor-first-name'] = [
      'name' => t("Instructor's first name."),
      'description' => t("The first name of the current user's instructor."),
    ];
    $data['tokens']['current-user']['instructor-last-name'] = [
      'name' => t("Instructor's last name."),
      'description' => t("The last name of the current user's instructor."),
    ];
    $data['tokens']['current-user']['instructor-email'] = [
      'name' => t("Instructor's email address."),
      'description' => t("The email address of the current user's instructor."),
    ];
  }

  /**
   * Supply replacement data for Skilling tokens.
   *
   * @param string $type
   *   Token type.
   * @param array $tokens
   *   Array of tokens to be replaced.
   * @param array $data
   *   Associative array of data objects to be used when
   *   generating replacement values.
   * @param array $options
   *   Associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   The bubbleable metadata.
   *
   * @return array
   *   Associative array of replacement values.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function tokens($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata) {
    $replacements = [];
    switch ($type) {
//      case 'skilling':
//        $replacements = $this->generalTokens($tokens, $data, $options, $bubbleableMetadata);
//        break;

      case 'current-user':
        $replacements = $this->currentUserTokens($tokens, $data, $options, $bubbleableMetadata);
        break;

      case 'exercise':
        $replacements = $this->exerciseTokens($tokens, $data, $options, $bubbleableMetadata);
        break;

      case 'class':
        $replacements = $this->classTokens($tokens, $data, $options, $bubbleableMetadata);
        break;

    }
    return $replacements;
  }

  /**
   * Process general tokens.
   *
   * @param array $tokens
   *   Array of tokens to be replaced.
   * @param array $data
   *   Associative array of data objects to be used when
   *   generating replacement values.
   * @param array $options
   *   Associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   The bubbleable metadata.
   * @return array
   *   Associative array of replacement values.
   */
//  protected function generalTokens($tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata) {
//    foreach ($tokens as $name => $original) {
//      // What's the token?
//      switch ($name) {
//        case 'doom-song':
//          $replacements[$original] = 'Doom de doom doom doom de doooooom dooom.';
//          break;
//      }
//    }
//    return $replacements;
//  }

  /**
   * Process current user tokens.
   *
   * @param array $tokens
   *   Array of tokens to be replaced.
   * @param array $data
   *   Associative array of data objects to be used when
   *   generating replacement values.
   * @param array $options
   *   Associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   The bubbleable metadata.
   *
   * @return array
   *   Associative array of replacement values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  protected function currentUserTokens(array $tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata) {
    $replacements = NULL;
    foreach ($tokens as $name => $original) {
      $name = strtolower($name);
      switch ($name) {
        case 'days-since-login':
          $lastLogin = $this->skillingCurrentUser->getDrupalUser()->getLastLoginTime();
          if (!$lastLogin) {
            $replacements[$original] = 'never';
          }
          else {
            $seconds = time() - $lastLogin;
            $minutes = (int) ($seconds / 60);
            $hours = (int) ($minutes / 60);
            $days = (int) ($hours / 24);
            $replacements[$original] = $days;
          }
          break;

        case 'is-author':
          $replacements[$original] = $this->skillingCurrentUser->isAuthor()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'is-reviewer':
          $replacements[$original] = $this->skillingCurrentUser->isReviewer()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'is-instructor':
          $replacements[$original] = $this->skillingCurrentUser->isInstructor()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'is-grader':
          $replacements[$original] = $this->skillingCurrentUser->isGrader()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'is-student':
          $replacements[$original] = $this->skillingCurrentUser->isStudent()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'is-anonymous':
          $replacements[$original] = $this->skillingCurrentUser->isAnonymous()
            ? self::TRUE_VALUE : self::FALSE_VALUE;
          break;

        case 'number-exercises-with-submissions':
          if (!$this->skillingCurrentUser->isStudent()) {
            $replacements[$original] = self::NOT_AVAILABLE_VALUE;
          }
          else {
            // Current user is a student.
            $submissionStatuses = $this->submissionsService->getSubmissionStatusesForStudent(
              $this->skillingCurrentUser
            );
            $numberSubmissions = count($submissionStatuses);
            $replacements[$original] = $numberSubmissions;
          }
          break;

        case 'number-exercises-completed':
          if (!$this->skillingCurrentUser->isStudent()) {
            $replacements[$original] = self::NOT_AVAILABLE_VALUE;
          }
          else {
            // Current user is a student.
            $submissionStatuses = $this->submissionsService->getSubmissionStatusesForStudent(
              $this->skillingCurrentUser
            );
            $numberComplete = 0;
            foreach ($submissionStatuses as $submissionStatus) {
              if ($submissionStatus === SkillingConstants::EXERCISE_SUBMISSION_COMPLETE) {
                $numberComplete++;
              }
            }
            $replacements[$original] = $numberComplete;
          }
          break;

        case 'instructor-first-name':
          $replacements[$original] = $this->getInstructorAttribute('first name');
          break;

        case 'instructor-last-name':
          $replacements[$original] = $this->getInstructorAttribute('last name');
          break;

        case 'instructor-email':
          $replacements[$original] = $this->getInstructorAttribute('email');
          break;

      }
    }
    if (!is_null($replacements)) {
      return $replacements;
    }
  }

  /**
   * Get an attribute of the current user's instructor.
   *
   * NB: gets the first instructor only.
   *
   * TODO: multi instrutor thing. Field to mark main one?
   *
   * Return a not-available value when appropriate.
   *
   * @param string $attributeName
   *   Attribute name, e.g., 'first name'.
   *
   * @return string
   *   Attribute value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  protected function getInstructorAttribute($attributeName) {
    $result = self::NOT_AVAILABLE_VALUE;
    $instructor = $this->currentClass->getInstructor();
    if ($instructor) {
      switch ($attributeName) {
        case 'first name':
          $result = $instructor->field_first_name->value;
          break;

        case 'last name':
          $result = $instructor->field_last_name->value;
          break;

        case 'email':
          $result = $instructor->getEmail();
          break;
      }
      if (!$result) {
        $result = self::NOT_AVAILABLE_VALUE;
      }
    }
    return $result;
  }

  /**
   * Process exercise tokens.
   *
   * @param array $tokens
   *   Array of tokens to be replaced.
   * @param array $data
   *   Associative array of data objects to be used when
   *   generating replacement values.
   * @param array $options
   *   Associative array of options for token replacement.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   The bubbleable metadata.
   *
   * @return array
   *   Associative array of replacement values.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingUnknownValueException
   * @throws \Drupal\skilling\Exception\SkillingValueMissingException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  protected function exerciseTokens(array $tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata) {
    $replacements = NULL;
    foreach ($tokens as $name => $original) {
      $submittedTokens = $this->tokenService->findWithPrefix($tokens, 'submitted');
      if ($submittedTokens) {
        // Submitted token applies to students. For others, return NA value.
        if (!$this->skillingCurrentUser->isStudent()) {
          $replacements[$original] = self::NOT_AVAILABLE_VALUE;
        }
        else {
          // Is a student.
          // Find the exercise internal name.
          $pieces = explode(':', $name);
          $exerciseInternalName = 'unknown';
          if (isset($pieces[1])) {
            $exerciseInternalName = $pieces[1];
          }
          // Find the exercise.
          $exercise = $this->skillingUtilities->getPublishedExerciseWithInternalName($exerciseInternalName);
          if ($exercise) {
            $exerciseNid = $exercise->id();
            $submissionStatuses = $this->submissionsService->getSubmissionStatusesForStudent(
              $this->skillingCurrentUser
            );
            $status = null;
            foreach($submissionStatuses as $exerId => $exerSubmissionStatus) {
              if ($exerId == $exerciseNid) {
                $status = $exerSubmissionStatus;
              }
            }
            if (is_null($status)) {
              $status = SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION;
            }
            // Any status but no-submission shows there has been a submission.
            $replacements[$original] = ($status === SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION)
              ? self::FALSE_VALUE : self::TRUE_VALUE;
          }
        }
      }
      $completedTokens = $this->tokenService->findWithPrefix($tokens, 'completed');
      if ($completedTokens) {
        // Completed token applies to students. For others, return NA value.
        if (!$this->skillingCurrentUser->isStudent()) {
          $replacements[$original] = self::NOT_AVAILABLE_VALUE;
        }
        else {
          // Is a student.
          // Has an exercise been completed?
          $exerciseInternalName = array_keys($completedTokens)[0];
          // Find the exercise.
          $exercise = $this->skillingUtilities->getPublishedExerciseWithInternalName($exerciseInternalName);
          if ($exercise) {
            $exerciseNid = $exercise->id();
            // Get the submission status for the exercise.
            $submissionStatuses = $this->submissionsService->getSubmissionStatusesForStudent(
              $this->skillingCurrentUser
            );
            $status = null;
            foreach($submissionStatuses as $exerId => $exerSubmissionStatus) {
              if ($exerId == $exerciseNid) {
                $status = $exerSubmissionStatus;
              }
            }
            if (is_null($status)) {
              $status = SkillingConstants::EXERCISE_SUBMISSION_NO_SUBMISSION;
            }
            $replacements[$original] = ($status === SkillingConstants::EXERCISE_SUBMISSION_COMPLETE)
              ? self:: TRUE_VALUE : self::FALSE_VALUE;
          }
        }
      }
    }
    if (!is_null($replacements)) {
      return $replacements;
    }
  }

}
