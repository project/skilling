<?php

namespace Drupal\skilling;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslationManager;

/**
 * A service to check whether an internal name is OK.
 *
 * Authors give internal names to exercises, patterns, and other
 * entities. Authors use internal names in tags.
 *
 * @package Drupal\skilling
 */
class InternalNameChecker {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $translationService;

  /**
   * Make an internal name checker.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\skilling\Utilities $skillingUtilities
   *   The Skilling utilities service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $translationService
   *   Translation service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Utilities $skillingUtilities,
    TranslationManager $translationService
  ) {
    // Remember services.
    $this->entityTypeManager = $entityTypeManager;
    $this->skillingUtilities = $skillingUtilities;
    $this->translationService = $translationService;
  }

  /**
   * Get the error message for a new internal name.
   *
   * @param string $internalName
   *   The name to check.
   * @param int $nidBeingEdited
   *   The nid of the node being edited. If null, there is no node
   *   being edited.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   Error message, or NULL if no error.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkNewInternalName($internalName, $nidBeingEdited) {
    // Check the format of the name.
    if (!$this->isInternalNameFormatOk($internalName)) {
      $message = $this->translationService->translate(
        'Sorry, internal names should contain only lowercase letters, '
        . 'digits, and underscores. They should be @max characters '
        . 'or less.',
        ['@max' => SkillingConstants::MAX_LENGTH_INTERNAL_NAME]
      );
      return $message;
    }
    //Get nids of nodes using the name.
    $nids = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('field_internal_name', $internalName)
      ->execute();
    if ( count($nids) === 0 ) {
      //Name is not in use.
      return NULL;
    }
    if (count($nids) > 1) {
      //Name is in use by more than one node. Should not happen.
      $message = $this->translationService->translate(
        'The internal name is in use by more than one node. '
        . 'That should not happen. Please send a screen shot of your '
        . 'entire browser window, including the URL, to someone.'
      );
      return $message;
    }
    //It's OK if the user is editing the node that is already using
    //the name.
    $nidUsingName = current($nids);
    if ( $nidUsingName !== $nidBeingEdited ) {
      $message = "Sorry, that internal name is in use. ";
      return $this->translationService->translate($message);
    }
    //All tests passed. No error message.
    return NULL;
  }

  /**
   * Check whether a string has a valid format for an internal name.
   *
   * @param string $name Name to check.
   *
   * @return bool True if valid, else false.
   */
  public function isInternalNameFormatOk($name) {
    //Check for invalid chars.
    $len = strlen($name);
    for ($i = 0; $i < $len; $i++) {
      $char = $name[$i];
      $number = $char >= '0' && $char <= '9';
      $letter = $char >= 'a' && $char <= 'z';
      $underScore = $char == '_';
      $charOk = $number || $letter || $underScore;
      if (!$charOk) {
        return FALSE;
      }
    }
    //Check length.
    if (strlen($name) > SkillingConstants::MAX_LENGTH_INTERNAL_NAME) {
      return FALSE;
    }
    return TRUE;
  }

}
