<?php
namespace Drupal\skilling;

use Drupal\skilling\Exception\SkillingInvalidValueException;
use Drupal\skilling\Exception\SkillingNotFoundException;
use Drupal\skilling\Exception\SkillingValueMissingException;
use Drupal\skilling\Exception\SkillingWrongTypeException;
use Drupal\skilling\Utilities as SkillingUtilities;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingClass\SkillingClass;

/**
 * Service to make a SkillingUser object. There is just one instance
 * of this service (as usual), but code may need more than
 * one instance of SkillingUser.
 *
 * @package Drupal\skilling
 */
class SkillingUserFactory {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * Default language service.
   *
   * @var \Drupal\Core\Language\LanguageDefault
   */
//  protected $languageDefault;

  //  /**
//   * @var LoggerChannel
//   */
//  protected $logger;

  /**
   * SkillingUserFactory constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\skilling\Utilities $skillingUtilities
//   * @param \Drupal\Core\Logger\LoggerChannel $logger
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skillingUtilities//,
//    LanguageDefault $languageDefault
  ) { //, LoggerChannel $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->skillingUtilities = $skillingUtilities;
//    $this->languageDefault = $languageDefault;
//    $this->logger = $logger;
  }

  /**
   * Make a SkillingUser object.
   *
   * @param $thingToWrap
   *
   * @return \Drupal\skilling\SkillingUser A SkillingUser object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingException
   * @throws \Drupal\skilling\Exception\SkillingInvalidValueException
   * @throws \Drupal\skilling\Exception\SkillingNotFoundException
   * @throws \Drupal\skilling\Exception\SkillingWrongTypeException
   */
  public function makeSkillingUser($thingToWrap) {
    $user = NULL;
    if (is_numeric($thingToWrap)) {
      // Passed a uid, 0 for anon.
      $userId = $thingToWrap;
      // Validate the id.
      if ($userId < 0) {
        throw new SkillingInvalidValueException(
          'Expected user id, got ' . $userId,
          __FILE__, __LINE__
        );
      }
      // Could be for anon.
      if ($userId != 0) {
        /** @var \Drupal\user\UserInterface $user */
        $user = $this->entityTypeManager
          ->getStorage('user')->load($userId);
        if (!$user) {
          throw new SkillingNotFoundException(
            'User with id not found. Id: ' . $userId,
            __FILE__, __LINE__
          );
        }
        // If user, should have a known method.
        if (!method_exists($user, 'getAccountName')) {
          throw new SkillingWrongTypeException(
            'Need user, got: ' . $thingToWrap,
            __FILE__, __LINE__
          );
        }
      }
    } // end is numeric
    else {
      // Found nothing?
      if (is_null($thingToWrap)) {
        throw new SkillingNotFoundException(
          'User making failed. Got null',
          __FILE__, __LINE__
        );
      }
      // Is user object?
      $isUser = substr(get_class($thingToWrap), -5) === '\User';
      if (!$isUser) {
        throw new SkillingWrongTypeException(
          'Need user, got: ' . $thingToWrap,
          __FILE__, __LINE__
        );
      }
      $userId = $thingToWrap->id();
    }
    $sU = new SkillingUser(
      $userId, $this->entityTypeManager, $this->skillingUtilities);
    return $sU;
  }

}
