<?php
namespace Drupal\skilling\SkillingParser;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
use Drupal\Core\Utility\Token;
use Drupal\skilling\SkillingConstants;
use Drupal\skilling\SkillingCurrentUser;
use Drupal\skilling\SkillingUserFactory;
use Drupal\skilling\Utilities as SkillingUtilities;

/**
 * The Skilling parser.
 */
class SkillingParser {

  const CONDITION_TEST_PARAM_NAME = 'condition';
  const ROLE_TEST_PARAM_NAME = 'roles';

  const OPTION_PARSING_ERROR_CLASS = 'skilling-custom-tag-option-parse-error';

  protected $errors = [];

  /**
   * Token replacement values passed to the expression parser.
   *
   * Keys are strings (mangled token names), values are
   * mixed.
   *
   * @var array
   */
  protected $tokenReplacements = [];

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

//  /**
//   * The expression language service.
//   *
//   * @var \Symfony\Component\DependencyInjection\ExpressionLanguage
//   */
//  protected $expressionLanguageService;

  /**
   * The Skilling current user service.
   *
   * @var \Drupal\skilling\SkillingCurrentUser
   */
  protected $currentUser;

  /**
   * The Skilling utilities service.
   *
   * @var \Drupal\skilling\Utilities
   */
  protected $skillingUtilities;

  /**
   * The user factory service.
   *
   * @var \Drupal\skilling\SkillingUserFactory
   */
  protected $skillingUserFactory;

  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;

  protected $customTagPlugins = [];

  /**
   * Parser constructor.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\skilling\Utilities $skilling_utilities
   *   The utilities service.
   * @param \Drupal\skilling\SkillingCurrentUser $currentUser
   *   The current user service.
   * @param \Drupal\skilling\SkillingUserFactory $skillingUserFactory
   *   The user factory service.
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
   */
  public function __construct(
    Token $token,
    EntityTypeManagerInterface $entity_type_manager,
    SkillingUtilities $skilling_utilities,
    SkillingCurrentUser $currentUser,
    SkillingUserFactory $skillingUserFactory,
    SkillingAjaxSecurityInterface $ajaxSecurityService
  ) {
    // Add local versions of Textile and ExpressionLanguage parsers to
    // autoloader.
    $loader = \Drupal::service('class_loader');
    $loader->addPsr4(
      'Netcarver\Textile\\',
      __DIR__ . '/Netcarver/Textile/src/Netcarver/Textile'
    );
//    $loader->addPsr4('Symfony\Component\ExpressionLanguage\\', __DIR__ . '/expression-language');
//    $loader->addPsr4('Vanderlee\Phpexpression\\', __DIR__ . '/Vanderlee/PHP-expression/classes');
    require_once 'Vanderlee/PHP-expression/classes/Expression.php';
    require_once 'Vanderlee/PHP-expression/classes/ExpressionException.php';
    $this->tokenService = $token;
    $this->entityTypeManager = $entity_type_manager;
    $this->expressionLanguageService = new \Expression();
    $this->skillingUtilities = $skilling_utilities;
    $this->currentUser = $currentUser;
    $this->skillingUserFactory = $skillingUserFactory;
    $this->ajaxSecurityService = $ajaxSecurityService;
  }

  /**
   * Remove leading and trailing whitespace from every line in a string.
   * Lines are defined by EOLs.
   *
   * @param string $source String with lines to trim.
   *
   * @return string Source with whitespace and comments removed.
   */
  protected function trimWhitespace($source) {
    // Remove CRs.
    $source = str_replace("\r", '', $source);
    // Explode into array, one element for each line.
    $lines = explode("\n", $source);
    $openCodeTagRegex = '/^\s*code.\s*$/miU';
    $closeCodeTagRegex = '/^\s*\/code.\s*$/miU';
    // Chars to trim from left and right of text lines.
    // A0 is nbsp. C2 is..., er, don't know what this appears. TODO: why?
    $charsToTrimWithSpaces = " \t\n\r\0\x0B\xA0\xC2";
    $charsToTrimWithoutSpaces = "\t\n\r\0\x0B\xC2";
    // Strip whitespace from each line, except for code tag.
    $linesOut = [];
    $lineNumber = 0;
    // Flag is on when processing code tag. In that case,
    // don't strip leading spaces.
    $inCodeTag = FALSE;
    while ($lineNumber < count($lines)) {
      if (preg_match($openCodeTagRegex, $lines[$lineNumber])) {
        $inCodeTag = TRUE;
      }
      if (preg_match($closeCodeTagRegex, $lines[$lineNumber])) {
        $inCodeTag = FALSE;
      }
      // Trim excess chars from right of line.
      $line = rtrim($lines[$lineNumber], $charsToTrimWithSpaces);
      if ($inCodeTag) {
        // Left trim all but spaces.
        $line = ltrim($line, $charsToTrimWithoutSpaces);
      }
      else {
        // Left trim all, including spaces.
        $line = ltrim($line, $charsToTrimWithSpaces . ' ');
      }
      $linesOut[] = $line;
      $lineNumber++;
    }
    // Convert from array back into one string.
    $result = implode("\n", $linesOut);
    return $result;
  }

  protected function lineIsEmpty($line) {
    $result = strlen($this->trimLineWhitespace($line)) === 0;
    return $result;
  }

  /**
   * Check whether a line of text has an option that the code tag uses.
   *
   * @param string $line Text to check.
   *
   * @return bool True is param contains a known option.
   */
  protected function lineHasCodeOption($line) {
    foreach (SkillingConstants::LINE_NUMBERS_ALTS as $optionMarker) {
      if (stripos($line, $optionMarker) !== FALSE) {
        return TRUE;
      }
    }
    foreach (SkillingConstants::START_ALTS as $optionMarker) {
      if (stripos($line, $optionMarker) !== FALSE) {
        return TRUE;
      }
    }
    return FALSE;
  }

  protected function trimLineWhitespace($string) {
    $result = trim($string, " \t\n\r\0\x0B\xA0\xC2");
    return $result;
  }

  /**
   * Format an error message about a custom tag option.
   *
   * @param string $message
   *   Message to show.
   *
   * @return string
   *   Formatted message.
   */
  protected function formatOptionParseError($message) {
    $result = "\np(" . self::OPTION_PARSING_ERROR_CLASS . "). $message\n\n";
    $this->errors[] = $result;
    return $result;
  }

  /**
   * Parse custom tags to Textile.
   *
   * @param string $source
   *   Source to parse to plain Textile.
   * @param \Drupal\node\NodeInterface $node
   *   Node with the content.
   *
   * @return string
   *   Result.
   *
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  protected function parseCustomTags($source, $node) {
    // Add LF to start and end of source, in case custom tag is first or last.
    $source = "\n" . $source . "\n";
    // Run through the custom tags.
    $customTagManager = \Drupal::service('plugin.manager.skilling.custom_tag');
    // Get the list of all the custom tag plugins defined on the system from the
    // plugin manager.
    $tagPluginDefinitions = $customTagManager->getDefinitions();
    // Make plugin instances for all the plugins.
//    $customTagPlugins = [];
    foreach ($tagPluginDefinitions as $pluginId => $tagPluginDefinition) {
      $plugin = $customTagManager->createInstance($pluginId);
      if (!key_exists($plugin->tag(), $this->customTagPlugins)) {
        /* @var \Drupal\skilling\Plugin\SkillingCustomTagBase $plugin */
        // Tell the plugin what parser is using it.
        $plugin->setParser($this);
        //      // Hand in the AJAX security service, in case it is needed to
        //      // filter user input.
        //      $plugin->setAjaxSecurityService($this->ajaxSecurityService);
        $plugin->preparse();
        $this->customTagPlugins[$plugin->tag()] = $plugin;
      }
    }
    foreach ($this->customTagPlugins as $tagType => $customTagPlugin) {
      // Keep processing $source, until don't find custom tag.
      $startChar = 0;
      $openTagText = $tagType . ".";
      list($gotOne, $tagPos) = $this->findOpenTag(
        $source, $tagType, $startChar
      );
      while ($gotOne) {
        // Found one.
        // Flag to show whether there was a test option, and the tag
        // failed the test.
        $failedTestOption = FALSE;
        // Get tag's options. Options on the following lines until there's
        // an MT line.
        // Look from the end of the tag until find two LFs in a row - MT line.
        // Accumulate chars until find it.
        $tagEndPoint = $tagPos + strlen($openTagText);
        $optionChars = '';
        $next2Chars = substr($source, $tagEndPoint, 2);
        while ($next2Chars !== "\n\n" && $tagEndPoint < strlen($source)) {
          $optionChars .= substr($source, $tagEndPoint, 1);
          $tagEndPoint++;
          $next2Chars = substr($source, $tagEndPoint, 2);
        }
        $options = [];
        // Parse the options, if there are any.
        $optionChars = trim($optionChars);
        // Error message for parsing of options, if it happens.
        $optionsParseErrorMessage = '';
        if (strlen($optionChars) > 0) {
          // Try parsing tag options.
          list($options, $optionsParseErrorMessage) = $this->parseOptions(
            $optionChars
          );
          // Is there no error, and a test?
          if (
              strlen($optionsParseErrorMessage) === 0
              && isset($options[self::CONDITION_TEST_PARAM_NAME])) {
            $expToEval = $options[self::CONDITION_TEST_PARAM_NAME];
            // FALSE is a special case for exp lang to get right.
            if ($expToEval === FALSE) {
              $expToEval = 'false';
            }
            // Replace non-breaking spaces with actual spaces.
            $expToEval = str_replace("\xc2\xa0", ' ', $expToEval);
            $expToEval = trim($expToEval);
            // Eval the condition expression.
            $expToEval = str_ireplace('true', '1', $expToEval);
            $expToEval = str_ireplace('false', '0', $expToEval);
            try {
              $result = $this->expressionLanguageService->evaluate(
                $expToEval //, $this->tokenReplacements
              );
              // Was is falsey?
              if (!$result) {
                $failedTestOption = TRUE;
              }
            }
            catch (\Exception $e) {
              // Error during expression evaluation.
              $optionsParseErrorMessage = 'Error in expression: ' . $expToEval
                . ': ' . $e->getMessage();
            }
          }
          // Test for role limits.
          if (
              strlen($optionsParseErrorMessage) === 0
              && isset($options[self::ROLE_TEST_PARAM_NAME])) {
            // Get the roles the author wants to test for.
            $rolesToTestFor = $options[self::ROLE_TEST_PARAM_NAME];
            if (!$rolesToTestFor) {
              // No roles.
              $optionsParseErrorMessage = 'Missing roles in roles test';
            }
            else {
              $rolesInTag = explode(',', $rolesToTestFor);
              // Validate the roles.
              $rolesOk = TRUE;
              $validatedRolesInTag = [];
              foreach ($rolesInTag as $role) {
                $role = strtolower(trim($role));
                if (!in_array($role, SkillingConstants::SKILLING_ROLES)) {
                  $rolesOk = FALSE;
                  $optionsParseErrorMessage = t('Unknown role: @r', $role);
                  break;
                }
                $validatedRolesInTag[] = $role;
              }
              if ( $rolesOk ) {
                //Does the user have one of the roles?
                $userRoles = $this->currentUser->getRoles();
                $commonRoles = array_intersect($userRoles, $validatedRolesInTag);
                $failedTestOption = ( count($commonRoles) === 0 );
              }
            } //End there are roles to test for.
          } //There is a role test.
        } //End there are option chars.
        //Find the close tag, if there is one, and the content between end
        //of options, and close tag.
        $tagContent = '';
        if ( $customTagPlugin->hasCloseTag() ) {
          $lookFor = $tagType . ".";
          $openTagCount = 1;
          //Where the content for the tag starts.
          $contentStartPos = $tagEndPoint;
          while ($openTagCount > 0) {
            //Find the tag, either opening or closing.
            $loc = stripos($source, $lookFor, $tagEndPoint);
            //If didn't find anything, then missing end tag.
            if ( $loc === FALSE ) {
              return 'h2. Missing/invalid close tag? Missing . at end? Tag: '
                . $tagType;
            }
            if (
                  $this->isTagTextOnLineByItself(
                    $source, $tagType, $loc
                  )
            ) {
              //Is it an opening or closing tag?
              $priorChar = substr($source, $loc - 1, 1);
              $isEndTag = ($priorChar == '/');
              //Change open tag count
              if ($isEndTag) {
                $openTagCount--;
              }
              else {
                $openTagCount++;
              }
              //Remember where the tag started, in case need it to extract
              //content when the loop ends.
              $contentEndPos = $loc;
              if ($isEndTag) {
                $contentEndPos--;
              }
            }
            //Move pointer past the tag just found.
            $tagEndPoint = $loc + strlen($lookFor);
          }
          //Extract the content.
          $tagContent = substr(
            $source, $contentStartPos, $contentEndPos - $contentStartPos);
          //Append parse error, if there was one.
          if ( strlen($optionsParseErrorMessage) > 0 ) {
            $tagContent = $this->formatOptionParseError(
              $optionsParseErrorMessage);
          }
        }
        //Process the tag.
        $replacementContent = '';
        //If test option failed, leave the replacement content MT.
        if ( ! $failedTestOption ) {
          $replacementContent = $customTagPlugin->processTag($tagContent, $options, $node);
        }
        //Replace tag.
        $source = substr($source, 0, $tagPos) . $replacementContent
          . substr($source, $tagEndPoint);
        //Move back to the start, and look for another custom tag.
        $startChar = 0;
        list($gotOne, $tagPos)
          = $this->findOpenTag($source, $tagType, $startChar);
      } //End while there are more tags of $tagType.
    }
    return $source;
  }

  /**
   * Forget definitions of custom tags, so that they are reinitialized and
   * preparse is run next time parseCustomTags is called.
   */
  public function customTagsReset() {
    $this->customTagPlugins = [];
  }

  /**
   * Parse tag params in INI format. Substitute tokens.
   * @param string $optionChars Params as string to parse.
   *
   * @return array [0](array): options. [1](string): error message
   */
  protected function parseOptions($optionChars){
    try {
      // Using raw means that values like "true" come through w/o change.
      $options = parse_ini_string($optionChars, FALSE, INI_SCANNER_RAW);
//      $options = IniExtended::parse($optionChars);
    } catch (\Exception $e) {
      //Make a message to be shown on the content output page.
      $message = 'Tag parameter parse error: ' . $e->getMessage() . ', in: '
        . str_replace("\n", ' _NL_ ', $optionChars);
      return [ [], $message ];
    }
    if ( $options === FALSE ) {
      //Make a message to be shown on the content output page.
      $message = 'Tag parameter parse error in: ' . str_replace("\n", ' _NL_ ', $optionChars);
      return [ [], $message ];
    }
    //Use [default] section as the entire options array.
    if ( isset($options['default']) ) {
      $options = $options['default'];
    }
    //Replace tokens. Convert option name to lowercase.
    $processedOptions = [];
    foreach($options as $index=>$val) {
      //Todo: what happens for invalid token?
      $newVal = $this->tokenService->replace($val);
      $processedOptions[strtolower($index)] = $newVal;
    }
    return [ $processedOptions, '' ];
  }

  /**
   * Find the next open tag, is there is one.
   *
   * @param string $textToSearch The text to search.
   * @param string $tag The text of the tag, e.g., "exercise"
   * @param int $searchPosStart Where to start the search.
   *
   * @return array [0] (boolean): whether found it, [1] (int) if found, where.
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  protected function findOpenTag($textToSearch, $tag, $searchPosStart) {
    $gotOne = false;
    $openTagText = $tag . '.';
    do {
      $tagPos = stripos($textToSearch, $openTagText, $searchPosStart);
      if ( $tagPos === false ) {
        //Nothing found.
        //Move searchPos to after end of textToSearch, so loop ends.
        $searchPosStart = strlen($textToSearch);
      }
      else {
        //Found something, but is it a tag, or random text?
        if ( $this->isTagTextOnLineByItself($textToSearch, $tag, $tagPos) ) {
          $gotOne = TRUE;
        }
        if ( ! $gotOne) {
          //Not a tag.
          //Move the searchPos to after the text that was found.
          $searchPosStart = $tagPos + strlen($openTagText);
        }
      }
    } while ( ! $gotOne && $searchPosStart < strlen($textToSearch) );
    return [$gotOne, $tagPos];
  }

  /**
   * Test whether tag text is on a line by itself. Nothing in front of it, and
   * nothing or only spaces and/or tabs between the end of the tag, and EOL.
   *
   * @param string $textToSearch The content with the tag.
   * @param string $tag The tag's opening text, e.g., "exercise.".
   * @param int $tagPos Where the tag starts.
   *
   * @return bool True if the tag text is on a line by itself.
   * @throws \Drupal\skilling\Exception\SkillingParserException
   */
  protected function isTagTextOnLineByItself($textToSearch, $tag, $tagPos) {
    // Walk back from tagPos to find beginning of line.
    $lineBegins = $tagPos;
    if ($lineBegins > 0) {
      $charPosToCheck = $lineBegins - 1;
      $charToCheck = $textToSearch[$charPosToCheck];
      while ($lineBegins > 0 && $charToCheck !== "\n") {
        $lineBegins--;
        $charPosToCheck = $lineBegins - 1;
        $charToCheck = $textToSearch[$charPosToCheck];
      }
    }
    // Walk forwards from end of tag to the end of the line.
    $lineEnds = $tagPos + strlen($tag);
    $charPosToCheck = $lineEnds + 1;
    $charToCheck = $textToSearch[$charPosToCheck];
    $textLen = strlen($textToSearch);
    while ($lineEnds < $textLen && $charToCheck !== "\n") {
      $lineEnds++;
      $charPosToCheck = $lineEnds + 1;
      $charToCheck = $textToSearch[$charPosToCheck];
    }
    // Get line with tag.
    $lineWithTag = substr($textToSearch, $lineBegins, $lineEnds - $lineBegins + 1);
    $lineByItself = (trim($lineWithTag) === $tag . '.') || (trim($lineWithTag) === '/' . $tag . '.');
    return $lineByItself;
  }

//  protected function parseCustomTokens($source) {
//    //Find the custom tokens that are defined.
//    $customTokenManager = \Drupal::service(
//      'plugin.manager.skilling.custom_token'
//    );
//    //Make plugin instances for all the plugins discovered.
//    $tokenPluginDefinitions = $customTokenManager->getDefinitions();
//    $customTokenPlugins = [];
//    foreach ($tokenPluginDefinitions as $pluginId => $tokenPluginDefinition) {
//      /** @var SkillingCustomTokenInterface $plugin */
//      $plugin = $customTokenManager->createInstance($pluginId);
//      $customTokenPlugins[$plugin->token()] = $plugin;
//    }
//    //Found any?
//    if ( count($customTokenPlugins) === 0 ) {
//      return $source;
//    }
//    //Tokens within [[]] are replaced with their values. This is
//    // for use in content, like:
//    // Hi there, [[student:first:698
  //name]].
//    $result = preg_replace_callback(
//      '/\[(.+)\]/U',
////      '/\[\[(.+)\]\]/U',
//      function ($match) use ($customTokenPlugins) {
//        $token = $match[1];
//        list($problem, $replacement) = $this->processCustomTokenMatch($token, $customTokenPlugins);
//        return $replacement;
//      },
//      $source);
//    //Tokens within {{}} are replaced with their normalized names,
//    //for later use by the expression engine. Their values are stored
//    //for passing to the expression engine. This is for use in conditions,
//    //like:
//    //    condition = {{student:about}}==''
//    $result = preg_replace_callback(
//      '/\{\{(.+)\}\}/U',
//      function ($match) use ($customTokenPlugins) {
//        $token = $match[1];
//        list($problem, $replacement) = $this->processCustomTokenMatch($token, $customTokenPlugins);
//        if ( ! $problem ) {
//          //Remember the token's value for use by the expression
//          //engine.
//          $this->storeTokenValue($token, $replacement);
//        }
//        return $this->normalizeTokenName($token);
//      },
//      $result);
//    return $result;
//  }

  /**
   * Process a token, return its replacement value.
   * Replacement value can be an error message.
   *
   * @param string $fullToken The token, e.g., student:first_name
   * @param array $customTokenPlugins Defined token plugins.
   *
   * @return array Two values:
   *   [0] whether there was a problem. Boolean.
   *   [1] token's replacement value. Mixed.
   */
//  protected function processCustomTokenMatch($fullToken, $customTokenPlugins) {
//    $problem = FALSE;
//    // If no match, return what was passed, unchanged.
//    $replacement = $fullToken;
//    $subtokens = explode(':', $fullToken);
//    //Anything found?
//    if ( isset($subtokens[0]) ) {
//      $tokenObject = $subtokens[0];
//      //Which plugin can interpret the token?
//      if ( isset($customTokenPlugins[$tokenObject]) ) {
//        //Tell the plugin to do something.
//        /** @var SkillingCustomTokenInterface $plugin */
//        $plugin = $customTokenPlugins[$tokenObject];
//        $replacement = $plugin->processToken($subtokens);
//      }
////      else {
////        //Not a plugin for this token.
////        $problem = TRUE;
////        $replacement = SkillingCustomTokenBase::formatCustomTokenError(
////          t('Unknown token: @t', ['@t' => $tokenObject,])
////        );
////      } //End found token plugin.
//    } //End isset($subtokens[0])
////    else {
////      //Not a token found in brackets.
////      $problem = TRUE;
////      $replacement = SkillingCustomTokenBase::formatCustomTokenError(
////        t('Missing token for @f', ['@f' => $fullToken])
////      );
////    }
//    return [$problem, $replacement];
//  }


  /**
   * Parse text.
   *
   * @param string $text
   *   Text to parse.
   * @param \Drupal\node\Entity\Node $node
   *   Node with the text.
   *
   * @return string Result.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\skilling\Exception\SkillingParserException
   * @throws \Drupal\skilling\Exception\SkillingException
   */
  public function parse($text, $node=NULL) {
    //Remove stuff CK left.
    $text = str_replace("\r", '', $text);
    $text = str_replace("<br />\n", "\n", $text);
    $text = str_replace("<br />", "\n", $text);
    $text = str_replace("&nbsp;", ' ', $text);
    //Find image tags, replace with path to file.
    $text = preg_replace_callback(
      '/!(.*)\<img.*src=(?:\'|\")(.*)(?:\'|\").*.*\/\>(.*)!/Ui',
      function ($matches) {
        //$matches[0]: original string
        //$matches[1]: stuff between the first bang, and the start of the img tag.
        //  (classes){styles} - could be MT.
        //$matches[2]: image path
        //$matches[3]: stuff between end of image tag and last bang
        //  (alt text) - could be MT.
        $fullMatch = $matches[0];
        $beforeImgTag = $matches[1];
        $imagePath = $matches[2];
        $afterImgTag = $matches[3];
        //All classes to output.
        $allClasses = 'skilling-parser-image';
        //Grab the Textile class spec, if there is one. In ().
        preg_match("/\((.*)\)/Ui", $beforeImgTag, $textileClassMatches);
        if ( isset( $textileClassMatches[1] ) ) {
          //Append to allClasses.
          $allClasses .= ' '. $textileClassMatches[1];
        }
        //Grab classes spec in the img tag, if there is one.
        preg_match("/\<img.*class=(?:\'|\")(.*)(?:\'|\")/Ui", $fullMatch, $imgClassMatches);
        if ( isset( $imgClassMatches[1] ) ) {
          //Append to allClasses.
          $allClasses .= ' '. $imgClassMatches[1];
        }
        //All styles to output.
        $allStyles = '';
        //Grab the Textile style spec, if there is one. In {}.
        preg_match("/\{(.*)\}/Ui", $beforeImgTag, $textileStylesMatches);
        if ( isset( $textileStylesMatches[1] ) ) {
          //Append to $allStyles.
          $allStyles .= $textileStylesMatches[1];
          //Add a trailing;.
          if ( substr($allStyles, -1) !== ';' ) {
            $allStyles .= ';';
          }
        }
        //Grab styles spec in the img tag, if exists.
        preg_match("/\<img.*style=(?:\'|\")(.*)(?:\'|\")/Ui", $fullMatch, $imgStylesMatches);
        if ( isset( $imgStylesMatches[1] ) ) {
          //Append to $allStyles.
          $allStyles .= ' '. $imgStylesMatches[1];
          //Add a trailing;.
          if ( substr($allStyles, -1) !== ';' ) {
            $allStyles .= ';';
          }
        }
        //Find width attr in the img tag.
        preg_match("/width=(?:\'|\")(.*)(?:\'|\")/Ui", $fullMatch, $imgWidthMatches);
        if ( isset( $imgWidthMatches[1] ) ) {
          //Add a unit if there isn't one.
          $width = trim($imgWidthMatches[1]);
          //Is the last character a digit?
          if ( is_numeric(substr($width, -1)) ) {
            //Add px.
            $width .= 'px';
          }
          //Append to $allStyles.
          $allStyles .= 'width:' . $width . ';';
        }
        //Find height attr in the img tag.
        preg_match("/height=(?:\'|\")(.*)(?:\'|\")/Ui", $fullMatch, $imgHeightMatches);
        if ( isset( $imgHeightMatches[1] ) ) {
          //Add a unit if there isn't one.
          $height = trim($imgHeightMatches[1]);
          //Is the last character a digit?
          if ( is_numeric(substr($height, -1)) ) {
            //Add px.
            $height .= 'px';
          }
          //Append to $allStyles.
          $allStyles .= 'height:' . $height . ';';
        }
        //Build complete Textile tag.
        $completeTextileTag = '!';
        if ( strlen($allClasses) > 0 ) {
          $completeTextileTag .= '(' . $allClasses . ')';
        }
        if ( strlen($allStyles) > 0 ) {
          $completeTextileTag .= '{' . $allStyles . '}';
        }
        $completeTextileTag .= $imagePath . $afterImgTag . '!';
        return $completeTextileTag;
      },
      $text
    );
    //Done processing img tags.
    $text = html_entity_decode($text);
    //Trim whitespace. Authors can use indentation as they want, but it
    //will mess up Textile.
    $text = $this->trimWhitespace($text);
    $text = $this->tokenService->replace($text);
    //Parse custom tags.
    $text = $this->parseCustomTags($text, $node);
    //Textile time.
    $textileParser = new \Netcarver\Textile\Parser('html5');
    $textileParser
      ->setLineWrap(false)
      ->setSymbol('apostrophe', "'")
      ->setSymbol('quote_single_open', "'")
      ->setSymbol('quote_single_close', "'")
      ->setSymbol('quote_double_open', '"')
      ->setSymbol('quote_double_close', '"')
      ->setSymbol('ellipsis', '...')
      ->setSymbol('caps', FALSE)
      ->setSymbol('emdash', ' - ')
      ->setSymbol('endash', ' - ')
      ->setDimensionlessImages()
      ->setRestricted(FALSE);
    // Prepend errors.
    if(count($this->errors) > 0) {
      $text = implode("\n", $this->errors) . $text;
    }
    // == doesn't always get parsed right by the Textile parser.
    // Replace with something else, parse, and then switch back.
    // This is a slimy hack.
    // arcanebumblebee suggestion by Teagan Mathieson.
    // Buy her art at https://byteagan.com.
    $text = str_replace('==', 'arcanebumblebee', $text);
    $result = $textileParser->parse($text);
    $result = str_replace('arcanebumblebee', '==', $result);
    // Need special chars in code and such.
//    $result = htmlspecialchars_decode($result);
    // Wrap it in div showing that's it a parsed block, for heading adjustment.
    $result = SkillingConstants::PARSE_MARKER_INCREASE_HEADING_LEVEL . $result . SkillingConstants::PARSE_MARKER_DECREASE_HEADING_LEVEL;
    return $result;
  }

  /**
   * Adjust heading tags in HTML for final output to the user.
   *
   * @param string $inputHtml
   *   HTML to adjust.
   *
   * @return string
   *   HTML with adjusted headings.
   */
  public function adjustHeadingLevels($inputHtml) {
    $outputHtml = '';
    $isMatch = TRUE;
    $currentCharPos = 0;
    $inputLength = strlen($inputHtml);
    $headingLevelOffset = 0;
    $pattern = "/<(h)([1-6])[ >]|<(pre)([ >])|<(code)[ >]|(" . SkillingConstants::PARSE_MARKER_INCREASE_HEADING_LEVEL_REGEX . ")|(" . SkillingConstants::PARSE_MARKER_DECREASE_HEADING_LEVEL_REGEX . ")/mU";
    //  $pattern = "/<(h)([1-6])[ >]|<(pre)[ >]|<(code)[ >]|(" . preg_quote($markerIncreaseHeadingLevel) . ")|(" . preg_quote($markerDecreaseHeadingLevel) . ")/mU";
    while ($isMatch && $currentCharPos < $inputLength) {
      $isMatch = preg_match($pattern, $inputHtml, $matches, PREG_OFFSET_CAPTURE, $currentCharPos);
      if ($isMatch) {
        $matchedTag = $matches[0][0];
        $matchStartPos = $matches[0][1];
        // Output chars from current pos to just before position of the new match.
        $outputChunk = substr($inputHtml, $currentCharPos, $matchStartPos - $currentCharPos);
        $outputHtml .= $outputChunk;
        $currentCharPos = $matchStartPos;

        if (substr($matchedTag, 0, 2) === "<h") {
          $headingLevel = (int) $matches[2][0];
          $newHeadingLevel = $headingLevel + $headingLevelOffset;
          // Cap at 6.
          $newHeadingLevel = $newHeadingLevel > 6 ? 6 : $newHeadingLevel;
          // Output the new heading level.
          $outputHtml .= '<h' . $newHeadingLevel;
          $currentCharPos += 3;
          // Find the closing h tag.
          $closingTagPos = strpos($inputHtml, '</h' . $headingLevel . '>', $currentCharPos);
          // Copy everything up to that point to the output.
          $outputChunk = substr($inputHtml, $currentCharPos, $closingTagPos - $currentCharPos);
          $outputHtml .= $outputChunk;
          $currentCharPos = $closingTagPos;
          // Output the closing tag.
          $outputHtml .= '</h' . $newHeadingLevel . '>';
          $currentCharPos += 5;
        }
        elseif ($matchedTag === SkillingConstants::PARSE_MARKER_INCREASE_HEADING_LEVEL) {
          $headingLevelOffset ++;
          $currentCharPos += strlen(SkillingConstants::PARSE_MARKER_INCREASE_HEADING_LEVEL);
        }
        elseif ($matchedTag === SkillingConstants::PARSE_MARKER_DECREASE_HEADING_LEVEL) {
          $headingLevelOffset --;
          $currentCharPos += strlen(SkillingConstants::PARSE_MARKER_DECREASE_HEADING_LEVEL);
        }
        elseif (substr($matchedTag, 0, 5) === '<code') {
          // Output the tag.
          $outputHtml .= '<code';
          // Find the end tag.
          $currentCharPos += 5;
          // Find the end of the tag.
          $endTagPos = stripos($inputHtml, '</code>', $currentCharPos);
          // Copy stuff up to end of tag to output.
          $outputChunk = substr($inputHtml, $currentCharPos, $endTagPos - $currentCharPos);
          $outputHtml .= $outputChunk;
          $currentCharPos = $endTagPos;
          // Output the closing tag.
          $outputHtml .= '</code>';
          $currentCharPos += 7;
        }
        elseif (substr($matchedTag, 0, 4) === '<pre') {
          // Output the tag.
          $outputHtml .= '<pre';
          // Find the end tag.
          $currentCharPos += 4;
          // Find the end of the tag.
          $endTagPos = stripos($inputHtml, '</pre>', $currentCharPos);
          // Copy stuff up to end of tag to output.
          $outputChunk = substr($inputHtml, $currentCharPos, $endTagPos - $currentCharPos);
          $outputHtml .= $outputChunk;
          $currentCharPos = $endTagPos;
          // Output the closing tag.
          $outputHtml .= '</pre>';
          $currentCharPos += 6;
        }
        else {
          $outputHtml .= '<';
          $currentCharPos ++;
        }
      }
    }
    // Output the last part of the input that did not have any matched strings.
    $outputChunk = substr($inputHtml, $currentCharPos);
    $outputHtml .= $outputChunk;
    return $outputHtml;

  }


  /**
   * Check whether a custom tag is used in content.
   *
   * @param string $content
   *   The content to check.
   *
   * @return bool
   *   True if there is a custom tag in the content.
   */
  public function isCustomTagUsedInContent($content) {
    // Run through the custom tags.
    $customTagManager = \Drupal::service('plugin.manager.skilling.custom_tag');
    // Get the list of all the custom tag plugins defined on the system from the
    // plugin manager.
    $tagPluginDefinitions = $customTagManager->getDefinitions();
    $customTagUsed = FALSE;
    // Strip the <br>s, or won't detect custom tags.
    $content = str_replace('<br>', '', $content);
    $content = str_replace('<br />', '', $content);
    $content = str_replace('<br/>', '', $content);
    // Make plugin instances for all the plugins.
    foreach ($tagPluginDefinitions as $pluginId => $tagPluginDefinition) {
      /* @var \Drupal\skilling\Plugin\SkillingCustomTagBase $plugin */
      $plugin = $customTagManager->createInstance($pluginId);
      $tag = $plugin->tag();
      $match = preg_match('/^\s*' . $tag . '\.\s*$/mu', $content);
      if ($match === 1) {
        $customTagUsed = TRUE;
        break;
      }
    }
    return $customTagUsed;
  }


  /**
   * A custom tag class is reporting an error.
   *
   * @param string $errorMessage
   *   Error message.
   */
  public function customTagErrorReported($errorMessage) {
    $this->errors[] = $errorMessage;
  }

}
