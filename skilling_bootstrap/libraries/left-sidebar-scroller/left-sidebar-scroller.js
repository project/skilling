/**
 * @file
 * Set up the scroller for the left sidebar timeline.
 */

(function ($, Drupal) {
  "use strict";
  // Exit if not being called from within Skilling context.
  if (Drupal === undefined) {
    return;
  }
  if (Drupal.skillingSettings === undefined) {
    return;
  }
  Drupal.skillingSettings.setupLeftsidebarScroller = false;
  Drupal.behaviors.leftSidebarScroller = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.setupLeftsidebarScroller) {
        return;
      }
      Drupal.skillingSettings.setupLeftsidebarScroller = true;
      // Minimum height of timeline.
      const minHeight = 400;
      $(document).ready(function (){
        let height = $("div.region-content").height();
        if (height < minHeight) {
          height = minHeight;
        }
        // Removed, so using a pause button doesn't leave big blocks of whitespace.
        // $("div.sidebar-container-content").height(height);
      });
    }
  };
}(jQuery, Drupal));
