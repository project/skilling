(function($) {
  "use strict";
  $(document).ready(function () {
    $(document).ready(function () {
      // Part of FOUC prevention.
      $("html")
          .css("visibility", "visible")
          .css("opacity", 1);
    });
  });
}(jQuery));
