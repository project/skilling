(function($) {
  "use strict";
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      trigger: 'focus'
    })
  });
}(jQuery));
