<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 4/24/19
 * Time: 2:59 PM
 */

function skilling_bootstrap_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state) {
  $form['extras'] = array(
    '#type'         => 'details',
    '#title'        => t('Extras'),
    '#description'  => t('Extra things for the Skilling theme.'),
    '#open' => TRUE,
  );

  $form['extras']['jump_to_top'] = array(
    '#type'         => 'checkbox',
    '#title'        => t('Jump to top widget'),
    '#default_value' => theme_get_setting('jump_to_top'),
    '#description'  => t('Check if you want a jump to top widget on every page.'),
  );

}
