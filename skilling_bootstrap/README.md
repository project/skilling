# Skilling theme based on Boostrap 4

Adapted from Vladimir Roudakov's Drupal Bootstrap 4
theme, at https://www.drupal.org/project/bootstrap4.

Color scheme computed at http://paletton.com. Try:

http://paletton.com/#uid=23A0u0k++NltxZOLK+V+WuG+5o8

Base color is primary button color of Bootstrap 4. Navbar
back color is darker version. Sidebar colors are complements.

## Initiate SASS compilation

Run 

```
npm install 
npm run sass-compile
#OR npm run sass-compile-watch
```
