/*
 * Fill-in-the-blank process answer.
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.setupFibs = false;
  // Flag to show when saving, used to disable processing another click.
  Drupal.skillingSettings.interactingWithServer = false;
  Drupal.behaviors.skillingFibCheckAnswer = {
    /**
     * What is checked? text, number, or none.
     */
    resultOfCheck: "",
    attach: function (context, settings) {
      if (Drupal.skillingSettings.setupFibs) {
        return;
      }
      Drupal.skillingSettings.setupFibs = true;
      $(document).ready(function () {
        $(".skilling-insert-fib-check-button").click(function (event) {
          let $fibQuestionContainer = $($(event.target).closest(".skilling-insert-fib"));
          Drupal.behaviors.skillingFibCheckAnswer.checkAnswer($fibQuestionContainer);
        });
        $(".skilling-insert-fib-give-up-button").click(function (event) {
          let $fibQuestionContainer = $($(event.target).closest(".skilling-insert-fib"));
          Drupal.behaviors.skillingFibCheckAnswer.giveUp($fibQuestionContainer);
        });
        $(".skilling-insert-fib-response-container input").keypress(function (event) {
          if (event.which === 13) {
            event.preventDefault();
            let $fibQuestionContainer = $($(event.target).closest(".skilling-insert-fib"));
            Drupal.behaviors.skillingFibCheckAnswer.checkAnswer($fibQuestionContainer);
          }
        });
      });
    },
    /**
     * Check the user's answer.
     *
     * @param $fibQuestionContainer
     *   The fib element.
     */
    checkAnswer: function ($fibQuestionContainer) {
      if ( Drupal.skillingSettings.interactingWithServer ) {
        return;
      }
      Drupal.skillingSettings.interactingWithServer = true;
      // Get the answer.
      // DOM element the user types his/her answer into.
      let answerControl = $fibQuestionContainer.find("input");
      let answer = answerControl.val().trim();
      // Stop if MT.
      if (answer === "") {
        Drupal.skillingSettings.interactingWithServer = false;
        swal(Drupal.t("Sorry, please type an answer."));
        $(answerControl).focus().select();
        return;
      }
      // Hide current answer, if there is one.
      let responseContainer = $fibQuestionContainer.find(".skilling-fib-response-message");
      responseContainer.hide("slow");
      // Tell the server about the user's answer.
      let internalName = $fibQuestionContainer.attr("id");
      let throbber = $fibQuestionContainer.find(".skilling-throbber");
      $(throbber).show();
      // NB: rely on server to protect against XSS from user input.
      $.ajax({
        url: Drupal.url("skilling/check-fib-response"),
        type: 'POST',
        dataType: 'json',
        data: {
          "csrfToken": drupalSettings.csrfToken,
          "sessionId": drupalSettings.sessionId,
          "internalName": internalName,
          "answer": answer
        },
        success: function (result) {
          $(throbber).hide();
          if (result.status.toLowerCase() === "error") {
            swal(result.errorMessage);
            return;
          }
          // Get the components passed by the server.
          let match = result.match;
          let responseMessage = result.responseMessage;
          // Response message can include HTML, e.g., for links to content.
          // Show results.
          let responseContainer = $fibQuestionContainer.find(".skilling-fib-response-message");
          responseContainer.removeClass("skilling-fib-response-explanation-incorrect");
          responseContainer.removeClass("skilling-fib-response-explanation-correct");
          if (match) {
            responseContainer.addClass("skilling-fib-response-explanation-correct");
          }
          else {
            responseContainer.addClass("skilling-fib-response-explanation-incorrect");
          }
          responseContainer
              .html(responseMessage)
              .show("slow");
        },
        fail: (function (jqXHR, textStatus) {
          $(throbber).hide();
          let message = Drupal.t(
              "Skilling history FIB fail @text. Please send a screen shot of your "
              + "entire browser window, including URL, to someone.",
              {'@text': textStatus}
          );
          swal(message);
        })
      });
      Drupal.skillingSettings.interactingWithServer = false;
    },
    /**
     * Give up.
     */
    giveUp: function ($fibQuestionContainer) {
      if ( Drupal.skillingSettings.interactingWithServer ) {
        return;
      }
      Drupal.skillingSettings.interactingWithServer = true;
      // Ask the server for the answer.
      let internalName = $fibQuestionContainer.attr("id");
      let throbber = $fibQuestionContainer.find(".skilling-throbber");
      $(throbber).show();
      // Disable the buttons.
      $("button.skilling-insert-fib-check-button").attr('disabled',true);
      $("button.skilling-insert-fib-give-up-button").attr('disabled',true);
      $.ajax({
        url: Drupal.url("skilling/fib-give-up"),
        type: 'POST',
        dataType: 'json',
        data: {
          "csrfToken": drupalSettings.csrfToken,
          "sessionId": drupalSettings.sessionId,
          "internalName": internalName
        },
        success: function (result) {
          $(throbber).hide();
          if (result.status.toLowerCase() === "error") {
            swal(result.errorMessage);
            return;
          }
          // Show it.
          $fibQuestionContainer.find("input").val(result.answer);
          // $("input.skilling-insert-fib-response").val(result.answer);
        },
        fail: (function (jqXHR, textStatus) {
          $(throbber).hide();
          let message = Drupal.t(
              "Skilling history FIB give up fail @text. Please send a screen shot of your "
              + "entire browser window, including URL, to someone.",
              {'@text': textStatus}
          );
          swal(message);
        })
      });
      Drupal.skillingSettings.interactingWithServer = false;
    }
  };
}(jQuery, Drupal));
