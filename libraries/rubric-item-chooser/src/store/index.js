import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    strict: true,
    dataLoaded: false,
    newRubricItemTemplate: {
      rubricItemId: -1,
      name: '(Empty)',
      categories: [],
      notes: '',
      responseOptionIds: []
    },
    newResponseOptionTemplate: {
      responseOptionId: -1,
      name: "(Empty)",
      notes: "",
      completes: true,
      appliesToExerciseIds: []
    },
    exercises: [],
    // 0 for new exercise.
    exerciseId: null,
    // exerciseRubricItemIds: [],
    rubricItemCategories: [],
    rubricItems: [],
    responseOptions: []
  },
  mutations: {
    addResponseOption(state, newResponseOption) {
      state.responseOptions.push(newResponseOption);
    },
    addRubricItemById(state, rubricItemId) {
      state.rubricItemIds.push(rubricItemId);
    },
    /**
     * Remove RI with given id from the exercise.
     *
     * @param {*} state 
     * @param {*} rubricItemId 
     */
    removeRubricItemById(state, rubricItemId) {
      for (let i = 0; i < state.rubricItemIds.length; i++) {
        if (state.rubricItemIds[i] == rubricItemId) {
          // TODO is there are rubric item ids?
          state.rubricItemIds.splice(i, 1);
          break;
        }
      }
    },
    /**
     * Delete a response option from a rubric item.
     * 
     * @param {*} state 
     * @param {*} params 
     */
    deleteResponseOptionFromRubricItem(state, params) {
      let rubricItemIdToFind = params.rubricItemId;
      let responseOptionIdToFind = params.responseOptionId;
      // console.log("deleteResponseOptionFromRubricItem rubricItemIdToFind", rubricItemIdToFind);
      // console.log("deleteResponseOptionFromRubricItem responseOptionId", responseOptionIdToFind);
      // Find the rubric item.
      let targetRubricItem = null;
      for (let rubricItem of state.rubricItems) {
        if (rubricItem.rubricItemId == rubricItemIdToFind) {
          targetRubricItem = rubricItem;
          break;
        }
      }
      if (targetRubricItem === null) {
        // TODO Make real
        alert(`bad RI id to find in deleteResponseOptionFromRubricItem: ${rubricItemIdToFind}`)
      }
      // Remove the response option.
      let foundRO = false;
      for (let i = 0; i < targetRubricItem.responseOptionIds.length; i++) {
        if (targetRubricItem.responseOptionIds[i] == responseOptionIdToFind) {
          targetRubricItem.responseOptionIds.splice(i, 1);
          foundRO = true;
          break;
        }
      }
      if (!foundRO) {
        alert(`bad Ro id to find in deleteResponseOptionFromRubricItem: ${responseOptionIdToFind}`)
        return;
      }

    },
    // TODO below works assuming that rubtic item is not cloned, but points to object in array.
    /**
     * 
     * 
     * 
     * @param {*} state 
     * @param {*} params 
     */
    updateRubricItemCategory(state, params) {
      // console.log("updateRubricItemCategory params", params);
      let targetRubricItemId = params.rubricItemId;
      let targetCategoryId = params.categoryId;
      let targetIsChecked = params.isChecked;
      // Find the rubric item.
      let rubricItem = state.rubricItems.find(
        function (rubricItemInState) {
          return rubricItemInState.rubricItemId == targetRubricItemId
        }
      );
      // console.log("rubricItem", rubricItem);
      if (rubricItem === undefined) {
        // TODO make real
        alert(`updateRubricItemCategory unknown RI id: ${targetRubricItemId}`);
        return;
      }
      if (targetIsChecked) {
        // Add category id to list, if not there already.
        let foundIndex = rubricItem.categories.find(
          function(catId) {
            return catId == targetCategoryId;
          }
        );
        // Category not in list. Add it.
        if (foundIndex === undefined) {
          rubricItem.categories.push(targetCategoryId);
        }
      }
      else {
        // Not checked. Remove from the list.
        for (let i = 0; i < rubricItem.categories.length; i++) {
          if (rubricItem.categories[i] == targetCategoryId) {
            // Here it is.
            rubricItem.categories.splice(i, 1);
            break;
          }
        }
      }
    },
    updateRubricItemName(state, params) {
      let targetRubricItemId = params.rubricItemId;
      let newName = params.name;
      if (newName.trim() == "") {
        newName = "(Empty)";
      }
      // Find the rubric item.
      let rubricItem = state.rubricItems.find(
        function (rubricItemInState) {
          return rubricItemInState.rubricItemId == targetRubricItemId
        }
      );
      // console.log("rubricItem", rubricItem);
      if (rubricItem === undefined) {
        // TODO make real
        alert(`updateRubricItemName unknown RI id: ${targetRubricItemId}`);
        return;
      }
      rubricItem.name = newName;
    },
    removeRubricItemFromExercise(state, params) {
      // console.log("removeRubricItemFromExercise params", params);
      // console.log("state.exercises", state.exercises);
      let targetExerciseId = params.exerciseId;
      if (isNaN(targetExerciseId) || targetExerciseId < 0) {
        // TODO make real
        alert(`removeRubricItemFromExercise unknown exer id: ${targetExerciseId}`);
        return;

      }
      let targetRubricItemId = params.rubricItemId;
      // Find the exercise.
      let targetExercise = state.exercises.find(
        function (exercise) {
          return exercise.exerciseId == targetExerciseId;
        }
      );
      if (targetExercise === undefined) {
        // TODO make real
        alert(`removeRubricItemFromExercise unknown exer id: ${targetExerciseId}`);
        return;
      }
      //Remove the rubric item.
      let targetIndex = null;
      for (let i = 0; i < targetExercise.rubricItemIds.length; i++) {
        if (targetExercise.rubricItemIds[i] == targetRubricItemId) {
          targetIndex = i;
          break;
        }
      }
      if (targetIndex === null) {
        // TODO make real
        alert(`removeRubricItemFromExercise unknown rubricItemId id: ${targetRubricItemId}`);
        return;
      }
      targetExercise.rubricItemIds.splice(targetIndex, 1);
    },
    updateResponseOptionName(state, params) {
      let responseOptionIdToFind = params.responseOptionId;
      let newName = params.name;
      if (newName.trim() == "") {
        newName = "(Empty)";
      }
      // Find the response option.
      let responseOption = state.responseOptions.find(
        function (responseOptionState) {
          return responseOptionState.responseOptionId == responseOptionIdToFind
        }
      );
      // console.log("responseOption", responseOption);
      if (responseOption === undefined) {
        // TODO make real
        alert(`updateResponseOptionName unknown RO id: ${responseOptionIdToFind}`);
        return;
      }
      responseOption.name = newName;
    },
    updateResponseOptionCompletes(state, params) {
      let responseOptionIdToFind = params.responseOptionId;
      let newIsChecked = params.isChecked;
      // Find the response option.
      let responseOption = state.responseOptions.find(
        function (responseOptionState) {
          return responseOptionState.responseOptionId == responseOptionIdToFind
        }
      );
      // console.log("responseOption", responseOption);
      if (responseOption === undefined) {
        // TODO make real
        alert(`updateResponseOptionCompletes unknown RO id: ${responseOptionIdToFind}`);
        return;
      }
      responseOption.completes = newIsChecked;
    },
    addResponseOptionAppliesTo(state, params) {
      let responseOptionIdToFind = params.responseOptionId;
      let exerciseId = params.exerciseId;
      // Find the response option.
      let responseOption = state.responseOptions.find(
        function (responseOptionState) {
          return responseOptionState.responseOptionId == responseOptionIdToFind
        }
      );
      // console.log("responseOption", responseOption);
      if (responseOption === undefined) {
        // TODO make real
        alert(`addResponseOptionAppliesTo unknown RO id: ${responseOptionIdToFind}`);
        return;
      }
      responseOption.appliesToExerciseIds.push(exerciseId);      
    },
    addNewRubricItem(state, rubricItem) {
      // console.log("veux addNewRubricItem ", rubricItem);
      state.rubricItems.push(rubricItem);
    },
    addRubricItemIdToExercise(state, params) {
      console.log("addRubricItemIdToExercise params ", params);
      let targetExerciseId = params.exerciseId;
      let rubricItemId = params.rubricItemId;
      // Find the exercise.
      let targetExercise = state.exercises.find(
        function (exercise) {
          return exercise.exerciseId == targetExerciseId;
        }
      );
      if (targetExercise === undefined) {
        // TODO make real
        alert(`addRubricItemIdToExercise unknown exer id: ${targetExerciseId}`);
        return;
      }
      targetExercise.rubricItemIds.push(rubricItemId);
      console.log("targetExercise", targetExercise);
    },
    addResponseOptionIdToRubricItem(state, params) {
      let rubricItemIdToFind = params.rubricItemId;
      let responseOptionIdToAdd = params.responseOptionId;
      // console.log("deleteResponseOptionFromRubricItem rubricItemIdToFind", rubricItemIdToFind);
      // console.log("deleteResponseOptionFromRubricItem responseOptionId", responseOptionIdToFind);
      // Find the rubric item.
      let targetRubricItem = null;
      for (let rubricItem of state.rubricItems) {
        if (rubricItem.rubricItemId == rubricItemIdToFind) {
          targetRubricItem = rubricItem;
          break;
        }
      }
      if (targetRubricItem === null) {
        alert(`bad RI id to find in addResponseOptionIdToRubricItem: ${rubricItemIdToFind}`);
        return;
      }
      // Add the response option.
      let foundRO = false;
      for (let i = 0; i < targetRubricItem.responseOptionIds.length; i++) {
        if (targetRubricItem.responseOptionIds[i] == responseOptionIdToAdd) {
          foundRO = true;
          break;
        }
      }
      if (!foundRO) {
        targetRubricItem.responseOptionIds.push(responseOptionIdToAdd);
      }
    },
    removeExerciseFromResponseOptionAppliesTo(state, params) {
      let responseOptionIdToFind = params.responseOptionId;
      let exerciseToRemoveId = params.exerciseId;
      // Find the response option.
      let responseOption = state.responseOptions.find(
        function (responseOptionState) {
          return responseOptionState.responseOptionId == responseOptionIdToFind
        }
      );
      // console.log("responseOption", responseOption);
      if (responseOption === undefined) {
        // TODO make real
        alert(`removeExerciseFromResponseOptionAppliesTo unknown RO id: ${responseOptionIdToFind}`);
        return;
      }
      // Find index of exer to remove.
      let exerciseToRemoveIndex = -1;
      for (let index = 0; index < responseOption.appliesToExerciseIds.length; index++) {
        if (responseOption.appliesToExerciseIds[index] == exerciseToRemoveId) {
          exerciseToRemoveIndex = index;
          break;
        }
      }
      if (exerciseToRemoveIndex === -1) {
        // TODO: replace
        // ******* ERROR *****
        alert(`Unknown exerciseToRemoveId: ` + exerciseToRemoveId);
        return;
      }
      responseOption.appliesToExerciseIds.splice(exerciseToRemoveIndex, 1)
    },
    replaceResponseOptionArray(state, params) {
      let rubricItemIdToFind = params.rubricItemId;
      let newList = params.newList;
      // Find the rubric item.
      let targetRubricItem = null;
      for (let rubricItem of state.rubricItems) {
        if (rubricItem.rubricItemId == rubricItemIdToFind) {
          targetRubricItem = rubricItem;
          break;
        }
      }
      if (targetRubricItem === null) {
        alert(`bad RI id to find in replaceResponseOptionArray: ${rubricItemIdToFind}`);
        return;
      }
      targetRubricItem.responseOptionIds = newList;
    },
    /**
     * Move a response option position up or down in RI array.
     *
     * @param {*} state
     * @param {*} params
     */
    moveResponseOption(state, params) {
      let rubricItemIdToFind = params.rubricItemId;
      let responseOptionId = params.responseOptionId;
      let direction = params.direction;
      // Find the rubric item.
      let targetRubricItem = null;
      for (let rubricItem of state.rubricItems) {
        if (rubricItem.rubricItemId == rubricItemIdToFind) {
          targetRubricItem = rubricItem;
          break;
        }
      }
      if (targetRubricItem === null) {
        alert(`bad RI id to find in moveResponseOption: ${rubricItemIdToFind}`);
        return;
      }
      // Find the position of the response option in the RI array.
      let responseOptionIndex = null;
      for (let index = 0; index < targetRubricItem.responseOptionIds.length; index ++) {
        if (targetRubricItem.responseOptionIds[index] == responseOptionId) {
          responseOptionIndex = index;
          break;
        }
      }
      if (responseOptionIndex === null) {
        alert(`bad RO id to find in moveResponseOption: ${responseOptionId}`);
        return;
      }
      // Move which direction?
      if (direction == "up") {
        // Move towards the front of the array.
        if (responseOptionIndex == 0) {
          // Already at the front of the array - nothing to do.
          return;
        }
        // Switch with prior value.
        let priorValue = targetRubricItem.responseOptionIds[responseOptionIndex - 1];
        targetRubricItem.responseOptionIds[responseOptionIndex - 1] = responseOptionId;
        targetRubricItem.responseOptionIds[responseOptionIndex] = priorValue;
      }
      else if (direction == "down") {
        // Move towards end of the array.
        if (responseOptionIndex == (targetRubricItem.responseOptionIds.length - 1) ) {
          // Already at the end of the array - nothing to do.
          return;
        }
        // Switch with next value.
        let nextValue = targetRubricItem.responseOptionIds[responseOptionIndex + 1];
        targetRubricItem.responseOptionIds[responseOptionIndex + 1] = responseOptionId;
        targetRubricItem.responseOptionIds[responseOptionIndex] = nextValue;
      }
      else {
        alert(`bad direction in moveResponseOption: ${direction}`);
        return;
      }
    },
    loadData(state, params) {
      // console.log("loadData params", params);
      // console.log("window.exerciseId", window.exerciseId);
      // 0 for new.
      state.exerciseId = parseInt(window.exerciseId);
      state.exercises = params.exercises;
      if (state.exerciseId === 0) {
        // New exercise, make a place for it.
        state.exercises.push({
          exerciseId: 0,
          name: "New exercise",
          rubricItemIds: []
        });
      }
      state.rubricItemCategories = params.rubricItemCategories;
      state.rubricItems = params.rubricItems;
      state.responseOptions = params.responseOptions;
      state.dataLoaded = true;
      // console.log("loadData state.exercises", state.exercises);
      // state.exerciseRubricItemIds = params.exerciseRubricItemIds;
    }
  },
  actions: {
    async loadDataAction(context) {
      // console.log("loadDataAction window.exerciseId", window.exerciseId);
      // console.log("drupalSettings", drupalSettings);
      const CancelToken = axios.CancelToken;
      window.axiosCanceler = CancelToken.source();
      await axios.get(Drupal.url("skilling/rubric-items/load"), {
        cancelToken: window.axiosCanceler.token,
        params: {
          "csrfToken": drupalSettings.csrfToken,
          "sessionId": drupalSettings.sessionId,
          "exerciseId": window.exerciseId
        }
      })
      .then(function (response) {
        // handle success
        // console.log("axios response", response);
        context.commit('loadData', response.data);
      })
      .catch(function (error) {
        // TODO make real
        alert("axios error:" + error);
      });
      //     "csrfToken": drupalSettings.csrfToken,
      //     "sessionId": drupalSettings.sessionId,
      //     "exerciseId": nodeNumber

    },
    removeRubricItemFromExerciseAction(context, params) {
      context.commit("removeRubricItemFromExercise", params);
    },
    addResponseOptionAction(context, responseOption) {
      context.commit("addResponseOption", responseOption);
    },
    addResponseOptionIdToRubricItemAction(context, params) {
      // Async API stuff here.
      context.commit("addResponseOptionIdToRubricItem", params);
    },
    deleteResponseOptionAction(context, responseOptionId) {
      context.commit("deleteResponseOption", responseOptionId);
    },
    updateResponseOptionAction(context, responseOption) {
      context.commit("updateResponseOption", responseOption);
    },
    // addRubricItemAction(context, rubricItem) {
    //   // Async API stuff here.
    //   context.commit("addRubricItem", rubricItem);
    // },
    updateRubricItemAction(context, rubricItem) {
      context.commit("updateRubricItem", rubricItem);
    },
    removeRubricItemActionById(context, rubricItemId) {
      context.commit("removeRubricItemById", rubricItemId);
    },
    addRubricItemActionById(context, rubricItemId) {
      context.commit("addRubricItemById", rubricItemId);
    },
    deleteResponseOptionFromRubricItemAction(context, params) {
      context.commit("deleteResponseOptionFromRubricItem", params);
    },
    updateRubricItemCategoryAction(context, params) {
      context.commit("updateRubricItemCategory", params);
    },
    updateRubricItemNameAction(context, params) {
      context.commit("updateRubricItemName", params);
    },
    updateResponseOptionNameAction(context, params) {
      context.commit("updateResponseOptionName", params);
    },
    updateResponseOptionCompletesAction(context, params) {
      context.commit("updateResponseOptionCompletes", params);
    },
    addResponseOptionAppliesToAction(context, params) {
      context.commit("addResponseOptionAppliesTo", params);
    },
    addRubricItemAction(context, rubricItem) {
      context.commit("addNewRubricItem", rubricItem);
    },
    addRubricItemIdToExerciseAction(context, params) {
      context.commit("addRubricItemIdToExercise", params);
    },
    removeExerciseFromResponseOptionAppliesToAction(context, params) {
      context.commit("removeExerciseFromResponseOptionAppliesTo", params);
    },
    replaceResponseOptionArrayAction(context, params) {
      context.commit("replaceResponseOptionArray", params);
    },
    moveResponseOptionAction(context, params) {
      context.commit("moveResponseOption", params);
    }
  },
  getters: {
    getDataLoaded(state) {
      return function() {
        return state.dataLoaded;
      }
    },
    getCurrentExerciseId(state) {
      return function() {
        return state.exerciseId;
      }
    },
    // getExerciseName: function(state) {
    //   return function() {
    //     return state.name;
    //   }
    // },
    getExerciseRubricItems(state) {
      // Find the exercise.
      return function(exerciseIdToFind) {
        // console.log("getExerciseRubricItems exerciseIdToFind", exerciseIdToFind);
        // console.log("getExerciseRubricItems state.exercises", state.exercises);
        let exercise = state.exercises.find(function (exercise) {
          return exercise.exerciseId == exerciseIdToFind;
        });
        if (exercise === undefined) {
          // TODO make real
          alert(`getExerciseRubricItems unknown exer id: ${exerciseIdToFind}`);
          return;
        }
        // console.log("getExerciseRubricItems exercise", exercise);
        const ids = exercise.rubricItemIds;
        // Make an array of those rubric items.
        let rubricItems = [];
        for (const id of ids) {
          // Get the rubric item with this id.
          const rubricItemFound = state.rubricItems.find(function (rubricItem) {
            return rubricItem.rubricItemId == id;
          });
          if (rubricItemFound === undefined) {
            // TODO Make real
            alert(`bad RI id to find in : ${id}`);
          }
          rubricItems.push(rubricItemFound);
        }
        // console.log("getExerciseRubricItems rubricItems found", rubricItems);
        return rubricItems;
      }
    },
    getExerciseRubricItemIds(state) {
      // Find the exercise.
      return function(exerciseIdToFind) {
        // console.log("getExerciseRubricItemIds exerciseIdToFind", exerciseIdToFind);
        let exercise = state.exercises.find(function (exercise) {
          return exercise.exerciseId == exerciseIdToFind;
        });
        if (exercise === undefined) {
          // TODO make real
          alert(`getExerciseRubricItemIds unknown exer id: ${exerciseIdToFind}`);
          return;
        }
        const ids = exercise.rubricItemIds;
        return ids;
      }
    },
    /**
     * Return a clone of the categories array.
     * @param {*} state 
     */
    getRubricItemCategories(state) {
      return function() {
        const clone = state.rubricItemCategories.slice();
        return clone;
      }
    },
    getRubricItemCategoryById(state) {
      return function(categoryIdToFind) {
        let category = state.rubricItemCategories.find(function (category) {
          return category.categoryId == categoryIdToFind;
        });
        return category;
      }
    },
    getExerciseById(state) {
      return function(exerciseIdToFind) {
        // console.log("getter getExerciseById exerciseIdToFind", exerciseIdToFind);
        let exercise = state.exercises.find(function (exerciseInList) {
          return exerciseInList.exerciseId == exerciseIdToFind;
        });
        // console.log("getter getExerciseById exercise", exercise);
        return exercise;
      }
    },
    /**
     * Return clone of the exercises array.
     * @param {*} state 
     */
    getExercises(state) {
      return function() {
        const clone = state.exercises.slice();
        return clone;
      }
    },
    getResponseOptionIds(state) {
      let ids = [];
      for (let responseOption of state.responseOptions) {
        ids.push(responseOption.responseOptionId);
      }
      return ids;
    },
    getResponseOptionById(state) {
      return function(responseOptionIdFind) {
        // console.log("getResponseOptionById responseOptionIdFind", responseOptionIdFind);
        // console.log("state.responseOptions", state.responseOptions);
        let responseOption = state.responseOptions.find(function (responseOption) {
          return responseOption.responseOptionId == responseOptionIdFind;
        });
        return responseOption;
      }
    },
    getResponseOptionsForRubricItem(state, getters) {
      return function(rubricItemId) {
        // console.log("getResponseOptionsForRubricItem rubricItemId", rubricItemId);
        let rubricItem = getters.getRubricItemById(rubricItemId);
        // console.log("getResponseOptionsForRubricItem rubricItem", rubricItem);
        let responseOptionIds = rubricItem.responseOptionIds;
        // console.log("getResponseOptionsForRubricItem responseOptionIds", responseOptionIds);
        let result = [];
        for (let responseOptionId of responseOptionIds) {
          // console.log("getResponseOptionsForRubricItem responseOptionId", responseOptionId);
          let responseOption = getters.getResponseOptionById(responseOptionId);
          result.push(responseOption);
        }
        return result;
      }
    },
    getRubricItemIds(state) {
      let ids = [];
      for (let rubricItem of state.rubricItems) {
        ids.push(rubricItem.rubricItemId);
      }
      return ids;
    },
    getRubricItemById(state) {
      return function(rubricItemIdFind) {
        // Could be the id of the fake one.
        if (rubricItemIdFind == state.newRubricItemTemplate.rubricItemId) {
          let clone = Object.assign({}, state.newRubricItemTemplate);
          return clone;
        }
        let rubricItem = state.rubricItems.find(function (rubricItem) {
          return rubricItem.rubricItemId == rubricItemIdFind;
        });
        return rubricItem;
      }
    },
    /**
     * Return a clone of the rubric items array.
     * @param {} state 
     */
    getRubricItems(state) {
      return function() {
        const clone = state.rubricItems.slice();
        return clone;
      }
    },
    getNewRubricItemTemplate(state) {
      return function() {
        let clone = JSON.parse(JSON.stringify(state.newRubricItemTemplate));
        // console.log("", );
        return clone;
      }
    },
    getNewResponseOptionTemplate(state) {
      return function() {
        // console.log("state.newResponseOptionTemplate", state.newResponseOptionTemplate);
        let clone = JSON.parse(JSON.stringify(state.newResponseOptionTemplate));
        // console.log("clone", clone);
        return clone;
      }
    },
    /**
     * Get the data for sending back to the server.
     */
    getAllTheThings(state) {
      return function() {
        // console.log("start getAllTheThings" );
        let allTheThings = {
          rubricItems: state.rubricItems,
          responseOptions: state.responseOptions
        };
        // Get the new rubric item order from the GUI.
        let rubricItemOrder = [];
        jQuery("#rubric-item-list").find(".rubric-item").each(
            function(index, element) {
              // console.log("element", element);
              let rubricItemId = jQuery(element).data("rubric-item-id");
              if (!rubricItemId) {
                alert("getAllTheThings: rubricItemId missing");
                return;
              }
              let recordItem = {
                rubricItemId: rubricItemId,
                index: index
              };
              rubricItemOrder.push(recordItem);
            }
        );
        allTheThings.rubricItemOrder = rubricItemOrder;
        return allTheThings;
      }
    }
    // getRubricItemsNotInExercise(state) {
    //   return function() {

    //   }
    // }
  }
})
