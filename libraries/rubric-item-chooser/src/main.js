
import Vue from 'vue';
import App from './App.vue';
import store from './store/index.js';
import VModal from 'vue-js-modal';

(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings = Drupal.skillingSettings || {};
  Drupal.skillingSettings.serverData = null;
  // Flag to track initialization.
  Drupal.skillingSettings.rubricItemGuiInited = false;
  Drupal.behaviors.rubricItemGui = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.rubricItemGuiInited) {
        return;
      }
      Drupal.skillingSettings.rubricItemGuiInited = true;
      $(document).ready(function() {
        // If this is a new exercise, can't add rubrics just yet.
        let isNew = jQuery("input[name='exercise_is_new']").val();
        // console.log("new: " + isNew);
        // if (isNew === "yes") {
        //   jQuery("#app").html(
        //       Drupal.t(`
        //         <p><strong>Create the exercise, and edit to add rubric items.</strong></p>
        //         <p><strong>
        //           Sorry for the trouble, but it makes coding this interface easier.
        //         </strong></p>
        //       `)
        //   );
        //   return;
        // }
        // console.log("document ready");
        // Get the exercise id.
        const exerciseId = Drupal.behaviors.rubricItemGui.getExerciseIdFromUrl();
        if (exerciseId === null) {
          // TODO make real
          alert("No node number or add:" + window.location.href);
          return;
        }
        // Store in window for Vuex to use. Could be 0, for new exercise.
        window.exerciseId = exerciseId;
        // Adapted from https://vegibit.com/vue-sibling-component-communication/
        window.eventBus = new Vue();
        Vue.config.productionTip = false;
        // Vue.use(CKEditor);
        Vue.use(VModal);
        let vueInstance = new Vue({
          store,
          render: h => h(App)
        }).$mount('#app');
        // Which form?
        // Look for add node form first.
        let insertForms = jQuery("form.node-exercise-form");
        let editForms = jQuery("form.node-exercise-edit-form");
        let theForm = null;
        if (insertForms.length === 0) {
          theForm = $(editForms).get(0);
        }
        else {
          theForm = $(insertForms).get(0);
        }
        // console.log("theForm", theForm);
        // jQuery("form.node-exercise-edit-form").on("submit", function(e) {
        $(theForm).on("submit", function(e) {
          // Cancel download, if running.
          window.axiosCanceler.cancel();
          // Get the data to send.
          let allTheThings = window.vueInstance.$store.getters.getAllTheThings();
          jQuery("input[name='rubric_item_chooser_return']").val(JSON.stringify(allTheThings));
          // console.log("allTheThings", allTheThings);
        });
        window.vueInstance = vueInstance;
        // console.log("leave doc ready");
      });
    },
    /**
     * Extract the exercise id from the URL, or 0 for a new record.
     * @return {null} | {number}
     */
    getExerciseIdFromUrl() {
      let urlComponents = window.location.href.split("/");
      let result = null;
      for (let i = 0; i < urlComponents.length - 2; i++) {
        if (urlComponents[i] === "node") {
          if (urlComponents[i + 2].slice(0, 4) === "edit") {
            result = urlComponents[i + 1];
            break;
          }
          if (urlComponents[i + 1].slice(0, 4) === "add") {
            result = 0;
            break;
          }
        }
      }
      return result;
    }
  }
})(jQuery, Drupal);

