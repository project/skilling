Switch to dir with webpack.config.js.

# rubric-item-proto

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Compiles for dev, no server run
```
npm run dev
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
