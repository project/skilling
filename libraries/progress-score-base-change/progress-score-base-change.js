/**
 * @file
 * Student can change basis on which progress score is completed.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings.progressScoreBaseChangeInitialized = false;
  Drupal.behaviors.progressScoreBaseChange = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.progressScoreBaseChangeInitialized ) {
        return;
      }
      Drupal.skillingSettings.progressScoreBaseChangeInitialized = true;
      $(document).ready(function() {
        $("#skilling-required-choice").on("click", function (event) {
          Drupal.behaviors.progressScoreBaseChange.changeBase();
        });
      });
    },
    changeBase: function(){
      const checked = $("#skilling-required-choice").prop("checked");
      // Show in-progress indicators.
      const $saving = $($("#skilling-changed-required-saving"));
      $saving.show();
      // Save new value.
      $.ajax({
        url: Drupal.url("skilling/save-progress-base"),
        type: "POST",
        dataType: "json",
        data: {
          csrfToken: drupalSettings.csrfToken,
          sessionId: drupalSettings.sessionId,
          required: checked
        },
        success(message) {
          $saving.hide();
          $("#skilling-changed-required").show();
          if (message.status.toLowerCase() !== "ok") {
            swal(message);
          }
        },
        fail(jqXHR, textStatus) {
          $(throbber).hide();
          const message = Drupal.t(
              "Skilling progress-based-on-required save fail @text. Please send a screen shot of your " +
              "entire browser window, including URL, to someone.",
              { "@text": textStatus }
          );
          swal(message);
        }

      })
    }
  }
})(jQuery, Drupal);
