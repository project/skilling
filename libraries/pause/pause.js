/*
 * Pause.
 */
(function($, Drupal) {
  Drupal.skillingSettings.setupPause = false;
  Drupal.behaviors.skillingPause = {
    attach: function(context, settings) {
      if (Drupal.skillingSettings.setupPause) {
        return;
      }
      Drupal.skillingSettings.setupPause = true;
      $(document).ready(function() {
        // Remove buttons that were recently used.
        $(".skilling-pause-container").each(function () {
          let buttonCounter = $(this).data("counter");
        });
        // On page load, hide content after a pause button group,
        // if prior code hasn't hidden the button.
        $(".skilling-pause-container.btn-group").each(function () {
            const followingElements = $(this).nextAll();
            $(followingElements).wrapAll("<div class='skilling-pause-followers' />");
          // }
        });

        /**
         * Show one group of hidden content.
         */
        let processingAll = false;
        $(".skilling-pause-container button.show-more").click(function() {
          // if ($(this).is(":visible")) {
            // Remember that this button was pressed.
            let buttonCounter = $(this)
                .closest(".skilling-pause-container")
                .data("counter");
            // sessionStorage.setItem("pause-button-" + buttonCounter, "clicked");
            if (! processingAll) {
              if (drupalSettings.recordingActive) {
                Drupal.behaviors.xapiSaveMoreClick('next', buttonCounter);
              }
            }
            let controlsContainer = $(this).parent();
            controlsContainer.hide();
            controlsContainer.next().show();
          // }
        });
        /**
         * Show all hidden content.
         */
        $(".skilling-pause-container a.dropdown-item.show-all").click(function(event) {
          event.preventDefault();
          // xAPI processing?
          if (drupalSettings.recordingActive) {
            let buttonCounter = $(this)
                .closest(".skilling-pause-container")
                .data("counter");
            Drupal.behaviors.xapiSaveMoreClick('all', buttonCounter);
          }
          processingAll = true;
          let nextButton;
          do {
            // Find a visible More button.
            nextButton = findVisibleMoreButton();
            // Found?
            if (nextButton !== null) {
              // Yes. Click it.
              $(nextButton).click();
            }
          } while (nextButton !== null);
          processingAll = false;
          return false;
        });
        /**
         * Find a visible More button.
         * @returns element|null
         *   Button, or null if none found.
         */
        let findVisibleMoreButton = function () {
          let result = null;
          let buttons = $(".skilling-pause-container button.show-more");
          $(buttons).each(function () {
            if ($(this).is(":visible")) {
              result = this;
            }
          });
          return result;
        };
      });
    } // End attach.
  };
  Drupal.behaviors.xapiSaveMoreClick = function(action, buttonCount) {
    var buttonsOnPage = $('.skilling-pause-container').length;
    $.ajax({
      url: Drupal.url("skilling-xapi/save-more-click"),
      type: 'POST',
      dataType: 'json',
      data: {
        "csrfToken": drupalSettings.csrfToken,
        "sessionId": drupalSettings.sessionId,
        "buttonCount": buttonCount,
        "buttonUse": action,
        "buttonsOnPage": buttonsOnPage,
        "nid": drupalSettings.skillingXapi.nid
      },
      success: function(result) {
      },
      fail: (function (jqXHR, textStatus) {
      })
    });
  };
}(jQuery, Drupal));
