/**
 * @file
 * Hack so that admin menus are hidden when there are no items.
 *
 * @see https://www.drupal.org/project/drupal/issues/296693#comment-12870898
 */

(function ($, Drupal) {
  "use strict";
  Drupal.doneMenuHack = false;
  Drupal.behaviors.menuHack = {
    attach: function (context, settings) {
      if (Drupal.doneMenuHack) {
        return;
      }
      Drupal.skillingSettings.doneMenuHack = true;
      // Remove Workflow menu if MT.
      let workflowMenu = $("a.toolbar-icon-system-admin-config-workflow").parent();
      if (workflowMenu.length > 0) {
        // There is a Workflow menu.
        if (workflowMenu.find("ul").length === 0) {
          // No items in the Workflow menu.
          workflowMenu.remove();
        }
      }
      // Kill help menus for users who only have the grader role.
      let isJustGrader =
          drupalSettings.currentUserRoles.includes("authenticated")
          && drupalSettings.currentUserRoles.includes("grader")
          && drupalSettings.currentUserRoles.length === 2;
      if (isJustGrader) {
        $("a.toolbar-icon-help-main").parent().remove();
      }
      do {
        $(".toolbar-menu-administration ul:not(:has(li))").remove();
        $(".toolbar-menu-administration li.menu-item--expanded").each(function () {
          if ($("ul", this).length === 0) {
            $(this).remove();
          }
        });
      } while ($(".toolbar-menu-administration ul:not(:has(li))").length > 0);
    }
  }
}(jQuery, Drupal));
