//alert('duck');
(function (Drupal, $) {
  "use strict";
  Drupal.assessmentWindow = null;
  Drupal.behaviors.startAssessmentInterface = {
    attach: function (context, settings) {
      $('#start-grading', context).once('starter').click(function () {
        Drupal.skillingSettings = settings;
        if ( Drupal.assessmentWindow == null
            || Drupal.assessmentWindow.closed ) {
          Drupal.assessmentWindow = window.open(
              settings.assessmentInterfaceUrl,
              'skillingAssessment',
              'height=900,width=1550,menubar=no,location=no,resizable=yes,scrollbars=no,status=yes,'
              + 'toolbar=no,personalbar=0'
          );
          Drupal.assessmentWindow.skillingSettings = settings;
        }
        else {
          Drupal.assessmentWindow.focus();
        }
      });
    }
  };
})(Drupal, jQuery);
