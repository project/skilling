/**
 * @file
 * ToC display.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings.tocInitialized = false;
  Drupal.behaviors.tocDisplay = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.tocInitialized ) {
        return;
      }
      Drupal.skillingSettings.tocInitialized = true;
      $(document).ready(function () {
        // Find all of the headings.
        let headingsData = findHeadings();
        // Exit if there are none.
        if (headingsData.length === 0) {
          return;
        }
        // Find the lowest heading number. It becomes the first level.
        let lowestHeadingNumber = findLowestHeadingNumber(headingsData);
        if (lowestHeadingNumber === null) {
          return;
        }
        // Adjust so that the lowest heading level is 1.
        headingsData = reduceHeadingLevels(headingsData, lowestHeadingNumber - 1);
        // Generate HTML based on the headings array.
        // HTML with links.
        let htmlWithLinks = makeHtmlForHeadingList(headingsData, true);
        // HTML without links.
        let htmlWithoutLinks = makeHtmlForHeadingList(headingsData, false);
        // Add HTML to document, where the ToC Drupal plugin has added markers.
        insertTocHtmlInDocument(htmlWithLinks, htmlWithoutLinks);
      });

      /**
       * Make an array of heading data for the page.
       *
       * @returns {Array}
       *   Array of objects for each heading.
       */
      let findHeadings = function () {
        let headings = [];
        // Find the content area.
        let contentArea = $("div.node__content");
        // Find all the heading tags in the content area from h2 onward.
        let headingTags = contentArea.find("h2, h3, h4, h5, h6");
        // Exit if there are none.
        if (headingTags.length === 0) {
          return headings;
        }
        // For each tag...
        headingTags.each(function(index, element) {
          let headingDeets = extractHeadingTagDeets(element);
          let rememberElement = true;
          let $element = $(element);
          if ($element.data("toc")) {
            let tocFlag = $element.data("toc").toLowerCase();
            if (tocFlag === "no" || tocFlag === "false" || tocFlag === "nay" || tocFlag === "0") {
              rememberElement = false;
            }
          }
          // Add to result.
          if (rememberElement) {
            headings.push(headingDeets);
          }
        });
        // Return the array.
        return headings;
      };

      let headingTagCount = 0;
      let extractHeadingTagDeets = function (headingTag) {
        // Find the id for the tag, to use in the jump.
        let headingTagId;
        let $headingTag = $(headingTag);
        // Does the heading have an id?
        if ($headingTag.attr("id")) {
          // Yes. Use that.
          headingTagId = $headingTag.attr("id");
        }
        else {
          // Make an id.
          headingTagId = "skilling-toc-" + headingTagCount;
          headingTagCount++;
        }
        // Make an indentation class name based on the tag.
        let tag = $headingTag.get(0).tagName;
        let tagLevel = tag.substr(1);
        // Get the text of the heading.
        let text = $headingTag.text();
        return {
          id: headingTagId,
          tagLevel: tagLevel,
          text: text
        };
      };

      /**
       * Find the lowest heading level.
       *
       * @param headingsData Headings data from extractHeadingTagDeets().
       * @returns {number} Lowest heading level.
       */
      let findLowestHeadingNumber = function(headingsData) {
        let lowest = null;
        for (let headingDatum of headingsData) {
          let headingLevel = parseInt(headingDatum.tagLevel);
          if (lowest === null || headingLevel < lowest) {
            lowest = headingLevel;
          }
        }
        return lowest;
      };

      /**
       * Reduce the heading levels by a given amount.
       *
       * @param headingsData Heading levels.
       * @param reduction Reduction amount.
       * @returns Adjusted levels.
       */
      let reduceHeadingLevels = function(headingsData, reduction) {
        for (let headingDatum of headingsData) {
          let headingLevel = parseInt(headingDatum.tagLevel);
          headingLevel -= reduction;
          headingDatum.tagLevel = headingLevel;
        }
        return headingsData;
      };

      let makeHtmlForHeadingList = function (headingTagsDeets, includeLinks) {
        // What class will the list items have?
        let linkTagClass = includeLinks
            ? "skilling-toc-with-links"
            : "skilling-toc-without-links";
        // Create the list opening tag.
        let result = "<div class='" + linkTagClass + "'>\n";
        // Loop across the array.
        for (let listItem of headingTagsDeets) {
          let itemClass = "skilling-toc-heading" + listItem.tagLevel;
          let listItemTag = "<div class='" + itemClass + "'>";
          if (includeLinks) {
            listItemTag += "<a href='#" + listItem.id + "'>" + listItem.text + "</a>";
          }
          else {
            listItemTag += listItem.text;
          }
          listItemTag += "</div>";
          // Add list item tag to overall result.
          result += listItemTag;
        }
        result += "</div>";
        return result;
      };

      let insertTocHtmlInDocument = function(htmlWithLinks, htmlWithoutLinks) {
        // Find the insertion locations.
        $("div.skilling-toc").each(function(index, tocMarker){
          let $tocMarker = $(tocMarker);
          let showLinks = $tocMarker.data("links");
          if (showLinks) {
            $tocMarker.html(htmlWithLinks);
          }
          else {
            $tocMarker.html(htmlWithoutLinks);
          }
        });
      };


    }
  }
})(jQuery, Drupal);
