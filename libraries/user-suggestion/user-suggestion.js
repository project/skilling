/*
 * User suggestion.
 */
(function($, Drupal) {
  Drupal.skillingSettings.setupUserSuggestion = false;
  Drupal.behaviors.userSuggestion = {
    attach: function(context, settings) {
      if (Drupal.skillingSettings.setupUserSuggestion) {
        return;
      }
      Drupal.skillingSettings.setupUserSuggestion = true;
      $(document).ready(function() {
        const modal = $("#suggestion-modal");
        if (modal.length === 0) {
          console.log("Suggestion modal element not found.");
          return;
        }
        // Move the modal somewhere better.
        modal.remove();
        $(".region-content").prepend(modal);
        const textarea = $("#suggestion");
        modal.on("shown.bs.modal", function() {
          // Remove previous value.
          textarea.val("");
          textarea.focus();
        });
        $("#cancel-button").click(function(){
          if (confirm(Drupal.t("Are you sure you want to forghedaboudit?"))) {
            $("#suggestion-modal").modal("hide");
          }
        });
        $("#suggestion-save").click(function() {
          let dataToSave = $(textarea).val();
          dataToSave = dataToSave.trim();
          if (dataToSave === "") {
            swal(Drupal.t("Sorry, nothing to save."));
            return;
          }
          const throbber = $(textarea)
            .parent()
            .find("img");
          $(throbber).show();
          $.ajax({
            url: Drupal.url("skilling/save-suggestion"),
            type: "POST",
            dataType: "json",
            data: {
              csrfToken: drupalSettings.csrfToken,
              sessionId: drupalSettings.sessionId,
              suggestion: dataToSave,
              page: window.location.href,
            },
            success: function(message) {
              $(throbber).hide();
              if (message.status.toLowerCase() !== "ok") {
                swal(message);
              }
              else {
                swal(Drupal.t("Thank you!"));
              }
              modal.modal("hide");
            },
            fail: function(jqXHR, textStatus) {
              $(throbber).hide();
              const message = Drupal.t(
                "Suggestion note save fail @text. Please send a screen shot of your " +
                  "entire browser window, including URL, to someone.",
                { "@text": textStatus}
              );
              swal(message);
            }
          });
        });
      });
    } // End attach.
  };
}(jQuery, Drupal));
