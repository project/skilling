npm modules needed:

* Vue
* Vuex
* Vuestrap
* Vuedraggable?
* Moment
* Vue good table


See https://michaelnthiessen.com/force-re-render/ for table rerender logic, on the vue-good-table element.

Copied sweet alert files to dist. Should get them from elsewhere.


https://momentjs.com/
https://lipis.github.io/bootstrap-sweetalert/
https://github.com/SortableJS/Vue.Draggable?
https://lipis.github.io/bootstrap-sweetalert/
https://github.com/xaksis/vue-good-table


To build dist:

Go to dir with webpack.config.js.

npm run build

