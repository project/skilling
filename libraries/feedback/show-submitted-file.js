function showSubmittedFile(event) {
  window.evnt = event;
  let target = $(event.target);
  let submissionId = target.data("submission-id");
  let url = target.data("url");
  // url = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hausziege_04.jpg/220px-Hausziege_04.jpg";
  // url = "http://s1.localhost/modules/contrib/skilling/images/smilies/neutral.svg";
  let fileName = target.data("name");
  let studentId = window.submissions[submissionId].studentId;
  let student = window.students[studentId];
  let studentName = student.firstName + " " + student.lastName;
  let exerciseId =  window.submissions[submissionId].exerciseId;
  let exerciseName = window.exercises[exerciseId].title;
  let newWindow = window.open("", "",
      "resizable=yes,status=yes,width=600,height=600");
  newWindow.document.title = "Student file submission";
  let htmlOutput = `
<h3>Student attached file</h3>
<div style="font-family: Sans-Serif;">
  <p><strong>Student:</strong> ${studentName}</p>
  <p><strong>Exercise:</strong> ${exerciseName}</p>
  <p><strong>File:</strong> ${fileName}</p>
  <iframe style="width: 100%;height: 100%;" id="student-submission" style="" 
  src="${url}" type="text/plain; length=16"></iframe>
 </div>`;
  newWindow.document.body.innerHTML = htmlOutput;
//   return;
//  let submittedFileData = window.submissions[submissionId].subFileLink[fileItemIndex];
//   console.log(submittedFileData);
//    newWindow = window.open("", "",
//       "resizable=yes,status=yes,width=600,height=600");
//   newWindow.document.title = "Student file submission";
//    htmlOutput = `
// <h3>Student attached file</h3>
// <div style="font-family: Sans-Serif;">
//   <p><strong>Student:</strong> ${submittedFileData.studentName}</p>
//   <p><strong>Exercise:</strong> ${submittedFileData.exerciseName}</p>
//   <p><strong>File:</strong> ${submittedFileData.fileName}</p>
//   <iframe style="width: 100%;height: 100%;" id="student-submission" style="" src="${submittedFileData.submittedFileUrl}"></iframe>
//  </div>`;
//   newWindow.document.body.innerHTML = htmlOutput;
}
