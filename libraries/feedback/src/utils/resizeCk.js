/**
 * Resize CK to fill pane.
 * Total height of pane container, less header height.
 * PLUS A SLIMY HACK. SEE FeedbackPane submissionId watcher.
 */
function resizeCk() {
  "use strict";
  if ( CKEDITOR
      && CKEDITOR.instances
      && CKEDITOR.instances['cyco-feedback-content'] ) {
    let paneTotalHeight
        = $('#cyco-feedback-content-container').parent().parent().height();
    let paneHeaderHeight
        = $('#cyco-feedback-content-container').parent().parent()
        .find('.cyco-pane-header').outerHeight();
    CKEDITOR.instances['cyco-feedback-content'].resize('100%',
        paneTotalHeight - paneHeaderHeight);
  }
  window.cycoFeedbackAnimating = false;
}
