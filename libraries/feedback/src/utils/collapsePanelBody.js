$(document).ready(function () {
  $(document).on('click', '.js-collapse-panel-body', function(e) {
    //When js-collapse-panel-body elements are nested, propagation causes problems.
    e.stopPropagation();
    e.preventDefault();
    // console.log("collapse clicked");
    var $this = $(this);
    var $panel = $this.closest('.panel');
    var panelBody = $panel.find('.panel-body');
    $(panelBody).toggle("medium");
    // $(panelBody).collapse('toggle'); //toggle();
    var iconSpan = $this.find('span.glyphicon');
    iconSpan.toggleClass('glyphicon-triangle-bottom');
    iconSpan.toggleClass('glyphicon-triangle-top');
  });
});

