/**
 * Animate a pane switch.
 * @param nameElementIdToAnimate Id of the pane to animate.
 * @param nextFunction Function to call after animation is finished. Optional.
 */
function animatePaneSwitch( nameElementIdToAnimate, nextFunction ) {
  if (! nextFunction ) {
    nextFunction = function(){};
  }
  let $elementToAnimate = $('#' + nameElementIdToAnimate);
  let oldTopMargin = $elementToAnimate.css('margin-top');
//      let oldLeftMargin = $elementToAnimate.css('margin-left');
  $elementToAnimate.css('opacity', 0);
//      console.log('oldTopMargin',oldTopMargin);
  $elementToAnimate.css('margin-top', 20);
//      $elementToAnimate.css('margin-left', 20);
  $elementToAnimate.animate({
    opacity: 1,
    marginTop: oldTopMargin,
//        marginLeft:oldLeftMargin
  }, 1000, nextFunction);
}