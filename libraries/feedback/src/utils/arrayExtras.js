/**
 * Find the number of elements being used in a sparse array.
 *
 * See http://coredogs.com/article/number-elements-javascript-array-length-misleading.html
 */
function numElements(arrayToProcess) {
  let numElements  = 0;
  for ( let indx = 0; indx < arrayToProcess.length; indx++ ) {
    if ( arrayToProcess[indx] ) {
      numElements ++;
    }
  }
  return numElements;
}
