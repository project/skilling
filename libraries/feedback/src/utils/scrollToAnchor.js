$(document).ready(function() {
  $(document).on('click', '#rubricItemDropDownContainer ul li', function(event) {
    event.preventDefault();
//     console.log('jump list event',event);
//     window.x = event.target;
//     window.y = event.currentTarget
// return;
    var idWanted = $(event.target).attr('href');
    //Reference the parent that is scrollish.
    var scrollishElement = $('#anchor-jump-list-targets').closest(".right-pane");
    var destination = $( idWanted).offset().top
        - $( idWanted ).parent().offset().top
        - $('#rubric-item-pane-cp').height()
        - parseInt($('.cyco-feedback-rubric-item-panel.panel').css('margin-top')) * 2;
    //Go there.
    $(scrollishElement).animate({
      scrollTop: destination
    }, 500);
  });
});