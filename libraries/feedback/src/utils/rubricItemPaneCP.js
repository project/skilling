/**
 * Adjust the display of the rubric item control panel.
 */
function adjustDisplayRubricItemCp() {
  "use strict";
  // console.log('resize');
  //Resize the CP to fill the space.
  let $cp = $('#rubric-item-pane-cp');
  if ( $cp ) {
    $('#anchor-jump-list-targets').css('padding-top', $cp.css('height'));
    $cp.width($cp.parent().width());
  }
  //Move list of rubric item titles left so user can see them.
  $('div#rubricItemDropDownContainer .dropdown-menu').css('margin-left',
      $('#rubricItemDropDown').width() - $('.dropdown-menu').width()
      + parseInt($('.dropdown-menu li a').css('padding-left')));
  //Scroll the submission window to the top.
  document.getElementById("submission-pane-container").parentNode.scrollTop = 0;
  //Open all of the rubric items.
  // console.log("Opening all");
  $(".panel-heading.js-collapse-panel-body").each(function( index ) {
    //this is one of the collapse panel bodies.
    let collapseExpandGlyph = $(this).find("span.glyphicon");
    let collapsed = collapseExpandGlyph.hasClass('glyphicon-triangle-top');
    // console.log("collapsed", collapsed);
    if (collapsed) {
      // Expand it.
      // console.log("Expanding");
      collapseExpandGlyph.click();
    }
  });
}
