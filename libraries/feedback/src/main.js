import Vue from 'vue'
import App from './App.vue'
// import VueGoodTable from 'vue-good-table';
// Vue.use(VueGoodTable);

import swal from 'sweetalert';

$(document).ready(function(){
    // Ask user to wait.
    swal ( {
        title: "",
        text: "Loading submissions...",
        icon: "src/images/ajax-loader.gif",
        buttons: {
            forghedaboudit: {
                text: "Forghedaboudit",
                value: "forghedaboudit",
            },
        },
    }).then((value) => {
        if (value === "forghedaboudit") {
            window.close();
        }
    });
    // loadFakeData();
    loadData().then(function () {
        startUp();
        swal.close();
    });
});


function startUp() {
  window.v = new Vue({
    el: '#app',
    render: h => h(App)
  });
}

