/**
 * @file
 * Server data access functions are all in this file.
 */

var skillingDebug = false;

/**
 * Load data for grading from the server.
 *
 * @returns {*|jQuery}
 */
function loadData() {
  // Create a new Deferred object.
  let deferred = $.Deferred();
  $.when(
    loadGraderPersona(),
    loadUngradedSubmissions(),
    loadClasses(),
    loadGrader()
  )
    .then(loadStudents)
    .then(loadExercises)
    .fail(function (result) {
      let report = Drupal.behaviors.skillingUtilities.createFailReport(result);
      swal.close();
      swal({
        title: "Fail!",
        text: report,
        icon: "error",
        button: "Close window",
      }).then((value) => {
         window.close();
      });
    })
    .done(function () {
    // Create some places to load data in the future.
    window.rubricItems = [];
    window.responseOptions = [];
    deferred.resolve();
  });
  // Return the Deferred's Promise object.
  return deferred.promise();
}

/**
 * Load grader persona.
 *
 * @returns {*|jQuery}
 */
function loadGraderPersona() {
  let deferred = $.Deferred();
  $.get('/skilling/get-grader-persona',
    {
      'sessionId': window.skillingSettings.sessionId,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadGraderPersona done!', dataReturned);
    }
    // Save the data returned.
    window.greetings = dataReturned.greetings;
    window.signatures = dataReturned.signatures;
    window.summaries = [];
    window.summaries['good'] = dataReturned.summaryGood;
    window.summaries['needs work'] = dataReturned.summaryNeedsWork;
    window.summaries['poor'] = dataReturned.summaryPoor;
    deferred.resolve();
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadGraderPersona fail!', dataReturned);
    }
    dataReturned.failFunction = "loadGraderPersona";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load ungraded submissions.
 *
 * @returns {*|jQuery}
 */
function loadUngradedSubmissions() {
  let deferred = $.Deferred();
  $.get('/skilling/get-submissions-for-grader',
    {
      'sessionId': window.skillingSettings.sessionId,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadUngradedSubmissions done!', dataReturned);
    }
    // Save the data returned.
    window.submissions = [];
    for (let submissionId in dataReturned) {
      window.submissions[submissionId] = dataReturned[submissionId];
    }
    if (skillingDebug) {
      console.log(window.submissions);
    }
    deferred.resolve();
  }).fail(function (dataReturned) {
    console.log('loadUngradedSubmissions fail!', dataReturned);
    dataReturned.failFunction = "loadUngradedSubmissions";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load classes for the grader.
 *
 * @returns {*|jQuery}
 */
function loadClasses() {
  // Get the data.
  let deferred = $.Deferred();
  $.get('/skilling/get-class-list-for-grader',
    {
      'sessionId': window.skillingSettings.sessionId,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadClasses done!', dataReturned);
    }
    // Save the data returned.
    window.classes = [];
    for (let classId in dataReturned) {
      window.classes[classId] = dataReturned[classId];
    }
    if (skillingDebug) {
      console.log(window.classes);
    }
    deferred.resolve();
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadClasses fail!', dataReturned);
    }
    dataReturned.failFunction = "loadClasses";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load student data.
 *
 * @returns {*|jQuery}
 */
function loadStudents() {
  window.students = [];
  // Get students need data about.
  let studentIds = [];
  window.submissions.forEach(submission => {
    // More than one submission per student, so need to
    // eliminate duplicates.
    if (studentIds.indexOf(submission.studentId) === -1) {
      studentIds.push(submission.studentId);
    }
  });
  if (skillingDebug) {
    console.log("Student ids ", studentIds);
  }
  if (studentIds.length === 0) {
    return;
  }
  // Get the data.
  let deferred = $.Deferred();
  $.get('/skilling/get-student-list-for-grader',
    {
      'sessionId': window.skillingSettings.sessionId,
      'studentIds': studentIds,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadStudents done!', dataReturned);
    }
    // Save the data returned.
    for (var studentId in dataReturned) {
        window.students[studentId] = dataReturned[studentId];
    }
    if (skillingDebug) {
      console.log(window.students);
    }
    deferred.resolve();
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadStudents fail!', dataReturned);
    }
    dataReturned.failFunction = "loadStudents";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load data about a grader.
 *
 * @returns {*|jQuery}
 */
function loadGrader(){
  let deferred = $.Deferred();
  $.get('/skilling/get-grader-for-grader',
    {
      'sessionId': window.skillingSettings.sessionId,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadGrader done!', dataReturned);
    }
    // Save the data returned.
    window.grader = {};
    window.grader.graderId = dataReturned.graderId;
    window.grader.firstName = dataReturned.firstName;
    window.grader.lastName = dataReturned.lastName;
    if (skillingDebug) {
      console.log(window.grader);
    }
    deferred.resolve();
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadGrader fail!', dataReturned);
    }
    dataReturned.failFunction = "loadGrader";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load exercise data.
 *
 * @returns {*|jQuery}
 */
function loadExercises() {
  window.exercises = [];
  // Get exercises need data about.
  let exerciseIds = [];
  window.submissions.forEach(submission => {
    // More than one submission per student, so need to
    // eliminate duplicates.
    if (exerciseIds.indexOf(submission.exerciseId) === -1) {
      exerciseIds.push(submission.exerciseId);
    }
  });
  if (skillingDebug) {
    console.log("Exercise ids ", exerciseIds);
  }
  if (exerciseIds.length === 0) {
    return;
  }
  // Get the data.
  let deferred = $.Deferred();
  $.get('/skilling/get-exercises-for-grader',
    {
      'sessionId': window.skillingSettings.sessionId,
      'exerciseIds': exerciseIds,
      'csrfToken': window.skillingSettings.csrfToken
    }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadExercises done!', dataReturned);
    }
    // Save the data returned.
    for (var exerciseId in dataReturned) {
        window.exercises[exerciseId] = dataReturned[exerciseId];
    }
    if (skillingDebug) {
      console.log(window.students);
    }
    deferred.resolve();
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('loadExercises fail!', dataReturned);
    }
    dataReturned.failFunction = "loadExercises";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Load rubric items and response options from server.
 *
 * @param rubricItemIds Ids of rubric items.
 *
 * @returns Promise.
 */
function loadRubricItems(rubricItemIds) {
  // Create a new Deferred object.
  let deferred = $.Deferred();
  // Find the rubric items and responses that have not been loaded yet.
  let missingRubricItemIds = [];
  rubricItemIds.forEach(rubricItemId => {
    if (window.rubricItems[rubricItemId] === undefined) {
      missingRubricItemIds.push(rubricItemId);
    }
  });
  if (missingRubricItemIds.length === 0) {
    // Got them all.
    deferred.resolve();
  }
  else {
    // Ask the server for the rubric items and their responses.
    $.get('/skilling/get-rubric-items-for-grader',
        {
          'sessionId': window.skillingSettings.sessionId,
          'rubricItemIds': missingRubricItemIds,
          'csrfToken': window.skillingSettings.csrfToken
        }
    ).done(function (dataReturned) {
      if (skillingDebug) {
        console.log('loadRubricItems done!', dataReturned);
      }
      // Save the data returned.
      let rubricItemsReturned = dataReturned.rubricItems;
      for (var rubricItemId in rubricItemsReturned) {
        window.rubricItems[rubricItemId] = rubricItemsReturned[rubricItemId];
      }
      if (skillingDebug) {
        console.log(window.rubricItems);
      }
      let responseOptionsReturned = dataReturned.responseOptions;
      for (var responseItemId in responseOptionsReturned) {
        window.responseOptions[responseItemId] = responseOptionsReturned[responseItemId];
        // Change completes from a 0/1 sent from server to a boolean.
        window.responseOptions[responseItemId].completes
          = (window.responseOptions[responseItemId].completes == 1);
      }
      if (skillingDebug) {
        console.log(window.responseOptions);
      }
      deferred.resolve();
    }).fail(function (dataReturned) {
      if (skillingDebug) {
        console.log('loadRubricItems fail!', dataReturned);
      }
      dataReturned.failFunction = "loadRubricItems";
      let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
      swal.close();
      swal({
        title: "Fail!",
        text: report,
        icon: "error",
        button: "Close report",
      });
      deferred.reject(dataReturned);
    });
  }
  // Return the Deferred's Promise object.
  return deferred.promise();
}

/**
 * Save new response options.
 *
 * @param newResponseOptions
 *
 * @returns {*|jQuery}
 */
function saveNewResponseOptions(newResponseOptions) {
  let deferred = $.Deferred();
  // If nothing to send, resolve immediately.
  if (newResponseOptions.length === 0) {
    deferred.resolve([]);
  }
  else {
    $.post('/skilling/save-new-response-options',
        { 'sessionId': window.skillingSettings.sessionId,
          'csrfToken': window.skillingSettings.csrfToken,
          'newResponseOptions': newResponseOptions
        }
    ).done(function (dataReturned) {
      if (skillingDebug) {
        console.log('newResponseOptions done!', dataReturned);
      }
      deferred.resolve(dataReturned);
    }).fail(function (dataReturned) {
      if (skillingDebug) {
        console.log('newResponseOptions fail!', dataReturned);
      }
      dataReturned.failFunction = "newResponseOptions";
      let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
      swal.close();
      swal({
        title: "Fail!",
        text: report,
        icon: "error",
        button: "Close report",
      });
      deferred.reject(dataReturned);
    });

  }
  return deferred.promise();
}

/**
 * Save feedback to server.
 *
 * @param submissionId Id of submission to save feedback for.
 *  Feedback data will already be in the window.submission object
 *  for that submission.
 *
 * @return Promise.
 */
function saveFeedback(submissionId) {
  let deferred = $.Deferred();
  // Make an object to return the feedback.
  let submission = window.submissions[submissionId];
  // Create the single-valued properties first.
  let feedback = {
    'submissionId': submissionId,
    'exerciseCompleted': submission.exerciseCompleted,
    'overallEvaluation': submission.overallEvaluation,
    'feedback': submission.feedback
  };
  // Add multi-valued rubric item choices. Make it an object, otherwise
  // get many MT values in array.
  responseOptionsChosen = {};
  window.submissions[submissionId].rubricItemChoices
      .forEach(function (optionsChosen, rubricItemId) {
        responseOptionsChosen[rubricItemId] = optionsChosen;
  });
  feedback.rubricItemChoices = responseOptionsChosen;
  $.post('/skilling/save-feedback',
      {
        'sessionId': window.skillingSettings.sessionId,
        'csrfToken': window.skillingSettings.csrfToken,
        'feedbackData': feedback
      }
  ).done(function (dataReturned) {
    deferred.resolve(dataReturned);
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('saveSubmission fail!', dataReturned);
    }
    dataReturned.failFunction = "saveSubmission";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}

/**
 * Delete grader file.
 *
 * @param submissionId
 * @param fileId
 *
 * @returns {*|jQuery}
 */
function deleteGraderFile(submissionId, fileId) {
  let deferred = $.Deferred();
  $.post('/skilling/delete-grader-file',
      { 'sessionId': window.skillingSettings.sessionId,
        'csrfToken': window.skillingSettings.csrfToken,
        'submissionId': submissionId,
        'fileId': fileId
      }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('deleteGraderFile done!', dataReturned);
    }
    deferred.resolve(dataReturned);
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('deleteGraderFile fail!', dataReturned);
    }
    dataReturned.failFunction = "deleteGraderFile";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}


function uploadGraderFile(submissionId, fileToUpload) {
  let deferred = $.Deferred();
  let fd = new FormData();
  fd.append('submissionId', submissionId);
  fd.append('file', fileToUpload);
  fd.append('sessionId', window.skillingSettings.sessionId);
  fd.append('csrfToken',window.skillingSettings.csrfToken);
  $.ajax({
        url: '/skilling/upload-grader-file',
        type: "post",
        data: fd,
        contentType: false,
        processData: false
      }

  // $.post('/skilling/upload-grader-file', fd
      // { 'sessionId': window.skillingSettings.sessionId,
      //   'csrfToken': window.skillingSettings.csrfToken,
      //   'submissionId': submissionId,
      //   'formData': fileToUpload
      // }
  ).done(function (dataReturned) {
    if (skillingDebug) {
      console.log('uploadGraderFile done!', dataReturned);
    }
    deferred.resolve(dataReturned);
  }).fail(function (dataReturned) {
    if (skillingDebug) {
      console.log('uploadGraderFile fail!', dataReturned);
    }
    dataReturned.failFunction = "uploadGraderFile";
    let report = Drupal.behaviors.skillingUtilities.createFailReport(dataReturned);
    swal.close();
    swal({
      title: "Fail!",
      text: report,
      icon: "error",
      button: "Close report",
    });
    deferred.reject(dataReturned);
  });
  return deferred.promise();
}
