import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    calendarName: 'Standard OU semester',
    notes: 'For fall and winter semesters.',
    calendarStart: 'Dec 11, 1956', //'12/11/1956',
    //This is the important one.
    numberDays: 100,
    //Display value.
    calendarLengthLabel: '15 weeks',
    days:[],
    dayNames: [
      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ],
  },
  mutations: {
    addDay(state, day) {
      "use strict";
      state.days.push(day);
    },
    updateDays(state, newDays) {
      "use strict";
      state.days = newDays;
    },
    setStartDate(state, startDate) {
      "use strict";
      state.calendarStart = startDate;
    },
    setNumberDays(state, numberDays) {
      "use strict";
      state.numberDays = numberDays;
    }
  }
});






