/**
 * @file
 * Handle client-side of submission creating/editing for students.
 */

(function (Drupal, $) {
  "use strict";
  Drupal.skilling = Drupal.skilling || {};
  Drupal.skilling.adjustedSubmissionFloater = false;
  Drupal.skilling.runReady = false;
  Drupal.behaviors.floatingSubmission = {
    /**
     * Adjust the page when it is a floating submission window.
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (Drupal.skilling.adjustedSubmissionFloater) {
        return;
      }
      Drupal.skilling.adjustedSubmissionFloater = true;
      // Attach runs before some things on the page have finished rendering.
      // The elements that need to be removed might not exist yet.
      // So delay the following code until the page is ready.
      $(document).ready(function () {
        if (Drupal.skilling.runReady) {
          return;
        }
        Drupal.skilling.runReady = true;
        // Need an exercise nid when closing popup window.
        if (drupalSettings.skillingExerciseNid === undefined) {
          throw new Error("Missing exercise nid.");
        }
        // Add destination of close window when submission saved.
        let isEdit = drupalSettings.skillingFloatOperation === "edit";
        let isAdd = drupalSettings.skillingFloatOperation === "add";
        if (isEdit || isAdd) {
          let formId = '';
          if (isEdit) {
            formId = "node-submission-edit-form";
            // Leave exercise nid at 0, to tell opener's
            // popup closing logic that links on the opener
            // don't need to be updated. Edits don't change
            // those links. Only adds and deletes do.
          }
          if (isAdd) {
            formId = "node-submission-form";
          }
          if (formId === '') {
            throw new Error("Floated sub: form not found.");
          }
          Drupal.behaviors.floatingSubmission.addCloseDestination(
              formId,
              drupalSettings.skillingFloatOperation,
              drupalSettings.skillingExerciseNid,
              drupalSettings.skillingSubmissionNid
          );
        }
        // Floating delete form?
        if (drupalSettings.skillingFloatOperation === "delete") {
          // Remove action tabs.
          // $(".tabs--primary").remove();
          // $("ul.vertical-tabs-list").remove();
          // $("div.vertical-tabs-panes").remove();
          // Add a destination to the delete confirm button.
          Drupal.behaviors.floatingSubmission.addCloseDestination(
              "node-submission-delete-form",
              drupalSettings.skillingFloatOperation,
              drupalSettings.skillingExerciseNid,
              drupalSettings.skillingSubmissionNid
          );
          // Change what the Cancel link does.
          $("#edit-cancel").attr("href", "#").attr("onclick", "window.close()");
        }
        // Require something is submitted.
        $("form").submit(function (event) {
          // Get solution text.
          let submittedHtml = CKEDITOR.instances["edit-field-solution-0-value"].getData();
          let submittedText = $(submittedHtml).text().trim();
          // Get count of files submitted.
          let numFiles = $("div.js-form-managed-file").length - 1;
          if (submittedText === "" && numFiles === 0) {
            swal(Drupal.t("Please submit something."));
            event.preventDefault();
          }
          // Check that there are initials.
          // let initialsField = $("#edit-field-initials-0-value");
          // let submittedInitials = initialsField.val().trim();
          // initialsField.val(submittedInitials);
          // if (submittedInitials.length === 0) {
          //   swal(Drupal.t("Please enter your initials."));
          //   initialsField.focus();
          //   event.preventDefault();
          // }
          // Are they the right initials?
          // let rightInitials = drupalSettings.userInitials.trim();
//           if (submittedInitials.toLowerCase() !== rightInitials.toLowerCase()) {
//             swal(
//                 {
//                   title: Drupal.t("Wrong initials"),
//                   text: Drupal.t(
// `These aren't the initials for your account. If you need to change your account, close the submission window, and choose Account from Your stuff in the main menu. Remember to copy your solution first, if you need to.`
//                   ),
//                   icon: "error",
//                   button: Drupal.t("Got it")
//                 }
//             );
//             initialsField.focus();
//             event.preventDefault();
//           }
        });
      });
    },
    /**
     * Add destination to form's action, that closes the current window.
     *
     * @param formId
     * @param operation
     * @param exerciseNid
     * @param submissionNid
     */
    addCloseDestination: function (formId, operation, exerciseNid, submissionNid) {
      if (operation !== "add"
           && operation !== "edit"
           && operation !== "delete")
        throw new Error("addCloseDestination: bad op: " + operation);
      if (! exerciseNid) {
        exerciseNid = 0;
      }
      if (! submissionNid) {
        submissionNid = 0;
      }
      let destination = drupalSettings.path.baseUrl;
      // Make sure the base URL has a / at the end.
      if (destination.slice(-1) !== "/") {
        destination += "/";
      }
      destination += "skilling/close-submission-window";
      let nodeSubmissionForm = $("#" + formId);
      let formAction = nodeSubmissionForm.attr("action");
      // Compute whether to add a ? to the url (there are no params there already)
      // or a & (there are no params already).
      let glueCharacter = "";
      if (formAction.indexOf("?") === -1) {
        // No existing ?, so use that.
        glueCharacter = "?";
      }
      else {
        // There is a ?, so use &.
        glueCharacter = "&";
      }
      formAction += glueCharacter + "destination=" + destination
          + "/" + operation
          + "/" + exerciseNid
          + "/" + submissionNid;
      nodeSubmissionForm.attr("action", formAction);
    }
  }
})(Drupal, jQuery);
