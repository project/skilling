/*
 * Reflect note processing.
 */
(function($, Drupal) {
  Drupal.skillingSettings.setupReflectNotes = false;
  Drupal.skillingSettings.reflectNoteSaveDelay = 1000;
  Drupal.behaviors.reflectNoteProcessing = {
    attach(context, settings) {
      if (Drupal.skillingSettings.setupReflectNotes) {
        return;
      }
      Drupal.skillingSettings.setupReflectNotes = true;
      // Skip for nonstudents.
      if (!drupalSettings.userIsStudent) {
        return;
      }
      // Array to collect timeout objects.
      Drupal.skillingSettings.reflectNoteTimeouts = [];
      // Flags showing whether user has typed in textareas.
      Drupal.skillingSettings.reflectNotesChanged = [];
      $(document).ready(function() {
        window.onbeforeunload = function() {
          // Are there any saves still needed?
          let savesNotFinished = Drupal.skillingSettings.reflectNotesChanged.length > 0;
          if (savesNotFinished) {
            return Drupal.t(
              "Some reflections are still saving. Are you sure you want to leave now? You could wait a couple of seconds, and give them time to finish."
            );
          }
        };
        // Find reflect elements.
        const reflectElements = $(".skilling-reflect-container");
        $(reflectElements).each(function() {
          // Has the student already entered a note for this reflect field?
          var existingNoteLength = parseInt(
            $(this).data("existing-note-length")
          );
          if (existingNoteLength === 0) {
            // The student has not entered a note yet.
            // The .html.twig template will have shown the Continue button,
            // but disabled. It will be enabled when the user types
            // something in the textarea.
            // Get the elements following the reflect element.
            const followingElements = $(this).nextAll();
            // Wrap all of the elements for hiding.
            $(followingElements).wrapAll(
              "<div class='skilling-reflect-followers' />"
            );
            // Clear the textarea. Otherwise, browser autofill might
            // put an initial value in it.
            $(this).find("textarea").val("");
          }
          $(this)
            .find("textarea")
            .keyup(function() {
              // Compute the identifier for reflect note container.
              // It's associated with a particular timeout.
              const container = $(this).closest(".skilling-reflect-container");
              const containerIdentifier = container.data("identifier");
              // Set a timeout for saving.
              if (
                Drupal.skillingSettings.reflectNoteTimeouts[containerIdentifier] !== undefined
              ) {
                window.clearTimeout(
                  Drupal.skillingSettings.reflectNoteTimeouts[containerIdentifier]
                );
              }
              Drupal.skillingSettings.reflectNoteTimeouts[containerIdentifier] = setTimeout(
                Drupal.behaviors.saveReflectNote,
                Drupal.skillingSettings.reflectNoteSaveDelay,
                containerIdentifier
              );
              // Flag to remember that it has changed.
              if (Drupal.skillingSettings.reflectNotesChanged.indexOf(containerIdentifier) === -1) {
                Drupal.skillingSettings.reflectNotesChanged.push(containerIdentifier);
              }
              // Is there is a button to show the following content?
              // There won't be, if the user has entered some reflect text previously.
              // In that case, the content will not be hidden.
              const button = container.find("button");
              if (button.length > 0) {
                // Yes, there is a button.
                // Is there text in the textarea?
                if ($(this).val().length === 0) {
                  // No text - disable the button.
                  button.attr("disabled", "disabled");
                } else {
                  // There is text, enable the button.
                  button.removeAttr("disabled");
                }
              }
            });
        });
        /**
         * Click continue button.
         */
        $(".skilling-reflect-container .skilling-reflect-continue").click(
          function() {
            // Get the parent of the button. It's the reflect element container.
            const reflectElement = $(this).parent();
            // Get the next element. It will be the follower wrapper.
            const followersWrapper = $(reflectElement).next();
            // Hide the button.
            $(this).hide();
            // Show the followers.
            $(followersWrapper).show();
            // Save all unsaved reflect notes.
            Drupal.skillingSettings.reflectNotesChanged.forEach(
                function (containerId) {
                    Drupal.behaviors.saveReflectNote(containerId);
            });
            Drupal.skillingSettings.reflectNotesChanged = [];
          }
        );
      }); // End document ready.
      Drupal.behaviors.saveReflectNote = function(identifier) {
        const textarea = $(
          `.skilling-reflect-container[data-identifier=${identifier}] textarea`
        );
        const dataToSave = $(textarea).val();
        const throbber = $(textarea)
          .parent()
          .find("img");
        // Show that no changes to process. Remove identifier from array.
        const index = Drupal.skillingSettings.reflectNotesChanged.indexOf(identifier);
        if (index !== -1) {
          Drupal.skillingSettings.reflectNotesChanged.splice(identifier, 1);
        }
        $(throbber).show();
        $.ajax({
          url: Drupal.url("skilling/save-reflect-note"),
          type: "POST",
          dataType: "json",
          data: {
            csrfToken: drupalSettings.csrfToken,
            sessionId: drupalSettings.sessionId,
            identifier,
            note: dataToSave
          },
          success(message) {
            $(throbber).hide();
            if (message.status.toLowerCase() !== "ok") {
              swal(message);
            }
          },
          fail(jqXHR, textStatus) {
            $(throbber).hide();
            const message = Drupal.t(
              "Skilling reflect note save fail @text. Please send a screen shot of your " +
                "entire browser window, including URL, to someone.",
              { "@text": textStatus }
            );
            swal(message);
          }
        });
      };
    } // End attach.
  };
})(jQuery, Drupal);
