/**
 * @file
 * Implements Ajax command to render a book tree.
 */
(function ($, Drupal) {
Drupal.AjaxCommands.prototype.renderBookTree = function (ajax, response, status) {
  //Show the tree in a container that includes the book id as part of its id.
  //Ids are set up in SkillingBookNavBlock->build().
  let containerId = '#skilling-book-nav-' + response.bid;
  let $container = $(containerId);
  //Flag to avoid response to activating node when initializing.
  let initializing = true;
  let tree = $container.fancytree({
    source: response.bookTree,
    activate: function(event, data) {
      if ( ! initializing ) {
        let nid = data.node.key;
        if (nid) {
          window.location.href = Drupal.behaviors.skillingUtilities.getRootUrl() + "node/" + nid;
        }
      }
    },
    autoActivate: false,
    icon: false
  });
  //Select tree item for the current Drupal node.
  let path = drupalSettings.path.currentPath;
  if ( path.indexOf("node/") === 0 ) {
    let nid = path.slice(5);
    if ( ! isNaN(nid) ) {
      //Look up the node in the tree.
      let node = tree.fancytree("getTree").getNodeByKey(nid);
      //Found it? Might not, is there are multiple trees on the page.
      if ( node ) {
        node.setSelected(true);
        node.setActive(true);
      }
    }
  }
  initializing = false;
  //Erase the loading throbber.
  $container.find(".skilling-throbber-container").remove();
}
})(jQuery, Drupal);
