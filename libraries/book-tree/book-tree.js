/**
 * @file
 * Make book nav trees.
 *
 * Works best with the Bootstrap theme.
 */

(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings = Drupal.skillingSettings || {};
  // Flag to track initialization.
  Drupal.skillingSettings.shownBookTrees = false;
  Drupal.behaviors.showBookTrees = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.shownBookTrees) {
        return;
      }
      Drupal.skillingSettings.shownBookTrees = true;
      $(document).ready(function() {
        //Select tree item for the current Drupal node.
        let path = drupalSettings.path.currentPath;
        if ( path.indexOf("node/") === 0 ) {
          let nid = path.slice(5);
          if ( ! isNaN(nid) ) {
            // Find the tree node for the current Drupal node.
            let $current = $($("a[data-key='" + nid + "']"));
            if ($current) {
              $current.addClass("current");
              while($current.parent().is("li") || $current.parent().is("ul")) {
                $current = $current.parent();
                if ($current.hasClass("collapse")) {
                  $current.collapse("toggle");
                }
              }
            }
          }
        }
      });
    }
  }
})(jQuery, Drupal);
