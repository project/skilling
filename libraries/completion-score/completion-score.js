/**
 * @file
 * Completion score display.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings.completionScoreDisplayInitialized = false;
  Drupal.behaviors.completionScoreDisplay = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.completionScoreDisplayInitialized ) {
        return;
      }
      Drupal.skillingSettings.completionScoreDisplayInitialized = true;
      $(document).ready(function() {
        // Get image data from drupalSettings.
        let srcPath = drupalSettings.completionScoreFileName;
        let altText = drupalSettings.completionScoreText;
        // Change the main menu item, if it is there.
        let menuItem = $("#skilling-completion-score-main-menu-item");
        let html = "<img src='" + srcPath + "' alt='" + altText + '" title="' +
            altText + '">';
        if (menuItem) {
          menuItem.html(html);
        }
        // Change other places on the page where the score image should be shown,
        // if they are there.
        let scoreImageElements = $(".skilling-completion-score-image");
        scoreImageElements.each(function () {
          $(this).html(html);
        });
      });
    }
  }
})(jQuery, Drupal);
