/**
 * @file
 * Repeat book nav at top of page.
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.bookNavInitialized = false;
  Drupal.behaviors.bookNav = {
    attach: function(context, settings) {
      if (Drupal.skillingSettings.bookNavInitialized) {
        return;
      }
      Drupal.skillingSettings.bookNavInitialized = true;
      $(document).ready(function() {
        const bookPager = $(".book-pager");
        if (bookPager.length === 1) {
          const newBookPager = bookPager.clone();
          newBookPager.insertBefore(".region-content .field--name-body");
        }
      });
    }
  };
})(jQuery, Drupal);
