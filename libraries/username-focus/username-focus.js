/**
 * @file
 * Give username field focus on page load.
 */
(function ($, Drupal) {

  return;


  "use strict";
  Drupal.skillingSettings.usernameFocusInitialized = false;
  Drupal.behaviors.usernameFocus = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.usernameFocusInitialized ) {
        return;
      }
      Drupal.skillingSettings.usernameFocusInitialized = true;
      $(document).ready(function() {
        $("form#user-login-form #edit-name").focus();
      });
    }
  }
})(jQuery, Drupal);
