/**
 * @file
 * Remove list links for which there are no items.
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.adjustListLinksInitialized = false;
  Drupal.behaviors.adjustListLinks = {
    attach: function(context, settings) {
      if (Drupal.skillingSettings.adjustListLinksInitialized) {
        return;
      }
      Drupal.skillingSettings.adjustListLinksInitialized = true;
      $(document).ready(function() {
        let contentTypesCounts = drupalSettings.contentTypeCounts;
        for (const contentType in contentTypesCounts) {
          const count = contentTypesCounts[contentType];
          if (count === 0) {
            // Remove the menu item.
            $("a[data-drupal-link-system-path='" + contentType + "s']").remove();
          }
        }
      });
    }
  };
})(jQuery, Drupal);
