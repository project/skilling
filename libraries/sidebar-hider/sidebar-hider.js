/*
 * Show/hide sidebars.
 *
 * Here's data collected about sidebars.
 *
 * The entire sidebar.
 *    sidebar: null,
 *  Div inside it that has a border and background color.
 *  This is the thing that will show during animation.
 *    sidebarContainer: null,
 *  Div inside that that has content. It will be made invisible
 *  during animation, to avoid repainting it.
 *    sidebarContainerContent: null,
 *  Width of the sidebar.
 *    sidebarWidth: 0,
 *  Width of the container of the content.
 *    contentContainerWidth: 0,
 *  Height of the container of the content.
 *    contentContainerHeight: 0,
 *  Where to stop changing left pos.
 *    sidebarStopPos: 0,
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.setupSidebarHider = false;
  // Define some shared vars.
  Drupal.skillingSettings.sidebarHider = {
    speed: "medium",
    // // Px per step.
    // stepDelta: 15,
    // // Ms per step.
    // stepTime: 10,
    right: {
      localStorageId: "skillingShowRightSidebar"
    },
    left: {
      localStorageId: "skillingShowLeftSidebar"
    },
    showSidebarTitle: Drupal.t("Show sidebar"),
    hideSidebarTitle: Drupal.t("Hide sidebar"),
    leftArrowCharacter: "\u21a4", //"&#x2BC7;",
    rightArrowCharacter: "\u21a6", //"&#x2BC8;",
  };
  Drupal.behaviors.sidebarHider = {
    attach: function(context, settings) {
      if (Drupal.skillingSettings.setupSidebarHider) {
        return;
      }
      Drupal.skillingSettings.setupSidebarHider = true;
      $(document).ready(function(){
        let behaviors = Drupal.behaviors.sidebarHider;
        // Set up the RSB.
        let right = Drupal.skillingSettings.sidebarHider.right;
        behaviors.findDomElements(right, drupalSettings.rightSideBarId);
        behaviors.initRSBPos(right);
        // Set up the LSB.
        let left = Drupal.skillingSettings.sidebarHider.left;
        behaviors.findDomElements(left, drupalSettings.leftSideBarId);
        behaviors.initLSBPos(left);
        /**
         * Right toggle button clicked.
         */
        $("#skilling-toggle-right-sidebar").click(function () {
          // behaviors.prepSidebarMove(right);
          if (right.showRight === "yes") {
            // Sidebar is showing. Hide it.
            behaviors.hideRightSidebar(right);
            right.showRight = "no";
          }
          else {
            // Sidebar is hidden. Show it.
            behaviors.showRightSidebar(right);
            right.showRight = "yes";
          }
          localStorage.setItem(right.localStorageId, right.showRight);
        }); // End toggle right sidebar click.
        /**
         * Left toggle button clicked.
         */
        $("#skilling-toggle-left-sidebar").click(function () {
          // behaviors.prepSidebarMove(left);
          if (left.showLeft === "yes") {
            // Sidebar is showing. Hide it.
            behaviors.hideLeftSidebar(left);
            left.showLeft = "no";
          }
          else {
            // Sidebar is hidden. Show it.
            behaviors.showLeftSidebar(left);
            left.showLeft = "yes";
          }
          localStorage.setItem(left.localStorageId, left.showLeft);
        }); // End toggle left sidebar click.
      });
    }, // End attach.
    findDomElements: function(sidebarStuff, sidebarCssId) {
      // sidebarStuff.sidebarCssId = sidebarCssId;
      sidebarStuff.sidebar = $($("#" + sidebarCssId));
      // sidebarStuff.sidebarContainer
      //     = $($(sidebarStuff.sidebar).find(".sidebar-container").get(0));
      // sidebarStuff.sidebarContainerContent
      //     = $($(sidebarStuff.sidebarContainer).find(".sidebar-container-content").get(0));
    },
    /**
     * Initialize the RSB on page display.
     *
     * @param sidebarStuff
     *   Data bundle for the sidebar.
     */
    initRSBPos: function(sidebarStuff) {
      // Hide all if not allowed to see the timeline.
      if (!drupalSettings.allowedTimeline) {
        $("#skilling-toggle-right-sidebar").hide();
        sidebarStuff.sidebar.addClass(drupalSettings.hidingClass);
        return;
      }
      // User is allowed to see timeline.
      if (!localStorage.getItem(sidebarStuff.localStorageId)) {
        localStorage.setItem(sidebarStuff.localStorageId, "yes");
      }
      sidebarStuff.showRight = localStorage.getItem(sidebarStuff.localStorageId);
      if (sidebarStuff.showRight === "yes") {
        // Make show the sidebar is showing.
        // Already is by default, no adjustment needed.
        // Set the arrow direction.
        $("#skilling-toggle-right-sidebar-arrow")
            .html(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
            .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
      }
      else {
        // Make sure the sidebar is not showing.
        $("#right-sidebar").hide();
        // Set the dimensions of the sidebar content container.
        // sidebarStuff.sidebarContainer
        //     .width(sidebarStuff.contentContainerWidth)
        //     .height(sidebarStuff.contentContainerHeight);
        // // Move the sidebar off the browser window.
        // sidebarStuff.sidebar.css("left", sidebarStuff.contentContainerWidth);
        // // Hide the sidebar.
        // sidebarStuff.sidebar.addClass(drupalSettings.hidingClass);
        // Set the arrow direction.
        $("#skilling-toggle-right-sidebar-arrow")
            .html(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
            .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
      }
    },
    /**
     * Initialize the LSB on page display.
     *
     * @param sidebarStuff
     *   Data bundle for the sidebar.
     */
    initLSBPos: function(sidebarStuff) {
      if (!localStorage.getItem(sidebarStuff.localStorageId)) {
        localStorage.setItem(sidebarStuff.localStorageId, "yes");
      }
      sidebarStuff.showLeft = localStorage.getItem(sidebarStuff.localStorageId);
      if (sidebarStuff.showLeft === "yes") {
        // Make show the sidebar is showing.
        // Already is by default, no adjustment needed.
        // Set the arrow direction.
        $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
            .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
      }
      else {
        // Make sure the sidebar is not showing.
        $("#left-sidebar").hide();
        // Set the dimensions of the sidebar content container.
        // sidebarStuff.sidebarContainer
        //     .width(sidebarStuff.contentContainerWidth)
        //     .height(sidebarStuff.contentContainerHeight);
        // // Hide the sidebar.
        // sidebarStuff.sidebar.addClass(drupalSettings.hidingClass);
        // // Move the sidebar off the browser window.
        // sidebarStuff.sidebar.css("left", -sidebarStuff.contentContainerWidth);
        // Set the arrow direction.
        $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
            .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
      }
    },
    /**
     * Prep sidebar and its data for a move.
     */
    prepSidebarMove: function(sidebarStuff) {
      // Get current dimensions of things.
      // sidebarStuff.sidebarContainerContent.show();
      // sidebarStuff.sidebar.removeClass(drupalSettings.hidingClass);
      // sidebarStuff.sidebarWidth = sidebarStuff.sidebar.width();
      // sidebarStuff.contentContainerWidth = sidebarStuff.sidebarContainerContent.width();
      // sidebarStuff.contentContainerHeight = sidebarStuff.sidebarContainerContent.height();
      // // Set dimensions of content container to match content,
      // // since the content is about to be hidden, and we want
      // // the visible box to stay the same size.
      // sidebarStuff.sidebarContainer
      //     .width(sidebarStuff.contentContainerWidth)
      //     .height(sidebarStuff.contentContainerHeight);
      // sidebarStuff.sidebarContainerContent.hide();
    },
    /**
     * Hide the right sidebar.
     */
    hideRightSidebar: function(sidebarStuff) {
      $("#right-sidebar").hide(Drupal.skillingSettings.sidebarHider.speed);
      // Set the arrow direction.
      $("#skilling-toggle-right-sidebar-arrow")
        .text(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
        .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
      // sidebarStuff.sidebarStopPos = sidebarStuff.sidebarWidth;
      // Drupal.behaviors.sidebarHider.hideRightSidebarStep(sidebarStuff);
    },
    /**
     * One animation step in hiding the right sidebar.
     * @param sidebarStuff
     *     Data summarising the sidebar.
     */
    // hideRightSidebarStep: function(sidebarStuff) {
    //   let sidebarLeftPos = parseInt(sidebarStuff.sidebar.css("left"));
    //   sidebarLeftPos += Drupal.skillingSettings.sidebarHider.stepDelta;
    //   sidebarStuff.sidebar.css("left", sidebarLeftPos);
    //   if (sidebarLeftPos < sidebarStuff.sidebarStopPos) {
    //     setTimeout(Drupal.behaviors.sidebarHider.hideRightSidebarStep, Drupal.skillingSettings.sidebarHider.stepTime, sidebarStuff);
    //   }
    //   else {
    //     sidebarStuff.sidebar.addClass(drupalSettings.hidingClass);
    //     // Set the arrow direction.
    //     $("#skilling-toggle-right-sidebar-arrow")
    //         .text(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
    //         .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
    //   }
    // },
    /**
     * Show the right sidebar.
     */
    showRightSidebar: function(sidebarStuff) {
      $("#right-sidebar").show(Drupal.skillingSettings.sidebarHider.speed);
      // Set the arrow direction.
      $("#skilling-toggle-right-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
          .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
      // sidebarStuff.sidebarStopPos = 0;
      // // Show the sidebar.
      // sidebarStuff.sidebar.removeClass(drupalSettings.hidingClass);
      // // Move it off-screen.
      // sidebarStuff.sidebar.css("left", sidebarStuff.sidebarWidth);
      // Drupal.behaviors.sidebarHider.showRightSidebarStep(sidebarStuff);
    },
    /**
     * One animation step in showing the right sidebar.
     * @param sidebarStuff
     *     Data summarising the sidebar.
     */
    // showRightSidebarStep: function(sidebarStuff) {
    //   let sidebarLeftPos = parseInt(sidebarStuff.sidebar.css("left"));
    //   sidebarLeftPos -= Drupal.skillingSettings.sidebarHider.stepDelta;
    //   sidebarStuff.sidebar.css("left", sidebarLeftPos);
    //   if (sidebarLeftPos > sidebarStuff.sidebarStopPos) {
    //     setTimeout(Drupal.behaviors.sidebarHider.showRightSidebarStep, Drupal.skillingSettings.sidebarHider.stepTime, sidebarStuff);
    //   }
    //   else {
    //     // Move the sidebar into final position, in case not exact.
    //     sidebarStuff.sidebar.css("left", sidebarStuff.sidebarStopPos);
    //     // Show the content.
    //     sidebarStuff.sidebarContainerContent.show();
    //     // Set the arrow direction.
    //     $("#skilling-toggle-right-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
    //         .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
    //   }
    // },
    /**
     * Hide the left sidebar.
     */
    hideLeftSidebar: function(sidebarStuff) {
      $("#left-sidebar").hide(Drupal.skillingSettings.sidebarHider.speed);
      // Set the arrow direction.
      $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
          .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
      // sidebarStuff.sidebarStopPos = -sidebarStuff.sidebarWidth;
      // Drupal.behaviors.sidebarHider.hideLeftSidebarStep(sidebarStuff);
    },
    /**
     * One animation step in hiding the left sidebar.
     * @param sidebarStuff
     *     Data summarising the sidebar.
     */
    // hideLeftSidebarStep: function(sidebarStuff) {
    //   let sidebarLeftPos = parseInt(sidebarStuff.sidebar.css("left"));
    //   sidebarLeftPos -= Drupal.skillingSettings.sidebarHider.stepDelta;
    //   sidebarStuff.sidebar.css("left", sidebarLeftPos);
    //   if (sidebarLeftPos > sidebarStuff.sidebarStopPos) {
    //     setTimeout(
    //         Drupal.behaviors.sidebarHider.hideLeftSidebarStep,
    //         Drupal.skillingSettings.sidebarHider.stepTime,
    //         sidebarStuff
    //     );
    //   }
    //   else {
    //     sidebarStuff.sidebar.addClass(drupalSettings.hidingClass);
    //     // Set the arrow direction.
    //     $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.rightArrowCharacter)
    //         .attr("title", Drupal.skillingSettings.sidebarHider.showSidebarTitle);
    //   }
    // },
    /**
     * Show the left sidebar.
     */
    showLeftSidebar: function(sidebarStuff) {
      $("#left-sidebar").show(Drupal.skillingSettings.sidebarHider.speed);
      // Set the arrow direction.
      $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
          .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
      // sidebarStuff.sidebarStopPos = 0;
      // // Show the sidebar. It will be off-screen.
      // sidebarStuff.sidebar.removeClass(drupalSettings.hidingClass);
      // // Move it off-screen
      // sidebarStuff.sidebar.css("left", -sidebarStuff.sidebarWidth);
      // Drupal.behaviors.sidebarHider.showLeftSidebarStep(sidebarStuff);
    },
    /**
     * One animation step in showing the left sidebar.
     * @param sidebarStuff
     *     Data summarising the sidebar.
     */
    // showLeftSidebarStep: function(sidebarStuff) {
    //   let sidebarLeftPos = parseInt(sidebarStuff.sidebar.css("left"));
    //   sidebarLeftPos += Drupal.skillingSettings.sidebarHider.stepDelta;
    //   sidebarStuff.sidebar.css("left", sidebarLeftPos);
    //   if (sidebarLeftPos < sidebarStuff.sidebarStopPos) {
    //     setTimeout(
    //         Drupal.behaviors.sidebarHider.showLeftSidebarStep,
    //         Drupal.skillingSettings.sidebarHider.stepTime,
    //         sidebarStuff
    //     );
    //   }
    //   else {
    //     // Move the sidebar into final position, in case not exact.
    //     sidebarStuff.sidebar.css("left", sidebarStuff.sidebarStopPos);
    //     // Show the content.
    //     sidebarStuff.sidebarContainerContent.show();
    //     // Set the arrow direction.
    //     $("#skilling-toggle-left-sidebar-arrow").text(Drupal.skillingSettings.sidebarHider.leftArrowCharacter)
    //         .attr("title", Drupal.skillingSettings.sidebarHider.hideSidebarTitle);
    //     ;
    //   }
    // }
  };
}(jQuery, Drupal));
