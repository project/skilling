(function (Drupal, $) {
  "use strict";
  Drupal.skilling = Drupal.skilling || {};
  // An array of window references, one for each exercise being shown in
  // a window.
  Drupal.skilling.submissionWindows
      = Drupal.skilling.submissionWindows || {};
  // Message to show students when a submission link
  // has become outdated.
  Drupal.skilling.submissionLinkChangedMessage
      = Drupal.skilling.submissionLinkChangedMessage || '';
  Drupal.skilling.setupExerciseLinks = false;
  Drupal.behaviors.exerciseLinks = {
    attach: function (context, settings) {
      if (! Drupal.skilling.setupExerciseLinks) {
        /**
         * User clicked on add submission link.
         */
        $(".skilling-add-submission-link").on("click", function () {
          let exerciseName = $(this).data("exercise-name");
          let exerciseNid = $(this).data("exercise-nid");
          let version = $(this).data("version");
          if (!exerciseName) {
            throw new Error("Skilling exerciseLinks: missing exercise name");
          }
          let windowName = Drupal.behaviors.exerciseLinks
              .computeWindowName("add", exerciseNid, 0);
          //Query param detected by hook_form_alter to set field values
          //from URL, hide fields, etc.
          let url = "/node/add/submission?"
            + settings.skillingFloatQueryParam
            + "&operation=add"
            + "&exercisename=" + exerciseName
            + "&exercisenid=" + exerciseNid
            + "&version=" + version;
          Drupal.behaviors.exerciseLinks.openSubmissionWindow(
              windowName, url
          );
          return false;
        });
        /**
         * User clicked on edit submission link.
         */
        $(".skilling-edit-submission-link").on("click", function () {
          let submissionNid = $(this).data("submission-id");
          if (!submissionNid) {
            throw new Error("Skilling exerciseLinks: missing submission id");
          }
          let windowName = Drupal.behaviors.exerciseLinks
              .computeWindowName("edit", 0, submissionNid);
          //Query param detected by hook_form_alter to set field values
          //from URL, hide fields, etc.
          let url = "/node/" + submissionNid + "/edit?"
              + settings.skillingFloatQueryParam
              + "&operation=edit"
              + "&submissionnid=" + submissionNid;
          Drupal.behaviors.exerciseLinks.openSubmissionWindow(
              windowName, url
          );
        });
        Drupal.skilling.setupExerciseLinks = true;
      }
    }, //End attach.
    //Compute the floating window's name.
    computeWindowName(operation, exerciseNid, submissionNid) {
      let windowName = "";
      if ( operation === "add" ) {
        windowName = "addSubmission" + exerciseNid;
      }
      else if ( operation === "edit" || operation === "delete" ) {
        //Delete uses the same floating window. Delete is link on that window.
        windowName = "editSubmission" + submissionNid;
      }
      else {
        throw new Error("computeWindowName: Unknown op: " + operation);
      }
      return windowName;
    },
    /**
     * Open a window with the given name and URL.
     * If there is already a window for that name, switch to it.
     * @param windowName Name of the window.
     * @param url URL to load into the window.
     */
    openSubmissionWindow(windowName, url) {
      //Is a ref to that browser window stored?
      if (Drupal.skilling.submissionWindows[windowName] === undefined
          || Drupal.skilling.submissionWindows[windowName].closed) {
        //There is no reference to that browser window.
        //Make a new one.
        Drupal.skilling.submissionWindows[windowName] = window.open(
            url,
            windowName,
            'height=800,width=800,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes,'
            + 'toolbar=no,personalbar=0'
        );
      }
      else {
        //There is already a window. Give it focus.
        Drupal.skilling.submissionWindows[windowName].focus();
      }
    },
    /**
     * Close the submission popup for the given exercise,
     * and adjust the submission links.
     *
     * @param operation
     * @param exerciseNid Exercise the popup is for. If the exercise nid is
     * zero, don't adjust the links.
     * @param submissionNid
     */
    submissionPopupClosed: function(
        operation, exerciseNid, submissionNid
    ) {
      //Update for submission links for add and deletes. Edit links
      //don't change.
      if ( operation === "add" || operation === "edit" || operation === "delete" ) {
        //Replace the submission link.
        let targetId = "skilling-exercise-links-" + exerciseNid.toString();
        $.when(this.getSubmissionChangedMessage())
            .then(function () {
              $("#" + targetId).html(
                Drupal.skilling.submissionLinkChangedMessage
              )
            });
      }
    },
    /**
     * Get the 'this submission has changed' message from the server.
     */
    getSubmissionChangedMessage() {
      let deferred = $.Deferred();
      //Has the message already been loaded?
      if ( Drupal.skilling.submissionLinkChangedMessage !== '' ) {
        //Already got it.
        deferred.resolve();
      }
      else {
        //Message has not been loaded.
        let destination = drupalSettings.path.baseUrl;
        //Make sure the base URL has a / at the end.
        if ( destination.slice(-1) !== "/" ) {
          destination += "/";
        }
        destination += "skilling/get_submission_changed_message";
        $.get(destination)
            .done(function(dataReturned) {
              if ( dataReturned.message === undefined
                   || ! dataReturned.message ) {
                throw new Error("No data in getting sub changed message.");
              }
              Drupal.skilling.submissionLinkChangedMessage
                  = dataReturned.message;
              deferred.resolve();
            })
            .fail(function(dataReturned) {
              throw new Error("Fail in getting sub changed message.");
            });
        return deferred.promise();
      }

    }
  };
})(Drupal, jQuery);

