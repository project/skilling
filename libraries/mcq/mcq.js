/*
 * MCQ processing.
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.setupMcqs = false;
  // Flag to show when saving, used to disable processing another click.
  Drupal.skillingSettings.savingMcqResponse = false;
  Drupal.behaviors.mcqProcessing = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.setupMcqs) {
        return;
      }
      Drupal.skillingSettings.setupMcqs = true;
      $(document).ready(function () {
        /**
         * User clicked on a response label.
         */
        $(".skilling-insert-mcq-response-active").click(function () {
          if (Drupal.skillingSettings.savingMcqResponse) {
            return;
          }
          Drupal.skillingSettings.savingMcqResponse = true;
          Drupal.behaviors.userChoseResponse(this);
          // Deactivate the click.
          $(this).unbind("click");
          //Make inactive.
          $(this)
              .removeClass("skilling-mcq-response-active")
              .addClass('skilling-insert-mcq-response-inactive');
          Drupal.skillingSettings.savingMcqResponse = false;
        });
      }); // End document ready.
      /**
       * User chose a response label.
       *
       * @param labelDomElement
       */
      Drupal.behaviors.userChoseResponse = function(labelDomElement) {
        // Find the response's container.
        let container = $(labelDomElement).closest(".skilling-insert-mcq-response-active");
        // Save the response, students only.
        // if (drupalSettings.currentUserRoles.includes('student') ) {
          // Get the response.
          let response = container.data("index");
          // Get the MCQ's internal name.
          let internalName = container.closest(".skilling-insert-mcq").attr("id");
          // Save the user's choice.
          Drupal.behaviors.saveMcqResponse(internalName, response, container);
        // }
      };
      /**
       * Save the user's choice.
       * @param internalName
       * @param response
       * @param responseDomContainer
       */
      Drupal.behaviors.saveMcqResponse = function(internalName, response, responseDomContainer) {
        let throbber = $(responseDomContainer)
            .closest(".skilling-insert-mcq").find(".skilling-mcq-throbber");
        $(throbber).show();
        // Make stored response 1 based.
        let storedResponse = response + 1;
        $.ajax({
          url: Drupal.url("skilling/check-mcq-response"),
          type: 'POST',
          dataType: 'json',
          data: {
            "csrfToken": drupalSettings.csrfToken,
            "sessionId": drupalSettings.sessionId,
            "internalName": internalName,
            "response": storedResponse
          },
          success: function(result) {
            $(throbber).hide();
            if (result.status.toLowerCase() === "error") {
              swal(result.errorMessage);
              return;
            }
            // Get the components passed by the server.
            let correct = result.correct;
            let responseMessage = result.responseMessage;
            // Show if response correct.
            let explanationDomContainer = $(responseDomContainer).find(".skilling-mcq-response-explanation");
            if (correct) {
              explanationDomContainer.addClass("skilling-mcq-response-explanation-correct");
            }
            else {
              explanationDomContainer.addClass("skilling-mcq-response-explanation-incorrect");
            }
            explanationDomContainer
                .html(responseMessage)
                .show("slow");

          },
          fail: (function (jqXHR, textStatus) {
            $(throbber).hide();
            let message = Drupal.t(
                "Skilling MCQ save fail @text. Please send a screen shot of your "
                + "entire browser window, including URL, to someone.",
                {'@text': textStatus}
            );
            swal(message);
          })
        });
      }
    }, //End attach.

  }
}(jQuery, Drupal));
