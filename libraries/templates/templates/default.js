/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
  // The name of sub folder which hold the shortcut preview images of the
  // templates.
  imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

  // The templates definitions.
  templates: [
    {
      title: 'Figure',
      image: 'figure.png',
      description: 'An image with a border.',
      html: '!(figure)()!'
    },
    {
      title: 'Drawing',
      image: 'drawing.png',
      description: 'Drawing, no border, caption.',
      html: 'p(figure-indent). !()!<br>' +
          '<br>' +
          'p(figure-indent small). ​<br><br>'
    },
    {
      title: 'Character',
      image: 'character.png',
      description: 'A face with emotion. <a href="/admin/skilling/characters-grid" target="_blank">List</a>',
      html: 'character.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>' +
          '&nbsp;&nbsp;<br>' +
          '<br>' +
          '/character.<br><br>'
    },
    {
      title: 'Code',
      image: 'code.png',
      description: 'Source code.',
      html: 'code.<br>' +
          '<br>' +
          '<br>' +
          '<br>' +
          '/code.<br>'
    },
    {
      title: 'Note',
      image: 'note.png',
      description: 'A note.',
      html: 'annotation.<br>\n' +
          '&nbsp;&nbsp;title=Custom title<br>' +
          '<br>' +
          '&nbsp;&nbsp;<br>' +
          '<br>' +
          '/annotation.<br>' +
          '<br>'
    },
    {
      title: 'Hint',
      image: 'hint.png',
      description: 'A hint.',
      html: 'annotation.<br>\n' +
          '&nbsp;&nbsp;type=hint<br>' +
          '&nbsp;&nbsp;title=Custom title<br>' +
          '<br>' +
          '&nbsp;&nbsp;<br>' +
          '<br>' +
          '/annotation.<br>' +
          '<br>'
    },
    {
      title: 'Pattern',
      image: 'pattern.png',
      description: 'Insert a pattern.',
      html: 'pattern.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>'
    },
    {
      title: 'Principle',
      image: 'principle.png',
      description: 'Insert a principle.',
      html: 'principle.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>'
    },
    {
      title: 'Fill-in-the-blank',
      image: 'fib.png',
      description: 'Insert a fill-in-the-blank.',
      html: 'fib.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>'
    },
    {
      title: 'Multiple choice',
      image: 'mcq.png',
      description: 'Insert a multiple choice.',
      html: 'mcq.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>'
    },
    {
      title: 'Exercise',
      image: 'exercise.png',
      description: 'A task for students.',
      html: 'exercise.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>'
    },
    {
      title: 'Reflect',
      image: 'reflect.png',
      description: 'Question for students.',
      html: 'reflect.<br>' +
          '&nbsp;&nbsp;internal_name=<br>' +
          '<br>' +
          '&nbsp;&nbsp;<br>' +
          '<br>' +
          '/reflect.<br><br>'
    },
    {
      title: 'Table',
      image: 'table.png',
      description: 'Rows and columns.',
      html: 'table(table).<br>' +
          '|_. head1 |_. head2 |_. head3 |<br>' +
          '| data | data | data |<br>' +
          '| data | data | data |<br><br>'
    },
    {
      title: 'Students log in',
      image: 'students-log-in.png',
      description: 'Message telling students they should log in.',
      html: 'annotation.<br>' +
          '&nbsp;&nbsp;condition=[current-user:is-anonymous]<br>' +
          '&nbsp;&nbsp;title=Log in<br>' +
          '<br>' +
          '&nbsp;&nbsp;If you\'re a student, please log in, so you can get the most from this page.<br>' +
          '<br>' +
          '/annotation.<br>' +
          '<br>'
    },
    // {
    // 	title: '',
    // 	image: '.png',
    // 	description: '',
    // 	html: ''
    // },
    // {
    // 	title: '',
    // 	image: '.png',
    // 	description: '',
    // 	html: ''
    // },
    // {
    // 	title: '',
    // 	image: '.png',
    // 	description: '',
    // 	html: ''
    // },
    // {
    // 	title: '',
    // 	image: '.png',
    // 	description: '',
    // 	html: ''
    // },
    // {
    // 	title: '',
    // 	image: '.png',
    // 	description: '',
    // 	html: ''
    // },
  ]
} );
