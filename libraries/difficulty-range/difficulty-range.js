/**
 * @file
 * Difficulty range widget.
 */

(function ($, Drupal) {
  "use strict";
  drupalSettings.runDifficultWidget = false;
  Drupal.behaviors.skillingRangeWidget = {
    attach: function (context, settings) {
      $(document).ready(function () {
        if (drupalSettings.runDifficultWidget) {
          return;
        }
        drupalSettings.runDifficultWidget = true;
        // Get the form operation - add or edit.
        let submissionForm = $(".node-submission-form");
        if (submissionForm.length === 0) {
          submissionForm = $(".node-submission-edit-form");
        }
        if (submissionForm.length === 0) {
          // Error report.
          swal("Form not found.");
          return;
        }
        let submissionFormAction = submissionForm.attr("action");
        let actionVars = {};
        let actionParts = submissionFormAction.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m,key,value) {
          actionVars[key] = value;
        });
        let operation = actionVars.operation;
        // Find the difficulty rating text field.
        let textWidgetContainer = $(".form-item-field-difficulty-0-value");
        let textWidget = $(textWidgetContainer).find("input");
        if (textWidget.length === 0) {
          // Error report.
          swal("Text widget for range widget not found.");
          return;
        }
        // Hide the text field.
        $(textWidgetContainer).hide();
        // Require value in text field before submitting form.
        submissionForm.submit(function (event) {
          if (textWidget.val() === "") {
            swal(Drupal.t("Please say how difficult the exercise was."));
            event.preventDefault();
          }
        });
        // Set up the rating range widget.
        // Minimum, in rating units.
        let minRating = parseFloat(textWidget.attr("min"));
        // Maximum, in rating units.
        let maxRating = parseFloat(textWidget.attr("max"));
        // Range, in rating units.
        let rangeRating = maxRating - minRating;
        // Number of arrow keystrokes for the entire range.
        const numKeySteps = 20;
        let rangeWidget = $(".skilling-difficulty-slider #rating-bar");
        let ratingMarker = rangeWidget.find("#rating-marker");
        // Rating change per keystroke step.
        let ratingPerKeyStep = rangeRating / numKeySteps;
        // Whether to show the initial state and position of the marker.
        let markerShown;
        // Position of the marker, in pixels.
        let markerPosPx;
        // Entire range, in pixels.
        let rangePx;
        if (operation === 'add') {
          markerShown = false;
        }
        else {
          markerShown = true;
        }
        const resize = function () {
          rangePx = rangeWidget.width();
          if (markerShown) {
            markerPosPx = (parseFloat(textWidget.val()) - minRating) / rangeRating * rangePx - ratingMarker.width() / 2;
            ratingMarker.css("left", markerPosPx).show();
          }
        };
        $(window).resize(resize);
        resize();
        $(rangeWidget).keydown(function (event) {
          // Get the key.
          event = event || window.event;
          let key = event.keyCode;
          // Prevent browser from jumping down on space.
          if (key === 32) {
            event.preventDefault();
          }
          if (!markerShown) {
            // This is the first keydown. Position marker in center.
            markerShown = true;
            textWidget.val(minRating + rangeRating / 2);
            markerPosPx = rangePx / 2 - ratingMarker.width() / 2;
            ratingMarker
                .css("left", markerPosPx)
                .show();
          }
          // Get current rating.
          let rating = parseFloat(textWidget.val());
          // Left key.
          if (key === 37) {
            rating -= ratingPerKeyStep;
            event.preventDefault();
            event.stopPropagation();
          }
          // Right key.
          else if (key === 39) {
            rating += ratingPerKeyStep;
            event.preventDefault();
            event.stopPropagation();
          }
          if (rating < minRating) {
            rating = minRating;
          }
          if (rating > maxRating) {
            rating = maxRating;
          }
          // Store the new rating in the return field.
          textWidget.val(rating);
          markerPosPx = (rating - minRating) / rangeRating * rangePx - ratingMarker.width() / 2;
          ratingMarker.css("left", markerPosPx);
        }); // End key down.
        $(rangeWidget).click(function (event) {
          let pxX = event.offsetX;
          if (pxX > rangePx) {
            pxX = rangePx;
          }
          markerShown = true;
          markerPosPx = pxX - ratingMarker.width() / 2;
          ratingMarker
              .css("left", markerPosPx)
              .show();
          let rating = minRating + pxX / rangePx * rangeRating;
          textWidget.val(rating);
        }); // End click.
      });
    }
  }
}(jQuery, Drupal));
