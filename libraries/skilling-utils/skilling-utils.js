/**
 * @file
 * Useful JS functions.
 */
/**
 * If loaded from grading interface, Drupal will not be defined.
 * Try to get it from the opener.
 */
if (typeof Drupal === "undefined") {
  Drupal = window.opener.Drupal;
}
(function (Drupal, $) {
  "use strict";
  Drupal.skillingSettings = Drupal.skillingSettings || {};
  //Some useful constants.
  // Drupal.skillingSettings.NO_COMPLETION_SCORE = -666;
  Drupal.behaviors.skillingUtilities = {
    /**
     * Strip HTML tags from a string.
     *
     * See https://stackoverflow.com/questions/822452/strip-html-from-text-javascript/47140708#47140708
     *
     * @param html
     *   The string to process.
     *
     * @returns string
     *   Result.
     */
    stripTags: function (html) {
      var doc = new DOMParser().parseFromString(html, 'text/html');
      return doc.body.textContent || "";
    },
    /**
     * Create a failure report.
     *
     * @param deets
     *   The details.
     *
     * @returns string
     *   The report text.
     */
    createFailReport: function (deets) {
      // Translated because regular people will see it.
      let report = Drupal.t('Well, shoot. Something bad happened.\n');
      if (deets.failFunction !== undefined) {
        report += "\nFailing function: " + deets.failFunction;
      }
      if (deets.status !== undefined) {
        report += "\nStatus: " + deets.status;
      }
      if (deets.statusText !== undefined) {
        report += "\nStatus text: " + deets.statusText;
      }
      if (typeof deets === "string") {
        report += deets;
      }
      let instructions = Drupal.t("Please take a screen shot " +
          "of this message box, and send it to someone.");
      report += "\n" + instructions;
      return report;
    },
    /**
     * Show a failure report to the user.
     *
     * @param report
     *   The report to show.
     */
    showFailReport: function (report) {
      swal("Oops", Drupal.behaviors.skillingUtilities.createFailReport(report),
          "error");
    },
    /**
     * Get the base URL of the site.
     *
     * @returns string Base URL, e.g., /.
     */
    getRootUrl: function () {
      let url = drupalSettings.path.baseUrl;
      // Make sure the base URL has a / at the end.
      if (url.slice(-1) !== "/") {
        url += "/";
      }
      return url;
    }
  }
})(Drupal, jQuery);
