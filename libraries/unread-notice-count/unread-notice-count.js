/**
 * @file
 * New notices display.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings.newNoticesDisplayInitialized = false;
  Drupal.behaviors.newNoticesDisplay = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.newNoticesDisplayInitialized ) {
        return;
      }
      Drupal.skillingSettings.newNoticesDisplayInitialized = true;
      let unreadNoticeCount = drupalSettings.countUnreadNotices;
      // Are there unread notices?
      if (unreadNoticeCount > 0) {
        // Yes. Add a badge to the Notices link.
        $("a.skilling-notices-link").each(function () {
          $(this)
            .append("<span class='badge badge-light'>" + unreadNoticeCount + "</span>")
            .attr("title", Drupal.t("You have unread notices"))
        });
      }
    }
  }
})(jQuery, Drupal);
