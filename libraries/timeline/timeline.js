/**
 * @file
 * Timeline display.
 */
(function ($, Drupal) {
  "use strict";
  // Submission status alternatives.
  const EXERCISE_SUBMISSION_NO_SUBMISSION = 'no submission';
  const EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK = 'waiting for feedback';
  const EXERCISE_SUBMISSION_COMPLETE = 'complete';
  const EXERCISE_SUBMISSION_NOT_COMPLETE = 'not complete';
  const EXERCISE_SUBMISSION_NA = 'na';
  const EXERCISE_SUBMISSION_MAX_SUBMISSIONS_REACHED = 'max reached';

  Drupal.skillingSettings.timelineDisplayInitialized = false;
  Drupal.behaviors.timelineDisplay = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.timelineDisplayInitialized ) {
        return;
      }
      Drupal.skillingSettings.timelineDisplayInitialized = true;
      $(document).ready(function() {
        // Where to show output.
        const $outputLocation = $($("#skilling-timeline-weeks-container"));
        const problemMessage = drupalSettings.problemMessage;
        if (problemMessage.trim().length > 0) {
          $outputLocation.html(`<p>${problemMessage}</p>`);
          return;
        }
        // Data to show.
        // console.log("weeks before json parse", drupalSettings.weeks);
        const weeks = JSON.parse(drupalSettings.weeks);
        // console.log("weeks after json parse", weeks);
        const challengeImageUrl = drupalSettings.challengeImageUrl;
        let output = `
          <div class="show-optional-container"> 
            <input 
              type="checkbox"
              id="show-optional" 
              title="Show optional exercises"
              onchange="Drupal.behaviors.timelineDisplay.changeShowOptional()"
            >
            <label for="show-optional" title="Show optional exercises">Show optional</label>
          </div>
          <ul class="skilling-timeline-weeks">`;
        window.weeks = weeks;
        for (const weekNumber in weeks) {
          const week = weeks[weekNumber];
          // console.log("weekNumber week", weekNumber, week);
          output += `<li class="skilling-timeline-week-container">
            <div class="skilling-timeline-week-number">Week ${weekNumber}</div>`;
          output += `<ul class="skilling-timeline-week">`;
          for (const dayNumber in week) {
            const day = week[dayNumber];
            const dayOfWeek = day.date_display.dow;
            const month = day.date_display.month;
            const displayDay = day.date_display.day;
            output += `<li class="skilling-timeline-day">`;
            output += `<div class="skilling-timeline-date-display">
                ${dayOfWeek} ${month} ${displayDay}`;
            if (day.date_display.today === 'yes') {
              output += `<span class="skilling-timeline-today">Today</span>`;
            }
            output += `</div>`;
            output += `<ul class="skilling-timeline-day-items">`;
            for (const event of day.events) {
              const url = drupalSettings.path.baseUrl + "node/" + event.exercise_nid;
              const submissionStatus = Drupal.behaviors.timelineDisplay.submissionStatus(
                event.exercise_submission_status,
                day.date_display.is_past
              );
              const isPastClass =
                  day.date_display.is_past && event.exercise_required
                      ? "is-past" : "";
              output += `<li class="skilling-timeline-exercise" data-required="${event.exercise_required}">`;
              output +=
                `<a title="${submissionStatus.titleAttr}"
                    class="${submissionStatus.cssClass} ${isPastClass}"
                    href="${url}">
                  <span class="skilling-timeline-marker">${submissionStatus.marker}</span>
                  ${event.title}
                </a>`;
              if (event.exercise_required) {
                output +=
                  `<span class="skilling-exercise-required" title="Required exercise">&reg;</span>`;
              }
              if (event.exercise_challenge === "1") {
                output +=
                  `<img src="${challengeImageUrl}"
                     title="Challenge exercise"
                     alt="Challenge exercise"
                     class="skilling-challenge-exercise"
                   >`;
              }
              output += `</li>`;

            }
            output += "</ul>";
            output += `</li>`;
          }
          output += "</ul>"; // End skilling-timeline-week
          output += "</li>"; // End skilling-timeline-week-container
        }
        output += "</ul>";
        $outputLocation.html(output);
        // What does the user want to see?
        let showOptional = localStorage.getItem("skillingShowOptionalExercises");
        // Was there any value?
        if (!showOptional) {
          showOptional = "true";
        }
        // Set initial value of checkbox.
        $("#show-optional").prop("checked", showOptional === "true");
        Drupal.behaviors.timelineDisplay.changeShowOptional();
      });
    },
    submissionStatus: function(statusType, isPast) {
      let title = "";
      let elementClass = "";
      let marker = "";
      switch(statusType) {
        case EXERCISE_SUBMISSION_NO_SUBMISSION:
          title = "No submission yet";
          elementClass = "skilling-exercise-no-submission";
          marker = isPast ? "!" : "?";
          break;
        case EXERCISE_SUBMISSION_WAITING_FOR_FEEDBACK:
          title = "Submitted, waiting for feedback";
          elementClass = "skilling-exercise-waiting-feedback";
          marker = "⌛";
          break;
        case EXERCISE_SUBMISSION_COMPLETE:
          title = "Complete";
          elementClass = "skilling-exercise-complete";
          marker = "✔";
          break;
        case EXERCISE_SUBMISSION_NOT_COMPLETE:
          title = "Submitted, not complete";
          elementClass = "skilling-exercise-not-complete";
          marker = "❌";
          break;
        case EXERCISE_SUBMISSION_NA:
          title = "";
          elementClass = "skilling-exercise-na";
          marker = "";
          break;
        case EXERCISE_SUBMISSION_MAX_SUBMISSIONS_REACHED:
          title = "No more submissions allowed";
          elementClass = "skilling-exercise-no-more-subs";
          marker = "🚫";
          break;
        default:
          title = "";
          elementClass = "";
          marker = "(Unknown status)";
      }
      const result = {
        cssClass: elementClass,
        titleAttr: title,
        marker: marker
      };
      return result;
    },
    changeShowOptional: function(){
      const checked = $("#show-optional").prop("checked");
      if (checked) {
        $(".skilling-timeline-exercise").show();
        $(".skilling-timeline-day").show();
        $(".skilling-timeline-week-container").show();
      }
      else {
        // Hide optional exercises.
        $(".skilling-timeline-exercise[data-required='false']").hide();
        // Hide days that have no visible exercises.
        $(".skilling-timeline-day").each(function (index, dayElement) {
          const numVisibleKids = $(dayElement)
              .find(".skilling-timeline-exercise:visible")
              .length;
          if (numVisibleKids === 0) {
            $(dayElement).hide();
          }
        });
        // Hide weeks that have no visible days.
        $(".skilling-timeline-week-container").each(function (index, weekElement) {
          const numVisibleKids = $(weekElement)
              .find(".skilling-timeline-exercise:visible")
              .length;
          if (numVisibleKids === 0) {
            $(weekElement).hide();
          }
        });
      }
      // Update localStorage
      localStorage.setItem(
        "skillingShowOptionalExercises",
        checked ? "true" : "false"
      );
    }
  }
})(jQuery, Drupal);
