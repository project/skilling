/**
 * @file
 * Calender builder.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings = Drupal.skillingSettings || {};
  Drupal.skillingSettings.calendarBuilderInitialized = false;
  Drupal.behaviors.calendarBuilder = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.calendarBuilderInitialized ) {
        return;
      }
      Drupal.skillingSettings.calendarBuilderInitialized = true;
      const EXERCISE_ID_KEY = "exercise-id";
      const DAY_KEY = "day";
      const REQUIRED_KEY = "required";
      const MAX_SUBS_KEY = "max-subs";
      // Grab data from server.
      let temp = $('input[name="exercises_due_hidden"]').val();
      let events = JSON.parse(temp);
      temp = $('input[name="exercises_hidden"]').val();
      let allExercises = JSON.parse(temp);
      // Add one to show nothing is selected.
      allExercises.unshift({
        exerciseId: 0,
        name: translateThisDude('(Select an exercise)')
      });
      // Make sure the ids are all integers.
      for (let index = 0; index < allExercises.length; index ++) {
        let item = allExercises[index];
        let intedItem = {
          exerciseId: parseInt(item.exerciseId),
          name: item.name,
          isRequired: item.isRequired,
          maxSubs: item.maxSubs
        };
        allExercises[index] = intedItem;
      }
      let $dateWidget = $("#edit-field-when-starts-0-value-date");
      let classStartDate = new Date($dateWidget.val());
      let $activeRow = null;
      $("#edit-field-notes-wrapper").before($(`
      <div class="form-item">
        <h4 class="label">Exercises due</h4> 
        <p>
          <button id="sort-by-day" type="button" class="btn btn-primary" title="Resort by day">Sort</button>
          <button id="add-first-event" type="button" class="btn btn-primary" title="Add a new event to the top of the table">Add first</button>
        </p>

        <div id="exercise-widget-container" class="skilling-hide-on-load">
          <select id="exercise-widget">
          </select>
        </div>

        <div id="exercise-required-widget-container" class="skilling-hide-on-load">
          <input type="checkbox" id="exercise-required-widget">
          <label for="exercise-required-widget">Required?</label>
        </div>

        <div id="day-widget-container" class="skilling-hide-on-load">
          <input type="text" id="day-widget">
        </div>

        <div id="max-subs-widget-container" class="skilling-hide-on-load">
          <input type="text" id="max-subs-widget">
        </div>

        <table id="skilling-calendar" class="table">
          <thead>
            <tr>
              <th title="Delete the row">Delete</th>
              <th title="Marks whether the same exercise appears more than once.">Duplicate?</th>
              <th>Exercise</th>
              <th title="Is the exercise required?">Required?</th>
              <th title="Maximum submissions">Max subs</th>
              <th title="Day (since the start of this class) the exercise is due.">Day</th>
              <th title="When the exercise is due.">Date</th>
              <th title="Add a new row">New</th>
            </tr>
          </thead>
          <tbody id="skilling-calendar-tbody">
          </tbody>
        </table>
      </div>
      `));
      // The SELECT
      const $exerciseWidget = $("#exercise-widget");
      const $exerciseWidgetContainer = $("#exercise-widget-container");
      // The day
      const $dayWidget = $("#day-widget");
      const $dayWidgetContainer = $("#day-widget-container");
      // Required
      const $requiredWidget = $("#exercise-required-widget");
      const $requiredWidgetContainer = $("#exercise-required-widget-container");
      // Max subs
      const $maxSubsWidget = $("#max-subs-widget");
      const $maxSubsWidgetContainer = $("#max-subs-widget-container");

      $(document).ready(function() {
        initialize();
        $dateWidget.change(function(event) {
          classStartDate = new Date($dateWidget.val());
          updateEventDisplayDates();
        });
        $dayWidget.on("click", function(event) {
          // console.log("day widget click");
          event.stopPropagation();
        });
        $dayWidget.on("change", function(event) {
          // console.log("day widget change");
          event.stopPropagation();
          if (isValidDayWidget()) {
            let day = parseInt($dayWidget.val());
            $activeRow.data("day", day);
            let displayDate = getDisplayDateForDay(day);
            $activeRow.find("td.display-date").text(displayDate);
          }
        });
        setupExerciseEvents();
        // Form submission.
        $("#node-class-edit-form").on("submit", function(event) {
          // Check that the active row is OK.
          if (!validateActiveRow()) {
            // event.preventDefault();
            // event.stopPropagation();
            // Scroll active row into view.
            scrollToElement($activeRow, 200);
            return false;
          }
          if (areDuplicates()) {
            // event.preventDefault();
            // event.stopPropagation();
            // Scroll top of table into view.
            scrollToElement($("#skilling-calendar"), 200);
            // Message to user.
            alert(
                translateThisDude(
                  "Sorry, duplicate entries for an exercise are not " +
                  "supported. If you want to repeat an exercise, make a copy " +
                  "of it, probably with a different name."
                )
            );
            return false;
          }
          // Call to store data from the active row.
          restoreRowDisplay($activeRow);
          // Prep data for return.
          let dueData = [];
          $("#skilling-calendar-tbody tr").each(function(index, row) {
            let exerciseId = parseInt($(row).data(EXERCISE_ID_KEY));
            let isRequired = $(row).data(REQUIRED_KEY) ? true : false;
            let day = parseInt($(row).data(DAY_KEY));
            let maxSubs = parseInt($(row).data(MAX_SUBS_KEY));
            dueData.push({
              exerciseId: exerciseId,
              day: parseInt(day),
              required: isRequired,
              maxSubs: parseInt(maxSubs)
            });
          });
          let dueDataString = JSON.stringify(dueData);
          $('input[name="exercises_due_hidden"]').val(dueDataString);
          // Submit.
          return true;
        });
        $("#skilling-calendar-tbody").on("click", function(event) {
          // console.log("tbody click");
          event.stopPropagation();
          let $targetElement = $(event.target);
          let clickedRow = $targetElement.closest("tr");
          // Try switching to a new row.
          if (!switchActiveRow(clickedRow)){
            return;
          }
          // let targetTagName = $targetElement.prop("tagName").toLowerCase();
          if ($targetElement.hasClass("delete-row")) {
            // console.log("delete-row");
            // User wants to delete a row.
            deleteActiveRow();
            return;
          }
          // Add a new row below the current one.
          if ($targetElement.hasClass("add-below")) {
            // console.log("add-below");
            // User wants to add a row.
            if (! validateActiveRow()) {
              return;
            }
            let newHtml = makeRowHtml(0, 0, false, 0);
            clickedRow.after(newHtml);
            // Click on the new row.
            clickedRow.next().click();
            return;
          }
        });
        $("#sort-by-day").on("click", function(){
          // Sort button clicked.
          sortByDay();
        });
        $("#add-first-event").on("click", function(){
          // console.log("add first clicked");
          // Add a new first event.
          if (! validateActiveRow()) {
            return;
          }
          let newHtml = makeRowHtml(0, 0, false, 0);
          $("#skilling-calendar-tbody").prepend(newHtml);
          // Click on the new row.
          $("#skilling-calendar-tbody tr").get(0).click();
        });
      });

      function initialize() {
        // $chooseExercise = $("#choose-exercise");
        $("#start-date").val(classStartDate.toISOString().substr(0, 10));
        // Add the exercises to the SELECT.
        for (let exercise of allExercises) {
          $exerciseWidget.append(new Option(exercise.name, exercise.exerciseId));
        }
        renderTable();
        sortByDay();
      }

      /**
       * Events on the exercise widget are lost somehow. Add them back.
       */
      function setupExerciseEvents() {
        $exerciseWidget.off("click");
        $exerciseWidget.on("click", function(event) {
          // console.log("exercise widget click");
          event.stopPropagation();
        });
        $exerciseWidget.off("change");
        $exerciseWidget.on("change", function(event) {
          // console.log("exercise widget change");
          event.stopPropagation();
          let exerciseId = $exerciseWidget.val();
          $activeRow.data(EXERCISE_ID_KEY, exerciseId);
          // console.log("exercise widget active row exer id: " + $activeRow.data(EXERCISE_ID_KEY));
          // Mark duplicated exercises.
          markDuplicatedExercises();
        });

      }

      /**
       * Render the event table.
       */
      function renderTable() {
        // Add rows for existing events to the table.
        let $options = $("select#choose-exercise option");
        let $tbody = $("#skilling-calendar-tbody")
        for (let event of events) {
          let exerciseId = event.exerciseId;
          $tbody.append(makeRowHtml(
              exerciseId,
              event.day,
              event.isRequired,
              event.maxSubs
          ));
        }
      }

      /**
       * Make HTML for a row, for exercise and day given.
       *
       * If exerciseId is 0, nothing is in the row.
       */
      function makeRowHtml(exerciseId, day, isRequired, maxSubs) {
        let exerciseName;
        let displayDate;
        const displayRequired = isRequired ? "Yes" : "No";
        // Find the exercise name.
        exerciseName = getNameOfExercise(exerciseId);
        if (exerciseName === "") {
          alert("BOK");
          return;
        }
        // Work out date for day.
        let displayDay = day;
        if (day === 0) {
          displayDay = "";
          displayDate = "";
        }
        else {
          displayDate = getDisplayDateForDay(day);
        }
        const displayMaxSubs = (maxSubs === null || maxSubs === 0 || maxSubs === "") ? "" : maxSubs;
        let result = `
        <tr 
            data-exercise-id="${exerciseId}" 
            data-day="${day}" 
            data-required="${isRequired}"
            data-max-subs="${maxSubs}"
        >
          <td><button type="button" class="btn btn-primary btn-sm delete-row" title="Delete this row">X</button></td>
          <td class="duplicate"></td>
          <td class="exercise-name">${exerciseName}</td>
          <td class="exercise-required">${displayRequired}</td>
          <td class="max-subs">${displayMaxSubs}</td>
          <td class="day">${displayDay}</td>
          <td class="display-date">${displayDate}</td>
          <td><button type="button" class="btn btn-primary btn-sm add-below" title="Add a new row below this one">+</button></td>
        </tr>
      `;
        return result;
      }

      /**
       * Get the date for a number of days after the
       * class start date.
       */
      function getDateForDay(day) {
        let dayDate = new Date(
            classStartDate.getFullYear(),
            classStartDate.getMonth(),
            classStartDate.getDate() + day);
        return dayDate;
      }

      /**
       * Make date display for a day.
       * @param day
       * @return {*}
       */
      function getDisplayDateForDay(day) {
        if (isNaN(day) || day === "" || day <= 0) {
          return "";
        }
        let dayDate = getDateForDay(day);
        let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' }
        let displayDate = new Intl.DateTimeFormat('en-US', options).format(dayDate);
        return displayDate;
      }

      function translateThisDude(inputText) {
        return inputText;
      }

      function switchActiveRow(newRow) {
        // console.log("switchActiveRow start");
        // Already active?
        if ($activeRow !== null) {
          if (newRow[0].rowIndex == $activeRow[0].rowIndex) {
            // console.log("switchActiveRow same row");
            return true;
          }
        }
        // There's a new active row.
        // Validate the data in the current one before switching.
        let rowDataOk = validateActiveRow();
        if (!rowDataOk) {
          return false;
        }
        // Restore the normal display of the old active row.
        restoreRowDisplay($activeRow);
        // Activate the new row.
        $activeRow = newRow;
        let rowExercise = $activeRow.data(EXERCISE_ID_KEY);
        let rowRequired = $activeRow.data(REQUIRED_KEY);
        let rowMaxSubs = $activeRow.data(MAX_SUBS_KEY);
        if (rowMaxSubs == 0) {
          rowMaxSubs = "";
        }
        let rowDay = $activeRow.data(DAY_KEY);
        // console.log("switchActiveRow switching exer: " + rowExercise + " day: " + rowDay);
        $activeRow.addClass("table-active");
        $activeRow.find("td.exercise-name").empty().append($exerciseWidgetContainer);
        $activeRow.find("td.exercise-required").empty().append($requiredWidgetContainer);
        $activeRow.find("td.max-subs").empty().append($maxSubsWidgetContainer);
        $activeRow.find("td.day").empty().append($dayWidgetContainer);
        // Set the values of the widgets.
        $exerciseWidget.val(rowExercise);
        $requiredWidget.prop("checked", rowRequired);
        $maxSubsWidget.val(rowMaxSubs);
        $dayWidget.val(rowDay);
        $exerciseWidgetContainer.show();
        setupExerciseEvents();
        $requiredWidgetContainer.show();
        $maxSubsWidgetContainer.show();
        $dayWidgetContainer.show();
        return true;
      }


      function validateActiveRow() {
        // If no active row, it's OK.
        if ($activeRow === null) {
          return true;
        }
        let errorMessages = translateThisDude("Some errors.") + "\n\n";
        let isDataOk = true;
        // Make sure an exercise is selected.
        if (parseInt($exerciseWidget.val()) === 0) {
          isDataOk = false;
          errorMessages +=
            translateThisDude("Please choose an exercise.")
            + "\n";
          $exerciseWidget.focus();
        }
        let errMess = isValidDayWidget();
        if (errMess !== null) {
          isDataOk = false;
          errorMessages += errMess + "\n";
        }
        errMess = isValidMaxSubs();
        if (errMess !== null) {
          isDataOk = false;
          errorMessages += errMess + "\n";
          $maxSubsWidget.focus();
        }
        if (!isDataOk) {
          alert(errorMessages);
        }
        return isDataOk;
      }

      function isValidMaxSubs() {
        // Check dinna apply if widget not visible.
        let errMess = null;
        if ($maxSubsWidget.is(":visible")) {
          let subs = $maxSubsWidget.val();
          subs = subs.trim();
          // MT is OK.
          if (subs !== "") {
            if (isNaN(subs)) {
              errMess = "Max submissions is not a number.";
            }
            else if (subs < 1) {
              errMess = "Max submissions must be 1 or more.";
            }
          }
        }
        return errMess;
      }

      function isValidDayWidget() {
        // Check dinna apply if widget not visible.
        let errMess = null;
        if ($dayWidget.is(":visible")) {
          let day = $dayWidget.val();
          day = day.trim();
          if (isNaN(day)) {
            errMess = "Day is not a number.";
          }
          else if (day <= 0) {
            errMess = "Day must be 1 or more.";
          }
        }
        return errMess;
      }

      function restoreRowDisplay($row) {
        // If no active row, nothing to do.
        if ($row === null) {
          return;
        }
        // Get the chosen values.
        let exerciseId = parseInt($exerciseWidget.val());
        let exerciseName = getNameOfExercise(exerciseId);
        let day = $dayWidget.val().trim();
        if (isNaN(day)) {
          alert("Arg! Jellyfish! Panic!");
          return;
        }
        day = parseInt(day);
        const isRequired = $requiredWidget.prop("checked");
        let maxSubs = $maxSubsWidget.val().trim();
        // MT is OK.
        if (maxSubs !== "") {
          if (isNaN(maxSubs)) {
            alert("Arg! Squids! Panic!");
            return;
          }
          maxSubs = parseInt(maxSubs);
        }
        $row.removeClass("table-active");
        // Move the widgets back to their default home.
        // $("#exercise-widget-container").empty().append($exerciseWidget);
        $exerciseWidgetContainer.hide();
        // $("#exercise-required-container").empty().append($requiredWidget);
        $requiredWidgetContainer.hide();
        // $("#exercise-required-container").empty().append($requiredWidget);
        $maxSubsWidgetContainer.hide();
        // $("#day-widget-container").empty().append($dayWidget);
        $dayWidgetContainer.hide();
        // Show the row's data.
        $row.find("td.exercise-name").html(exerciseName);
        $row.find("td.day").text(day);
        $row.find("td.exercise-required").text(isRequired ? "Yes" : "No");
        $row.find("td.max-subs").text(maxSubs);
        // Set rows data attrs and id.
        $row.data(EXERCISE_ID_KEY, exerciseId);
        $row.data(REQUIRED_KEY, isRequired);
        $row.data(MAX_SUBS_KEY, maxSubs);
        $row.data(DAY_KEY, day);
        // $row.attr("id", "row-" + exerciseId);
      }

      function getNameOfExercise(exerciseIdToFind) {
        exerciseIdToFind = parseInt(exerciseIdToFind);
        for (let exercise of allExercises) {
          if (exercise.exerciseId === exerciseIdToFind) {
            return exercise.name;
          }
        }
        return "Unknown";
      }

      function markDuplicatedExercises() {
        // Count the number of times each exercise is used in the table.
        let exerciseIdCounts = countEventIds();
        // Mark the exercises where there are duplicates.
        $("#skilling-calendar-tbody tr").each(function(index, row) {
          let exerciseId = $(row).data(EXERCISE_ID_KEY);
          let marker = exerciseIdCounts[exerciseId] > 1
              ? "X"
              : "";
          $(row).find(".duplicate").text(marker);
        });
      }

      /**
       * Find events that use the same exercises.
       *
       * @return Array, index is exercise id, value is count.
       */
      function countEventIds() {
        // Count the number of times each exercise is used in the table.
        let exerciseIdCounts = [];
        $("#skilling-calendar-tbody tr").each(function(index, row) {
          let exerciseId = $(row).data(EXERCISE_ID_KEY);
          if (!exerciseIdCounts[exerciseId]) {
            exerciseIdCounts[exerciseId] = 0;
          }
          exerciseIdCounts[exerciseId]++;
        });
        return exerciseIdCounts;
      }

      /**
       * Are there any duplicate exercises?
       *
       * @return {boolean}
       */
      function areDuplicates() {
        // Count the number of times each exercise is used in the table.
        let exerciseIdCounts = countEventIds();
        for (let exerciseIdCount of exerciseIdCounts) {
          if (exerciseIdCount > 1) {
            return true;
          }
        }
        return false;
      }

      function scrollToElement($element, verticalOffset) {
        if (!verticalOffset) {
          verticalOffset = 0;
        }
        let offset = $element.offset();
        offset.top -= verticalOffset;
        $('html, body').animate({
          scrollTop: offset.top,
          scrollLeft: offset.left
        });
      }

      function sortByDay() {
        // if ($dayWidget.is(":visible")) {
        if (!validateActiveRow()) {
          return;
        }
        // }
        // Reset the active row to normal display, no widgets.
        if ($activeRow !== null) {
          restoreRowDisplay($activeRow);
        }
        // Rebuild events array from data in displayed table.
        events = [];
        $("#skilling-calendar-tbody tr").each(function(index, row) {
          let exerciseId = parseInt($(row).data(EXERCISE_ID_KEY));
          let day = parseInt($(row).data(DAY_KEY));
          let isRequired = $(row).data(REQUIRED_KEY);
          let maxSubs = $(row).data(MAX_SUBS_KEY);
          if (maxSubs === "") {
            maxSubs = 0;
          }
          // console.log(day);
          // Skip MT rows.
          if (exerciseId > 0) {
            events.push({
              exerciseId: exerciseId,
              day: day,
              isRequired: isRequired,
              maxSubs: maxSubs
            });
          }
        });
        // Sort events.
        events.sort((a, b) => {
          if (a.day > b.day) {
            return 1;
          }
          if (a.day < b.day) {
            return -1;
          }
          return 0;
        });
        // Rerender table.
        $activeRow = null;
        $("#skilling-calendar-tbody tr").remove();
        renderTable();
        markDuplicatedExercises();
      }

      /**
       * Start date was changed.
       */
      function updateEventDisplayDates() {
        $("#skilling-calendar-tbody tr").each(function(index, row) {
          let day = parseInt($(row).data("day"));
          // Skip MT rows.
          let displayDate = (day > 0) ? getDisplayDateForDay(day) : "";
          $(row).find("td.display-date").text(displayDate);
        });
      }

      /**
       * Delete the active row.
       */
      function deleteActiveRow() {
        // User wants to the active row.
        let killRow = false;
        // Is is an MT row?
        if (parseInt($activeRow.data(EXERCISE_ID_KEY)) === 0 ) {
          // Don't confirm kill MT rows.
          killRow = true;
        }
        else {
          // Not an MT row. Ask the user.
          let exerciseToKill = $exerciseWidget.find("option:selected").text();
          let dayToKill = $dayWidget.val();
          let message = translateThisDude("Exercise: ")
              + exerciseToKill + translateThisDude("  Day: ") + dayToKill + "\n\n"
              + translateThisDude("Are you sure you went to delete this?");
          killRow = confirm(message);
        }
        if (killRow) {
          $activeRow.remove();
          $activeRow = null;
        }
      } // End deleteActiveRow.
    }
  }
})(jQuery, Drupal);
