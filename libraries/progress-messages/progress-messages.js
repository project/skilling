/*
 * Progress message template processing.
 */
(function($, Drupal) {
  "use strict";
  Drupal.skillingSettings.setupProgressMessageTemplates = false;
  Drupal.behaviors.progressMessageTemplatesProcessing = {
    attach: function (context, settings) {
      if (Drupal.skillingSettings.setupProgressMessageTemplates) {
        return;
      }
      Drupal.skillingSettings.setupProgressMessageTemplates = true;
      /**
       * When the user clicks on a message template, copy it to the message
       * text area.
       */
      $(document).ready(function () {
        $(".skilling-progress-message-template").click(function () {
          $("textarea#edit-message").val($(this).html());
        });
       });
    }
  }
}(jQuery, Drupal));
