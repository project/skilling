/**
 * @file
 * UI changes.
 */
(function ($, Drupal) {
  "use strict";
  Drupal.skillingSettings = Drupal.skillingSettings || {};
  Drupal.skillingSettings.uiMungeInitialized = false;
  Drupal.behaviors.uiMunge = {
    attach: function (context, settings) {
      if ( Drupal.skillingSettings.uiMungeInitialized ) {
        return;
      }
      Drupal.skillingSettings.uiMungeInitialized = true;
      $(document).ready(function () {
        if (drupalSettings) {
          if (drupalSettings.currentUserName) {
            let userName = drupalSettings.currentUserName;
            let logoutLink = $("li.skilling-logout-link a");
            if (logoutLink) {
              let title = logoutLink.attr("title");
              title += " (" + userName + ")"
              logoutLink.attr("title", title);
            }
          }
        }
      });
    }
  }
})(jQuery, Drupal);
