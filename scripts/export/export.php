<?php

/**
 * @file
 * Copy files from config export to Skilling config install.
 *
 * The names of the files to be copied are in file_list.txt.
 *
 * Run this program from the Drupal root, where index.php is.
 */

// Check that we are in the Drupal root dir.
$drupalRootPath = getcwd();
if (!is_dir($drupalRootPath . '/core') || !is_dir($drupalRootPath . '/modules')) {
  echo "This doesn't look like a Drupal root dir. Path: $drupalRootPath  Aborting.\n";
  exit(1);
}

// Work out paths to various things. Change these as needed.
// Path to Drupal Console executable.
$pathToDrupalConsoleExecutable = '~/subdomains/s1/vendor/bin';
$pathToModuleRoot = $drupalRootPath . '/modules/contrib/skilling';
//$pathToModuleRoot = $drupalRootPath . '/modules/contrib/skilling';
// Main module.
$skillingConfigInstall = $pathToModuleRoot . '/config/install';
$skillingConfigOptional = $pathToModuleRoot . '/config/optional';
// Entities module.
$skillingContentTypesConfigInstall = $pathToModuleRoot . '/skilling_content_types/config/install';
$skillingContentTypesConfigOptional = $pathToModuleRoot . '/skilling_content_types/config/optional';
// Users module.
$skillingUsersConfigInstall = $pathToModuleRoot . '/skilling_users/config/install';
$skillingUsersConfigOptional = $pathToModuleRoot . '/skilling_users/config/optional';
// History module.
$skillingHistoryConfigInstall = $pathToModuleRoot . '/skilling_history/config/install';
$skillingHistoryConfigOptional = $pathToModuleRoot . '/skilling_history/config/optional';

$pathToFileList = $pathToModuleRoot . '/scripts/export/file_list.txt';
// Where Drupal Console export will put its files.
// **** MESSED UP! NEXT IS STRANGE! ***
$relativePathToAllConfigFilesDir = 'web/modules/contrib/skilling/scripts/export/all_config_files';
$pathToAllConfigFilesDir = $pathToModuleRoot . '/scripts/export/all_config_files';

// Check the file listing files.
if (!file_exists($pathToFileList)) {
  echo "Couldn't find file_list.txt. Expected path: $pathToFileList  Aborting.\n";
  exit(1);
}

// Erase all of the files in the dir that Drupal Console
// will fill with config files.
erase_files_in_dir($pathToAllConfigFilesDir);

// Run the export command.
echo "Exporting config files with Drupal Console.\n";
$command = "$pathToDrupalConsoleExecutable/drupal ce --directory=$relativePathToAllConfigFilesDir --remove-uuid --remove-config-hash";
//$command = "$pathToDrupalConsoleExecutable/drupal ce --directory=$pathToAllConfigFilesDir --remove-uuid --remove-config-hash";
echo $command;
echo "\n";
//exit(0);
exec($command);
// Check that an expected file exists.
$fileNameToCheck = 'automated_cron.settings.yml';
if (!file_exists($pathToAllConfigFilesDir . '/' . $fileNameToCheck)) {
  echo "That's strange. $fileNameToCheck Isn't there. Aborting.\n";
  exit(1);
}

// Erase files in destinations.
erase_files_in_dir($skillingConfigInstall);
erase_files_in_dir($skillingConfigOptional);
erase_files_in_dir($skillingContentTypesConfigInstall);
erase_files_in_dir($skillingContentTypesConfigOptional);
erase_files_in_dir($skillingUsersConfigInstall);
erase_files_in_dir($skillingUsersConfigOptional);
erase_files_in_dir($skillingHistoryConfigInstall);
erase_files_in_dir($skillingHistoryConfigOptional);

// Read list of names of files to copy into array.
$fileNameListFile = fopen($pathToFileList, 'r');
if (!$fileNameListFile) {
  echo "Dude! Could not open list of file names at $fileNameListFile Aborting.\n";
  exit(1);
}
echo "Copying files...\n";
$fileNamesToCopy = [];
$allFileDataOk = TRUE;
while (!feof($fileNameListFile)) {
  $isThisFileOk = TRUE;
  $line = strtolower(trim(fgets($fileNameListFile)));
  // Skip blank lines.
  if (!$line || strlen($line) == 0) {
    continue;
  }
  // Skip comments.
  if ($line[0] === '#') {
    continue;
  }
  // Check the destination.
  // Should be a space.
  if ($line[-3] !== ' ') {
    echo "Expected space in $line\n";
    $isThisFileOk = FALSE;
    continue;
  }
  // Which module/destination?
  $lastTwoChars = substr($line, -2, 2);
  switch ($lastTwoChars) {
    // Skilling main module.
    case 'si':
      $destination = $skillingConfigInstall;
      break;

    case 'so':
      $destination = $skillingConfigOptional;
      break;

    // User module.
    case 'ui':
      $destination = $skillingUsersConfigInstall;
      break;

    case 'uo':
      $destination = $skillingUsersConfigOptional;
      break;

    // Content types module.
    case 'ci':
      $destination = $skillingContentTypesConfigInstall;
      break;

    case 'co':
      $destination = $skillingContentTypesConfigOptional;
      break;

    // History module.
    case 'hi':
      $destination = $skillingHistoryConfigInstall;
      break;

    case 'ho':
      $destination = $skillingHistoryConfigOptional;
      break;

    default:
      echo "Bad destination in $line\n";
      $isThisFileOk = FALSE;
  };
  if ($isThisFileOk) {
    // Strip last three characters.
    $line = substr($line, 0, -3);
    // Check that the named file exists.
    if (!file_exists($pathToAllConfigFilesDir . '/' . $line)) {
      echo "Could not find file: $line\n";
      $isThisFileOk = FALSE;
      continue;
    }
    $fileNamesToCopy[] = [
      'filename' => $line,
      'destination' => $destination,
    ];
  }
  if (!$isThisFileOk) {
    $allFileDataOk = FALSE;
  }
}
fclose($fileNameListFile);

if (!$allFileDataOk) {
  echo "Stopped, not complete.\n";
  exit(1);
}

// Copy the files.
$fileCount = 0;
foreach ($fileNamesToCopy as $fileInfo) {
  $fileName = $fileInfo['filename'];
  $destination = $fileInfo['destination'];
  $result = copy(
    $pathToAllConfigFilesDir . '/' . $fileName,
    $destination . '/' . $fileName
  );
  if ($result === FALSE) {
    echo "Something went wrong copying $fileName\n";
  }
  else {
    // OK.
    // Check file system spec for fields that need private files.
    $hiddenAttachments = ($fileName === 'field.storage.node.field_hidden_attachments.yml');
    $submittedFiles = ($fileName === 'field.storage.node.field_submitted_files.yml');
    if ($hiddenAttachments || $submittedFiles) {
      $filePath = $destination . '/' . $fileName;
      // Check the uri_scheme.
      $fileContents = file_get_contents($filePath);
      $fileContents = str_replace(
        'uri_scheme: public',
        'uri_scheme: private',
        $fileContents);
      $result = file_put_contents($filePath, $fileContents);
      if ($result === FALSE) {
        echo "Problem writing $fileName in privatge URI processing.\n";
      }
      else {
        echo "Checked for private URI in $fileName.\n";
      }
    }
    // Check for views_merge_rows problem.
    $filePath = $destination . '/' . $fileName;
    $fileContents = file_get_contents($filePath);
    $toFind = "display_extenders:
        views_merge_rows: {  }";
    $replaceWith = "display_extenders: {  }";
    if (strpos($fileContents, $toFind) !== FALSE) {
      echo "Found views_merge_rows in $fileName\n";
      $fileContents = str_replace($toFind, $replaceWith, $fileContents);
      file_put_contents($filePath, $fileContents);
    }

  }

  $fileCount++;
}
echo "Processed $fileCount files.\n";

// Erase the files that Drupal exported.
echo "Ready to erase config files exported from Drupal Console.\n";
erase_files_in_dir($pathToAllConfigFilesDir);

// We are outta here!
echo "All done.\n";

/**
 * Show a message. Wait for a response. Exit program if user doesn't type y.
 *
 * @param string $message
 *   Message to show the user.
 */
function confirm($message) {
  echo $message;
  $handle = fopen("php://stdin", "r");
  $line = fgets($handle);
  fclose($handle);
  $firstChar = strtolower(substr(trim($line), 0, 1));
  if ($firstChar != 'y') {
    echo "Aborting.\n";
    exit;
  }
}

/**
 * Erase the files in a directory.
 *
 * @param string $dir
 *   Directory path.
 */
function erase_files_in_dir($dir) {
  // Erase files in a directory..
  // Make the dir, if it does not exist.
  if (!is_dir($dir)) {
    mkdir($dir);
  }
  // Confirm from user to erase all files in the dir.
//  confirm(
//    "All files in $dir will be erased (though not subdirectories). Are you sure (Y/N)?"
//  );
//  echo "Erasing files...\n";
  // Get all file names.
  $files = glob($dir . '/*.yml');
  $fileCount = 0;
  foreach ($files as $fileName) {
    if (is_file($fileName)) {
      unlink($fileName);
      $fileCount++;
    }
  }
//  echo "Erased $fileCount files.\n";
}
