<?php

/**
 * @file
 * Function here so it's easier to call from submodule's installation logic.
 */

use Drupal\skilling\Help\HelpConstants;
use Symfony\Component\HttpFoundation\Request;

/**
 * Check whether trusted hosts is set up.
 *
 * Split into separate file, since it is used during installation of
 * different Skilling modules.
 *
 * @param array $requirements
 *   Installation requirements array.
 *
 * @return array
 *   Requirements array, with new requirement added if needed.
 */
function skilling_trusted_hosts_set_up(array &$requirements) {
  // Are there trusted hosts?
  $trustedHosts = Request::getTrustedHosts();
  if (count($trustedHosts) === 0) {
    $requirements['skilling_trusted_hosts_not_set'] = [
      'title' => t('Skilling: Trusted hosts settings'),
      'value' => t('Trusted hosts has not been set.'),
      'description' => t(
        'Trusted hosts must be set. See the <a href=":url" target="_blank">documentation</a>.',
        [':url' => HelpConstants::USER_DOC_BASE . '/' . HelpConstants::HELP_TRUSTED_HOST]
      ),
      'severity' => REQUIREMENT_WARNING,
    ];
  } // End there are no trusted hosts.
  return $requirements;
}
