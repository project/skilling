<?php
namespace Drupal\skilling_xapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Gets a list of lessons, responding to JSON call.
 *
 * For xAPI analysis programs.
 *
 * @package Drupal\skilling_xapi\Controller
 */
class GetLessonListController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Send a list of lesson ids and titles.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLessonList() {
    // Find ids of lessons. Whether published, or not.
    $lessonIds = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', SkillingConstants::LESSON_CONTENT_TYPE)
      ->sort('title')
      ->execute();
    $lessons = $this->entityTypeManager->getStorage('node')
      ->loadMultiple($lessonIds);
    $result = [];
    /** @var \Drupal\node\Entity\Node $lesson */
    foreach ($lessons as $lesson) {
      $result[$lesson->id()] = $lesson->getTitle();
    }
    return new JsonResponse($result);
  }


}
