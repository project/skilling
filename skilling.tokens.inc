<?php

use \Drupal\skilling\SkillingTokens;

/**
 * @file
 * Contains Skilling tokens code.
 */

/**
 * Implements hook_token_info().
 */
function skilling_token_info() {
  /** @var \Drupal\skilling\SkillingTokens $skillingTokenService */
  $skillingTokenService = \Drupal::service('skilling.tokens');
  return $skillingTokenService->tokenInfo();
}

/**
 * Implements hook_token_info_alter().
 */
function skilling_token_info_alter(&$data) {
  /** @var \Drupal\skilling\SkillingTokens $skillingTokenService */
  $skillingTokenService = \Drupal::service('skilling.tokens');
  $skillingTokenService->tokenInfoAlter($data);
}

/**
 * Implements hook_tokens().
 */
function skilling_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  /** @var \Drupal\skilling\SkillingTokens $skillingTokenService */
  $skillingTokenService = \Drupal::service('skilling.tokens');
  $replacements = $skillingTokenService->tokens($type, $tokens, $data, $options, $bubbleable_metadata);
  return $replacements;
}
