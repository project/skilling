OUT OF DATE

This file lists the specifications for history items. It
covers:

* Principles
* Uses of history data
* History nodes
* History data exports

Only Skilling's history system is covered. Other logging systems,
like the web server's logs, are outside the scope of this
document.


Identifying information
===

The term "identifying information" refers to data that can be
used to associate a data object with a particular person.
That means the user data:

* Last name
* First name
* User name
* Email address
* About statement
* Picture

User names are included on the list, because sometimes they 
are based on identifying information. For example, an 
institution might use the first part of an email address
as a user name.

Each user has a user id (uid), a number like 3141. That number bears
no relation to anything outside a particular Skilling website.

If a person has accounts on two or more Skilling websites, they
will most likely have different uids on each site. There is no
relationship between uids on different Skilling websites. Researchers
who want to connect data across sites will have to make
their own arrangements.

Uids are included in data exports, as they are not considered
identifying information. That being said, uids are still
restricted in Skilling. For example, students cannot learn
the uids of other students. They would even find it difficult
to learn their own uids, although it is theoretically possible.


Principles
==========

Store necessary data only
-------------------

Only data that is relevant to creating and running good skill 
courses should be stored. Data that is not necessary for the
anticipated uses of the data is not stored by 
Skilling's history system.

* Only data about students is stored.
* Data about IP addresses, browsers, and other technical details
of user interaction is not of clear pedagogical value. It is
not stored. 

Privacy and use context
-------

To be useful, history data must contain identifying information. 
However, identifying data is not needed for all data
usage contexts. For example, instructors need data about student
identity to do their jobs. Authors do not.

No choices, except for administrators
---------

Users cannot choose which history records or fields they see,
beyond what Skilling offers in its standard reports.
Some users can apply filters (e.g., on date ranges) to 
limit the data they see, but they cannot expand data sets
beyond what Skilling offers.

The only users who can see more data than on standard reports
are administrators. They can, for example,
use Drupal's Views module to create their own reports, including
data that Skilling's reports do not. It is up to 
administrators and their organizations to implement appropriate
policies.

Only administrators can export data
------

Some Skilling reports let users export data in XML for
analysis. However, only administrators can export data in this
way. If authors or others want exports, they should ask
administrators, who can apply appropriate policies when
deciding whether to grant the requests.

As noted below, Skilling's exports do not include identifying
information, even if they are triggered from reports
that do.


Uses of history data
===

Instructor review of student behavior
------






History data
===

There is a custom history entity type. All history records are 
of this type. History entities all have the same base fields:

* Name
* User id (also the entity owner)
* When the event occurred
* Notes

History records can be edited by administrators to, e.g., 
enter notes, but that will probably be rare.

Each event category (like log in, view lesson, answer
MCQ) is a history entity type bundle (a subtype). Bundles can add extra
fields to history records. For example, the MCQ bundle adds
the MCQ item that was answered, the user's response, and whether
the response was correct. 

Here are the fields added by the bundles.


* Log in
* Log out

* Exercise submission
* Feedback accessed (can do this?)

* FiB response
* MCQ response
* Reflect note response

* View lesson
* More clicked
* Show all clicked
* Viewing lesson (1/min)

* View history
* View your subm
* View your account
* Update your account
* View notices



* Message from instructor to student
  * instructorUid
  * studentUid
  * message
* Message from student to instructor 
  * instructorUid
  * studentUid
  * message
* Search
  * text
* Submit exercise solution
  * studentUid
  * exerciseNid
  * solution: submitted text
  * files: array of file ids
  * difficultyRating
  * difficultyReason
* Submission graded
  * studentUid
  * exerciseNid
  * graderUid
  * complete
  * overall
  * responses
  
  
??? DO THIS???  
  
Reflect notes 

What is saved depends on Skilling configuration. 

If "Save all reflect note events" is off (the default), then an event it created
when the student clicks the ?? button. The student
may change the note after that. The current state of the reflect notes is in its
reflect note node.

If "Save all reflect note events" is on, then an event it created
each time the server updates a reflect note, that is, when the autosave for reflect
notes runs. This might generate a lot of data.



