
$container->get('link_generator');
$url = Url::fromRoute('hello.dog', ['name'=>'Rosie']);
$link = $this->linkGenerator->generate('Hello', $url);

/**
 * HelloController constructor.
 *
 * @param \Drupal\hello\Quote $qs
 * @param \Drupal\hello\SkillCourseParser $ps
 * @internal param \Drupal\hello\Quote $quoteService
 */
public function __construct(Quote $qs, SkillCourseParser $ps) {
  $this->quoteService = $qs;
  $this->parserService = $ps;
}

public static function create(ContainerInterface $container) {
  return new static(
    $container->get('hello.quote'),
    $container->get('hello.skillcourseparser')
  );
}


public function helloRosie() {
  return $this->redirect('hello.dog', ['name' => 'Rrosie']);
  //return new RedirectResponse('/dog/Rosie');
}

// Is a module enabled?
$moduleHandler = \Drupal::service('module_handler');
if ($moduleHandler->moduleExists('devel')){


WebDriverTestBase stuff

getSession returns a Min session.
assertSession returns a WebAssert

$this->getSession()->getPage()->getHTML();

$page = $this->getSession()->getPage();
$button = $page->findButton('Save');
$field = $page->findField('field_test[0][value]');
$link = $page->findLink('Link text');
$element = $page->find('css', 'css selector');

$this->assertNotEmpty($field);


