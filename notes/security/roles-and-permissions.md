This document explains Skilling's role and permission system. It affects
data security, and also the privacy of student data.


# Definitions

## Entities, nodes, and fields

In Drupal, an entity is something about which data is stored. Think of it
as a row in a database table. (Nodes are not implemented as single
database rows, however.)

There are various entity types. The most common are nodes and users. Nodes
usually represent units of content. A lesson is a node, for example.

Each node is an instance of a node type, usually called a content type.
Content types in Skilling include lessons, patterns, and exercises.

Nodes have fields. Different content types can have different fields,
depending on what the content type is used for. For example,
the lesson content type has a field for attached files. The class content
type has a field for start date.

For data security and privacy, we need to protect data at both:

* The entity level
* The field level

Entity-level controls block access to an entire entity. For example,
submission is a content type. Each instance is a one student's
solution to one exercise. The student who created the submission
should have access to that entity. Instructors and graders should have
access, but only for classes they have special roles for (more below).

Even though a user might have access to an entity, s/he might not have
access to all of the fields of that entity. Consider submissions again.
Instructors can know the identity of the student who created a
submission. However, graders do not have access to student identity.



## Roles and permissions

The Drupal core permission system lets administrators define node
creation, editing, and deletion at the content type (node type)
level. For instance, authors can create lessons, but students cannot.

However, node viewing is controlled only by a single permission
for all content types. This is not sufficient for Skilling.

Skilling supplements the standard system with
extra checks for sensitive data, especially data about:

* Exercise submissions
* Enrollments
* User accounts
* User history

The following six roles are defined:

* Author. Creates course material, like lessons, exercises, and rubric items.
* Reviewer. Reviews course material for, e.g., assessment.
* Student. Taking one or more classes.
* Grader. Assesses student submissions for one or more classes.
* Instructor. Runs one or more classes.
* Administrator. System manager.

In Drupal, user 1, also called the superuser or root, is an administrator
with no restrictions.

There is another concept called "class-specific roles." These are not part of Drupal.
They are specific to Skilling.

Class-specific roles are defined by enrollment records. Each record has references
to a user and a class, and a list of class-specific.
The possible roles are:

* Enrolled student
* Enrolled grader
* Enrolled instructor

These roles are specific to a user/class combination. For example, user Sally might be a
grader in class C1, and a student in class C2.

(Skilling does not allow a user to have the enrolled student role and
either of the other roles in the same class.)

Some permissions use only normal Drupal rules. For example, authors can
create lessons, but students cannot. Other permissions require class-specific
roles. For example, Sally can see all students' submissions for C1, but
only her own for C2.

To review, we have:

* Entity types (e.g., users, and nodes)
* Content types, or node types (e.g., lessons, and submissions)
* Fields, with each entity and content type able to have different fields
* Roles, like author, and instructor. Roles are used for most access checks.
* Class-specific roles, that prevent users from seeing data about
  classes they are not enrolled in.
* Controls apply at the entity and field levels.



# Limiting conditions

When designing Skilling, some decisions were made that affect
data access. This section examines three issues.


## Social information

Skilling concentrates on cognitive aspects of skill learning. Skilling
does not directly support group work, discussion, or other social
interaction, though social norms can be influenced in lessons,
by, for example, characters in worked examples.

Social interaction is important in learning, of course. The ideal use case
for Skilling is a flipped class. Face-to-face time is reserved
for exercise troubleshooting, group work, and motivation. High-touch
classes built on human relationships may be the most effective
classes of all.

Why not have social features? There are two reasons. First,
the lack of social features makes it easier to maintain strict privacy
standards. This is important for FERPA compliance, and is a good idea in any case.

Second, Drupal already has modules for forums, chats, and other social
interaction. In fact, a forum module is part of Drupal core. While Skilling
does not require those modules, it does not disallow them, either.
Organizations are free to use whatever suits their purposes.

(Skilling.us)[https:/skilling.us] discusses Skilling's (lack of) social features more completely.


## Limits on graders

Skilling allows for the
possibility that graders are former students, as little
as one year ahead of current students. To protect privacy and reduce bias,
it's safer for students not the know who their graders are, and for
graders not to know whose work they are evaluating.

Of course, there should be a means for a student to appeal a grader's evaluation. That appeal
should be directed to the student's instructor. There is a student-to-instructor
contact form in Skilling. 


## Limits on data sharing

A vanilla Skilling installation is a "walled garden."
It does not pull data from Twitter, or other social media.
It does not let users login with their Facebook accounts. It does
not send data to Google Analytics.

Vanilla Skilling does not exchange data with LMS like Moodle.
It does not connect to administrative systems. This means that,
for example, student accounts must be created manually.

This can all be changed. In fact, Drupal is quite good at interacting
with other systems. For example,
it can talk to LDAP and OAuth servers, to support campus-wide single sign-on.
Drupal can import data from student information systems.

The possibilities for useful system integration are endless.
Skilling doesn't block such connections. However, it is up to
each organization to decide what it wants to do.

(Skilling.us)[https:/skilling.us] has more on system integration.


# Entity restrictions by role

Entity access restrictions apply to all of the ways
data can be accessed through Drupal:

* Accessing an entity through a browser link, or by directly typing a URL.
* Accessing a data object via an Ajax call.
* The data object being referenced in a search result.
* The data object appearing in a list.

The critical objects (from a privacy viewpoint) are:

* Exercise submissions
* Enrollments
* User accounts
* User history

Let's call them CPOs, critical privacy objects. (C3POs would be
better, but it's hard to work in.)

Here are data object restrictions for the six roles.

* Author
  * Does not need access to CPOs for current classes.
  * To improve their work, authors have a legitimate need for submission
    and history data for past courses. Requests for anonymized
    versions of this data should be sent to an administrator.
* Reviewer
  * Does not need access to CPOs.
  * Requests for summary data should be sent to authors and/or
    administrators.
* Student
  * Can access data on his/her own CPOs.
  * Cannot access data about any other student's CPOs.
  * Does not know the identity of other users, with one exception given
    below. Cannot contact other users through the system (e.g., with a
    contact form), again with one exception.
  * Cannot tell through Skilling how many other students there are,
    or even if there are any other students.
  * Knows the identity of his/her instructor(s). Can contact instructor(s)
    through a contact form Skilling provides. The form does not reveal
    the instructor(s) contact information.
  * Does not know the identity of his/her graders. Cannot contact them
    through the system.
* Grader
  * Can access submissions waiting for him/her to grade. Data identifying
    students is stripped.
  * Can access data on submissions that s/he has evaluated in the past.
    Again, student data is stripped.
  * Can access a list of exercises, including their rubric items.
* Instructor
  * Can access CPOs, but only for his/her own classes. "Own classes"
    are defined through enrollment records, as classes for
    which the user has the instructor role.
* Administrator
  * Has access to CPOs.


h2. Additional content type restrictions

Design pages: all roles have access, except anons and students. Their
access depends on a configuration option set by an administrator.

Rubric items and rubric item responses: all roles have access, except
anons and students.

Calendars, events, and calendar events: authors, reviewers and instructors
can see all. Graders and students can see them for classes they are
enrolled in.


h1. Controlling entity viewing

Recall that the standard Drupal submission system does not control
node viewing well enough for Skilling. Here are some extra restrictions.


h2. Entity-level retrictions

For enrollment and history nodes:
    - if current user owns the node being viewed, OK.
    - if the node is owned by a user who is a student in a class
      the current user is an instructor for, OK

Note that graders do not have access.

For submission nodes:
    - if the current user owns the node being viewed, OK.
    - if the node is owned by a user who is a student in a class
      the current user is an instructor or grader for, OK
    - if the current user is a grader who has already graded the submission, OK,

As noted about, student identity data is stripped for graders.

For user account entities:
    - if your own account, ok
    - if user who is a student in a class the current user is an
      instructor for, OK




h1. Events and effects on roles

When something that could affect a user's class roles changes, then
recomputeClassRoles is called. Here are events
that invoke recomputeClassRoles(SkillingUser user).

Enrollment node
    - add - call recomputeClassRoles, pass the new enrollment node.
    - update - call recomputeClassRoles, pass the old and new enrollment nodes.
      Note: update of any field, like published status, causes recomputation.
    - delete - call recomputeClassRoles, pass record being deleted.

User deleted
    - delete all enrollment nodes for the user
    - recomputeClassRoles for the user

User blocked/unblocked
    - recomputeClassRoles for the user

Class deleted
    - load enrollments for class
    - extract uids
    - delete all enrollment nodes for class
    - recompute class roles for uids

Class published/unpublished
    - load enrollments for class
    - extract uids
    - recompute class roles for uids



h1. Stopping node access

Two hooks work together in :

- skilling_node_access - controls access to nodes based on roles
and class roles

- hook_entity_field_access - limits access to individual fields
within content types.
