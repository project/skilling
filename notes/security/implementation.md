This document explains how Skilling's security and privacy policies implemented.
Read `roles-and-permissions.md` to learn what those policies are.

Skilling has special requirements that are not satisfied by Drupal's standard
roles/permissions system. For example, a user with the Instructor role should not
necessarily have access to the submissions of a user with the Student role. Access
depends on whether the student is in one of the instructor's classes. So access
depends on enrollment records.

There are also field-level restrictions. For example, students should not have access
to field_hidden_attachments field of the lessons content type. Administrators, authors,
reviewers, instructors, and graders should have access.

# Centralized Access Checking

All access checks are sent through the SkillingAccessChecker service, 
in `src/Access/SkillingAccessChecker.php`. 

## Code auditing

Through-the-system auditing is improved if security code is open to manual
inspection. This is in addition to automated tests. Making the code easy to 
follow helps auditors be more thorough in their evaluation of system security. 

What does this requirement mean in practice? Someone not familiar with Drupal's 
architecture should be able to look at the code, and check the access 
given to each entity and field. 

Much of the checking is hard-coded. This is not the Drupal Way. Usually, permissions 
are configurable by administrators. Drupal plugins and services read configuration
to determine permissions. Someone not trained in Drupal internals would be 
unable to draw inferences about security by inspecting the code in those plugins 
and services. 

`SkillingAccessChecker` has simple, well-documented code, that is easy to follow.
Consider lesson objects, for example. They have fields like:

* Body: lesson content.
* Tags: keywords.
* Attachments: files students can download.
* Notes: authors' notes about the lesson.
* Hidden attachments: restricted files for privileged users

The following code from `SkillingAccessChecker` limits viewing of each field:

```// Deny access by default.
   $allow = FALSE;
   ...
   if ($operation === SkillingConstants::VIEW_OPERATION) {
     switch ($fieldName) {
       // Viewing a lesson.
       case SkillingConstants::FIELD_BODY:
       case SkillingConstants::FIELD_TAGS:
       case SkillingConstants::FIELD_ATTACHMENTS:
         // Everyone can see body, tags, and attachments fields of
         // the lesson content type.
         $allow = TRUE;
         break;
   
       case SkillingConstants::FIELD_NOTES:
       case SkillingConstants::FIELD_HIDDEN_ATTACHMENTS:
         // Admins, authors, reviewers, instructors, and graders
         // can see notes and hidden attachments fields
         // of the lesson content type.
         $allow = $admin || $author || $reviewer || $instructor || $grader;
         break;
```

Programmers not familiar with Drupal should be able to understand this code.

## Inflexible

The code is not flexible. If extra fields are added to content types,
or extra content types are added, this code will need to be changed,
to allow access to the new objects.

This is because of a common security practice: access is denied by default. The 
code only grants access when it recognizes a specific situation that it knows 
about. It won't know about new objects.

The only exception to this is that custom fields can be added to user
entities. This lets authors and schools react to, for example, the
disability status of students.

Implementation note: The `getNodeFieldAccess()` method returns 
`AccessResult::forbidden()` for fields it wants to deny, or does not 
recognize. For fields where access is to be granted, it returns 
`AccessResult::neutral()`, *not* `AccessResult::allowed()`. So, the 
best the method will do is not deny the user access to a field. Access 
could still be denied by other code.

(Unfortunately, Drupal uses different logic in different circumstances. That is
explained below.) 

Implementation note: Each content type has its own field access check method. 
They all follow the same code template. To add a method for a new content type, 
copy the code in `AccessCheckerFieldsInContentTypeTemplate.txt`. This will make 
it easier for auditors to check the code, since it will all follow the same
pattern.

# Calling SkillingAccessChecker

There are several places in Drupal where access checks 
are done. In each case, there is glue code for calling `SkillingAccessChecker`.

Here are the places you'll find glue code.

## Hooks

Hooks for access checking have been moved out of `skilling.module` into 
`module-chunks/access-control.inc` for easy inspection. 

`skilling_entity_access()`

Limits access to entities. Calls `SkillingAccessChecker->getUserEntityAccess()` or 
`SkillingAccessChecker->getNodeEntityAccess()` depending on the entity type being checked.

The hook is invoked when users access entities through their canonical URLs, like `/node/123`, or through 
aliases. The hook limits URL guessing.

`skilling_block_access()`

Limits access to blocks. Calls `SkillingAccessChecker->getBlockAccess()`.

`skilling_entity_field_access()`

Limits access to fields in entities. Calls `SkillingAccessChecker->getFieldAccess()`.

`skilling_file_download()`

Restricts users' ability to download files. Note that Skilling requires that a private file 
system be set up. 

`skilling_query_search_node_search_alter()`

Limits access to search results by changing the search query.

`skilling_views_post_execute()`

Removes rows and fields from views.
    
## Views plugins

`/src/Plugin/views` has views access, field, filter, and query plugins. Access plugins 
give all-or-nothing access to views. Filter plugins limit the rows that a view will
display. Field and query plugins are not about access control _per se_, but can limit 
the data displayed.

There is overlap between filter plugins and `skilling_views_post_execute()`. 

## Controllers

There are three topics here.

### `skilling.router.yml`

Routes can have a `_custom_access` key. It specifies a controller method that can refuse 
access to the route.

The method returns either `AccessResult::forbidden()` or `AccessResult::allowed()`.
The latter is correct. In Drupal 8, other code can still block access, even 
if the controller's access method returns `AccessResult::allowed()`.
According to the documentation:

> If a route has multiple access checks, the andIf operation is used to
> chain them together: all results must be AccessResult::allowed otherwise
> access will be denied.

(https://www.drupal.org/docs/8/api/routing-system/access-checking-on-routes)

Note that this is different from the way entity access works, as 
described earlier. 

The access methods use the `SkillingAccessChecker` service, of course.

### Extra controller checks

Controllers with a `_custom_access` method can still conduct their own access checks 
before returning data. This is not strictly needed, but is there because... reasons.

They use the `SkillingAccessChecker` service.

### AJAX responders

Skilling does not use REST. Instead, there are custom controllers that respond to 
predefined AJAX calls. Each one uses the `SkillingAccessChecker` service.

Some controllers support the feedback interface, a Vue application. Those controllers
have extra security checks, like script sources, and current sessions. In fact, 
most of the code in those controllers is security code.

## Custom tag plugins

The Skilling parser supports custom tags that insert nodes into lessons. An example:

```
exercise.
  internal_name: dogs_are_great

```

There is a custom plugin type called `SkillingCustomTag`.
It has about a dozen plugins at the time of writing.

Each plugin is responsible for its own security checking, calling
`SkillingAccessChecker`. `SkillingCustomTagBase` has some convenience methods.

# Enrollment node publication

If an enrollment node is unpublished, Skilling will act as if it does
not exist.

# Infrastructure checks

Most of Skilling's security and privacy checks are at the application 
level. There are a few aspects of infrastructure that 
Skilling checks as well.

## Skilling requires a private file system

Drupal reuses entity fields
across content types (allowing for storage settings to be reused). This
means that it is sometimes difficult to change a file field from using the
public file system, to using the private system. 

A simple solution: SKilling cannot be enabled unless a private file system has been
set up for the Drupal instance. Skilling's online documentation 
explains how to do this.

## HTTPS

Every Skilling website should use HTTPS. However, Skilling does not
force you to do this. It will nag you about it, though.

See Skilling's online documentation for details.

## Trusted server setting

Drupal's `settings.php` file accepts a regex specifying where files
are allowed to be served from. Drupal and Skilling complain
if this isn't set.

See Skilling's online documentation for instructions.

