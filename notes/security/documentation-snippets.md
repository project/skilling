https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/group/entity_api/8.2.x

The default entity access control handler invokes two hooks while checking access on a single entity: hook_entity_access()
is invoked first, and then hook_ENTITY_TYPE_access() (where ENTITY_TYPE is the
machine name of the entity type). If no module returns a TRUE or FALSE value from
either of these hooks, then the entity's default access checking takes place. For
create operations (creating a new entity), the hooks that are invoked are
hook_entity_create_access() and hook_ENTITY_TYPE_create_access() instead.

The Node entity type has a complex system for determining access,
which developers can interact with. This is described in the Node access topic.



https://www.drupal.org/docs/8/api/routing-system/access-checking-on-routes:

If a route has multiple access checks, the andIf operation is used to
chain them together: all results must be AccessResult::allowed otherwise
access will be denied. Returning a AccessResult::neutral() and
a AccessResult::forbidden() object does not make any difference.

BUT...

https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/function/hook_entity_access/8.2.x

The final result is calculated by using \Drupal\Core\Access\AccessResultInterface::orIf()
on the result of every hook_entity_access() and hook_ENTITY_TYPE_access() implementation,
and the result of the entity-specific checkAccess() method in the entity access control
handler. Be careful when writing generalized access checks shared between routing
and entity checks: routing uses the andIf() operator. So returning an isNeutral()
does not determine entity access at all but it always ends up denying access while routing.


The two most important countermeasures to prevent cross-site scripting attacks are to:

* Constrain input.
* Encode output.

