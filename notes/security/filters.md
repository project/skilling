This documents describes Skilling's approach to user input filtering. Input comes from:

* Drupal forms
* AJAX requests
* The search form

Drupal forms
===

Drupal has a powerful form system that can, when configured correctly, protect against
malicious user input. Drupal lets site administrators configure "input formats." 
Input formats do various things, but we are concerned here with input filtering, 
applied on outbound traffic. 

Skilling uses two custom input formats, called:

* Skilling
* Stripped

Skilling sets them up and applies them during installation. Administrators should 
check the configuration on install, but should not need to change anything.

Skilling
---

Only authors and administrators can use the Skilling input format. It is a 
Textile parser wrapped in a Skilling parser.

Anything is allowed, including JS.

Stripped
---

All other roles use the Stripped input format, for all of their input.

The Stripped input format uses a filter supplied with Drupal, configured to allow:

* Inline formatting: strong, em, code.
* Blocks and page breaks: p, br.
* Lists: ul, ol, li.
* Links: a.

The lang and dir attributes are allowed on all tags. In addition, the a tag allows
the href and hreflang attributes. JavaScript event attributes, JavaScript URLs, 
and CSS are always stripped. No other attributes, like class,
are permitted.

This might seem overly restrictive. However, in Skilling, only authors create complex content. 
Content from other roles is considered ephemera, like feedback, reflect notes, fill-in-the-blank 
responses, and messages. Users need only a few HTML tags for these purposes.

The a tag is necessary when, for example:

* Students submit URLs to their work.
* Graders link to content that will help students correct errors.

For these purposes, only the href attribute is necessary.

The img tag is not included. Student can submit images by uploading images files. 

Accessibility
---

The whitelist does not include h1-h6. This means that long-form content 
created with the Stripped input format will not be able to have accessible headings. 

The Stripped format is not suitable for long-form content. 
If, for example, some instructors want to add new lessons, exercises, etc., 
they should be given the author role, or work with someone who has that role.

Twig escaping
---

Drupal uses the Twig templating system. All Skilling output runs through Twig.

By default, Drupal turns on Twig autoescaping, which uses htmlspecialchars(). 

Twig's raw filter is used to suppress autoescaping of content as needed. 

AJAX requests
===

Skilling uses AJAX for some user input, like fill-in-the blank, and reflect notes. 
This input is not processed by Drupal's form system, so the Stripped input format
is not applied automatically. User input is sometimes reflect in output, e.g., 
in reflect notes.

Several checks are used. First, SkillingAjaxSecurityInterface::securityCheckAjaxRequest
does some basic checking. It examines requests, checking methods, paths, and a
few other things.

Second, student data from AJAX forms is run through the 
same filters that Stripped uses, before
being stored. So, anything except the tags and attributes listed above will be removed.

Third, student data is run through Stripped again before being included in output.

Skilling does not apply filtering on the client in JS. JS AJAX calls can be simulated. The
origin policies from the trusted host setting will help defend against that,
but it is possible to run Skilling without that setting. 

For each AJAX call that stores data from users, please use the following checklist:

* Controller called from JS.
  * Call SkillingAjaxSecurityInterface::securityCheckAjaxRequest. Throw
    an exception if the check does not pass.
  * Input from client. Apply Stripped. The code is given below.
    It is suggested that Stripped to applied to all fields, even those that
    are not expected to have direct user input.
  * Storing input in content a node. Be sure that Stripped has been applied. 
  * Storing input in history a node. Be sure that Stripped has been applied.
  * Output returned to client. Be sure that Stripped has been applied.
* Custom tag rendering plugin. Apply Stripped if needed. The plugins use 
  Twig templates. Apply Stripped before data is passed to the template,
  in a render array.
* History plugin. The history system uses plugins for each event type. The
  plugins have separate methods for rendering HTML, and XML. Stripping
  can be applied to data originating from user input.
* JS library. Examine the library, to be sure that all AJAX data exchanges
  are covered. Recall that it may be unsafe to rely solely on JS to 
  filter user input.
  
See `FibController` and `CompleteFib` for examples.

Note that filtering user input multiple times will not cause ill effects.

Sample code for the AJAX security check:

```
$passedBasicSecurity = $this->ajaxSecurityService->securityCheckAjaxRequest(
  $request,
  ['POST'],
  ['/skilling/check-fib-response'] // URL of the controller.
);
if (!$passedBasicSecurity) {
  throw new AccessDeniedException('Access denied, sec fail in FiB::checkResponse');
}
```   
  
The code to run the filter is:

```
$filtered = $this->ajaxSecurityService->filterUserContent($unfiltered);
```

This assumes that appropriate dependency injection has been used. For example:

```
use Drupal\skilling\Access\SkillingAjaxSecurityInterface;
...
  /**
   * Service to check AJAX calls.
   *
   * @var \Drupal\skilling\Access\SkillingAjaxSecurityInterface
   */
  protected $ajaxSecurityService;
...
  /**
   * Constructs a new controller object.
... 
   * @param \Drupal\skilling\Access\SkillingAjaxSecurityInterface $ajaxSecurityService
   *   AJAX security checking service.
...
   */
  public function __construct(..., SkillingAjaxSecurityInterface $ajaxSecurityService, ...) {
...  
    $this->ajaxSecurityService = $ajaxSecurityService;
...
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
...
      $container->get('skilling.ajax_security'),
...
    );
  }
```

The search form
===




