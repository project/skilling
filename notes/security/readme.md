Security and privacy is central to Skilling's design. This directory has 
several documents on the topic.

* `roles-and-permissions.md` Start here to learn about Skilling security and
privacy concepts, and what policies are enforced.
* `implementation.md` Describes the techincal implementation of the policies.
* `documentation-snippets` Snippets from Drupal's documentation explaining some
aspects of the code that Skilling relies on.
