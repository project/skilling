# Installation

Installation

X - add new fields
 - test on shared host

X - initial valuies of new config flags and shit.

? Trimmed date format

Module updates

User fields not active on form.

# Parsing

Ignore missing exercises in commented stuff.


# Enrollment

  public function getIsProgressScoreBasedOnRequiredOnlyForCurrentEnrollment() {
    $baseOnRequired = $this->currentEnrollment
      ->get(SkillingConstants::FIELD_BASE_PROGRESS_ON_REQUIRED_ONLY)
      ->getValue()[0]['value'];
      
? fails if no current enrollment (SkillingCurrentClass line 317)

Add checks for no current enrollment.

Add report for accounts with no current enrollment.     


# Classes

When delete, fail if there are enrollments in the course.

Other such dependencies?

- fail delete enrollment if there are submissions for it.

Clone!


# Exercises

Rebuild exercise references command?

list - exercise that is added twice has two items in the exercise list.




# Add user form

Check student role, and highlight automatically.

Add enrollment button on view, fill in user field automatically.

On enrollment form, choose the last class that was chosen.


# Grading

No show names, ho put in fb message.

# Permission

Grader can get to http://s1.localhost/admin/config/user-interface, does nothing.

What do graders see when there is no admin toolbar?
- search in admin toolbar.


# Submission delays - user confidence

When submit, can be slow, tell user that something is happening.

Same for showing the exercise in the first place.


# Starter content 
- can't create paragraphs when adding exercises due to class.
- class - start date MT
- description text format not selected
- same for notes




# Skilling config

After install, config form shows initials not required. Student submission asks for them.
Save config form, and it works, when checkbox is clear. Need to explicitly set
the value of the flag during install?

?- changed code, did it work?

- check other config flag init code. Some things missing?


# User display

Button to show submissions. 

 
# Testing

Check progress score computation, and reports for instructor.


# Grading interface

Missing linethrough. CAN'T FIGURE OUT. GIVE UP.

When grader can't see names, what happens generate message containing names?
- Maybe turn off the names thing for now.

Override completed - set when needs work.

? During fetching data for the interface, only include classes in student data 
that the grader has access to. 


Toggle of keep-this-response unclear, when id on or off.

- doesn't work? Stops working?

Refresh after a while crashes.


? **2.0** Maintain order of RIs in text message

**2.0** Check if excludes RIs that are not shown for a given exercise.



Save and edit fior exercises

? Skip submissions for exercises/students that don't exist anymore. Or student blocked.


Authors use skilling input filter.

Everyone elses uses stripped, or plain.



[Installation](#installation)

[Starter content](#starter-content)

[Class admin](#class-admin)

[Content editing](#content-editing)

[Model, patterns, principles lists](#models-patterns-principles-lists)

[Characters](#characters)

[Custom tokens and tags](#custom-tokens-and-tags)

[Exercise and rubric authoring](#exercise-and-rubric-authoring)

[Grading interface](#grading-interface)

[Student monitoring](#student-monitoring)

[Submissions management](#submissions-management)

[Users](#users)

[Notifications](#notifications)

[Suggestions](#suggestions)

[Lesson tree](#lesson-tree)

[History](#history)

[Badges](#badges)

[MCQ](#mcq)

[FiB](#fib)

[Calendar](#calendar)

[Security](#security)

[Performance](#performance)

[A11y and I18N](#a11y-and-i18n)

[Theme](#tTeme)

[Docs and help](#docs-and-help)

[Code](#code)

[Content export](#content-export)

[Website](#website)



# Installation

**2.0** Add new fields.

**2.0** Make sure installation scripts work on Reclaim.

Module update procedures.

Can install all three modules at once?

Turn show back to top on by default on install.

Make character records during install.

Uninstall paragraph types and taxonomies

Remove theme on uninstall. Or tell user to make it not current.




# Starter content

Still works?

When create starter content, is user About set to stripped format?

Starter - use batch API



# Class admin

**2.0** Check progress score computation, and reports for instructor.

Add recompute submission scores option to classes operations.

X Class list, show start date and number of days.

Enrollments view for class - show score, when last updated.

X Add enrollment from class form, fill in the class field.

Field to add exercises required for class, use in showing
data to students.

  Count of all required exercises should be less than this number?
  
  Do it dyamically - required is max of class config number, and 
  number of exercises marked as required.
  
  


# Content editing

When using the code special tag, make it easy to copy the code. 

Use syntax highlighter?

In editor, drop-down for inserting tokens?

cloning

X **2.0** When multiple instances of same exercise on a page, clicking Submit shows error.

  Error message - not supported.

X **2.0** On every node display , add links to add another, and show admin list.

? **2.0** Form save internal name - strip leading spaces

Save&Edit

    - add to module 
    
    - on full-screen edit view?

Autosave - add to module.

X **2.0** Author can't save lesson with attached file 
added by admin. "You do not have access to the referenced entity."



# Model, patterns, principles lists

X **2.0** Suppress menu items when no items of each type.


# Characters

Package Ts chars.

X **2.0** Show chars list at top of all char pages.


# Custom tokens and tags

Let Custom tags supply their own CSS.

Debug, for authors to see results of expression evaluation. Option in settings tag? Checkbox (field)?

Time since last login - student token.

"Why do this?" add to pause.

For tag with content., need blank line after tag.
Change error message when tag problem found?
Prefer fix it.

ToC - finish implementing. 

Instructor can insert their own stuff in lessons.

General insert node tag?

X **2.0** Quick commenting, or ignore-me thing.

**2.0** Map out content escaping.

pause. w/o blank line in front breaks.


# Exercise and rubric authoring

X **2.0** Optional exercises

X  What effect on smiley face? Adjust algo.

X **2.0** Rubric item selection interface

X - If use clicks Save button on edit form before interface is initialized, then 
abort the load.

X **2.0** Creating new lesson, add ref exercise that doesn't exist yet, 
then create the exercise. Need to edit the lesson after 
creating the exericse, remove and add the ref to the exercise.

  Create exercise with dummy title? Could ask user when 
  detect this when saving. Show error message, with link
  to create exercise with give internal name, open in a new
  tab.
  
  How work with auto save? Might be OK, if autosave 
  isn't a real save, just a save to a dummy thing.
  
  Could disallow saving lesson when it refs an exercise
  that does not exist.

X **2.0** "Add exercise" and "Exercise admin" buttons appearing for students on exercises.

X **2.0** Limit number of resubmissions.

X  Max for entire course.
  
X  Limits for individual exercises. Esp for exams.
  
  


# Grading interface

Toggle of keep-this-response unclear, when id on or off.

- doesn't work? Stops working?

Refresh after a while crashes.

**2.0** Show prior submissions.

**2.0** Maintain order of RIs in text message

**2.0** Check if excludes RIs that are not shown for a given exercise.

**2.0** Allow ungrade option. Allow regrade option. Delay notification?




# Student monitoring

Overdue exercises report for student. Maybe deets link on progress report.

Email students N days after exer is due.

x **2.0** Put due date on each exercise display. In x days.

**2.0** Instructor can show history for student.


# Submissions management

Manage submissions view - bulk views operations

How detect cheaters?

    Maybe compare digests of submissions.
    
X **2.0** Make initials check optional
  Was too easy - didn't check everything.


    
# Users

Give students a nickname, e.g., obsessive koala



# Notifications

X **2.0** Works?


# Suggestions

After make suggestion, move focus to thank you message, so hit enter clears

Suggestions page - put reply to user button/link



# Lesson tree

X No tree for authors. After editing a node.
- Race condition? Updating tree?

Sibling page in right place, following page with the button.

X Make server-side tree replacement
- make tree code nonajax?


# History

**2.0** /why-history-data-kept

**2.0** Export all the things.

npm build adds hash to file name, update libraries.xml each time. How to fix?

X **2.0** Add other event types

    X Update installation files
    
    Edit submission does not show what changed.
    
    Edit account does not show what changed.

    X Log in
    X Log out
    
    X View basic page
    X View lesson
    X View pattern
    X View model
    X View principle
    X View badge desc
    
    X View lesson list
    X View exercise list
    X View badges list
    X View patterns list
    X View principles list
    X View models list
    
    x View submissions list
    x View submissions policies
    X View class
    x View reflect notes list
    
    X View notices list
    
    X Search
    
    X View account
    X Update account
    
    x Submit suggestion
    
    X Submit submission
    x Update submission
    x Del submission
    X View submission
    X View feedback
    
    X MCQ answer
    X FiB answer
    X Reflection answer
    X Message to instructor/grader
    X Awarded badge
      
    

Data analysis scripts

**2.0** Students can see own history.


# Badges

Award for number of nonchallenge exercises?



# Reflection note

Reflection note link for student on list jumps to position of note on page.

Reflect notes view for admins, instructors, authors.



# Calendar

X Install - remove old content types.

X Access check - remove content types.

X Constants - remove content types.

x Redo completion score?
  
X Timeline service

X Timeline client.

X Tokens

X CalendarController

X Calendar planner - kill?

Make starter content

X Timeline template

Tests?

Feedback interface?

X Exercises, X getCourseExerciseSummary, X getEventsForCalendar

x Utilities - remove calendar stuff



# Security

**2.0** Rabbit hole module

Field permission approach for non-user fields.

Flood control for suggestions.

flood control on suggestions, search?

Check no direct access to reflect notes.

flopod control on fib and reflect notes

Can run fb index.html directly?

Student enters a link in submission like:

<a href=# onclick="document.location='http://a-malicious-link.com/xss. php'">Malicious Link 1</a>

<a href="http://localhost:8080/todo?javascript:window. onload=function(){var link=document.getElementsByTagName('a');link[0]. href='http://malicious-website.com/';}">This is an alert</a>

review roles-and-permissions.txt

Search finds text in field that should be hidden from the user?

Check students can't see each others hostory.



# Performance


# A11y and I18N



# Theme

In CSS, use transitions for buttons on mouse over? Same for links?

Update screenshot.png to use new logo.

RSB not showing for mobile



# Docs and help

https://textile-lang.com/ - link to textile ref

Remake, so others can easily add.

Comments.




# Code

Constants

Consistent field access

Remove inactive views plugins.

Typescript


# Content export

Export all content to XML files.

Other options for using Textile content.


# Website

Discussion

