OUT OF DATE

What happens when an entity is being deleted when it is referenced in 
an entity reference field?

If not required, then set reference field to empty.

References:
    class: field_calendar -> calendar. Not required.
    calendar: field_calendar_events -> calendar event. Not required. ERR (ER revisions)
    rubric_item: field_categories -> taxonomy, rubric item cats.
    enrollment: field_class -> class, required
    submission: field_exercise -> exercise, required.
    event: field_exercise -> exercise, not required.
    Submission: field_feedback_source -> user, not required unless there is feedback
    Fill-in-the-blank: field_fib_responses-> para, is required.
        Check what interface does when delete last response.
    Reflect note: field_node -> node, required.
        When referenced element is deleted, kill these too.
    MCQ: field_responses-> para, is required.
        Check what interface does when delete last response.
    Exercise: field_rubric_items, not required.
    Rubric item: field_rubric_item_responses -> ri responses
        Check what interface does...
    History: field_subject_node -> node, not required.
    field_tags - LessonExerciseDesign pagePatternPrincipleModel
    field_user - Enrollment Submission History
    field_where_referenced - PatternPrincipleExerciseFill-in-the-blankCharacterMultiple-choice questionModel
    field_event - Calendar event
    field_exercises - Rubric item response
    
