<?php

namespace Drupal\skilling_content_types\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Reports that the private file system is not configured.
 *
 * @package Drupal\skilling_content_types\Exception
 */
class SkillingContentTypesNoPrivateFileSystemException extends \Exception {

  use StringTranslationTrait;

  /**
   * SkillingContentTypesNoPrivateFileSystemException constructor.
   */
  public function __construct() {
    $message = $this->t(
      "Sorry, Drupal's private file system must be set up first."
    );
    parent::__construct($message);
  }

}
