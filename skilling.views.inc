<?php

/**
 * Implements hook_views_data().
 */
function skilling_views_data_alter(&$data) {
  $data['views']['table']['group'] = t('Custom Global');
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['date_plus_days_field'] = [
    'title' => t('Date plus days field'),
    'help' => t('Creates a field showing the date you get by adding a number of days to a date.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'date_plus_days_field',
    ],
  ];

  $data['views']['pattern_summary_needs_provides'] = [
    'title' => t('Pattern summary, needs, provides'),
    'help' => t('A field showing the summary, needs and provides for a pattern.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'pattern_summary_needs_provides',
    ],
  ];

  // Show whether a notice has been read.
  $data['views']['is_notice_unread'] = [
    'title' => t('Is notice unread?'),
    'help' => t('Creates a field showing whether a notice has been read.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'is_notice_unread',
    ],
  ];

  // Show whether an exercise is required.
  $data['views']['is_exercise_required'] = [
    'title' => t('Is exercise required?'),
    'help' => t('Creates a field showing whether an exercise is required.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'is_exercise_required',
    ],
  ];

  // Show whether an exercise is required.
  $data['views']['exercise_due_date'] = [
    'title' => t('Exercise due date'),
    'help' => t('Date an exercise is due.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'exercise_due_date',
    ],
  ];

  // Max submissions allowed for an exercise.
  $data['views']['max_submissions'] = [
    'title' => t('Max submissions allowed for an exercise'),
    'help' => t('Maximum submissions of allowed for an exercise.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'max_submissions',
    ],
  ];

  // Really a filter.
  $data['views']['instructor_of_node_owner'] = [
    'title' => t('Current user is instructor of node owner'),
    'help' => t('Current user is an instructor of the node\'s owner.'),
    'group' => 'Skilling',
    'field' => [
      'id' => 'instructor_of_node_owner',
    ],
  ];

  $data['node__field_when_read']['unread_filter'] = array(
    //  $data['node_field_data']['unread_filter'] = array(
    'filter' => [
      'title' => t('Notices to show'),
      'help' => t('Only show unread notices, or show all?'),
      'field' => 'field_when_read_value',
      'id' => 'unread_filter',
      'group' => 'Skilling',
    ],
  );

  $data['node_field_data']['instructor_of_node_owner_filter'] = array(
    'filter' => [
      'title' => t('Current user instructor of node owner'),
      'help' => t('Is the current user an instructor of the node owner?'),
      'field' => 'instructor_of_node_owner',
      'id' => 'instructor_of_node_owner_filter',
      'group' => 'Skilling',
    ],
  );

  $data['node_field_data']['instructor_of_node_owner_in_class_filter'] = array(
    'filter' => [
      'title' => t('Current user instructor of node owner in class from URL'),
      'help' => t('Is the current user an instructor of the node owner in the class with the id given as the last argument of the URL?'),
      'field' => 'instructor_of_node_owner_in_class_filter',
      'id' => 'instructor_of_node_owner_in_class_filter',
      'group' => 'Skilling',
    ],
  );

  $data['node_field_data']['node_owner_student_in_class_filter'] = array(
    'filter' => [
      'title' => t('Node owner is student in class from URL'),
      'help' => t('Is the node owner a student in the class with the id given as the last argument of the URL?'),
      'field' => 'node_owner_student_in_class_filter',
      'id' => 'node_owner_student_in_class_filter',
      'group' => 'Skilling',
    ],
  );


  // For query plugin giving data on one student's exercise submissions.
  $data['student_submissions']['table']['group'] = t("Student's submissions");
  $data['student_submissions']['table']['base'] = [
    'title' => t("Student's submissions"),
    'help' => t("A student's exercise submissions."),
    'query_id' => 'student_submissions',
  ];
  // Fields.
  $data['student_submissions']['exercise_nid'] = [
    'title' => t('Exercise id'),
    'help' => t('Node id of an exercise.'),
    'field' => [
      'id' => 'numeric',
    ],
  ];
  $data['student_submissions']['exercise_title'] = [
    'title' => t('Exercise title'),
    'help' => t('Title of an exercise.'),
    'field' => [
      'id' => 'standard',
    ],
  ];
  $data['student_submissions']['exercise_internal_name'] = [
    'title' => t('Exercise internal name'),
    'help' => t('Internal name of an exercise.'),
    'field' => [
      'id' => 'standard',
    ],
  ];
  $data['student_submissions']['exercise_max_subs'] = [
    'title' => t('Exercise maximum submissions'),
    'help' => t('Maximum submissions allowed for an exercise.'),
    'field' => [
      'id' => 'standard',
    ],
  ];
  $data['student_submissions']['exercise_due_date'] = [
    'title' => t('Exercise due date'),
    'help' => t('Exercise due date.'),
    'field' => [
      'id' => 'date',
    ],
  ];
  $data['student_submissions']['where_referenced'] = [
    'title' => t('Where referenced'),
    'help' => t('Lessons that include the exercise.'),
    'field' => [
      'id' => 'markup',
      'format' => 'full_html',
    ],
  ];
  $data['student_submissions']['completed'] = [
    'title' => t('Completed?'),
    'help' => t('Has the student completed the exercise?'),
    'field' => [
      'id' => 'boolean',
    ],
    'filter' => [
      'id' => 'completed_exercise',
    ],
  ];
  $data['student_submissions']['status'] = [
    'title' => t('Status'),
    'help' => t('The status of submissions for an exercise.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'filter_status',
    ],
  ];
  $data['student_submissions']['when_first_submission'] = [
    'title' => t('First submission date/time'),
    'help' => t('When was the first submission for an exercise?'),
    'field' => [
      'id' => 'date',
    ],
  ];
  $data['student_submissions']['submissions'] = [
    'title' => t('Submissions'),
    'help' => t("Submissions for a student's exercises."),
    'field' => [
      'id' => 'markup',
      'format' => 'full_html',
    ],
  ];

}
