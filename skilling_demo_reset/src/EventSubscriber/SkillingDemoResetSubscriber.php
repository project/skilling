<?php

namespace Drupal\skilling_demo_reset\EventSubscriber;

use DateInterval;
use DateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\skilling\SkillingConstants;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class SkillingDemoResetSubscriber.
 */
class SkillingDemoResetSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  const MAX_DAYS_DIFFERENCE_BEFORE_RESET = 9;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new SkillingDemoResetSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'] = ['kernelRequest'];
    return $events;
  }

  /**
   * The kernel.request event was dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   What happened.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function kernelRequest(Event $event) {
    $request = \Drupal::request();
    $isAjax = $request->isXmlHttpRequest();
    if (!$isAjax) {
      // Need to reset class dates?
      if ($this->isResetClassDates()) {
        // Delete all submissions.
        $this->deleteAllSubmissions();
        // Reset class start dates.
        $this->resetClassStartDates();
      }
    }
  }

  /**
   * Is a class date reset needed?
   *
   * @return bool
   *   True if a class date reset is needed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  protected function isResetClassDates() {
    $result = FALSE;
    // Get the start date of a class.
    // Find classes.
    $classIds = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', SkillingConstants::CLASS_CONTENT_TYPE)
      ->execute();
    // Are there any?
    if (count($classIds) > 0) {
      // Yes.
      // Grab the first one.
      $classId = reset($classIds);
      /** @var \Drupal\node\NodeInterface $class */
      $class = $this->entityTypeManager->getStorage('node')->load($classId);
      // Get the start date.
      $classStartDate = $class->get(SkillingConstants::FIELD_WHEN_STARTS)->value;
      $classStartDate = new \DateTime($classStartDate);
      // Get now.
      $now = new \DateTime();
      $interval = $now->diff($classStartDate);
      $daysDiff = $interval->format('%a');
      $result = $daysDiff > self::MAX_DAYS_DIFFERENCE_BEFORE_RESET;
    }
    return $result;
  }

  /**
   * Delete all exercise submissions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function deleteAllSubmissions() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $submissionIds = $nodeStorage->getQuery()
      ->condition('type', SkillingConstants::EXERCISE_SUBMISSION_CONTENT_TYPE)
      ->execute();
    if (count($submissionIds) > 0) {
      $nodes = $nodeStorage->loadMultiple($submissionIds);
      $nodeStorage->delete($nodes);
      $this->messenger->addStatus($this->t('Submissions deleted.'));
    }
  }

  /**
   * Reset class' start dates.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function resetClassStartDates() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    // Find classes.
    $classIds = $nodeStorage->getQuery()
      ->condition('type', SkillingConstants::CLASS_CONTENT_TYPE)
      ->execute();
    if (count($classIds) > 0) {
      $classes = $nodeStorage->loadMultiple($classIds);
      // Compute class start date.
      // Work out the start date for class 1.
      $startDate = new DateTime();
      $startDate->sub(new DateInterval('P' . self::MAX_DAYS_DIFFERENCE_BEFORE_RESET . 'D'));
      $dateFormat = 'Y-m-d';
      $startDateFormatted = $startDate->format($dateFormat);
      /** @var \Drupal\node\NodeInterface $class */
      foreach ($classes as $class) {
        $class->set(SkillingConstants::FIELD_WHEN_STARTS, $startDateFormatted);
        $class->save();
      }
      $this->messenger->addStatus($this->t('Class start dates reset.'));
    }
  }

}
