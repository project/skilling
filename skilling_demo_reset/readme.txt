In info.yml:

hidden: true - This will hide your module from the module list
on the Extend page (/admin/modules). You might find it useful
to hide a module if it only contains tests, or is intended to
serve as an example for developers who need to implement the
main module's API. You can make these modules visible by
adding

$settings['extension_discovery_scan_tests'] = TRUE;

to your settings.php.
