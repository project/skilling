Skilling is a method and software for creating and
running skill courses. A skill course is a course
where students learn to do tasks, like writing programs,
analysing data, and writing reports.

Skilling is based on research in learning science. Schema theory and
cognitive load theory are the primary
theoretical commitments. Skilling aligns well with 4C/ID, and
a subset of Jeroen J. G. van Merriënboer and Paul A. Kirschner,
"Ten Steps to Complex Learning", 3rd edition, 2018. See
http://www.tensteps.info/.

Project deets at https://skilling.us.


